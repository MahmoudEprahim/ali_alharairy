<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessOwnersInterviews extends Model
{
    protected $table = 'business_owners_interviews';
    protected $fillable=[
        'applicant_id',
        'branche_id',
        'company_id',
        'datepaper',
        'move_number',
        // 'move_number_paper',
        'passport',
        'date_passport',
        'certificate',
        'date_certificate',
        'fash_and_metaphor',
        'date_fash_and_metaphor',
        'representations',
        'date_representations',
        'data_degrees',
        'date_data_degrees',
        'ex_certificates',
        'date_ex_certificates',
        'disclaimer',
        'date_disclaimer',
        'driving_license',
        'date_driving_license',
        'datemedical',
        // 'move_number_medical',
        'net',
        'date_net',
        'fingerprint',
        'date_fingerprint',
        'medical_examination',
        'date_medical_examination',
        'laboratories',
        'date_laboratories',
        'datevisa',
        // 'move_number_visa',
        'consulate',
        'date_consulate',
        'visa_on_site',
        'date_visa_on_site',
        'visa_approved',
        'date_visa_approved',
        'datecontract',
        // 'move_number_contract',
        'consular_contract',
        'date_consular_contract',
        'man_power',
        'date_man_power',
        'datepassport',
        // 'move_number_passport',
        // 'contract_value',
        // 'value_of_fees',
        // 'payee',
        'delivery_date',
        'datetravelone',
        // 'move_number_travel',
        'date_travel',
        'Port_takeoff',
        'takeoff_time',
        'date_arrival',
        'port_access',
        'arrival_time',
        'imagetravel',
    ];
    public function branches(){
        return $this->hasOne('App\Branches','id','branche_id');
    }
    public function applicant(){
        return $this->hasOne('App\Applicant','id','applicant_id');
    }
    public function company(){
        return $this->hasOne('App\Company','id','company_id');
    }
}
