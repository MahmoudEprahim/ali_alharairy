<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryMedia extends Model
{
    protected $table = 'librarymedia';
    protected $fillable = [
        'media',
        'name_ar',
        'name_en',
        'desc_ar',
        'desc_en',
        'grade_id',
        'branch_id',
        'list_id',
        'Librarytype',
    ];

     public function branch()
    {
        return $this->belongsTo('App\Branches');
    }

    public function list()
    {
        return $this->belongsTo('App\LibraryList');
    }
    
    public function grade()
    {
        return $this->belongsTo('App\Grade');
    }
}


