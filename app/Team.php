<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';
    protected $fillable = [
        'name_ar',
        'name_en',
        'teams_title_ar',
        'teams_title_en',
        'teams_desc_ar',
        'teams_desc_en',
        'facebook',
        'twitter',
        'google',
        'linkedin',
        'email',
        'phone',
        'image',

        ];

}
