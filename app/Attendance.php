<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table = 'attendances';
    protected $fillable = ['student_id', 'month_id', 'lecture_id'];


    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
