<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategoryChild extends Model
{
    protected $table = 'sub_category_child';
    protected $fillable = ['child_name_en', 'bodyContent', 'cat_id', 'sub_cat_id'];


    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class,'id', 'sub_cat_id');
    }
}
