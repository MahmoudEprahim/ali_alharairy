<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sayabout extends Model
{
    protected $table = 'sayabout';

    protected $fillable =
    [
    	'name',
    	'graduation_year',
    	'current_work',
    	'email',
        'comment',
        'isactive',
    ];

}
