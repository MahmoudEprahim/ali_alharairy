<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeVideo extends Model
{
    protected $table = 'homevideos';
    protected $fillable = [
        'media',
        'desc_ar',
        'desc_en',
        'branch_id',
    ];

     public function branch()
    {
        return $this->belongsTo('App\Branches');
    }
}


