<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $table = 'appointments';

    protected $fillable =
    [
    	'groups',
    	'days',
    	'capacity',
    	'count',
    	'branch_id',
        'grade_id',
    ];

    public function branch()
    {
        return $this->belongsTo('App\Branches');
    }

    public function grade()
    {
        return $this->belongsTo('App\Grade');
    }

    public function students(){
        return $this->hasMany('App\Student');
    }


}
