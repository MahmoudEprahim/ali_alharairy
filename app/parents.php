<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class parents extends Authenticatable
{

    protected $table = 'parents';
    protected $guard = 'parent';
    protected $hidden = ['password', 'remember_token'];
    protected $fillable = [
        'name_ar',
        'name_en',
        'active',
        'password',
        'phone_1',
        'phone_2',
        'mobile_1',
        'mobile_2',
        'relation',
        'type',
        'countries_id',
        'nations_id',
        'number',
        'date_issuance',
        'expired_date',
        'created_from',
        'job',
        'home_address',
        'note',
        'education_level',
        'Specialization',
        'email',
        'monthly_income',
        'average_income',
        'tax_number',
        'birthdate',
        'birthplace',
        'work_status',
        'labor_sector',
        'job_number',
        'job_phone',
        'job_address',
        'boys_num',
        'girls_num',
        'state_id',
        'city_id',
        'street',
        'house_num',
        'compound_num',
        'compound_name',
        'near',
        'remember_token',
        'password',

    ];

    public function students()
    {
        return $this->hasMany('App\Student','parent_id','id');
    }
}
