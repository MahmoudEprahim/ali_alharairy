<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryList extends Model
{
    protected $table = 'librarylists';

    protected $fillable = [
       'name_ar',
        'name_en',
        'Librarytype',
    ];

    public function media(){
        return $this->hasMany('App\LibraryMedia');
    }

    public function books(){
        return $this->hasMany('App\LibraryBook');
    }
}
