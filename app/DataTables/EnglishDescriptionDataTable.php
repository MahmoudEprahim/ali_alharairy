<?php

namespace App\DataTables;

use App\EnglishDescription;
use Yajra\DataTables\Services\DataTable;

class EnglishDescriptionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addColumn('edit', function($query){
            return '<a href="englishdescription/'.$query->id.'/edit" class="btn btn-success edit"><i class="fa fa-edit"></i> ' . trans('admin.edit') .'</a>';
        })->addColumn('show', function ($query) {
            return '<a href="englishdescription/'.$query->id.'" class="btn btn-info"><i class="fa fa-info"></i> ' . trans('admin.information_details') . '  </a>';
        })
        // ->addColumn('list',function($query){
        //     return $query->list->name_ar;
        // })
        ->addColumn('image', function ($query) {
            $url= asset('storage/'.$query->image);
            if ($query->image != null) {
                return '<img src="' . $url . '" border="0" width="40" class="img-rounded" align="center" style="width:140px" />';
            }else{
                return '<img src="'.asset('/').'adminlte/user.jpg" class="profile-user-img img-responsive img-circle" style="width:140px" alt="User Image">';
            }
        })
       
 
         
          ->addColumn('delete', 'admin.english.content.btn.delete')
 
         ->rawColumns([
             'edit',
             'show',
             'delete',
             'image',
             
         ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return EnglishDescription::query()->with('list');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */



    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }


    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters([
            'dom' => 'Blfrtip',
            'lengthMenu' => [
                [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
            ],
            'buttons' => [
                [
                    'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add_new_content'),
                    'className' => 'btn btn-primary create',
                    'action' => 'function( e, dt, button, config){
                         
                         window.location.href = "'.\URL::current().'/create";
                     }',

                     
                ],
                // ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
            ],
            "language" =>  self::lang(),

        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
        
            ['name'=>'title','data'=>'title','title'=>trans('admin.title')],
            ['name'=>'content','data'=>'content','title'=>trans('admin.content')],
            ['name'=>'word','data'=>'word','title'=>trans('admin.word')],
            ['name'=>'maeaning','data'=>'maeaning','title'=>trans('admin.maeaning')],
            // ['name'=>'list','data'=>'list','title'=>trans('admin.list')],
           
            // ['name'=>'show','data'=>'show','title'=>trans('admin.show'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'show','data'=>'show','title'=>trans('admin.show'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'EnglishDescription_' . date('YmdHis');
    }
}
