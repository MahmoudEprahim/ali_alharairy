<?php

namespace App\DataTables;

use App\bus;
use App\Enums\ClassbusType;
use App\Enums\GearboxType;
use App\Enums\TypeType;
use App\Enums\WorkType;
use App\User;
use Yajra\DataTables\Services\DataTable;

class busDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('edit', function ($query) {
                return '<a href="bus/'.$query->id.'/edit" class="btn btn-success edit"><i class="fa fa-edit"></i>' . trans('admin.edit') . '</a>';
            })
            ->addColumn('typecar', function ($query) {
                return session_lang($query->cartype['name_en'],$query->cartype['name_ar']);
            })
            ->addColumn('stylecar', function ($query) {
                return session_lang($query->stylecar['name_en'],$query->stylecar['name_ar']);
            })
            ->addColumn('ownercar', function ($query) {
                return session_lang($query->ownercar['name_en'],$query->ownercar['name_ar']);
            })
            ->addColumn('gearbox', function ($query) {
                return GearboxType::getDescription($query->gearbox);
            })
            ->addColumn('buscolor', function ($query) {
                return '<input type="color" value="'.$query->buscolor.'" disabled>';
            })
            ->addColumn('classcar', function ($query) {
                return ClassbusType::getDescription($query->classcar);
            })
            ->addColumn('type', function ($query) {
                return WorkType::getDescription($query->type);
            })
//            ->addColumn('image', function ($query) {
//                $url= asset('storage/'.$query->image);
//                if ($query->image != null) {
//                    return '<img src="' . $url . '" border="0" width="40" class="img-rounded" align="center" />';
//                }else{
//                    return '<img src="'.asset('/').'adminlte/Bus.png" class="profile-user-img img-responsive img-circle" alt="User Image">';
//                }
//            })
            ->addColumn('delete', 'admin.bus.btn.delete')
            ->rawColumns([
                'edit',
                'delete',
                'buscolor',
                'image',
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return bus::query()->orderByDesc('id');
    }
    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfo"=> trans('admin.sInfo'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'dom' => 'Blfrtip',
                        'lengthMenu' => [
                            [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                        ],
                        'buttons' => [
                            [
                                'text' => '<i class="fa fa-plus"></i> ' . trans('admin.Add_New_bus'),
                                'className' => 'btn btn-primary create',
                                'action' => 'function( e, dt, button, config){ 
                                     window.location = "bus/create";
                                 }',
                            ],[
                                'text' => '<i class="fa fa-bus"></i> ' . trans('admin.Owner_Buses'),
                                'className' => 'btn btn-primary',
                                'action' => 'function( e, dt, button, config){ 
                                     window.location = "ownerbus";
                                 }',
                            ],[
                                'text' => '<i class="fa fa-tag"></i> ' . trans('admin.Suppliers'),
                                'className' => 'btn btn-primary',
                                'action' => 'function( e, dt, button, config){ 
                                     window.location = "suppliers";
                                 }',
                            ],[
                                'text' => '<i class="fa fa-flag"></i> ' . trans('admin.Bus_Types'),
                                'className' => 'btn btn-primary',
                                'action' => 'function( e, dt, button, config){ 
                                     window.location = "bustype";
                                 }',
                            ],
                            [
                                'text' => '<i class="fa fa-flag"></i> ' . trans('admin.Bus_Styles'),
                                'className' => 'btn btn-primary',
                                'action' => 'function( e, dt, button, config){ 
                                     window.location = "busstyle";
                                 }',
                            ],
                            [
                                'text' => '<i class="fa fa-university"></i> ' . trans('admin.Insurance'),
                                'className' => 'btn btn-primary',
                                'action' => 'function( e, dt, button, config){ 
                                     window.location = "insurances";
                                 }',
                            ],
                            ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                            ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                        ],
                        "initComplete" => "function () {
                            this.api().columns([0,1]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('keyup', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                
                                    column.search(val ? val : '', true, false).draw();
                                });
                            });
                            }",
                        "language" =>  self::lang(),

                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name'=>'busnumber','data'=>'busnumber','title'=>  trans('admin.bus_number')],
            ['name'=>'structure_num','data'=>'structure_num','title'=>trans('admin.structure_number')],
            ['name'=>'ownercar','data'=>'ownercar','title'=>trans('admin.bus_owner')],
            ['name'=>'classcar','data'=>'classcar','title'=>trans('admin.class_car')],
            ['name'=>'typecar','data'=>'typecar','title'=>trans('admin.bus_type')],
            ['name'=>'stylecar','data'=>'stylecar','title'=>trans('admin.bus_style')],
            ['name'=>'busmodel','data'=>'busmodel','title'=>trans('admin.bus_model')],
            ['name'=>'gearbox','data'=>'gearbox','title'=>trans('admin.gearbox')],
            ['name'=>'buscolor','data'=>'buscolor','title'=>trans('admin.bus_color')],
//            ['name'=>'image','data'=>'image','title'=>trans('admin.bus_image')],
            ['name'=>'type','data'=>'type','title'=>trans('admin.type')],
            ['name'=>'seats_num','data'=>'seats_num','title'=>trans('admin.seats_number')],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'bus_' . date('YmdHis');
    }
}
