<?php

namespace App\DataTables;

use App\Branches;
use App\Company;
use App\Enums\GenderType;
use App\Enums\PayType;
use App\Enums\TypeType;
//use App\subscription;
use Yajra\DataTables\Services\DataTable;

class companyDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('edit', function ($query) {
                return '<a href="companies/'.$query->id.'/edit" class="btn btn-success edit"><i class="fa fa-edit"></i> </a>';
            })

            ->addColumn('show', function ($query) {
                return '<a href="companies/'.$query->id.'" class="btn btn-info"><i class="fa fa-eye"></i> </a>';
            })

            ->addColumn('country', function ($query) {
                return session_lang($query->country['country_name_en'],$query->country['country_name_ar']);
            })
            ->addColumn('status', function ($query) {
                return $query->status == 2 ? trans('admin.active') : trans('admin.notactive') ;
            })
            ->addIndexColumn()

            ->addColumn('delete', 'admin.companies.btn.delete')

            ->rawColumns([
                'edit',
                'show',
                'country',
                'status',
                'delete',
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
//        if (auth()->guard('admin')->user()){
//            return Company::query()->orderByDesc('id');
//        }else{
//            return Company::query()->orderByDesc('id');
//        }
        return Company::query()->orderByDesc('id');
    }
    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {

        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addColumnBefore([
                'defaultContent' => '',
                'data'           => 'DT_RowIndex',
                'name'           => 'DT_RowIndex',
                'title'          => trans('admin.id'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',

            ])
            ->parameters([
                'dom' => 'Blfrtip',
                'lengthMenu' => [
                    [5,10,25,50,100,-1],[5,10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    [
                        'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add_new_company'),
                        'className' => 'btn btn-primary create',
                        'action' => 'function( e, dt, button, config){
                             window.location = "companies/create";
                         }',
                    ],
                    ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "language" =>  self::lang(),

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
//            ['name'=>'id','data'=>'id','title'=>trans('admin.id')],
            ['name'=>'name_'.session('lang'),'data'=>'name_'.session('lang'),'title'=>trans('admin.name')],
            ['name'=>'address','data'=>'address','title'=>trans('admin.addriss')],
            ['name'=>'website','data'=>'website','title'=>trans('admin.website')],
            ['name'=>'email','data'=>'email','title'=>trans('admin.email')],
            ['name'=>'phone_1','data'=>'phone_1','title'=>trans('admin.phone')],
            ['name'=>'owner_name','data'=>'owner_name','title'=>trans('admin.owner_name')],
            ['name'=>'owner_mobile','data'=>'owner_mobile','title'=>trans('admin.owner_mobile')],
            ['name'=>'country','data'=>'country','title'=>trans('admin.country')],
            ['name'=>'show','data'=>'show','title'=>trans('admin.show'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'company_' . date('YmdHis');
    }
}
