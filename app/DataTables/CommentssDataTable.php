<?php

namespace App\DataTables;

use App\Comment;
use Yajra\DataTables\Services\DataTable;

class CommentsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addColumn('edit', function ($query) {
            return '<a href="comments/'.$query->id.'/edit" class="btn btn-success edit"><i class="fa fa-edit"></i> ' . trans('admin.edit') .'</a>';
        })

            ->addColumn('show', function ($query) {
            return '<a href="comments/'.$query->id.'" class="btn btn-success"><i class="fa fa-eye"></i> </a>';
        })

//         ->addColumn('user',function($query){
//            return $query->student->name;
//        })
//         ->addColumn('post',function($query){
//            return $query->post->id;
//        })


//         ->addColumn('delete', 'admin.chat.comments.btn.delete')

        ->rawColumns([
            'edit',
            'show',
            'delete',


        ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Comment::query();
    }

    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'dom' => 'Blfrtip',
                        'lengthMenu' => [
                            [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                        ],
                        'buttons' => [
                            // [
                            //     'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add_new_comment'),
                            //     'className' => 'btn btn-primary create',
                            //     'action' => 'function( e, dt, button, config){
                            //         window.location.href = "'.\URL::current().'/create";
                            // }',
                            // ],
                            ['name'=>'updated_at','data'=>'updated_at','title'=>trans('admin.updated_at')],
                            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
                            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],

                        ],


                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['name'=>'id','data'=>'id','title'=>trans('admin.id')],
//            ['name'=>'body','data'=>'body','title'=>trans('admin.comment_body')],
//            ['name'=>'image','data'=>'image','title'=>trans('admin.image')],
//            ['name'=>'user','data'=>'user','title'=>trans('admin.user')],
//            ['name'=>'post','data'=>'post','title'=>trans('admin.post')],
            ['name'=>'show','data'=>'show','title'=>trans('admin.show'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],

        ];
    }

    protected function filename()
    {
        return 'Comments_' . date('YmdHis');
    }
}
