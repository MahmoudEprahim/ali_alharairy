<?php

namespace App\DataTables;
use App\Enums\GenderType;
use App\Student;
use App\Enums\HealthType;
use App\Enums\LicenseType;
use App\Enums\StatusType;
use App\Enums\WorkType;
use Yajra\DataTables\Services\DataTable;

class StudentsDataTable extends DataTable
{
    
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('edit', function ($query) {
                return '<a href="students/'.$query->id.'/edit" class="btn btn-success edit"><i class="fa fa-edit"></i> '  . '</a>';
            })
            ->addColumn('student_active', function ($query) {
                $query->active == 1 ? $class='success': $class='danger';
                $query->active == 1 ? $icon='check': $icon='close';
                return '<a data-url="'.route('studentActive').'" data-id="'.$query->id.'" class="btn btn-'.$class.' student_active_link link_'.$query->id.'"><i class="fa fa-'.$icon.'"></i></a>';
            })

            ->addColumn('delete', 'admin.students.btn.delete')
            // ->addIndexColumn()

        ->addColumn('gender',  function ($query) {
        return GenderType::getDescription($query->gender);
    }       )
            ->rawColumns([
                'edit',
                'delete',
                'branches',
                'show',
                'student_active',
            ]);
    }

  
    public function query()
    {
        return Student::whereNotNull('name_en')->orderByDesc('acc_no');
        
    }
    
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            // ->addColumnBefore([
            //     'defaultContent' => '',
            //     'data'           => 'DT_RowIndex',
            //     'name'           => 'DT_RowIndex',
            //     'title'          => trans('admin.id'),
            //     'render'         => null,
            //     'orderable'      => false,
            //     'searchable'     => false,
            //     'exportable'     => false,
            //     'printable'      => true,
            //     'footer'         => '',
            // ])
            ->parameters([
                'dom' => 'Blfrtip',
                'lengthMenu' => [
                    [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    [
                        'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add_new_students'),
                        'className' => 'btn btn-primary create',
                        'action' => 'function( e, dt, button, config){ 
                                             window.location = "students/create";
                                         }',
                    ], 
                    // ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "initComplete" => "function () {
                                    this.api().columns([0,1,2,3]).every(function () {
                                        var column = this;
                                        var input = document.createElement(\"input\");
                                        $(input).appendTo($(column.footer()).empty())
                                        .on('keyup', function () {
                                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        
                                            column.search(val ? val : '', true, false).draw();
                                        });
                                    });
                                    }"
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name'=>'acc_no','data'=>'acc_no','title'=>trans('admin.acc_no')],
            ['name'=>'email','data'=>'email','title'=>trans('admin.email')],
            ['name'=>'name_en','data'=>'name_en','title'=>trans('admin.name')],
            ['name'=>'phone','data'=>'phone','title'=>trans('admin.phone')],
            ['name'=>'active','data'=>'student_active','title'=>trans('admin.student_active')],            
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],

        ];
    }

    protected function filename()
    {
        return 'student_' . date('YmdHis');
    }
}
