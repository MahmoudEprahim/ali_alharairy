<?php

namespace App\DataTables;

use App\book;
use App\Branches;
use App\Enums\ScheduleType;
use App\Enums\TypeType;
use App\User;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;

class BookDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('subscriper', function ($query) {
                return $query->subscriper['name_'.session('lang')];
            })
            ->addColumn('type', function ($query) {
                return TypeType::getDescription($query->subscriper['type']);
            })
            ->addColumn('branche', function ($query) {
                return session_lang(Branches::where('id','=',$query->branche_id)->first()['name_en'],Branches::where('id','=',$query->branche_id)->first()['name_ar']);
            })
            ->addColumn('transport', function ($query) {
                return getbus($query->transport['bus_id'])->busnumber . '  <div class="badge">'.date('h:i A', strtotime(getschedule(getschedule($query->transport['schedule_id'])->schedule_time))).'</div>  '.'  <div class="badge">'.ScheduleType::getDescription(getschedule($query->transport['schedule_id'])->type).'</div>  ';
            })
            ->addColumn('depart', function ($query) {
                return session_lang(state($query->transport['depart_id'])->state_name_en,state($query->transport['depart_id'])->state_name_ar);
            })
            ->addColumn('desname', function ($query) {
                return session_lang(state($query->transport['desname_id'])->state_name_en,state($query->transport['desname_id'])->state_name_ar);
            })
            ->addColumn('delete', 'admin.book.btn.delete')
            ->rawColumns([
                'delete',
                'transport',
                'branche',
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Branches $branches)
    {
        if (auth()->guard('admin')->user()->branches_id == $branches->first()->id) {
            return book::query()->orderByDesc('id')->with('subscriper', 'transport')->where('date','>=',Carbon::now()->format('Y-m-d'));
        }else{
            return book::query()->orderByDesc('id')->with('subscriper', 'transport')->where('branche_id','=',auth()->guard('admin')->user()->branches_id)->where('date','>=',Carbon::now()->format('Y-m-d'));
        }
    }
    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfo"=> trans('admin.sInfo'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'dom' => 'Blfrtip',
                        'lengthMenu' => [
                            [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                        ],
                        'buttons' => [
                            [
                                'text' => '<i class="fa fa-plus"></i> ' . trans('admin.Add_New_student_Schedule'),
                                'className' => 'btn btn-primary create',
                                'action' => 'function( e, dt, button, config){ 
                                             window.location = "substudent/create";
                                         }',
                            ],
                            [
                                'text' => '<i class="fa fa-plus"></i> ' . trans('admin.Add_New_Subscribers_Schedule'),
                                'className' => 'btn btn-primary create',
                                'action' => 'function( e, dt, button, config){ 
                                             window.location = "subbook/create";
                                         }',
                            ],[
                                'text' => '<i class="fa fa-users"></i> ' . trans('admin.Subscribers'),
                                'className' => 'btn btn-primary',
                                'action' => 'function( e, dt, button, config){ 
                                             window.location = "subscribers";
                                         }',
                            ],[
                                'text' => '<i class="fa fa-calendar"></i> ' . trans('admin.Bus_Schedule'),
                                'className' => 'btn btn-primary',
                                'action' => 'function( e, dt, button, config){ 
                                             window.location = "busschedule";
                                         }',
                            ],
                            ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                            ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                        ],
                        "initComplete" => "function () {
                                    this.api().columns([0,1,2]).every(function () {
                                        var column = this;
                                        var input = document.createElement(\"input\");
                                        $(input).appendTo($(column.footer()).empty())
                                        .on('keyup', function () {
                                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        
                                            column.search(val ? val : '', true, false).draw();
                                        });
                                    });
                                    }",
                        "language" =>  self::lang(),

                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name'=>'id','data'=>'id','title'=>trans('admin.id')],
            ['name'=>'branche','data'=>'branche','title'=>trans('admin.branche')],
            ['name'=>'date','data'=>'date','title'=>trans('admin.date')],
            ['name'=>'subscriper','data'=>'subscriper','title'=>trans('admin.subscriper')],
            ['name'=>'transport','data'=>'transport','title'=>trans('admin.transport')],
            ['name'=>'type','data'=>'type','title'=>trans('admin.typesub')],
            ['name'=>'depart','data'=>'depart','title'=>trans('admin.depart')],
            ['name'=>'desname','data'=>'desname','title'=>trans('admin.desname')],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Book_' . date('YmdHis');
    }
}
