<?php

namespace App\DataTables;

use App\Sayabout;
use Yajra\DataTables\Services\DataTable;

class SayaboutDataTabe extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        // ->addColumn('edit', function ($query) {
        //     return '<a href="'.\URL::current().'/'.$query->id.'/update" class="btn btn-info edit"><i class="fa fa-info"></i></a>';
        // })
        ->addColumn('activate', function ($query) {
            return '<a href="'.\URL::current().'/'.$query->id.'/activate" class="btn btn-info edit"><i class="fa fa-info"></i></a>';
        })
        
        ->addColumn('delete', 'admin.sayabout.btn.delete')
        ->rawColumns([
            'delete',
            'activate',
        ]);
    }

   
    public function query()
    {
        return Sayabout::query()->orderByDesc('id');
    }

    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }
    
    public function html()
    {

        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addColumnBefore([
                'defaultContent' => '',
                'data'           => 'DT_RowIndex',
                'name'           => 'DT_RowIndex',
                'title'          => trans('admin.id'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',

            ])
            ->parameters([
                'dom' => 'Blfrtip',
                'lengthMenu' => [
                    [5,10,25,50,100,-1],[5,10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    // [
                    //     'text' => '<i class="fa fa-plus"></i> ' . trans('admin.create_user_profile'),
                    //     'className' => 'btn btn-primary create',
                    //     'action' => 'function( e, dt, button, config){
                    //          window.location = "'.\URL::current().'/create";
                             
                    //      }',
                    // ],
                    // ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    // ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "language" =>  self::lang(),

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            
           
            ['name'=>'name','data'=>'name','title'=>trans('admin.name')],
            ['name'=>'graduation_year','data'=>'graduation_year','title'=>trans('admin.graduation_year')],
            ['name'=>'current_work','data'=>'current_work','title'=>trans('admin.current_work')],
            ['name'=>'email','data'=>'email','title'=>trans('admin.email')],
            ['name'=>'comment','data'=>'comment','title'=>trans('admin.comment')],
            ['name'=>'isactive','data'=>'isactive','title'=>trans('admin.isactive')],
            
            // ['name'=>'edit','data'=>'edit','title'=>trans('admin.activate'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'activate','data'=>'activate','title'=>trans('admin.activate'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],

        ];
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'LibraryVideosDataTabe_' . date('YmdHis');
    }
}
