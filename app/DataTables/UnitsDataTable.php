<?php

namespace App\DataTables;

use App\Unit;
use Yajra\DataTables\Services\DataTable;

class UnitsDataTable extends DataTable
{
    
    public function dataTable($query)
    {

        return datatables($query)
        ->addColumn('edit', function ($query) {
            return '<a href="units/'.$query->id.'/edit" class="btn btn-success edit"><i class="fa fa-edit"></i></a>';
        })
        ->addColumn('show', function ($query) {
            return '<a href="units/'.$query->id.'" class="btn btn-info"><i class="fa fa-info"></i></a>';
            //return '<a href="'.route('units.show',8) .'"class="btn btn-success "><i class="fa fa-show"></i> ' . trans('admin.show') .'</a>';
        })
    //     ->addColumn('branches_id',function($query){   
    //        return session_lang($query->branche->name_en,$query->branche->name_ar);
    //    })
    //   ->addColumn('branches_id',function($query){
    //         return session_lang($query->Branches->name_en,$query->Branches->name_ar);
    //     }) 
            ->addColumn('grade_id',function($query){
                return session_lang($query->grade->name_en,$query->grade->name_ar);
      })

         ->addColumn('delete', 'admin.secondary.units.btn.delete')
         ->addIndexColumn()

        ->rawColumns([
            'edit',
            'show',
            'delete',
            'grade_id',
            
        ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Unit::query();
    }

    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }

    public function html()
    {
        return $this->builder()
            ->addIndex()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addColumnBefore([
                'defaultContent' => '',
                'data'           => 'DT_RowIndex',
                'name'           => 'DT_RowIndex',
                'title'          => trans('admin.id'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
 
            ])
            ->parameters([
                'dom' => 'Blfrtip',
                "columnDefs" => [
                    ["width" => "2%", "targets" =>  0 ]
                ],
                'lengthMenu' => [
                    [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    [
                        'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add_units'),
                        'className' => 'btn btn-primary create',
                        'action' => 'function( e, dt, button, config){ 
                                             window.location = "units/create";
                                         }',
                    ],
                    ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "initComplete" => "function () {
                            this.api().columns([]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('keyup', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                
                                    column.search(val ? val : '', true, false).draw();
                                });
                            });
                            }",
                "language" =>  self::lang(),


            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            
            ['name'=>'name_en','data'=>'name_en','title'=>trans('admin.name')],
            ['name'=>'grade_id','data'=>'grade_id','title'=>trans('admin.en_grade_id')],
            // ['name'=>'name_'.session('lang'),'data'=>'name_'.session('lang'),'title'=>trans('admin.name')],
            // ['name'=>'branches_id','data'=>'branches_id','title'=>trans('admin.branches_id')],
            // ['name'=>'branches_id','data'=>'branches_id','title'=>trans('admin.branches_id')],

            ['name'=>'show','data'=>'show','title'=>trans('admin.show'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Units_' . date('YmdHis');
    }
}
