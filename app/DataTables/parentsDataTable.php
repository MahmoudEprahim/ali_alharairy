<?php

namespace App\DataTables;

use App\Admin;
use App\parents;
use App\User;
use Yajra\DataTables\Services\DataTable;

class parentsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('relation', 'admin.parents.relation')
            ->addColumn('edit', function ($query) {
                return '<a href="parents/'.$query->id.'/edit" class="btn btn-success"><i class="fa fa-edit"></i></a>';
            })
            ->addColumn('parent_active', function ($query) {
                $query->active == 1 ? $class='success': $class='danger';
                $query->active == 1 ? $icon='check': $icon='close';
                return '<a data-url="'.route('parentActive').'" data-id="'.$query->id.'" class="btn btn-'.$class.' parent_active_link link_'.$query->id.'"><i class="fa fa-'.$icon.'"></i></a>';
            }) 
            ->addColumn('details', function ($query) {
                return '<a href="parents/'.$query->id.'" class="btn btn-primary"><i class="fa fa-info"></i></a>';
            })
            ->addColumn('delete', 'admin.parents.btn.delete')
            ->addColumn('students', function (parents $parents) {
                return $parents->students->map(function($students) {
                    return $students->name_ar;
                })->implode(' ');
            })
            ->addIndexColumn()
            ->rawColumns([
                'edit',
                'students',
                'delete',
                'details',
                'parent_active',
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return parents::query();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
//                    ->parameters($this->getBuilderParameters());
            ->addColumnBefore([
                'defaultContent' => '',
                'data'           => 'DT_RowIndex',
                'name'           => 'DT_RowIndex',
                'title'          => trans('admin.id'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ])
            ->parameters([
                'dom' => 'Blfrtip',
                
                'lengthMenu' => [
                    [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    [
                        'text' => '<i class="fa fa-plus"></i> ' . trans('admin.create_parents'),
                        'className' => 'btn btn-primary create',
                        'action' => 'function( e, dt, button, config){ 
                             window.location = "parents/create";
                         }',
                    ],
                    // ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    // ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "initComplete" => "function () {
                    this.api().columns([0,1,2]).every(function () {
                        var column = this;
                        var input = document.createElement(\"input\");
                        $(input).appendTo($(column.footer()).empty())
                        .on('keyup', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
        
                            column.search(val ? val : '', true, false).draw();
                        });
                    });
                    }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name'=>'name_en','data'=>'name_en','title'=>trans('admin.name')],
            ['name'=>'email','data'=>'email','title'=>trans('admin.email')],
            ['name'=>'job','data'=>'job','title'=>trans('admin.job')],
            ['name'=>'active','data'=>'parent_active','title'=>trans('admin.parent_active')],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            // ['name'=>'name_'.session('lang'),'data'=>'name_'.session('lang'),'title'=>trans('admin.name')],
            // ['name'=>'relation','data'=>'relation','title'=>trans('admin.relation')],
            // ['name'=>'students','data'=>'students','title'=>trans('admin.students_whom_follow')],
//            ['name'=>'created_at','data'=>'created_at','title'=>trans('admin.created_at')],
            // ['name'=>'details','data'=>'details','title'=>trans('admin.details'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
           
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'parents_' . date('YmdHis');
    }
}
