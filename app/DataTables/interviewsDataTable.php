<?php

namespace App\DataTables;

use App\Applicant;
use App\applicants_company;
use App\Company;
use Yajra\DataTables\Services\DataTable;

class interviewsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('applicants', function ($query) {
                return '<a href="applicants/'.$query->applicant_id.'">' . $query->applicant_name . '</a>';
//                return '<a href="applicants/'.$query->applicants->id.'">' . $query->applicants->name_ar . '</a>';
            })
            ->addColumn('date', function ($query) {
                if ($query->date != null){
                    return date('Y-m-d',strtotime($query->date));
                }else{
                    return null;
                }
            })
            ->addColumn('hall', function ($query) {
                return  \App\Enums\HallsType::getDescription($query->hall);
            })
            ->addColumn('companies', function ($query) {
                return '<a href="companies/'.$query->company_id.'">' . $query->company_name . '</a>';
//                return '<a href="companies/'.$query->companies->id.'">' . $query->companies->name_ar . '</a>';
            })
            ->addColumn('delete', 'admin.interviews.btn.delete')
            ->rawColumns([
                'applicants',
                'companies',
                'delete',
                'hall',
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return applicants_company::query()
            ->leftJoin('applicants', 'applicants_company.applicants_id', '=', 'applicants.id')
            ->leftJoin('companies', 'applicants_company.companies_id', '=', 'companies.id')
            ->select(['applicants_company.id', 'applicants.name_ar as applicant_name', 'applicants.id as applicant_id', 'companies.id as company_id', 'companies.name_ar as company_name','applicants_company.date', 'applicants_company.schedules', 'applicants_company.hall']);
    }
    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'dom' => 'Blfrtip',
                'lengthMenu' => [
                    [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    [
                        'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add_new_interviews'),
                        'className' => 'btn btn-primary create',
                        'action' => 'function( e, dt, button, config){ 
                                     window.location = "interviews/create";
                                 }',
                    ],
                    ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "initComplete" => "function () {
                            this.api().columns([0,1]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('keyup', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                
                                    column.search(val ? val : '', true, false).draw();
                                });
                            });
                            }",
                "language" =>  self::lang(),

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
//            ['name'=>'id','data'=>'id','title'=>trans('admin.id')],
            ['name'=>'companies.name_ar','data'=>'companies','title'=>trans('admin.companies')],
            ['name'=>'applicants.name_ar','data'=>'applicants','title'=>trans('admin.applicants')],
            ['name'=>'date','data'=>'date','title'=>trans('admin.date')],
            ['name'=>'schedules','data'=>'schedules','title'=>trans('admin.Schedules')],
            ['name'=>'hall','data'=>'hall','title'=>trans('admin.hall')],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'interviews_' . date('YmdHis');
    }
}
