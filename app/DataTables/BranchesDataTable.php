<?php

namespace App\DataTables;

use App\Branches;
use Illuminate\Support\Facades\Broadcast;
use Yajra\DataTables\Services\DataTable;

class BranchesDataTable extends DataTable
{
   
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('edit', function ($query) {
                return '<a href="'.route('branches.edit', $query->id).'" class="btn btn-success edit"><i class="fa fa-edit"></i></a>';
            })
            ->addColumn('branch_active', function ($query) {
                $query->active == 1 ? $class='success': $class='danger';
                $query->active == 1 ? $icon='check': $icon='close';
                return '<a data-url="'.route('branchActive').'" data-id="'.$query->id.'" class="btn btn-'.$class.' branch_active_link link_'.$query->id.'"><i class="fa fa-'.$icon.'"></i></a>';
            }) 
            // ->addColumn('delete', 'admin.branches.btn.delete')
             ->addIndexColumn()
            ->rawColumns([
                'edit','branch_active',
            ]);
    }

    
    public function query()
    {
        return Branches::query()->orderByDesc('id');
    }

    public function html()
    {
        return $this->builder()
                    ->addIndex()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addColumnBefore([
                        'defaultContent' => '',
                        'data'           => 'DT_RowIndex',
                        'name'           => 'DT_RowIndex',
                        'title'          => trans('admin.id'),
                        'render'         => null,
                        'orderable'      => false,
                        'searchable'     => false,
                        'exportable'     => false,
                        'printable'      => true,
                        'footer'         => '',
        
                    ])
                    ->parameters([
                        'dom' => 'Blfrtip',
                        'lengthMenu' => [
                            [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                        ],
                        'buttons' => [
                            [
                                'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add_new_branches'),
                                'className' => 'btn btn-primary create',
                                'action' => 'function( e, dt, button, config){
                                                             window.location = "branches/create";
                                                         }',
                            ],
                            ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                            ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                        ],
                        "initComplete" => "function () {
                        this.api().columns([1]).every(function () {
                            var column = this;
                            var input = document.createElement(\"input\");
                            $(input).appendTo($(column.footer()).empty())
                            .on('keyup', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                        });
                        }"

                    ]);
    }

    
    protected function getColumns()
    {
        return [
            ['name'=>'name_en','data'=>'name_en','title'=>trans('admin.name')],
            ['name'=>'active','data'=>'branch_active','title'=>trans('admin.active')],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            // ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
        ];
    }

    
    protected function filename()
    {
        return 'Branches_' . date('YmdHis');
    }
}
