<?php

namespace App\DataTables;

use App\Admin;
use App\Attendance;
use App\Enums\MonthName;
use App\User;
use Yajra\DataTables\Services\DataTable;

class AttendancesDataTable extends DataTable
{
    
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('name_en', function ($query) {
                return $query->student->{'name_en'};
            })

            ->addColumn('month_id', function ($query) {
                return MonthName::getDescription($query->month_id);
            })

            ->addColumn('edit', function ($query) {
                return '<a href="'.route('attendance.edit', $query->id).'" class="btn btn-success"><i class="fa fa-edit"></i> </a>';
            })

            ->addColumn('delete', 'admin.students.attendance.btn.delete')
            ->addIndexColumn()
            ->rawColumns([
                'edit',
                'delete',
            ]);
    }

    public function query()
    {
        return Attendance::with('student');
    }

    
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()

            ->addColumnBefore([
                'defaultContent' => '',
                'data'           => 'DT_RowIndex',
                'name'           => 'DT_RowIndex',
                'title'          => trans('admin.id'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ])
            ->parameters([
                'dom' => 'Blfrtip',
                'lengthMenu' => [
                    [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    [
                        'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add_absence'),
                        'className' => 'btn btn-primary create',
                        'action' => 'function( e, dt, button, config){ 
                             window.location = "attendance/create";
                         }',
                    ],
                    // ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    // ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "initComplete" => "function () {
                    this.api().columns([1,2]).every(function () {
                        var column = this;
                        var input = document.createElement(\"input\");
                        $(input).appendTo($(column.footer()).empty())
                        .on('keyup', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
        
                            column.search(val ? val : '', true, false).draw();
                        });
                    });
                    }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
//            ['name'=>'attendances.id','data'=>'id','title'=>trans('admin.ID')],
            ['name'=>'student.name_en','data'=>'name_en','title'=>trans('admin.student')],
            ['name'=>'month_id','data'=>'month_id','title'=>trans('admin.month')],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
//            ['name'=>'detail','data'=>'detail','title'=>trans('admin.show'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admins_' . date('YmdHis');
    }
}
