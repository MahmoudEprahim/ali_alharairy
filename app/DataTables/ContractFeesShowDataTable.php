<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Services\DataTable;
use App\applicants_company;
use App\Applicant;
use App\receiptsType;

class ContractFeesShowDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            // ->addColumn('show', function ($query) {
            //     return '<a href="ContractFeesShow/'.$query->id.'" class="btn btn-info">' . trans('admin.show') . '</a>';
            // })
            ->addColumn('edit', function ($query) {
                return '<a href="ContractFeesShow/'.$query->id.'/edit" class="btn btn-success edit"><i class="fa fa-edit"></i></a>';
            })
            ->addColumn('agreement_date', function ($query) {
                // return $query->agreement_date;
                return date('Y-m-d', strtotime($query->agreement_date));
            })
            ->addColumn('branch_id', function ($query) {
                return $query->branches['name_ar'];
            })
            ->addColumn('applicant_id', function ($query) {
                return $query->applicants->name_ar;
            })
            ->addColumn('company_id', function ($query) {
                return $query->companies->name_ar;
            })
            ->addColumn('creditor', function ($query) {
                $applicantCC = Applicant::where('id',$query->applicants_id)->get(['cc_id']);
                $receptCreditor = receiptsType::where('tree_id',232)->where('operation_id',4)->where('cc_id',$applicantCC[0]->cc_id)->get(['creditor']);
                $totalCreditor = 0;
                for ($i=0; $i < count($receptCreditor); $i++) {
                    $totalCreditor += $receptCreditor[$i]->creditor;
                };
                return $totalCreditor;
            })
            ->addColumn('total', function ($query) {
                $applicantCC = Applicant::where('id',$query->applicants_id)->get(['cc_id']);
                $receptCreditor = receiptsType::where('tree_id',232)->where('operation_id',4)->where('cc_id',$applicantCC[0]->cc_id)->get(['creditor']);
                $totalCreditor = 0;
                for ($i=0; $i < count($receptCreditor); $i++) {
                    $totalCreditor += $receptCreditor[$i]->creditor;
                };
                return $query->company_fees - $totalCreditor;
            })
            ->addColumn('delete', 'admin.progress.btn.delete')
            ->rawColumns([
                'show',
                'print',
                // 'receiptsType',
                'edit',
                'delete',
            ]);
    }
    // date('Y-m-d', strtotime($help[$i]->agreement_date));
    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return applicants_company::query()->where('move_number','!=',null)->orderByDesc('agreement_date');
    }

    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'dom' => 'Blfrtip',
                'lengthMenu' => [
                    [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    [
                        'text' => '<i class="fa fa-plus"></i> ' .'اضافة وتعديل التعاقد والاتعاب',
                        'className' => 'btn btn-primary create',
                        'action' => 'function( e, dt, button, config){
                                     window.location = "/admin/contracts_fees";
                                 }',
                    ],
                    ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "initComplete" => "function () {
                            this.api().columns([0,1]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('keyup', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                    column.search(val ? val : '', true, false).draw();
                                });
                            });
                            }",
                "language" =>  self::lang(),

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['name'=>'agreement_date','data'=>'agreement_date','title'=>'التاريخ'],
            ['name'=>'move_number','data'=>'move_number','title'=>'رقم الحركة'],
            ['name'=>'branch_id','data'=>'branch_id','title'=>'الفرع'],
            ['name'=>'applicant_id','data'=>'applicant_id','title'=>'اسم المتعاقد'],
            ['name'=>'company_id','data'=>'company_id','title'=>'الشركة'],
            ['name'=>'contract_value','data'=>'contract_value','title'=>'قيمة العقد بالريال'],
            ['name'=>'company_fees','data'=>'company_fees','title'=>'اجمالى الاتعاب'],
            ['name'=>'creditor','data'=>'creditor','title'=>'سندات القبض'],
            ['name'=>'total','data'=>'total','title'=>'الباقى'],
            // ['name'=>'show','data'=>'show','title'=>trans('admin.show'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            // ['name'=>'print','data'=>'print','title'=>trans('admin.print'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ContractFeesShow_' . date('YmdHis');
    }
}
