<?php

namespace App\DataTables;

use App\Applicant;
use App\applicants_company;
use App\Company;
use App\User;
use Yajra\DataTables\Services\DataTable;

class RamadanInterviewsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('applicants', function ($query) {
                return '<a href="applicants/'.$query->id.'">' . $query->name_ar . '</a>';
            })
            ->addColumn('date', function ($query) {
                if ($query->date != null){
                    return date('Y-m-d',strtotime($query->date));
                }else{
                    return null;
                }
            })->addColumn('schedules', function ($query) {
                if ($query->schedules != null){
                    return date('g:i A', strtotime($query->schedules));
                }else{
                    return null;
                }
            })
            ->addColumn('job_specifications', function ($query) {
                return $query->name_en;
            })
            ->addColumn('meeting_status', function ($query) {
                if ($query->meeting_status == 0) {
                    return '';
                }elseif ($query-> meeting_status == 1) {
                    return 'تمت المقابله';
                }else {
                    return '';
                }
            })
            ->addColumn('delete', 'admin.interviews.ramadan.btn.delete')
            ->rawColumns([
                'applicants',
                'date',
                'job_specifications',
                'delete',
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Applicant::query()->join('job_specifications', 'applicants.job_spec_id', '=', 'job_specifications.id')->select(['applicants.id', 'applicants.name_ar', 'job_specifications.name_en', 'applicants.date', 'applicants.schedules','applicants.meeting_status']);
    }
    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'dom' => 'Blfrtip',
                'lengthMenu' => [
                    [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    [
                        'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add_new_first_interviews'),
                        'className' => 'btn btn-primary create',
                        'action' => 'function( e, dt, button, config){ 
                                     window.location = "applicant_inter/create";
                                 }',
                    ],
                    ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "initComplete" => "function () {
                            this.api().columns([0,1]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('keyup', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                
                                    column.search(val ? val : '', true, false).draw();
                                });
                            });
                            }",
                "language" =>  self::lang(),

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
//            ['name'=>'applicants.id','data'=>'id','title'=>trans('admin.id')],
            ['name'=>'applicants.name_ar','data'=>'applicants','title'=>trans('admin.applicants')],
            ['name'=>'job_specifications.name_en','data'=>'job_specifications','title'=>trans('admin.job_spec')],
            ['name'=>'applicants.date','data'=>'date','title'=>trans('admin.date')],
            ['name'=>'applicants.schedules','data'=>'schedules','title'=>trans('admin.Schedules')],
            ['name'=>'applicants.meeting_status','data'=>'meeting_status','title'=>trans('admin.meeting_status')],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'RamadanInterviews_' . date('YmdHis');
    }
}
