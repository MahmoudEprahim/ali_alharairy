<?php

namespace App\DataTables;

use App\CompanyContact;
use App\User;
use Yajra\DataTables\Services\DataTable;

class CompanyContactsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('edit', function ($query) {
                return '<a href="companyContacts/'.$query->id.'/edit" class="btn btn-success edit"><i class="fa fa-edit"></i> ' . trans('admin.edit') .'</a>';
            })

            ->addColumn('show', function ($query) {
                return '<a href="companyContacts/'.$query->id.'" class="btn btn-info"><i class="fa fa-info"></i> ' . trans('admin.information_details') . '  </a>';
            })

            ->addColumn('company', function ($query) {
                return session_lang($query->company['name_en'],$query->company['name_ar']);
            })

            ->addColumn('delete', 'admin.companies.contacts.btn.delete')

            ->rawColumns([
                'edit',
                'show',
                'delete',
                'company',
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return CompanyContact::query()->orderByDesc('id')->with('company');
    }

    public static function lang(){
        $langJson = [
            "Processing"=> trans('admin.sProcessing'),
            "ZeroRecords"=> trans('admin.sZeroRecords'),
            "EmptyTable"=> trans('admin.sEmptyTable'),
            "InfoFiltered"=> trans('admin.sInfoFiltered'),
            "Search"=> trans('admin.sSearch'),
            "Url"=> trans('admin.sUrl'),
            "InfoThousands"=> trans('admin.sInfoThousands'),
            "LoadingRecords"=> trans('admin.sLoadingRecords'),
            "Paginate"=> [
                "First"=> trans('admin.sFirst'),
                "Last"=> trans('admin.sLast'),
                "Next"=> trans('admin.sNext'),
                "Previous"=> trans('admin.sPrevious')
            ],
            "Aria"=> [
                "SortAscending"=> trans('admin.sSortAscending'),
                "SortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'dom' => 'Blfrtip',
                'lengthMenu' => [
                    [5,10,25,50,100,-1],[5,10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    [
                        'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add_new_company_contact'),
                        'className' => 'btn btn-primary create',
                        'action' => 'function( e, dt, button, config){
                             window.location = "companyContacts/create";
                         }',
                    ],
                    ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "language" =>  self::lang(),

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name'=>'id','data'=>'id','title'=>trans('admin.id')],
            ['name'=>'name_'.session('lang'),'data'=>'name_'.session('lang'),'title'=>trans('admin.name')],
            ['name'=>'job_desc','data'=>'job_desc','title'=>trans('admin.job_desc')],
            ['name'=>'phone','data'=>'phone','title'=>trans('admin.phone')],
            ['name'=>'email','data'=>'email','title'=>trans('admin.email')],
            ['name'=>'company.name_en','data'=>'company','title'=>trans('admin.company')],
            ['name'=>'show','data'=>'show','title'=>trans('admin.show'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'CompanyContacts_' . date('YmdHis');
    }
}
