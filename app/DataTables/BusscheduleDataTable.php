<?php

namespace App\DataTables;

use App\Branches;
use App\bus;
use App\Enums\BusStatusType;
use App\Enums\ScheduleType;
use App\transport;
use App\User;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;

class BusscheduleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('edit', function ($query) {
//                if($query->status == 1){
                    return '<a href="busschedule/'.$query->id.'/edit" class="btn btn-success"><i class="fa fa-edit"></i> ' . trans('admin.edit') .'</a>';
//                }else{
//                    return trans('admin.you_cannot').BusStatusType::getDescription($query->status);
//                }
            })
            ->addColumn('branche', function ($query) {
                return session_lang(Branches::where('id','=',$query->branche_id)->first()['name_en'],Branches::where('id','=',$query->branche_id)->first()['name_ar']);
            })
            ->addColumn('schedule', function ($query) {
                return '<div class="badge">'.date('h:i A', strtotime($query->schedule['schedule_time'])).'</div>';
            })
            ->addColumn('type', function ($query) {
                return ScheduleType::getDescription($query->schedule['type']);
            })
            ->addColumn('status', function ($query) {
                return BusStatusType::getDescription($query->status);
            })
            ->addColumn('drivers', function ($query) {
                return $query->drivers['name_'.session('lang')];
            })
            ->addColumn('desname', function ($query) {
                return $query->desnames['state_name_'.session('lang')];
            })
            ->addColumn('depart', function ($query) {
                return $query->departs['state_name_'.session('lang')];
            })
            ->addColumn('bus', function ($query) {
                return $query->bus['busnumber'];
            })
            ->addColumn('delete', 'admin.busschedule.btn.delete')
            ->rawColumns([
                'edit',
                'delete',
                'schedule',
                'branche',
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Branches $branches)
    {
        if (auth()->guard('admin')->user()->branches_id == $branches->first()->id) {
            return transport::query()->orderByDesc('id')->with('schedule', 'bus', 'drivers', 'departs', 'desnames')->where('date','>=',Carbon::now()->format('Y-m-d'));
        }else{
            return transport::query()->orderByDesc('id')->with('schedule', 'bus', 'drivers', 'departs', 'desnames')->where('branche_id','=',auth()->guard('admin')->user()->branches_id)->where('date','>=',Carbon::now()->format('Y-m-d'));
        }
    }
    public static function lang(){
        $langJson = [
            "sProcessing"=> trans('admin.sProcessing'),
            "sZeroRecords"=> trans('admin.sZeroRecords'),
            "sEmptyTable"=> trans('admin.sEmptyTable'),
            "sInfoFiltered"=> trans('admin.sInfoFiltered'),
            "sSearch"=> trans('admin.sSearch'),
            "sUrl"=> trans('admin.sUrl'),
            "sInfoThousands"=> trans('admin.sInfoThousands'),
            "sLoadingRecords"=> trans('admin.sLoadingRecords'),
            "oPaginate"=> [
                "sFirst"=> trans('admin.sFirst'),
                "sLast"=> trans('admin.sLast'),
                "sNext"=> trans('admin.sNext'),
                "sPrevious"=> trans('admin.sPrevious')
            ],
            "oAria"=> [
                "sSortAscending"=> trans('admin.sSortAscending'),
                "sSortDescending"=> trans('admin.sSortDescending')
            ]
        ];
        return $langJson;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'dom' => 'Blfrtip',
                'lengthMenu' => [
                    [10,25,50,100,-1],[10,25,50,trans('admin.all_record')]
                ],
                'buttons' => [
                    [
                        'text' => '<i class="fa fa-plus"></i> ' . trans('admin.Add_New_Bus_Schedule'),
                        'className' => 'btn btn-primary create',
                        'action' => 'function( e, dt, button, config){ 
                                             window.location = "busschedule/create";
                                         }',
                    ],
                    ['extend' => 'print','className' => 'btn btn-primary' , 'text' => '<i class="fa fa-print"></i>'],
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => '<i class="fa fa-file"></i> ' . trans('admin.EXCEL')],
                    ['extend' => 'reload','className' => 'btn btn-info' , 'text' => '<i class="fa fa-refresh"></i>']
                ],
                "initComplete" => "function () {
                                    this.api().columns([0,1,2]).every(function () {
                                        var column = this;
                                        var input = document.createElement(\"input\");
                                        $(input).appendTo($(column.footer()).empty())
                                        .on('keyup', function () {
                                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        
                                            column.search(val ? val : '', true, false).draw();
                                        });
                                    });
                                    }",
                "language" =>  self::lang(),

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name'=>'branche','data'=>'branche','title'=>trans('admin.branche')],
            ['name'=>'date','data'=>'date','title'=>trans('admin.date')],
            ['name'=>'bus','data'=>'bus','title'=>trans('admin.Bus')],
            ['name'=>'drivers','data'=>'drivers','title'=>trans('admin.driver')],
            ['name'=>'schedule','data'=>'schedule','title'=>trans('admin.Schedule')],
            ['name'=>'type','data'=>'type','title'=>trans('admin.type')],
            ['name'=>'seats','data'=>'seats','title'=>trans('admin.seats')],
            ['name'=>'status','data'=>'status','title'=>trans('admin.status')],
            ['name'=>'depart','data'=>'depart','title'=>trans('admin.depart')],
            ['name'=>'desname','data'=>'desname','title'=>trans('admin.desname')],
            ['name'=>'edit','data'=>'edit','title'=>trans('admin.edit'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
            ['name'=>'delete','data'=>'delete','title'=>trans('admin.delete'),'printable'=>false,'exportable'=>false,'orderable'=>false,'searchable'=>false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Busschedule_' . date('YmdHis');
    }
}
