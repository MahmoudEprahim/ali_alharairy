<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class wordlist extends Model
{
    //
    protected $table = 'wordlists';

    protected $fillable = [
        'name_ar',
        'name_en',
        'units_id',
        'branches_id',
        'grade_id',
        'type',
    ];



    public function grade()
    {
        return $this->belongsTo('App\Grade');
    }

    public function branche()
    {
        return $this->belongsTo('App\Branches');
    }


    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function wordlist(){
        return $this->hasMany('App\wordlist');
    }
}
