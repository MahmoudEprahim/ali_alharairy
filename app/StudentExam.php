<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentExam extends Model
{
    protected $table = 'studentexams';
    protected $fillable = [
        
        'grade',
        'exam_id',
        'student_id',

    ];


    public function student()
    {
        return $this->belongsToMany('App\Student');
    }

    public function exam()
    {
        return $this->belongsToMany('App\Exam');
    }
}
