<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Student extends Authenticatable
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'students';
    protected $guard_name = 'student';
    protected $fillable = [
        'active',
        'acc_no',
        'branches_id',
        'name_ar',
        'name_en',
        'school',
        'grades_id',
        'start_register',
        'appointments_id',
        'parents_phone',
        'phone',
        'email',
        'password',
        'image',
        'addriss',
        'status',
        'class_info',
        'bus',
        'per_status',
        'enrollment_status',
        'phone',
        'age',
        'gender',
        'birthdate',
        'birthplace',
        'receivables_past_id',
        'receivables_Present_id',
        'receivables_future_id',
        'activity_revenues_id',
        'learn_time',
        'debtor',
        'creditor',
        'health_status',
        'previous_estimate',
        'past_school',
        'Joining Date',
        'appointments_id',
        'parent_id',
        'admin_id',
        'operation_id',
        'tree_id',
        'bus_id',
        'driver_id',
        'countries_id',
        'class_id',
        'classroom_id',
        'islamic_date',
        'islamic_birthdate',
        'religion',
        'employees_sons',
        'education_notes',
        'special_needs',
        'medical_condition',
        'medical_attention',
        'medical_notes',
        'medical_desc',
        'state_id',
        'city_id',
        'street',
        'number',
        'house_num',
        'compound_num',
        'compound_name',
        'phone_1',
        'phone_2',
        'near',
        'image_accommodation',
        'image_passport',
        'image_birth_certificate',
        'family_id_number',
        'certificate_previous_year',
        'Health_card',
        'image_1',
        'image_2',
        'teacher_notes',
                'type',
    ];
    protected $hidden = ['password', 'remember_token'];

    protected $guard = 'student';


    public function parent(){
        return $this->belongsTo('App\parents','parent_id','id');
    }
    public function appointment(){
        return $this->belongsTo('App\Appointment','appointments_id','id');
    }

    public function grade(){
        return $this->hasOne('App\Grade','id','grades_id');
    }
    public function branches(){
        return $this->hasOne('App\Branches','id','branches_id');
    }
    public function parents(){
        return $this->belongsTo('App\parents','parent_id','id');
    }


    public function student()
    {
        return $this->hasMany('App\Student','id','relatives_id');
    }
    public function classinfo()
    {
        return $this->belongsToMany('App\classinfo','sub_classinfo','student_id','class_id');
    }
    public function other_students(){
        return $this->belongsToMany('App\Student','relatives','parent_id','student_id');
    }


    public function exams()
    {
        return $this->belongsToMany('App\Exam', 'studentexams', 'student_id', 'exam_id')->withPivot(["grade"]);
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }


    // honr board
    public function honrboard()
    {
        return $this->hasMany('App\HonorBoard');
    }

    public function attendances()
    {
        return $this->hasMany(Attendance::class);
    }

}
