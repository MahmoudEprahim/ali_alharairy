<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryBook extends Model
{
    protected $table = 'librarybooks';
    protected $fillable = [
        'media',
        'image',
        'name_ar',
        'name_en',
        'desc_ar',
        'desc_en',
        'author',
        'publish_date',
        'Librarytype',
        'Term',
        'booktype',
        'grade_id',
        'list_id',
        'branch_id',
    ];


    public function branch()
    {
        return $this->belongsTo('App\Branches');
    }

    public function list()
    {
        return $this->belongsTo('App\LibraryList');
    }
    public function grade()
    {
        return $this->belongsTo('App\Grade');
    }
}


