<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryImage extends Model
{

    protected $table = 'libraryimages';
    protected $fillable = [
        'image',
        'name_ar',
        'name_en',
        'desc_ar',
        'desc_en',
        'list_id',
        'branch_id',
    ];


    public function branch()
    {
        return $this->belongsTo('App\Branches');
    }
    public function list()
    {
        return $this->belongsTo('App\LibraryList');
    }


}
