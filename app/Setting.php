<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $fillable = [
        'sitename_ar',
        'sitename_en',
        'email',
        'main_lang',
        'title_about_center_ar',
        'about_center_en',
        'features_title_ar',
        'features_title_en',
        'icon_1',
        'icon_2',
        'icon_3',
        'addriss',
        'phone',
        'phone_2',
        'phone_3',
        'phone_4',
        'facebook',
        'twitter',
        'google',
        'youtube',
        'work_from',
        'work_to',
        'keyword',
        'status',
        'logo_ar',
        'logo_en',

    ];
}
