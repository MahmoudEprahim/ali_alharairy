<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branches extends Model
{
    protected $table ='branches';
    protected $fillable =[
        'name_ar',
        'name_en',
        'address',
        'type',
        'mini_charge',
        'location',
    ];


    public function students(){
        return $this->hasMany('App\Students');
    }

    public function customer(){
        return $this->hasMany('App\Customer');
    }

     public function wordlsts(){
        return $this->hasMany('App\wordlist','branches_id' , 'id');
    }

    public function grammer(){
        return $this->hasMany('App\EnglishGrammer');
    }
    public function Listening(){
        return $this->hasMany('App\EnglishListening');
    }
    public function reading(){
        return $this->hasMany('App\EnglishReading');
    }
    public function words(){
        return $this->hasMany('App\EnglishWords');
    }
    public function librarybooks(){
        return $this->hasMany('App\LibraryBook');
    }
    public function appointments(){
        return $this->hasMany('App\Appointment');
    }
    public function exams(){
        return $this->hasMany('App\Exam');
    }

    public function units()
    {
        return $this->hasMany('App\Unit');
    }

}
