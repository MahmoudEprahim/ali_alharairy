<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = 'features';
    protected $fillable = [
        'features_title_ar',
        'features_title_en',
        'main_image',
        'image_1',
        'image_2',
        'image_3',
        'image_4',
        'title_1_ar',
        'title_1_en',
        'title_2_ar',
        'title_2_en',
        'title_3_ar',
        'title_3_en',
        'title_4_ar',
        'title_4_en',
    ];
}
