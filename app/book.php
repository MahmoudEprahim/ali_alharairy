<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class book extends Model
{
    protected $table = 'book';
    protected $fillable =[
        'date',
        'subscriper_id',
        'transport_id',
        'status',
        'cancelled',
        'branche_id',
    ];
    public function transport(){
        return $this->belongsTo('App\transport','transport_id','id');
    }
    public function subscriper(){
        return $this->belongsTo('App\subscription','subscriper_id','id');
    }
    public function drivers()
    {
        return $this->hasManyThrough('App\drivers', 'App\transport');
    }
}
