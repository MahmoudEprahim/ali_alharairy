<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnglishListening extends Model
{
    protected $table = 'englishlistening';

    protected $fillable =[
        'branch_id',
        'list_id',
        'desc',
        'desc_ar',
        'media',
        'media_name',
        'EnglishGate',    
    ];

    public function list()
    {
        return $this->belongsTo('App\EnglishList'); 
    }
    public function branch()
    {
        return $this->belongsTo('App\Branches');
    }

    
}