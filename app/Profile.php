<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Datebase\Eloquent\SoftDeletes;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = [
        'name_ar',
        'name_en',
        'title_ar',
        'title_en',
        'profile_desc_ar',
        'profile_desc_en',
        'date_first_stage',
        'first_stage_title',
        'first_stage',
        'date_second_stage',
        'second_stage',
        'second_stage_title',
        'date_third_stage',
        'third_stage_title',
        'third_stage',
        'date_fourth_stage',
        'fourth_stage_title',
        'fourth_stage',
        'date_ex_first_stage',
        'ex_first_stage',
        'ex_first_stage_title',
        'date_ex_second_stage',
        'ex_second_stage',
        'ex_second_stage_title',
        'date_ex_third_stage',
        'ex_third_stage',
        'ex_third_stage_title',
        'date_ex_fourth_stage',
        'ex_fourth_stage',
        'ex_fourth_stage_title',
        'skill_1',
        'skill_2',
        'skill_3',
        'skill_4',
        'hobbies_1',
        'hobbies_2',
        'hobbies_3',
        'hobbies_4',
        'facebook',
        'twitter',
        'google',
        'linkedin',
        'email',
        'phone',
        'location',
        'site',
        'image',

    ];
}
