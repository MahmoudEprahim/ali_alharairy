<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    protected $fillable = [
        'fees',
        'MonthName',
        'student_id',
    ];

    public function student()
    {
        return $this->belongsTo('App\Student');
    }
}
