<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content';

    protected $fillable = [
         'title',
         'description_ar',
         'description_en',
         'media',
         'branches_id',
         'grade_id',
         'units_id',
         'type_list',
     ];
    public function grade()
    {
        return $this->belongsTo('App\Grade','grade_id', 'id');
    }

    public function Branches()
    {
        return $this->hasOne('App\Branches','id','branches_id');
    }


    public function unit()
    {
        return $this->hasOne('App\Unit','id','units_id');
    }



    public function wordlist()
    {
        return $this->hasOne('App\wordlist','id','type_list');
    }

    public function contentsMedia(){
        return $this->hasMany('App\SubContent');
    }



}
