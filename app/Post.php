<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = [
    	'title',
    	'body',
    	'image',
        'admin_id',
    ];


    public function admin(){
        return $this->hasOne('App\Admin','id','admin_id');
    }



    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }
}
