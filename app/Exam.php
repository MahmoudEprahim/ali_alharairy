<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $table = 'exams';
    protected $fillable = [
        'name_ar',
        'name_en',
        'exam_date',
        'full_grade',
        'image',
        'branche_id',
        'grade_id',

    ];


    public function branche()
    {
        return $this->belongsTo('App\Branches');
    }

    public function grade()
    {
        return $this->belongsTo('App\Grade');
    }


    public function students()
    {
        return $this->belongsToMany('App\Student');
    }
}
