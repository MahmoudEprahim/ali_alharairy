<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'units';

    protected $fillable = [
        'name_ar',
        'name_en',
        'grade_id',
        'branches_id',
    ];



    // public function branche()
    // { 
    //     return $this->belongsTo('App\Branches','id','branches_id');
    // }

    public function Branches()
    {
        return $this->hasOne('App\Branches','id','branches_id');
    }
    
    

    public function grade()
    {
        return $this->belongsTo('App\Grade','grade_id','id');
    }
    
    public function content()
    {
        return $this->hasMany('App\Content');
    }



}
