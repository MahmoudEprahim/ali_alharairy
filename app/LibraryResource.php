<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryResource extends Model
{
    protected $table = 'libraryresources';
    protected $fillable = [
        'media',
        'image',
        'name_ar',
        'name_en',
        'desc_ar',
        'desc_en',
        'author',
        'list_id',
        'branch_id',
    ];


    public function branch()
    {
        return $this->belongsTo('App\Branches');
    }

    public function list()
    {
        return $this->belongsTo('App\LibraryList');
    }
    
}


