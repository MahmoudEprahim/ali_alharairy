<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class classinfo extends Model
{
    protected $table = 'class_infos';
    protected $fillable =[
        'name_ar',
        'name_en',
        'level',
        'fees',
        'price',
    ];
}
