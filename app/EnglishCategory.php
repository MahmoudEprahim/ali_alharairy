<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnglishCategory extends Model
{
    protected $table = 'englishcategories';

    protected $fillable = [
//       'name_ar',
        'cat_name_en',
//        'content',
//        'parent_id',
        'EnglishGate',
    ];


    public function subCategories()
    {
        return $this->hasMany(SubCategory::class, 'cat_id', 'id');
    }

}
