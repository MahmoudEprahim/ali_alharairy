<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryVideo extends Model
{
    protected $table = 'libraryvideos';
    protected $fillable = [
        'image',
        'video_src',
        'name_ar',
        'name_en',
        'desc_ar',
        'desc_en',
        'branch_id',
        'list_id',
        'VideoType',
    ];

     public function branch()
    {
        return $this->belongsTo('App\Branches');
    }

    public function list()
    {
        return $this->belongsTo('App\LibraryList');
    }
}


