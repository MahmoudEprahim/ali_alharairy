<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HonorBoard extends Model
{
    protected $table = 'honorboards';

    protected $fillable = [
        'description',
        'student_id',
    ];

    public function student()
    {
        return $this->belongsTo('App\Student');
    }
}
