<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blog extends Model
{
    protected $table = 'blog_entries';
    protected $fillable =[
        'blog',
        'publish_after',
        'slug',
        'title',
        'author_name',
        'author_email',
        'author_url',
        'image',
        'content',
        'summary',
        'page_title',
        'description',
        'meta_tags',
        'display_full_content_in_feed',
    ];
    public static function boot()
    {
        parent::boot();

        static::saving(function($model) {
            $model->slug = str_slug($model->title);

            return true;
        });
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
