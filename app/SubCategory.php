<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_categories';
    protected $fillable = ['sub_cat_name_en', 'cat_id'];


    public function category()
    {
        return $this->belongsTo(EnglishCategory::class, 'id', 'cat_id');
    }

    public function subCategoryChild()
    {
        return $this->hasMany(SubCategoryChild::class, 'sub_cat_id', 'id');
    }
}
