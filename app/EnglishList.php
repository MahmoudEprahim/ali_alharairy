<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnglishList extends Model
{
    protected $table = 'englishlists';

    protected $fillable = [
       'name_ar',
        'name_en',
        'EnglishGate',
    ];

    public function Listening(){
        return $this->hasMany('App\EnglishListenin');
    }
}
