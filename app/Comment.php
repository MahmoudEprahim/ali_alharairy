<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable =
    [
    	'title',
    	'body',

        'student_id',
        'post_id',

    ];


    public function post()
    {
        return $this->belongsTo('App\Post', 'id','post_id');
    }

    public function student()
    {
        return $this->belongsTo('App\Admin','student_id','id');
    }
}
