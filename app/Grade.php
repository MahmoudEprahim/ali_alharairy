<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'grades';
    protected $fillable =[
        'name_ar',
        'name_en'
    ];

    public function units()
    {
        return $this->hasMany('App\Unit', 'grade_id', 'id');
    }
    public function exams()
    {
        return $this->hasMany('App\Exam');
    }

    public function wordlist()
    {
        return $this->hasMany('App\wordlist');
    }

    public function contents()
    {
        return $this->hasMany('App\Content', 'grade_id', 'id');
    }

    public function appointments(){
        return $this->hasMany('App\Appointment');
    }
    
}
