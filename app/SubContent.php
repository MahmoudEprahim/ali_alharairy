<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubContent extends Model
{
    protected $table = 'secondarycontent';

    protected $fillable = [
        'video_src',
        'video_desc',
        'content_id',
    ];

    public function content(){
        return $this->belongsTo('App\Content');
    }



}
