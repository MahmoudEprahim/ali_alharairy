<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $connection;
    public function __destruct(){
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);
        $str .= "\n";
        $keyPosition = strpos($str, 'DB_CONNECTION=');
        $endOfLinePosition = strpos($str, "\n", $keyPosition);
        $oldLine = substr($str, $keyPosition, $endOfLinePosition - $keyPosition);
        $str_arr = explode("=", $oldLine);
        // dd($str_arr);
        // dd(\App\database::where('id',1)->first()->name);
        $this->connection = $str_arr[1];
    }
    protected $table = 'employees';
    protected $fillable =[
        'name_ar',
        'name_en',
        'phone',
        'image',
        'address',
        'birth_date',
        'nation_id',
        'religion',
        'social_status',
        'gender',
        'salary',
        'status',
    ];
    public function nations(){
        return $this->hasOne('App\nation','id','nation_id');
    }

//    public function state(){
//        return $this->hasOne('App\state','id','state_id');
//    }
//    public function birth(){
//        return $this->hasOne('App\state','id','birth_id');
//    }

}
