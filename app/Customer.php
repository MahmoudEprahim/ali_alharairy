<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'Glcustmr';


    protected $fillable = [
        'Brn_No',
        'Cstm_No',
        'Cstm_Activ',
        'Cstm_Catg',
        'Tr_Dt',
        'Nof_Brothr',
        'Parnt_Name', 
        'Parnt_Job',
        'Cstm_NmAr',
        'Cstm_NmEn',
        'Cstm_Adr',
        'Mobile1',
        'Mobile2',
        'SchL_Name',
        'Fees',
        'Disc_Prct',
        'Disc_Val',
        'TotFees',
        'Disc_Val',
        'TotFees',
        'Sex_Ty',
        'Strt_Dt'
    ];

    public function branches(){
        
        return $this->hasOne('App\Branches','id','Brn_No');
    }
}
