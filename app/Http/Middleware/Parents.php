<?php

namespace App\Http\Middleware;

use Closure;

class Parents
{
    protected $redirectTo = '/parent/parent';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth('parent')->check()){
            return redirect()->route('parent.index');
        }
        return $next($request);
    }
}
