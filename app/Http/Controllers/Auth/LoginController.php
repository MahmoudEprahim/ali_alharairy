<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\UserResetPassword;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function forgetPassword()
    {

        return view('web.pages.forgetpassword');
    }
    public function forgetPasswordPost(Request $request)
    {



        $user = User::where('email',$request->email)->first();
        if(!empty($user)){
            $token = app('auth.password.broker')->CreateToken($user);
            DB::table('password_resets')->insert([
                'email'=>$user->email,
                'token'=>$token,
                'created_at' => Carbon::now()
            ]);
            Mail::to($user->email)->send(new UserResetPassword(['data'=>$user,'token'=>$token]));
            session()->flash('flash_message',trans('web.Success Sent check your email'));
            return back();
        }
        return back();
    }
        public function reset_password($token)
    {
     
        $csrfToken = DB::table('password_resets')->where('token',$token)->where('created_at','>',Carbon::now()->subHours(2))->first();
      
        if(!empty($csrfToken)){
            return view('web.pages.resetPassword',compact('csrfToken'));
        }else{
            return redirect('forgetPassword');
        }
    }
    public function reset_password_post($token,Request $request)
    {

        $this->validate($request,[
            'password' => 'required|confirmed',
            'password_confirmation'=>'required'
        ],[],[
            'password' => trans('web.password'),
            'password_confirmation' => trans('web.password_Confirmation')
        ]);

        $csrf_token = DB::table('password_resets')->where('token',$token)->where('created_at','>',Carbon::now()->subHours(2))->first();

        if(!empty($csrf_token)){
            $user = User::where('email' , $csrf_token->email )->update(['email'=>$csrf_token->email,'password'=>bcrypt($request->password)]);
            DB::table('password_resets')->where('email',$request->email)->delete();
            auth()->attempt(['email'=>$csrf_token->email,'password'=>$request->password]);
            return redirect('/');
        }else{
            return redirect('forget/password');
        }
    }
}
