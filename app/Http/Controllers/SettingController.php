<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Up;
class SettingController extends Controller
{
    public function index()
    {
        $settins = Setting::all();
        return view('admin.setting', compact('settins'));
    }


    public function setting_save(Request $request){

        $data = $this->validate($request,[
            'sitename_en' => 'sometimes',
            'sitename_ar' => 'sometimes',
            'logo_ar' => validate_image(),
            'logo_en' => validate_image(),
            'email' => 'sometimes',
            'main_lang' => 'sometimes',
            'title_about_center_ar' => 'sometimes',
            'title_about_center_en' => 'sometimes',
            'about_center_ar' => 'sometimes|max:1000',
            'about_center_en' => 'sometimes|max:1000',
            'features_title_ar' => 'sometimes|max:100',
            'features_title_en' => 'sometimes|max:100',
            'icon_1' => 'sometimes',
            'icon_2' => 'sometimes',
            'icon_3' => 'sometimes',
            'icon_4' => 'sometimes',
            'addriss' => 'sometimes',
            'phone' => 'sometimes',
            'phone2' => 'sometimes',
            'phone_3' => 'sometimes',
            'phone_4' => 'sometimes',
            'contact_us_title_ar' => 'sometimes',
            'contact_us_title_en' => 'sometimes',
            'facebook' => 'sometimes',
            'twitter' => 'sometimes',
            'googel' => 'sometimes',
            'linkedin' => 'sometimes',
            'youtube' => 'sometimes',
            'title_of_team_members_ar' => 'sometimes|max:100',
            'title_of_team_members_en' => 'sometimes|max:100',
            'member_name_ar' => 'sometimes',
            'member_name_en' => 'sometimes',
            'member_name_ar2' => 'sometimes',
            'member_name_en2' => 'sometimes',
            'member_name_ar3' => 'sometimes',
            'member_name_en3' => 'sometimes',
            'about_desc_ar' => 'sometimes|max:1000',
            'about_desc_en' => 'sometimes|max:1000',
            'keyword' => 'sometimes',

        ],[],[
            'logo_ar' => trans('admin.image'),
            'logo_en' => trans('admin.icon'),
            'sitename_en' => trans('admin.arabic_name'),
            'sitename_ar' => trans('admin.english_name'),
            'email' => trans('admin.email'),
            'main_lang' => trans('admin.main_lang'),
            'title_about_center_ar' => trans('admin.title_about_center_ar'),
            'title_about_center_en' => trans('admin.title_about_center_en'),
            'about_center_ar' => trans('admin.about_center_ar'),
            'about_center_en' => trans('admin.about_center_en'),
            'title_about_teacher_ar' => trans('admin.title_about_teacher_ar'),
            'title_about_teacher_en' => trans('admin.title_about_teacher_en'),
            'about_teacher_ar' => trans('admin.about_teacher_ar'),
            'about_teacher_en' => trans('admin.about_teacher_en'),
            'features_title_ar' => trans('admin.features_title_ar'),
            'features_title_en' => trans('admin.features_title_en'),
            'icon_1' => trans('admin.icon_1'),
            'icon_2' => trans('admin.icon_2'),
            'icon_3' => trans('admin.icon_3'),
            'icon_4' => trans('admin.icon_4'),
            'video_src' => trans('admin.video_src'),
            'addriss' => trans('admin.addriss'),
            'phone' => trans('admin.phone'),
            'phone_2' => trans('admin.phone'),
            'phone_3' => trans('admin.phone'),
            'phone_4' => trans('admin.phone'),
            'facebook' => trans('admin.facebook'),
            'twitter' => trans('admin.twitter'),
            'googel' => trans('admin.googel'),
            'linkedin' => trans('admin.linkedin'),
            'youtube' => trans('admin.youtube'),
            'title_of_team_members_ar' => trans('admin.title_of_team_members_ar'),
            'title_of_team_members_en' => trans('admin.title_of_team_members_en'),
            'member_name_ar' => trans('admin.member_name_ar'),
            'member_name_en' => trans('admin.member_name_en'),
            'member_name_ar2' => trans('admin.member_name_ar2'),
            'member_name_en2' => trans('admin.member_name_en2'),
            'member_name_ar3' => trans('admin.member_name_ar3'),
            'member_name_en3' => trans('admin.member_name_en3 '),
            'about_desc_ar' => trans('admin.about_desc_ar'),
            'about_desc_ar' => trans('admin.about_desc_en'),
        ]);
        // if($request->hasFile('logo_ar')){
        //     $data['logo_ar'] = Up::upload([
        //         'request' => 'logo_ar',
        //         'path'=>'setting',
        //         'upload_type' => 'single',
        //         'delete_file'=> setting()->logo_ar
        //     ]);
        // }
         $setting = Setting::first();
          Setting::orderBy('id','desc')->update($data);
                if ($request->hasFile('logo_ar')) {
            $image = $request->file('logo_ar');
       
            $filename = time() . '.' . $image->getClientOriginalExtension();
                
            $request->logo_ar->move(public_path('uploads/logo/'.$setting->id), $filename);

            $setting->logo_ar = '/uploads/logo/'. $setting->id .'/'. $filename;
            $setting->save();
        }
                        if ($request->hasFile('logo_en')) {
            $image = $request->file('logo_en');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $request->logo_en->move(public_path('uploads/logo/'.$setting->id), $filename);

            $setting->logo_en = '/uploads/logo/'. $setting->id .'/'. $filename;
         
            $setting->save();
        }

        // if($request->hasFile('logo_en')){
        //     $data['logo_en'] = Up::upload([
        //         'request' => 'logo_en',
        //         'path'=>'setting',
        //         'upload_type' => 'single',
        //         'delete_file'=> setting()->logo_en
        //     ]);
        // }

        //https://www.youtube.com/embed/PDdUeN4cuE8
        // https://www.youtube.com/watch?v=PDdUeN4cuE8
        // $request->video_src = str_replace('watch?v=', 'embed/', $request->video_src);
        // $data['video_src'] = $request->video_src;
        
      
        return redirect(aurl('setting'))->with('message',trans('admin.success_update'));
    }
}
