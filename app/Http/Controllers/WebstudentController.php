<?php

namespace App\Http\Controllers;

use App\webstudent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebstudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        return view('web.index.login');
    }

    public function getlogin()
    {
//
//        return view('web/followstudents');
        return view('web/studentsfollow/followstudents');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\webstudent  $webstudent
     * @return \Illuminate\Http\Response
     */
    public function show(webstudent $webstudent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\webstudent  $webstudent
     * @return \Illuminate\Http\Response
     */
    public function edit(webstudent $webstudent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\webstudent  $webstudent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, webstudent $webstudent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\webstudent  $webstudent
     * @return \Illuminate\Http\Response
     */
    public function destroy(webstudent $webstudent)
    {
        //
    }
}
