<?php

namespace App\Http\Controllers\web;


use App\Applicant;
use App\applicants_company;
use App\applicants_requests;
use App\blog;
use App\AboutPage;
use App\city;
use App\College;
use App\Department;
use App\Job;
use App\JobFeature;
use App\JobSpecification;
use App\Mail\VisitorMessage;
use App\nation;
use App\Setting;
use App\state;
use App\University;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use DB;

class PageController extends Controller{

    public function getIndex() {
        $blogs = blog::paginate(5);

        $applicantRequests =applicants_requests::where('job_spec_id','!=',null)->groupBy('job_spec_id')->paginate(6);


        return view('web.index.home')->with('blogs', $blogs)->with('applicantRequests', $applicantRequests);

    }

    public function jobDetails($id) {
//        dd(auth()->check());
        if (auth()->check() == TRUE){
            $applicantRequest = applicants_requests::findOrFail($id);
            $employer = Applicant::where('user_id',auth()->user()->id)->first();

            $features = JobFeature::where('applicant_request_id', $applicantRequest->id)->get();
            $company = $applicantRequest->companies()->first();
            return view('web.index.job_details',compact('applicantRequest','features','employer','company'));
        }else{
            $applicantRequest = applicants_requests::findOrFail($id);
//            $employer = Applicant::where('user_id',auth()->user()->id)->first();
            //        dd($employer);
            $features = JobFeature::where('applicant_request_id', $applicantRequest->id)->get();
            $company = $applicantRequest->companies()->first();
            return view('web.index.job_details',compact('applicantRequest','features','company'));
        }
    }
    public function alljobs() {
        $request_busseness = applicants_requests::where('job_id','!=',null)->groupBy('job_id')->get();
        $applicantRequests = DB::table('applicants_requests')
            ->select('applicants_requests.*',DB::raw('COUNT(job_id) as count'))
            ->groupBy('job_id')
            ->orderBy('count')
            ->get();



            return view('web.index.alljobs',compact('applicantRequests','request_busseness'));
        }


    public function applyToJob($id) {
//        dd(auth()->check());
        if (auth()->check() == TRUE){
            $employer = Applicant::where('user_id',auth()->user()->id)->first();
            $applicantRequest = applicants_requests::findOrFail($id);
            if ($employer){

                $applicant_id = $employer->id;
                $companies_id = $applicantRequest->companies()->first()->id;
                $applicants_requests_id = $applicantRequest->id;
                applicants_company::create([
                    'applicants_id' => $applicant_id,
                    'companies_id' => $companies_id,
                    'applicants_requests_id' => $applicants_requests_id,
                ]);
//                dd($applicant_id,$companies_id,$applicants_requests_id);
                return redirect(url('job_details/' . $applicantRequest->id))->with('employer',$employer);
            }else{

                return redirect('/profile');
            }
        }else{
            return redirect('/profile');
        }
    }

    public function contact() {
        return view('web.index.contact');
    }
    public function blog() {
        return view('web.blog.index');
    }

    public function getSingle($id) {
        // fetch from BD based on the slug
        $blog = blog::where('id', '=', $id)->first();
        // return the view
        return view('web.blog.single')->with('blog', $blog);
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws
     */

    public function postContact(Request $request) {

        $this->validate($request,[
            'name' => 'required|min:3',
            'email' => 'required|email',
            'subject' => 'required|max:255',
            'message' => 'required',
        ]);


//        $from = $request->email;
//        $message = $request->message;
//        $subject = $request->subject;
//        $name = $request->name;
//        $to = 'elmonieribrahim@gmail.com';
//        Mail::to($to)->send(new VisitorMessage($from,$message,$subject,$name));
//
//        return redirect()->route('web.index.home');





    }

}
