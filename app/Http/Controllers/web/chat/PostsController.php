<?php

namespace App\Http\Controllers\web\chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;


use App\Admin;
use App\Post;
use App\Setting;
use App\Comment;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class PostsController extends Controller
{
    public function index() 
    { 

        
        // $lastpost = DB::table('posts')->latest()->first();
        $lastpost = Post::latest()->first();
        $lastposttime = $lastpost->updated_at; //updted at time 
        $date = date('Y-m-d h:i:s'); // current time
        $diff = strtotime($date) - strtotime($lastposttime);
        $hours = date('h:i:s',$diff);
        
        // if($hours < 24){
        //     return $post->render('admin.chat.posts.index',['title'=>trans('admin.posts_information')]);
        // }else{
        //     echo 'heloo '; 
        // }
        $comment = Comment::all();
        $settings = Setting::all(); 
        return view('web.chat.index',['comment' => $comment, 'hours'=> $hours,'settings'=> $settings,'lastpost'=>$lastpost,'title'=>trans('admin.chat_home')]);
        
    } 

    public function create(Post $post)
    {
        $last = DB::table('posts')->latest()->first();
        $lastposttime = $last->created_at;
        $date = date('Y-m-d H:i:s');
        
        $diff = strtotime($date) - strtotime($lastposttime);
        
        $hours = date('H:i:s',$diff);

        $user = Admin::pluck('name','id');       
        return view('admin.chat.posts.create', ['hours' => $hours, 'title'=> trans('admin.create_new_post'),'user' => $user]);
    }

    public function store(Request $request, Post $post)
    {
        $this->validate($request,[
            'body' => 'required',
            'image' => 'sometimes',
            
            
        ],[],[
            'body'=> trans('admin.message_content'),
        ]);
            $post = new Post();
            $post->body = $request->body;
            $post->admin_id = Auth::user()->id;
            
            if($request->hasFile('image')){
                $post['image']= up::upload([
                    'request' => 'image',
                    'path' => 'posts',
                    'upload_type' => 'single',
                ]);
            }
            
            $post->save();
        
        return redirect(aurl('posts'))->with(session()->flash('message',trans('admin.success_add')));

    }


    public function show($id)
    {
        $currentadmin =  Auth::user()->id;
        $user = Admin::findOrFail($currentadmin);
        
       $post = Post::findOrFail($id);
      
        return view('admin.chat.posts.show',['user'=>$user,'post'=> $post,'title'=>trans('admin.post_information')]);
    }
    public function edit($id)
    {
        $user = Admin::pluck('name','id');
        $post = Post::findOrFail($id);
        return view('admin.chat.posts.edit',['user'=>$user,'post'=> $post,'title'=>trans('admin.edit_post')]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'body' => 'required',
            'image' => 'sometimes',
            
        ],[],[
            'body'=> trans('admin.post_content'),
        ]);
            $post = Post::findOrFail($id);
            $post->body = $request->body;
            $post->admin_id = Auth::user()->id;
            
            if($request->hasFile('image')){
                $post['image']= up::upload([
                    'request' => 'image',
                    'path' => 'posts',
                    'upload_type' => 'single',
                    'delete_file' => $post->image,
                ]);
            }
            
            $post->save();
        
        return redirect(aurl('posts'))->with(session()->flash('message',trans('admin.success_add')));


    }


    public function destroy($id)
    {
        $post= Post::findOrFail($id);
        $post->delete();
        return redirect(aurl('posts'));
    }
}
