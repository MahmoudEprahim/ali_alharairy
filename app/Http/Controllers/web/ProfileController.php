<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Profile;
use App\Slider;
use App\Setting;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Up;

class ProfileController extends Controller
{
    public function index()
    {
    	$profiles = Profile::all();
        
    	 $settings = Setting::all(); 
    	return view('web.profile')->with('settings',$settings)->with('profiles',$profiles);
    }
}
