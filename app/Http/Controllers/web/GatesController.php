<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Grade;
use App\Chapter;
use App\Listt;
use App\Content;
use App\Setting;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Up;


class GatesController extends Controller
{
    public function index()
    {
        $grades = Grade::all();
        $lists = Listt::all();
        $chapters = Chapter::all();
        $contents = Content::all();
        $settings = Setting::all();  
        
        return view('web.secondaryfirstgrade', compact('settings','grades','lists','chapters','contents'));
    }
}
