<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Up;




/*
		// create_sliders_table.php
		Schema::create(sliders, function (Blueprint $table) {
            $table->increments('id');
            $table->string(title);
            $table->string('photo');
            $table->timestamps();
		});

		// slider.php
		protected $fillable = [
   		'title', 'photo',
		];

		// route
		// Route::resource('slides', 'SliderController');

		// index.blade.php
		<a href="{{route(â€˜slides.createâ€™)}}">Add New Slide</a>
		@foreach($sliders as $slider)
		<img src="{{url('images')}}/{{$slider->photo}}" alt="{{$slider->title}}" width="250" height="150"> <br />
		@endforeach

		// create.blade.php
		{{ Form::open(array('route' => 'slides.store', 'files' => true)) }}

		{{ Form::label('title', 'Title') }}

		{{ Form::text('title', null, array('class' => 'form-control')) }}


		{{ Form::label('photo', 'Photo') }}

		{{ Form::file('photo', array('class' => 'form-control')) }}


		{{ Form::submit('Add', array('class' => 'pull-right btn btn-primary')) }}

		{{ Form::close() }}


		// SliderController 
		// add to the top of your controller
		use App\Slider;

		// index function
		public function index()
			{
				$sliders = Slider::orderby('id', 'desc')->paginate(10);
				return view('sliders.index', compact('sliders'));
			}

		// create function
		public function create()
		{
			return  view (â€˜sliders.createâ€™);
		}

		// store function
		public function store(Request $request)
			{
				$this->validate($request, array(
					'title'=>'required|max:225',
					'photo'=>'required|image',
				));
				$slider = new Slider;
				$slider->title = $request->input('title');
				if ($request->hasFile('photo')) {
					$photo = $request->file('photo');
					$filename = 'slide' . '-' . time() . '.' . $photo->getClientOriginalExtension();
					$location = public_path('images/');
					$request->file('photo')->move($location, $filename);
		
					$slider->photo = $filename;
				}
				$slider->save();
				return redirect()->route('slides.index');
			}

			public function edit($id)
			{
			$slider = Slider::findOrFail($id);
			return view('sliders.edit', compact('slider'));
			}
			public function update(Request $request, $id)
				{
				$slider = Slider::find($id);
				$this->validate($request, array(
					'title'=>'required|max:225',
					'photo'=>'required|image'
				));

				$slider = Slider::where('id',$id)->first();

				$slider->title = $request->input('title');

				if ($request->hasFile('photo')) {
					$photo = $request->file('photo');
					$filename = 'slide' . '-' . time() . '.' . $photo->getClientOriginalExtension();
					$location = public_path('images/');
					$request->file('photo')->move($location, $filename);

					$oldFilename = $slider->photo;
					$slider->photo= $filename;
					if(!empty($slider->photo)){
					Storage::delete($oldFilename);
					}
				}

				$slider->save();

				return redirect()->route('slides.index',
					$slider->id)->with('success',
					'Slider, '. $slider->title.' updated');
				}
				
				public function destroy($id)
				{
					$slider = Slider::findOrFail($id);
					Storage::delete($slider->photo);
					$slider->delete();

					return redirect()->route('slides.index')
						->with('success',
						'Slide successfully deleted');
				}
*/