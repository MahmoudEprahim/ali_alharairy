<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\HonorBoard;
use App\Branches;
use App\Grade;
use App\Term;
use App\Setting;

class HonorBoardController extends Controller
{
    public function show($id) 
    { 
        
        $settings = Setting::all(); 
        $branches = Branches::all(); 
        $grade = Grade::all(); 
        $term = Term::all(); 
        
        $honorboards = HonorBoard::findOrFail($id);
        // dd($honorboards->image);
        return view('web.honorboard',['title'=>trans('admin.HonorBoard_information'),'settings' => $settings,'honorboards'=>$honorboards]);
    } 
}