<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Profile;
use App\Slider;
use App\Setting;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Up;

class ForumController extends Controller
{
    public function index()
    {
    	
        
    	 $settings = Setting::all(); 
    	return view('web.forum')->with('settings',$settings);
    }
}
