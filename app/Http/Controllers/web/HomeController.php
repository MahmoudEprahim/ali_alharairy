<?php

namespace App\Http\Controllers\web;

use App\Appointment;
use App\EnglishCategory;
use App\SubCategoryChild;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Profile;
use App\Slider;
use App\Setting;
use App\HonorBoard;
use App\Feature;
// use App\HomeVideo;
use App\LibraryImage;
use App\LibraryList;
use App\LibraryVideo;
use App\LibraryResource;
use App\breaknew;
// high school gate
use App\Grade;
use App\Unit;
use App\Content;
use App\Sayabout;
use App\Team;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Up;

class HomeController extends Controller
{

    public function index()
    {
    	$profile = Profile::first();

    	$sliders = Slider::all();
    	 $settings = Setting::all();
		//  $honorboards = HonorBoard::all();
		$feature = Feature::first();
		// dd($feature);
		$honorboards = HonorBoard::all();
		// dd($honorboards);
		// $sayabouts = Sayabout::all();
		$sayabouts = Sayabout::where('isactive', 1)->get();
		$teams = Team::all();
		// $teams = count($teams);
		$breaknew = breaknew::first();
    	return view('web.index.home', compact('feature','honorboards','profile','sayabouts','teams','breaknew'));
	}

	// pages
	//onlinetest
	public function onlinetest()
	{
		return view('web.pages.onlinetest');
	}
	// testnow
	public function testnow()
	{
		return view('web.pages.testnow');
	}

	// pages
	public function profile()
	{
		$profile = Profile::first();
		return view('web.pages.profile',compact('profile'));
	}

	public function videogallery()
	{
		$contents = LibraryVideo::all();
		return view('web.pages.video-galerry', compact('contents'));
	}

	public function photosgallery()
	{
		// $lists = LibraryList::pluck('name_'.session('lang'),'id');
		$lists = LibraryList::all();
		// dd($lists);
		$contents = LibraryImage::all();
		return view('web.pages.photos-galerry', compact('contents','lists'));
	}

	public function references()
	{
		$contents = LibraryResource::all();
		return view('web.pages.references', compact('contents'));
	}

	public function parents_follow_up()
	{
		return view('web.pages.parents_follow_up');
	}

	public function sign_up_parents()
	{
		return view('web.parent.sign_up_parents');
	}
	public function sign_in_parents()
	{
		return view('web.pages.sign_in_parents');
	}
	public function high_school_gate()
	{
		// $grades = Grade::pluck('name_en','id');
		$grades = Grade::all();
		$units = Unit::all();
		$contents = Content::all();
        return view('web.pages.high_school_gate', compact('grades','units','contents'));
	}
	public function english_gate()
	{
		$category_1 = EnglishCategory::where('EnglishGate', 1)->get();
		$category_2 = EnglishCategory::where('EnglishGate', 2)->get();

		return view('web.pages.english_gate', compact(['category_1', 'category_2']));
	}

	public function signin()
	{
		return view('web.pages.sign-in');
	}



	public function getGradeInfo(Request $request)
    {
        if ($request->ajax() && $request->grade_id){
            $grades = Grade::find($request->grade_id);
            return view('web.pages.get_grade_info_ajax', compact(['grades']));
        }
    }

    public function getTree(Request $request)
    {
        if($request->ajax() && $request->EnglishGate){
            $category = EnglishCategory::where('EnglishGate', $request->EnglishGate)->get();

            return view('web.pages.getTree', compact(['category']));
        }
    }
    public function getTreeContent(Request $request)
    {
        if($request->ajax() && $request->id){
            $content = SubCategoryChild::where('id', $request->id)->get();
//            dd($content);
            return view('web.pages.getTreeContent', compact(['content']));
        }
    }

    public function showAppointments(Request $request)
    {
        if($request->ajax() && ($request->branch_id && $request->grade_id)){
            $appointments = Appointment::where('branch_id', $request->branch_id)->where('grade_id', $request->grade_id)->get();
            return view('admin.students.appointment.get_appointment',compact('appointments'));
        }
    }



}
