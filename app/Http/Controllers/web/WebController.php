<?php

namespace App\Http\Controllers\web;

use App\Branches;
use App\Grade;
use App\Appointment;
use App\parents;
use App\Payment;
use App\Student;
use App\Sayabout;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Up;
use DB;

class WebController extends Controller
{

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest');
    }

//    use RegistersUsers;
//    /**
//     * Where to redirect users after registration.
//     *
//     * @var string
//     */
//    protected $redirectTo = '/profile';
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('guest');
//    }

    public function index(Request $request)
    {
//        $branches=  Branches::all();
//        $grade=  Grade::all();
//        $appointments=  Appointment::all();
//        $parents = parents::all();
//        if($request->ajax()){
//            $appointments = Appointment::where('branch_id', $request->branches_id)->where('grade_id', $request->grades_id)->pluck('groups','id');
//        }
//        return view('web.pages.student-register',compact(['branches','grade','appointments', 'parents']));
    }



        public function register(Request $request)
    {


//            $data = $this->validate($request,[
//                'branches_id' => 'required',
//                'name_en' => 'required',
//                'grades_id' => 'required',
//                'school' => 'required',
//                'addriss' => 'required',
//                'phone' => 'required|unique:students',
//                'mobile_1' => 'required',
//                'start_register' => 'sometimes',
//                'appointments_id' => 'sometimes',
//                'email' => 'required|email|unique:students',
//                'password'=> 'required|confirmed',
//                'image' => 'sometimes',
//            ]
        $data = $this->validate($request,[
                'branches_id' => 'required',
                'name_en' => 'required',
                'grades_id' => 'required',
                'school' => 'sometimes',
                'addriss' => 'sometimes',
                'phone' => 'sometimes',
                'mobile_1' => 'sometimes',
                'start_register' => 'sometimes',
                'appointments_id' => 'required',
              'email' => 'required|email|unique:students',
              'email' => 'required|email|unique:users',
                'password'=> 'required|confirmed',
                'image' => 'sometimes',
                'type' => 'sometimes',
            ],[],[


                 'branches_id' => trans('admin.branche'),
                 'name_en' => trans('admin.name'),
                 'grades_id' => trans('admin.grades_id'),
                 'school' => trans('admin.school'),
                 'addriss' => trans('admin.addriss'),
                 'phone' => trans('admin.phone'),
                 'mobile_1' => trans('admin.parents_phone'),
                 'start_register' => trans('admin.start_register'),
                 'appointments_id' => trans('web.appointments_id'),
                 'email' => trans('admin.email'),
                 'image' => trans('admin.image'),
                 'password' => trans('admin.password'),
            ]);


           $data['password'] =  Hash::make($request->password);
            if($request->hasFile('image')){
                $data['image'] = Up::upload([
                    'request' => 'image',
                    'path'=>'students',
                    'upload_type' => 'single'
                ]);
            }
            $student = Student::create($data);


            $user =User::create([
                'name' => $data['name_en'],
                'type' => 1,
                'email' => $data['email'],
                'password' => Hash::make($request->password),







            ]);


            $allStudentPayment = Payment::where('student_id', $student->id)->get();

            if(count($allStudentPayment) == 0){

                for ($i=0; $i < 10 ; $i++) {
                    Payment::create([
                        'student_id' => $student->id,
                        'MonthName' => $i+1
                    ]);
                }
            }
// @dd($request->appointments_id);
            // $branches = Branches::pluck('id')->toArray();
            if ($request->appointments_id != null){
                $appointment = Appointment::findOrFail($request->appointments_id);
                $appointment->update([
                    'count' => $appointment->count+1,
                ]);
            }

        $fullName = explode(" ", $request->name_en);
        $fullName = array_slice($fullName, 1);
        $parentName = implode(' ', $fullName);
        if($parentName == ''){
                $parent = parents::create([
                    'name_en' => $request->name_en,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'mobile_1' => $request->mobile_1
                ]);
            }else{
                $parent = parents::create([
                    'name_en' => $parentName,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'mobile_1' => $request->mobile_1
                ]);
            }

            Student::find($student->id)->update(['parent_id' => $parent->id]);




            event(new Registered($user));

            $this->guard()->login($user);

            return  $this->registered($request, $user)
                ?: redirect($this->redirectPath());


            // return back()->with(session()->flash('success', trans('web.success_student_register')));
    }

    public function parentRegister(Request $request){
        $data = $this->validate($request,[
            'name_en' => 'required',
            'home_address' => 'required',
            'job' => 'required',
            'mobile_1' => 'required|integer|unique:parents',
            'parent_email' => 'required|email',
            'password' => 'required',
        ],[],[
            'name_en' => trans('admin.english_name'),
            'home_address' => trans('admin.address'),
            'job' => trans('admin.job'),
            'mobile_1' => trans('admin.phone'),
            'parent_email' =>trans('admin.email'),
            'password' =>trans('admin.password'),
        ]);

        $parent = parents::where('parent_email', $request->parent_email)->orWhere('mobile_1', $request->mobile_1)->first();
        if($parent && count($parent) > 0){
            return back()->with(session()->flash('error', 'You already have account please sign in'));
        }
        $data['password'] = Hash::make($request->password);
        parents::create($data);
        return back()->with(session()->flash('success', trans('admin.parent_create_success')));


    }

    public function success_register()
    {

        // dd(Auth()->students());
        return view('web/pages/success_register');
    }

    // sayabout
     public function sayabout_store(Request $request,Sayabout $sayabout)
     {
         $data= $this->validate($request,[
             'name'=> 'required',
             'graduation_year'=> 'required',
             'current_work'=> 'required',
             'email'=> 'required',
             'comment'=> 'required',
         ],[],[
             'name'=> trans('admin.name'),
             'graduation_year' => trans('admin.graduation_year'),
             'current_work' => trans('admin.current_work'),
             'email' => trans('admin.email'),
             'comment' => trans('admin.comment'),
         ]);
         $sayabout->create($data);
          return back()->with(session()->flash('success', trans('admin.your_comment_added_successfully_but_need_activation')));
        //  return back()->with(session()->flash('message',trans('admin.success_add')));
     }

     public function sayabout_update(Request $request, $id)
     {
         $sayabout = Sayabout::findOrFail($id);
         $sayabout->update(['isactive' => '1']);
         return back()->with(session()->flash('message',trans('admin.success_add')));
     }

//
//    public function showAppointments(Request $request)
//    {
//        if($request->ajax() && ($request->branch_id && $request->grade_id)){
//            $appointments = Appointment::where('branch_id', $request->branch_id)->where('grade_id', $request->grade_id)->get();
//            return view('admin.students.appointment.get_appointment',compact('appointments'));
//        }
//    }
}
