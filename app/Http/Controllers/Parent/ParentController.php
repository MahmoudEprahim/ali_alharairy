<?php

namespace App\Http\Controllers\Parent;

use App\parents;
use App\Student;
use App\Attendance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ParentController extends Controller
{
    
    public function index()
    {
        $parentID = auth('parent')->user()->id;

        $sons = Student::whereHas('parent', function ($query) use ($parentID){
            return $query->where('students.parent_id', $parentID);
        })->get();
        

         if($sons != null){
             $absences = \App\Attendance::where('student_id', $sons->first()->id)->get();
         }
        
        
        
        
        
        
    
        return view('web.parent.profile', compact(['sons','absences']));
    }

   
    public function getSonInfo(Request $request)
    {
        if ($request->ajax() && $request->studentId){
            $student = Student::find($request->studentId);
            return view('web.parent.getSonInfo', compact(['student']));
        }
    }
}
