<?php

namespace App\Http\Controllers\Admin\library;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\LibraryBook;
use App\Branches;
use App\LibraryList;
use App\Grade;
use App\DataTables\LibraryBooksDataTabe;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class LibraryBooksController extends Controller
{
    public function index(LibraryBooksDataTabe $librarybook) 
    { 
        return $librarybook->render('admin.library.books.index',['title'=>trans('admin.books_information')]);
    } 

    public function create(LibraryBook $librarybook)
    {   
        $branch = Branches::pluck('name_'.session('lang'),'id');
        $list = LibraryList::pluck('name_'.session('lang'),'id');
        $grade = Grade::pluck('name_'.session('lang'),'id');
        return view('admin.library.books.create', ['title'=> trans('admin.add_new_content'),'grade' => $grade,'branch'=> $branch,'list' => $list,'librarybook' => $librarybook]);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request,[
            'media' => 'sometimes',
            'image' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'desc_ar' => 'sometimes',
            'desc_en' => 'sometimes',
            'author' => 'sometimes',
            'publish_date' => 'sometimes',
            'Librarytype' => 'required',
            'Term'=> 'sometimes',
            'booktype' => 'sometimes',
            'grade_id' => 'sometimes',
            'list_id' => 'required',
            'branch_id' => 'sometimes',
        ],[],[
              
            'Librarytype' => trans('admin.Librarytype'),  
            'list_id' => trans('admin.list_id'),  
        ]);

        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'library/book',
                'upload_type' => 'single',
            ]);
        }

        $file = $request->file('media');
        if($file){
            $mediaType=explode('/',$file->getMimeType())[0];
            $filetype=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
            if( $filetype=="pdf" Or $filetype=="docx" Or $filetype == 'txt')
            {
                $data['media'] = $filePath;
                $file->move('uploads/library/books/', $filePath);
    
            }else{
                return redirect(aurl('librarybooks'))->with('error');
            }
        }
         
        $librarybook = new LibraryBook();
        $librarybook->create($data); 
        return redirect(aurl('librarybooks'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
        $librarybook = LibraryBook::findOrFail($id);
        return view('admin.library.books.show',compact('librarybook'), ['title'=> trans('admin.show_book')]);
    }

    public function edit($id)
    { 
        $branch = Branches::pluck('name_'.session('lang'),'id');
        $librarybook = LibraryBook::findOrFail($id);
        $list = LibraryList::pluck('name_'.session('lang'),'id');
        $grade = Grade::pluck('name_'.session('lang'),'id');
        return view('admin.library.books.edit',compact('librarybook','branch','list', 'grade'),['title'=>trans('admin.edit_library_book')]);
    }

    public function update(Request $request, $id)
    {

        $librarybook = LibraryBook::findOrFail($id);
        $data = $this->validate($request,[
            'media' => 'sometimes',
            'image' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'desc_ar' => 'sometimes',
            'desc_en' => 'sometimes',
            'author' => 'sometimes',
            'publish_date' => 'sometimes',
            'Librarytype' => 'required',
            'Term'=> 'sometimes',
            'booktype' => 'sometimes',
            'grade_id' => 'sometimes',
            'list_id' => 'required',
            'branch_id' => 'sometimes',
        ],[],[     
            
        ]);

        
        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'library/book',
                'upload_type' => 'single',
                'delete_file' => $librarybook->image,
            ]);
        }

        $file = $request->file('media');
        if($file){
            $mediaType=explode('/',$file->getMimeType())[0];
            $filetype=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
            if( $filetype=="pdf" Or $filetype=="docx" Or $filetype == 'txt')
            {
                $data['media'] = $filePath;
                $file->move('uploads/library/books/', $filePath);
    
            }else{
                return redirect(aurl('librarybooks'))->with('error');
            }
        }

        if($data != null){
            $librarybook->update($data);
           
        return redirect(aurl('librarybooks'))->with(session()->flash('message',trans('admin.success_update')));
        }
        
        return redirect(aurl('librarybooks'));
    }

    public function destroy($id)
    {
        $librarybook = LibraryBook::findOrFail($id);
        $librarybook->delete();
        return redirect(aurl('librarybooks'));
    }

    // public function library_content(Request $request)
    // {
       
    //     if($request->ajax()){
    //         $branch = $request->branche_id;
    //         $list = $request->list_id;
    //         $booktype = $request->booktype;  
    //         $gradetype = $request->gradetype;
    //         $Term = $request->Term;
    //         $librarytype = $request->librarytype;
            
    //         if($branch != null && $librarytype != null){
    //             if($librarytype == 2){
    //                 $branch = $request->branche_id;
    //                 $list = $request->list_id;
    //                 $data = view('admin.library.books.ajax.index', ['list' => $list,'branch' => $branch,'booktype'=> $booktype,'gradetype'=> $gradetype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
    //                 return $data;
    //             }
    //             if($librarytype == 3){
    //                 $branch = $request->branche_id;
    //                 $list = $request->list_id;
    //                 $data = view('admin.library.books.ajax.index', ['list' => $list,'branch' => $branch,'booktype'=> $booktype,'gradetype'=> $gradetype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
    //                 return $data;
    //             }
    //             if($librarytype == 0){
    //                 $branch = $request->branche_id;
    //                 $list = $request->list_id;
    //                 $data = view('admin.library.media.ajax.index', ['list' => $list,'branch' => $branch,'booktype'=> $booktype,'gradetype'=> $gradetype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
    //                 return $data;
    //             }
    //             if($librarytype == 1){
    //                 $branch = $request->branche_id;
    //                 $list = $request->list_id;
    //                 $data = view('admin.library.media.ajax.index', ['list' => $list,'branch' => $branch,'booktype'=> $booktype,'gradetype'=> $gradetype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
    //                 return $data;
    //             }

               
    //         }
    //     }

    // }

}
