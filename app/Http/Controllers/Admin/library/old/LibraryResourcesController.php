<?php

namespace App\Http\Controllers\Admin\library;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\LibraryResource;
use App\DataTables\LibraryResourcesDataTabe;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class LibraryResourcesController extends Controller
{
    public function index(LibraryResourcesDataTabe $libraryresource) 
    { 
        return $libraryresource->render('admin.library.resources.index',['title'=>trans('admin.resources_information')]);
    } 

    public function create(LibraryResource $libraryresource)
    {   
        
        return view('admin.library.resources.create', ['title'=> trans('admin.Create_new_library_resource'),'LibraryResource' => $libraryresource]);
    }

    public function store(Request $request, LibraryResource $libraryresource)
    {
        $this->validate($request,[
            'publish_date' => 'sometimes',
            'book_author' => 'sometimes',
            'book' => 'required|mimes:doc,pdf,docx,zip',
            'image' => 'sometimes',
            'book_name' => 'required',
            'bief_history' => 'sometimes',
            'desciption' => 'sometimes',
            
        ],[],[
            'book_name' => trans('admin.book_name'),  
        ]);

        $libraryresource = new LibraryResource();

        $libraryresource->book_name = $request->book_name;
        $libraryresource->desciption = $request->desciption;  
        $libraryresource->bief_history = $request->bief_history;        
        $libraryresource->publish_date = $request->publish_date;          
        $libraryresource->book_author = $request->book_author;    
        
        if($request->hasFile('image')){
            $libraryresource['image'] = Up::upload([
                'request' => 'image',
                'path'=>'resource',
                'upload_type' => 'single',
            ]);
        }
        if($request->hasFile('book')){
            $file = $request->file('book'); 
            $mediaType=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
            
            if($mediaType=="pdf")
            {
                $libraryresource->book = $filePath;
                $file->move('uploads/library/resources/', $filePath);

            }else{
                return redirect(aurl('library/resources'))->with('error');
            }
        }
        $libraryresource->save();
        return redirect(aurl('library/resources'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
       
        $libraryresource = LibraryResource::findOrFail($id);
        
        return view('admin.library.resources.show',compact('libraryresource'));
    }

    public function edit($id)
    { 
        $libraryresource = LibraryResource::findOrFail($id);
        return view('admin.library.resources.edit',compact('libraryresource'),['title'=>trans('admin.edit_library_resource')]);
    }

    public function update(Request $request, $id)
    {

        $libraryresource = LibraryResource::findOrFail($id);
        $this->validate($request,[
            'publish_date' => 'sometimes',
            'book_author' => 'sometimes',
            'book' => 'sometimes',
            'image' => 'sometimes',
            'book_name' => 'required',
            'bief_history' => 'sometimes',
            'desciption' => 'sometimes',
            
        ],[],[
            'book_name' => trans('admin.book_name'),  
        ]);

        $libraryresource->book_name = $request->book_name;
        $libraryresource->desciption = $request->desciption;  
        $libraryresource->bief_history = $request->bief_history;        
        $libraryresource->publish_date = $request->publish_date;          
        $libraryresource->book_author = $request->book_author;    
        
        if($request->hasFile('image')){
            $libraryresource['image'] = Up::upload([
                'request' => 'image',
                'path'=>'resource',
                'upload_type' => 'single',
                'delete_file' => $libraryresource->image,
            ]);
        }

        if($request->hasFile('book')){
            $file = $request->file('book'); 
            $mediaType=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
            
            if($mediaType=="pdf" && $mediaType == 'txt' && $mediaType == 'docs' )
            {
                $libraryresource->book = $filePath;
                $file->move('uploads/library/resources/', $filePath);
    
            }else{
                return redirect(aurl('library/resources'))->with('enter a valid type pdf docx txt');
            }
    
        }
       
        $libraryresource->save();
           
        return redirect(aurl('library/resources'))->with(session()->flash('message',trans('admin.success_add')));

          
    }


    public function destroy($id)
    {
        $libraryresource = LibraryResource::findOrFail($id);
        $libraryresource->delete();
        return redirect(aurl('library/resources'));
    }
}
