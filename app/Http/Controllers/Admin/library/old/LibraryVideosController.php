<?php

namespace App\Http\Controllers\Admin\library;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use \App\Video;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class LibraryVideosController extends Controller
{
    public function index() 
    { 
        $videos = \App\Video::all();
       
        return view('admin.library.videos.index', compact('videos'));
    } 

    public function create()
    {   
        return view('admin.library.videos.create', ['title'=> trans('admin.Create_new_library_video')]);
    }

    public function store(Request $request, Video $libraryvideo)
    {
        $this->validate($request,[
            
            'video' => 'required',
            
        ],[],[
            
            'video' => trans('admin.video'),
            
            
        ]);
            $libraryvideo = new Video();
          
            
            $file = $request->file('video'); 
            $mediaType=explode('/',$file->getMimeType())[0];
            
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
            
            if($mediaType=="video" )
            {
               $libraryvideo->video = $filePath;
               $file->move('uploads/library/video/', $filePath);

            }else{
                return redirect(aurl('library/videos'))->with('error');
            }
           

            
            $libraryvideo->save();
        
        return redirect(aurl('library/videos'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
       
        $libraryvideo = Video::findOrFail($id);
    
        return view('admin.library.videos.show',compact('libraryvideo'));
    }

    public function edit($id)
    {
       
        $libraryvideo = Video::findOrFail($id);
        return view('admin.library.videos.edit',compact('libraryvideo'),['title'=>trans('admin.edit_library_image')]);
    }

    public function update(Request $request, $id)
    {

        $libraryvideo = Video::findOrFail($id);
        $this->validate($request,[
            'video' => 'required',
            
        ],[],[
            'video' => trans('admin.image'),

        ]);

        $file = $request->file('video'); 
        $mediaType=explode('/',$file->getMimeType())[1];

        // if($mediaType=="video"){
            
           
        //         $libraryvideo['vide'] = Up::upload([
        //             'request' => 'video',
        //             'path'=>'library/video',
        //             'upload_type' => 'single',
        //         ]);   
        //     }

            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
            
            if($mediaType=="video" )
            {
                $file->move('storage/library/video', $filePath);   
            }
           
            
            $libraryvideo->save();
        
        return redirect(aurl('library/videos'))->with(session()->flash('message',trans('admin.success_add')));

          
    }


    public function destroy($id)
    {
        $libraryvideo = Video::findOrFail($id);
        $libraryvideo->delete();
        return redirect(aurl('library/videos'));
    }
}
