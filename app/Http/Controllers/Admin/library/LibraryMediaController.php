<?php

namespace App\Http\Controllers\Admin\library;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\LibraryMedia;
use App\Branches;
use App\LibraryList;
use App\DataTables\LibraryMediaDataTable;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class LibraryMediaController extends Controller
{
    public function index(LibraryMediaDataTable $content) 
    {  
        return $content->render('admin.library.media.index',['title'=>trans('admin.library_media')]);
    } 

    public function create()
    {
        $content =  LibraryMedia::pluck('id');
        $branch = Branches::pluck('name_'.session('lang'),'id');
        $list = LibraryList::pluck('name_'.session('lang'),'id');
        
        return view('admin.library.media.create', ['content' => $content,'list'=> $list, 'branch' => $branch,'title'=> trans('admin.Create_new_media')]);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request,[
            'media' => 'sometimes',
            'desc_en' => 'sometimes',
            'desc_ar' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'branch_id' => 'sometimes',
            'list_id' => 'required',
            'Librarytype' => 'required',
        ],[],[
            'list_id' =>  trans('admin.list_id'),
            'Librarytype' => trans('admin.Librarytype'),
        ]);
        

        $file = $request->file('media');
        
        if($file){
            $mediaType=explode('/',$file->getMimeType())[0];
            $filetype=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
            if( $mediaType=="video" Or $mediaType=="image" Or $filetype=="pdf" Or $filetype=="docx"  )
            {
                $data['media'] = $filePath;
                $file->move('uploads/library/books/', $filePath);
    
            }else{
                return redirect(aurl('librarymedia'))->with('error');
            }
        }

        $content = new LibraryMedia();
        $content->create($data);

        return redirect(aurl('librarymedia'))->with(session()->flash('message',trans('admin.success_add')));
            
    }

    public function show($id)
    {
        $content = LibraryMedia::findOrFail($id);
        return view('admin.library.media.show',compact('content'), ['title'=>trans('admin.show_media')]);
    }

    public function edit($id)
    {
        $content =  LibraryMedia::findOrFail($id);  
        $branch = Branches::pluck('name_'.session('lang'),'id');
        $list = LibraryList::pluck('name_'.session('lang'),'id');
        return view('admin.library.media.edit',['content'=> $content,'list'=> $list, 'branch'=> $branch, 'title'=>trans('admin.edit_library_media_content')]);
    }

    public function update(Request $request, $id)
    {
        $content = LibraryMedia::findOrFail($id);
        $data = $this->validate($request,[
            'media' => 'sometimes',
            'desc_en' => 'sometimes',
            'desc_ar' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'branch_id' => 'sometimes',
            'list_id' => 'required',
            'Librarytype' => 'required',
        ],[],[
            
        ]);

        $file = $request->file('media');
        
        if($file){
            $mediaType=explode('/',$file->getMimeType())[0];
            $filetype=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
            if( $mediaType=="video" Or $mediaType=="image" Or $filetype=="pdf" Or $filetype=="docx"  )
            {
                $data['media'] = $filePath;
                $file->move('uploads/library/books/', $filePath);
    
            }else{
                return redirect(aurl('librarymedia'))->with('error');
            }
        }
       $content->update($data);
        return redirect(aurl('librarymedia'))->with(session()->flash('message',trans('admin.success_update')));
            
    }


    public function destroy($id)
    {
       $content = LibraryMedia::findOrFail($id);
       $content->delete();
        return redirect(aurl('librarymedia'));
    }


    public function upload(Request $request)
    {
        for($i=0; $i < count($request->file('file_name')); $i++)
        {
            $file = $request->file('file_name')[$i];
            $filePath = public_path().'/files';
            $extension = $file->getClientOriginalExtension();
            $files = $file->getClientOiginalName();
            $fileName = $files . '_' . $time() . '.' .$extension;
            $file->move($filePath, $fileName);
        }
        return redirect()->back();
    }
}