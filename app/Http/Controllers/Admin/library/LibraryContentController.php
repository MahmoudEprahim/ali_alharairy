<?php

namespace App\Http\Controllers\Admin\library;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\LibraryBook;
use App\LibraryMedia;
use App\Branches;
use App\LibraryList;
use App\Grade;
use App\DataTables\LibraryBooksDataTabe;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class LibraryContentController extends Controller
{
    

    public function library_content(Request $request)
    {
      
        if($request->ajax()){
            $branch = $request->branche_id;
           
            $booktype = $request->booktype;  
            $grade = $request->grade_id;
            $Term = $request->Term;
            $librarytype = $request->librarytype;
            
            if($branch != null && $librarytype != null){
                if($librarytype == 0){
                    $branch = $request->branche_id;
                    $list  = LibraryList::where('librarytype','0')->pluck('name_ar','id');
                    $data = view('admin.library.media.ajax.index', ['list' => $list,'branch' => $branch,'booktype'=> $booktype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
                    return $data;
                }
                
                if($librarytype == 1){
                    $branch = $request->branche_id;
                    
                    $list  = LibraryList::where('librarytype','1')->pluck('name_ar','id');
                    $data = view('admin.library.media.ajax.index', ['list' => $list,'branch' => $branch,'booktype'=> $booktype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
                    return $data;
                }

                if($librarytype == 2){
                    $branch = $request->branche_id;
                   
                    $grade = Grade::pluck('name_'.session('lang'),'id');
                    
                    $list  = LibraryList::where('librarytype','2')->pluck('name_ar','id');
                    $data = view('admin.library.books.ajax.index', ['grade' => $grade,'list' => $list,'branch' => $branch,'booktype'=> $booktype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
                    return $data;
                }
                if($librarytype == 3){
                    $branch = $request->branche_id;
                    $list  = LibraryList::where('librarytype','3')->pluck('name_ar','id');
                    $grade = Grade::pluck('name_'.session('lang'),'id');
                    $data = view('admin.library.books.ajax.index', ['grade' => $grade,'list' => $list,'branch' => $branch,'booktype'=> $booktype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
                    return $data;
                }
                
            }
        }

    }

}
