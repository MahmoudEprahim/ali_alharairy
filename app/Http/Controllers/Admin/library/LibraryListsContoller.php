<?php

namespace App\Http\Controllers\Admin\library;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\LibraryList;

use App\DataTables\LibraryListsDataTable;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;


class LibraryListsContoller extends Controller
{
    public function index(LibraryListsDataTable $list) 
    { 
        return $list->render('admin.library.list.index',['title'=>trans('admin.library_lists_information')]);
    }    

    public function create(LibraryListsDataTable $list)
    {
        $list = LibraryList::pluck('name_'.session('lang'),'id');
        
        return view('admin.library.list.create', ['list' => $list,'title'=> trans('admin.Create_new_list')]);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request,[
            'name_en' => 'required',
            'name_ar' => 'sometimes',
            'Librarytype' => 'sometimes',
        ],[],[
            'name_en'=> trans('admin.name_en'),
            'name_ar' => trans('admin.name_ar'),
            'Librarytype' => trans('admin.Librarytype'),
            
            
        ]);
        $list = new LibraryList();
        $list->create($data); 
        
        return redirect(aurl('librarylists'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
        $list = LibraryList::findOrFail($id);
       
        return view('admin.library.list.show', ['list' => $list,'title'=> trans('admin.show_list')]);
        
    }

    public function edit($id)
    {
        
        $list = LibraryList::findOrFail($id);
       
        return view('admin.library.list.edit',['list'=> $list, 'title'=>trans('admin.edit_list')]);
    }

    public function update(Request $request, $id)
    {
        $list = LibraryList::findOrFail($id);
        $data = $this->validate($request,[
            'name_en' => 'sometimes',
            'name_ar' => 'sometimes',
            'Librarytype' => 'sometimes',
        ],[],[
            'name_en'=> trans('admin.name_en'),
            'name_ar' => trans('admin.name_ar'),
            'Librarytype' => trans('admin.Librarytype'),
        ]);
            
        $list->update($data);
            
        return redirect(aurl('librarylists'))->with(session()->flash('message',trans('admin.success_update')));

    }


    public function destroy($id)
    {
        $grade = LibraryList::findOrFail($id);
        $grade->delete();
        return redirect(aurl('librarylists'));
    }
}
