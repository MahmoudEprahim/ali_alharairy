<?php

namespace App\Http\Controllers\Admin\Cc;
use App\Branches;
use App\Department;
use App\glcc;
use App\levels;
use App\limitations;
use App\limitationsType;
use App\pjitmmsfl;
use App\receipts;
use App\receiptsData;
use App\receiptsType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class ReportController extends Controller
{
    public function motioncc(){
        $title = trans('admin.motion_detection_center_cost');
        $glcc = glcc::where('type','1')->pluck('name_'.session('lang'),'id');
        return view('admin.cc.reports.index',compact('title','glcc'));
    }
    public function show(Request $request){
        $from_glcc = $request->from_glcc;
        $to_glcc = $request->to_glcc;

        if($request->ajax()) {

            $contents = view('admin.cc.reports.show', compact('to_glcc','from_glcc'))->render();
            return $contents;
        }
    }
    public function details(Request $request){
        $from_glcc = $request->from_glcc;
        $to_glcc = $request->to_glcc;
        $from = $request->from;
        $to = $request->to;
//        $hasTask = limitations::whereDate('created_at','>=',$from)->whereDate('created_at','<=',$to)->whereHas('limitations_type',function ($query) use ($from_glcc,$to_glcc){
//            $query->where('cc_id','>=',$from_glcc);
//            $query->where('cc_id','<=',$to_glcc);
//        })->exists();
//        $hasTask1 = receipts::whereDate('created_at','>=',$from)->whereDate('created_at','<=',$to)->whereHas('receipts_type',function ($query) use ($from_glcc,$to_glcc){
//            $query->where('cc_id','>=',$from_glcc);
//            $query->where('cc_id','<=',$to_glcc);
//        })->exists();


        $contents = view('admin.cc.reports.details',compact('from_glcc', 'to_glcc', 'from', 'to'))->render();
        return $contents;
    }

    //below this comment edited by Ibrahim El Monier

    public function pdf(Request $request) {
        $to_glcc = $request->to_glcc;
        $from_glcc = $request->from_glcc;
        $from = $request->from;
        $to = $request->to;



//        $glcc = glcc::where('id','>=', $from_glcc)->where('id','<=', $to_glcc)->get();
        $glcc = glcc::where('id', '>=', $from_glcc)->where('id', '<=', $to_glcc)->pluck('id')->toArray();


        $receipts_id = receipts::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->pluck('id');


        $receiptsType = receiptsType::whereIn('receipts_id',$receipts_id)->whereHas('glcc',function ($query) use ($from_glcc,$to_glcc){
            $query->where('id', '>=', $from_glcc)->where('id', '<=', $to_glcc);
        })->get();


        $data = $receiptsType->map(function ($data){
            $data->name_ar = $data->glcc->name_ar;
            $data->name_en = $data->glcc->name_ar;
            $data->type = $data->glcc->type;
            $data->cc_creditor = $data->glcc->creditor;
            $data->cc_debtor = $data->glcc->debtor;
            return $data;
        });
//
        $data = $data->groupBy(function($date) {
            return session_lang($date->glcc->name_en,$date->glcc->name_ar);
        });


        $EmptyreceiptsType = receiptsType::whereHas('glcc',function ($query) use ($from_glcc,$to_glcc){
            $query->where('id', '>=', $from_glcc)->where('id', '<=', $to_glcc);
        })->get();

        $EmptyreceiptsType = $EmptyreceiptsType->map(function ($data){
            $data->name_ar = $data->glcc->name_ar;
            $data->name_en = $data->glcc->name_ar;
            $data->type = $data->glcc->type;
            return $data;
        });

        $first_name = glcc::where('id',$from_glcc)->first();
        $second_name = glcc::where('id',$to_glcc)->first();
        $config = ['instanceConfigurator' => function($mpdf) {
            $mpdf->SetHTMLFooter('
                        <div dir="ltr" style="text-align: right">{DATE j-m-Y H:m}</div>
                        <div dir="ltr" style="text-align: center">{PAGENO} of {nbpg}</div>'
            );
        }];
        $pdf = PDF::loadView('admin.cc.reports.pdf.motioncc', compact('data','EmptyreceiptsType','first_name','second_name','from','to'), [], $config);
        return $pdf->stream('glcc_report.pdf');
    }



    public function checkReports(){
        $title = trans('admin.disclosure_of_balances_of_accounts_of_cost_centers');
        $branches = Branches::pluck('name_'.session('lang'),'id');
        $levels = levels::where('type',2)->pluck('levelId','id');
        return view('admin.cc.checkReports.index',compact('levels','title','branches'));
    }
    public function checkShow(Request $request){
        if($request->ajax()) {

            $level = $request->level;
            $kind = $request->kind;

            $reporttype = $request->reporttype;


                if($reporttype == 0 && $level != null )
                {


                    $glcc_first = glcc::where('level_id',$level)->orderBy('code', 'ASC')->pluck('id','name_'.session('lang'))->first();

                    $glcc_last = glcc::where('level_id',$level)->orderBy('code', 'desc')->pluck('id','name_'.session('lang'))->first();

                    $glcc = glcc::where('level_id',$level)->pluck('name_'.session('lang'),'id');

                    return view('admin.cc.checkReports.show_1', compact('level','glcc','glcc_first','glcc_last'));
                }else if($reporttype == 1 && $kind != null)
                {
                    //individual    total_cc

                    switch ($kind) {
                        case '0':
//total
                            $levels = levels::where('type',2)->pluck('id','levelId');
                            $level_last = levels::where('type',2)->pluck('id')->last();


                            $glcc_first = glcc::where('level_id',$level_last)->pluck('id','name_'.session('lang'))->first();

                            $glcc_last = glcc::where('level_id',$level_last)->pluck('id','name_'.session('lang'))->last();

                            $glcc = glcc::where('level_id',$level_last)->pluck('name_'.session('lang'),'id');

                            return view('admin.cc.checkReports.show', compact('level','glcc','glcc_first','glcc_last'));
                           break;
                        case '1':
//balance
                            $levels = levels::where('type',2)->pluck('id','levelId');

                            $level_last = levels::where('type',2)->pluck('id')->last();


                            $glcc_first = glcc::where('level_id',$level_last)->pluck('id','name_'.session('lang'))->first();

                            $glcc_last = glcc::where('level_id',$level_last)->pluck('id','name_'.session('lang'))->last();

                            $glcc = glcc::where('level_id',$level_last)->pluck('name_'.session('lang'),'id');
                            return view('admin.cc.checkReports.show', compact('level','glcc','glcc_first','glcc_last'));



                            break;
                        case '2':
//no_balance
                            $levels = levels::where('type',2)->pluck('id','levelId');
                            $level_last = levels::where('type',2)->pluck('id')->last();


                            $glcc_first = glcc::where('level_id',$level_last)->pluck('id','name_'.session('lang'))->first();

                            $glcc_last = glcc::where('level_id',$level_last)->pluck('id','name_'.session('lang'))->last();

                            $glcc = glcc::where('level_id',$level_last)->pluck('name_'.session('lang'),'id');
                            return view('admin.cc.checkReports.show', compact('level','glcc','glcc_first','glcc_last'));


                            break;

                        default:
                            $levels = levels::where('type',2)->pluck('id','levelId');
                            $level_last = levels::where('type',2)->pluck('id')->last();


                            $glcc_first = glcc::where('level_id',$level_last)->pluck('id','name_'.session('lang'))->first();

                            $glcc_last = glcc::where('level_id',$level_last)->pluck('id','name_'.session('lang'))->last();

                            $glcc = glcc::where('level_id',$level_last)->pluck('name_'.session('lang'),'id');
                            return view('admin.cc.checkReports.show', compact('level','glcc','glcc_first','glcc_last'));



                    }




                }



        }
    }

    public function checkDetails(Request $request){
        if($request->ajax()) {

            $from = $request->from;
            $to = $request->to;

            $fromtree = $request->fromtree;
            $totree = $request->totree;
            $level = $request->level;
            $reporttype = $request->reporttype;
            $kind = $request->kind;


            if ($from != null && $to != null && $fromtree != null && $totree != null && $kind != null){
                $contents = view('admin.cc.checkReports.details',compact('from','to','fromtree','totree','level','reporttype','kind'))->render();
                return $contents;
            }

        }
    }
    public function print(Request $request){

        $from = $request->from;
        $to = $request->to;
        $fromtree = $request->fromtree;
        $totree = $request->totree;
        $level = $request->level;
        $reporttype = $request->reporttype;
        $kind = $request->kind;




        if ($from != null && $to != null && $fromtree != null && $totree != null){

if($reporttype == 0 && $level != null)
{


    switch ($kind){
        case '0';
//        total
            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)->where('level_id',$level)->orderBy('code', 'ASC')->get();

            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.cc.reports.pdf.level_1', ['glcc'=>$glcc,'from'=>$from,'to'=>$to], [] , $config);
            return $pdf->stream();

            break;
        case '1';
//cc_move
            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)->where('level_id',$level)->orderBy('code', 'ASC')->get();

            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.cc.reports.pdf.level_2', ['glcc'=>$glcc,'from'=>$from,'to'=>$to], [] , $config);
            return $pdf->stream();
            break;
        case '2';
            // no_move
            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)->where('level_id',$level)->orderBy('code', 'ASC')->get();


            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.cc.reports.pdf.level_3', ['glcc'=>$glcc,'from'=>$from,'to'=>$to], [] , $config);
            return $pdf->stream();
            break;
        case '4';
//            $departments = Department::orderBy('code')->where('id', '>=', $fromtree)->where('id', '<=', $totree)->where('level_id',$level)->get();
            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)->where('level_id',$level)->orderBy('code', 'ASC')->get();
            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.cc.reports.pdf.level_4', ['glcc'=>$glcc,'from'=>$from,'to'=>$to], [] , $config);
            return $pdf->stream();
        case '5';
            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)->where('level_id',$level)->orderBy('code', 'ASC')->get();

            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.cc.reports.pdf.level_5', ['glcc'=>$glcc,'from'=>$from,'to'=>$to], [] , $config);
            return $pdf->stream();
            break;
    }



}else if($reporttype == 1)
{


    switch ($kind) {
        case '0';

// total
            $parent_id =glcc::groupBy('parent_id')->pluck('parent_id');
            $id =glcc::pluck('id');
            $diff = $id->diff($parent_id);



            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)
                ->whereIn('id',$diff)->orderBy('code', 'ASC')->get();

            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                     <div dir="ltr" style="text-align: right">{DATE j-m-Y H:m}</div>
                     <div dir="ltr" style="text-align: center">{PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.cc.reports.pdf.cc_total', compact('glcc','from','to','fromtree','totree'), [], $config);
            return $pdf->stream('glcc_report.pdf');

            break;
        case '1';
// cc_move

            $parent_id =glcc::groupBy('parent_id')->pluck('parent_id');

            $id =glcc::pluck('id');
            $diff = $id->diff($parent_id);



            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)
                ->whereIn('id',$diff)->orderBy('code', 'ASC')->get();

            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                     <div dir="ltr" style="text-align: right">{DATE j-m-Y H:m}</div>
                     <div dir="ltr" style="text-align: center">{PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.cc.reports.pdf.cc_balance', compact('glcc','from','to','fromtree','totree'), [], $config);
            return $pdf->stream('glcc_report.pdf');
            break;
        case '2';
//        cc_no_move

            $parent_id =glcc::groupBy('parent_id')->pluck('parent_id');

            $id =glcc::pluck('id');
            $diff = $id->diff($parent_id);



            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)
                ->whereIn('id',$diff)->orderBy('code', 'ASC')->get();


//            $parent_id =glcc::groupBy('parent_id')->pluck('parent_id');
//            $id =glcc::pluck('id');
//            $diff = $id->diff($parent_id);
//
//
//            $value_0 = \App\limitationsType::where('cc_id','!=',null)->whereHas('limitations',function ($q)use($to,$from){
//
//                $q->whereDate('created_at','>=',$from);
//                $q->whereDate('created_at','<=',$to);
//            })->pluck('cc_id');
//            $value_1 = \App\receiptsType::where('cc_id','!=',null)->whereHas('receipts',function ($q)use($to,$from){
//
//                $q->whereDate('created_at','>=',$from);
//                $q->whereDate('created_at','<=',$to);
//            })->pluck('cc_id');
//
//            $value2 = array_merge($value_1->toArray(), $value_0->toArray());
//
//            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)
//                ->whereIn('id',$diff)->get();


            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                     <div dir="ltr" style="text-align: right">{DATE j-m-Y H:m}</div>
                     <div dir="ltr" style="text-align: center">{PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.cc.reports.pdf.cc_no_balance', compact('glcc','from','to','fromtree','totree'), [], $config);
            return $pdf->stream('glcc_report.pdf');
            break;
        case '4';
//cc_dept
//            $value_0 = limitationsType::where('creditor','=',0)->where('debtor','!=',0)->where('cc_id','!=',null)->whereHas('limitations',function ($query) use ($from,$to){
//                $query->where('created_at', '>=', $from);
//                $query->where('created_at', '<=', $to);
//            })->pluck('cc_id');
//
//            $value_1 = receiptsType::where('creditor','=',0)->where('debtor','!=',0)->where('cc_id','!=',null)->whereHas('receipts',function ($query) use ($from,$to){
//                $query->where('created_at', '>=', $from);
//                $query->where('created_at', '<=', $to);
//            })->pluck('cc_id');
//
//            $value2 = array_merge($value_1->toArray(), $value_0->toArray());


            $parent_id =glcc::groupBy('parent_id')->pluck('parent_id');
            $id =glcc::pluck('id');


             $diff = $id->diff($parent_id);
            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)
                ->whereIn('id',$diff)->get();









            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                     <div dir="ltr" style="text-align: right">{DATE j-m-Y H:m}</div>
                     <div dir="ltr" style="text-align: center">{PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.cc.reports.pdf.cc_debtor', compact('glcc','from','to','fromtree','totree'), [], $config);
            return $pdf->stream('glcc_report.pdf');
            break;
        case '5';


            $value_0 = receiptsType::where('creditor','!=',0)->where('debtor','=',0)->where('cc_id','!=',null)->whereHas('receipts',function ($query) use ($from,$to){
                $query->where('created_at', '>=', $from);
                $query->where('created_at', '<=', $to);
            })->pluck('cc_id');
            $value_1 = limitationsType::where('creditor','!=',0)->where('debtor','=',0)->where('cc_id','!=',null)->whereHas('limitations',function ($query) use ($from,$to){
                $query->where('created_at', '>=', $from);
                $query->where('created_at', '<=', $to);
            })->pluck('cc_id');
            $value2 = [];
            $value2 = array_merge($value_1->toArray(), $value_0->toArray());

            $parent_id =glcc::groupBy('parent_id')->pluck('parent_id');

            $id =glcc::pluck('id');

            $diff = $id->diff($parent_id);

            $glcc = glcc::where('id','>=',$fromtree)->where('id','<=',$totree)
                ->whereIn('id',$diff)->get();


            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                     <div dir="ltr" style="text-align: right">{DATE j-m-Y H:m}</div>
                     <div dir="ltr" style="text-align: center">{PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.cc.reports.pdf.cc_credior', compact('glcc','from','to','fromtree','totree'), [], $config);
            return $pdf->stream('glcc_report.pdf');

            break;
    }




}


        }

    }



    public function CCpublicbalance(){
        $title = trans('admin.trial_balance_cc');
        $glcc = glcc::where('type','0')->pluck('name_'.session('lang'),'id');

        return view('admin.cc.PublicBalance.index',compact('title','glcc'));
    }


    public function CCpublicbalancelevel(Request $request){
        if($request->ajax())
        {
            $glcc=  $request->glcc;
            $data1 = '';

            $data2 = view('admin.cc.PublicBalance.CCpublicbalance_level', compact('glcc'))->render();

        }
return $data = [$data1,$data2];
    }

    public function CCpublicbalanceprint(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $glcc = $request->glcc;
        $kind = $request->kind;
        $level = $request->level;


        if ($request->ajax()) {




                    $data1 = view('admin.cc.PublicBalance.CCpublicbalance_level', compact('kind','from', 'to', 'glcc', 'level'))->render();
                   $data2 = view('admin.cc.PublicBalance.CCpublicbalance_print', compact('kind','from', 'to', 'glcc', 'level'))->render();

                    return $data = [$data1,$data2];


        }


    }


public  function  CCpublicbalancepdf(Request $request)
{
    $from = $request->from;
    $to = $request->to;
    $glcc = $request->glcc;
    $level = $request->level;
    $kind = $request->kind;



if($from != null && $to != null && $glcc != null && $level != null && $kind != null){
    switch ($kind) {
        case '0';

            if($level == 1){

                $glcc = glcc::where('id','=',$glcc)->get();
                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_total_1', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();
            }elseif( $level == 2){

                $depart =  glcc::where('id','=',$glcc)->first();

                $glcc = $depart->children;




                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_total_2', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'depart'=>$depart,'level'=>$level], [] , $config);
                return $pdf->stream();



            }elseif($level == 3){
//                0

                $value = [];
                $depart =  glcc::where('id','=',$glcc)->first();

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){
                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }

                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection


                $plucks = $pro->pluck('id');

                $categories = $depart->children->pluck('id');

                $glcc_3 = $plucks->concat($categories);
                $glcc = \App\glcc::whereIn('id',$glcc_3)->orderBy('code','asc')->get();


                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_total_3', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'depart'=>$depart,'level'=>$level], [] , $config);
                return $pdf->stream();



            }else
            {
                $value = [];
                $depart =  glcc::where('id','=',$glcc)->first();

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){

                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }


                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                $pros = $depart->children->pluck('id');

                $plucks = $pro->pluck('id');
                $values = $pros->concat($plucks);


                $glcc_3 = \App\glcc::whereIn('parent_id',$values)->pluck('id');

                $values_all = $pros->concat($plucks,$glcc_3);


                $glcc = \App\glcc::whereIn('id',$values_all)->orderBy('code','asc')->get();


                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_total_4', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'depart'=>$depart,'level'=>$level], [] , $config);
                return $pdf->stream();

            }

            break;
        case '1';

            if($level == 1){

                $glcc = glcc::where('id','=',$glcc)->get();
                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];

                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_1', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();
            }elseif( $level == 2){

                $depart =  glcc::where('id','=',$glcc)->first();


                $glcc = $depart->children;
                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_2', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'depart'=>$depart,'level'=>$level], [] , $config);
                return $pdf->stream();



            }elseif($level == 3){
                $value = [];

                $depart =  glcc::where('id','=',$glcc)->first();

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){

                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }
                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection


                $plucks = $pro->pluck('id');

                $categories = $depart->children->pluck('id');

                $glcc_3 = $plucks->concat($categories);
                $glcc = \App\glcc::whereIn('id',$glcc_3)->orderBy('code','asc')->get();


                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_3', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'depart'=>$depart,'level'=>$level], [] , $config);
                return $pdf->stream();



            }else
            {
                $value = [];
                $depart =  glcc::where('id','=',$glcc)->first();

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){

                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }

                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                $pros = $depart->children->pluck('id');

                $plucks = $pro->pluck('id');
                $values = $pros->concat($plucks);


                $glcc_3 = \App\glcc::whereIn('parent_id',$values)->pluck('id');

                $values_all = $pros->concat($plucks,$glcc_3);


                $glcc = \App\glcc::whereIn('id',$values_all)->orderBy('code','asc')->get();



                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_4', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'depart'=>$depart,'level'=>$level], [] , $config);
                return $pdf->stream();

            }

            break;
        case '2';

            if($level == 1){

                $glcc = glcc::where('id','=',$glcc)->get();
                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];

                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_No_1', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();
            }elseif( $level == 2){

                $depart =  glcc::where('id','=',$glcc)->first();


                $glcc = $depart->children;
                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_No_2', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'depart'=>$depart,'level'=>$level], [] , $config);
                return $pdf->stream();



            }elseif($level == 3){
                $value = [];

                $depart =  glcc::where('id','=',$glcc)->first();

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){

                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }

                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection


                $plucks = $pro->pluck('id');

                $categories = $depart->children->pluck('id');

                $glcc_3 = $plucks->concat($categories);
                $glcc = \App\glcc::whereIn('id',$glcc_3)->orderBy('code','asc')->get();


                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_No_3', ['from'=>$from,'to'=>$to,'depart'=>$depart,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();



            }else
            {
                $value = [];
                $depart =  glcc::where('id','=',$glcc)->first();

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){

                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }

                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                $pros = $depart->children->pluck('id');

                $plucks = $pro->pluck('id');
                $values = $pros->concat($plucks);


                $glcc_3 = \App\glcc::whereIn('parent_id',$values)->pluck('id');

                $values_all = $pros->concat($plucks,$glcc_3);


                $glcc = \App\glcc::whereIn('id',$values_all)->orderBy('code','asc')->get();



                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_No_4', ['from'=>$from,'to'=>$to,'depart'=>$depart,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();

            }

            break;
        case '4';

            if($level == 1){

                $glcc = glcc::where('id','=',$glcc)->get();
                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];

                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_debt_1', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();
            }elseif( $level == 2){

                $depart =  glcc::where('id','=',$glcc)->first();


                $glcc = $depart->children;
                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_debt_2', ['from'=>$from,'to'=>$to,'depart'=>$depart,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();



            }elseif($level == 3){
                $value = [];

                $depart =  glcc::where('id','=',$glcc)->first();

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){

                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }

                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection


                $plucks = $pro->pluck('id');

                $categories = $depart->children->pluck('id');

                $glcc_3 = $plucks->concat($categories);
                $glcc = \App\glcc::whereIn('id',$glcc_3)->orderBy('code','asc')->get();



                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_debt_3', ['from'=>$from,'to'=>$to,'depart'=>$depart,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();



            }else
            {
                $value = [];
                $depart =  glcc::where('id','=',$glcc)->first();

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){

                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }

                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                $pros = $depart->children->pluck('id');

                $plucks = $pro->pluck('id');
                $values = $pros->concat($plucks);


                $glcc_3 = \App\glcc::whereIn('parent_id',$values)->pluck('id');

                $values_all = $pros->concat($plucks,$glcc_3);


                $glcc = \App\glcc::whereIn('id',$values_all)->orderBy('code','asc')->get();


                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_debt_4', ['from'=>$from,'to'=>$to,'depart'=>$depart,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();

            }

            break;
        case '5';

            if($level == 1){

                $glcc = glcc::where('id','=',$glcc)->get();
                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];

                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_cred_1', ['from'=>$from,'to'=>$to,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();
            }elseif( $level == 2){

                $depart =  glcc::where('id','=',$glcc)->first();


                $glcc = $depart->children;
                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_cred_2', ['from'=>$from,'to'=>$to,'depart'=>$depart,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();



            }elseif($level == 3){
                $value = [];

                $depart =  glcc::where('id','=',$glcc)->first();

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){

                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }

                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection


                $plucks = $pro->pluck('id');

                $categories = $depart->children->pluck('id');

                $glcc_3 = $plucks->concat($categories);
                $glcc = \App\glcc::whereIn('id',$glcc_3)->orderBy('code','asc')->get();



                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_cred_3', ['from'=>$from,'to'=>$to,'depart'=>$depart,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();



            }else
            {
                $value = [];
                $depart =  glcc::where('id','=',$glcc)->first();

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){

                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }

                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                $pros = $depart->children->pluck('id');

                $plucks = $pro->pluck('id');
                $values = $pros->concat($plucks);


                $glcc_3 = \App\glcc::whereIn('parent_id',$values)->pluck('id');

                $values_all = $pros->concat($plucks,$glcc_3);


                $glcc = \App\glcc::whereIn('id',$values_all)->orderBy('code','asc')->get();

                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.cc.reports.pdf.PB_balance_cred_4', ['from'=>$from,'to'=>$to,'depart'=>$depart,'glcc'=>$glcc,'level'=>$level], [] , $config);
                return $pdf->stream();

            }

            break;
    }
}
}



}
