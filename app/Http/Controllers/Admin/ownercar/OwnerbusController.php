<?php

namespace App\Http\Controllers\Admin\ownercar;

use App\DataTables\ownerbusDataTable;
use App\Http\Controllers\Controller;
use App\ownercar;
use Illuminate\Http\Request;

class OwnerbusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ownerbusDataTable $ownerbus)
    {
        return $ownerbus->render('admin.ownerbus.index',['title'=>trans('admin.bus_owner')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ownerbus.create',['title'=> trans('admin.Add_New_Owner_Bus')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ownercar $ownercar)
    {
        $data = $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
            'date_buy' => 'required',
            'buy_value' => 'required',
            'prepaid' => 'sometimes',
            'installment_value' => 'sometimes',
            'installment_num' => 'sometimes',
            'payment' => 'sometimes',
        ],[],[
            'name_ar' => trans('admin.arabic_name'),
            'name_en' => trans('admin.english_name'),
            'date_buy' => trans('admin.date_buy'),
            'buy_value' => trans('admin.buy_value'),
            'prepaid' => trans('admin.prepaid'),
            'installment_value' => trans('admin.installment_value'),
            'installment_num' => trans('admin.installment_number'),
            'payment' => trans('admin.payment'),
        ]);
        $ownercar->create($data);
        return redirect(aurl('ownerbus'))->with(session()->flash('message',trans('admin.success_add')));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ownerbus = ownercar::findOrFail($id);
        return view('admin.ownerbus.edit',['title'=> trans('admin.Edit_Owner_Bus') ,'ownerbus'=>$ownerbus]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
            'date_buy' => 'required',
            'buy_value' => 'required',
            'prepaid' => 'sometimes',
            'installment_value' => 'sometimes',
            'installment_num' => 'sometimes',
            'payment' => 'sometimes',
        ],[],[
            'name_ar' => trans('admin.arabic_name'),
            'name_en' => trans('admin.english_name'),
            'date_buy' => trans('admin.date_buy'),
            'buy_value' => trans('admin.buy_value'),
            'prepaid' => trans('admin.prepaid'),
            'installment_value' => trans('admin.installment_value'),
            'installment_num' => trans('admin.installment_number'),
            'payment' => trans('admin.payment'),
        ]);
        $ownercar = ownercar::findOrFail($id);
        $ownercar->update($data);
        return redirect(aurl('ownerbus'))->with(session()->flash('message',trans('admin.success_update')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ownercar = ownercar::findOrFail($id);
        $ownercar->delete();
        return redirect(aurl('ownerbus'));
    }
}
