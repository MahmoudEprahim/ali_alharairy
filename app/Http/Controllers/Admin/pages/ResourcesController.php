<?php

namespace App\Http\Controllers\Admin\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\LibraryResource;
use App\Branches;
use App\LibraryList;
use App\Grade;
use App\DataTables\ResourcesDataTabe;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class ResourcesController extends Controller
{
    public function index(ResourcesDataTabe $content) 
    { 
        return $content->render('admin.library.resources.index',['title'=>trans('admin.resources_information')]);
    } 

    public function create(LibraryResource $content)
    {   
        $branch = Branches::pluck('name_'.session('lang'),'id');
        $list = LibraryList::pluck('name_'.session('lang'),'id');
        return view('admin.library.resources.create', ['title'=> trans('admin.add_new_resources'),'branch'=> $branch,'list' => $list,'content' => $content]);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request,[
            'media' => 'sometimes',
            'image' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'desc_ar' => 'sometimes',
            'desc_en' => 'sometimes',
            'list_id' => 'sometimes',
            'branch_id' => 'sometimes',
        ],[],[
              
            
        ]);

        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'library/resources',
                'upload_type' => 'single',
            ]);
        }

        $file = $request->file('media');
        if($file){
            $mediaType=explode('/',$file->getMimeType())[0];
            $filetype=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
            if( $filetype=="pdf" Or $filetype=="docx" Or $filetype == 'txt')
            {
                $data['media'] = $filePath;
                $file->move('uploads/library/resources/', $filePath);
    
            }else{
                return redirect(aurl('resources'))->with('error');
            }
        }
         
        $content = new LibraryResource();
        $content->create($data); 
        return redirect(aurl('resources'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
        $content = LibraryResource::findOrFail($id);
        return view('admin.library.resources.show',compact('content'), ['title'=> trans('admin.show_resources')]);
    }

    public function edit($id)
    { 
        $branch = Branches::pluck('name_'.session('lang'),'id');
        $content = LibraryResource::findOrFail($id);
        $list = LibraryList::pluck('name_'.session('lang'),'id');
       
        return view('admin.library.resources.edit',compact('content','branch','list'),['title'=>trans('admin.edit_library_resources')]);
    }

    public function update(Request $request, $id)
    {

        $content = LibraryResource::findOrFail($id);
        $data = $this->validate($request,[
            'media' => 'sometimes',
            'image' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'desc_ar' => 'sometimes',
            'desc_en' => 'sometimes',
            'list_id' => 'sometimes',
            'branch_id' => 'sometimes',
        ],[],[     
            
        ]);

        
        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'library/book',
                'upload_type' => 'single',
                'delete_file' => $content->image,
            ]);
        }

        $file = $request->file('media');
        if($file){
            $mediaType=explode('/',$file->getMimeType())[0];
            $filetype=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
            if( $filetype=="pdf" Or $filetype=="docx" Or $filetype == 'txt')
            {
                $data['media'] = $filePath;
                $file->move('uploads/library/resources/', $filePath);
    
            }else{
                return redirect(aurl('resources'))->with('error');
            }
        }

        if($data != null){
            $content->update($data);
           
        return redirect(aurl('resources'))->with(session()->flash('message',trans('admin.success_update')));
        }
        
        return redirect(aurl('resources'));
    }

    public function destroy($id)
    {
        $content = LibraryResource::findOrFail($id);
        $content->delete();
        return redirect(aurl('resources'));
    }

    // public function library_content(Request $request)
    // {
       
    //     if($request->ajax()){
    //         $branch = $request->branche_id;
    //         $list = $request->list_id;
    //         $booktype = $request->booktype;  
    //         $gradetype = $request->gradetype;
    //         $Term = $request->Term;
    //         $librarytype = $request->librarytype;
            
    //         if($branch != null && $librarytype != null){
    //             if($librarytype == 2){
    //                 $branch = $request->branche_id;
    //                 $list = $request->list_id;
    //                 $data = view('admin.library.resources.ajax.index', ['list' => $list,'branch' => $branch,'booktype'=> $booktype,'gradetype'=> $gradetype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
    //                 return $data;
    //             }
    //             if($librarytype == 3){
    //                 $branch = $request->branche_id;
    //                 $list = $request->list_id;
    //                 $data = view('admin.library.resources.ajax.index', ['list' => $list,'branch' => $branch,'booktype'=> $booktype,'gradetype'=> $gradetype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
    //                 return $data;
    //             }
    //             if($librarytype == 0){
    //                 $branch = $request->branche_id;
    //                 $list = $request->list_id;
    //                 $data = view('admin.library.media.ajax.index', ['list' => $list,'branch' => $branch,'booktype'=> $booktype,'gradetype'=> $gradetype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
    //                 return $data;
    //             }
    //             if($librarytype == 1){
    //                 $branch = $request->branche_id;
    //                 $list = $request->list_id;
    //                 $data = view('admin.library.media.ajax.index', ['list' => $list,'branch' => $branch,'booktype'=> $booktype,'gradetype'=> $gradetype,'Term'=> $Term,'librarytype'=>$librarytype])->render();
    //                 return $data;
    //             }

               
    //         }
    //     }

    // }

}
