<?php

namespace App\Http\Controllers\Admin\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Sayabout;
use App\DataTables\SayaboutDataTabe;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class SayaboutController extends Controller
{
    public function index(SayaboutDataTabe $content) 
    { 
        return $content->render('admin.sayabout.index',['title'=>trans('admin.sayabout_information')]);
    } 


    public function store(Request $request,Sayabout $sayabout)
    {  
        $data= $this->validate($request,[
            'name'=> 'required',
            'graduation_year'=> 'required',
            'current_work'=> 'required',
            'email'=> 'required',
            'comment'=> 'required',
        ],[],[
            'name'=> trans('admin.name'),
            'graduation_year' => trans('admin.graduation_year'),
            'current_work' => trans('admin.current_work'),
            'email' => trans('admin.email'),
            'comment' => trans('admin.comment'),
        ]);
        
        $sayabout->create($data);
        
        return back()->with(session()->flash('message',trans('admin.success_add')));
    }


    public function isActive(Request $request, $id)
    {
        $sayabout = Sayabout::findOrFail($id);
        $sayabout->update(['isactive' => '1']);
        return back()->with(session()->flash('message',trans('admin.success_update')));
    
    }

    public function destroy($id)
    {
        $content = Sayabout::findOrFail($id);
        $content->delete();
        return redirect(aurl('sayabout'));
    }



}
