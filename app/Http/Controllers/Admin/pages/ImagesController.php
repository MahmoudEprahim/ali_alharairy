<?php

namespace App\Http\Controllers\Admin\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\LibraryImage;
use App\Branches;
use App\LibraryList;
use App\DataTables\LibraryImageDataTable;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class ImagesController extends Controller
{
   
    public function index(LibraryImageDataTable $content) 
    {  
        return $content->render('admin.library.images.index',['title'=>trans('admin.library_iamge_data')]);
    } 

    public function create()
    {
        $content =  LibraryImage::pluck('id');
        $branch = Branches::pluck('name_'.session('lang'),'id');
        $list = LibraryList::pluck('name_'.session('lang'),'id');
        
        return view('admin.library.images.create', ['content' => $content,'list'=> $list, 'branch' => $branch,'title'=> trans('admin.Create_new_image')]);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request,[
            'image' => 'required',
            'desc_en' => 'sometimes',
            'desc_ar' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'branch_id' => 'sometimes',
            'list_id' => 'sometimes',
        ],[],[
            
            'image' => trans('admin.image'),
        ]);
        
        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'library/images',
                'upload_type' => 'single',
            ]);
        }
       
        $content = new LibraryImage();
        $content->create($data);

        return redirect(aurl('images'))->with(session()->flash('message',trans('admin.success_add')));
            
    }

    public function show($id)
    {
        $content = LibraryImage::findOrFail($id);
        return view('admin.library.images.show',compact('content'), ['title'=>trans('admin.show_images')]);
    }

    public function edit($id)
    {
        $content =  LibraryImage::findOrFail($id);  
        $branch = Branches::pluck('name_'.session('lang'),'id');
        $list = LibraryList::pluck('name_'.session('lang'),'id');
        return view('admin.library.images.edit',['content'=> $content,'list'=> $list, 'branch'=> $branch, 'title'=>trans('admin.edit_library_images')]);
    }

    public function update(Request $request, $id)
    {
        $content = LibraryImage::findOrFail($id);
        $data = $this->validate($request,[
            'image' => 'sometimes',
            'desc_en' => 'sometimes',
            'desc_ar' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'branch_id' => 'sometimes',
            'list_id' => 'sometimes',
        ],[],[
            
        ]);

        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'library/images',
                'upload_type' => 'single',
                'delete_file' => $content->image,
            ]);
        }
        if($data != null){
            $content->update($data);
            return redirect(aurl('images'))->with(session()->flash('message',trans('admin.success_update')));  
        }
         
    }


    public function destroy($id)
    {
       $content = LibraryImage::findOrFail($id);
       $content->delete();
        return redirect(aurl('images'));
    }


    public function upload(Request $request)
    {
        for($i=0; $i < count($request->file('file_name')); $i++)
        {
            $file = $request->file('file_name')[$i];
            $filePath = public_path().'/files';
            $extension = $file->getClientOriginalExtension();
            $files = $file->getClientOiginalName();
            $fileName = $files . '_' . $time() . '.' .$extension;
            $file->move($filePath, $fileName);
        }
        return redirect()->back();
    }
}
