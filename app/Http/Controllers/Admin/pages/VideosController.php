<?php

namespace App\Http\Controllers\Admin\pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\LibraryVideo;
use App\Branches;
use App\LibraryList;
use App\DataTables\LibraryVideoDataTable;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class VideosController extends Controller
{
   
    public function index(LibraryVideoDataTable $content) 
    {  
        return $content->render('admin.library.videos.index',['title'=>trans('admin.library_videos')]);
    } 

    public function create()
    {
        $content =  LibraryVideo::pluck('id');
        $branch = Branches::pluck('name_'.session('lang'),'id');
        $list = LibraryList::pluck('name_'.session('lang'),'id');
        
        return view('admin.library.videos.create', ['content' => $content,'list'=> $list, 'branch' => $branch,'title'=> trans('admin.Create_new_video')]);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request,[
            'image' => 'sometimes',
            'video_src' => 'required',
            'desc_en' => 'sometimes',
            'desc_ar' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'branch_id' => 'sometimes',
            'list_id' => 'sometimes',
            'VideoType' => 'required',
        ],[],[
            'video_src' => trans('admin.video'),
            'required' => trans('admin.required'),
        ]);

        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'library/videos',
                'upload_type' => 'single',
            ]);
        }
       
        $embedvideo = str_replace('watch?v=', 'embed/', $request->video_src);
        $data['video_src'] = $embedvideo;
        
        
        $content = new LibraryVideo();
        $content->create($data);

        return redirect(aurl('videos'))->with(session()->flash('message',trans('admin.success_add')));
            
    }

    public function show($id)
    {
        $content = LibraryVideo::findOrFail($id);
        return view('admin.library.videos.show',compact('content'), ['title'=>trans('admin.show_media')]);
    }

    public function edit($id)
    {
        $content =  LibraryVideo::findOrFail($id);  
        $branch = Branches::pluck('name_'.session('lang'),'id');
        $list = LibraryList::pluck('name_'.session('lang'),'id');
        return view('admin.library.videos.edit',['content'=> $content,'list'=> $list, 'branch'=> $branch, 'title'=>trans('admin.edit_library_media_content')]);
    }

    public function update(Request $request, $id)
    {
        $content = LibraryVideo::findOrFail($id);
        $data = $this->validate($request,[
            'image' => 'sometimes',
            'video_src' => 'sometimes',
            'desc_en' => 'sometimes',
            'desc_ar' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'branch_id' => 'sometimes',
            'list_id' => 'sometimes',
            'VideoType' => 'sometimes',
        ],[],[
            
        ]);

        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'library/videos',
                'upload_type' => 'single',
                'delete_file' => $content->image,
            ]);
        }
        $embedvideo = str_replace('watch?v=', 'embed/', $request->video_src);
        $data['video_src'] = $embedvideo;
        
        if($data != null){
            $content->update($data);
            return redirect(aurl('videos'))->with(session()->flash('message',trans('admin.success_update')));  
        }
         
    }

    public function destroy($id)
    {
       $content = LibraryVideo::findOrFail($id);
       $content->delete();
        return redirect(aurl('videos'));
    }


    public function upload(Request $request)
    {
        for($i=0; $i < count($request->file('file_name')); $i++)
        {
            $file = $request->file('file_name')[$i];
            $filePath = public_path().'/files';
            $extension = $file->getClientOriginalExtension();
            $files = $file->getClientOiginalName();
            $fileName = $files . '_' . $time() . '.' .$extension;
            $file->move($filePath, $fileName);
        }
        return redirect()->back();
    }
}
