<?php

namespace App\Http\Controllers\Admin\progress;

use App\Applicant;
use App\applicants_company;
use App\Branches;
use App\Company;
use App\country;
use App\evaluation;
use App\files;
use App\glcc;
use App\levels;
use App\papers;
use App\questions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Up;

class progressController extends Controller
{
    public function index()
    {
        return view('admin.progress.create', ['title' => trans('admin.motion_detection_filter')]);
    }

    public function createPapers(papers $paper)
    {
        $applicant = Applicant::pluck('name_' . session('lang'), 'id');
        return view('admin.progress.create', compact('applicant', 'paper', 'applicant_company'), ['title' => trans('admin.motion_detection_filter')]);
    }
//    public function details(Request $request){
//        if ($request->ajax()){
//            if ($request->companies && $request->jobs && $request->applicants){
//                $company = $request->companies;
//                $job = $request->jobs;
//                $applicant = $request->applicants;
//                $technical_questions = questions::where('companies_id',$company)->where('applicants_requests_id',$job)->where('status','=',1)->get();
//                $educational_questions = questions::where('companies_id',$company)->where('applicants_requests_id',$job)->where('status','=',2)->get();
//                $technical_questionexists = questions::where('companies_id',$company)->where('applicants_requests_id',$job)->where('status','=',1)->exists();
//                $educational_questionexists = questions::where('companies_id',$company)->where('applicants_requests_id',$job)->where('status','=',2)->exists();
//                $evaluation = evaluation::all();
//                $paper = papers::where('applicants_id',$applicant)->first();
//                if ($paper == null){
//                    papers::create([
//                        'applicants_id' => $applicant,
//                    ]);
//                }
//                if (!DB::table('applicants_evaluation')->where('applicants_id',$applicant)->where('evaluations_id',1)->exists()){
//                    DB::table('applicants_evaluation')->insert([
//                        'applicants_id' => $applicant,
//                        'evaluations_id' => 1
//                    ]);
//                }
//                if (!DB::table('applicants_evaluation')->where('applicants_id',$applicant)->where('evaluations_id',2)->exists()){
//                    DB::table('applicants_evaluation')->insert([
//                        'applicants_id' => $applicant,
//                        'evaluations_id' => 2
//                    ]);
//                }
//                $skills = DB::table('applicants_evaluation')->where('applicants_id',$applicant)->where('evaluations_id',1)->first();
//                $general_look = DB::table('applicants_evaluation')->where('applicants_id',$applicant)->where('evaluations_id',2)->first();
//                $applicant_companies = applicants_company::where('companies_id',$request->companies)->where('applicants_id',$request->applicants)->where('applicants_requests_id',$request->jobs)->first();
//                $countries = country::pluck('country_name_'.session('lang'),'id');
//                $fileexist = files::where('relation_id',$paper->id)->exists();
//                if ($fileexist){
//                    $files = files::where('relation_id',$paper->id)->get();
//                    $contents = view('admin.progress.ajax.details', ['jobs'=>$request->jobs,'applicant'=>$request->applicants,'company'=>$request->companies,'applicant_companies'=>$applicant_companies,'countries'=>$countries,'technical_questions'=>$technical_questions,'educational_questions'=>$educational_questions,'technical_questionexists'=>$technical_questionexists,'educational_questionexists'=>$educational_questionexists,'evaluation'=>$evaluation,'paper'=>$paper,'files'=>$files,'fileexist'=>$fileexist,'skills'=>$skills,'general_look'=>$general_look])->render();
//                    return $contents;
//                }
//                $contents = view('admin.progress.ajax.details', ['jobs'=>$request->jobs,'applicant'=>$request->applicants,'company'=>$request->companies,'applicant_companies'=>$applicant_companies,'countries'=>$countries,'technical_questions'=>$technical_questions,'educational_questions'=>$educational_questions,'technical_questionexists'=>$technical_questionexists,'educational_questionexists'=>$educational_questionexists,'evaluation'=>$evaluation,'paper'=>$paper,'fileexist'=>$fileexist,'skills'=>$skills,'general_look'=>$general_look])->render();
//                return $contents;
//
//            }
//        }
//    }
//    public function evaluation(Request $request){
//        if ($request->ajax()){
//            if ($request->companies && $request->jobs && $request->applicants){
//                $applicant_companies = applicants_company::where('companies_id',$request->companies)->where('applicants_id',$request->applicants)->where('applicants_requests_id',$request->jobs)->first();
//                $countries = country::pluck('country_name_'.session('lang'),'id');
//                $contents = view('admin.progress.ajax.details', ['applicant_companies'=>$applicant_companies,'countries'=>$countries])->render();
//                return $contents;
//            }
//        }
//    }
    public function details(Request $request)
    {
        if ($request->ajax()) {
            if ($request->companies && $request->jobs && $request->applicants) {
                $company = $request->companies;
                $job = $request->jobs;
                $applicant = $request->applicants;
//                $paper = papers::where('applicants_id', $applicant)->first();
//                if ($paper == null) {
//                    papers::create([
//                        'applicants_id' => $applicant,
//                    ]);
//                }
                $applicant_companies = applicants_company::where('companies_id', $request->companies)->where('applicants_id', $request->applicants)->where('applicants_requests_id', $request->jobs)->first();
                $applicant_con = applicants_company::where('companies_id', $request->companies)->where('applicants_id', $request->applicants)->get();
                $countries = country::pluck('country_name_' . session('lang'), 'id');
//                $fileexist = files::where('relation_id', $paper->id)->exists();
//                if ($fileexist) {
//                    $files = files::where('relation_id', $paper->id)->get();
//                    $contents = view('admin.progress.ajax.details', ['jobs' => $request->jobs, 'applicant' => $request->applicants, 'company' => $request->companies, 'applicant_companies' => $applicant_companies, 'countries' => $countries, 'paper' => $paper, 'files' => $files, 'fileexist' => $fileexist, 'applicant_con' => $applicant_con])->render();
//                    return $contents;
//                }
                $contents = view('admin.progress.ajax.details', ['jobs' => $request->jobs, 'applicant' => $request->applicants, 'company' => $request->companies, 'applicant_companies' => $applicant_companies, 'countries' => $countries, 'applicant_con' => $applicant_con])->render();
                return $contents;
//                dd($fileexist);
            }
        }
    }

    public function contracts_fees(){
        $branches = Branches::pluck('name_ar','id');
        $applicants =  Applicant::join('applicants_company', 'applicants.id', '=', 'applicants_company.applicants_id')->where('Candidate_case',1)->pluck('applicants.name_ar','applicants.id');
        $companies =  Company::pluck('name_ar','id');
        // $applicantsCompany = applicants_company::where('move_number','!=',null)->get(['move_number']);
        // $applicantsCompanyCount = count($applicantsCompany);
        return view('admin.progress.contracts_fees.index', compact('applicants','branches','companies'), ['title' => trans('admin.contracts_fees')]);
    }
    public function MoveContractFees()
    {
        return view('admin.progress.contracts_fees.showmovecontract',['title'=>'تقارير التعاقدات']);
    }
    public function getCompanies(Request $request)
    {
        if ($request->ajax()) {
            $companies = Company::join('applicants_company', 'companies.id', 'applicants_company.companies_id')->where('applicants_id', $request->applicants)->pluck('companies.name_ar','companies.id');
            // $applicantsCompany = applicants_company::where('move_number','!=',null)->get(['move_number']);
            // $applicantsCompanyCount = count($applicantsCompany);
            // return(count($applicantsCompany));
            return view('admin.progress.contracts_fees.companies', compact('companies'));
        }
    }

    public function getContracts(Request $request)
    {
        // return $request;
        if ($request->ajax()) {
            $applicant_companies = applicants_company::where('companies_id', $request->companies)->where('applicants_id', $request->applicants)->first();
            // dd($applicant_companies);
            $applicantsCompany = applicants_company::where('companies_id', $request->companies)->where('applicants_id', $request->applicants)->get(['move_number']);
            // return($applicantsCompany[0]->move_number);
            if ($applicantsCompany[0]->move_number != null) {
                $applicantsCompanyCount = $applicantsCompany[0]->move_number;
                // return($applicantsCompanyCount);
                return view('admin.progress.contracts_fees.contracts', ['applicant' => $request->applicants, 'company' => $request->companies, 'applicant_companies' => $applicant_companies],compact('applicantsCompanyCount'));
            }else if($applicantsCompany[0]->move_number == null){
                $applicantsCompanyNew = applicants_company::where('move_number','!=',null)->get(['move_number']);
                $applicantsCompanyNew = count($applicantsCompanyNew);
                $applicantsCompanyCount = ($request->branches * 10000) + ($applicantsCompanyNew + 1);
                // return($applicantsCompanyCount);
                return view('admin.progress.contracts_fees.contracts', ['applicant' => $request->applicants, 'company' => $request->companies, 'applicant_companies' => $applicant_companies],compact('applicantsCompanyCount'));
            };
        }
    }

    public function storeContracts(Request $request) {
        if ($request->ajax()) {
            $move_number = $request->move_number;
            $agreement_date = $request->agreement_date;
            $price = $request->price;
            $price_else = $request->price_else;
            $company_fees = $request->company_fees;
            $contract_value = $request->contract_value;
            $contract_percent = $request->contract_percent;
            $percent_total = $request->percent_total;
            $exchange_rate = $request->exchange_rate;
            $contract_total = $request->contract_total;
//            dd($agreement_date,$price,$price_else,$company_fees);

            applicants_company::where('companies_id', $request->company)->where('applicants_id', $request->applicant)->update([
                'move_number' => $move_number,
                'agreement_date' => $agreement_date,
                'price' => $price,
                'price_else' => $price_else,
                'company_fees' => $company_fees,
                'contract_value' => $contract_value,
                'contract_percent' => $contract_percent,
                'percent_total' => $percent_total,
                'exchange_rate' => $exchange_rate,
                'contract_total' => $contract_total,
            ]);
            return '';
        }
    }

    public function store(Request $request)
    {

        if ($request->ajax()) {

            $date = $request->date;
            $time = $request->time;
            $contacts_id = $request->contacts_id;
            $countries = $request->countries;
            $salary = $request->salary;
            $skill = $request->skills;
            $look = $request->general_look;
            $educational = $request->educational;
            $technical = $request->technical;
            $flight_number = $request->flight_number;
            $airline = $request->airline;
            $departure_date = $request->departure_date;
            $arrival_date = $request->arrival_date;
            $departure_time = $request->departure_time;
            $arrival_time = $request->arrival_time;
            $airport_departure = $request->airport_departure;
            $airport_arrival = $request->airport_arrival;
            $Candidate_case = $request->Candidate_case;
//            dd($agreement_date,$price,$price_else,$company_fees);

            applicants_company::where('companies_id', $request->company)->where('applicants_id', $request->applicant)->where('applicants_requests_id', $request->jobs)->update([
                'date' => $date,
                'schedules' => $time,
                'contacts_id' => $contacts_id,
                'countries_id' => $countries,
                'salary' => $salary,
                'skills' => $skill,
                'general_look' => $look,
                'educational' => $educational,
                'technical' => $technical,
                'flight_number' => $flight_number,
                'airline' => $airline,
                'departure_date' => $departure_date,
                'arrival_date' => $arrival_date,
                'departure_time' => $departure_time,
                'arrival_time' => $arrival_time,
                'airport_departure' => $airport_departure,
                'airport_arrival' => $airport_arrival,
                'Candidate_case' => $Candidate_case,
            ]);
//            $applicant = Applicant::findOrFail($request->applicant);
//            $skills = evaluation::where('name','skills')->first();
//            $general_look = evaluation::where('name','general_look')->first();
//            $evaluationexist = DB::table('applicants_evaluation')->where('applicants_id',$applicant->id)->exists();
//            if (!$evaluationexist){
//                $applicant->evaluations()->attach($skills,['result'=>$skill]);
//                $applicant->evaluations()->attach($general_look,['result'=>$look]);
//            }else{
//                $applicant->evaluations()->sync([$skills->id =>['result'=>$skill],$general_look->id =>['result'=>$look]]);
//            }
//
//            DB::table('applicants_questions')->where('applicants_id',$applicant->id)->delete();
//            $e_questinsex = DB::table('questions')->where('companies_id',$request->company)->where('status',2)->where('applicants_requests_id',$request->jobs)->exists();
//            $t_questinsex = DB::table('questions')->where('companies_id',$request->company)->where('status',1)->where('applicants_requests_id',$request->jobs)->exists();
//            $t_questins = DB::table('questions')->where('companies_id',$request->company)->where('status',1)->where('applicants_requests_id',$request->jobs)->get();
//            if ($request->tb){
//                for($ii = 0; $ii < count($request->tb) ; $ii++) {
//                    $tquestions = questions::where('id',$request->tb[$ii])->first();
//                    if ($e_questinsex) {
//                        $calc = 500 / count($t_questins);
//                    }else{
//                        $calc = 750 / count($t_questins);
//                    }
//                    $applicant->applicants_questions()->attach($tquestions,['type'=>1,'result'=>$calc]);
//                }
//            }else{
//                DB::table('applicants_questions')->where('applicants_id',$applicant->id)->where('type',1)->delete();
//            }
//
//            $e_questins = DB::table('questions')->where('companies_id',$request->company)->where('status',2)->where('applicants_requests_id',$request->jobs)->get();
//            if ($request->eb){
//                for($ii = 0; $ii < count($request->eb) ; $ii++) {
//                    $tquestions = questions::where('id',$request->eb[$ii])->first();
//                    if ($t_questinsex) {
//                        $calc = 250 / count($e_questins);
//                    }else{
//                        $calc = 750 / count($e_questins);
//                    }
//                    $applicant->applicants_questions()->attach($tquestions,['type'=>2,'result'=>$calc]);
//                }
//            }else{
//                DB::table('applicants_questions')->where('applicants_id',$applicant->id)->where('type',2)->delete();
//            }
//
//
//            $applicant->evaluation()->attach();
            return ' ';
        }
    }

//    public function storePapers(Request $request, $id, glcc $glcc)
//    {
//
//        $this->validate($request, [
//            'visa' => 'sometimes|image',
//            'licence' => 'sometimes|image',
//            'graduation' => 'sometimes|image',
//            'medical' => 'sometimes|image',
//        ]);
//        $company = Company::findOrFail($request->company);
//        $visa = $request->visa;
//        $licence = $request->licence;
//        $graduation = $request->graduation;
//        $medical = $request->medical;
//        $paper = papers::findOrFail($id);
//        if ($visa) {
//            $paper->visa = $visa;
//            if ($request->hasFile('visa')) {
//                $paper['visa'] = Up::upload([
//                    'request' => 'visa',
//                    'path' => 'papers/' . $paper->id,
//                    'upload_type' => 'single'
//                ]);
//            }
//        }
//        if ($licence) {
//            $paper->licence = $licence;
//            if ($request->hasFile('licence')) {
//                $paper['licence'] = Up::upload([
//                    'request' => 'licence',
//                    'path' => 'papers/' . $paper->id,
//                    'upload_type' => 'single'
//                ]);
//            }
//        }
//        if ($graduation) {
//            $paper->graduation = $request->graduation;
//            if ($request->hasFile('graduation')) {
//                $paper['graduation'] = Up::upload([
//                    'request' => 'graduation',
//                    'path' => 'papers/' . $paper->id,
//                    'upload_type' => 'single'
//                ]);
//            }
//        }
//        if ($medical) {
//            $paper->medical = $request->medical;
//            if ($request->hasFile('medical')) {
//                $paper['medical'] = Up::upload([
//                    'request' => 'medical',
//                    'path' => 'papers/' . $paper->id,
//                    'upload_type' => 'single'
//                ]);
//            }
//        }
//        $paper->save();
////        $exists = \App\papers::where('id',$id)->where('visa','!=',null)->orWhere('licence','!=',null)->orWhere('graduation','!=',null)->orWhere('medical','!=',null)->orWhere('experience','!=',null)->exists();
////        if ($exists){
////            $ex = $paper->applicant->cc_id;
////        if (empty($ex)){
////            $ccs = $glcc->create([
////                'name_ar'=>$paper->applicant->name_ar,
////                'parent_id'=>$company->cc_id,
////                'levelType'=>2,
////
////            ]);
////
////            if($ccs->parent_id == null){
////                $count = count(DB::table('glccs')->where('parent_id',null)->get());
////                $level_id = levels::where('type',2)->where('levelId',1)->first()->id;
////                DB::table('glccs')->where('id',$ccs->id)->update(['code' => $count,'level_id'=>$level_id]);
////            }else{
////
////                $parent =  glcc::where('id',$ccs->parent_id)->first();
////                if ($parent->levelType != $ccs->levelType){
////                    $ccs->delete();
////                    return back()->with(session()->flash('error',trans('admin.cannot_add_branche')));
////                }else{
////                    $count = count(DB::table('glccs')->where('parent_id',$ccs->parent_id)->where('levelType',$ccs->levelType)->get())-1;
////                    if (levels::where('type',$ccs->levelType)->where('id',$parent->level_id + 1)->exists()){
////                        $level_id = levels::where('type',$ccs->levelType)->where('id',$parent->level_id + 1)->first()->id;
////                        $code = DB::table('glccs')->where('parent_id',$ccs->parent_id)->where('levelType',$ccs->levelType)->max('code');
//////                        there here an issue may be from table or from model
////                        $i = substr($code + 1, -3,1);
////                        if (substr($code + 1, -3) == $i.'00') {
////                            $ccs->delete();
////                            return back()->with(session()->flash('error', trans('admin.cannot_add')));
////                        } else {
////                            $company->cc_id = $ccs->id;
////                            if ($count == null){
////                                $code_first = substr(glcc::where('id',$ccs->parent_id)->where('levelType',$ccs->levelType)->first()->code, 0,1);
////                                DB::table('glccs')->where('id',$ccs->id)->update(['code' => (($code_first.substr($code,1)).'01') ,'level_id'=>$level_id]);
////                            }else{
////                                $co = DB::table('glccs')->where('parent_id',$ccs->parent_id)->where('levelType',$ccs->levelType)->max('code');
////                                DB::table('glccs')->where('id', $ccs->id)->update(['code' => $co + 1, 'level_id' => $level_id]);
////                            }
////                        }
////                    }else{
////                        $ccs->delete();
////                        return back()->with(session()->flash('error',trans('admin.cannot_add')));
////                    }
////                }
////            }
////            DB::table('glccs')->where('id',$ccs->parent_id)->update(['type' => '0']);
////            DB::table('glccs')->where('id',$ccs->id)->update(['type' => '1']);
////            Applicant::where('id',$paper->applicant->id)->update(['cc_id'=>$ccs->id]);
//        $applicant = Applicant::where('id', $paper->applicant->id)->first();
//        $applicant->status = 3;
//        $applicant->save();
//        return redirect(aurl('progress/papers'))->with(session()->flash('message', trans('admin.success_add')));
//    }

//    public function upload_image(Request $request, $id)
//    {
//        if ($request->hasFile('file')) {
//            $fid = Up::upload([
//                'request' => 'file',
//                'path' => 'papers/' . $id,
//                'upload_type' => 'files',
//                'file_type' => 'paper',
//                'relation_id' => $id
//            ]);
//            return response(['status' => true, 'id' => $fid], 200);
//        }
//    }
//
//    public function delete_image(Request $request)
//    {
//        if ($request->has('id')) {
//            Up::delete($request->id);
//        }
//    }

    public function calc(Request $request)
    {
        if ($request->applicant) {
            $applicant = $request->applicant;
            return view('admin.progress.ajax.calc', compact('applicant'))->render();
        }
    }

    public function glcc(Request $request, $id, glcc $glcc)
    {
        if ($request->applicant) {
            $applicant = $request->applicant;
            $applicant_company = applicants_company::where('applicants_id', $applicant)->findOrFail($id);

            $exists = applicants_company::where('id', $id)->where('Candidate_case', '=', 1)->exists();
            if ($exists) {
                $ex = $applicant_company->applicants->cc_id;
                if (empty($ex)) {
                    $ccs = $glcc->create([
                        'name_ar' => $applicant_company->applicants->name_ar,
                        'parent_id' => $applicant_company->companies->cc_id,
                        'levelType' => 2,

                    ]);
                    if ($ccs->parent_id == null) {
                        $count = count(DB::table('glccs')->where('parent_id', null)->get());
                        $level_id = levels::where('type', 2)->where('levelId', 1)->first()->id;
                        DB::table('glccs')->where('id', $ccs->id)->update(['code' => $count, 'level_id' => $level_id]);
                    } else {
                        $parent = glcc::where('id', $ccs->parent_id)->first();
                        if ($parent->levelType != $ccs->levelType) {
                            $ccs->delete();
                            return back()->with(session()->flash('error', trans('admin.cannot_add_branche')));
                        } else {
                            $count = count(DB::table('glccs')->where('parent_id', $ccs->parent_id)->where('levelType', $ccs->levelType)->get()) - 1;
                            if (levels::where('type', $ccs->levelType)->where('id', $parent->level_id + 1)->exists()) {
                                $level_id = levels::where('type', $ccs->levelType)->where('id', $parent->level_id + 1)->first()->id;
                                $code = DB::table('glccs')->where('parent_id', $ccs->parent_id)->where('levelType', $ccs->levelType)->max('code');
//                        there here an issue may be from table or from model
                                $i = substr($code + 1, -3, 1);
                                if (substr($code + 1, -3) == $i . '00') {
                                    $ccs->delete();
                                    return back()->with(session()->flash('error', trans('admin.cannot_add')));
                                } else {
                                    $applicant_company->companies->cc_id = $ccs->id;
                                    if ($count == null) {
                                        $code_first = substr(glcc::where('id', $ccs->parent_id)->where('levelType', $ccs->levelType)->first()->code, 0, 1);
                                        DB::table('glccs')->where('id', $ccs->id)->update(['code' => (($code_first . substr($code, 1)) . '01'), 'level_id' => $level_id]);
                                    } else {
                                        $co = DB::table('glccs')->where('parent_id', $ccs->parent_id)->where('levelType', $ccs->levelType)->max('code');
                                        DB::table('glccs')->where('id', $ccs->id)->update(['code' => $co + 1, 'level_id' => $level_id]);
                                    }
                                }
                            } else {
                                $ccs->delete();
                                return back()->with(session()->flash('error', trans('admin.cannot_add')));
                            }
                        }
                    }
                    DB::table('glccs')->where('id', $ccs->parent_id)->update(['type' => '0']);
                    DB::table('glccs')->where('id', $ccs->id)->update(['type' => '1']);
                    Applicant::where('id', $applicant_company->applicants->id)->update(['cc_id' => $ccs->id]);
                }
            }
        }
    }
}
