<?php

namespace App\Http\Controllers\Admin\progress;

use App\Applicant;
use App\applicants_company;
use App\applicants_requests;
use App\Company;
use App\country;
use App\evaluation;
use App\files;
use App\glcc;
use App\Job;
use App\levels;
use App\papers;
use App\questions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Up;

class FirstProgressController extends Controller
{
    public function index(){
        $applicants = Applicant::where('applicant_status', 0)->pluck('name_'.session('lang'),'id');
        return view('admin.first_progress.index', compact('applicants'),['title'=>trans('admin.motion_detection_filter')]);
    }

//    public function jobs(Request $request){
//        if ($request->ajax()){
//            if ($request->applicants){
//                $applicant = Applicant::where('id',$request->applicants)->first();
//                $jobs = applicants_requests::where('job_id',$applicant->job_id)->where('job_spec_id',$applicant->job_spec_id)->get();
//                $contents = view('admin.first_progress.ajax.jobs', ['applicant'=>$applicant,'jobs' => $jobs])->render();
//                return $contents;
//            }
//        }
//    }

    public function details(Request $request){
        if ($request->ajax()){
            if ($request->applicants){
                $applicant = Applicant::where('id',$request->applicants)->first();
//                $job = $request->jobs;
//                dd($job,$applicant);

                $contents = view('admin.first_progress.ajax.details',compact('applicant'))->render();
                return $contents;
            }
        }
    }


    public function store(Request $request){

        $applicant = Applicant::findOrFail($request->applicants);
//        dd($applicant);
        $this->validate($request,[
            'eval' => 'required',
        ],[],[
            'eval' => trans('admin.evaluation'),
        ]);

        $applicant->update([
            'eval' => $request->eval,
            'meeting_status' => 1,
        ]);

        return redirect(aurl('first_progress'))->with(session()->flash('message',trans('admin.success_add')));
    }
}
