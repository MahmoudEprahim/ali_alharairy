<?php

namespace App\Http\Controllers\Admin\insurance;

use App\DataTables\insuranceDataTable;
use App\Http\Controllers\Controller;
use App\insurance;
use Illuminate\Http\Request;

class InsuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(insuranceDataTable $insurance)
    {
        return $insurance->render('admin.insurance.index',['title'=>trans('admin.Insurance')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.insurance.create',['title'=> trans('admin.create_new_insurance')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,insurance $insurance)
    {
        $data = $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
        ],[],[
            'name_ar' => trans('admin.arabic_name'),
            'name_en' => trans('admin.english_name'),
        ]);
        $insurance->create($data);
        return redirect(aurl('insurances'))->with(session()->flash('message',trans('admin.success_add')));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $insurance = insurance::findOrFail($id);
        return view('admin.insurance.edit',['title'=> trans('admin.edit_insurance') ,'insurance'=>$insurance]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
        ],[],[
            'name_ar' => trans('admin.arabic_name'),
            'name_en' => trans('admin.english_name'),
        ]);
        $insurance = insurance::findOrFail($id);
        $insurance->update($data);
        return redirect(aurl('insurances'))->with(session()->flash('message',trans('admin.success_update')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $insurance = insurance::findOrFail($id);
        $insurance->delete();
        return redirect(aurl('insurances'));
    }
}
