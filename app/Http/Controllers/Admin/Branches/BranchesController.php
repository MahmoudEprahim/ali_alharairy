<?php

namespace App\Http\Controllers\Admin\Branches;

use App\Branches;
use App\DataTables\BranchesDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BranchesController extends Controller
{
    
    public function index(BranchesDataTable $branches)
    {
        return $branches->render('admin.branches.index',['title'=>trans('admin.Branches')]);
    }

    public function create()
    {
        $branches = Branches::count();
        $numberOfBranches = $branches + 1 ;
        return view('admin.branches.create',['title'=> trans('admin.add_new_branches'),'numberOfBranches'=>$numberOfBranches]);
    }

    
    public function store(Request $request,Branches $branches)
    {
        $data = $this->validate($request,[
            'name_ar' => 'sometimes',
            'name_en' => 'required',
            'address' => 'sometimes',
            'type' => 'sometimes',
            'mini_charge' => 'sometimes',
            'location' => 'sometimes',
        ],[],[
            'name_ar' => trans('admin.arabic_name'),
            'name_en' => trans('admin.english_name'),
           
            'type' => trans('admin.type'),
            'mini_charge' => trans('admin.mini_charge'),
        ]);
        $branches->create($data);
        return redirect(aurl('branches'))->with(session()->flash('message',trans('admin.success_add')));
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $branches = Branches::count();
        $numberOfBranches = $branches + 1 ;
        $branches = Branches::findOrFail($id);
        return view('admin.branches.edit',['title'=> trans('admin.edit_branches') ,'branches'=>$branches,'numberOfBranches'=>$numberOfBranches,]);
    }

    
    public function update(Request $request, $id)
    {
        $data = $this->validate($request,[
            'name_ar' => 'sometimes',
            'name_en' => 'required',
            'address' => 'sometimes',
            'type' => 'sometimes',
            'mini_charge' => 'sometimes',
            'location' => 'sometimes',
        ],[],[
            'name_ar' => trans('admin.arabic_name'),
            'name_en' => trans('admin.english_name'),
           
            'type' => trans('admin.type'),
            'mini_charge' => trans('admin.mini_charge'),
        ]);
        $branches = Branches::findOrFail($id);
        $branches->update($data);
        return redirect(aurl('branches'))->with(session()->flash('message',trans('admin.success_update')));
    }

    
    public function destroy($id)
    {
        $branches = Branches::findOrFail($id);
        $branches->delete();
        return redirect(aurl('branches'));
    }

    // activate
    public function branchActive(Request $request){
        if($request->ajax()){
            $branch = Branches::find($request->id);
            if($branch != 'null'){
                if ($branch->active == 0){
                    $branch->update(['active' => 1]);
                    return response()->json(['status'=>1, 'class' => 'btn-success', 'icon' => 'fa-check']);
                } elseif($branch->active == 1){
                    $branch->update(['active' => 0]);
                    return response()->json(['status'=>1,'class' => 'btn-warning', 'icon' => 'fa-close']);
                }

            }
        }
    }

}
