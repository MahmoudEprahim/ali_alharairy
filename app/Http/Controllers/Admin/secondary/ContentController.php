<?php

namespace App\Http\Controllers\Admin\secondary;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


use App\Branches;
use App\Grade;
use App\Unit;
use App\wordlist;

use App\Content;
use App\SubContent;

use App\DataTables\ContentDataTable;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class ContentController extends Controller
{
    public function index(ContentDataTable $content)
    {
        return $content->render('admin.secondary.content.index',['title'=>trans('admin.description_information')]);
    }

    public function create(Content $content)
    {
        $branche = Branches::pluck('name_'.session('lang'),'id');
        $grade = Grade::pluck('name_'.session('lang'),'id');
        $unit = Unit::pluck('name_'.session('lang'),'id');
        $wordlist = wordlist::pluck('name_'.session('lang'), 'id');
        return view('admin.secondary.content.create', ['wordlist' => $wordlist, 'grade' => $grade, 'unit'=> $unit,'branche'=> $branche, 'title'=> trans('admin.Create_new_content')]);

    }

    public function store(Request $request, Content $content)
    {
      $data =  $this->validate($request,[
            'title' => 'sometimes',
            'description_ar' => 'sometimes',
            'description_en' => 'sometimes',
            'branches_id' => 'sometimes',
            'grade_id' => 'required',
            'type_list' => 'required',
            'units_id' => 'required',
            // subcontent
            'video_src' => 'sometimes',
            'video_desc' => 'sometimes',
            'content_id' => 'sometimes',
        ],[],[
            'title' => trans('admin.title'),
            'description_ar' => trans('admin.description_ar'),
            'description_en' => trans('admin.description_en'),
            'type_list' => trans('admin.type_list'),
            'branches_id' => trans('admin.branches_id'),
            'grade_id' => trans('admin.grade_id'),
            'units_id' => trans('admin.units_id'),

        ]);
        
        $content = Content::create([
            'description_en' => $request->description_en,
            'grade_id' => $request->grade_id,
            'type_list' => $request->type_list,
            'units_id' => $request->units_id,
        ]);

        // $video_srcs = $request->input('video_src');
        $video_desc = $request->input('video_desc');

        $embedvideo = str_replace('watch?v=', 'embed/', $request->video_src);
        $data['video_src'] = $embedvideo;
        
        foreach ($embedvideo as  $key => $video_src)
        {
            $subcontent = new SubContent();
            $subcontent->video_src = $embedvideo[$key];
            $subcontent->video_desc = $video_desc[$key];
            $subcontent->content_id = $content->id;
            $subcontent->save();
            
        }
        return redirect(aurl('setting/content'))->with(session()->flash('message',trans('admin.success_add')));
    }

    public function show($id)
    {
        $content = Content::findORfail($id);

        return view('admin.secondary.content.show', ['content' => $content,'title'=> trans('admin.show_secondary_content')]);

    }

   public function edit($id)
   {
       $branche = Branches::pluck('name_'.session('lang'),'id');
       $grade = Grade::pluck('name_'.session('lang'),'id');

       $unit = Unit::pluck('name_'.session('lang'),'id');
       $wordlist = wordlist::pluck('name_'.session('lang'), 'id');
       $content = Content::findOrFail($id);

        return view('admin.secondary.content.edit', compact('grade','branche','content','unit','wordlist'), ['title'=> trans('admin.edit_content')]);
   }

    public function update(Request $request, $id)
    {

       $content = Content::findOrFail($id);
       $data = $this->validate($request,[

            'title' => 'sometimes',
            'description_ar' => 'sometimes',
            'description_en' => 'sometimes',
            'media' => 'sometimes',
            'video'=>'sometimes',
            'branches_id' => 'sometimes',
            'grade_id' => 'required',
            'type_list' => 'required',
            'units_id' => 'required',
        ],[],[
            'title' => trans('admin.title'),
            'media' => trans('admin.media'),
            'description_ar' => trans('admin.description'),
            'description_en' => trans('admin.description'),
            'branches_id' => trans('admin.branches_id'),
            'grade_id' => trans('admin.grade_id'),
            'units_id' => trans('admin.units_id'),
            'type_list' => trans('admin.type_list'),

        ]);


        $content->update($data);

        return redirect(aurl('setting/content'))->with(session()->flash('message',trans('admin.success_update')));

    }


    public function destroy($id)
    {
        $content = Content::findOrFail($id);
        $content->delete();
        return redirect(aurl('setting/content'));
    }


    public function content_grammar(Request $request)
    {

        if($request->ajax()){
            $grade_id = $request->grade_id;
            $type_list = $request->type_list;
 
            if($grade_id != null){
                if($grade_id == 1){
                    $unit  = Unit::where('grade_id','1')->pluck('name_en','id');
                    $data = view('admin.secondary.content.ajax.index', ['type_list' => $type_list, 'grade_id'=>$grade_id,'unit'=> $unit])->render();
                    return $data;
                }
                if($grade_id == 2){
                    $unit  = Unit::where('grade_id','2')->pluck('name_en','id');
                    $data = view('admin.secondary.content.ajax.index', ['type_list' => $type_list, 'grade_id'=>$grade_id,'unit'=> $unit])->render();
                    return $data;
                }
                if($grade_id == 3){
                    $unit  = Unit::where('grade_id','3')->pluck('name_en','id');
                    $data = view('admin.secondary.content.ajax.index', ['type_list' => $type_list, 'grade_id'=>$grade_id,'unit'=> $unit])->render();
                    return $data;
                }


            }
        }

    }

}
