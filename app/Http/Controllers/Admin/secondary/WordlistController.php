<?php

namespace App\Http\Controllers\Admin\secondary;

use App\Branches;
use App\Grade;
use App\Unit;
use App\wordlist;
use App\DataTables\wordlistDataTable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WordlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(wordlistDataTable $wordlist)
    {
            return $wordlist->render('admin.secondary.wordlist.index',['title'=>trans('admin.word_grammar_information')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branche = Branches::pluck('name_'.session('lang'),'id');
        $unit = Unit::pluck('name_'.session('lang'),'id');
        $grade = Grade::pluck('name_'.session('lang'),'id');

        return view('admin.secondary.wordlist.create', ['branche' => $branche,'grade' => $grade,'unit' => $unit,'title'=> trans('admin.create_word_list')]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $data= $this->validate($request,[
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'units_id' => 'sometimes',
            'branches_id' => 'sometimes',
            'grade_id'=> 'sometimes',
            'type'=> 'sometimes',
        ],[],[
            // 'name_ar'=> trans('admin.unit_name_ar'),
            // 'name_en' => trans('admin.unit_name_en'),
            // 'type' => trans('admin.type'),
        ]);
        $wordlist = new wordlist();
        $wordlist->create($data);
        return redirect(aurl('setting/wordlist'))->with(session()->flash('message',trans('admin.success_add')));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\wordlist  $wordlist
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $wordlist = WordList::findOrFail($id);
        return view('admin.secondary.wordlist.show', ['wordlist' => $wordlist, 'title'=> trans('admin.create_word_list')]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\wordlist  $wordlist
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $wordlist = WordList::findOrFail($id);
        
        $branche = Branches::pluck('name_'.session('lang'),'id');
        $unit = Unit::pluck('name_'.session('lang'),'id');
        $grade = Grade::pluck('name_'.session('lang'),'id');

        return view('admin.secondary.wordlist.edit', ['wordlist' => $wordlist,'grade' => $grade, 'branche' => $branche,'unit' => $unit,'title'=> trans('admin.edit_word_grammar_list')]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\wordlist  $wordlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data= $this->validate($request,[
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'units_id' => 'sometimes',
            'branches_id' => 'sometimes',
            'grade_id' => 'sometimes',
            'type' => 'sometimes',
        ],[],[
            // 'name_ar'=> trans('admin.unit_name_ar'),
            // 'name_en' => trans('admin.unit_name_en'),
            // 'units_id' => trans('admin.units_id'),
            // 'branches_id' => trans('admin.branche_id'),
            // 'grade_id' => trans('admin.grade_id'),
            // 'type' => trans('admin.type'),
        ]);
        $wordlist = WordList::findOrFail($id);
        
        $wordlist->update($data);
        return redirect(aurl('setting/wordlist'))->with(session()->flash('message',trans('admin.success_update')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\wordlist  $wordlist
     * @return \Illuminate\Http\Response
     */
    public function destroy(wordlist $wordlist)
    {
        $wordlist->delete();
        return back();
    }
    public function wordGrammar(Request $request)
    {

        if($request->ajax()){
            $type_list = $request->type_list;
            $data = view('admin.secondary.wordlist.word_list', ['type_list'=>$type_list])->render();
            return $data;


        }

        //
    }

    }
