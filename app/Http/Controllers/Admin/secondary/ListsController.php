<?php

namespace App\Http\Controllers\Admin\secondary;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DataTables\ListsDataTable;

use App\SecondaryList;
use App\Grade;
use App\Unit;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class ListsController extends Controller
{
    public function index(ListsDataTable $list) 
    { 
        return $list->render('admin.secondary.lists.index',['title'=>trans('admin.lists_information')]);
    } 

    public function create(SecondaryList $list)
    {
        $grade = Grade::pluck('grade_name_'.session('lang'),'id');       
        $unit = Unit::pluck('unit_name_'.session('lang'),'id');       
        return view('admin.secondary.lists.create', ['title'=> trans('admin.Create_new_list'),'grade'=> $grade, 'unit'=> $unit]);
    }

    public function store(Request $request, SecondaryList $list)
    {

        
        $this->validate($request,[
            'list_name_ar' => 'required',
            'list_name_en' => 'required',
            'grade_id' => 'required',
            'unit_id' => 'required',
            
            
        ],[],[
            'list_name_ar'=> trans('admin.list_name_ar'),
            'list_name_en' => trans('admin.list_name_en'),
            'grade_id' => trans('admin.grade_name'),
            'unit_id' => trans('admin.unit_id'),
            
        ]);

        $list = new SecondaryList();
        $list->list_name_ar = $request->list_name_ar;
        $list->list_name_en = $request->list_name_en;
        $list->grade_id = $request->grade_id;          
        $list->unit_id = $request->unit_id;          
        $list->save();
        
           
        
        return redirect(aurl('lists'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
        $grade = Grade::pluck('grade_name_'.session('lang'),'id');
        $list = SecondaryList::findOrFail($id);
    
        return view('admin.secondary.list.show',compact('list', 'grade'));
    }

    public function edit($id)
    {
        $grade = Grade::pluck('grade_name_'.session('lang'),'id');
        $unit = Unit::pluck('unit_name_'.session('lang'),'id');
        $list = SecondaryList::findOrFail($id);
        return view('admin.secondary.lists.edit',['list'=>$list,'grade'=> $grade,'unit'=>$unit,'title'=>trans('admin.edit_list')]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'list_name_ar'=> 'required',
            'list_name_en' => 'required',
            'grade_id' => 'required',
            'unit_id' => 'required',
        ],[],[
            'list_name_ar'=> trans('admin.list_name_ar'),
            'list_name_en' => trans('admin.list_name_en'),
            'grade_id' => trans('admin.grade_id'),
            'unit_id' => trans('admin.unit_id'),
        ]);

        $list = SecondaryList::findOrFail($id);
  
        
        $list->list_name_ar = $request->list_name_ar;
        $list->list_name_en = $request->list_name_en;
        $list->grade_id = $request->grade_id;
        $list->unit_id = $request->unit_id;
        
        $list->save();
        return redirect(aurl('lists'))->with(session()->flash('message',trans('admin.success_update')));
    }


    public function destroy($id)
    {
        $list = SecondaryList::findOrFail($id);
        $list->delete();
        return redirect(aurl('lists'));
    }
}
