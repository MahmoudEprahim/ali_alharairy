<?php

namespace App\Http\Controllers\Admin\secondary;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Grade;
use App\Chapter;
use App\Listt;
use App\Content;


class SecondaryGatesController extends Controller
{
    public function grades(){
        $grades = Grade::all();
        return view('admin.secondary.secondarygrades', compact('grades'));
      }
  
      public function lists(){
        $grades_id = Input::get('grade_id');
        $lists = Listt::where('grade_id', '=', $grades_id)->get();
        return response()->json($lists);
      }
  
      public function chapters(){ 
        
        $lists_id = Input::all();
        dd($lists_id);
        $chapters = Chapter::where('list_id', '=', $lists_id)->get();
        return response()->json($chapters);
      }
  
      public function contents(){

        $chapters_id = Input::get('chapter_id');
        $contents = Content::where('chapter_id', '=', $chapters_id)->get();
        return response()->json($contents);
      }
}
