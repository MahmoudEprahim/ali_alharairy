<?php

namespace App\Http\Controllers\Admin\secondary;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Branches;
use App\Unit;
use App\Grade;


use App\DataTables\UnitsDataTable;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class UnitsController extends Controller
{
    public function index(UnitsDataTable $unit)
    {
        return $unit->render('admin.secondary.units.index',['title'=>trans('admin.units_information')]);
    }

    public function create(Unit $unit)
    {
        $grade = Grade::pluck('name_'.session('lang'),'id');
        // $branche = Branches::pluck('name_'.session('lang'),'id');

        return view('admin.secondary.units.create', ['grade' => $grade,'title'=> trans('admin.Create_new_chapter')]);
    }

    public function store(Request $request, Unit $unit)
    {


         $data= $this->validate($request,[
            'name_ar' => 'sometimes',
            'name_en' => 'required',
            'grade_id' => 'required',
            'branches_id' => 'sometimes',
        ],[],[
            'name_ar'=> trans('admin.unit_name_ar'),
            'name_en' => trans('admin.unit_name_en'),
            'grade_id' => trans('admin.grade_id'),
            // 'branches_id' => trans('admin.list_id'),
        ]);

        $unit->create($data);




        return redirect(aurl('setting/units'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {

        $unit = Unit::findOrFail($id);

        return view('admin.secondary.units.show',compact('unit'));
    }

    public function edit($id)
    {

        $branche = Branches::pluck('name_'.session('lang'),'id');
        $grade = Grade::pluck('name_'.session('lang'),'id');
        $unit = Unit::findOrFail($id);
        return view('admin.secondary.units.edit',['unit'=> $unit,'grade'=>$grade, 'branche'=> $branche,'title'=>trans('admin.edit_unit')]);
    }

    public function update(Request $request, $id)
    {
        $unit = Unit::findOrFail($id);
        $data = $this->validate($request,[
            'name_ar' => 'sometimes',
            'name_en' => 'required',
            'grade_id' => 'required',
            'branches_id' => 'sometimes',
        ],[],[
            
        ]);

        if($data != null){
            $unit->update($data);

        return redirect(aurl('setting/units'))->with(session()->flash('message',trans('admin.success_update')));
        }

        return redirect(aurl('setting/units'));

        }


    public function destroy($id)
    {
        $unit = Unit::findOrFail($id);
        $unit->delete();
        return redirect(aurl('setting/units'));
    }
}
