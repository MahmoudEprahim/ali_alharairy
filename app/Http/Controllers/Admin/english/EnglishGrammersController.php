<?php

namespace App\Http\Controllers\Admin\english;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



use App\EnglishGrammer;
use App\EnglishList;
use App\Branches;

use App\DataTables\EnglishGrammersDataTable; 


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;


class EnglishGrammersController extends Controller
{
    public function index(EnglishGrammersDataTable $content) 
    {  
        return $content->render('admin.english.grammer.index',['title'=>trans('admin.english_contents_information')]);
    } 

    public function create()
    {
        $word =  EnglishGrammer::pluck('id');
        $list = EnglishList::pluck('name_'.session('lang'),'id');  
        $branch = Branches::pluck('name_'.session('lang'),'id');
        
        return view('admin.english.grammer.create', ['word' => $word, 'list' => $list,'branch' => $branch,'title'=> trans('admin.add_new_content')]);
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'name_en' => 'sometimes',
            'name_ar' => 'sometimes',
            'grammer_content' => 'required',
            'grammer_content_ar' => 'sometimes',
            'branch_id' => 'required',
            'list_id' => 'required',
            'EnglishGate' => 'required',
        ],[],[
            'grammer_content' => trans('admin.grammer_content'),
            'grammer_content_ar' => trans('admin.grammer_content'),
            'name_en' => trans('admin.grammer_name'),
            'name_ar' => trans('admin.grammer_name'),
            'branch_id' => trans('admin.branch_id'),
            'list_id' => trans('admin.list_id'),
            'EnglishGate' => trans('admin.EnglishGate'),
        ]);
        

        

        $grammernames = $request->input('name_en');
        $grammernamear = $request->input('name_ar');
        $grammercontent = $request->input('grammer_content');
        $grammercontentar = $request->input('grammer_content_ar');
        

        foreach ($grammernames as  $key => $grammername)
        {
            $englishgrammer = new EnglishGrammer();
            $englishgrammer->name_en = $grammername;
            $englishgrammer->name_ar = $grammernamear[$key];
            $englishgrammer->grammer_content = $grammercontent[$key];
            $englishgrammer->grammer_content_ar = $grammercontentar[$key];

            $englishgrammer->branch_id = $request->branch_id;
            $englishgrammer->list_id = $request->list_id;
            $englishgrammer->EnglishGate = $request->EnglishGate;
            $englishgrammer->save();
            
        }
        
        
        return redirect(aurl('englishgrammer'))->with(session()->flash('message',trans('admin.success_add')));
            
    }

    public function show($id)
    {
        $content = EnglishGrammer::findOrFail($id);
        return view('admin.english.grammer.show',compact('content'));
    }

    public function edit($id)
    {
        $content =  EnglishGrammer::findOrFail($id);
        $list = EnglishList::pluck('name_'.session('lang'),'id');  
        $branch = Branches::pluck('name_'.session('lang'),'id');

       
        return view('admin.english.grammer.edit',['content'=> $content, 'list' => $list,'branch'=> $branch, 'title'=>trans('admin.edit_english_content')]);
    }

    public function update(Request $request, $id)
    {
        $content = EnglishGrammer::findOrFail($id);
        $this->validate($request,[
            'name_en' => 'sometimes',
            'name_ar' => 'sometimes',
            'grammer_content' => 'required',
            'grammer_content_ar' => 'sometimes',
            'branch_id' => 'required',
            'list_id' => 'required',
            'EnglishGate' => 'required',
        ],[],[
            'grammer_content' => trans('admin.grammer_content'),
            'grammer_content_ar' => trans('admin.grammer_content'),
            'name_en' => trans('admin.grammer_name'),
            'name_ar' => trans('admin.grammer_name'),
            'branch_id' => trans('admin.branch_id'),
            'list_id' => trans('admin.list_id'),
            'EnglishGate' => trans('admin.EnglishGate'),
        ]);

        
        $content->branch_id = $request->branch_id;
        $content->list_id = $request->list_id;       
        $content->EnglishGate = $request->EnglishGate; 
        $content->name_en = $request->name_en;
        $content->name_ar = $request->name_ar;
        $content->grammer_content = $request->grammer_content;
        $content->grammer_content_ar = $request->grammer_content_ar;
              
                      
        $content->save();
        
        return redirect(aurl('englishgrammer'))->with(session()->flash('message',trans('admin.success_update')));

    }


    public function destroy($id)
    {
        $enlishwords = EnglishGrammer::findOrFail($id);
        $enlishwords->delete();
        return redirect(aurl('englishgrammer'));
    }


    public function english_content(Request $request)
    {
       
        if($request->ajax()){
            $branch = $request->branche_id;
            $EnglishGate = $request->EnglishGate;
            if($branch != null && $EnglishGate != null){
                if($EnglishGate == 1){
                    $branch = $request->branche_id;
                    $list  = EnglishList::where('EnglishGate','1')->pluck('name_ar','id');
                    $data = view('admin.english.grammer.ajax.index', ['list'=> $list, 'branch' => $branch,'EnglishGate'=>$EnglishGate])->render();
                    return $data;
                }

                // if($EnglishGate == 1 Or $EnglishGate == 3){
                //     $branch = $request->branche_id;
                //     $list  = EnglishList::where('EnglishGate','1')->pluck('name_ar','id');
                //     $data = view('admin.english.words.ajax.index', ['list'=> $list,'branch' => $branch,'EnglishGate'=>$EnglishGate])->render();
                //     return $data;
                // }

                if($EnglishGate == 2){
                    $branch = $request->branche_id;
                    $list  = EnglishList::where('EnglishGate','2')->pluck('name_ar','id');
                    $data = view('admin.english.listening.ajax.index', ['list'=> $list,'branch' => $branch,'EnglishGate'=>$EnglishGate])->render();
                    return $data;
                }
            }
        }

    }
}
