<?php

namespace App\Http\Controllers\Admin\english;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



use App\EnglishDescription;
use App\EnglishList;

use App\DataTables\EnglishDescriptionDataTable; 


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;


class EnglishDescriptionCntoller extends Controller
{
    public function index(EnglishDescriptionDataTable $content) 
    { 
        return $content->render('admin.english.content.index',['title'=>trans('admin.english_contents_information')]);
    } 

    public function create(EnglishDescriptionDataTable $content)
    {
        $content = EnglishDescription::pluck('id');
        $list =  EnglishList::pluck('name_'.session('lang'),'id');
        
        return view('admin.english.content.create', ['content' => $content, 'list' => $list,'title'=> trans('admin.Create_new_chapter')]);
    }

    public function store(Request $request, EnglishDescription $content)
    {

        $this->validate($request,[
            'title' => 'sometimes',
            'content' => 'sometimes',
            'word' => 'sometimes',
            'maeaning' => 'sometimes',
            'image' => 'sometimes',
            'list_id' => 'required',
            
        ],[],[
            'title' => trans('admin.title'),
            'content' => trans('admin.description'),
            'word' => trans('admin.word'),
            'maeaning' => trans('admin.maeaning'),
            'image' => trans('admin.image'),
            'list_id' => trans('admin.list_id'),
        ]);
            
        
        $content->title = $request->title;
        $content->content = $request->content;
        $content->word = $request->word;
        $content->maeaning = $request->maeaning;
        $content->image = $request->image;
        $content->list_id = $request->list_id; 
        
        
        $content->save();
        
        return redirect(aurl('englishdescription'))->with(session()->flash('message',trans('admin.success_add')));
            
    }

    public function show($id)
    {
        
        $content = EnglishDescription::findOrFail($id);
      
        return view('admin.english.content.show',compact('content'));
    }

    public function edit($id)
    {
        
        $content = EnglishDescription::findOrFail($id);
        $list =  EnglishList::pluck('name_'.session('lang'),'id');
       
        return view('admin.english.content.edit',['content'=> $content, 'list'=> $list, 'title'=>trans('admin.edit_content')]);
    }

    public function update(Request $request, $id)
    {
        $content = EnglishDescription::findOrFail($id);
        $this->validate($request,[
            'title' => 'sometimes',
            'content' => 'sometimes',
            'word' => 'sometimes',
            'maeaning' => 'sometimes',
            'image' => 'sometimes',
            'list_id' => 'required',
            
        ],[],[
            'title' => trans('admin.title'),
            'content' => trans('admin.description'),
            'word' => trans('admin.word'),
            'maeaning' => trans('admin.maeaning'),
            'image' => trans('admin.image'),
            'list_id' => trans('admin.list_id'),
        ]);
            
        
        $content->title = $request->title;
        $content->content = $request->content;
        $content->word = $request->word;
        $content->maeaning = $request->maeaning;
        $content->image = $request->image;
        $content->list_id = $request->list_id;       
                      
        $content->save();
        
        return redirect(aurl('englishdescription'))->with(session()->flash('message',trans('admin.success_add')));

    }


    public function destroy($id)
    {
        $grade = EnglishDescription::findOrFail($id);
        $grade->delete();
        return redirect(aurl('englishdescription'));
    }
}
