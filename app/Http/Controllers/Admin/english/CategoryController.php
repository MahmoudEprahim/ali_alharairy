<?php

namespace App\Http\Controllers\Admin\english;
use App\Attendance;
use App\DataTables\categoriesDataTable;
use App\Http\Controllers\Controller;
use App\Student;
use App\SubCategory;
use App\SubCategoryChild;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\EnglishCategory;

class CategoryController extends Controller
{

    public function index(categoriesDataTable $dataTable)
    {
        return  $dataTable->render('admin.english.category.index');
    }


    public function create()
    {
        return  view('admin.english.category.create');
    }


    public function store(Request $request)
    {

        $this->validate($request, [
            'cat_name_en' => 'required',
            'sub_cat_name_en' => 'required|array',
            'EnglishGate' => 'required',
            'sub_cat_child' => 'sometimes',
            'content' => 'sometimes',
        ]);

        if ($request)

        $category = EnglishCategory::create([
            'cat_name_en' => $request->cat_name_en,
            'EnglishGate' => $request->EnglishGate
        ]);

        foreach ($request->sub_cat_name_en as $subCat) {
            if (empty($subCat)){
                continue;
            }
            SubCategory::create([
                'sub_cat_name_en' => $subCat,
                'cat_id' => $category->id,
            ]);
        }

        return back()->with('success', 'New Category added successfully.');

    }

    public function show($id)
    {
        $attendance = Attendance::findOrFail($id);
        return view('admin.students.attendance.show',compact('attendance'));
    }


    public function edit($id)
    {
        $allCategories = EnglishCategory::findOrFail($id);
        return view('admin.english.category.edit',compact(['allCategories']));
    }


    public function update(Request $request, $id)
    {
        $allCategories = EnglishCategory::findOrFail($id);
        $this->validate($request, [
            'cat_name_en' => 'required',
            'sub_cat_name_en.*' => 'required',
            'EnglishGate' => 'required',
            'sub_cat_child' => 'sometimes',
            'content' => 'sometimes',
        ]);

        $allCategories->update([
            'cat_name_en' => $request->cat_name_en,
            'EnglishGate' => $request->EnglishGate
        ]);

        if (count($allCategories->subCategories) == count($request->sub_cat_name_en)){
            foreach($request->sub_cat_name_en as $key=>$subs) {
                $allCategories->subCategories[$key]->update([
                    'sub_cat_name_en' => $subs,
                ]);
            }
        }

        elseif (count($allCategories->subCategories) < count($request->sub_cat_name_en)){
            $sub_cat_name_en = $request->sub_cat_name_en;
            foreach($allCategories->subCategories as $key=>$subs) {
                array_shift($sub_cat_name_en);
            }
            foreach ($sub_cat_name_en as $sub) {
                SubCategory::create([
                    'sub_cat_name_en' => $sub,
                    'cat_id' => $id,
                ]);
            }

        }

        return redirect()->route('category.index')->with('success', 'New Category updated successfully.');

    }

    public function destroy(Request $request, $id)
    {
        $attendance = EnglishCategory::findOrFail($id);
        $attendance->delete();
        return redirect()->route('category.index')->with('success', 'New Category deleted successfully.');
    }

    public function editSubCat(Request $request)
    {
        if($request->ajax() && ($request->subCatId && $request->subCatName)){
            SubCategory::find($request->subCatId)->update(
                [
                    'sub_cat_name_en' => $request->subCatName
                ]);
        }
    }

    public function deleteSubCat(Request $request){
        if($request->ajax() && $request->subCatId){
            SubCategory::find($request->subCatId)->delete();
        }
    }

    public function createTree()
    {
        $categories = EnglishCategory::all();

        return view('admin.english.category.categoryTreeview',compact(['categories']));
    }

    public function getSubCategories(Request $request)
    {
       if ($request->ajax() && $request->cat_id){
           $subCategories = SubCategory::where('cat_id', $request->cat_id)->get();
           return view('admin.english.category.getSubCategories',compact('subCategories'));
       }

    }

    public function getSubCategoryChild(Request $request)
    {
        if ($request->ajax() && ($request->sub_cat_id && $request->cat_id)){

            $SubCategoryChild = SubCategoryChild::where('sub_cat_id', $request->sub_cat_id)
                ->where('cat_id', $request->cat_id)
                    ->get();
            return view('admin.english.category.getSubCategoryChild',compact('SubCategoryChild'));
        }

    }


    public function addContent(Request $request)
    {
        $data = $this->validate($request, [
            'child_name_en' => 'sometimes',
            'bodyContent' => 'required',
            'cat_id' => 'required',
            'sub_cat_id' => 'required',

        ], [], [
            'child_name_en' => trans('admin.child_name_en'),
            'bodyContent' => trans('admin.content'),
            'cat_id' => trans('admin.cat_id'),
            'sub_cat_id' => trans('admin.parent'),
        ]);

        if ($request->sub_cat_child){
            SubCategoryChild::find($request->sub_cat_child)->update(['bodyContent' => $request->bodyContent]);
        } else {
            SubCategoryChild::create($data);
        }
        return back()->with('message', 'New content added successfully.');
    }

}
