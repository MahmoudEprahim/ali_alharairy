<?php

namespace App\Http\Controllers\Admin\english;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



use App\EnglishWords;
use App\EnglishList;
use App\Branches;

use App\DataTables\EnglishWordsDataTable; 


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;


class EnglishWordsController extends Controller
{
    public function index(EnglishWordsDataTable $word) 
    {  
        return $word->render('admin.english.words.index',['title'=>trans('admin.english_contents_information')]);
    } 

    public function create()
    {
        $word =  EnglishWords::pluck('id');
        $list = EnglishList::pluck('name_'.session('lang'),'id');  
        $branch = Branches::pluck('name_'.session('lang'),'id');
        
        return view('admin.english.words.create', ['word' => $word, 'list' => $list,'branch' => $branch,'title'=> trans('admin.Create_new_english_word')]);
    }

    public function store(Request $request)
    {

        
        $this->validate($request,[
           
            'word' => 'required',
            'meaning' => 'required',
            'branch_id' => 'required',
            'list_id' => 'required',
            'EnglishGate' => 'required',
           
            
        ],[],[
            'word' => trans('admin.word'),
            'meaning' => trans('admin.meaning'),
            'branch_id' => trans('admin.branch_id'),
            'list_id' => trans('admin.list_id'),
            'EnglishGate' => trans('admin.EnglishGate'),
        ]);

        $words = $request->input('word');
        $meaning = $request->input('meaning');

        foreach ($words as  $key => $word)
        {
            $englishwords = new EnglishWords();
            $englishwords->word = $word;
            $englishwords->meaning = $meaning[$key];
            $englishwords->branch_id = $request->branch_id;
            $englishwords->list_id = $request->list_id;
            $englishwords->EnglishGate = $request->EnglishGate;
            $englishwords->save();
            
        }
        
        
        return redirect(aurl('englishwords'))->with(session()->flash('message',trans('admin.success_add')));
            
    }

    public function show($id)
    {
        $content = EnglishWords::findOrFail($id);
        return view('admin.english.words.show',compact('content'), ['title'=>trans('admin.show_english_content')]);
    }

    public function edit($id)
    {
        $content =  EnglishWords::findOrFail($id);
        $list = EnglishList::pluck('name_'.session('lang'),'id');  
        $branch = Branches::pluck('name_'.session('lang'),'id');

       
        return view('admin.english.words.edit',['content'=> $content, 'list' => $list,'branch'=> $branch, 'title'=>trans('admin.edit_english_content')]);
    }

    public function update(Request $request, $id)
    {
        $content = EnglishWords::findOrFail($id);
        $this->validate($request,[
            'word' => 'sometimes',
            'meaning' => 'sometimes',
            'list_id' => 'required',
            'branch_id' => 'required',
            'EnglishGate' => 'required',
            
        ],[],[
            'word' => trans('admin.word'),
            'meaning' => trans('admin.maeaning'),
            'list_id' => trans('admin.list_id'),
            'branch_id' => trans('admin.branch_id'),
            'EnglishGate' => trans('admin.EnglishGate'),
        ]);
        
        $content->word = $request->word;
        $content->meaning = $request->meaning;
        $content->branch_id = $request->branch_id;
        $content->list_id = $request->list_id;       
        $content->EnglishGate = $request->EnglishGate;       
                      
        $content->save();
        
        return redirect(aurl('englishwords'))->with(session()->flash('message',trans('admin.success_update')));

    }


    public function destroy($id)
    {
        $enlishwords = EnglishWords::findOrFail($id);
        $enlishwords->delete();
        return redirect(aurl('englishwords'));
    }
}
