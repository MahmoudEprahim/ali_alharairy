<?php

namespace App\Http\Controllers\Admin\chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DataTables\CommentsDataTabelDataTable;

use App\User;
use App\Post;
use App\Comment;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class CommentsController extends Controller
{
    public function index(CommentsDataTabelDataTable $comment)
    {

        return $comment->render('admin.chat.comments.index',['title'=>trans('admin.comments_information')]);
    }

    public function create(Comment $comment)
    {
        $user = User::pluck('name','id');
        $post = Post::pluck('id');
        $comment = Comment::pluck('body','id');
        return view('admin.chat.comments.create', ['user' => $user, 'post' => $post,'title'=> trans('admin.create_new_comment')]);
    }

    public function store(Request $request, Comment $comment)
    {
        $this->validate($request,[
            'body' => 'required',
            'image' => 'sometimes',
            'user_id' => 'required',
            'post_id' => 'required',

        ],[],[
            'body'=> trans('admin.body'),
            'user_id' => trans('admin.user_id'),
            'post_id' => trans('admin.post_id'),
        ]);
            $comment = new Comment();
            $comment->body = $request->body;
            $comment->post_id = $request->post_id;
            $comment->user_id = $request->user_id;


            if($request->hasFile('image')){
                $comment['image'] = up::upload(
                    [
                        'request' => 'image',
                        'path' => 'comments',
                        'upload_type' => 'single',
                    ]);
            }
            $comment->save();

        return redirect(aurl('comments'))->with(session()->flash('message',trans('admin.success_add')));

    }
    public function show($id)
    {
        $comment = Comment::findOrFail($id);


        return view('admin.chat.comments.show', ['comment' => $comment, 'title'=> trans('admin.create_new_comment')]);
    }

    public function reply($id,Request $request)
    {

        $comment = Comment::findOrFail($id);
        $comment->reply = $request->reply;
        $comment->save();

        return redirect(aurl('comments'))->with(session()->flash('message',trans('admin.success_add')));
    }
    public function edit($id)
    {
        $user = User::pluck('name','id');
        $comment = Comment::pluck('body','id');
        $comment = Comment::findOrFail($id);
        return view('admin.chat.comments.edit',['comment'=>$comment, 'user'=> $user, 'message'=> $comment,'title'=>trans('admin.edit_comment')]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'body' => 'required',
            'image' => 'sometimes',
            'user_id' => 'required',
            'message_id' => 'required',

        ],[],[
            'body'=> trans('admin.body'),
            'user_id' => trans('admin.user_id'),
            'message_id' => trans('admin.message_id'),
        ]);
            $comment = Comment::findOrFail($id);
            $comment->body = $request->body;
            $comment->user_id = $request->user_id;
            $comment->message_id = $request->message_id;

            if($request->hasFile('image')){
                $comment['image'] = up::upload(
                    [
                        'request' => 'image',
                        'path' => 'comments',
                        'upload_type' => 'single',
                        'delete_file' => $comment->image,
                    ]);
            }

            $comment->save();

        return redirect(aurl('comments'))->with(session()->flash('message',trans('admin.success_add')));


        }


    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return redirect(aurl('comments'));
    }
}
