<?php

namespace App\Http\Controllers\Admin\chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



use App\LibraryBook;
use App\User;
use App\DataTables\UsersDataTable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;


class UsersController extends Controller
{
    

    public function index(UsersDataTable $user)
    {
       
        return $user->render('admin.chat.users.index',['title'=>trans('admin.users_infomation')]);
    }

    public function create(User $user)
    {
        return view('admin.chat.users.create', ['title'=> trans('admin.Create_new_user'),'user' => $user]);
    }

    public function store(Request $request, User $user)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'image' =>  'sometimes',
        ],[],[
            'name'=> trans('admin.name'),
            'email' => trans('admin.email'),
            'password' => trans('admin.password'),
            'image' =>  trans('admin.image'),
        ]);

        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password =  Hash::make($request->password);
           
        if($request->hasFile('image')){
            $user['image'] = Up::upload([
                'request' => 'image',
                'path'=>'users',
                'upload_type' => 'single', 
            ]);
        }

        $user->save();
        return redirect(aurl('users'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
        $user = User::findOrFail($id);
    
        return view('admin.chat.users.show',compact('user'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        
        return view('admin.chat.users.edit',['user'=>$user,'title'=>trans('admin.edit_user')]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'sometimes',
            'email' => 'sometimes',
            'password' => 'sometimes',
            'image' =>  'sometimes',
        ],[],[
            'name'=> trans('admin.name'),
            'email' => trans('admin.email'),
            
        ]);

        $user = User::findOrFail($id);

       $user->name = $request->name;
        $user->email = $request->email;

        if(isset($user->password) && $user->password != ""){
            $user->password =  Hash::make($request->password);
        }else{
            unset($requestArray['password']);
        }

           
        if($request->hasFile('image')){
            $user['image'] = Up::upload([
                'request' => 'image',
                'path'=>'users',
                'upload_type' => 'single',
                'delete_file' => $user->image, 
            ]);
        }

        
        $user->save();
        
        return redirect(aurl('users'))->with(session()->flash('message',trans('admin.success_update')));


    }


    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect(aurl('users'));
    }
}
