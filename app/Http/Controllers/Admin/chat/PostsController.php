<?php

namespace App\Http\Controllers\Admin\chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

use App\DataTables\PostDataTable;

use App\Admin;
use App\Post;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class PostsController extends Controller
{
    public function index(PostDataTable $post)
    {


        $last = DB::table('posts')->latest()->first();

      if($last != null)
      {
          $lastposttime = $last->updated_at; //updted at time
          $date = date('Y-m-d h:i:s'); // current time
          $diff = strtotime($date) - strtotime($lastposttime);
          $hours = date('h:i:s',$diff);
          return $post->render('admin.chat.posts.index',['title'=>trans('admin.posts_information')]);

      }else
      {
          return $post->render('admin.chat.posts.index',['title'=>trans('admin.posts_information')]);

      }



    }

    public function create(Post $post)
    {
        $last = DB::table('posts')->latest()->first();

       if($last != null)
       {
           $lastposttime = $last->created_at;
           $date = date('Y-m-d H:i:s');

           $diff = strtotime($date) - strtotime($lastposttime);

           $hours = date('H:i:s',$diff);
           return view('admin.chat.posts.create', ['hours' => $hours,'title'=> trans('admin.create_new_post')]);

       }else
       {
           $hours = null;
           return view('admin.chat.posts.create', ['hours' => $hours,'title'=> trans('admin.create_new_post')]);

       }


    }

    public function store(Request $request, Post $post)
    {
        $this->validate($request,[
            'body' => 'required',
            'image' => 'sometimes',


        ],[],[
            'body'=> trans('admin.message_content'),
        ]);
            $post = new Post();
            $post->body = $request->body;
            $post->admin_id = Auth::user()->id;

            if($request->hasFile('image')){
                $post['image']= up::upload([
                    'request' => 'image',
                    'path' => 'posts',
                    'upload_type' => 'single',
                ]);
            }

            $post->save();

        return redirect(aurl('posts'))->with(session()->flash('message',trans('admin.success_add')));

    }


    public function show($id)
    {
        $currentadmin =  Auth::user()->id;
        $user = Admin::findOrFail($currentadmin);

       $post = Post::findOrFail($id);

        return view('admin.chat.posts.show',['user'=>$user,'post'=> $post,'title'=>trans('admin.post_information')]);
    }
    public function edit($id)
    {
        $user = Admin::pluck('name','id');
        $post = Post::findOrFail($id);
        return view('admin.chat.posts.edit',['user'=>$user,'post'=> $post,'title'=>trans('admin.edit_post')]);
    }

    public function update(Request $request, $id)
    {

      $data =    $this->validate($request,[
            'title' => 'sometimes',
            'body' => 'required',
            'image' => 'sometimes',


        ],[],[
            'title'=> trans('admin.post_title'),
            'body'=> trans('admin.post_content'),
            'image'=> trans('admin.post_content'),
        ]);
            $post = Post::findOrFail($id);

        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'posts',
                'upload_type' => 'single',
                'delete_file'=> $post->image
            ]);
        }

        $post->update($data);


        return redirect(aurl('posts'))->with(session()->flash('message',trans('admin.success_add')));


    }


    public function destroy($id)
    {
        $post= Post::findOrFail($id);
        $post->delete();
        return redirect(aurl('posts'));
    }
}
