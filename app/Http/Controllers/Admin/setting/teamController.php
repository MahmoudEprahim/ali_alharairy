<?php

namespace App\Http\Controllers\admin\setting;

use App\Team;
use App\DataTables\TeamDataTable;
use App\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Up;

class teamController extends Controller
{
    public function index(TeamDataTable $team)
    {
        
        return $team->render('admin.setting.team.index',['title'=>trans('admin.our_team')]);

    }

    
    public function create()
    {
        return view('admin.setting.team.create' ,['title'=>trans('admin.add_new_team')]);
    }

    public function store(Request $request, Team $team)
    {

        $data =  $this->validate($request,[
            'image' => 'required',
            'name_en' => 'required',
            'teams_title_en' => 'required',
            'name_ar' => 'sometimes',
            'teams_title_ar' => 'sometimes',
            'teams_desc_ar' => 'sometimes',
            'teams_desc_en' => 'sometimes',
            'facebook' => 'sometimes',
            'twitter' => 'sometimes',
            'google' => 'sometimes',
            'linkedin' => 'sometimes',
            'email' => 'sometimes',
            'phone' => 'sometimes',
        ],[],[
            'name_ar'=> trans('admin.name'),
            'name_en' => trans('admin.name'),
            'teams_title_ar' => trans('admin.teams_title_ar'),
            'teams_title_en' => trans('admin.teams_title_en'),
            'teams_desc_ar' =>  trans('admin.teams_desc_ar'),
            'teams_desc_en' => trans('admin.teams_desc_en'),
            'facebook' => trans('admin.facebook'),
            'twitter' => trans('admin.twitter'),
            'google' => trans('admin.google'),
            'linkedin' => trans('admin.linkedin'),
            'email' => trans('admin.email'),
            'phone' => trans('admin.phone'),
        ]);

        if($request->hasFile('image')){
            $team['image'] = Up::upload([
                'request' => 'image',
                'path'=>'team',
                'upload_type' => 'single'
            ]);
        }

        $team->create($data);
        return redirect(aurl('setting/team'))->with(session()->flash('message',trans('admin.success_add')));

    }

    
    public function show($id)
    {
        $team = Team::findOrFail($id);
        $title = trans('admin.show_team');
            return view('admin.setting.team.show',['team'=>$team,'title'=>$title]);
    }

    
    public function edit($id)
    {
        $team = Team::findOrFail($id);
        $title = trans('admin.edit_team');
            return view('admin.setting.team.edit',['team'=>$team,'title'=>$title]);


    }
    public function update(Request $request, Team $team)
    {


        $data =  $this->validate($request,[
            'image' => 'required',
            'name_en' => 'required',
            'teams_title_en' => 'required',
            'name_ar' => 'sometimes',
            'teams_title_ar' => 'sometimes',
            'teams_desc_ar' => 'sometimes',
            'teams_desc_en' => 'sometimes',
            'facebook' => 'sometimes',
            'twitter' => 'sometimes',
            'google' => 'sometimes',
            'linkedin' => 'sometimes',
            'email' => 'sometimes',
            'phone' => 'sometimes',
        ],[],[
            'name_ar'=> trans('admin.name'),
            'name_en' => trans('admin.name'),
            'teams_title_ar' => trans('admin.teams_title_ar'),
            'teams_title_en' => trans('admin.teams_title_en'),
            'teams_desc_ar' =>  trans('admin.teams_desc_ar'),
            'teams_desc_en' => trans('admin.teams_desc_en'),
            'facebook' => trans('admin.facebook'),
            'twitter' => trans('admin.twitter'),
            'google' => trans('admin.google'),
            'linkedin' => trans('admin.linkedin'),
            'email' => trans('admin.email'),
            'phone' => trans('admin.phone'),
        ]);

        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'profile',
                'upload_type' => 'single',
                'delete_file'=> $team->image
            ]);
        }

        $team->update($data);


        return redirect(url('admin/setting/team'))->with(session()->flash('message',trans('admin.success_add')));

    }
    public function destroy($id)
    {
        $team = Team::findORFail($id);
        $team->delete();
        return redirect(url('admin/setting/team'))->with(session()->flash('message',trans('admin.success_delete')));
    }
}
