<?php

namespace App\Http\Controllers\Admin\setting;

use App\InterviewPage;
use App\ServicePage;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;
use Monolog\Handler\ElasticSearchHandler;
use Up;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interview = InterviewPage::all();
        return view('admin.setting.interview.index')->with('interview', $interview);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.setting.interview.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, array(
            'title' => 'required|max:255',
            'body'  => 'required',
        ));

        $interview = new InterviewPage();
        $interview->title = $request->title;
        $interview->body = $request->body;


        $interview->save();

        return redirect()->route('interview.show', $interview->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $interview = InterviewPage::find($id);
        return view('admin.setting.interview.show')->with('interview', $interview);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // find the post in database
        $interview = InterviewPage::find($id);
        return view('admin.setting.interview.edit')->with('interview', $interview);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function update(Request $request, $id)
    {
        // validate the data
        $interview = InterviewPage::find($id);
        $this->validate($request, array(
            'title' => 'required|max:255',
            'body'  => 'sometimes'
        ));


        // save the data to the database
        $interview = InterviewPage::find($id);
        $interview->title = $request->input('title');
        $interview->body = $request->input('body');


        $interview->save();

        // redirect to posts.show
        return redirect()->route('interview.show', $interview->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $interview = InterviewPage::find($id);

        $interview->delete();

        return redirect()->route('interview.index');
    }
}
