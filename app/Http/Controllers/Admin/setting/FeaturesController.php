<?php

namespace App\Http\Controllers\Admin\setting;

use App\Feature;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Up;
class FeaturesController extends Controller
{
    public function index()
    {
        $feature = Feature::first();
        
        return view('admin.setting.features.index', compact('feature'));
    }

    
    public function features_save(Request $request){
        $feature = Feature::first();

        $data = $this->validate($request,[
            'features_title_ar' => 'sometimes',
            'features_title_en' => 'sometimes',
            'main_image' => validate_image(),
            'image_1' => validate_image(),
            'image_2' => validate_image(),
            'image_3' => validate_image(),
            'image_4' => validate_image(),
            'title_1_ar' => 'sometimes|max:100',
            'title_1_en' => 'sometimes|max:100',
            'title_2_ar' => 'sometimes|max:100',
            'title_2_en' => 'sometimes|max:100',
            'title_3_ar' => 'sometimes|max:100',
            'title_3_en' => 'sometimes|max:100',
            'title_4_ar' => 'sometimes|max:100',
            'title_4_en' => 'sometimes|max:100',

        ],[],[
          
        ]);
        if($request->hasFile('main_image')){
            $data['main_image'] = Up::upload([
                'request' => 'main_image',
                'path'=>'setting/features',
                'upload_type' => 'single',
                'delete_file'=>  $feature->main_image
            ]);
        }
        if($request->hasFile('image_1')){
            $data['image_1'] = Up::upload([
                'request' => 'image_1',
                'path'=>'setting/features',
                'upload_type' => 'single',
                'delete_file'=>  $feature->image_1
            ]);
        }
        if($request->hasFile('image_2')){
            $data['image_2'] = Up::upload([
                'request' => 'image_2',
                'path'=>'setting/features',
                'upload_type' => 'single',
                'delete_file'=>  $feature->image_2
            ]);
        }
        if($request->hasFile('image_3')){
            $data['image_3'] = Up::upload([
                'request' => 'image_3',
                'path'=>'setting/features',
                'upload_type' => 'single',
                'delete_file'=>  $feature->image_3
            ]);
        }
        if($request->hasFile('image_4')){
            $data['image_4'] = Up::upload([
                'request' => 'image_4',
                'path'=>'setting/features',
                'upload_type' => 'single',
                'delete_file'=>  $feature->image_4
            ]);
        }


        Feature::orderBy('id','desc')->update($data);
        return redirect(aurl('setting/features'))->with('message',trans('admin.success_update'));
    }
}
