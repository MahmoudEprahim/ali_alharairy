<?php

namespace App\Http\Controllers\Admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\HomeVideo;   
use App\Branches;
use App\DataTables\HomeMediaDataTable;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class HomeVideosController extends Controller
{
    public function index(HomeMediaDataTable $content)
    {

        return $content->render('admin.setting.media.index',['title'=>trans('admin.medai')]);
    }

    public function create()
    {

        $branch = Branches::pluck('name_'.session('lang'),'id');

        return view('admin.setting.media.create', ['branch' => $branch,'title'=> trans('admin.Create_new_media')]);
    }

    public function store(Request $request)
    {

        // dd($request->all());
       $content = new HomeVideo();
       $content->desc_en = $request->desc_en;
       $content->desc_ar = $request->desc_ar;
       $content->branch_id = $request->branch_id;
        $content->video_src = str_replace('watch?v=', 'embed/', $request->video_src);



        $file = $request->file('media');

        if($file) {
            $mediaType = explode('/', $file->getMimeType())[0]; // videos audio
            $filetype = explode('/', $file->getMimeType())[1]; // images

            $name = str_slug($request->input('name')) . '_' . time();
            $filePath = $name . '.' . $file->getClientOriginalExtension();
            if ($mediaType == "video" Or $mediaType == "image" Or $filetype == "pdf" Or $filetype == "docx") {
                $content->media = $filePath;
                $file->move('home/videos', $filePath);

            } else {
                return redirect(aurl('homevideos'))->with('error');
            }
        }

       $content->save();
        return redirect(aurl('setting/homevideos'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
        $content = HomeVideo::findOrFail($id);
        $branch = Branches::pluck('name_'.session('lang'),'id');
        return view('admin.setting.media.show',compact('content','branch'));
    }

    public function edit($id)
    {
        $content =  HomeVideo::findOrFail($id);
        $branch = Branches::pluck('name_'.session('lang'),'id');
        return view('admin.setting.media.edit',['content'=> $content,'branch'=> $branch, 'title'=>trans('admin.edit_media_content')]);
    }

    public function update(Request $request, $id)
    {
        $content = HomeVideo::findOrFail($id);
        // $this->validate($request,[

        //     'media' => 'sometimes|mimes:video,pdf,docx,image',
        //     'desc_en' => 'sometimes',
        //     'desc_ar' => 'sometimes',

        //     'branch_id' => 'sometimes',



        // ],[],[
        //     // 'desc' => trans('admin.desc'),
        //     // 'desc_ar' => trans('admin.desc'),
        //     // 'media' => trans('admin.media'),
        //     // 'branch_id' => trans('admin.branch_id'),
        //     // 'list_id' => trans('admin.list_id'),
        //     'librarytype' => trans('admin.librarytype'),
        // ]);

       $content->desc_en = $request->desc_en;
       $content->desc_ar = $request->desc_ar;
       $content->branch_id = $request->branch_id;
       $content->video_src = str_replace('watch?v=', 'embed/', $request->video_src);






        $file = $request->file('media');
        if($file){
            $mediaType=explode('/',$file->getMimeType())[0];
            $filetype=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();
                if($mediaType=="video" Or $mediaType=="image" Or $filetype=="pdf" Or $filetype=="docx"  )
                {
                  $content->media = $filePath;
                   $file->move('library/videos', $filePath);

                }else{
                    return redirect(aurl('homevideos'))->with('error');
                }
        }
       $content->save();
        return redirect(aurl('setting/homevideos'))->with(session()->flash('message',trans('admin.success_update')));

    }


    public function destroy($id)
    {
       $content = HomeVideo::findOrFail($id);
       $content->delete();
        return redirect(aurl('setting/homevideos'));
    }


    public function upload(Request $request)
    {
        for($i=0; $i < count($request->file('file_name')); $i++)
        {
            $file = $request->file('file_name')[$i];
            $filePath = public_path().'/files';
            $extension = $file->getClientOriginalExtension();
            $files = $file->getClientOiginalName();
            $fileName = $files . '_' . $time() . '.' .$extension;
            $file->move($filePath, $fileName);
        }
        return redirect()->back();
    }
}
