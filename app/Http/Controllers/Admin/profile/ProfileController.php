<?php

namespace App\Http\Controllers\Admin\profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Profile;
use App\DataTables\ProfileDataTable;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class ProfileController extends Controller
{
    public function index(ProfileDataTable $profile)
    {

        return $profile->render('admin.profile.index',['title'=>trans('admin.profile')]);
    }

    public function create(Profile $profile)
    {
        return view('admin.profile.create', ['title'=> trans('admin.Create_new_profile_to_user'),'profile' => $profile]);
    }

    public function store(Request $request, Profile $user)
    {


       $data =  $this->validate($request,[
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'title_ar' => 'sometimes',
            'title_en' => 'sometimes',
            'profile_desc_ar' => 'sometimes',
            'profile_desc_en' => 'sometimes',
            'date_first_stage' => 'sometimes',
            'first_stage' => 'sometimes',
            'first_stage_title' => 'sometimes',
            'second_stage_title' => 'sometimes',
            'third_stage_title' => 'sometimes',
            'fourth_stage_title' => 'sometimes',
           'date_second_stage' => 'sometimes',
           'second_stage' => 'sometimes',
           'date_third_stage' => 'sometimes',
           'third_stage' => 'sometimes',
           'date_fourth_stage' => 'sometimes',
           'fourth_stage' => 'sometimes',


           'date_ex_first_stage' => 'sometimes',
           'ex_first_stage_title' => 'sometimes',
           'ex_first_stage' => 'sometimes',
           'date_ex_second_stage' => 'sometimes',
           'ex_second_stage' => 'sometimes',
           'ex_second_stage_title' => 'sometimes',
           'date_ex_third_stage' => 'sometimes',
           'ex_third_stage' => 'sometimes',
           'ex_third_stage_title' => 'sometimes',
           'date_ex_fourth_stage' => 'sometimes',
           'ex_fourth_stage' => 'sometimes',
           'ex_fourth_stage_title' => 'sometimes',

           'skill_1' => 'sometimes',
           'skill_2' => 'sometimes',
           'skill_3' => 'sometimes',
           'skill_4' => 'sometimes',
           'facebook' => 'sometimes',
           'twitter' => 'sometimes',
           'google' => 'sometimes',
           'linkedin' => 'sometimes',
           'email' => 'sometimes',
           'phone' => 'sometimes',
           'location' => 'sometimes',
           'site' => 'sometimes',
           'image' => 'sometimes'




        ],[],[
            //  'name_ar'=> trans('admin.name'),
            //  'name_en' => trans('admin.name'),
            
            //  'image' =>  trans('image'),
            //  'profile_desc_ar' => trans('profile_desc_ar'),
            //  'profile_desc_ar' => trans('profile_desc_ar'),
            //  'profile_desc_en' => trans('profile_desc_en'),
        ]);




            if($request->hasFile('image')){
                $user['image'] = Up::upload([
                    'request' => 'image',
                    'path'=>'profile',
                    'upload_type' => 'single'
                ]);
            }

        $user->create($data);


        return redirect(aurl('profile'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
        $profile = Profile::findOrFail($id);

        return view('admin.profile.show',compact('profile'));
    }

    public function edit($id)
    {
        $profile = Profile::findOrFail($id);
        return view('admin.profile.edit',['profile'=>$profile,'title'=>trans('admin.edit_profile')]);
    }

    public function update(Request $request, $id)
    {


        $user = Profile::findOrFail($id);
       $data  =  $this->validate($request,[

        'name_ar' => 'sometimes',
        'name_en' => 'sometimes',
        'title_ar' => 'sometimes',
        'title_en' => 'sometimes',
        'profile_desc_ar' => 'sometimes',
        'profile_desc_en' => 'sometimes',
        'date_first_stage' => 'sometimes',
        'first_stage' => 'sometimes',
        'first_stage_title' => 'sometimes',
        'second_stage_title' => 'sometimes',
        'third_stage_title' => 'sometimes',
        'fourth_stage_title' => 'sometimes',
       'date_second_stage' => 'sometimes',
       'second_stage' => 'sometimes',
       'date_third_stage' => 'sometimes',
       'third_stage' => 'sometimes',
       'date_fourth_stage' => 'sometimes',
       'fourth_stage' => 'sometimes',


       'date_ex_first_stage' => 'sometimes',
       'ex_first_stage_title' => 'sometimes',
       'ex_first_stage' => 'sometimes',
       'date_ex_second_stage' => 'sometimes',
       'ex_second_stage' => 'sometimes',
       'ex_second_stage_title' => 'sometimes',
       'date_ex_third_stage' => 'sometimes',
       'ex_third_stage' => 'sometimes',
       'ex_third_stage_title' => 'sometimes',
       'date_ex_fourth_stage' => 'sometimes',
       'ex_fourth_stage' => 'sometimes',
       'ex_fourth_stage_title' => 'sometimes',

       'skill_1' => 'sometimes',
       'skill_2' => 'sometimes',
       'skill_3' => 'sometimes',
       'skill_4' => 'sometimes',

       'hobbies_1' => 'sometimes',
       'hobbies_2' => 'sometimes',
       'hobbies_3' => 'sometimes',
       'hobbies_4' => 'sometimes',

       'facebook' => 'sometimes',
       'twitter' => 'sometimes',
       'google' => 'sometimes',
       'linkedin' => 'sometimes',
       'email' => 'sometimes',
       'phone' => 'sometimes',
       'location' => 'sometimes',
       'site' => 'sometimes',
       'image' => 'sometimes',


           ],[],[
          
        ]);





        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'profile',
                'upload_type' => 'single',
                'delete_file'=> $user->image
            ]);
        }

        $user->update($data);

        return redirect(aurl('profile'))->with(session()->flash('message',trans('admin.success_update')));
    }


    public function destroy($id)
    {
        if($id != 1){
            $profile = Profile::findOrFail($id);
            $profile->delete();
            return redirect(aurl('profile'));
        }else
        {
            $profile = Profile::findOrFail($id);
            $profile->delete();
            return redirect(aurl('profile'));

        }

    }
}
