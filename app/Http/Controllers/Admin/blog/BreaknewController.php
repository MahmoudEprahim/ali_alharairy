<?php

namespace App\Http\Controllers\admin\blog;

use App\breaknew;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\breaknewsDataTable;

class BreaknewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(breaknewsDataTable $dataTable)
    {

        return   $dataTable->render('admin.blog.breaknews.index',['title'=>trans('admin.add_new_breaknews')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.breaknews.create',['title'=>trans('admin.add_new_breaknews')]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,breaknew $breaknew)
    {


        $data = $this->validate($request,[
            'title' => 'required',
            'publish_in' => 'sometimes',
            'description' => 'sometimes',



        ],[],[
            'title' => trans('admin.title'),
            'publish_in' => trans('admin.publish_in'),
            'description' => trans('admin.description'),

        ]);

        $breaknew->updated_at = date('Y-m-d h:i:s', strtotime($request->publish_in));
        $breaknew->title = $request->title;
        $breaknew->description = $request->description;
        $breaknew->save();
        return redirect(aurl('breaknews'))->with(session()->flash('message',trans('admin.success_add')));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\breaknew  $breaknew
     * @return \Illuminate\Http\Response
     */
    public function show(breaknew $breaknew)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\breaknew  $breaknew
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $breaknew = breaknew::findORfail($id);

        return view('admin.blog.breaknews.edit',['title'=>trans('admin.edit_new_breaknews'),'breaknew'=>$breaknew]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\breaknew  $breaknew
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $breaknews = breaknew::findORfail($id);
        $data = $this->validate($request,[
            'title'=>'sometimes',
            'publish_in'=>'sometimes',
            'description'=>'sometimes',

        ],[],[
            'title'=>trans('admin.title'),
            'publish_in'=>trans('admin.publish_in'),
            'description'=>trans('admin.description'),
        ]);
        $breaknews->update($data);
        $breaknews->save();

      return redirect(aurl('breaknews'))->with(session()->flash('message',trans('admin.success_edit')));








    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\breaknew  $breaknew
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $breaknews = breaknew::findORfail($id);
        $breaknews->delete();
        return redirect(aurl('breaknews'))->with(session()->flash('message',trans('admin.delete')));
    }
}
