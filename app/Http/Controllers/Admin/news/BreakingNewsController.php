<?php

namespace App\Http\Controllers\Admin\news;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\breakingNews;
use Illuminate\Support\Facades\Storage;
use Up;

class BreakingNewsController extends Controller
{
    public function index()
    {
        $settins = Setting::all();
        return view('admin.setting', compact('settins'));
    }


    public function setting_save(Request $request){

        
        $data = $this->validate($request,[
            'logo' => validate_image(),
            'icon' => validate_image(),
            'sitename_en' => 'required',
            'sitename_ar' => 'required',
            'email' => 'sometimes',
            'main_lang' => 'required',
            'title_about_center_ar' => 'sometimes|max:100',
            'title_about_center_en' => 'sometimes|max:100',
            'about_center_ar' => 'sometimes|max:1000',
            'about_center_en' => 'sometimes|max:1000',
            'title_about_teacher_ar' => 'sometimes|max:100',
            'title_about_teacher_en' => 'sometimes|max:100',
            'about_teacher_ar' => 'sometimes|max:1000',
            'about_teacher_en' => 'sometimes|max:1000',
            'features_title_ar' => 'sometimes|max:100',
            'features_title_en' => 'sometimes|max:100',
            'icon_1' => validate_image(),
            'icon_2' => validate_image(),
            'icon_3' => validate_image(),
            'icon_4' => validate_image(),
            'video_src' => 'sometimes',
            'addriss' => 'sometimes',
            'phone' => 'sometimes',
            'phone2' => 'sometimes',
            'phone_3' => 'sometimes',
            'phone_4' => 'sometimes',
            'contact_us_title_ar' => 'sometimes|max:100',
            'contact_us_title_en' => 'sometimes|max:100',
            'facebook' => 'sometimes',
            'twitter' => 'sometimes',
            'googel' => 'sometimes',
            'linkedin' => 'sometimes',
            'youtube' => 'sometimes',
            'title_of_team_members_ar' => 'sometimes|max:100',
            'title_of_team_members_en' => 'sometimes|max:100',
            'member_name_ar' => 'sometimes',
            'member_name_en' => 'sometimes',
            'member_name_ar2' => 'sometimes',
            'member_name_en2' => 'sometimes',
            'member_name_ar3' => 'sometimes',
            'member_name_en3' => 'sometimes',
            'about_desc_ar' => 'sometimes|max:1000',
            'about_desc_en' => 'sometimes|max:1000',
                        
        ],[],[
            'logo' => trans('admin.image'),
            'icon' => trans('admin.icon'),
            'sitename_en' => trans('admin.arabic_name'),
            'sitename_ar' => trans('admin.english_name'),
            'email' => trans('admin.email'),
            'main_lang' => trans('admin.main_lang'),
            'title_about_center_ar' => trans('admin.title_about_center_ar'),
            'title_about_center_en' => trans('admin.title_about_center_en'),
            'about_center_ar' => trans('admin.about_center_ar'),
            'about_center_en' => trans('admin.about_center_en'),
            'title_about_teacher_ar' => trans('admin.title_about_teacher_ar'),
            'title_about_teacher_en' => trans('admin.title_about_teacher_en'),
            'about_teacher_ar' => trans('admin.about_teacher_ar'),
            'about_teacher_en' => trans('admin.about_teacher_en'),
            'features_title_ar' => trans('admin.features_title_ar'),
            'features_title_en' => trans('admin.features_title_en'),
            'icon_1' => trans('admin.icon_1'),
            'icon_2' => trans('admin.icon_2'),
            'icon_3' => trans('admin.icon_3'),
            'icon_4' => trans('admin.icon_4'),
            'video_src' => trans('admin.video_src'),
            'addriss' => trans('admin.addriss'),
            'phone' => trans('admin.phone'),
            'phone_2' => trans('admin.phone'),
            'phone_3' => trans('admin.phone'),
            'phone_4' => trans('admin.phone'),
            'facebook' => trans('admin.facebook'),
            'twitter' => trans('admin.twitter'),
            'googel' => trans('admin.googel'),
            'linkedin' => trans('admin.linkedin'),
            'youtube' => trans('admin.youtube'),
            'title_of_team_members_ar' => trans('admin.title_of_team_members_ar'),
            'title_of_team_members_en' => trans('admin.title_of_team_members_en'),
            'member_name_ar' => trans('admin.member_name_ar'),
            'member_name_en' => trans('admin.member_name_en'),
            'member_name_ar2' => trans('admin.member_name_ar2'),
            'member_name_en2' => trans('admin.member_name_en2'),
            'member_name_ar3' => trans('admin.member_name_ar3'),
            'member_name_en3' => trans('admin.member_name_en3 '),
            'about_desc_ar' => trans('admin.about_desc_ar'),
            'about_desc_ar' => trans('admin.about_desc_en'),
        ]);
        if($request->hasFile('logo')){
            $data['logo'] = Up::upload([
                'request' => 'logo',
                'path'=>'setting',
                'upload_type' => 'single',
                'delete_file'=> setting()->logo
            ]);
        }
        if($request->hasFile('icon')){
            $data['icon'] = Up::upload([
                'request' => 'icon',
                'path'=>'setting',
                'upload_type' => 'single',
                'delete_file'=> setting()->icon
            ]);
        }

        if($request->hasFile('icon_1')){
            $data['icon_1'] = Up::upload([
                'request' => 'icon_1',
                'path'=>'setting',
                'upload_type' => 'single',
                'delete_file'=> setting()->icon
            ]);
        }

        if($request->hasFile('icon_2')){
            $data['icon_2'] = Up::upload([
                'request' => 'icon_2',
                'path'=>'setting',
                'upload_type' => 'single',
                'delete_file'=> setting()->icon
            ]);
        }

        if($request->hasFile('icon_3')){
            $data['icon_3'] = Up::upload([
                'request' => 'icon_3',
                'path'=>'setting',
                'upload_type' => 'single',
                'delete_file'=> setting()->icon
            ]);
        }

        if($request->hasFile('icon_4')){
            $data['icon_4'] = Up::upload([
                'request' => 'icon_4',
                'path'=>'setting',
                'upload_type' => 'single',
                'delete_file'=> setting()->icon
            ]);
        }
//https://www.youtube.com/embed/PDdUeN4cuE8
// https://www.youtube.com/watch?v=PDdUeN4cuE8
        
       
    
       $request->video_src = str_replace('watch?v=', 'embed/', $request->video_src);

       
       
       $data['video_src'] = $request->video_src;
        Setting::orderBy('id','desc')->update($data);
        return redirect(aurl('setting'))->with('message',trans('admin.success_update'));
    }
}
