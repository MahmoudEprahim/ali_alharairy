<?php

namespace App\Http\Controllers\Admin\parents;
use App\Branches;
use App\country;
use App\DataTables\parentsDataTable;
use App\Department;
use App\parents;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Hash;

class ParentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(parentsDataTable $parent)

    {

//        $parent = parents::where('name_ar','=',null)->orWhere('name_ar','=','')->pluck('id');
        DB::table('parents')->where('name_en',null)->where('name_ar',null)->orWhere('name_ar','=','')->delete();
        return $parent->render('admin.parents.index',['title'=> trans('admin.parents_data')]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(parents $parents)
    {
        // $countries = country::pluck('country_name_'.session('lang'),'id');
        // $departments = Department::where('operation_id',2)->pluck('dep_name_'.session('lang'),'id');
        $parent = parents::findOrFail(parents::create([
            'name_ar' => '',
        ])->id);
        if (!empty($parent)) {
            return redirect(aurl('parents/' . $parent->id . '/edit'));
        }
        $branches = Branches::pluck('name_'.session('lang'),'id');
        return view('admin.parents.create',['title'=> trans('admin.Create_new_parents'),'branche'=>$branches]);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,parents $parents)
    {
        $data = $this->validate($request,[
            'branch_id' => 'required',
            'name_ar' => 'required',
            'name_en' => 'sometimes',
            'Parnt_Job' => 'sometimes',
            'Parnt_address' => 'sometimes',
            'Mobile1' => 'sometimes',
            'Mobile2'=>'sometimes',



        ],[],[
            'name_ar' => trans('admin.arabic_name'),
            'name_en' => trans('admin.english_name'),
            'parents_phone' => trans('admin.parents_phone'),
            'branch_id' => trans('admin.branche'),
            'beginning_study_date' =>trans('admin.beginning_study_date'),
            'addriss' => trans('admin.addriss'),
            'total_fees' =>trans('admin.total_fees'),

        ]);




        $student = parents::findOrFail($parents->create($data)->id);
        $branches = Branches::pluck('id')->toArray();

        return redirect(aurl('parents'))->with(session()->flash('message',trans('admin.success_add')));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\parents  $parents
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parents = parents::findOrFail($id);
        return view('admin.parents.show',['title'=> trans('parents') ,'parents'=>$parents]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\parents  $parents
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $related = parents::findOrFail($id);
        $branches = Branches::pluck('name_'.session('lang'),'id');
        return view('admin.parents.create',['title'=> trans('admin.edit_parents') ,'related'=>$related,'branche'=>$branches]);

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\parents  $parents
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $parent = parents::findOrFail($id);
        $data = $this->validate($request,[
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'password' => 'sometimes',
            'phone_1' => 'sometimes',
            'phone_2' => 'sometimes',
            'mobile_1' => 'sometimes',
            'mobile_2' => 'sometimes',
            'relation' => 'sometimes',
            'type' => 'sometimes',
            'countries_id' => 'sometimes',
            'nations_id' => 'sometimes',
            'number' => 'sometimes',
            'date_issuance' => 'sometimes',
            'expired_date' => 'sometimes',
            'created_from' => 'sometimes',
            'job' => 'sometimes',
            'home_address' => 'sometimes',
            'note' => 'sometimes',
            'education_level' => 'sometimes',
            'Specialization' => 'sometimes',
            'email' => 'sometimes',
            'monthly_income' => 'sometimes',
            'average_income' => 'sometimes',
            'tax_number' => 'sometimes',
            'birthdate' => 'sometimes',
            'birthplace' => 'sometimes',
            'work_status' => 'sometimes',
            'labor_sector' => 'sometimes',
            'job_number' => 'sometimes',
            'job_phone' => 'sometimes',
            'job_address' => 'sometimes',
            'boys_num' => 'sometimes',
            'girls_num' => 'sometimes',
            'state_id' => 'sometimes',
            'city_id' => 'sometimes',
            'street' => 'sometimes',
            'house_num' => 'sometimes',
            'compound_num' => 'sometimes',
            'compound_name' => 'sometimes',
            'near' => 'sometimes',
        ],[],[]);

        $data['password'] = Hash::make($request->password);
        $parent->update($data);
        return redirect(aurl('parents'))->with(session()->flash('message',trans('admin.success_update')));
    }

    
    public function destroy($id)
    {
        $parents = parents::findOrFail($id);
        $parents->delete();
        return redirect(aurl('parents'))->with(session()->flash('message',trans('admin.success_delete')));
    }

    public function parentActive(Request $request){
        if($request->ajax()){
            $parent = parents::find($request->id);
            if($parent != 'null'){
                if ($parent->active == 0){
                    $parent->update(['active' => 1]);
                    return response()->json(['status'=>1, 'class' => 'btn-success', 'icon' => 'fa-check']);
                } elseif($parent->active == 1){
                    $parent->update(['active' => 0]);
                    return response()->json(['status'=>1,'class' => 'btn-warning', 'icon' => 'fa-close']);
                }

            }
        }
    }
}
