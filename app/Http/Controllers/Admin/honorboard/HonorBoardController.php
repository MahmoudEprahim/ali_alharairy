<?php

namespace App\Http\Controllers\Admin\honorboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\HonorBoard;
use App\Student;
use App\DataTables\HonorBoadDataTable;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class HonorBoardController extends Controller
{
    public function index(HonorBoadDataTable $honorBoard)
    {
        return $honorBoard->render('admin.honorboard.index',['title'=>trans('admin.honorboard_information')]);
    }

    public function create(HonorBoard $honorBoard)
    {
        
        $student = Student::where('deleted_at', NULL)->pluck('name_'.session('lang'),'id');
        return view('admin.honorboard.create',compact('student'), ['title'=> trans('admin.add_student_to_honorboard')]);
    }

    public function store(Request $request, HonorBoard $honorBoard)
    {
        $this->validate($request,[
            'student_id' => 'required',
            'description' => 'sometimes'
        ],[],[
            'student_id' => trans('admin.student_id'),
        ]);


        $student = HonorBoard::where('student_id', $request->student_id)->get();
//        dd(count($student) != 0);
        if($student && count($student) != 0){
            $honorBoard->update(['description' => $request->description]);
        }else{
            $honorBoard = new HonorBoard();
            $honorBoard->student_id = $request->student_id;
            $honorBoard->description = $request->description;
            $honorBoard->save();
        }

        return redirect(aurl('honorboard'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
        $content = HonorBoard::findOrFail($id);
        return view('admin.honorboard.show',compact('content'));
    }

    public function edit($id)
    {
        $student = Student::pluck('name_'.session('lang'),'id');
        $honorBoard = HonorBoard::findOrFail($id);
        return view('admin.honorboard.edit',compact('honorBoard', 'student'),['title'=>trans('admin.edit_honor_board')]);
    }

    public function update(Request $request, $id)
    {
        $honorBoard = HonorBoard::findOrFail($id);
        $this->validate($request,[
            'description' => 'sometimes',
            'student_id' => 'sometimes',
        ],[],[
            'description' => trans('admin.description'),
            'student_id' => trans('admin.student_id'),
        ]);
            $honorBoard->description = $request->description;
            $honorBoard->student_id = $request->student_id;

            if($request != null){
                $honorBoard->update();

            return redirect(aurl('honorboard'))->with(session()->flash('message',trans('admin.success_add')));
            }
            return redirect(aurl('honorboard'));

    }


    public function destroy($id)
    {
        $honorBoard = HonorBoard::findOrFail($id);
        $honorBoard->delete();
        return redirect(aurl('honorboard'));
    }
}
