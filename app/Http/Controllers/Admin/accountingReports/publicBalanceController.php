<?php

namespace App\Http\Controllers\Admin\accountingReports;

use App\Department;
use App\glcc;
use App\levels;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class publicBalanceController extends Controller
{
    public function index(){
        $title = trans('admin.public_balance');
        $departments = Department::where('type','0')->pluck('dep_name_'.session('lang'),'id');
        $level = levels::where('type','1')->pluck('levelId','id');
        return view('admin.accountingReports.publicbalance.index',compact('title','departments','level'));
    }

    public function level(Request $request){
        if($request->ajax()){


            $department = $request->departments;
            $level = levels::where('type','1')->pluck('levelId','id');

//            if ($from && $to){

                return $data2 =  view('admin.accountingReports.publicbalance.level',compact('department','level'))->render();
//            }elseif($from && $to && $department){
//                return view('admin.accountingReports.publicbalance.level',compact('department'))->render();
//            }elseif($from && $to && $level){
//                return view('admin.accountingReports.publicbalance.level',compact('department'))->render();
//            }elseif($from && $to && $department && $level){
//                return view('admin.accountingReports.publicbalance.level',compact('department'))->render();
//            }
        }
    }
    public function show(Request $request){
        $from = $request->from;
        $to = $request->to;
        $department = $request->departments;
        $level = $request->level;

        $kind = $request->kind;
        if($request->ajax()){


                $data2 =  view('admin.accountingReports.publicbalance.level',compact('department','level'))->render();

                $data =  view('admin.accountingReports.publicbalance.show',compact('from','to','kind','department','level'))->render();
                return $dataa = [$data,$data2];


        }
    }


    public function pdf(Request $request){

            $from = $request->from;
            $to = $request->to;
            $department = $request->department;
            $level = $request->level;
            $kind = $request->kind;

        switch ($kind) {

            case '0';
                if($level == 1){

                    $departments = Department::where('id','=',$department)->get();
                    $config = ['instanceConfigurator' => function($mpdf) {
                        $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                        );
                    }];
                    $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_total_1', ['from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                    return $pdf->stream();
                }elseif( $level == 2){

                    $depart = \App\Department::findOrFail($department);

                    $departments = $depart->children;

                    $config = ['instanceConfigurator' => function($mpdf) {
                        $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                        );
                    }];
                    $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_total_2', ['from'=>$from,'to'=>$to,'depart'=>$depart,'departments'=>$departments,'level'=>$level], [] , $config);
                    return $pdf->stream();



                }elseif($level == 3){

                    $value = [];

                    $depart = \App\Department::findOrFail($department);

                    $pros = [];
                    $products = [];
                    $categories = $depart->children;

                    while(count($categories) > 0){

                        $nextCategories = [];
                        foreach ($categories as $category) {

                            $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                        }
                        $categories = $nextCategories;
                    }


                    $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                    $pros = $depart->children->pluck('id');

                    $plucks = $pro->pluck('id');
                    $values = $pros->concat($plucks);

                    $departments = \App\Department::whereIn('id',$values)->orderBy('code','asc')->get();




                    $config = ['instanceConfigurator' => function($mpdf) {
                        $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                        );
                    }];
                    $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_total_3', ['from'=>$from,'to'=>$to,'depart'=>$depart,'departments'=>$departments,'level'=>$level], [] , $config);
                    return $pdf->stream();



                }else
                {

                    $value = [];

                    $depart = \App\Department::findOrFail($department);

                    $pros = [];
                    $products = [];
                    $categories = $depart->children;

                    while(count($categories) > 0){

                        $nextCategories = [];
                        foreach ($categories as $category) {

                            $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                        }
                        $categories = $nextCategories;
                    }


                    $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                    $pros = $depart->children->pluck('id');

                    $plucks = $pro->pluck('id');
                    $values = $pros->concat($plucks);

                    $department_id = \App\Department::whereIn('parent_id',$values)->orderBy('code','asc')->pluck('id');
                    $values = $pros->concat($plucks)->concat($department_id);

                    $departments = \App\Department::whereIn('id',$values)->orderBy('code','asc')->get();




                    $config = ['instanceConfigurator' => function($mpdf) {
                        $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                        );
                    }];
                        $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_total_4', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);
                    return $pdf->stream();

                }

                break;
            case '1';

            if($level == 1)
            {

                $departments = Department::where('id','=',$department)->get();

                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_1', ['from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                return $pdf->stream();

            } elseif($level == 2)
            {

                $depart = \App\Department::findOrFail($department);

                $departments = $depart->children;

                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_2', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                return $pdf->stream();

            }
        elseif($level == 3)
            {

                $value = [];
                $depart = \App\Department::findOrFail($department);

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){
                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }

                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                $pros = $depart->children->pluck('id');

                $plucks = $pro->pluck('id');
                $values = $pros->concat($plucks);

                $departments = \App\Department::whereIn('id',$values)->orderBy('code','asc')->get();



                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_3', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                return $pdf->stream();

            } else
            {

                $value = [];

                $depart = \App\Department::findOrFail($department);

                $pros = [];
                $products = [];
                $categories = $depart->children;

                while(count($categories) > 0){

                    $nextCategories = [];
                    foreach ($categories as $category) {

                        $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                    }
                    $categories = $nextCategories;
                }

                $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                $pros = $depart->children->pluck('id');

                $plucks = $pro->pluck('id');
                $values = $pros->concat($plucks);

                $department_id = \App\Department::whereIn('parent_id',$values)->orderBy('code','asc')->pluck('id');
                $values = $pros->concat($plucks)->concat($department_id);

                $departments = \App\Department::whereIn('id',$values)->orderBy('code','asc')->get();




                $config = ['instanceConfigurator' => function($mpdf) {
                    $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                    );
                }];
                $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_4', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                return $pdf->stream();

            }

                break;
            case '2';
                if($level == 1){

                    $departments = Department::where('id','=',$department)->get();

                    $config = ['instanceConfigurator' => function($mpdf) {
                        $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                        );
                    }];
                    $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_no_1', ['from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                    return $pdf->stream();

                }else if($level == 2){



                    $depart = \App\Department::findOrFail($department);

                    $departments = $depart->children;

                    $config = ['instanceConfigurator' => function($mpdf) {
                        $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                        );
                    }];
                    $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_no_2', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                    return $pdf->stream();

                }else if($level == 3){


                    $value = [];
                    $depart = \App\Department::findOrFail($department);

                    $pros = [];
                    $products = [];
                    $categories = $depart->children;

                    while(count($categories) > 0){
                        $nextCategories = [];
                        foreach ($categories as $category) {

                            $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                        }
                        $categories = $nextCategories;
                    }

                    $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                    $pros = $depart->children->pluck('id');

                    $plucks = $pro->pluck('id');
                    $values = $pros->concat($plucks);

                    $departments = \App\Department::whereIn('id',$values)->orderBy('code','asc')->get();



                    $config = ['instanceConfigurator' => function($mpdf) {
                        $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                        );
                    }];
                    $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_no_3', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                    return $pdf->stream();
                }else
                {

                    $value = [];

                    $depart = \App\Department::findOrFail($department);

                    $pros = [];
                    $products = [];
                    $categories = $depart->children;

                    while(count($categories) > 0){

                        $nextCategories = [];
                        foreach ($categories as $category) {

                            $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                        }
                        $categories = $nextCategories;
                    }



                    $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                    $pros = $depart->children->pluck('id');

                    $plucks = $pro->pluck('id');
                    $values = $pros->concat($plucks);

                    $department_id = \App\Department::whereIn('parent_id',$values)->orderBy('code','asc')->pluck('id');
                    $values = $pros->concat($plucks)->concat($department_id);

                    $departments = \App\Department::whereIn('id',$values)->orderBy('code','asc')->get();




                    $config = ['instanceConfigurator' => function($mpdf) {
                        $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                        );
                    }];
                    $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_no_4', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                    return $pdf->stream();

                }
                break;
                 case '3';
                     if($level == 1){

                         $departments = Department::where('id','=',$department)->get();

                         $config = ['instanceConfigurator' => function($mpdf) {
                             $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                             );
                         }];
                         $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_debt_1', ['from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                         return $pdf->stream();

                     }elseif($level == 2){



                     $depart = \App\Department::findOrFail($department);

                     $departments = $depart->children;

                     $config = ['instanceConfigurator' => function($mpdf) {
                         $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                         );
                     }];
                         $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_debt_2', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                     return $pdf->stream();

                 }elseif($level == 3){


                         $value = [];
                         $depart = \App\Department::findOrFail($department);

                         $pros = [];
                         $products = [];
                         $categories = $depart->children;

                         while(count($categories) > 0){
                             $nextCategories = [];
                             foreach ($categories as $category) {

                                 $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                             }
                             $categories = $nextCategories;
                         }
                         $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                         $pros = $depart->children->pluck('id');

                         $plucks = $pro->pluck('id');
                         $values = $pros->concat($plucks);

                         $departments = \App\Department::whereIn('id',$values)->orderBy('code','asc')->get();



                         $config = ['instanceConfigurator' => function($mpdf) {
                             $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                             );
                         }];
                         $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_debt_3', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                         return $pdf->stream();
                     }else
                     {

                         $value = [];

                         $depart = \App\Department::findOrFail($department);

                         $pros = [];
                         $products = [];
                         $categories = $depart->children;

                         while(count($categories) > 0){

                             $nextCategories = [];
                             foreach ($categories as $category) {

                                 $products = array_merge($products, $category->children->all());


//                        $nextCategories = array_merge($nextCategories, $category->children->all());

                             }
                             $categories = $nextCategories;
                         }



                         $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                         $pros = $depart->children->pluck('id');

                         $plucks = $pro->pluck('id');
                         $values = $pros->concat($plucks);

                         $department_id = \App\Department::whereIn('parent_id',$values)->orderBy('code','asc')->pluck('id');
                         $values = $pros->concat($plucks)->concat($department_id);

                         $departments = \App\Department::whereIn('id',$values)->orderBy('code','asc')->get();



                         $config = ['instanceConfigurator' => function($mpdf) {
                             $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                             );
                         }];
                         $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_debt_4', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                         return $pdf->stream();

                     }
                     break;
                     case '4';
                         if($level == 1) {

                             $departments = Department::where('id', '=', $department)->get();
                             $config = ['instanceConfigurator' => function($mpdf) {
                                 $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                                 );
                             }];
                             $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_cred_1', ['from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                             return $pdf->stream();
                         }elseif($level == 2){


                         $depart = \App\Department::findOrFail($department);

                         $departments = $depart->children;
                         $config = ['instanceConfigurator' => function($mpdf) {
                             $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                             );
                         }];
                         $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_cred_2', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                         return $pdf->stream();
                     }elseif($level == 3){


                             $value = [];
                             $depart = \App\Department::findOrFail($department);

                             $pros = [];
                             $products = [];
                             $categories = $depart->children;

                             while(count($categories) > 0){
                                 $nextCategories = [];
                                 foreach ($categories as $category) {

                                     $products = array_merge($products, $category->children->all());
                                 }
                                 $categories = $nextCategories;
                             }

                             $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                             $pros = $depart->children->pluck('id');

                             $plucks = $pro->pluck('id');
                             $values = $pros->concat($plucks);

                             $departments = \App\Department::whereIn('id',$values)->orderBy('code','asc')->get();


                             $config = ['instanceConfigurator' => function($mpdf) {
                             $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                             );
                         }];
                         $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_cred_3', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                         return $pdf->stream();
                     }else{

                             $value = [];

                             $depart = \App\Department::findOrFail($department);

                             $pros = [];
                             $products = [];
                             $categories = $depart->children;

                             while(count($categories) > 0){

                                 $nextCategories = [];
                                 foreach ($categories as $category) {

                                     $products = array_merge($products, $category->children->all());

                            //                        $nextCategories = array_merge($nextCategories, $category->children->all());

                                 }
                                 $categories = $nextCategories;
                             }

                             $pro = new Collection($products); //Illuminate\Database\Eloquent\Collection

                             $pros = $depart->children->pluck('id');

                             $plucks = $pro->pluck('id');
                             $values = $pros->concat($plucks);


                             $glcc_3 = \App\Department::whereIn('parent_id',$values)->pluck('id');

                             $values_all = $pros->concat($plucks,$glcc_3);


                             $departments = \App\Department::whereIn('id',$values_all)->orderBy('code','asc')->get();



                             $config = ['instanceConfigurator' => function($mpdf) {
                                 $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                                 );
                             }];
                             $pdf = PDF::loadView('admin.accountingReports.publicbalance.pdf.PB_balance_cred_4', ['depart'=>$depart,'from'=>$from,'to'=>$to,'departments'=>$departments,'level'=>$level], [] , $config);

                             return $pdf->stream();
                     }

             }
    }
}
