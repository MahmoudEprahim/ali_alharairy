<?php

namespace App\Http\Controllers\Admin\accountingReports;

use App\Branches;
use App\Company;
use App\Contractors;
use App\Department;
use App\drivers;
use App\employee;
use App\Enums\dataLinks\ReceiptType;
use App\limitationReceipts;
use App\limitations;
use App\limitationsType;
use App\operation;
use App\Project;
use App\receipts;
use App\receiptsData;
use App\receiptsType;
use App\subscription;
use App\supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class accountStatementController extends Controller
{
    public function index(){
        $operations = operation::whereIn('receipt',[1,2])->pluck('name_'.session('lang'),'id');

        $branches = Branches::pluck('name_'.session('lang'),'id');
        $title = trans('admin.account_statement');
        return view('admin.accountingReports.accountStatement.index',compact('title','operations','branches'));
    }
    public function show(Request $request)
    {
        if($request->ajax()){
            $operations = $request->operations;
            $operation = operation::where('id',$operations)->first();
            $branches = $request->branches;
            if($operations != null && $branches != null){
                if ($operation->id == 4){
                    $tree = Department::where('type', '1')->pluck('dep_name_'.session('lang'),'id');
                }elseif ($operation->id == 2){
                    $tree = Company::pluck('name_'.session('lang'),'id');
                }elseif ($operation->id == 5) {
                    $tree = employee::pluck('name_' . session('lang'), 'id');
                }
                $contents = view('admin.accountingReports.accountStatement.show', ['operations'=>$operations,'operation'=>$operation,'tree'=>$tree])->render();
                return $contents;
            }
        }
    }
    public function details(Request $request)
    {
        if($request->ajax()){
            $operations = $request->operations;
//            $branches = Branches::where('id',$request->branches)->first();
            $branches = $request->branches;
            $to = $request->to;
            $from = $request->from;
            $fromtree = $request->fromtree;
            $totree = $request->totree;
            if($from != null && $to != null && $fromtree != null && $totree != null){
                if ($operations == 5){
                    $suppliers = DB::table('employees')->where('id', '>=', $fromtree)->where('id', '<=', $totree)->pluck('id')->toArray();
                    $receipts_id = receipts::where('branche_id',$branches)->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->pluck('id');
                    $limitations_id = limitations::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->where('branche_id',$branches)->pluck('id');
                    $hasTask = receiptsType::where('operation_id',$operations)->whereIn('relation_id',$suppliers)->whereIn('receipts_id',$receipts_id)->exists();
                    $hasTask2 = limitationsType::where('operation_id',$operations)->whereIn('relation_id',$suppliers)->whereIn('limitations_id',$limitations_id)->exists();
                }elseif ($operations == 2){
                    $suppliers = DB::table('companies')->where('id', '>=', $fromtree)->where('id', '<=', $totree)->pluck('id')->toArray();
                    $receipts_id = receipts::where('branche_id',$branches)->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->pluck('id');
                    $limitations_id = limitations::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->where('branche_id',$branches)->pluck('id');
                    $hasTask = receiptsType::where('operation_id',$operations)->whereIn('relation_id',$suppliers)->whereIn('receipts_id',$receipts_id)->exists();
                    $hasTask2 = limitationsType::where('operation_id',$operations)->whereIn('relation_id',$suppliers)->whereIn('limitations_id',$limitations_id)->exists();

                }elseif ($operations == 4){
                    $depart = Department::where('id', '>=', $fromtree)->where('id', '<=', $totree)->pluck('id')->toArray();
//                    $Rtypes = receiptsType::where('operation_id',$operations)->whereIn('tree_id',$depart)->whereIn('receipts_id',$receipts_id)->exists();
//                    $Ltypes = limitationsType::whereIn('tree_id',$depart)->whereIn('limitations_id',$limitations_id)->exists();
                    $receipts_id = receipts::where('branche_id',$branches)->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
                        ->whereHas('receipts_type',function ($query) use ($operations,$depart,$fromtree,$totree){
                            $query->where('tree_id', '>=', $fromtree)->where('tree_id', '<=', $totree);
                        })
                        ->pluck('id');
                    $limitations_id = limitations::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
                        ->whereHas('limitations_type',function ($query) use ($operations,$depart,$fromtree,$totree){
                            $query->where('tree_id', '>=', $fromtree)->where('tree_id', '<=', $totree);
                        })
                        ->pluck('id');
                    $hasTask = receiptsType::where('operation_id',$operations)->whereIn('tree_id',$depart)->whereIn('receipts_id',$receipts_id)->exists();
                    $hasTask2 = limitationsType::whereIn('tree_id',$depart)->whereIn('limitations_id',$limitations_id)->exists();
                }
                $contents = view('admin.accountingReports.accountStatement.details', ['operations'=>$operations,'branches'=>$branches,'from'=>$from,'to'=>$to,'fromtree'=>$fromtree,'totree'=>$totree, 'hasTask' => $hasTask, 'hasTask2' => $hasTask2])->render();
                return $contents;
            }
        }
    }

    public function pdf(Request $request) {

        $branches = Branches::where('id',$request->branches)->first();
        $to = $request->to;
        $from = $request->from;
        $fromtree = $request->fromtree;
        $totree = $request->totree;
        $operations = $request->operations;
        $operation = operation::where('id',$operations)->first();


        if ($operations == 5){
            $drivers = employee::where('id', '>=', $fromtree)->where('id', '<=', $totree)->pluck('id')->toArray();
            $persons = employee::where('id', '>=', $fromtree)->where('id', '<=', $totree)->get();
            $openingEntry = limitations::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->where('limitationsType_id', 12)->where('branche_id',$branches->id)->whereYear('created_at', '=', date('Y',strtotime($from)))->pluck('id')->toArray();
            $limitations_id = limitations::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->whereNotIn('limitationsType_id',[12])->where('branche_id',$branches->id)->orderBy('limitationsType_id', 'ASC')->pluck('id');
            $receipts_id = receipts::where('branche_id',$branches->id)->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->pluck('id');
            $receiptsType = receiptsType::whereIn('receipts_id',$receipts_id)->whereIn('relation_id',$drivers)->where('operation_id',$operations)->get();
            $limitationsType = limitationsType::whereIn('limitations_id',$limitations_id)->whereIn('relation_id',$drivers)->where('operation_id',$operations)->get();
            $openingEntryType = limitationsType::whereIn('limitations_id',$openingEntry)->whereIn('relation_id',$drivers)->where('operation_id',$operations)->get();
            $value_merged = $openingEntryType->toBase()->merge($limitationsType)->sortByDesc('status');
            $person_merged =$persons->toBase()->merge($value_merged)->sortByDesc('status');
            $merged = $person_merged->toBase()->merge($receiptsType)->sortBy('created_at');
//            dd($merged);
            // $posts = $merged->sortBy(function($post) {
            //     return $post->receipts->receiptsType_id;
            // });
            // $posts = $posts->sortBy(function($p) {

            //     return $p->receipts->updated_at;

            // });
            $data = $merged->groupBy(function($date) {
                return session_lang($date->name_en,$date->name_ar);
            });

        }elseif ($operations == 2){
            $subscribers = Company::where('id', '>=', $fromtree)->where('id', '<=', $totree)->pluck('id')->toArray();
            $persons = Company::where('id', '>=', $fromtree)->where('id', '<=', $totree)->get();
            $openingEntry = limitations::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->where('limitationsType_id', 12)->where('branche_id',$branches->id)->pluck('id');
            $limitations_id = limitations::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->whereNotIn('limitationsType_id',[12])->where('branche_id',$branches->id)->orderBy('limitationsType_id', 'ASC')->pluck('id');
            $receipts_id = receipts::where('branche_id',$branches->id)->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->pluck('id');
            $receiptsType = receiptsType::whereIn('receipts_id',$receipts_id)->whereIn('relation_id',$subscribers)->where('operation_id',$operations)->get();
            $limitationsType = limitationsType::whereIn('limitations_id',$limitations_id)->whereIn('relation_id',$subscribers)->where('operation_id',$operations)->get();
            $openingEntryType = limitationsType::whereIn('limitations_id',$openingEntry)->whereIn('relation_id',$subscribers)->where('operation_id',$operations)->get();
            $value_merged = $openingEntryType->toBase()->merge($limitationsType)->sortByDesc('status');
            $person_merged =$persons->toBase()->merge($value_merged)->sortByDesc('status');
            $merged = $person_merged->toBase()->merge($receiptsType)->sortBy('created_at');
//            dd($merged);
            // $posts = $merged->sortBy(function($post) {

            //     if( $post->receipts)
            //     {
            //         return $post->receipts->receiptsType_id;
            //     }else
            //     {
            //         return $post->limitations->receiptsType_id;
            //     }

            // });
            // $posts = $posts->sortBy(function($p) {
            //     if( $p->receipts)
            //     {
            //         return $p->receipts->updated_at;
            //     }else
            //     {
            //         return $p->limitations->updated_at;
            //     }


            // });
            $data = $merged->groupBy(function($date) {
                return session_lang($date->name_en,$date->name_ar);
            });



        }elseif ($operations == 4){
//            $departments = Department::where('id', '>=', $fromtree)->where('id', '<=', $totree)->get();
//            $departments = $departments->map(function ($department){
//                $receiptTypedeb = receiptsType::where('tree_id',$department->id)->sum('debtor');
//                $receiptTypecre = receiptsType::where('tree_id',$department->id)->sum('creditor');
//                $limitationTypedeb = limitationsType::where('tree_id',$department->id)->sum('debtor');
//                $limitationTypecre = limitationsType::where('tree_id',$department->id)->sum('creditor');
//
//                $department->creditor = ($receiptTypecre + $limitationTypecre);
//                $department->debtor = ($receiptTypedeb + $limitationTypedeb);
//                return $department;
//            });
//            dd($departments);

            $departments = Department::where('id', '>=', $fromtree)->where('id', '<=', $totree)->pluck('id')->toArray();
            $department_name     = Department::where('id', '>=', $fromtree)->where('id', '<=', $totree)->pluck('code','dep_name_ar');


            $openingEntry = limitations::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->where('limitationsType_id', 12)->where('branche_id',$branches->id)->pluck('id');
            $limitations_id = limitations::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->whereNotIn('limitationsType_id',[12])->where('branche_id',$branches->id)->orderBy('limitationsType_id', 'ASC')->pluck('id');
            $receipts_id = receipts::where('branche_id',$branches->id)->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->pluck('id');
            $receiptsData = receiptsData::whereHas('departments',function ($query) use ($fromtree,$totree){
                $query->where('id', '>=', $fromtree)->where('id', '<=', $totree);
            })->whereIn('receipts_id',$receipts_id)->get();
//            dd($receiptsData);
            /////////
            $receiptsData = $receiptsData->filter(function ($user) {
                return $user->creditor != '0' || $user->debtor != '0';
            });
            /////////
            $receiptsData = $receiptsData->map(function ($data){
                $data->name_ar = $data->departments->dep_name_ar;
                $data->name_en = $data->departments->dep_name_ar;
                return $data;
            });

            $receiptsType = receiptsType::whereIn('receipts_id',$receipts_id)->whereHas('departments',function ($query) use ($fromtree,$totree){
                $query->where('id', '>=', $fromtree)->where('id', '<=', $totree);
            })->get();
//            0
            /////////
            $receiptsType = $receiptsType->filter(function ($user) {
                return $user->creditor != '0' || $user->debtor != '0';
            });
            /////////

            $EmptyreceiptsType = receiptsType::whereHas('departments',function ($query) use ($fromtree,$totree){
                $query->where('id', '>=', $fromtree)->where('id', '<=', $totree);
            })->get();
            $EmptyreceiptsData = receiptsData::whereHas('departments',function ($query) use ($fromtree,$totree){
                $query->where('id', '>=', $fromtree)->where('id', '<=', $totree);
            })->get();
//            1

            $EmptyreceiptsType = $EmptyreceiptsType->toBase()->merge($EmptyreceiptsData);

                        $EmptyreceiptsType = $EmptyreceiptsType->map(function ($data){
                $data->name_ar_ar = $data->departments->dep_name_ar;
                $data->code = $data->departments->code;
                return $data;
            });

            $receiptsType = $receiptsType->toBase()->merge($receiptsData);
            $limitationsType = limitationsType::whereIn('limitations_id',$limitations_id)->whereHas('departments',function ($query) use ($fromtree,$totree){
                $query->where('id', '>=', $fromtree)->where('id', '<=', $totree);
            })->get();
            /////////
            $limitationsType = $limitationsType->filter(function ($user) {
                return $user->creditor != '0' || $user->debtor != '0';
            });
            /////////

            $openingEntryType = limitationsType::whereIn('limitations_id',$openingEntry)->whereHas('departments',function ($query) use ($fromtree,$totree){
                $query->where('id', '>=', $fromtree)->where('id', '<=', $totree);
            })->get();
            $value_merged = $openingEntryType->toBase()->merge($limitationsType)->sortByDesc('status');

            $merged = $value_merged->toBase()->merge($receiptsType)->sortBy('status');
//            dd($limitationsType);

            $posts = $merged->sortBy(function($post) {
                if( $post->receipts)
                {
                    return $post->receipts->receiptsType_id;
                }else
                {
                    return $post->limitations->receiptsType_id;
                }

            });

            $posts = $posts->sortBy(function($p) {
                if( $p->receipts)
                {
                    return $p->receipts->created_at;
                }else
                {
                    return $p->limitations->created_at;
                }


            });





            $data = $posts->groupBy(function($date) {
                return session_lang($date->name_en,$date->name_ar);
            });

        }

        if ($operations == 5 || $operations == 2){

            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.accountingReports.accountStatement.pdf.report', ['operations'=>$operations,'operation'=>$operation,'data'=>$data,'openingEntryType'=>$openingEntryType, 'to' => $to, 'from' => $from,'from' => $from],[],$config);
            return $pdf->stream();
        }else{

            $config = ['instanceConfigurator' => function($mpdf) {
                $mpdf->SetHTMLFooter('
                    <div style="font-size:10px;width:25%;float:right">Print Date: {DATE j-m-Y H:m}</div>
                    <div style="font-size:10px;width:25%;float:left;direction:ltr;text-align:left">Page {PAGENO} of {nbpg}</div>'
                );
            }];
            $pdf = PDF::loadView('admin.accountingReports.accountStatement.pdf.report2', ['EmptyreceiptsType'=>$EmptyreceiptsType,'operations'=>$operations,'operation'=>$operation,'data'=>$data,'openingEntryType'=>$openingEntryType, 'to' => $to, 'from' => $from, 'tree' => $from, 'department_name' => $department_name],[],$config);
            return $pdf->stream();
        }

    }
}
