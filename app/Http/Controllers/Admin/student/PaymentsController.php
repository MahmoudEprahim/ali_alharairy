<?php

namespace  App\Http\Controllers\Admin\student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Payment;
use App\Student;


use App\DataTables\ExamsDataTable;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class PaymentsController extends Controller
{

    public function index(Request $request){

        if($request->ajax()){
            $allStudentPayment = Payment::where('student_id', $request->student_id)->get();
           
            if(count($allStudentPayment) == 0){
                for ($i=0; $i < count($request->MonthName) ; $i++) { 
                    Payment::create([
                        'student_id' => $request->student_id,
                        'MonthName' => $i+1
                    ]);
                }
            }
        }

    }
    

    public function create(Payment $payment)
    {
        
        $student = Student::pluck('name_'.session('lang'),'id');
        return view('admin.students.create.payment_data', ['student' => $student,'title'=> trans('admin.Create_payment_data')]);
    }



    public function store(Request $request)
    {
        $payments = Payment::where('MonthName', $request->MonthName)->where('student_id', $request->student_id)->first();
        if($request->ajax() && $request->fees != null){
                if($payments){
                    $payments->update(['fees' => $request->fees]);
                }
        } elseif($request->ajax() && $request->fees == null) {
            $payments->update(['fees' => null]);
        }
    }

    public function getStudentPrice(Request $request)
    {
        $payments = Payment::where('student_id', $request->student_id)->get();
       
        return view('admin.students.show.get_payment',compact('payments')); 
    }
    
}
