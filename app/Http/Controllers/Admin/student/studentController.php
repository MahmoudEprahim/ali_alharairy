<?php

namespace App\Http\Controllers\Admin\student;

use App\bus;
use App\city;
use App\Grade;
use App\DataTables\StudentsDataTable;
use App\Branches;
use App\Department;
use App\drivers;
use App\Appointment;
use App\parents;
use App\state;
use App\Student;
use App\country;
use App\Exam;
use App\DataTables\CityDataTable;
use App\Http\Controllers\Controller;
use App\students;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Up;
use DB;

class studentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(StudentsDataTable $student)
    {
        DB::table('students')->where('name_en',NULL)->delete();
        return $student->render('admin.students.index',['title'=>trans('admin.student')]);
    }

    public function create()
    {
        $student = Student::findOrFail(Student::create([
            'name_ar' => '',
        ])->id);
        if (!empty($student)) {
            return redirect(aurl('students/' . $student->id . '/edit'));
        }
    }

    public function edit($id, Request $request)
    {

        $student = Student::findOrFail($id);
        $branches = Branches::pluck('name_'.session('lang'),'id');
        $allStudentPayment = \App\Payment::where('student_id', $student->id)->get();
        $grades = Grade::pluck('name_'.session('lang'),'id');
        $others = Student::whereNotIn('id',[$id])->pluck('name_'.session('lang'),'id');
        $parents = parents::where('name_ar','!=',null)->where('name_en','!=',null)->pluck('name_'.session('lang'),'id');
       $exams = Exam::where('branche_id', $student->branches_id)->where('grade_id', $student->grades_id)->get();

        if($student->appointment){
            $appointments = Appointment::where('id', '!=', $student->appointment->id)->where('branch_id', $student->branches_id)->where('grade_id', $student->grades_id)->get();
        } else {
            $appointments = Appointment::where('branch_id', $student->branches_id)->where('grade_id', $student->grades_id)->get();
        }

        return view('admin.students.create',['title'=> trans('admin.edit_student') ,'appointments' => $appointments, 'allStudentPayment' => $allStudentPayment,
            'student'=>$student,'branche'=>$branches,'parents'=>$parents,'grades'=>$grades,'others'=>$others,'exams'=> $exams]);

    }

    public function update(Request $request, $id)
    {
        $student = Student::findOrFail($id);
        $data = $this->validate($request,[
            'email' => [
                'sometimes',
                Rule::unique('students')->ignore($student->id),
            ],
            'acc_no' => [
                'sometimes',
                Rule::unique('students')->ignore($student->id),
            ],
            
            'phone' => [
                'sometimes',
                Rule::unique('students')->ignore($student->id),
            ],
            
            
            'active' => 'sometimes',
            'name_ar' => 'sometimes',
            'name_en' => 'sometimes',
            'image' => 'sometimes',
            'Joining_date' => 'sometimes',
            'addriss' => 'sometimes',
            'branches_id' => 'sometimes',
            
            'age' => 'sometimes',
            'gender' => 'sometimes',
            'number' => 'sometimes',
            'bus' => 'sometimes',
            'appointments_id' => 'sometimes',
            'password'=> 'sometimes',
            'remember_token'=> 'sometimes',
            'per_status' => 'sometimes',
            'learn_time' => 'sometimes',
            'grades_id' => 'sometimes',
            'birthdate' => 'sometimes',
            'birthplace' => 'sometimes',
            'receivables_past_id' => 'sometimes',
            'receivables_Present_id' => 'sometimes',
            'receivables_future_id' => 'sometimes',
            'activity_revenues_id' => 'sometimes',
            'health_status' => 'sometimes',
            'previous_estimate' => 'sometimes',
            'past_school' => 'sometimes',
            'Joining_date' => 'sometimes',
            'parent_id' => 'sometimes',
            'admin_id' => 'sometimes',
            'operation_id' => 'sometimes',
            'tree_id' => 'sometimes',
            'bus_id' => 'sometimes',
            'driver_id' => 'sometimes',
            'countries_id' => 'sometimes',
            'class_id' => 'sometimes',
            'classroom_id' => 'sometimes',
            'islamic_date' => 'sometimes',
            'islamic_birthdate' => 'sometimes',
            'religion' => 'sometimes',
            'employees_sons' => 'sometimes',
            'education_notes' => 'sometimes',
            'special_needs' => 'sometimes',
            'medical_condition' => 'sometimes',
            'medical_attention' => 'sometimes',
            'medical_notes' => 'sometimes',
            'medical_desc' => 'sometimes',
            'state_id' => 'sometimes',
            'city_id' => 'sometimes',
            'street' => 'sometimes',
            'house_num' => 'sometimes',
            'compound_num' => 'sometimes',
            'compound_name' => 'sometimes',
            'phone_1' => 'sometimes',
            'phone_2' => 'sometimes',
            'near' => 'sometimes',
            'debtor' => 'sometimes',
            'creditor' => 'sometimes',
            'image_1' => 'sometimes',
            'image_2' => 'sometimes',
            'image_3' => 'sometimes',
            'image_4' => 'sometimes',
            'image_5' => 'sometimes',
            'image_6' => 'sometimes',
            'job' => 'sometimes',
            'teacher_notes' => 'sometimes',
            

        ],[],[
            'acc_no' => trans('admin.acc_no'),
            'password' => trans('admin.password'),
            'remember_token' => trans('admin.remember_token'),
            'name_ar' => trans('admin.arabic_name'),
            'name_en' => trans('admin.english_name'),
            'image' => trans('admin.image'),
            'addriss' => trans('admin.addriss'),
            'branches_id' => trans('admin.Branches'),
            'phone' => trans('admin.phone'),
            'type' => trans('admin.type'),
            'grades_id' => trans('admin.grades'),
            'age' => trans('admin.age'),
            'gender' => trans('admin.gender'),
            'bus' => trans('admin.sub_bus'),
            'per_status' => trans('admin.per_status'),
            'learn_time' => trans('admin.learn_time'),
            'bloodtype' => trans('admin.blood'),
            'birthdate' => trans('admin.birth_date'),
            'birthplace' => trans('admin.birth_place'),
            'receivables_past_id' => trans('admin.receivables_past'),
            'receivables_Present_id' => trans('admin.receivables_Present'),
            'receivables_future_id' => trans('admin.receivables_future'),
            'activity_revenues_id' => trans('admin.activity_revenues'),
            'health_status' => trans('admin.health_status'),
            'previous_estimate' => trans('admin.previous_estimate'),
            'past_school' => trans('admin.past_school'),
            'Joining_date' => trans('admin.Joining_date'),
            'religion' => trans('admin.Joining_date'),
            'employees_sons' => trans('admin.religion'),
            'education_notes' => trans('admin.employees_sons'),
            'special_needs' => trans('admin.education_notes'),
            'medical_condition' => trans('admin.special_needs'),
            'medical_attention' => trans('admin.medical_condition'),
            'medical_notes' => trans('admin.medical_attention'),
            'medical_desc' => trans('admin.medical_notes'),
            'state_id' => trans('admin.state_id'),
            'city_id' => trans('admin.city_id'),
            'street' => trans('admin.street'),
            'house_num' => trans('admin.house_num'),
            'compound_num' => trans('admin.compound_num'),
            'compound_name' => trans('admin.compound_name'),
            'phone_1' => trans('admin.phone_1'),
            'phone_2' => trans('admin.phone_2'),
            'near' => trans('admin.near'),
        ]);
        
        
        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'students',
                'upload_type' => 'single',
                'delete_file'=> $student->image
            ]);
        }
        
        $data['name_ar'] =  $request->name_en; 
        $data['password'] =   Hash::make($request->password);
        $data['remember_token'] = Hash::make($request->remember_token);
        
        // dd($request->appointments_id !== null); // [true] selected branch and grade
        // dd($request->appointments_id !== "null"); // [true] not selected branch and grade
        if($request->appointments_id !== "null" && $request->appointments_id != null){
            $appointment = Appointment::findOrFail($request->appointments_id);
            $appointment->update(['count' => $appointment->count+1]);
            if($student->appointments_id != null){
                $oldAppointment = Appointment::findOrFail($student->appointments_id);
                $oldAppointment->update(['count' => $appointment->count-1]);
            }
        } else {
            $data['appointments_id'] = $student->appointments_id;
        }
       

        
       
        // parent date 
        $fullName = explode(" ", $request->name_en);
        $fullName = array_slice($fullName, 1);
        $parentName = implode(' ', $fullName);
        
        
        if($student->parent_id == null Or $student->parent_id == "null"){
            if($parentName == ''){
                $parent = parents::create([
                    'name_en' => $request->name_en,
                    'name_ar' => $request->name_en,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'mobile_1' => $request->mobile_1,
                    'job' => $request->job
                ]);
            }else{
                $parent = parents::create([
                    'name_en' => $parentName,
                    'name_ar' => $parentName,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'mobile_1' => $request->mobile_1,
                    'job' => $request->job
                ]);
            }
            Student::find($student->id)->update(['parent_id' => $parent->id]);
        }
       
        $parent = Parents::where('id', $student->parent_id)->first();
       
        if($parent != null){
            if($parentName == ''){
                $parent->update([
                    'name_en' => $request->name_en,
                    'name_ar' => $request->name_en,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'mobile_1' => $request->mobile_1,
                    'job' => $request->job
                ]);
            }else{
                $parent->update([
                    'name_en' => $parentName,
                    'name_ar' => $parentName,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'mobile_1' => $request->mobile_1,
                    'job' => $request->job
                ]);
            }
        }
        
        // parents::where('id',);
        // dd($data);
        $student->update($data);
        return redirect(aurl('students'))->with(session()->flash('message',trans('admin.success_update')));
    }

    public function destroy($id)
    {

        $student = Student::findOrFail($id);
        $student->delete();
        return redirect(aurl('students'));
    }


    // appointmwnts ajax
    public function grades_data(Request $request)
    {
        if($request->ajax() && $request->branches_id){

            if($branch != null && $grade_id != null){
                if($grade_id == 1){
                    $appointments  = Appointment::where(['grade_id','1'])->get();
                    $data = view('admin.students.subappointmets', ['appointments'=> $appointments])->render();
                    return $data;
                }
                if($grade_id == 2){
                    $appointments  = Appointment::where(['grade_id','2'])->get();
                    $data = view('admin.students.subappointmets', ['appointments'=> $appointments])->render();
                    return $data;
                }
                if($grade_id == 3){
                    $appointments  = Appointment::where(['grade_id','3'])->get();
                    $data = view('admin.students.subappointmets', ['appointments'=> $appointments])->render();
                    return $data;
                }

            }
        }

    }
    // appointments ajax

    // activate
    public function studentActive(Request $request){
        if($request->ajax()){
            $student = Student::find($request->id);
            if($student != 'null'){
                if ($student->active == 0){
                    $student->update(['active' => 1]);
                    return response()->json(['status'=>1, 'class' => 'btn-success', 'icon' => 'fa-check']);
                } elseif($student->active == 1){
                    $student->update(['active' => 0]);
                    return response()->json(['status'=>1,'class' => 'btn-warning', 'icon' => 'fa-close']);
                }

            }
        }
    }

}
