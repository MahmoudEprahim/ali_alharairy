<?php

namespace  App\Http\Controllers\Admin\student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Student;
use App\Exam;
use App\StudentExam;


use App\DataTables\StudentExamsDataTable;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class StudentExamsController extends Controller
{
    public function index(StudentExamsDataTable $stdentgrade)
    {
        return $stdentgrade->render('admin.students.studentexams.index',['title'=>trans('admin.studentexams_information')]);
    }

    public function create(StudentExam $stdentgrade)
    {
        $student = Student::pluck('name_'.session('lang'),'id');
        $stdentgrade = Exam::pluck('name_'.session('lang'),'id');

        return view('admin.students.studentexams.create', ['student' => $student,'stdentgrade' => $stdentgrade,'title'=> trans('admin.Create_new_exam_to_student')]);
    }

    public function store(Request $request, Exam $stdentgrade)
    {

         $data= $this->validate($request,[
            'grade' => 'required',
            'student_id' => 'required',
            'exam_id' => 'required',
        ],[],[
            'grade'=> trans('admin.grade'),
            'student_id' => trans('admin.student_id'),
            'exam_id' => trans('admin.exam_id'),
        ]);

        $stdentgrade->create($data);

        return redirect(aurl('exams'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {

        $stdentgrade = Exam::findOrFail($id);

        return view('admin.students.exams.show',compact('Appointment'));
    }

    public function edit($id)
    {

        $branche = Branches::pluck('name_'.session('lang'),'id');
        $grade = Grade::pluck('name_'.session('lang'),'id');
        $stdentgrade = Exam::findOrFail($id);
        return view('admin.students.exams.edit',['exam'=> $stdentgrade,'grade'=>$grade, 'branche'=> $branche,'title'=>trans('admin.edit_Appointment')]);
    }

    public function update(Request $request, $id)
    {
        $stdentgrade = Exam::findOrFail($id);
        $data= $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
            'image' => 'sometimes',
            'grade_id' => 'required',
            'branche_id' => 'sometimes',
        ],[],[
            'name_ar'=> trans('admin.name_ar'),
            'name_en' => trans('admin.name_en'),
            'image' => trans('admin.image'),
            'grade_id' => trans('admin.grade_id'),
            'branche_id' => trans('admin.branche_id'),
        ]);



        if($request->hasFile('image')){
            $slider['image'] = Up::upload([
                'request' => 'image',
                'path'=>'exams',
                'upload_type' => 'single'
            ]);
        }


        $stdentgrade->update($data);

        $stdentgrade->save();

        return redirect(aurl('exams'))->with(session()->flash('message',trans('admin.success_update')));

        }


    public function destroy($id)
    {
        $stdentgrade = Exam::findOrFail($id);
        $stdentgrade->delete();
        return redirect(aurl('exams'));
    }

}
