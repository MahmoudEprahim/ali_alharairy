<?php

namespace App\Http\Controllers\Admin\student;

use App\StudentExam;
use App\classes;
use App\classroom;
use App\country;
use App\Department;
use App\drivers;
use App\parents;
use App\Appointment;
use App\Student;
use App\Exam;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class subbusController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()){
            $bus = $request->bus;
            $buses = bus::pluck('busnumber','id');
            $drivers = drivers::pluck('name_'.session('lang'),'id');
            $student = students::where('id',$request->student)->first();
            if($bus != 0){
                $content = view('admin.students.subbus',['buses'=>$buses,'student'=>$student,'drivers'=>$drivers])->render();
                return $content;
            };
        }
    }
    public function substudent(Request $request){
        if ($request->ajax()){
            $student = Student::where('id',$request->students)->first();

            $self = Student::where('id',$request->self)->first();

            $self->other_students()->attach($student);

            $content = view('admin.students.substudent',['self'=>$self])->render();
            return $content;
        }
    }
    public function subparents(Request $request){
        if ($request->ajax()){
            $parent = parents::where('id',$request->parents)->first();
            $countries = country::pluck('country_name_'.session('lang'),'id');
            $departments = Department::where('operation_id',2)->pluck('dep_name_'.session('lang'),'id');
            $content = view('admin.students.subparents',['parent'=>$parent,'departments'=>$departments,'countries'=>$countries])->render();
            return $content;
        }
    }
    // appointments
    public function subappointments(Request $request){
        if ($request->ajax()){
            $appointments = Appointment::pluck('groups','id');
           
            $content = view('admin.students.subappointmets',['appointments'=>$appointments])->render();
            return $content;
        }
    }

    
    // exams
    public function subexams(Request $request){

        if ($request->ajax()){
            $student = Student::where('id',$request->student)->first();
            $exam = Exam::where('id',$request->exams)->first();
            $grade = $request->grade;

            if(DB::table('studentexams')->where('student_id',$student->id)->where('exam_id',$exam->id)->count() == 0)
            {
                $student->exams()->attach($exam, ['grade' => $request->grade]);
                $content = view('admin.students.sub_exam', ['student' => $student])->render();
                return $content;
            }else
            {
                $content = view('admin.students.sub_exam', ['student' => $student])->render();
                return $content;
            }
        }
    }

    public function remove_subparents($id, $idd){

        $student = Student::where('id',$idd)->first();

        $parent_id = Student::where('id',$id)->first();
        $student->other_students()->detach($parent_id);


        return back();
    }
    public function remove_subexam($id, $exam){

        $student = Student::where('id',$id)->first();
        $exam = Exam::where('id',$exam)->first();
        $student->exams()->detach($exam);
        return back();
    }
    public function phase(Request $request){
        if ($request->ajax()){
            $learn_time = $request->learn_time;
            $level = $request->level;
            $classes = classes::where('grades_id',$level)->pluck('name_'.session('lang'),'id');
            $classes_id = classes::where('grades_id',$level)->pluck('id');
            $classrooms = classroom::whereIn('id',$classes_id)->pluck('name_class','id');
            $student = students::where('id',$request->student)->first();
            $content = view('admin.students.phase',['classrooms'=>$classrooms,'classes'=>$classes,'level'=>$level,'student'=>$student])->render();
            return $content;
        }
    }
    public function classesphase(Request $request){
        if ($request->ajax()){
            $classrooms = classroom::where('class_id',$request->classes)->pluck('name_class','id');
            $student = students::where('id',$request->student)->first();
            $content = view('admin.students.classesphase',['classrooms'=>$classrooms,'student'=>$student])->render();
            return $content;
        }
    }


}
