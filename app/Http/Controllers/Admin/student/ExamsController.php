<?php

namespace  App\Http\Controllers\Admin\student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Branches;
use App\Grade;
use App\Exam;
use App\Student;


use App\DataTables\ExamsDataTable;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class ExamsController extends Controller
{
    public function index(ExamsDataTable $exam)
    {
        return $exam->render('admin.students.exams.index',['title'=>trans('admin.exams_information')]);
    }

    public function create(Exam $exam)
    {
        $grade = Grade::pluck('name_'.session('lang'),'id');
        $branche = Branches::pluck('name_'.session('lang'),'id');
        return view('admin.students.exams.create', ['branche' => $branche,'grade' => $grade,'title'=> trans('admin.Create_new_exam')]);
    }

    public function store(Request $request, Exam $exam)
    {
         $data= $this->validate($request,[
            'exam_date' => 'required',
            'name_en' => 'required',
            'image' => 'sometimes',
            'grade_id' => 'required',
            'branche_id' => 'sometimes',
        ],[],[
            'exam_date'=> trans('admin.name_ar'),
            'name_en' => trans('admin.name_en'),
            'image' => trans('admin.image'),
            'grade_id' => trans('admin.grade_id'),
            'branche_id' => trans('admin.branche_id'),
        ]);

        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'exam',
                'upload_type' => 'single',
            ]);
        }
        $exam->create($data);
        return redirect(aurl('exams'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
        $exam = Exam::findOrFail($id);
        return view('admin.students.exams.show',compact('exam'));
    }

    public function edit($id)
    {

        $branche = Branches::pluck('name_'.session('lang'),'id');
        $grade = Grade::pluck('name_'.session('lang'),'id');
        $exam = Exam::findOrFail($id);
        return view('admin.students.exams.edit',['exam'=> $exam,'grade'=>$grade, 'branche'=> $branche,'title'=>trans('admin.edit_exam')]);
    }

    public function update(Request $request, $id)
    {
        $exam = Exam::findOrFail($id);
        $data= $this->validate($request,[
            'exam_date' => 'required',
            'name_en' => 'required',
            'image' => 'sometimes',
            'grade_id' => 'required',
            'branche_id' => 'sometimes',
        ],[],[
            'exam_date'=> trans('admin.name_ar'),
            'name_en' => trans('admin.name_en'),
            'image' => trans('admin.image'),
            'grade_id' => trans('admin.grade_id'),
            'branche_id' => trans('admin.branche_id'),
        ]);
        if($request->hasFile('image')){
            $data['image'] = Up::upload([
                'request' => 'image',
                'path'=>'exam',
                'upload_type' => 'single',
                'delete_file'=> $exam->image
            ]);
        }
        $exam->update($data);
        $exam->save();
        return redirect(aurl('exams'))->with(session()->flash('message',trans('admin.success_update')));
    }

    public function destroy($id)
    {
        $exam = Exam::findOrFail($id);
        $exam->delete();
        return redirect(aurl('exams'));
    }


    public function show_exams(Request $request)
    {
        if($request->ajax() && ($request->branch_id && $request->grade_id && $request->student_id)){
            $exams = Exam::where('branche_id', $request->branch_id)->where('grade_id', $request->grade_id)->get();
            $student = Student::findOrFail($request->student_id);
            return view('admin.students.exams.exams_data',compact(['exams', 'student']));
        }
    }

}
