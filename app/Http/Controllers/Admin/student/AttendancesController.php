<?php

namespace App\Http\Controllers\Admin\student;

use App\Attendance;
use App\DataTables\AttendancesDataTable;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttendancesController extends Controller
{
    
    public function index(AttendancesDataTable $dataTable)
    {
        return  $dataTable->render('admin.students.attendance.index');
    }

   
    public function create()
    {
        $students = Student::all();
        return  view('admin.students.attendance.create', compact(['students']));
    }

    
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'student_id' => 'required',
            'month_id' => 'required',
            'lecture_id' => 'required|array',
        ], [] ,[
            'student_id' => trans('admin.student'),
            'month_id' => trans('admin.month'),
            'lecture_id' => trans('admin.lecture'),
        ]);
        
        

        $attendance = Attendance::where('student_id', $request->student_id)->where('month_id', $request->month_id)->first();
        // dd($request->all());
        if ($attendance == null){
            Attendance::create([
                'student_id' => $request->student_id,
                'month_id' => $request->month_id,
                'lecture_id' => json_encode($request->lecture_id),
            ]);
        } else {
            $attendance->update(
                ['lecture_id' => json_encode($request->lecture_id)]
            );
        }


    //    foreach ($request->lecture_id as $key => $lecture){
    //        Attendance::create([
    //            'student_id' => $request->student_id,
    //            'month_id' => $request->month_id,
    //            'lecture_id' => json_encode([
    //             $request->lecture_id => $request->lecture_date
    //         ]),
    //        ]);
    //    }

        return  redirect()->route('attendance.index')->with(session()->flash('message', trans('admin.success')));
    }

    public function show($id)
    {
        $attendance = Attendance::findOrFail($id);
        return view('admin.students.attendance.show',compact('attendance'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Student::all();
        $attendance = Attendance::findOrFail($id);
        return  view('admin.students.attendance.edit', compact(['attendance','students']));
    }

   
    public function update(Request $request, $id)
    {
        $data = $this->validate($request, [
            'student_id' => 'required',
            'month_id' => 'sometimes',
            'lecture_id' => 'sometimes|array',
        ], [] ,[
            'student_id' => trans('admin.student'),
            // 'month_id' => trans('admin.month'),
            'lecture_id' => trans('admin.lecture'),
        ]);

        $attendance = Attendance::findOrFail($id);
        $attendance->update(
            [
                'lecture_id' => json_encode($request->lecture_id),
                'month_id' => $request->month_id
            ]
        );

        return  redirect()->route('attendance.index')->with(session()->flash('message', trans('admin.success_update')));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attendance = Attendance::findOrFail($id);
        $attendance->delete();
        return  redirect()->route('attendance.index')->with(session()->flash('message', trans('admin.success_update')));
    }
}
