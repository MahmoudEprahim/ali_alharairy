<?php

namespace App\Http\Controllers\Admin\student;

use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Branches;
use App\Grade;
use App\Appointment;


use App\DataTables\AppointmentsDataTable;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;

class AppointmentController extends Controller
{
    public function index(AppointmentsDataTable $appointment)
    {

        return $appointment->render('admin.students.appointment.index',['title'=>trans('admin.appointment_information')]);
    }

    public function create(Appointment $appointment)
    {
        $grade = Grade::pluck('name_'.session('lang'),'id');
        $branche = Branches::pluck('name_'.session('lang'),'id');

        return view('admin.students.appointment.create', ['branche' => $branche,'grade' => $grade,'title'=> trans('admin.Create_new_appointment')]);
    }

    public function store(Request $request, Appointment $appointment)
    {

         $data= $this->validate($request,[
            'groups' => 'required',
            'days' => 'required',
            'capacity' => 'required',
            'grade_id' => 'required',
            'branch_id' => 'required',
        ],[],[
            'groups'=> trans('admin.appointment'),
            'capacity' => trans('admin.capacity'),
            'days' => trans('admin.teacher'),
            'grade_id' => trans('admin.grade_id'),
            'branch_id' => trans('admin.branches_id'),
        ]);

        $appointment->create($data);

        return redirect(aurl('appointments'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {

        $appointment = Appointment::findOrFail($id);
        return view('admin.students.appointment.show',compact('appointment'));
    }

    public function edit($id)
    {

        $branche = Branches::pluck('name_'.session('lang'),'id');
        $grade = Grade::pluck('name_'.session('lang'),'id');
        $appointment = Appointment::findOrFail($id);
        return view('admin.students.appointment.edit',['appointment'=> $appointment,'grade'=>$grade, 'branche'=> $branche,'title'=>trans('admin.edit_Appointment')]);
    }

    public function update(Request $request, $id)
    {
        $appointment = Appointment::findOrFail($id);
        $data= $this->validate($request,[
            'groups' => 'sometimes',
            'days' => 'sometimes',
            'capacity' => 'sometimes',
            'grade_id' => 'sometimes',
            'branch_id' => 'sometimes',
        ],[],[

        ]);
        $appointment->update($data);
        $appointment->save();
        return redirect(aurl('appointments'))->with(session()->flash('message',trans('admin.success_update')));
    }


    public function destroy($id)
    {
        $appointment = Appointment::findOrFail($id);
        $appointment->delete();
        return redirect(aurl('appointments'));
    }


    public function show_appointments(Request $request)
    {
        if($request->ajax() && ($request->branch_id && $request->grade_id && $request->student_id)){
            $appointments = Appointment::where('branch_id', $request->branch_id)->where('grade_id', $request->grade_id)->get();
            $student = Student::findOrFail($request->student_id);
            return view('admin.students.appointment.get_appointment_for_admin',compact(['appointments', 'student']));
        }
    }
    

//    public function appointmentCountUpdate(Request $request)
//    {
//        if($request->ajax() && $request->id){
//
//            $appointment = Appointment::findOrFail($request->id)->first();
//            $appointment->update(['count' => $appointment->count+1]);
//            return response()->json(['status' => 1, 'msg' => 'Appointment has been reserved for you']);
//        }
//    }

    public function getStudentPrice(Request $request)
    {

        $payments = Payment::where('student_id', $request->student_id)->get();

        return view('admin.students.show.get_payment',compact('payments'));


    }


}
