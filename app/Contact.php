<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $fillable = [
        'name_ar',
        'job_desc',
        'mob',
        'mob_2',
        'email',
        'company_id',
    ];

    public function company() {
        return$this->hasOne('App\Company','id','company_id');
    }
}
