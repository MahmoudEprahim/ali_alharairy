<?php
return
[
    //Page Header
    'Ali_Elhariry' => 'المستر / علي الحريري',

    'there_is_error_in_inputs' => 'يوجد خطأ في المدخلات',
    'Success_login' => 'تم التسجيل بنجاح',
    'grades_id' => 'اختيار الصف',
    //Page student_register

    'success_student_register' => 'تم التسجيل بنجاح',
    'Private_Groups' => 'التسجيل في مجموعة',
    'Enroll_in_summer_cuorse' => 'التسجيل في الدورة الصيفية',
    'Upload_photo' => 'رفع الصورة',
     'appointments_id' => 'المواعيد',
    'Center' => 'مركز',
    'Full_name' => 'الاسم بالكامل',
    'select_grade' => 'اختار الصف',
    'School' => 'المدرسة',
    'Mobile_no' => 'رقم التلفون',
    'Address' => 'العنوان',
    'Parent\'s_mobile no' => 'رقم تلفون  ولي الامر',
    'beginning_attendance' => 'بداية الحضور',
    'Email' => 'البريد الالكتروني',
    'password' => 'كلمة المرور',
    'confirm_password' => 'تأكيد كلمة المرور',
    'Groups' => 'المجموعات',
    'Days' => 'الايام',
    'Group_Availability' => 'المجموعات المتاحة',
    'Available_Places' => 'الاماكن المتاحة',
    'Enroll' => 'التحق',
    'Student_register' => 'تسجيل الطالب',
    // footer
    'Home' => 'الرئيسية',
    'profile' => 'السيرة الذاتية',
    'Secondary_school_gate' => 'بوابة الثانوية العامة',
    'English_language_gate' => 'بوابة اللغة الانجليزية',
    'Aga' => 'أجا',
    'importtant_links' => 'روابط مهمة',
    'Location' => 'موقع',
    'videos' => 'فيديوهات',

    'Copyrights_Reserved' => 'إنفوسيز © 2020 حقوق النشر محفوظة',
    'Recommended' => 'الموصي به',
    'Lectures' => 'محاضرات',
    'Comics' => 'كاريكاتير',
    'video_decription' => 'وصف الفيديو',
    'photos_galerry' => 'معرض الصور',
    'video_gallery' => 'معرض الفيديو',
    'PREVIOUS' => 'السابق',
    'next' => 'اللاحق',
    'Photos' => 'الصور',
    'ABOUT' => 'السيرة الذاتية للمستر',
    'Honor_Frame' => 'لوحة الشرف',
    'What_people_say_about_us' => 'ماذا يقول الطلاب عنا',
    'Name' => 'ادخل اسمك من فضلك',
    'Current_work' => 'العمل الحالي',
    'Say_opinion' => 'شاركنا برأيك',
    'graduation_year' => 'سنة التخرج',
    'Leave_comment' => 'من فضلك اترك تعليق',
    'Submit' => 'إرسال',
    'english-gate'=>'بوابة اللغة الانجليزية',
    'login' =>'التسجيل',
    'register' =>'التسجيل',
    'You_don_have_account_Register' =>'إنشاء حساب جديد',
    'Do_you_already_have_account_Sign_in' =>'هل لديك حساب بالفعل؟ تسجيل الدخول',
    'Log_in' =>'تسجيل الدخول',
    'Remembe_me' =>'تذكرنى',
    'Log_out' =>'تسجيل خروج',
    'Did_you_forget_password' =>'هل نسيت رقم السري',
    'log_in' =>'تسجيل الدخول',





];
