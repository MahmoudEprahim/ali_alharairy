<?php
return
[
    //Page Header
    'Ali_Elhariry' => 'Mr/Ali  Elhariry',
    'there_is_error_in_inputs' => 'there is error in inputs',
    'Success_login' => 'Success login',
    'grades_id' => 'Choose Grade',
    //Page student_register

    'success_student_register' => 'successfully registered',
    'Private_Groups' => 'Private Groups',
    'Enroll_in_summer_cuorse' => 'Enroll in Summer Course',
      'appointments_id' => 'appointments',
    'Upload_photo' => 'Upload Photo',
    'Center' => 'Center',
    'Full_name' => 'Full Name',
    'select_grade' => 'Select Grade',
    'School' => 'School',
    'Address' => 'Address',
    'Mobile_no' => 'Mobile no',
    'Parent\'s_mobile no' => 'Parent\'s mobile no',
    'beginning_attendance' => 'The beginning of attendance',
    'Email' => 'Email',
    'password' => 'password',
    'confirm_password' => 'confirm password',
    'Groups' => 'Groups',
    'Days' => 'Days',
    'Group_Availability' => 'Group Availability',
    'Available_Places' => 'Available Places',
    'Enroll' => 'Enroll',
    'Student_register' => 'Student Register',

    // footer
    'Home' => 'Home',
    'profile' => 'Profile',
    'Secondary_school_gate' => 'Secondary School Gate',
    'English_language_gate' => 'English Language Gate',
    'Copyrights_Reserved' => 'Infosas © 2020 Copyrights Reserved',
    'importtant_links' => 'important links',
    'Location' => 'Location',
    'videos' => 'videos',
    'Recommended' => 'Recommended',
    'Lectures' => 'Lectures',
    'Comics' => 'Comics',
    'video_decription' => 'Video Decription',
    'photos_galerry' => 'Photos Galerry',
    'video_gallery' => 'Videos Galerry',
    'PREVIOUS' => 'PREVIOUS',
    'Next' => 'Next',
    'Photos' => 'Photos',
    'ABOUT' => 'BIOGRAPHY OF THE MASTER',
    'What_people_say_about_us' => 'What People Say About Us',
    'Name' => 'Please enter your name',
    'Current_work' => 'Current Work',
    'Say_opinion' => 'Share your opinion',
    'graduation_year' => 'Graduation Year',
    'Leave_comment' => 'Please leave a comment',
    'Submit' => 'Submit',
    'HONOR_FRAME' =>'HONOR FRAME' ,
    'english-gate' =>'English Gate',
    'login' =>'login',
    'register' =>'register',
    'You_don_have_account_Register' =>'You don\'t have account ? Register',
    'Do_you_already_have_account_Sign_in' =>'Do you already have an account? Sign in',
    'Log_in' =>'Log In',
    'Remembe_me' =>'Remember Me',
    'Log_out' =>'Log out',
    'Did_you_forget_password' =>'Did you forget the password?',
    'log_in' =>'Log In',


];
