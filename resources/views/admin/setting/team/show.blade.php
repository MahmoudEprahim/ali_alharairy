@extends('admin.index')
@section('title',trans('admin.show_team') . $team->name_en)
@section('content')


    @push('js')
        <script>
            function myJsFunc() {
                var i = 0;
                ++i;
                var newInput = $('.new-row').html();
                $('.new-one').append(newInput);
            }
        </script>
    @endpush
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">

            <div class="form-group row">
                
                <div class="col-md-3">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{ $team->name_en }}</div>
                </div>
            
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.teams_title_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{ $team->teams_title_en }}</div>
                </div>
                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.teams_desc_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{ $team->teams_desc_en }}</div>
                </div>
            
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.facebook'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{ $team->facebook }}</div>
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.twitter'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{ $team->twitter }}</div>
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.google'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{ $team->google }}</div>
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.linkedin'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{ $team->linkedin }}</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">
                    {{ Form::label(trans('admin.email'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{ $team->email }}</div>
                </div> <div class="col-md-3">
                    {{ Form::label(trans('admin.phone'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{ $team->phone }}</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6 ">
                    <img style="width: 100%;height: 100%;" src="{{asset('storage/'. $team->image) }}">
                </div>
            </div>



            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('slider/')}}" class="btn btn-danger">{{trans('admin.back')}} </a>
            {!! Form::close() !!}
        </div>

    </div>
    </div>








@endsection
