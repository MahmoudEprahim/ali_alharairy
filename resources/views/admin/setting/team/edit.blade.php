@extends('admin.index')
@section('title',trans('admin.edit_team') . $team->name_ar)
@section('content')


    @push('js')
        <script>
            function myJsFunc() {
                var i = 0;
                ++i;
                var newInput = $('.new-row').html();
                $('.new-one').append(newInput);
            }
        </script>
    @endpush
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($team,['method'=>'PUT','route' => ['team.update',$team->id],'files'=>true]) !!}

            <div class="form-group row">
                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-6 ">
                    <img style="width: 100%;height: 100%;" src="{{asset('storage/'. $team->image) }}">
                  </div>

                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_ar', $team->name_ar, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_en',  $team->name_en, array_merge(['class' => 'form-control'])) }}
                </div>
                
                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.teams_title_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('teams_title_en',$team->teams_title_en, array_merge(['class' => 'form-control'])) }}
                </div>

                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.teams_desc_en'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('teams_desc_en',$team->teams_desc_en, array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.facebook'), null, ['class' => 'control-label']) }}
                    {{ Form::text('facebook', $team->teams_desc_en, array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.twitter'), null, ['class' => 'control-label']) }}
                    {{ Form::text('twitter', $team->teams_desc_en, array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.google'), null, ['class' => 'control-label']) }}
                    {{ Form::text('google',$team->google, array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.linkedin'), null, ['class' => 'control-label']) }}
                    {{ Form::text('linkedin', $team->linkedin, array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">
                    {{ Form::label(trans('admin.email'), null, ['class' => 'control-label']) }}
                    {{ Form::text('email', $team->email, array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div> <div class="col-md-3">
                    {{ Form::label(trans('admin.phone'), null, ['class' => 'control-label']) }}
                    {{ Form::text('phone', $team->phone, array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
            </div>

            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('slider/')}}" class="btn btn-danger">{{trans('admin.back')}} </a>
            {!! Form::close() !!}
        </div>

    </div>
    </div>








@endsection
