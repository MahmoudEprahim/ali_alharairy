@extends('admin.index')
@section('title',trans('admin.add_new_team'))
@section('content')


    @push('js')
        <script>
            function myJsFunc() {
                var i = 0;
                ++i;
                var newInput = $('.new-row').html();
                $('.new-one').append(newInput);
            }
        </script>
    @endpush
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">


        

        {!! Form::open(['method'=>'POST','route' => 'team.store','files'=>true]) !!}
            <div class="form-group row">
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                </div>

                <div class="col-md-6 hidden">
                    {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_ar', old('name_ar'), array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_en', old('name_en'), array_merge(['class' => 'form-control'])) }}
                </div>


            
                <div class="col-md-6 hidden">
                    {{ Form::label(trans('admin.teams_title_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('teams_title_ar',  old('teams_title_ar'), array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.teams_title_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('teams_title_en', old('teams_title_en'), array_merge(['class' => 'form-control'])) }}
                </div>


            
                <div class="col-md-6 hidden">
                    {{ Form::label(trans('admin.teams_desc_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('teams_desc_ar', old('teams_desc_ar'), array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
                <div class="col-md-12 ">
                    {{ Form::label(trans('admin.teams_desc_en'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('teams_desc_en', old('teams_desc_en'), array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>


            </div>
            <div class="form-group row">
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.facebook'), null, ['class' => 'control-label']) }}
                    {{ Form::text('facebook', old('facebook'), array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.twitter'), null, ['class' => 'control-label']) }}
                    {{ Form::text('twitter', old('twitter'), array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.google'), null, ['class' => 'control-label']) }}
                    {{ Form::text('google',  old('google'), array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.linkedin'), null, ['class' => 'control-label']) }}
                    {{ Form::text('linkedin', old('linkedin'), array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>
                </div>
                <div class="form-group row">
                <div class="col-md-3">
                    {{ Form::label(trans('admin.email'), null, ['class' => 'control-label']) }}
                    {{ Form::text('email', old('email'), array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div> <div class="col-md-3">
                    {{ Form::label(trans('admin.phone'), null, ['class' => 'control-label']) }}
                    {{ Form::text('phone', old('phone'), array_merge(['class' => 'form-control','style' =>'resize:none'])) }}
                </div>


            </div>



            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('slider/')}}" class="btn btn-danger">{{trans('admin.back')}} </a>
            {!! Form::close() !!}
        </div>

    </div>
    </div>








@endsection
