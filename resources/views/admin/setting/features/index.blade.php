@extends('admin.index')
@section('title',trans('admin.dashboard_setting'))
@section('content')
    @push('css')
        <style>
            .icon-popup
            {
                top: 919.264px !important;
                right: 279px !important;
                left: 0px !important;
                display: block !important;
                width: 18%;

            }

        </style>
     @endpush 


     <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.dashboard_setting')}} </h3>
        </div>


        <div class="box-body">
            @include('admin.layouts.message')
            {!! Form::open(['route'=>'features.save','files' => true]) !!}

        <div class="row form-group">
            <div class="col-sm-6 hidden">
                {{ Form::label(trans('admin.features_title_ar'), null, ['class' => 'control-label']) }}
                {{ Form::text('features_title_ar', $feature->features_title_ar, array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-sm-4">
                {{ Form::label(trans('admin.features_title_en'), null, ['class' => 'control-label']) }}
                {{ Form::text('features_title_en', $feature->features_title_en, array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-sm-4">
                {{ Form::label(trans('admin.main_image'), null, ['class' => 'control-label']) }}
                {{ Form::file('main_image', array_merge(['class' => 'form-control'])) }}
                @if(!empty($feature->main_image))
                    <img src="{{asset('storage/'.$feature->main_image)}}" style="width: 50px; margin-top: 20px" class="img-responsive" >
                @endif
            </div>
            
        </div>

        <div class="row form-group">
                <div class="col-sm-4">
                    {{ Form::label(trans('admin.image_1'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image_1', array_merge(['class' => 'form-control'])) }}
                    @if(!empty($feature->image_1))
                        <img src="{{asset('storage/'.$feature->image_1)}}" style="width: 50px; margin-top: 20px" class="img-responsive" >
                    @endif
                </div>
                <div class="col-md-4 hidden">
                    {{ Form::label(trans('admin.title_1_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_1_ar', $feature->title_1_ar, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.title_1_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_1_en', $feature->title_1_en, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-4">
                    {{ Form::label(trans('admin.image_2'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image_2', array_merge(['class' => 'form-control'])) }}
                    @if(!empty($feature->image_2))
                        <img src="{{asset('storage/'.$feature->image_2)}}" style="width: 50px; margin-top: 20px" class="img-responsive" >
                    @endif
                </div>
                <div class="col-md-4 hidden">
                    {{ Form::label(trans('admin.title_2_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_2_ar', $feature->title_2_ar, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.title_2_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_2_en', $feature->title_2_en, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-4">
                    {{ Form::label(trans('admin.image_3'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image_3', array_merge(['class' => 'form-control'])) }}
                    @if(!empty($feature->image_3))
                        <img src="{{asset('storage/'.$feature->image_3)}}" style="width: 50px; margin-top: 20px" class="img-responsive" >
                    @endif
                </div>
                <div class="col-md-4 hidden">
                    {{ Form::label(trans('admin.title_3_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_3_ar', $feature->title_3_ar, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.title_3_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_3_en', $feature->title_3_en, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-4">
                    {{ Form::label(trans('admin.image_4'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image_4', array_merge(['class' => 'form-control'])) }}
                    @if(!empty($feature->image_4))
                        <img src="{{asset('storage/'.$feature->image_4)}}" style="width: 50px; margin-top: 20px" class="img-responsive" >
                    @endif
                </div>
                <div class="col-md-4 hidden">
                    {{ Form::label(trans('admin.title_4_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_4_ar', $feature->title_4_ar, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.title_4_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_4_en', $feature->title_4_en, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>

            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
        <a href="{{aurl('setting')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        {!! Form::close() !!}


        </div>
     </div>
     @endsection