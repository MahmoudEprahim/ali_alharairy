@extends('admin.index')
@section('title',trans('admin.Create_new_englishdescription'))
@section('content')


@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush

<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
<div class="box-body">
    {!! Form::open(['method'=>'POST','route' => 'homevideos.store','files'=>true]) !!}
    <div class="form-group row">
            <div class="col-md-4">
                {{ Form::label(trans('admin.branch_id'),null, ['class' => 'control-label']) }}
                {{ Form::select('branch_id',$branch,  null, array_merge(['class' => 'form-control branche_id', 'placeholder'=>trans('admin.select')])) }}
            </div>
             <div class="col-md-8">
                 {{ Form::label(trans('admin.video_src'), null, ['class' => 'control-label']) }}
                 {{ Form::text('video_src',null, array_merge(['class' => 'form-control'])) }}
             </div>
    </div>



    <div class="row">
        <div class="col-md-6">
            {{ Form::label(trans('admin.desc_ar'), null, ['class' => 'control-label']) }}
            {{ Form::textarea('desc_ar',null, array_merge(['class' => 'form-control','style' => 'resize:none'])) }}
        </div>
        <div class="col-md-6">
            {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
            {{ Form::textarea('desc_en',null, array_merge(['class' => 'form-control','style' => 'resize:none'])) }}
        </div>

    </div>



 <br>
    <div class="clearfix"></div>
    {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
    <a href="{{aurl('setting/homevideos')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
    {!! Form::close() !!}


</div>

</div>
@endsection




