@extends('admin.index')
@section('title',trans('admin.show_video'))
@section('content')
    @push('css')
        <style>
            .list-group-item {
                padding: 30px 15px !important;
            }
        </style>

    @endpush


<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{trans('admin.show_video')}}</h3>
    </div>
    <div class="box-body">


        <div class="form-group row">

            <div class="col-md-4">
                {{ Form::label('branch_id', trans('admin.branch_id'), null, ['class' => 'control-label']) }}
                {{ Form::select('branch_id',$branch,  $content->branch_id, array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-md-4">
                {{ Form::label(trans('admin.desc_ar'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{$content->desc_ar}}</div>
            </div>
            <div class="col-md-4">
                {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{$content->desc_en}}</div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6 ">
                {{ Form::label(trans('admin.video_src'), null, ['class' => 'control-label']) }}
                @if($content->video_src != null)
                <iframe width="320" height="240" src="{{$content->video_src}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
@endif
            </div>
            <div class="col-md-6 ">
                {{ Form::label(trans('admin.media'), null, ['class' => 'control-label']) }}
                @if($content->media != null)
                <video class="gallery_img" width="320" height="240" controls src="{{url('home/videos/'.$content->media)}}">

                </video>
                @endif

            </div>
        </div>
        </div>


    <a href="{{aurl('setting/homevideos')}}" class="btn btn-danger">{{trans('admin.back')}}</a>







@endsection
