@extends('admin.index')
@section('title',trans('admin.edit_content'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($content,['method'=>'PUT','route' => ['homevideos.update',$content->id],'files'=>true]) !!}


            <div class="form-group row">
                    <div class="col-md-4">
                            {{ Form::label('branch_id', trans('admin.branch_id'), null, ['class' => 'control-label']) }}
                            {{ Form::select('branch_id',$branch,  $content->branch_id, array_merge(['class' => 'form-control'])) }}

                 </div>
                <div class="col-md-6">
                    {{ Form::label(trans('admin.video_src'), null, ['class' => 'control-label']) }}
                    {{ Form::text('video_src',null, array_merge(['class' => 'form-control'])) }}
                    @if($content->video_src != null)
                        <iframe width="320" height="240" src="{{$content->video_src}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    @endif
                </div>
                <div class="col-md-6 ">

                    @if($content->media != null)
                        <video class="gallery_img" width="320" height="240" controls src="{{url('home/videos/'.$content->media)}}">

                        </video>
                    @endif
                </div>





          </div>
            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.desc_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('desc_ar',old('desc_ar'), array_merge(['class' => 'form-control','style' => 'resize:none'])) }}
                </div>
                <div class="col-md-6">
                    {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('desc_en',old('desc'), array_merge(['class' => 'form-control','style' => 'resize:none'])) }}
                </div>

            </div>


            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('setting/homevideos')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>







@endsection
