
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
</head>
<style>
    body {
        font-family: 'dejavu sans', sans-serif;
        direction:rtl;
        text-align:right;
        padding:0;
        margin: 0;
    }
    .el-date{
        float: right;
        width: 40%
    }
    .el-date p{
        font-size: 12px;
        margin: 0 20px -25px;
        padding: 15px 0;
    }
    .el-no3{
        width:100%;
        display:block;
        margin:0 auto;
        text-align:center;
    }
    .el-no3 span{
        padding: 5px 20px !important;
        font-weight: bold;
        font-size: 12px;
    }
    .clearfix{
        clear:both;
    }
    table{
        width: 100%;
        text-align: center;
        font-size: 10px;
        margin-top: 20px;
    }
    .table th{
        background-color: #f3f3f3;
        text-align: center;
        font-size: 11px;
    }
    .table td{
        text-align: right;
    }
    table td, table th {
        padding: .5rem;
        vertical-align: middle;
        border: 1px solid #000000 !important;
    }
    table .th-empty{
        border: none !important;
        background: none
    }
</style>
<body>
<div style="display: none">
    {{$allCredit = 0}}
    {{$allDebtor = 0}}</div>
<div>
    <div style="float:right;font-weight:bold;width:50%">{{setting()->sitename_ar}}</div>
    <div style="float:left;font-weight:bold;width:50%;text-align:left">{{setting()->sitename_en}}</div>
</div>
<div style="text-align:center">
    <img src="{{asset('storage/'. setting()->icon)}}" style="max-width:70px;margin:15px 0">
</div>

<div class="el-no3">
    <span>تقرير بارصدة الشركات ومراكز التكلفة</span>
</div>

<div class="clearfix"></div>
<div class="el-date">
    <p>من تاريخ : {{$from}}</p>
    <p>الى تاريخ : {{$to}}</p>
</div>


<div class="clearfix"></div>
<div class="table-responsive">
    <table style="border: none" class="table table-bordered table-striped table-hover text-center">
        <tr>
            <th colspan="2">{{trans('admin.cc_account_name')}}</th>
            <th colspan="2">{{trans('admin.first_date')}}</th>
            <th colspan="2">{{trans('admin.motion')}}</th>
            <th colspan="2">{{trans('admin.last_date')}}</th>
        </tr>
        <tr>
            <th style="width:20px">الرقم</th>
            <th>{{trans('admin.description')}}</th>
            <th>{{trans('admin.debtor')}}</th>
            <th>{{trans('admin.creditor')}}</th>
            <th>{{trans('admin.debtor')}}</th>
            <th>{{trans('admin.creditor')}}</th>
            <th>{{trans('admin.debtor')}}</th>
            <th>{{trans('admin.creditor')}}</th>
        </tr>
        <div style="display: none">{{ $i = 1 }}
            {{$balance = 0}}
            {{$sum = 0}}
            {{$dataDebtor = 0}}
            {{$dataCredit = 0}}
            {{$dataCredity = 0}}
            {{$dataDebtory = 0}}
            {{$creditorx = 0}}
            {{$first_debtor_father = 0}}
            {{$first_creditor_father = 0}}
            {{$move_debt_father = 0}}
            {{$move_creditor_father = 0}}
            {{$balance_debtor_father = 0}}
            {{$balance_creditor_father = 0}}

            {{$debtorx = 0}}
            {{$creditor1x = 0}}
            {{$debtor1x = 0}}
            {{$dataDebtor1 = 0}}
            {{$dataDebtor1y = 0}}
            {{$dataCredit1 = 0}}
            {{$dataCredit1y = 0}}
            {{$dataDebtor2 = 0}}
            {{$dataDebtor2y = 0}}
            {{$dataCredit2 = 0}}
            {{$debtor2y = 0}}
            {{$creditor2y = 0}}
            {{$dataCredit2y = 0}}
            {{$alldeby = 0}}
            {{$alldeb2y = 0}}
            {{$creditor2x = 0}}
            {{$debtor2x = 0}}
            {{$alldeb  = 0}}
            {{$balance_creditor = 0}}
            {{$balance_debtor = 0}}
        </div>


        <div style="display: none">
            @if($depart->type == '0')
                {{$creditor =glcc_first_blance($depart->id,'creditor') + cc_first_balance_public($depart->id,$from,$to,'creditor','<')}}
                {{$debtor = glcc_first_blance($depart->id,'debtor') +cc_first_balance_public($depart->id,$from,$to,'debtor','<')}}
                {{$value_debtor2 = move_cc($depart->id,$from,$to,'debtor','>=')}}
                {{$value_creditor2 = move_cc($depart->id,$from,$to,'creditor','>=')}}

            @else
                {{$creditor =$depart->creditor + cc_first_individual($depart->id,$from,$to,'creditor','<')}}
                {{$debtor = $depart->debtor + cc_first_individual($depart->id,$from,$to,'debtor','<')}}
                {{$value_debtor2 = move_cc_individual($depart->id,$from,$to,'debtor','>=')}}
                {{$value_creditor2 = move_cc_individual($depart->id,$from,$to,'creditor','>=')}}

            @endif
            {{ $balance_creditor = ($creditor + $value_creditor2) - ($debtor + $value_debtor2)}}
            {{ $balance_debtor = ($debtor + $value_debtor2) - ($creditor + $value_creditor2)}}
        </div>

        @if($balance_creditor > $balance_debtor)

            <tr>
                <td>

                    {{$depart->code}}
                </td>
                <td>

                    {{session_lang($depart->name_en,$depart->name_ar)}}
                </td>

                <td class="cc_first_debt">
                    <div style="display:none">
                        @if($depart->type == '0')
                            {{$creditor =glcc_first_blance($depart->id,'creditor') +  cc_first_balance_public($depart->id,$from,$to,'creditor','<')}}
                            {{$debtor = glcc_first_blance($depart->id,'debtor') + cc_first_balance_public($depart->id,$from,$to,'debtor','<')}}
                        @else
                            {{$creditor =$depart->creditor +  cc_first_individual($depart->id,$from,$to,'creditor','<')}}
                            {{$debtor =$depart->debtor +  cc_first_individual($depart->id,$from,$to,'debtor','<')}}

                        @endif


                    </div>

                    @if(($debtor - $creditor) > 0)
                        {{ $first_debtor_father = $debtor - $creditor}}
                    @else
                        {{$first_debtor_father = 0}}
                    @endif


                </td>
                <td class="cc_first_creditor">
                    <div style="display:none">
                        @if($depart->type == '0')
                            {{$creditor1 =glcc_first_blance($depart->id,'creditor') +  cc_first_balance_public($depart->id,$from,$to,'creditor','<')}}
                            {{$debtor1 = glcc_first_blance($depart->id,'debtor') + cc_first_balance_public($depart->id,$from,$to,'debtor','<')}}
                        @else
                            {{$creditor1 =$depart->creditor + cc_first_individual($depart->id,$from,$to,'creditor','<')}}
                            {{$debtor1 =$depart->debtor + cc_first_individual($depart->id,$from,$to,'debtor','<')}}

                        @endif

                    </div>
                    @if(($creditor1- $debtor1) > 0)
                        {{ $first_creditor_father = $creditor1 - $debtor1}}
                    @else
                        {{$first_creditor_father = 0}}
                    @endif

                </td>
                <td class="cc_move_debtor">
                    @if($depart->type == '0')
                        {{$move_debt_father = move_cc($depart->id,$from,$to,'debtor','>=')}}

                    @else
                        {{$move_debt_father = move_cc_individual($depart->id,$from,$to,'debtor','>=')}}

                    @endif

                </td>

                <td class="cc_move_creditor">
                    @if($depart->type == '0')
                        {{$move_cred_father = move_cc($depart->id,$from,$to,'creditor','>=')}}

                    @else
                        {{$move_cred_father = move_cc_individual($depart->id,$from,$to,'creditor','>=')}}

                    @endif

                </td>
                <td class="cc_end_debtor">

                    @if(($first_debtor_father + $move_debt_father) - ($first_creditor_father + $move_cred_father) > 0)
                        {{$balance_debtor_father= ($first_debtor_father + $move_debt_father) - ($first_creditor_father + $move_cred_father)}}
                    @else
                        {{$balance_debtor_father = 0}}
                    @endif

                </td>
                <td class="cc_end_creditor">

                    @if(($first_creditor_father + $move_cred_father) - ($move_debt_father + $first_debtor_father) > 0)
                        {{ $balance_creditor_father= ($first_creditor_father + $move_cred_father) - ($move_debt_father + $first_debtor_father)}}
                    @else
                        {{$balance_creditor_father = 0 }}
                    @endif

                </td>
            </tr>

        @endif

        @foreach($glcc as $key => $merge)

            <div style="display: none">
                @if($merge->type == '0')
                    {{$creditor =glcc_first_blance($merge->id,'creditor') + cc_first_balance_public($merge->id,$from,$to,'creditor','<')}}
                    {{$debtor = glcc_first_blance($merge->id,'debtor') + cc_first_balance_public($merge->id,$from,$to,'debtor','<')}}
                    {{$value_debtor2 = move_cc($merge->id,$from,$to,'debtor','>=')}}
                    {{$value_creditor2 = move_cc($merge->id,$from,$to,'creditor','>=')}}

                @else
                    {{$creditor = $merge->creditor +  cc_first_individual($merge->id,$from,$to,'creditor','<')}}
                    {{$debtor = $merge->debtor +  cc_first_individual($merge->id,$from,$to,'debtor','<')}}
                    {{$value_debtor2 = move_cc_individual($merge->id,$from,$to,'debtor','>=')}}
                    {{$value_creditor2 = move_cc_individual($merge->id,$from,$to,'creditor','>=')}}

                @endif
                {{ $balance_creditor = ($creditor + $value_creditor2) - ($debtor + $value_debtor2)}}
                {{ $balance_debtor = ($debtor + $value_debtor2) - ($creditor + $value_creditor2)}}
            </div>
            @if($balance_creditor > $balance_debtor )

                <tr>
                    <td>

                        {{$merge->code}}
                    </td>
                    <td>

                        {{session_lang($merge->name_en,$merge->name_ar)}}
                    </td>

                    <td class="cc_first_debt">
                        <div style="display:none">
                            @if($merge->type == '0')
                                {{$creditor = glcc_first_blance($merge->id,'creditor') +cc_first_balance_public($merge->id,$from,$to,'creditor','<')}}
                                {{$debtor =glcc_first_blance($merge->id,'debtor') + cc_first_balance_public($merge->id,$from,$to,'debtor','<')}}
                            @else
                                {{$creditor = $merge->creditor + cc_first_individual($merge->id,$from,$to,'creditor','<')}}
                                {{$debtor =  $merge->debtor +cc_first_individual($merge->id,$from,$to,'debtor','<')}}

                            @endif

                            @if(($debtor - $creditor) > 0)
                                {{ $dataDebtory += $debtor - $creditor}}


                            @else
                                {{$dataDebtory += 0}}
                            @endif

                        </div>

                        @if(($debtor - $creditor) > 0)
                            {{ $value_debtor = $debtor - $creditor}}
                        @else
                            {{$value_debtor = 0}}
                        @endif


                    </td>
                    <td class="cc_first_creditor">
                        <div style="display:none">
                            @if($merge->type == '0')
                                {{$creditor1 =glcc_first_blance($merge->id,'creditor') + cc_first_balance_public($merge->id,$from,$to,'creditor','<')}}
                                {{$debtor1 =glcc_first_blance($merge->id,'debtor') + cc_first_balance_public($merge->id,$from,$to,'debtor','<')}}
                            @else
                                {{$creditor1 =$merge->creditor +  cc_first_individual($merge->id,$from,$to,'creditor','<')}}
                                {{$debtor1 =$merge->debtor +  cc_first_individual($merge->id,$from,$to,'debtor','<')}}

                            @endif
                            @if(($creditor1 - $debtor1) > 0)
                                {{$dataCredity += $creditor1 - $debtor1}}

                            @else

                                {{$dataCredity += 0}}
                            @endif
                        </div>
                        @if(($creditor1- $debtor1) > 0)
                            {{ $value_creditor = $creditor1 - $debtor1}}
                        @else
                            {{$value_creditor = 0}}
                        @endif

                    </td>
                    <td class="cc_move_debtor">
                        @if($merge->type == '0')
                            {{$value_debtor2 = move_cc($merge->id,$from,$to,'debtor','>=')}}
                            <div style="display:none">
                                {{$dataDebtor1y += move_cc($merge->id,$from,$to,'debtor','>=')}}
                            </div>
                        @else
                            {{$value_debtor2 = move_cc_individual($merge->id,$from,$to,'debtor','>=')}}
                            <div style="display:none">

                                {{$dataDebtor1y += move_cc_individual($merge->id,$from,$to,'debtor','>=')}}
                            </div>
                        @endif

                    </td>
                    <td class="cc_move_creditor">
                        @if($merge->type == '0')
                            {{$value_creditor2 = move_cc($merge->id,$from,$to,'creditor','>=')}}
                            <div style="display:none">
                                {{$dataCredit1y += move_cc($merge->id,$from,$to,'creditor','>=')}}
                            </div>
                        @else
                            {{$value_creditor2 = move_cc_individual($merge->id,$from,$to,'creditor','>=')}}
                            <div style="display:none">

                                {{$dataCredit1y += move_cc_individual($merge->id,$from,$to,'creditor','>=')}}
                            </div>
                        @endif

                    </td>
                    <td class="cc_end_debtor">

                        @if(($value_debtor + $value_debtor2) - ($value_creditor + $value_creditor2) > 0)
                            {{($value_debtor + $value_debtor2) - ($value_creditor + $value_creditor2)}}
                        @else
                            0
                        @endif
                        <div style="display:none">
                            @if(($value_debtor + $value_debtor2) - ($value_creditor + $value_creditor2) > 0)
                                {{$dataDebtor2y += ($value_debtor + $value_debtor2) - ($value_creditor + $value_creditor2)}}

                            @else

                                {{$dataDebtor2y += 0}}
                            @endif
                        </div>
                    </td>
                    <td class="cc_end_creditor">

                        @if(($value_creditor + $value_creditor2) - ($value_debtor + $value_debtor2) > 0)
                            {{($value_creditor + $value_creditor2) - ($value_debtor + $value_debtor2)}}
                        @else
                            0
                        @endif
                        <div style="display:none">
                            @if(($value_creditor + $value_creditor2) - ($value_debtor + $value_debtor2) > 0)
                                {{$dataCredit2y += ($value_creditor + $value_creditor2) - ($value_debtor + $value_debtor2)}}

                            @else
                                {{$dataCredit2y += 0}}
                            @endif
                        </div>
                    </td>
                </tr>
            @endif
        @endforeach

        <tr>

            <th class="th-empty"></th>
            <th>{{trans('admin.Total_motion')}}</th>
            <th>{{$first_debtor_father}} </th>
            <th>{{$first_creditor_father}} </th>
            <th>{{$move_debt_father}} </th>
            <th>{{$move_cred_father}} </th>
            <th>{{$balance_debtor_father}} </th>
            <th>{{$balance_creditor_father}} </th>
        </tr>
    </table>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rLQihCFPSNPkwLNBTbVZHUAnYc5iRYaWz9em+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTLT1Kqob5UDEML61gCyjnAcfMXgkdP3wGcg45XN0VxHd" crossorigin="anonymous"></script>
</body>
</html>
