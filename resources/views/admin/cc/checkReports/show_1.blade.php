<script>
    $(function () {
        'use strict';
        if ("{{$glcc_last,$glcc_first}}"){
            var fromtree = $('.fromtree').val();
            var totree = $('.totree').val();
            var to = $('.to').val();
            var kind = $('.kind').val();
            var from = $('.from').val();
            var reporttype = $('.reporttype').val();
            var level = '{{isset($level) ? $level : null}}';

            $("#loadingmessage-2").css("display","block");
            $(".column-account-date").css("display","none");
            if (this) {
                $.ajax({
                    url: '{{aurl('cc/report/checkReports/details')}}',
                    type: 'get',
                    dataType: 'html',
                    data: {reporttype:reporttype,level: level, kind: kind, fromtree: fromtree, totree: totree, from: from, to: to},
                    success: function (data) {
                        $("#loadingmessage-2").css("display", "none");
                        $('.column-account-date').css("display", "block").html(data);

                    }
                });
            }
        }
        // var schedule = $('.schedules').val();
        $('.from,.to,.fromtree,.totree').on('change',function () {
            var from = $('.from').val();
            var to = $('.to').val();

            var fromtree = $('.fromtree').val();
            var totree = $('.totree').val();
            var reporttype = $('.reporttype').val();
            var level = $('.level').val();
            var kind = $('.kind').val();
            $("#loadingmessage-2").css("display","block");
            $(".column-account-date").css("display","none");
            if (this){
                $.ajax({
                    url: '{{aurl('cc/report/checkReports/details')}}',
                    type:'get',
                    dataType:'html',
                    data:{from : from,to: to,fromtree: fromtree,totree: totree,level:level,reporttype:reporttype,kind:kind},
                    success: function (data) {
                        $("#loadingmessage-2").css("display","none");
                        $('.column-account-date').css("display","block").html(data);

                    }
                });
            }else{
                $('.column-account-date').html('');
            }
        });


    });


    $(document).ready(function(){
        var minDate = '{{\Carbon\Carbon::today()->format('Y-m-d')}}';
        console.log(minDate);
        $('.date').datepicker({
            format: 'yyyy-mm-dd',
            rtl: true,
            language: '{{session('lang')}}',
            autoclose:true,
            todayBtn:true,
            clearBtn:true,
        });
    });
</script>
<script>
    $(function () {
        'use strict'
        $('.e2').select2();
        $('.e3').select2();
    });

</script>

<div class="form-group row" style="padding: 0 10px">
    <div class="col-md-3">
        {{ Form::label('form',trans('admin.from'), ['class' => 'control-label']) }}
        {{ Form::select('fromtree',$glcc,$glcc_first, array_merge(['class' => 'form-control e3 fromtree','placeholder'=> trans('admin.select') ])) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('to', trans('admin.to'), ['class' => 'control-label']) }}
        {{ Form::select('totree',$glcc,$glcc_last, array_merge(['class' => 'form-control e3 totree','placeholder'=> trans('admin.select') ])) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('From', trans('admin.From'), ['class' => 'control-label']) }}
        {{ Form::text('From',\Carbon\Carbon::today()->format('Y').'-01-01', array_merge(['class' => 'form-control from date','required'=>'required','autocomplete'=>'off'])) }}
    </div>
    <div class="col-md-3">
        {{ Form::label('To', trans('admin.To'), ['class' => 'control-label']) }}
        {{ Form::text('To',\Carbon\Carbon::today()->format('Y-m-d'), array_merge(['class' => 'form-control to date','required'=>'required','autocomplete'=>'off'])) }}
    </div>
</div>
<div id='loadingmessage-2' style='display:none; margin-top: 20px' class="text-center">
    <img src="{{ url('/') }}/images/ajax-loader.gif"/>
</div>
<div class="column-account-date">

</div>
<br>
<?php
