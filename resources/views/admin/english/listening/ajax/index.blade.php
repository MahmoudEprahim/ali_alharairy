
 {!! Form::open(['method'=>'POST','route' => 'englishlistening.store','files'=>true]) !!}
 <div class="content">
        <div class="hidden" name="EnglishGate">
                <input name="EnglishGate" type="text" value="{{$EnglishGate}}" class="form-control">
        </div>
        <div class="hidden" name="branch_id">
                <input name="branch_id" type="text" value="{{$branch}}" class="form-control">
        </div>
<div class="form-group row">
    <div class="col-md-6 "> 
        {{ Form::label('list_id', trans('admin.list_id'),null, ['class' => 'control-label']) }}
        {{ Form::select('list_id',$list ,null, array_merge(['class' => 'form-control','required' => 'required'])) }}
    </div>
</div>
    
    


<div class="form-group row">
    <div class="col-md-6">
        {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
        {{ Form::textarea('desc',null, array_merge(['class' => 'form-control','style'=>'resize:none;'])) }}
    </div>
    <div class="col-md-6">
        {{ Form::label(trans('admin.desc_ar'), null, ['class' => 'control-label']) }}
        {{ Form::textarea('desc_ar',null, array_merge(['class' => 'form-control','style'=>'resize:none;'])) }}
    </div>
    <div class="col-md-6">
        {{ Form::label(trans('admin.media_name'), null, ['class' => 'control-label']) }}
        {{ Form::text('media_name',null, array_merge(['class' => 'form-control'])) }}
    </div>
    <div class="col-md-6 ">
    {{ Form::label('media', trans('admin.upload_media') . ':', array( 'style' => 'margin-top: 20px;')) }}
    {{ Form::file('media') }}
    </div>
</div>
<br>
<div class="clearfix"></div>
{{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
<a href="{{aurl('englishlistening')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
{!! Form::close() !!}
 </div>

 








