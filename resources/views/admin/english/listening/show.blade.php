@extends('admin.index')
@section('title',trans('admin.english_gates'))
@section('content')

<div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.show_content')}}</h3>
        </div>
    <div class="box-body">
        <div class="form-group row">  
        <div class="col-md-4 ">
                {{ Form::label(trans('admin.branch'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{session_lang($content->branch->name_en,$content->branch->name_ar)}}</div>
            </div>
            
            <div class="col-md-4 ">
                {{ Form::label(trans('admin.EnglishGate'), null, ['class' => 'control-label']) }}
                <div  class="form-control">{{\App\Enums\EnglishGate::getDescription($content->EnglishGate) }}</div>
            </div>

            <div class="col-md-4 ">
                {{ Form::label(trans('admin.list'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{session_lang($content->list->name_en,$content->list->name_ar)}}</div>

            </div>
            
            </div>

        <div class="form-group row">

            <div class="col-md-4 ">
                {{ Form::label(trans('admin.desc'), null, ['class' => 'control-label']) }}
            <div class="form-control">{{session_lang($content->desc_ar,$content->desc)}}</div>
            </div>
            <div class="col-md-4 ">
                {{ Form::label(trans('admin.media_name'), null, ['class' => 'control-label']) }}
            <div class="form-control">{{$content->media_name}}</div>
            </div>
            @if($content->EnglishGate == 2)
            <div class="col-md-4 ">
                {{ Form::label(trans('admin.media'), null, ['class' => 'control-label']) }}
                
                <video width="320" height="240" controls>
                    <source src="{{asset('english/audios/'.$content->media)}}" >
                </video> 
            </div>
            @endif
           
        </div> 
        <br>
            <div class="clearfix"></div>
           
            <a href="{{aurl('englishlistening')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!} 
    </div>
</div>
   
@endsection