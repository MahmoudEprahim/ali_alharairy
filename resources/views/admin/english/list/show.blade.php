
@extends('admin.index')
@section('title',trans('admin.english_gates'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.show_list'). ' ' . session_lang($list->name_en,$list->name_ar)}}</h3>
        </div>
        <div class="box-body">
            

            <div class="form-group row">
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.type'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{\App\Enums\EnglishSections::getDescription($list->EnglishGate)}}</div>
                </div>

                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$list->name_ar}}</div>
                </div>
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$list->name_en}}</div>
                </div>
            </div>

            <br>
            <a href="{{aurl('englishlists')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        </div>
    </div>








@endsection