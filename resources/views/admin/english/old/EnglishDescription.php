<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnglishDescription extends Model
{
    protected $table = 'englishcontents';

    protected $fillble = [
        'title',
        'content',
        'word',
        'maeaning',
        'image',
        'list_id',
    ];

    public function list()
    {
        return $this->belongsTo('App\EnglishList'); 
    }

    
}
