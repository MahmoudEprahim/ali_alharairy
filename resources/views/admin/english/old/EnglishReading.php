<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnglishReading extends Model
{
    protected $table = 'englishlreading';

    protected $fillable =[
        'media',
        'desc',
        'EnglishGate',
        'branch_id',
        'list_id',
    ];

    public function list()
    {
        return $this->belongsTo('App\EnglishList'); 
    }
    public function branch()
    {
        return $this->belongsTo('App\Branches');
    }
}
