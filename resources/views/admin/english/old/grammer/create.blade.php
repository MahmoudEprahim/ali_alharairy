@extends('admin.index')
@section('title',trans('admin.Create_new_english_grammer'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush


@push('js')
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
        <script>

            $('.content').on('click', '.remove', function() {
                $('.remove').closest('.content').find('.element').not(':first').last().remove();
            });
            $('.content').on('click', '.clone', function() {
                $('.clone').closest('.content').find('.element').first().clone().appendTo('.results');

            }); 
        </script>

        <script>
            $(function () {
                'use strict';

                $('.type_list').on('change',function () {

                    var 30 = $('.branche_id option:selected').val();
                    var grade_id = $('.grade_id option:selected').val();
                    var units_id = $('.units_id option:selected').val();
                    var type_list = $('.type_list option:selected').val();

                    $("#loadingmessage").css("display","block");
                    $(".column-form").css("display","none");
                    console.log(type_list,type_list);
                    if (this){
                        $.ajax({
                            url: '{{aurl('content_wordlist_grammar')}}',
                            type:'get',
                            dataType:'html',
                            data:{branche_id : 30,grade_id : grade_id,units_id : units_id,type_list : type_list,},
                            success: function (data) {
                                $("#loadingmessage").css("display","none");
                                $('.column-form').css("display","block").html(data);

                            }
                        });
                    }else{
                        $('.column-form').html('');
                    }
                });


            });
        </script>

    @endpush



<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
      
        {!! Form::open(['method'=>'POST','route' => 'englishgrammer.store','files'=>true]) !!}
        
    
        <div class="form-group row">
                <div class="col-md-4">
                        {{ Form::label(trans('admin.branch_id'),null, ['class' => 'control-label']) }}
                        {{ Form::select('branch_id',$branch,  null, array_merge(['class' => 'form-control', 'placeholder'=>trans('admin.select')])) }}
                    </div>

            <div class="col-md-4 required">
                {{ Form::label(trans('admin.EnglishGate'), null, ['class' => 'control-label']) }}
                {{ Form::select('EnglishGate',\App\Enums\EnglishGate::toSelectArray() ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
            </div>
            <div class="col-md-4 "> 
                {{ Form::label('list_id', trans('admin.list_id'),null, ['class' => 'control-label']) }}
                {{ Form::select('list_id',$list ,null, array_merge(['class' => 'form-control','required' => 'required','placeholder'=>trans('admin.select')])) }}
            </div>
             
            
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="{{trans('admin.grammer_name')}}" class="control-label">{{trans('admin.grammer_name')}}</label> 
                <input  name="name_en[]" type="text" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="{{trans('admin.grammer_name_ar')}}" class="control-label">{{trans('admin.grammer_name_ar')}}</label> 
                <input  name="name_ar[]" type="text" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="{{trans('admin.grammer_content')}}" class="control-label">{{trans('admin.grammer_content')}}</label> 
                <textarea name="grammer_content[]" style=" resize: none;" cols="50" rows="10" class="form-control"></textarea>
            </div>
            <div class="col-md-6">
                <label for="{{trans('admin.grammer_content_ar')}}" class="control-label">{{trans('admin.grammer_content_ar')}}</label> 
                <textarea name="grammer_content_ar[]" style=" resize: none;" cols="50" rows="10" class="form-control"></textarea>
            </div>
        </div>

        <a href="#" class="grammer-plus fa fa-plus"></a>
       
        <div class="form-group add-new-grammer">
           
        </div>

        
        

       
            
            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('englishwords')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
            </div>
    </div>
</div>



@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        







@endsection
