@extends('admin.index')
@section('title',trans('admin.show_english_grammer'))
@section('content')
    @push('css')
        <style>
            .list-group-item {
                padding: 30px 15px !important;
            }
        </style>

    @endpush


    <div class="box-body">
           
            
    <div class="form-group row">
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.word'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->name_en}}</div>
                </div>
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.meaning'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->grammer_content}}</div>

                </div>
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.EnglishGate'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->EnglishGate}}</div>

                </div>
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.list'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->list_id}}</div>

                </div>
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.branch'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->branch_id}}</div>

                </div>

             

            </div>
           
    </div>
   
@endsection