<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnglishWords extends Model
{
    protected $table = 'englishlwords';

    protected $fillable =[
        'word',
        'meaning',
        'EnglishGate',
        'branch_id',
        'list_id',
    ];

    public function list()
    {
        return $this->belongsTo('App\EnglishList'); 
    }
    public function branch()
    {
        return $this->belongsTo('App\Branches');
    }
}
