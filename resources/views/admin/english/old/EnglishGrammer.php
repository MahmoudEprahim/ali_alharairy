<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnglishGrammer extends Model
{
    protected $table = 'englishlgrammer';

    protected $fillable =[
        'branch_id',
        'list_id',
        'name_en',
        'name_ar',
        'grammer_content',
        'grammer_content_ar',
        'EnglishGate',
        
    ];

    public function list()
    {
        return $this->belongsTo('App\EnglishList'); 
    }
    public function branch()
    {
        return $this->belongsTo('App\Branches');
    }
}
