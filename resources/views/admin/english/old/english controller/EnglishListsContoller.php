<?php

namespace App\Http\Controllers\Admin\english;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\EnglishList;

use App\DataTables\EnglishListsDataTable;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;


class EnglishListsContoller extends Controller
{
    public function index(EnglishListsDataTable $list) 
    { 
        return $list->render('admin.english.list.index',['title'=>trans('admin.english_lists_information')]);
    }    

    public function create(EnglishListsDataTable $list)
    {
        $list = EnglishList::pluck('name_'.session('lang'),'id');
        
        return view('admin.english.list.create', ['list' => $list,'title'=> trans('admin.Create_new_list')]);
    }

    public function store(Request $request, EnglishList $list)
    {
        $this->validate($request,[
            'name_en' => 'required',
            'name_ar' => 'required',
            'EnglishGate' => 'required',
        ],[],[
            'name_en'=> trans('admin.name_en'),
            'name_ar' => trans('admin.name_ar'),
            'EnglishGate' => trans('admin.EnglishGate'),
            
            
        ]);
            $list = new EnglishList();
            $list->name_ar = $request->name_ar;
            $list->name_en = $request->name_en;          
            $list->EnglishGate = $request->EnglishGate;          
                     
            
            $list->save();
        
        return redirect(aurl('englishlists'))->with(session()->flash('message',trans('admin.success_add')));

    }

    public function show($id)
    {
        $list = EnglishList::findOrFail($id);
        
        
        return view('admin.english.list.show', ['list' => $list,'title'=> trans('admin.show_list')]);
        
    }

    public function edit($id)
    {
        
        $list = EnglishList::findOrFail($id);
       
        return view('admin.english.list.edit',['list'=> $list, 'title'=>trans('admin.edit_list')]);
    }

    public function update(Request $request, $id)
    {
        $list = EnglishList::findOrFail($id);
        $this->validate($request,[
            'name_en' => 'required',
            'name_ar' => 'required',
            'EnglishGate' => 'required',
        ],[],[
            'name_en'=> trans('admin.name_en'),
            'name_ar' => trans('admin.name_ar'),
            'EnglishGate' => trans('admin.EnglishGate'),
        ]);
            
            $list->name_ar = $request->name_ar;
            $list->name_en = $request->name_en;
            $list->EnglishGate = $request->EnglishGate;          
                     
            
            $list->save();
        
        return redirect(aurl('englishlists'))->with(session()->flash('message',trans('admin.success_add')));

    }


    public function destroy($id)
    {
        $grade = EnglishList::findOrFail($id);
        $grade->delete();
        return redirect(aurl('englishlists'));
    }
}
