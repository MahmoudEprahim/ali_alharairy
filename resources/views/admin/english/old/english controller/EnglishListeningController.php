<?php

namespace App\Http\Controllers\Admin\english;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



use App\EnglishListening;
use App\EnglishList;
use App\Branches;

use App\DataTables\EnglishListeningDataTable; 


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Up;


class EnglishListeningController extends Controller
{
    public function index(EnglishListeningDataTable $content) 
    {  
        return $content->render('admin.english.listening.index',['title'=>trans('admin.english_contents_information')]);
    } 

    public function create()
    {
        $content =  EnglishListening::pluck('id');
        $list = EnglishList::pluck('name_'.session('lang'),'id');  
        $branch = Branches::pluck('name_'.session('lang'),'id');
        
        return view('admin.english.listening.create', ['content' => $content, 'list' => $list,'branch' => $branch,'title'=> trans('admin.Create_new_english_audio')]);
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
           
            'media' => 'sometimes|mimes:audio,pdf,docx',
            'desc' => 'sometimes',
            'desc_ar' => 'sometimes',
            'branch_id' => 'required',
            'list_id' => 'required',
            'EnglishGate' => 'required',
           
            
        ],[],[
            'desc' => trans('admin.desc'),
            'desc_ar' => trans('admin.desc'),
            'media' => trans('admin.media'),
            'branch_id' => trans('admin.branch_id'),
            'list_id' => trans('admin.list_id'),
            'EnglishGate' => trans('admin.EnglishGate'),
        ]);

        $englishlistening = new EnglishListening();
        $englishlistening->desc = $request->desc;
        $englishlistening->media_name = $request->media_name;
        $englishlistening->desc_ar = $request->desc_ar;
        $englishlistening->branch_id = $request->branch_id;
        $englishlistening->list_id = $request->list_id;
        $englishlistening->EnglishGate = $request->EnglishGate;

        
        $file = $request->file('media');
        if($file){
            $mediaType=explode('/',$file->getMimeType())[0];
            $filetype=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension();  
                if($mediaType=="audio" Or $filetype=="pdf" Or $filetype=="docx" )
                {
                   $englishlistening->media = $filePath;
                   $file->move('english/audios', $filePath);
        
                }else{
                    return redirect(aurl('englishlistening'))->with('error');
                }
        }
       
        
        $englishlistening->save();
        return redirect(aurl('englishlistening'))->with(session()->flash('message',trans('admin.success_add')));
            
    }

    public function show($id)
    {
        $content = EnglishListening::findOrFail($id);
        return view('admin.english.listening.show',compact('content'));
    }

    public function edit($id)
    {
        $content =  EnglishListening::findOrFail($id);
        $list = EnglishList::pluck('name_'.session('lang'),'id');  
        $branch = Branches::pluck('name_'.session('lang'),'id');

       
        return view('admin.english.listening.edit',['content'=> $content, 'list' => $list,'branch'=> $branch, 'title'=>trans('admin.edit_english_content')]);
    }

    public function update(Request $request, $id)
    {
        $content = EnglishListening::findOrFail($id);
        $this->validate($request,[
           
            'media' => 'sometimes',
            'desc' => 'required',
            'desc_ar' => 'required',
            'branch_id' => 'required',
            'list_id' => 'required',
            'EnglishGate' => 'required',
           
            
        ],[],[
            'desc' => trans('admin.desc'),
            'desc_ar' => trans('admin.desc'),
            'media' => trans('admin.media'),
            'branch_id' => trans('admin.branch_id'),
            'list_id' => trans('admin.list_id'),
            'EnglishGate' => trans('admin.EnglishGate'),
        ]);
       

        $content->desc = $request->desc;
        $content->media_name = $request->media_name;
        $content->desc_ar = $request->desc;
        $content->branch_id = $request->branch_id;
        $content->list_id = $request->list_id;
        $content->EnglishGate = $request->EnglishGate;

       
        $file = $request->file('media');
        if($file){
             
            $mediaType=explode('/',$file->getMimeType())[0];
            $filetype=explode('/',$file->getMimeType())[1];
            $name = str_slug($request->input('name')).'_'.time();
            $filePath =  $name. '.' . $file->getClientOriginalExtension(); 
            if($mediaType=="audio" Or $filetype=="pdf" Or $filetype=="docx" )
            {
               $content->media = $filePath;
               $file->move('english/audios', $filePath);
    
            }else{
                return redirect(aurl('englishlistening'))->with('error');
            }
        }
        
        
        $content->save();
        return redirect(aurl('englishlistening'))->with(session()->flash('message',trans('admin.success_update')));

    }


    public function destroy($id)
    {
        $englishlistening = EnglishListening::findOrFail($id);
        $englishlistening->delete();
        return redirect(aurl('englishlistening'));
    }
}
