<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnglishWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('englishlwords', function (Blueprint $table) {
            $table->increments('id');
            $table->text('word')->nullable();
            $table->text('meaning')->nullable();
            $table->integer('EnglishGate')->nullable();
            $table->integer('list_id')->unsigned()->nullable()->index();
            $table->foreign('list_id')->references('id')->on('englishlists')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('branch_id')->unsigned()->nullable()->index();
            $table->foreign('branch_id')->references('id')->on('branches')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('englishlwords');
    }
}
