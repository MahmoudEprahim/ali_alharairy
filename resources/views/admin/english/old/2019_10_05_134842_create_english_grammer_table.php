<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnglishGrammerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() 
    {
        Schema::create('englishlgrammer', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name_en')->nullable();
            $table->text('name_ar')->nullable();
            $table->longText('grammer_content')->nullable();
            $table->longText('grammer_content_ar')->nullable();
            $table->integer('EnglishGate')->nullable();
            $table->integer('list_id')->unsigned()->nullable()->index();
            $table->foreign('list_id')->references('id')->on('englishlists')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('branch_id')->unsigned()->nullable()->index();
            $table->foreign('branch_id')->references('id')->on('branches')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('englishlgrammer');
    }
}
