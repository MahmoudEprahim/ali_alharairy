@extends('admin.index')
@section('title',trans('admin.Create_new_english_word'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
      
        {!! Form::open(['method'=>'POST','route' => 'englishwords.store','files'=>true]) !!}
        
    
        <div class="form-group row">
                <div class="col-md-4">
                        {{ Form::label(trans('admin.branch_id'), null, ['class' => 'control-label']) }}
                        {{ Form::select('branch_id',$branch,  null, array_merge(['class' => 'form-control'])) }}
                    </div>
                    
                <div class="col-md-4 required">
                        {{ Form::label(trans('admin.EnglishGate'), null, ['class' => 'control-label']) }}
                        {{ Form::select('EnglishGate',\App\Enums\EnglishGate::toSelectArray() ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                    </div>
            <div class="col-md-4 "> 
                {{ Form::label('list_id', trans('admin.list_id'), ['class' => 'control-label']) }}
                {{ Form::select('list_id',$list ,null, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.list_id')])) }}
            </div>
            
            
        </div>

        <div class="form-group row">


       

        <div class="col-md-6">
            <label for="الكلمة" class="control-label">الكلمة</label> 
            <input  name="word[]" type="text" class="form-control">
        </div>
        <div class="col-md-6">
            <label for="المعنى" class="control-label">المعنى</label> 
            <input  name="meaning[]" type="text" class="form-control">
        </div>


            <!-- <div class="col-md-6">
                {{ Form::label(trans('admin.word'), null, ['class' => 'control-label']) }}
                {{ Form::text('word', null, array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-md-6">
                {{ Form::label(trans('admin.meaning'), null, ['class' => 'control-label']) }}
                {{ Form::text('meaning', null, array_merge(['class' => 'form-control'])) }}
            </div> -->
        </div>

        <a href="#" class="word-plus fa fa-plus"></a>
        <!-- <a href="#" style="margin-right:30px" class="word-minus fa fa-minus"></a> -->
        <div class="form-group add-new-words">
           
        </div>

        
        

       
            
            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('englishwords')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
            </div>
    </div>
</div>



@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        







@endsection
