@extends('admin.index')
@section('title',trans('admin.english_gates'))
@section('content')
    @push('css')
        <style>
            .list-group-item {
                padding: 30px 15px !important;
            }
        </style>

    @endpush

<div class="box">
    <div class="box-body">   
        <div class="form-group row">
            <div class="col-md-3 ">
                {{ Form::label(trans('admin.desc'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{$content->desc}}</div>
            </div>
            <div class="col-md-3 ">
                {{ Form::label(trans('admin.media'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{$content->media}}</div>

            </div>
            <div class="col-md-3 ">
                {{ Form::label(trans('admin.list'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{session_lang($content->list->name_en,$content->list->name_ar)}}</div>

            </div>
            <div class="col-md-3 ">
                {{ Form::label(trans('admin.branch'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{session_lang($content->Branches->name_en,$content->Branches->name_ar)}}</div>

            </div>
        </div>
        <a href="{{aurl('englishwords')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
    </div>
</div>
@endsection