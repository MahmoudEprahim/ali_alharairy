@extends('admin.index')
@section('title',trans('admin.english_gates))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($content,['method'=>'PUT','route' => ['englishdescription.update',$content->id],'files'=>true]) !!}
            

            <div class="form-group row">
                <div class="col-md-6 "> 
                    {{ Form::label('list_id', trans('admin.list_id'),null, ['class' => 'control-label']) }}
                    {{ Form::select('list_id',$list ,$content->list_id, array_merge(['class' => 'form-control','required' => 'required'])) }}
                </div>
                
                <div class="col-md-6">
                    {{ Form::label('branch_id', trans('admin.branch_id'), null, ['class' => 'control-label']) }}
                    {{ Form::select('branch_id',$branch,  $content->branch_id, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.desc'), null, ['class' => 'control-label']) }}
                    {{ Form::text('desc',old('desc'), array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-6 ">
                {{ Form::label('media', trans('admin.upload_media') . ':', array( 'style' => 'margin-top: 20px;')) }}
                {{ Form::file('media') }}
                </div>
            </div>


            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('englishlistening')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
