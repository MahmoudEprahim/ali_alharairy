@extends('admin.index')
@section('title',trans('admin.show_category'))
@section('root_link', route('category.index'))
@section('root_name', trans('admin.category'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.show_appointment')}}</h3>
        </div>
        <div class="box-body">


            <div class="form-group row">
                
                <div class="col-md-6">
                    {{ Form::label('branch_id', trans('admin.branch_id'), ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($appointment->branch->name_en,$appointment->branch->name_ar)}}</div>
                </div>
                <div class="col-md-6">
                    {{ Form::label('grade_id', trans('admin.ar_grade_id'), ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($appointment->grade->name_en,$appointment->grade->name_ar)}}</div>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-md-4 ">
                    {{ Form::label('groups', trans('admin.groups'), ['class' => 'control-label']) }}
                    <div class="form-control">{{$appointment->groups}}</div>
                </div>

                <div class="col-md-4 ">
                    {{ Form::label(trans('admin.capacity'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$appointment->capacity}}</div>
                </div>
                <div class="col-md-4 ">
                    {{ Form::label(trans('admin.days'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$appointment->days}}</div>
                </div>

            </div>

            <a href="{{aurl('appointments')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>



@endsection
