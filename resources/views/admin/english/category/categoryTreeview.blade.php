@extends('admin.index')

@section('root_link', route('category.index'))
@section('root_name', trans('admin.category'))

@section('content')

@push('css')
    <link href="{{ asset('css/treeview.css') }}" rel="stylesheet">
@endpush
@push('js')
<script src="{{ asset('js/treeview.js') }}"></script>

<script>

    $(document).ready(function () {
        $('#cat_id').change(function () {
            let cat_id = $(this).val();
            if(cat_id !== '0'){
                $.ajax({
                    url: "{{route('getSubCategories')}}",
                    type: 'get',
                    dataType: 'html',
                    data: {cat_id: cat_id},
                    success: function (data) {
                        $('#sub_cat_id').html(data);
                    }
                })
            }
        });

        $(document).on('change' , '#sub_cat_id', function () {
            let sub_cat_id = $(this).val();
            if(sub_cat_id !== '0'){
                $.ajax({
                    url: "{{route('getSubCategoryChild')}}",
                    type: 'get',
                    dataType: 'html',
                    data: {
                        cat_id: $('#cat_id').val(),
                        sub_cat_id: sub_cat_id
                    },
                    success: function (data) {
                        $('#sub_cat_child').removeClass('hidden').html(data);
                        // $('.content_div').removeClass('hidden');
                        $('.sub_cat_child_dev').removeClass('hidden');
                        $('.child_name_en_dev').removeClass('hidden');
                    }
                })
            }
        });

        $('#sub_cat_child').change(function () {
            if($(this).val() != '0'){
                $('.content_div').removeClass('hidden');
                $('.child_name_en_dev').val('').addClass('hidden');
            } else {
                $('.content_div').addClass('hidden').children('textarea').val('');
                $('.child_name_en_dev').removeClass('hidden').children('input').val('');
            }
        });

        $('.child_name_en').change(function () {
            if($(this).val() != 0){
                $('.content_div').removeClass('hidden');
                $('.sub_cat_child_dev').val('').slideUp();
            } else {
                $('.content_div').addClass('hidden').children('textarea').val('');
                $('.sub_cat_child_dev').slideDown();
            }
        })
    })
</script>



<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"></script>
        <script>
            tinymce.init({
                selector: '#bodyContent',
                height: 500,
                theme: 'modern',
                plugins: 'print preview powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
                image_advtab: true,
                templates: [
                    { title: 'Test template 1', content: 'Test 1' },
                    { title: 'Test template 2', content: 'Test 2' }
                ],
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                ],

            });
            var temp =tinymce.get('#bodyContent').getContent();
            console.log(temp);
        </script>
        <script>
            $(function () {
                'use strict'
                $('.title').keyup(function () {
                    var str = $('.title').val();
                    $('.slug').val(str.replace(/\s+/g, '-').toLowerCase());
                })
            });
        </script>
        <script type="text/javascript">
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#profile-img-tag').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#profile-img").change(function(){
                readURL(this);
            });
        </script>


@endpush

<div class="box">
    <div class="box-body"></div>
    <div class="col-md-12">@include('admin.layouts.message')</div>
        <div class="panel panel-primary" style="border:none;border-color:none;">
            <!-- <div class="panel-heading">Unlimited Hierarchical Category Tree View</div> -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <h3>{{trans('admin.Category_List')}}</h3>
                        <ul id="tree1">
                            @foreach($categories as $category)
                                <li>
                                    {{ $category->cat_name_en }}
                                    @if(count($category->subCategories))
                                        @include('admin.english.category.manageChild',['childs' => $category->subCategories])
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <h3>{{trans('admin.add_new_category')}}</h3>

                        <form role="form" id="category" method="POST" action="{{ route('addchildcatContent') }}">
                            @csrf
                        <div class="form-group">
                            <label>Category:</label>
                            <select id="cat_id" name="cat_id" class="form-control">
                                <option value="0">Select</option>
                                @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->cat_name_en }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('cat_id'))
                                <span class="text-red" role="alert">
                                    <strong>{{ $errors->first('cat_id') }}</strong>
                                </span>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>Sub category:</label>
                            <select id="sub_cat_id" name="sub_cat_id" class="form-control">
                                <option value="0">Select</option>
                            </select>

                            @if ($errors->has('sub_cat_id'))
                                <span class="text-red" role="alert">
                                <strong>{{ $errors->first('sub_cat_id') }}</strong>
                            </span>
                            @endif

                        </div>

                        <div class="form-group sub_cat_child_dev">
                            <label>Child sub category:</label>
                            <select id="sub_cat_child" name="sub_cat_child" class="form-control">
                                <option value="0">Select</option>
                            </select>
                            @if ($errors->has('sub_cat_child'))
                                <span class="text-red" role="alert">
                                    <strong>{{ $errors->first('sub_cat_child') }}</strong>
                                </span>
                            @endif  
                        </div>

                        <div class="form-group hidden child_name_en_dev">
                            <label>Child sub category:</label>
                            <input type="text" name="child_name_en" id="child_name_en" class="child_name_en form-control">

                            @if ($errors->has('child_name_en'))
                                <span class="text-red" role="alert">
                                    <strong>{{ $errors->first('child_name_en') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group hidden content_div">
                            <label class="required">name ar:</label>
                            <textarea name="bodyContent" id="bodyContent"  class="form-control" style="height:100px;resize: none" placeholder="Content"></textarea>
                            @if ($errors->has('bodyContent'))
                                <span class="text-red" role="alert">
                                <strong>{{ $errors->first('bodyContent') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">{{trans('admin.add')}}</button>
                            <a href="{{aurl('category')}}" class="btn btn-danger " >{{trans('admin.back')}}</a>
                        </div>

                        </form>


                    </div>

                </div>




            </div>

        </div>
    </div>
</div>


@endsection
