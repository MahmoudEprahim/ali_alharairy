@extends('admin.index')
@section('title',trans('admin.category_create'))
@section('root_link', route('category.index'))
@section('root_name', trans('admin.category'))
@section('content')


@push('js')
    <script>
        $(document).ready(function () {
            $('#appendSubCat').click(function (e) {
                e.preventDefault();
                let parentDiv = $('.appendDiv');
                parentDiv.append(`
                <div class="form-group" style="display: flex">
                       <input type="text" name="sub_cat_name_en[]" class="form-control" id="sub_cat_name_en" placeholder="{{trans('admin.sub_cat_name_en')}}">
                       <a style="margin-right: 10px" id="deleteLink" onclick="$(this).parent().remove()" class="btn btn-danger" href="#"><i class="fa fa-close"></i></a>
                   </div>
                `);
            });
        })
    </script>

@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{trans('admin.category_create')}}</h3>
    </div>
    <div class="box-body">


        {!! Form::open(['method'=>'POST','route' => 'category.store']) !!}

        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('EnglishGate', trans('admin.EnglishGate'), ['class' => 'control-label']) }}
                <select name="EnglishGate" id="EnglishGate" class="form-control">
                    <option value="">select</option>
                    @foreach(\App\Enums\EnglishSections::toSelectArray() as $key => $gate)
                        <option value="{{$key}}">{{$gate}}</option>
                    @endforeach
                </select>

            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('category_name_en', trans('admin.cat_name_en'), ['class' => 'control-label']) }}
                <input type="text" name="cat_name_en" class="form-control" id="category_name_en" placeholder="{{trans('admin.cat_name_en')}}">

            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group appendDiv">
                {{ Form::label('sub_cat_name_en', trans('admin.sub_cat_name_en'), ['class' => 'control-label']) }}

                   <div class="form-group" style="display: flex">
                       <input type="text" name="sub_cat_name_en[]" class="form-control" id="sub_cat_name_en" placeholder="{{trans('admin.sub_cat_name_en')}}">
                       <a style="margin-right: 10px" id="appendSubCat" class="btn btn-success" href="#"><i class="fa fa-plus"></i></a>
                   </div>

            </div>
        </div>

        <div class="col-md-12">
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-info btn-block'])}}
        </div>

            {!! Form::close() !!}
    </div>

</div>

@endsection
