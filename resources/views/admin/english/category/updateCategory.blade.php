@extends('admin.index')
@section('root_link', route('category.index'))
@section('root_name', trans('admin.category'))

@section('content')

@push('css')
    

    <link href="{{ asset('css/treeview.css') }}" rel="stylesheet">
@endpush
@push('js')
<script src="{{ asset('js/treeview.js') }}"></script>
@endpush

<div class="box">
    <div class="box-body"></div>
        <div class="panel panel-primary" style="border:none;border-color:none;">

            <!-- <div class="panel-heading">Unlimited Hierarchical Category Tree View</div> -->

            <div class="panel-body">

                <div class="row">

                    

                    <div class="col-md-6">

                        <h3>{{'admin.update_category'}}</h3>
                        {!! Form::model($oneCategory,['method'=>'PUT','route' => ['category.update',$oneCategory->id],'files'=>true, 'id'=>'category']) !!}


                        <div class="form-group required">
                            {{ Form::label(trans('admin.EnglishGate'), null, ['class' => 'control-label']) }}
                            {{ Form::select('Librarytype',\App\Enums\EnglishSections::toSelectArray($oneCategory->Librarytype) ,$oneCategory->Librarytype, array_merge(['class' => 'form-control librarytype','placeholder'=>trans('admin.select')])) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label(trans('admin.name_en'),null, ['class' => 'control-label']) }}
                            {{ Form::text('name_en',old('name_en'), array_merge(['class' => 'form-control'])) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label(trans('admin.name_ar'),null, ['class' => 'control-label']) }}
                            {{ Form::text('name_ar',old('name_ar'), array_merge(['class' => 'form-control'])) }}
                        </div>

                        

                        <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">

                            <label>Category:</label>
                            <select id="parent_id" name="parent_id" class="form-control">
                                <option value="0">Select</option>
                                @foreach($allCategories as $rows)
                                        <option value="{{ $rows->id }}">{{ $rows->name_en }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('parent_id'))
                                <span class="text-red" role="alert">
                                    <strong>{{ $errors->first('parent_id') }}</strong>
                                </span>
                            @endif

                        </div>


                        <div class="form-group">

                            <button type="submit" class="btn btn-success">{{'admin.add'}}</button>

                        </div>


                        </form>


                    </div>

                </div>




            </div>

        </div>
    </div>
</div>


@endsection