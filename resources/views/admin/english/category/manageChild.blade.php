<ul>
    @foreach($childs as $child)
        <li>
            {{ $child->sub_cat_name_en }}
            @if(count($child->subCategoryChild))
                @include('admin.english.category.manageContent',['childs' => $child->subCategoryChild])
            @endif
        </li>
    @endforeach
</ul>
