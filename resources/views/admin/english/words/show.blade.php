@extends('admin.index')
@section('title',trans('admin.english_gates'))
@section('content')

<div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.show_content')}}</h3>
        </div>
    <div class="box-body">
           
           
            
        <div class="form-group row">
            <div class="col-md-4 ">
                {{ Form::label(trans('admin.branch'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{session_lang($content->branch->name_en,$content->branch->name_ar)}}</div>
            </div>

            <div class="col-md-4 ">
                {{ Form::label(trans('admin.list'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{session_lang($content->list->name_en,$content->list->name_ar)}}</div>
            </div>
            
            <div class="col-md-4 ">
                {{ Form::label(trans('admin.type'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{\App\Enums\EnglishGate::getDescription($content->EnglishGate)}}</div>
            </div>
        </div> 

        <div class="fom-group row">
           
            <div class="col-md-6">
                {{ Form::label(trans('admin.word'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{$content->word}}</div>
            </div>
            <div class="col-md-6">
                {{ Form::label(trans('admin.meaning'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{$content->meaning}}</div>

            </div>
        </div>

        <br>
            <div class="clearfix"></div>
           
            <a href="{{aurl('englishwords')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!} 
    </div>

</div>
   
@endsection