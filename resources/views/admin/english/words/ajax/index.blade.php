
 {!! Form::open(['method'=>'POST','route' => 'englishwords.store','files'=>true]) !!}
    <div class="content">
        <div class="hidden" name="EnglishGate">
                <input name="EnglishGate" type="text" value="{{$EnglishGate}}" class="form-control">
        </div>
        <div class="hidden" name="branch_id">
            <input name="branch_id" type="text" value="{{$branch}}" class="form-control">
        </div>

        <div class="form-group row">
            <div class="form-group col-md-6 "> 
                {{ Form::label('list_id', trans('admin.list_id'), ['class' => 'control-label']) }}
                {{ Form::select('list_id',$list ,null, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.list_id')])) }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label for="trans('admin.word')" class="control-label">{{trans('admin.word')}}</label> 
                <input  name="word[]" type="text" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="{{trans('admin.meaning')}}" class="control-label">{{trans('admin.meaning')}}</label> 
                <input  name="meaning[]" type="text" class="form-control">
            </div>
        </div>
        
        <br>
    <div class="clearfix"></div>
    {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
    <a href="{{aurl('englishwords')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
    {!! Form::close() !!}
        
    </div>



