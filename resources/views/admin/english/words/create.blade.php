@extends('admin.index')
@section('title',trans('admin.Create_new_english_word'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
      
       
        
    
        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label(trans('admin.branch_id'), null, ['class' => 'control-label']) }}
                {{ Form::select('branch_id',$branch,  null, array_merge(['class' => 'form-control'])) }}
            </div>
                
            <div class="col-md-6 required">
                {{ Form::label(trans('admin.EnglishGate'), null, ['class' => 'control-label']) }}
                {{ Form::select('EnglishGate',\App\Enums\EnglishGate::toSelectArray() ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>


            
        </div>
        


        

        
        

       
            
           
            </div>
    </div>
</div>



{{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}
        







@endsection
