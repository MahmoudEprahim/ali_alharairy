@extends('admin.index')
@section('title',trans('admin.english_gates'))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($content,['method'=>'PUT','route' => ['englishgrammer.update',$content->id],'files'=>true]) !!}
            

            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label(trans('admin.branch_id'), null, ['class' => 'control-label']) }}
                    {{ Form::select('branch_id',$branch,  $content->branch_id, array_merge(['class' => 'form-control'])) }}
                </div>

                <div class="col-md-4 required">
                    {{ Form::label(trans('admin.EnglishGate'), null, ['class' => 'control-label']) }}
                    {{ Form::select('EnglishGate',\App\Enums\EnglishGate::toSelectArray() ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>
                <div class="col-md-4 "> 
                    {{ Form::label('list_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                    {{ Form::select('list_id',$list ,$content->list_id, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.grade_id')])) }}
                </div>
            
            
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label(trans('admin.grammer_name'), null, ['class' => 'control-label']) }}
                {{ Form::text('name_en', old('name_en'), array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-md-6">
                {{ Form::label(trans('admin.grammer_name'), null, ['class' => 'control-label']) }}
                {{ Form::text('name_ar', old('name_ar'), array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-md-6">
                {{ Form::label(trans('admin.grammer_content'), null, ['class' => 'control-label']) }}
                {{ Form::textarea('grammer_content', old('grammer_content'), array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-md-6">
                {{ Form::label(trans('admin.grammer_content'), null, ['class' => 'control-label']) }}
                {{ Form::textarea('grammer_content_ar', old('grammer_content'), array_merge(['class' => 'form-control'])) }}
            </div>
        </div>



            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.edit'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('englishgrammer')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
