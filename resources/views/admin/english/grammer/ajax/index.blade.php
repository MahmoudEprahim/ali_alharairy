
  
{!! Form::open(['method'=>'POST','route' => 'englishgrammer.store','files'=>true]) !!}
<div class="content">
        <div class="hidden" name="EnglishGate">
                <input name="EnglishGate" type="text" value="{{$EnglishGate}}" class="form-control">
        </div>
        <div class="hidden" name="branch_id">
                <input name="branch_id" type="text" value="{{$branch}}" class="form-control">
        </div>
        
<div class="form-group row"> 
    <div class="col-md-6" style="margin:0 auto;"> 
            {{ Form::label('list_id', trans('admin.list_id'),null, ['class' => 'control-label']) }}
            {{ Form::select('list_id',$list ,null, array_merge(['class' => 'form-control list_id','required' => 'required','placeholder'=>trans('admin.select')])) }}
    </div>
</div>
<div class="element">
<div class="form-group row">
        <div class="col-md-6">
            <label for="{{trans('admin.grammer_name')}}" class="control-label">{{trans('admin.grammer_name')}}</label> 
            <input  name="name_en[]" type="text" class="form-control">
        </div>
        <div class="col-md-6">
            <label for="{{trans('admin.grammer_name_ar')}}" class="control-label">{{trans('admin.grammer_name_ar')}}</label> 
            <input  name="name_ar[]" type="text" class="form-control">
        </div>
        <div class="col-md-6">
            <label for="{{trans('admin.grammer_content')}}" class="control-label">{{trans('admin.grammer_content')}}</label> 
            <textarea name="grammer_content[]" style=" resize: none;" cols="50" rows="10" class="form-control"></textarea>
        </div>
        <div class="col-md-6">
            <label for="{{trans('admin.grammer_content_ar')}}" class="control-label">{{trans('admin.grammer_content_ar')}}</label> 
            <textarea name="grammer_content_ar[]" style=" resize: none;" cols="50" rows="10" class="form-control"></textarea>
        </div>
    </div>
</div>

<br>
<div class="clearfix"></div>
{{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
<a href="{{aurl('englishgrammer')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
{!! Form::close() !!}

</div>





    



