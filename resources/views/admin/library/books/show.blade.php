@extends('admin.index')
@section('title',trans('admin.library'))
@section('content')
    @push('css')
        <style>
            .list-group-item {
                padding: 30px 15px !important;
            }
        </style>

    @endpush

<div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>

    <div class="box-body">
           
            
    <div class="form-group row">


                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.branch'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($librarybook->branch->name_ar,$librarybook->branch->name_en )}}</div>
                </div>

                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.list'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($librarybook->list->name_ar,$librarybook->list->name_en )}}</div>
                </div> 

                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.word'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($librarybook->name_en,$librarybook->name_ar)}}</div>
                </div>

                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.meaning'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($librarybook->desc_ar,$librarybook->desc_en)}}</div>

                </div>

                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.EnglishGate'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$librarybook->author}}</div>
                </div>

                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.publish_date'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$librarybook->publish_date}}</div>
                </div>

                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.librarytype'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{\App\Enums\LibraryType::getDescription($librarybook->Librarytype)}}</div>
                </div>
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.grade_id'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($librarybook->grade->name_en,$librarybook->grade->name_ar )}}</div>
                </div>
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.Term'), null, ['class' => 'control-label']) }}
                    
                    <div class="form-control">{{\App\Enums\Terms::getDescription($librarybook->Term)}}</div>
                </div>
                <div class="col-md-3 ">
                    {{ Form::label(trans('admin.booktype'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{\App\Enums\BookType::getDescription($librarybook->booktype)}}</div>
                </div>

                @if($librarybook->image != null)
                    <div class="col-md-3 ">
                        {{ Form::label(trans('admin.media'), null, ['class' => 'control-label']) }}
                        @if($librarybook->image)
                            <img class="form-control" src="{{asset('storage/'.$librarybook->image)}}" style="width: 217px; height:145px; margin-top: 10px">
                        @endif
                    </div>
                @endif
                

             

            </div>
            <a href="{{aurl('librarybooks')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
    </div>
    </div>
   
@endsection