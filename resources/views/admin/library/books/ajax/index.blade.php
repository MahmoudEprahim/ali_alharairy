
  
{!! Form::open(['method'=>'POST','route' => 'librarybooks.store','files'=>true]) !!}
<div class="content">

        <div class="hidden" name="Librarytype">
            <input name="Librarytype" type="text" value="{{$librarytype}}" class="form-control">
        </div>

        <div class="hidden" name="branch_id">
            <input name="branch_id" type="text" value="{{$branch}}" class="form-control">
        </div>


    <div class="element">
        <div class="fom-group row">
            <div class="col-md-6">
                {{ Form::label(trans('admin.list_id'),null, ['class' => 'control-label']) }}
                {{ Form::select('list_id',$list,  null, array_merge(['class' => 'form-control list_id', 'placeholder'=>trans('admin.select')])) }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                {{ Form::label(trans('admin.booktype'), null, ['class' => 'control-label']) }}
                {{ Form::select('booktype',\App\Enums\BookType::toSelectArray() ,null, array_merge(['class' => 'form-control booktype','placeholder'=>trans('admin.select')])) }}
            </div>
            <div class="col-md-4">
                {{ Form::label(trans('admin.grade'), null, ['class' => 'control-label']) }}
                {{ Form::select('grade_id',$grade ,null, array_merge(['class' => 'form-control gradetype','placeholder'=>trans('admin.select')])) }}
            </div>
            <div class="col-md-4">
                {{ Form::label(trans('admin.Term'), null, ['class' => 'control-label']) }}
                {{ Form::select('Term',\App\Enums\Terms::toSelectArray() ,null, array_merge(['class' => 'form-control Term','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>

       
        <div class="form-group row">
            <div class="col-md-3">
                <label for="{{trans('admin.name_ar')}}" class="control-label">{{trans('admin.name_ar')}}</label> 
                <input  name="name_ar" type="text" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="{{trans('admin.name_en')}}" class="control-label">{{trans('admin.name_en')}}</label> 
                <input  name="name_en" type="text" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="{{trans('admin.desc_ar')}}" class="control-label">{{trans('admin.desc_ar')}}</label> 
                <textarea name="desc_ar" style=" resize: none;" cols="50" rows="10" class="form-control"></textarea>
            </div>
            <div class="col-md-3">
                <label for="{{trans('admin.desc_en')}}" class="control-label">{{trans('admin.desc_en')}}</label> 
                <textarea name="desc_en" style=" resize: none;" cols="50" rows="10" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-3">
                <label for="{{trans('admin.author')}}" class="control-label">{{trans('admin.author')}}</label> 
                <input  name="author" type="text" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="{{trans('admin.publish_date')}}" class="control-label">{{trans('admin.date')}}</label> 
                <input  name="publish_date" type="date" class="form-control">
            </div>

            <div class="col-md-3 ">
                <label for="image" >{{trans('admin.image')}}</label>
                <input name="image" type="file" id="image">
            </div>
            <div class="col-md-3 ">
                <label for="media" >{{trans('admin.media')}}</label>
                <input name="media" type="file" id="media">
            </div>
        </div>
    </div>

</div>


<br>
<div class="clearfix"></div>
{{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
<a href="{{aurl('librarybooks')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
{!! Form::close() !!}


    



