@extends('admin.index')
@section('title',trans('admin.library'))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($librarybook,['method'=>'PUT','route' => ['librarybooks.update',$librarybook->id],'files'=>true]) !!}
            

             <!--student name -->
             
                    <div class="form-group">
                            <div class="col-md-3">
                                {{ Form::label(trans('admin.branch_id'),null, ['class' => 'control-label']) }}
                                {{ Form::select('branch_id',$branch,  null, array_merge(['class' => 'form-control branche_id', 'placeholder'=>trans('admin.select')])) }}
                            </div>

                            <div class="col-md-3">
                                {{ Form::label(trans('admin.list_id'),null, ['class' => 'control-label']) }}
                                {{ Form::select('list_id',$list,  null, array_merge(['class' => 'form-control branche_id', 'placeholder'=>trans('admin.select')])) }}
                            </div>
                
                            <div class="col-md-3"> 
                                {{ Form::label(trans('admin.booktype'), null, ['class' => 'control-label']) }}
                                {{ Form::select('booktype',\App\Enums\BookType::toSelectArray($librarybook->booktype) ,$librarybook->booktype, array_merge(['class' => 'form-control booktype','placeholder'=>trans('admin.select')])) }}
                            </div>
                            <div class="col-md-3">
                                {{ Form::label(trans('admin.grade_type'), null, ['class' => 'control-label']) }}
                                {{ Form::select('grade_id',$grade, $librarybook->grade_id, array_merge(['class' => 'form-control gradetype','placeholder'=>trans('admin.select')])) }}
                            </div>
                            <div class="col-md-3">
                                {{ Form::label(trans('admin.Term'), null, ['class' => 'control-label']) }}
                                {{ Form::select('Term',\App\Enums\Terms::toSelectArray($librarybook->Term) ,$librarybook->Term, array_merge(['class' => 'form-control Term','placeholder'=>trans('admin.select')])) }}
                            </div>
                            <div class="col-md-3">
                                {{ Form::label(trans('admin.Librarytype'), null, ['class' => 'control-label']) }}
                                {{ Form::select('Librarytype',\App\Enums\Librarytype::toSelectArray($librarybook->Librarytype) ,$librarybook->Librarytype, array_merge(['class' => 'form-control Librarytype','placeholder'=>trans('admin.select')])) }}
                            </div>
                            
                        </div>

                            <div class="form-group row">
                                <div class="col-md-3">
                                    {{ Form::label(trans('admin.name_ar'),null, ['class' => 'control-label']) }}
                                    {{ Form::text('name_ar',old('name_ar'), array_merge(['class' => 'form-control'])) }}
                                </div>

                                <div class="col-md-3">
                                    {{ Form::label(trans('admin.name_en'),null, ['class' => 'control-label']) }}
                                    {{ Form::text('name_en',old('name_en'), array_merge(['class' => 'form-control'])) }}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::label(trans('admin.desc_ar'),null, ['class' => 'control-label']) }}
                                    {{ Form::textarea('desc_ar',old('desc_ar'), array_merge(['class' => 'form-control'])) }}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::label(trans('admin.desc_en'),null, ['class' => 'control-label']) }}
                                    {{ Form::textarea('desc_en',old('desc_en'), array_merge(['class' => 'form-control'])) }}
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    {{ Form::label(trans('admin.author'),null, ['class' => 'control-label']) }}
                                    {{ Form::textarea('author',old('author'), array_merge(['class' => 'form-control'])) }}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::label(trans('admin.publish_date'),null, ['class' => 'control-label']) }}
                                    {{ Form::date('publish_date',old('publish_date'), array_merge(['class' => 'form-control'])) }}
                                </div>
                    
                                <div class="col-md-3 ">
                                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                                    {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                                </div>
                                <div class="col-md-3 ">
                                    {{ Form::label(trans('admin.media'), null, ['class' => 'control-label']) }}
                                    {{ Form::file('media', array_merge(['class' => 'form-control'])) }}
                                </div>

                                @if($librarybook->image != null)
                                <div class="col-md-3 ">
                                    {{ Form::label(trans('admin.media'), null, ['class' => 'control-label']) }}
                                    @if($librarybook->image)
                                        <img class="form-control" src="{{asset('storage/'.$librarybook->image)}}" style="width: 217px; height:145px; margin-top: 10px">
                                    @endif
                                </div>
                                @endif
                            </div>
            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.edit'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('librarybooks')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
