@extends('admin.index')
@section('title',trans('admin.create_new_images'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
      

        {!! Form::open(['method'=>'POST','route' => 'images.store','files'=>true]) !!}
        

        <!--student name -->
            <div class="form-group row">
                
            <div class="col-md-6">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                </div>

                
            </div>
            
            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('library/images')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
            </div>
        

        

        


       
    </div>
</div>
        







@endsection
