@extends('admin.index')
@section('title',trans('admin.edit_library_images'))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($libraryresource,['method'=>'PUT','route' => ['resources.update',$libraryresource->id],'files'=>true]) !!}
            

             <!--student name -->
             <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
            </div>  
            <div class="col-md-6">
                {{ Form::label(trans('admin.book'), null, ['class' => 'control-label']) }}
                {{ Form::file('book', array_merge(['class' => 'form-control'])) }}
            </div>     
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label(trans('admin.book_author'), null, ['class' => 'control-label']) }}
                {{ Form::text('book_author', old('book_author'), array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-md-6">
                {{ Form::label(trans('admin.book_name'), null, ['class' => 'control-label']) }}
                {{ Form::text('book_name', old('book_name'), array_merge(['class' => 'form-control'])) }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label(trans('admin.book_description'), null, ['class' => 'control-label']) }}
                {{ Form::textarea('desciption', old('desciption'), array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-md-6">
                {{ Form::label(trans('admin.brief_history'), null, ['class' => 'control-label']) }}
                {{ Form::textarea('bief_history', old('bief_history'), array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-md-6">
                {{ Form::label(trans('admin.publish_date'), null, ['class' => 'control-label']) }}
                {{ Form::text('publish_date', old('publish_date'), array_merge(['class' => 'form-control datepicker'])) }}
            </div>
        </div>



            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.edit'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('resources')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
