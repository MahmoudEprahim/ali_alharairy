@extends('admin.index')
@section('title',trans('admin.Create_new_englishdescription'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
 
 
<div class="box-body">
    {!! Form::open(['method'=>'POST','route' => 'librarymedia.store','files'=>true]) !!}
 <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label(trans('admin.branch_id'),null, ['class' => 'control-label']) }}
                {{ Form::select('branch_id',$branch,  null, array_merge(['class' => 'form-control branche_id', 'placeholder'=>trans('admin.select')])) }}
            </div>

            <div class="col-md-6">
                {{ Form::label(trans('admin.list_id'),null, ['class' => 'control-label']) }}
                {{ Form::select('list_id',$list,  null, array_merge(['class' => 'form-control list_id', 'placeholder'=>trans('admin.select')])) }}
            </div>
                     
            <div class="col-md-6">
                {{ Form::label(trans('admin.Librarytype'), null, ['class' => 'control-label']) }}
                {{ Form::select('librarytype',\App\Enums\LibraryType::toSelectArray() ,null, array_merge(['class' => 'form-control librarytype','placeholder'=>trans('admin.select')])) }}
            </div>
 
 

        <div class="col-md-6">
            {{ Form::label(trans('admin.desc_ar'), null, ['class' => 'control-label']) }}
            {{ Form::text('desc_ar',null, array_merge(['class' => 'form-control'])) }}
        </div>
        <div class="col-md-6">
            {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
            {{ Form::text('desc_en',null, array_merge(['class' => 'form-control'])) }}
        </div>
        <div class="col-md-6">
            {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
            {{ Form::text('name_ar',null, array_merge(['class' => 'form-control'])) }}
        </div>
        <div class="col-md-6">
            {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
            {{ Form::text('name_en',null, array_merge(['class' => 'form-control'])) }}
        </div>
        <div class="col-md-6 ">
        {{ Form::label('media', trans('admin.upload_media') . ':', array( 'style' => 'margin-top: 20px;')) }}
        {{ Form::file('media') }}
        </div>
</div>


 <br>
    <div class="clearfix"></div>
    {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
    <a href="{{aurl('librarymedia')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
    {!! Form::close() !!}


</div>


@endsection




