
 {!! Form::open(['method'=>'POST','route' => 'librarymedia.store','files'=>true]) !!}
 <div class="content">
 <div class="hidden" name="librarytype">
                <input name="librarytype" type="text" value="{{$librarytype}}" class="form-control">
        </div>
        <div class="hidden" name="branch_id">
                <input name="branch_id" type="text" value="{{$branch}}" class="form-control">
        </div>
        <div class="hidden" name="list_id">
                <input name="list_id" type="text" value="{{$list}}" class="form-control">
        </div>

    


<div class="form-group row">
    <div class="col-md-6">
        {{ Form::label(trans('admin.desc_ar'), null, ['class' => 'control-label']) }}
        {{ Form::text('desc_ar',null, array_merge(['class' => 'form-control'])) }}
    </div>
    <div class="col-md-6">
        {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
        {{ Form::text('desc_en',null, array_merge(['class' => 'form-control'])) }}
    </div>
    <div class="col-md-6">
        {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
        {{ Form::text('name_ar',null, array_merge(['class' => 'form-control'])) }}
    </div>
    <div class="col-md-6">
        {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
        {{ Form::text('name_en',null, array_merge(['class' => 'form-control'])) }}
    </div>
    <div class="col-md-6 ">
    {{ Form::label('media', trans('admin.upload_media') . ':', array( 'style' => 'margin-top: 20px;')) }}
    {{ Form::file('media') }}
    </div>
</div>
 </div>

 <br>
<div class="clearfix"></div>
{{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
<a href="{{aurl('librarymedia')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
{!! Form::close() !!}








