@extends('admin.index')
@section('title',trans('admin.Create_new_englishdescription'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush



@push('js')
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
        <script>

            $('.content').on('click', '.remove', function() {
                $('.remove').closest('.content').find('.element').not(':first').last().remove();
            });
            $('.content').on('click', '.clone', function() {
                $('.clone').closest('.content').find('.element').first().clone().appendTo('.results');

            }); 
        </script>

        <script>
            $(function () {
                'use strict';

                $('.librarytype ').on('change',function () {

                    var branche_val = $('.branche_id option:selected').val();
                    var librarytype = $('.librarytype option:selected').val();

                    $("#loadingmessage").css("display","block");
                    $(".column-form").css("display","none");
                    console.log(librarytype);
                    if (this){
                        $.ajax({
                            url: '{{aurl('library_content')}}',
                            type:'get',
                            dataType:'html',
                            data:{branche_id : branche_val, librarytype : librarytype},
                            success: function (data) {
                                $("#loadingmessage").css("display","none");
                                $('.column-form').css("display","block").html(data);
                            }
                        });
                    }else{
                        $('.column-form').html('not found');
                    }
                });


            });
        </script>

    @endpush





<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
      

       
        

        
        <div class="form-group row">
            <div class="col-md-3">
                {{ Form::label(trans('admin.branch_id'),null, ['class' => 'control-label']) }}
                {{ Form::select('branch_id',$branch,  null, array_merge(['class' => 'form-control branche_id', 'placeholder'=>trans('admin.select')])) }}
            </div>

            <div class="col-md-3">
                {{ Form::label(trans('admin.list_id'),null, ['class' => 'control-label']) }}
                {{ Form::select('list_id',$list,  null, array_merge(['class' => 'form-control list_id', 'placeholder'=>trans('admin.select')])) }}
            </div>
                    
            <div class="col-md-3">
                {{ Form::label(trans('admin.Librarytype'), null, ['class' => 'control-label']) }}
                {{ Form::select('librarytype',\App\Enums\LibraryType::toSelectArray() ,null, array_merge(['class' => 'form-control librarytype','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
            

           
                
            
            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('librarymedia')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
        

        

        


       
    </div>
</div>
        







@endsection
