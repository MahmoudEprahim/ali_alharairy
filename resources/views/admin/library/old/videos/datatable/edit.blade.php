@extends('admin.index')
@section('title',trans('admin.edit_library_videos'))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($libraryvideo,['method'=>'PUT','route' => ['videos.update',$libraryvideo->id],'files'=>true]) !!}
            

             <!--student name -->
             <div class="form-group row">
                
                <div class="col-md-6">
                    {{ Form::label(trans('admin.video'), null, ['class' => 'control-label']) }}
                    {{ Form::file('video', array_merge(['class' => 'form-control'])) }}
                </div>
<!-- 
                <video width="320" height="240" controls>
					<source src="/uploads/posts/video/{{$post->postMedia->path}}" controls style='width:250px;height:250px;'>
				</video> -->


            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.edit'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('library/videos')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
