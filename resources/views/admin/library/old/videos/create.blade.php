@extends('admin.index')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.Create_New_Video')}}</h3>
        </div>

        <div class="box-body">
            {!!  Form::open(array('route' => 'videos.store', 'files' => 'true')) !!}
 
            
            {{ Form::label('video', trans('admin.upload_video') . ':', array( 'style' => 'margin-top: 20px;')) }}
            {{ Form::file('video') }}

            {{ Form::submit(trans('admin.Create_New_Video'), array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}

            {!! Form::close() !!}
        </div>
    </div>
@endsection