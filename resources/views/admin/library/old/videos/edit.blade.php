@extends('admin.index')

@section('content')
    <div class="row">
        {!! Form::model($libraryvideo, ['route' => ['videos.update', $libraryvideo['id']], 'method' => 'PUT', 'files' => 'true']) !!}
        <div class="col-md-8">
            
            {{ Form::label('video', trans('admin.upload_image') . ':', array( 'style' => 'margin-top: 20px;')) }}
            {{ Form::file('video') }}
        </div>

        <div class="col-sm-8">
            <video width="320" height="240" controls>
				<source src="{{url('/')}}/uploads/library/video/{{$libraryvideo->video}}" controls style='width:250px;height:250px;'>
			</video>
        </div>

        <div class="col-md-4">
            <div class="well">
                <div>
                    <label>{{trans('admin.Created_At')}}:</label>
                    <p>{{ date('M j, Y H:i A', strtotime($libraryvideo->created_at)) }}</p>
                </div>
                <div>
                    <label>{{trans('admin.Last_Update_At')}}:</label>
                    <p>{{ date('M j, Y h:i A', strtotime($libraryvideo->updated_at)) }}</p>
                </div>
                <hr>
                <div class="row text-center">
                    <div class="col-sm-6">
                        {!! Html::linkRoute('videos.show', trans('admin.Cancle'), array($libraryvideo['id']), array('class' => 'btn btn-danger btn-block')) !!}
                    </div>

                    <div class="col-sm-6">
                        {{ Form::submit(trans('admin.Save'), array('class' => 'btn btn-success btn-block')) }}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div> <!-- end of row -->
@endsection