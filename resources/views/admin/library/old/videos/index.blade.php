@extends('admin.index')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <h1>{{trans('admin.All_videos')}}</h1>
        </div>

        <div class="col-md-4">
            <a href="{{ route('videos.create') }}" class="btn btn-primary btn-block btn-line" style="margin-top: 20px;">{{trans('admin.Create_New_video')}}</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>#{{trans('admin.ID')}}</th>
                    <th>{{trans('admin.tag_video')}}</th>
                    <th>{{trans('admin.Control')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($videos as $video)

                    <tr>
                        <th>{{ $video->id }}</th>
                        
                        <td>
                        <video width="320" height="240" controls>
					        <source src="{{url('/')}}/uploads/library/video/{{$video->video}}" controls style='width:250px;height:250px;'>
				        </video>
                        </td>
                        <td>
                            {!! Html::linkRoute('videos.show', trans('admin.View'), array($video['id']), array('class' => 'btn btn-success btn-sm')) !!}
                            {!! Html::linkRoute('videos.edit', trans('admin.edit'), array($video['id']), array('class' => 'btn btn-primary btn-sm')) !!}
                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection







