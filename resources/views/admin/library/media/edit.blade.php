@extends('admin.index')
@section('title',trans('admin.library'))
@section('content')
        @include('admin.layouts.message')
        
        <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
            <div class="box-body">
                {!! Form::model($content,['method'=>'PUT','route' => ['librarymedia.update',$content->id],'files'=>true]) !!}
                
                <div class="form-group row">
                    <div class="col-md-3">
                        {{ Form::label(trans('admin.branch_id'),null, ['class' => 'control-label']) }}
                        {{ Form::select('branch_id',$branch,  null, array_merge(['class' => 'form-control branche_id', 'placeholder'=>trans('admin.select')])) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label(trans('admin.list_id'),null, ['class' => 'control-label']) }}
                        {{ Form::select('list_id',$list,  null, array_merge(['class' => 'form-control branche_id', 'placeholder'=>trans('admin.select')])) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::label(trans('admin.Librarytype'), null, ['class' => 'control-label']) }}
                        {{ Form::select('Librarytype',\App\Enums\LibraryType::toSelectArray($content->Librarytype) ,$content->Librarytype, array_merge(['class' => 'form-control librarytype','placeholder'=>trans('admin.select')])) }}
                    </div>
                </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            {{ Form::label(trans('admin.name_ar'),null, ['class' => 'control-label']) }}
                            {{ Form::text('name_ar',old('name_ar'), array_merge(['class' => 'form-control'])) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::label(trans('admin.name_en'),null, ['class' => 'control-label']) }}
                            {{ Form::text('name_en',old('name_en'), array_merge(['class' => 'form-control'])) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::label(trans('admin.desc_ar'),null, ['class' => 'control-label']) }}
                            {{ Form::textarea('desc_ar',old('desc_ar'), array_merge(['class' => 'form-control'])) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::label(trans('admin.desc_en'),null, ['class' => 'control-label']) }}
                            {{ Form::textarea('desc_en',old('desc_en'), array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div> 
                    <div class="form-group row">
                        <div class="col-md-3 ">
                            {{ Form::label(trans('admin.media'), null, ['class' => 'control-label']) }}
                            {{ Form::file('media', array_merge(['class' => 'form-control'])) }}
                        </div>
                        @if($content->media != null)
                        <div class="col-sm-3">
                        @if($content->media)
                            <img src="{{asset('uploads/library/books/'.$content->media)}}" style="width: 100px;margin-top: 10px">
                        @endif
                        @if($content->media == 'video')
                        <video src="{{asset('uploads/library/books/'.$content->media)}}"></video>
                        @endif
                        </div>
                        @endif
                    </div>
                <br>
                <div class="clearfix"></div>
                {{Form::submit(trans('admin.edit'),['class'=>'btn btn-primary'])}}
                <a href="{{aurl('librarymedia')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
                {!! Form::close() !!}
            
            </div>
        </div>


@endsection
