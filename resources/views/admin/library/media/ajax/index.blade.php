
 {!! Form::open(['method'=>'POST','route' => 'librarymedia.store','files'=>true]) !!}
 <div class="content">
 <div class="hidden" name="Librarytype">
                <input name="Librarytype" type="text" value="{{$librarytype}}" class="form-control">
        </div>
        <div class="hidden" name="branch_id">
                <input name="branch_id" type="text" value="{{$branch}}" class="form-control">
        </div>
       

    
        <div class="fom-group row">
            <div class="col-md-6">
                {{ Form::label(trans('admin.list_id'),null, ['class' => 'control-label']) }}
                {{ Form::select('list_id',$list,  null, array_merge(['class' => 'form-control list_id', 'placeholder'=>trans('admin.select')])) }}
            </div>
        </div>

<div class="form-group row">
    <div class="col-md-6">
        {{ Form::label(trans('admin.desc_ar'), null, ['class' => 'control-label']) }}
        {{ Form::text('desc_ar',null, array_merge(['class' => 'form-control'])) }}
    </div>
    <div class="col-md-6">
        {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
        {{ Form::text('desc_en',null, array_merge(['class' => 'form-control'])) }}
    </div>
    <div class="col-md-6">
        {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
        {{ Form::text('name_ar',null, array_merge(['class' => 'form-control'])) }}
    </div>
    <div class="col-md-6">
        {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
        {{ Form::text('name_en',null, array_merge(['class' => 'form-control'])) }}
    </div>
    <div class="col-md-6 ">
    {{ Form::label('media', trans('admin.upload_media') . ':', array( 'style' => 'margin-top: 20px;')) }}
    {{ Form::file('media') }}
    </div>
</div>
 </div>

 <br>
<div class="clearfix"></div>
{{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
<a href="{{aurl('librarymedia')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
{!! Form::close() !!}








