@extends('admin.index')
@section('title',trans('admin.library'))
@section('content')
   

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>

        <div class="box-body">
            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label(trans('admin.branch_id'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($content->branch->name_ar,$content->branch->name_en )}}</div>
                </div>
                <div class="col-md-4"> 
                    {{ Form::label(trans('admin.list_id'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($content->list->name_ar,$content->list->name_en )}}</div>
                </div>
                <div class="col-md-4 ">
                    {{ Form::label(trans('admin.Librarytype'), null, ['class' => 'control-label']) }}
                    
                    {{ Form::select('Librarytype',\App\Enums\LibraryType::toSelectArray($content->Librarytype) ,$content->Librarytype, array_merge(['class' => 'form-control disabled librarytype','placeholder'=>trans('admin.select')])) }}
                </div>
               
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.name_ar'),null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->name_ar}}</div>
                </div>
                <div class="col-md-6">
                    {{ Form::label(trans('admin.name_en'),null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->name_en}}</div>
                </div>
            </div>

            <div class="for-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.desc_ar'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->desc_ar}}</div>
                </div>
                <div class="col-md-6">
                    {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->desc_en}}</div>
                </div>
            </div> 

            <div class="form-group row">
                @if($content->media != null)
                    <div class="col-sm-4">
                    @if($content->media)
                        <img src="{{asset('uploads/library/books/'.$content->media)}}" style="width: 100px;margin-top: 10px">
                    @endif
                    </div>
                @endif
            </div>
            <a href="{{aurl('librarymedia')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        </div>
    </div>
   
@endsection