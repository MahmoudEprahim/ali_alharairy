@extends('admin.index')
@section('title',trans('admin.library'))
@section('root_link', route('images.index'))
@section('root_name', trans('admin.images'))
@section('content')
   

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>

        <div class="box-body">
            

            <div class="form-group row">
                
                <div class="col-md-6">
                    {{ Form::label(trans('admin.name_en'),null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->name_en}}</div>
                </div>
            </div>

            <div class="for-group row">
               
                <div class="col-md-6">
                    {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->desc_en}}</div>
                </div>
            </div> 

            <div class="form-group row">
                @if($content->image != null)
                    <div class="col-sm-4">
                    @if($content->image)
                        <img src="{{asset('storage/'.$content->image)}}" style="width: 100%;margin-top: 10px">
                    @endif
                    </div>
                @endif
            </div>
            <a href="{{aurl('images')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        </div>
    </div>
   
@endsection