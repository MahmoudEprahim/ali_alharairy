@extends('admin.index')
@section('title',trans('admin.edit_library_images'))
@section('root_link', route('images.index'))
@section('root_name', trans('admin.images'))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($content,['method'=>'PUT','route' => ['images.update',$content->id],'files'=>true]) !!}
            

             <!--student name -->
             <div class="form-group row">

                <!--<div class="col-md-3">-->
                <!--    {{ Form::label(trans('admin.branch_id'),null, ['class' => 'control-label']) }}-->
                <!--    {{ Form::select('branch_id',$branch,  null, array_merge(['class' => 'form-control branche_id', 'placeholder'=>trans('admin.select')])) }}-->
                <!--</div>-->
                
                
                <div class="col-md-4">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                </div>

                <div class="col-sm-4">
                <img src="{{asset('storage/'.$content->image)}}" style="width: 100%;margin-top: 10px">
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.name_en'),null, ['class' => 'control-label']) }}
                    {{ Form::text('name_en',old('name_en'), array_merge(['class' => 'form-control'])) }}
                </div>
            <div class="col-md-4">
                    {{ Form::label(trans('admin.desc_en'),null, ['class' => 'control-label']) }}
                    {{ Form::textarea('desc_en',old('desc_en'), array_merge(['class' => 'form-control'])) }}
                </div>
            </div>
            
               

            <div class="form-group row">
                <!--<div class="col-md-3">-->
                <!--    {{ Form::label(trans('admin.name_ar'),null, ['class' => 'control-label']) }}-->
                <!--    {{ Form::text('name_ar',old('name_ar'), array_merge(['class' => 'form-control'])) }}-->
                <!--</div>-->
                
                <!--<div class="col-md-3">-->
                <!--    {{ Form::label(trans('admin.desc_ar'),null, ['class' => 'control-label']) }}-->
                <!--    {{ Form::textarea('desc_ar',old('desc_ar'), array_merge(['class' => 'form-control'])) }}-->
                <!--</div>-->
                
            </div>
                



            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.edit'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('images')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
