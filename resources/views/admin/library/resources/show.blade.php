@extends('admin.index')
@section('title',trans('admin.library'))
@section('root_link', route('resources.index'))
@section('root_name', trans('admin.resources'))
@section('content')
   

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>

        <div class="box-body">
            <div class="for-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.desc_ar'), null, ['class' => 'control-label']) }}
                    <div><a href='{{$content->desc_ar}}'>{{$content->desc_ar}}</a> </div>
                </div>
                <div class="col-md-6">
                    {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
                    <div >{{$content->desc_en}}</div>
                </div>
            </div> 

            <div class="form-group row">
                @if($content->image != null)
                    <div class="col-sm-4">
                    @if($content->image)
                        <img src="{{asset('storage/'.$content->image)}}" style="width: 100px;margin-top: 50px">
                    @endif
                    </div>
                @endif

                <div class="col-md-6 ">
                
                @if($content->video_src != null)
                <iframe width="320" height="240" src="{{$content->video_src}}" style="width: 250px;margin-top: 50px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @endif
                </div>
            </div>

            

            <a href="{{aurl('resources')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        </div>
    </div>
   
@endsection