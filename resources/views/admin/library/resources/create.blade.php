@extends('admin.index')
@section('title',trans('admin.library'))
@section('root_link', route('resources.index'))
@section('root_name', trans('admin.resources'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
        {!! Form::open(['method'=>'POST','route' => 'resources.store','files'=>true]) !!}
        <div class="form-group row">
            <div class="col-md-6 col-sm-6">
                <div class="col-md-12">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                </div>

                <div class="col-md-12">
                    {{ Form::label(trans('admin.media'), null, ['class' => 'control-label']) }}
                    {{ Form::file('media', array_merge(['class' => 'form-control'])) }}
                </div>

                <div class="col-md-12">
                {{ Form::label(trans('admin.driveLink'), null, ['class' => 'control-label']) }}
                {{ Form::text('desc_ar',null, array_merge(['class' => 'form-control'])) }}
                </div>
                
                <div class="col-md-12">
                    {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('desc_en',null, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>
        </div>
            
        <br>
        <div class="clearfix"></div>
        {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
        <a href="{{aurl('resources')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        {!! Form::close() !!}
    </div>
</div>
@endsection
