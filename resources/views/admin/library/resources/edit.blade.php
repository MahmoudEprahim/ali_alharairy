@extends('admin.index')
@section('title',trans('admin.library'))
@section('root_link', route('resources.index'))
@section('root_name', trans('admin.resources'))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($content,['method'=>'PUT','route' => ['resources.update',$content->id],'files'=>true]) !!}
            

             <!--student name -->
             <div class="form-group row">

                <div class="col-md-6 -sm-6">
                    <div class="col-md-12">
                        {{ Form::label(trans('admin.media'), null, ['class' => 'control-label']) }}
                        {{ Form::file('media', array_merge(['class' => 'form-control'])) }}
                    </div>
                    
                    <div class="col-md-12">
                        {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                        {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                    </div>

                    <div class="col-md-12">
                    {{ Form::label(trans('admin.driveLink'), null, ['class' => 'control-label']) }}
                    {{ Form::text('desc_ar',null, array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="col-md-12">
                        {{ Form::label(trans('admin.desc_en'),null, ['class' => 'control-label']) }}
                        {{ Form::textarea('desc_en',old('desc_en'), array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>

                <div class="col-md-6 -sm-6">
                    <div class="col-sm-12">
                        @if($content->image != null)
                        <img src="{{asset('storage/'.$content->image)}}" style="width: 200px;margin-top: 10px">
                        @endif
                    </div>
                </div>
            </div>
            
               

            <!--<div class="form-group row">-->
            <!--    <div class="col-md-3">-->
            <!--        {{ Form::label(trans('admin.name_ar'),null, ['class' => 'control-label']) }}-->
            <!--        {{ Form::text('name_ar',old('name_ar'), array_merge(['class' => 'form-control'])) }}-->
            <!--    </div>-->
            <!--    <div class="col-md-3">-->
            <!--        {{ Form::label(trans('admin.name_en'),null, ['class' => 'control-label']) }}-->
            <!--        {{ Form::text('name_en',old('name_en'), array_merge(['class' => 'form-control'])) }}-->
            <!--    </div>-->
            <!--    <div class="col-md-3">-->
            <!--        {{ Form::label(trans('admin.desc_ar'),null, ['class' => 'control-label']) }}-->
            <!--        {{ Form::textarea('desc_ar',old('desc_ar'), array_merge(['class' => 'form-control'])) }}-->
            <!--    </div>-->
            <!--    >-->
            <!---->
                

            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.edit'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('resources')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
