@extends('admin.index')
@section('title',trans('admin.library'))
@section('root_link', route('videos.index'))
@section('root_name', trans('admin.videos'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
      

        {!! Form::open(['method'=>'POST','route' => 'videos.store','files'=>true]) !!}
        

            <div class="form-group row">
                

                <div class="col-md-2 required">
                    {{ Form::label(trans('admin.VideoType'), null, ['class' => 'control-label']) }}
                    {{ Form::select('VideoType',\App\Enums\VideoType::toSelectArray() ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>


                <div class="col-md-3">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                </div>

                <div class="col-md-3">
                    {{ Form::label(trans('admin.video_src'), null, ['class' => 'control-label']) }}
                    {{ Form::text('video_src',null, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('desc_en',null, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>
            <div class="form-group row">
                
                
            </div>
            
            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <!--<a href="{{aurl('videos')}}" class="btn btn-danger">{{trans('admin.back')}}</a>-->
            {!! Form::close() !!}
            </div>
        

        

        


       
    </div>
</div>
        







@endsection
