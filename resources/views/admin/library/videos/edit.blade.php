@extends('admin.index')
@section('title',trans('admin.library'))
@section('root_link', route('videos.index'))
@section('root_name', trans('admin.videos'))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($content,['method'=>'PUT','route' => ['videos.update',$content->id],'files'=>true]) !!}
            

             <!--student name -->
             <div class="form-group row">

                
                <div class="col-md-2 required"> 
                    {{ Form::label(trans('admin.EnglishGate'), null, ['class' => 'control-label']) }}
                    {{ Form::select('VideoType',\App\Enums\VideoType::toSelectArray(),$content->VideoType , array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>
                
                
                <div class="col-md-3">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                    
                </div>
                

                <div class="col-md-3">
                    {{ Form::label(trans('admin.video_src'), null, ['class' => 'control-label']) }}
                    {{ Form::text('video_src',null, array_merge(['class' => 'form-control'])) }}
                </div>
                
                <div class="col-md-4">
                    {{ Form::label(trans('admin.desc_en'),null, ['class' => 'control-label']) }}
                    {{ Form::textarea('desc_en',old('desc_en'), array_merge(['class' => 'form-control'])) }}
                </div>

            </div>
               

            <div class="form-group row">
                <div class="col-sm-6">
                    @if($content->image != null)
                    <img src="{{asset('storage/'.$content->image)}}" style="width: 100%;margin-top: 10px">
                    @endif
                </div>
                
                <div class="col-sm-6" style="height: 317px;">
                     @if($content->video_src != null)
                        <iframe width="100%" height="100%" src="{{$content->video_src}}" style="margin-top: 10px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    @endif
                </div>
            
                
            </div>
                



            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.edit'),['class'=>'btn btn-primary'])}}
            <!--<a href="{{aurl('videos')}}" class="btn btn-danger">{{trans('admin.back')}}</a>-->
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
