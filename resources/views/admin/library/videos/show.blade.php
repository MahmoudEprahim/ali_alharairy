@extends('admin.index')
@section('title',trans('admin.library'))
@section('root_link', route('videos.index'))
@section('root_name', trans('admin.videos'))
@section('content')
   

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>

        <div class="box-body">
            <div class="form-group row">
                
                
                <div class="col-md-8">
                    {{ Form::label(trans('admin.desc_en'), null, ['class' => 'control-label']) }}
                    <div >{{$content->desc_en}}</div>
                </div>
            </div>
            
            <div class="col-sm-6" style="magin-bottom:20px;">
                    @if($content->image != null)
                    <img src="{{asset('storage/'.$content->image)}}" style="width: 100%;margin-top: 10px; ">
                    @endif
                </div>
                
                <div class="col-sm-6" style="height: 317px;magin-bottom:20px;">
                     @if($content->video_src != null)
                        <iframe width="100%" height="100%" src="{{$content->video_src}}" style="margin-top: 10px; margin-bottom:20px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    @endif
                </div>
            

            <br>
            <div class="clearfix"></div>

            <!--<a href="{{aurl('videos')}}" class="btn btn-danger">{{trans('admin.back')}}</a>-->
        </div>
    </div>
   
@endsection