@extends('admin.index')
@section('title',trans('admin.library'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
      

        {!! Form::open(['method'=>'POST','route' => 'librarylists.store','files'=>true]) !!}
        

        <!--teacher name -->
            <div class="form-group row">
                <div class="col-md-4 required">
                    {{ Form::label(trans('admin.Librarytype'), null, ['class' => 'control-label']) }}
                    {{ Form::select('Librarytype',\App\Enums\Librarytype::toSelectArray() ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                    
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_ar', null, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_en',null, array_merge(['class' => 'form-control'])) }}
                </div>
                
            </div>

        {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
        <a href="{{aurl('librarylists')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        {!! Form::close() !!}
        </div>
        

        

        


       
    </div>
</div>
        







@endsection
