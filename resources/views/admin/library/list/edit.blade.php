@extends('admin.index')
@section('title',trans('admin.library'))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($list,['method'=>'PUT','route' => ['librarylists.update',$list->id],'files'=>true]) !!}
            

            <div class="form-group row">
                <div class="col-md-4 ">
                    {{ Form::label(trans('admin.Librarytype'), null, ['class' => 'control-label']) }}
                    {{ Form::select('Librarytype',\App\Enums\Librarytype::toSelectArray() ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>

                <div class="col-md-4">
                {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                {{ Form::text('name_ar', $list->name_ar, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-4 ">

                {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                {{ Form::text('name_en', $list->name_en, array_merge(['class' => 'form-control'])) }}
                </div>

                


            </div>


            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.update'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('librarylists')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
