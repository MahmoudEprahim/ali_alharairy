<div class="parent">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label(trans('admin.arabic_name'), null, ['class' => 'control-label']) }}
                <br>
                {{$parent->name_ar}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label(trans('admin.english_name'), null, ['class' => 'control-label']) }}
                <br>
                {{$parent->name_en}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('relation', trans('admin.relation'), ['class' => 'control-label']) }}
                <br>
                {{\App\Enums\RelationType::getDescription($parent->relation)}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label(trans('admin.phone_1'), null, ['class' => 'control-label']) }}
                <br>
                {{$parent->phone_1}}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label(trans('admin.phone_2'), null, ['class' => 'control-label']) }}
                <br>
                {{$parent->phone_2}}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }} <strong>1</strong>
                <br>
                {{$parent->mobile_1}}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }} <strong>2</strong>
                <br>
                {{$parent->mobile_2}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('par_countries', trans('admin.nationality'), ['class' => 'control-label']) }}
                <br>
                {{session_lang(getcountries($parent->countries_id)->country_name_en,getcountries($parent->countries_id)->country_name_ar)}}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('par_type', trans('admin.prove_personal'), ['class' => 'control-label']) }}
                <br>
                {{\App\Enums\PersonalType::getDescription($parent->type)}}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('par_number', trans('admin.number'), ['class' => 'control-label']) }}
                <br>
                {{$parent->number}}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('par_created_from', trans('admin.source'), ['class' => 'control-label']) }}
                <br>
                {{$parent->created_from}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('par_job', trans('admin.job'), ['class' => 'control-label']) }}
                <br>
                {{$parent->job }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('par_home_address', trans('admin.home_address'), ['class' => 'control-label']) }}
                <br>
                {{$parent->home_address}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('par_note', trans('admin.note'), ['class' => 'control-label']) }}
                <br>
                {{$parent->note}}
            </div>
        </div>
    </div>
</div>