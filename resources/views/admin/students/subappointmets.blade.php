<div class="appointment">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label(trans('admin.appointment'), null, ['class' => 'control-label']) }}
                <br>
                {{$appointments->groups}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label(trans('admin.english_name'), null, ['class' => 'control-label']) }}
                <br>
                {{$appointments->days}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('relation', trans('admin.relation'), ['class' => 'control-label']) }}
                <br>
                {{$appointments->capacity}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('relation', trans('admin.relation'), ['class' => 'control-label']) }}
                <br>
                {{$appointments->branch->name_en}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('relation', trans('admin.relation'), ['class' => 'control-label']) }}
                <br>
                {{$appointments->grade->name_en}}
            </div>
        </div>
    </div>
    
</div>
