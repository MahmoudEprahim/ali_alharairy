@push('js')
        <script>
            $(function () {
                'use strict'

                var student_id = $('#student_id').val()
                $.ajax({
                    url: '{{route('getStudentPrice')}}',
                    type:'get',
                    dataType:'html',
                    data:{
                    "_token": "{{ csrf_token() }}",
                    student_id: student_id
                    },
                    success: function (data) {
                        // $('.student-register-table tbody').html(data);
                    }
                });


            });


        </script>

@endpush




<div class="tab-pane fade in" id="payment_data">
    <h3></h3>
    <div class="form-group">

        {!! Form::open(['method'=>'POST','route' => 'payments.store','files'=>true]) !!}
                <input id="student_id" type="hidden" value="{{$student->id}}">

                <div class="form-group row">
                    @foreach(\App\Enums\MonthName::toSelectArray() as $key => $value)
                    <div class="col-md-2">
                        {{ Form::label(trans('admin.payment_value'), \App\Enums\MonthName::getDescription($key), ['class' => 'control-label']) }}
                        @if(count($allStudentPayment) == count(\App\Enums\MonthName::toSelectArray()))
                        <div class="form-group">
                            <input type="text" data-month-id={{$key}} value="{{$allStudentPayment[$key-1]->fees}}" class="form-control price">
                        </div>
                        @else
                        <div class="form-group">
                            <input type="text" data-month-id={{$key}} value="" class="form-control price">
                        </div>
                        @endif
                    </div>
                    @endforeach
                </div>


            
            {!! Form::close() !!}

        <br>
        <br>
    </div>
</div>



