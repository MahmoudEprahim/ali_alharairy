<div class="tab-pane fade in active" id="busowner">
    <h3></h3>
    <div class="row form-group">
        <div class="col-md-2 required">
            {{ Form::label('created_at', trans('admin.register_date'), ['class' => 'control-label']) }}
            {{ Form::text('created_at', date('Y-m-d',strtotime($student->created_at)), array_merge(['required' => 'required','class' => 'form-control', 'disabled'])) }}
        </div>
        <div class="col-md-2 required">
            {{ Form::label('acc_no', trans('admin.acc_no'), ['class' => 'control-label']) }}
            {{ Form::number('acc_no', $student->acc_no , array_merge(['required' => 'required','title'=>trans('admin.acc_no'), 'class' => 'form-control',  'placeholder'=>trans('admin.acc_no')])) }}
        </div>
        <div class="col-md-2 required">
            {{ Form::label('branches', trans('admin.branche'), ['class' => 'control-label']) }}
            {{ Form::select('branches_id', $branche,$student->branches_id, array_merge(['required' => 'required','class' => 'form-control owner branches_id', 'placeholder'=>trans('admin.select')])) }}
        </div>
        <div class="col-md-2 required">
            {{ Form::label('gender', trans('admin.gender'), ['class' => 'control-label']) }}
            {{ Form::select('gender', \App\Enums\GenderType::toSelectArray(),$student->gender, array_merge(['required' => 'required','class' => 'form-control owner','placeholder'=>trans('admin.select')])) }}
        </div>
        
        <div class="col-md-4 required">
            {{ Form::label(trans('admin.name'), null, ['class' => 'control-label']) }}
            {{ Form::text('name_en', $student->name_en, array_merge(['required' => 'required','class' => 'form-control'])) }}
        </div>
        
        <div class="col-md-3 required">
            {{ Form::label('email', trans('admin.email'), ['class' => 'control-label']) }}
            {{ Form::text('email',$student->email, array_merge(['required' => 'required','class' => 'form-control'])) }}
        </div>        
        
        <div class="col-md-3">
            {{ Form::label('password', trans('admin.password'), ['class' => 'control-label']) }}
            {{ Form::password('password',array_merge(['class' => 'form-control password', 'id' => 'password'])) }}
        </div>

        <div class="col-md-3 required">
            {{ Form::label('phone', trans('admin.mob'), ['class' => 'control-label']) }}
            {{ Form::text('phone',$student->phone, array_merge(['required' => 'required','class' => 'form-control owner'])) }}
        </div>

        <div class="col-md-3 required">
            {{ Form::label('mobile_1', trans('admin.parent_mob_no'), ['class' => 'control-label']) }}
            @if($student->parent)
            {{ Form::text('mobile_1',$student->parent->mobile_1, array_merge(['required' => 'required','class' => 'form-control'])) }}
            @else
            {{ Form::text('mobile_1',null, array_merge(['required' => 'required','class' => 'form-control'])) }}
            @endif
        </div>

        <div class="col-md-3 required">
            {{ Form::label('grades_id',trans('admin.student_grade'), ['class' => 'control-label']) }}
            {{ Form::select('grades_id',$grades ,$student->grades_id, array_merge(['required' => 'required','class' => 'form-control lvl grades_id','placeholder'=>trans('admin.select')])) }}
        </div>
        <div class="col-md-3 ">
            {{ Form::label('job',trans('admin.parent_job'), ['class' => 'control-label']) }}
            @if($student->parent)
            {{ Form::text('job',$student->parent->job, array_merge(['class' => 'form-control'])) }}
            @else
            {{ Form::text('job',null, array_merge(['class' => 'form-control'])) }}
            @endif 
        </div>
        
        <div class="col-md-12 ">
            {{ Form::label(trans('admin.teacher_notes'), null, ['class' => 'control-label']) }}
            {{ Form::textarea('teacher_notes', $student->teacher_notes, array_merge(['class' => 'form-control ','style'=>'direction: rtl'])) }}
        </div>
        
        
    </div>
    

    
    <div class="row">

        

        <div class="col-md-4">
            {{ Form::label('image', trans('admin.image'), ['class' => 'control-label']) }}
            {{Form::file('image')}}

            @if($student->image != null)
                <img src="{{asset('storage/'.$student->image)}}" style="width: 150px" />
            @else
                <img src="{{url('/')}}adminlte/preview_Image.png" style="width: 150px">
            @endif
        </div>
    </div>
</div>

