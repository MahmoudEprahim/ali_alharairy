@push('js')

    <script>
        $(function () {
            'use strict'
            $('#parents').on('change',function () {
                console.log($(this).val());
                if ($(this).val() != '') {
                    $('.parent').css({display:'none'});
                } else{
                    $('.parent').css({display:'block'});
                }
            });
        });
    </script>
    <script>
        $(function () {
            'use strict'

            $(document).on('change','#parents',function () {
                var parents = $('#parents option:selected').val();
                console.log(parents);
                if (parents) {
                    $.ajax({
                        url: '{{aurl('sub_parents')}}',
                        type: 'post',
                        dataType: 'html',
                        data: {"_token": "{{ csrf_token() }}",parents: parents},
                        success: function (data) {
                            $('.column-parent').html(data);
                        }
                    });
                }else{
                    $('.column-parent').html('');
                }
            });

        });
    </script>

    @endpush








    @push('js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
        <script>
            $(function () {
                'use strict';
                $('.parent_id').on('change',function () {

                    var parent_id = $('.parent_id option:selected').val();
    
                    console.log(parent_id);
                    if (this){
                        $.ajax({
                            url: '{{aurl('student_data')}}',
                            type:'get',
                            dataType:'html',
                            data:{parent_id : parent_id,},
                            success: function (data) {
                                $("#loadingmessage").css("display","none");
                                $('.column-form').css("display","block").html(data);

                            }
                        });
                    }else{
                        $('.column-form').html('');
                    }
                });


            });
        </script>

    @endpush









<div class="tab-pane fade in" id="activity">
    <h3></h3>
    <br>
    <div class="form-group">
        {{ Form::label('employees_sons', trans('admin.employees_sons'), ['class' => 'control-label']) }}
        <br>

        @foreach(\App\Enums\EmployeesSonsType::toArray() as $index => $employees_sons)
            {{ Form::label('employees_sons',trans('admin.'.\App\Enums\EmployeesSonsType::getKey($employees_sons)), ['class' => 'control-label']) }}
            <input style="margin-left:30px" type="radio" name="employees_sons" id="{!! $employees_sons !!}" value="{{$employees_sons}}" @if ($student->employees_sons == $employees_sons) checked @endif>

        @endforeach
    </div>
    <br>

    <div class="form-group">
        
            {{ Form::label(trans('admin.parents'), null, ['class' => 'control-label']) }}
            {{ Form::select('parent_id', $parents,$student->parent_id != null ? $student->parents->id : null, array_merge(['class' => 'searchselect form-control parent_id','placeholder'=>trans('admin.select'),'id'=>'parents'])) }}
    </div>
    <div class="column-parent">

    </div>
        <div class="parent" style="@if($student->parent_id == null)  display:none; @endif">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label(trans('admin.arabic_name'), null, ['class' => 'control-label']) }}
                        {{ Form::text('par_name_ar', $student->parent_id != null?$student->parents->name_ar:null, array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label(trans('admin.english_name'), null, ['class' => 'control-label']) }}
                        {{ Form::text('par_name_en', $student->parent_id != null?$student->parents->name_en:null, array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('relation', trans('admin.relation'), ['class' => 'control-label']) }}
                        {{ Form::select('relation', \App\Enums\RelationType::toSelectArray(),$student->parent_id != null?$student->parents->relation:null, array_merge(['class' => 'form-control owner','placeholder'=>trans('admin.select')])) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label(trans('admin.phone_1'), null, ['class' => 'control-label']) }}
                        {{ Form::text('phone_1', $student->parent_id != null?$student->parents->phone_1:null, array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label(trans('admin.phone_2'), null, ['class' => 'control-label']) }}
                        {{ Form::text('phone_2', $student->parent_id != null?$student->parents->phone_2:null, array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }} <strong>1</strong>
                        {{ Form::text('mobile_1', $student->parent_id != null?$student->parents->mobile_1:null, array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }} <strong>2</strong>
                        {{ Form::text('mobile_2', $student->parent_id != null?$student->parents->mobile_2:null, array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('par_number', trans('admin.number'), ['class' => 'control-label']) }}
                        {{ Form::text('par_number',$student->parent_id != null?$student->parents->number:null, array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('par_job', trans('admin.job'), ['class' => 'control-label']) }}
                        {{ Form::text('par_job',$student->parent_id != null?$student->parents->job:null, array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('par_home_address', trans('admin.home_address'), ['class' => 'control-label']) }}
                        {{ Form::text('par_home_address',$student->parent_id != null?$student->parents->home_address:null, array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('par_note', trans('admin.note'), ['class' => 'control-label']) }}
                        {{ Form::text('par_note',$student->parent_id != null?$student->parents->note:null, array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>
            </div>
        </div>

</div>
