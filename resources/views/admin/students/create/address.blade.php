
<div class="tab-pane fade in" id="menu6">
    <h3></h3>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.street'), null, ['class' => 'control-label']) }}
                {{ Form::text('street', $student->street, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.house_num'), null, ['class' => 'control-label']) }}
                {{ Form::text('house_num', $student->house_num, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.phone_1'), null, ['class' => 'control-label']) }}
                {{ Form::text('phone_1', $student->phone_1, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }}
                {{ Form::text('phone_2', $student->phone_2, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('near',trans('admin.near'), ['class' => 'control-label']) }}
                {{ Form::textarea('near' ,$student->near, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
    </div>
</div>
