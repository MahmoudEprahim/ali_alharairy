

<div class="tab-pane fade in" id="timeline">
<h3></h3>
    <div class="form-group">
        {{ Form::label('bus', trans('admin.sub_bus'), ['class' => 'control-label']) }}
        {{ Form::select('bus', \App\Enums\busType::toSelectArray() ,$student->bus, array_merge(['class' => 'form-control subbus','placeholder'=>trans('admin.select')])) }}
    </div>
    {{--loader spinner--}}
    <div id='loadingmessage' style='display:none; margin-top: 20px' class="text-center">
        <img src="{{ url('/') }}/images/ajax-loader.gif"/>
    </div>
    <div class="column-form">
        @if($student->bus != 0 && $student->bus_id != null)
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('bus_id', trans('admin.bus'), ['class' => 'control-label']) }}
                        {{ Form::select('bus_id', $buses ,$student->bus_id, array_merge(['class' => 'form-control bus','placeholder'=>trans('admin.select')])) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('driver_id', trans('admin.transition_officer'), ['class' => 'control-label']) }}
                        {{ Form::select('driver_id', $drivers ,$student->driver_id, array_merge(['class' => 'form-control bus','placeholder'=>trans('admin.select')])) }}
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>