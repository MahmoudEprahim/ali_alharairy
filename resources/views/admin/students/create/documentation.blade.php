@push('css')
    <link rel="stylesheet" href="{{url('/')}}/css/dropzone.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .dropzone .dz-preview:hover .dz-image img,.dropzone .dz-preview .dz-image img{
            width: 100%;
        }
    </style>
@endpush
@push('js')
    <script src="{{url('/')}}/js/dropzone.min.js"></script>
    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            'use strict'
            $('#dropzonefield').dropzone({
                url: "{{aurl('upload/image/'.$student->id)}}" ,
                paramName:'file' ,
                uploadMultiple:false ,
                maxFiles:15,
                maxFilesize:10,
                dictDefaultMessage:"{{trans('admin.Click_here_to_upload_files_or_drag_and_drop_files_here')}}",
                dictRemoveFile:"حذف",
                acceptedFiles:'image/*',
                params:{
                    _token: '{{csrf_token()}}' ,
                },
                addRemoveLinks:true,
                removedfile: function (file) {
                    $.ajax({
                        dataType:'json',
                        type:'POST',
                        url:'{{aurl('delete/image')}}',
                        data:{id:file.id,_token:'{{csrf_token()}}'},
                    });
                    var fmock;
                    return (fmock = file.previewElement) != null ? fmock.parentNode.removeChild(file.previewElement):void 0;
                },
                sending:function (file,xhr,formData) {
                    formData.append('fid', '');
                    file.fid = '';
                },
                success:function (file,response) {
                    file.fid = response.id;
                },
                init:function () {
                            @foreach($student->files()->get() as $file)
                    var mock = {name: '{{$file->name}}',id: '{{$file->id}}',size:'{{$file->size}}',type:'{{$file->mime_type}}'};
                    this.emit('addedfile',mock);
                    this.options.thumbnail.call(this,mock,'{{url('storage/'.$file->full_file)}}');
                    @endforeach

                }
            });

        });

    </script>

@endpush

<div class="tab-pane" id="menu4">
{{--    <div class="form-group">--}}
{{--        {{ Form::label(trans('admin.bus_color'), null, ['class' => 'control-label']) }}--}}
{{--        {{ Form::color('buscolor', $->buscolor, array_merge(['class' => 'form-control'])) }}--}}
{{--    </div>--}}
    <center><h5 class="h2">{{trans('admin.main_photo')}}</h5></center>
    <div class="form-group">
        {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
        {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
        @if($student->image != null)
            <img src="{{asset('storage/'.$student->image)}}" style="width: 100px" class="img-responsive">
        @else
            <div class="alert alert-danger">{{trans('admin.no_image_yet')}}</div>

        @endif
    </div>
    <center><h5 class="h2">{{trans('admin.all_images')}}</h5></center>
    <div class="dropzone" id="dropzonefield"></div>

</div>