<div class="tab-pane" id="menu7">
    <center><h5 class="h2"></h5></center>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.image_accommodation'), null, ['class' => 'control-label']) }}
                {{ Form::file('image_1', array_merge(['class' => 'form-control upld'])) }}

                <img style="width: 100px;" class="img-responsive" id="blah" src='' />
            </div>
        </div>
        <div class="col-md-6">
            @if($student->image_1 != null)
                <img src="{{asset('storage/'.$student->image_1)}}" class="img-responsive">
            @endif

        </div>
    </div>

    <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label(trans('admin.image_birth_certificate'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image_2', array_merge(['class' => 'form-control upld'])) }}
                    <img style="width: 100px;" class="img-responsive" id="blah" src=''  />
                </div>
            </div>
        <div class="col-md-6">

            @if($student->image_2 != null)
                <img src="{{asset('storage/'.$student->image_2)}}" class="img-responsive">

            @endif
        </div>
    </div>

{{--<div class="row">--}}
{{--    <div class="col-md-6">--}}
{{--        <div class="form-group">--}}
{{--            {{ Form::label(trans('admin.image_birth_certificate'), null, ['class' => 'control-label']) }}--}}
{{--            {{ Form::file('image_3', array_merge(['class' => 'form-control upld'])) }}--}}

{{--            <img style="width: 100px;" class="img-responsive" id="blah" src=''  />--}}

{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="col-md-6">--}}
{{--        @if($student->image_birth_certificate != null)--}}
{{--            <img src="{{asset($student->image_birth_certificate)}}" class="img-responsive">--}}
{{--        @endif--}}
{{--    </div>--}}
{{--</div>--}}
{{--<div class="row">--}}
{{--    <div class="col-md-6">--}}
{{--        <div class="form-group">--}}
{{--            {{ Form::label(trans('admin.family_id_number'), null, ['class' => 'control-label']) }}--}}
{{--            {{ Form::file('image_4', array_merge(['class' => 'form-control upld'])) }}--}}

{{--            <img style="width: 100px;" class="img-responsive" id="blah" src=''  />--}}

{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="col-md-6">--}}
{{--        @if($student->family_id_number != null)--}}
{{--            <img src="{{asset($student->family_id_number)}}"  class="img-responsive">--}}

{{--        @endif--}}
{{--    </div>--}}

{{--</div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-6">--}}
{{--            <div class="form-group">--}}
{{--                {{ Form::label(trans('admin.certificate_previous_year'), null, ['class' => 'control-label']) }}--}}
{{--                {{ Form::file('image_5', array_merge(['class' => 'form-control upld'])) }}--}}

{{--                <img style="width: 100px;" class="img-responsive" id="blah" src=''/>--}}

{{--            </div>--}}

{{--        </div>--}}
{{--        <div class="col-md-6">--}}
{{--            @if($student->certificate_previous_year != null)--}}
{{--                <img src="{{asset($student->certificate_previous_year)}}"  class="img-responsive">--}}

{{--            @endif--}}
{{--        </div>--}}

{{--    </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6">--}}
{{--                <div class="form-group">--}}
{{--                    {{ Form::label(trans('admin.Health_card'), null, ['class' => 'control-label']) }}--}}
{{--                    {{ Form::file('image_6', array_merge(['class' => 'form-control upld'])) }}--}}

{{--                    <img style="width: 100px;" class="img-responsive" id="blah" src=''  />--}}

{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-6">--}}
{{--                @if($student->Health_card != null)--}}
{{--                    <img src="{{asset($student->Health_card)}}" class="img-responsive">--}}
{{--                @endif--}}
{{--            </div>--}}

{{--        </div>--}}
</div>
