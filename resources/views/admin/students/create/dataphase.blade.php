@push('js')
    <script>
        $(function () {
            'use strict'

            $(document).on('change','.learn_time,.level',function () {
                var learn_time = $('.learn_time option:selected').val();
                var level = $('.level option:selected').val();
                var student = '{{$student->id}}';
                console.log(learn_time,level,student);
                if (learn_time && level) {
                    $.ajax({
                        url: '{{aurl('phase')}}',
                        type: 'get',
                        dataType: 'html',
                        data: {learn_time: learn_time, level: level, student: student},
                        success: function (data) {
                            $('.column-phase').html(data);
                        }
                    });
                }else{
                    $('.column-phase').html('');
                }
            });

        });
    </script>

@endpush
<div class="tab-pane fade in" id="category">
    <h3></h3>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('learn_time',trans('admin.learn_time'), ['class' => 'control-label']) }}
                {{ Form::select('learn_time',\App\Enums\learntimeType::toSelectArray() ,$student->learn_time, array_merge(['class' => 'form-control learn_time','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
        <div class="col-md-6">


            <div class="form-group">
                {{ Form::label('grades_id',trans('admin.student_grade'), ['class' => 'control-label']) }}
                {{ Form::select('grades_id',$grades ,$student->grades_id, array_merge(['class' => 'form-control lvl grades_id','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
    </div>
    <div class="column-phase">

    </div>
    <div class="row">
        {{-- <div class="col-md-4">
       
        {{--    <div class="form-group"> --}}
        {{--    {{ Form::label('appointments_id',trans('admin.appointment_id'), ['class' => 'control-label']) }} --}}
        {{--       {{ Form::select('appointments_id',$appointments,$student->appointments_id, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}  --}}
        {{--        </div> --}}
        {{-- </div> --}} 

        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('Joining_date',trans('admin.Joining_date'), ['class' => 'control-label']) }}
                {{ Form::text('Joining_date',$student->Joining_date, array_merge(['class' => 'form-control datepicker','style'=>'direction:rtl'])) }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('past_school',trans('admin.past_school'), ['class' => 'control-label']) }}
                {{ Form::text('past_school',$student->past_school, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('previous_estimate',trans('admin.past_year_result'), ['class' => 'control-label']) }}
                {{ Form::text('previous_estimate' ,$student->previous_estimate, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('health_status',trans('admin.health_status'), ['class' => 'control-label']) }}
                {{ Form::text('health_status',$student->health_status, array_merge(['class' => 'form-control','style'=>'direction:rtl'])) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('education_notes',trans('admin.education_notes'), ['class' => 'control-label']) }}
                {{ Form::textarea('education_notes',$student->education_notes, array_merge(['class' => 'form-control','style'=>'direction:rtl'])) }}
            </div>
        </div>
    </div>
</div>
