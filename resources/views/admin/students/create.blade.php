@extends('admin.index')
@section('title',trans('admin.students_data'))
@section('root_link', route('students.index'))
@section('root_name', trans('admin.students'))
@section('content')

        @push('js')
            <script>

                $(document).ready(function(){

                    // this code create 12 records to student pauments and initialized whene document ready
                    var price = $('.price'), allMonth = [];
                    price.each(function(){
                        allMonth.push($(this).attr('data-month-id'))
                    });

                    $.ajax({
                        url: "{{route('payments.index')}}",
                        type: 'get',
                        dataType: 'html',
                        data: {
                            student_id: $('#student_id').val(),
                            MonthName: allMonth
                        }
                    });

                     // when  change input update payments for student
                    $('.price').change(function(){
                        var price = $(this).val(), student_id = $('#student_id').val(), MonthName = $(this).attr('data-month-id');

                        $.ajax({
                            url: '{{route('payments.store')}}',
                            type:'post',
                            dataType:'json',
                            data:{
                            "_token": "{{ csrf_token() }}",
                                student_id: student_id,
                                fees : price,
                                MonthName: MonthName
                            }
                        });
                    });


                    // appointment by branch and grade(level)
                    $('.grades_id, .branches_id').change(function(){
                        let grade_id = $('.grades_id').val(),
                            branch_id = $('.branches_id').val();
                        if(grade_id !== null && branch_id !== null){
                            $.ajax({
                                url: "{{route('show_appointments')}}",
                                type: "get",
                                dataType: 'html',
                                data:{'_token': "{{csrf_token()}}",
                                    grade_id:grade_id,
                                    branch_id:branch_id,
                                    student_id: "{{$student->id}}"
                                },
                                success: function (data) {
                                    $('.appointment').html(data);
                                    $.ajax({
                                        url: "{{route('show_exams')}}",
                                        type: "get",
                                        dataType: 'html',
                                        data:{'_token': "{{csrf_token()}}",
                                            grade_id:grade_id,
                                            branch_id:branch_id,
                                            student_id: "{{$student->id}}"
                                        },
                                        success: function (data) {
                                            $('.exams-data').html(data)
                                        }
                                    })
                                }
                            })
                        }
                    });

                    $('#password, #confirm_password').on('keyup', function () {
                    if ($('#password').val() == $('#confirm_password').val()) {
                        $('#message').html('Matching').css('color', 'green');
                    } else
                        $('#message').html('Not Matching').css('color', 'red');
                    });

                    $(document).on('change','.subbus',function () {
                        var bus = $('.subbus option:selected').val();
                        {{--                        --}}{{--var student = '{{$student->id}}';--}}
                        $("#loadingmessage").css("display","block");
                        $(".column-form").css("display","none");
                        console.log(bus);
                        if (bus){
                            $.ajax({
                                url: '{{aurl('subbus')}}',
                                type:'get',
                                dataType:'html',
                                data:{student:student,bus : bus},
                                success: function (data) {
                                    $("#loadingmessage").css("display","none");
                                    $('.column-form').css("display","block").html(data);
                                }
                            });
                        }else{
                            $('.column-form').html('');
                        }
                    });

                    function readURL(input) {

                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $(input).next('img').attr('src', e.target.result);
                            }

                            reader.readAsDataURL(input.files[0]);
                        }
                    }

                    $(".upld").change(function () {
                        readURL(this);
                    });
                })


            </script>
        @endpush

        <div class="box">
            @include('admin.layouts.message')
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div>
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{trans('admin.delete')}}</h4>
                        </div>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['students.destroy',$student->id],'id'=>'modal-delete']) !!}
                        <div class="modal-body">
                            <p>{{trans('admin.You_Want_You_Sure_Delete_This_Record')}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" value="delete">{{trans('admin.close')}}</button>
                            {!! Form::submit('delete', ['type' => 'submit', 'class' => 'btn btn-danger']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
            <div class="box-body">

                <br>
                {!! Form::model($student,['method'=>'PUT','route' => ['students.update',$student->id],'files'=>true]) !!}
                {{--{!! Form::submit( 'Save', ['class' => 'btn btn-default']) !!}--}}
                <div style="float:left; margin-bottom:30px">
                    {!! Form::button( '<i class="fa fa-floppy-o"></i> ' . trans('admin.save'), ['type' => 'submit','class' => 'btn btn-primary', 'name' => 'submitbutton', 'value' => 'save'])!!}
                    <a href="#" class="btn btn-danger"  data-toggle="modal" data-target="#myModal"><i class="fa fa-trash"></i> {{trans('admin.delete')}}</a>
                </div>
                <br>

                <div class="nav-tabs-custom" style="margin-top: 50px;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#busowner" data-toggle="tab"><i class="fa fa-user"></i> {{trans('admin.basicinformation')}}</a></li>
                        <li class=""><a href="#appointment" data-toggle="tab"><i class="fa fa-clock-o"></i> {{trans('admin.appointment')}}</a></li>
                        <li><a href="#menu9" data-toggle="tab"><i class="fa fa-university"></i> {{trans('admin.exams_data')}}</a></li>
                        <li><a href="#payment_data" data-toggle="tab"><i class="fa fa-credit-card"></i> {{trans('admin.payment_data')}}</a></li>
                    </ul>
                    <div class="tab-content">

                        @include('admin.students.create.basicinformation')
                        @include('admin.students.create.appointment')
                        @include('admin.students.create.exams_data')
                        @include('admin.students.create.payment_data')

                    </div>

                    <!-- /.tab-content -->
                </div>

                {!! Form::close() !!}



            </div>
        </div>




@endsection
