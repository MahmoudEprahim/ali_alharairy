@foreach($appointments as $appointment)
    <tr>
        <td> {{$appointment->groups}}</td>
        <td>{{$appointment->days}}</td>
        <td>@if($appointment->count == $appointment->capacity) No @else Yes @endif</td>
        @if($appointment->count == $appointment->capacity)
            <td><p class="text-danger text-center">No seats Available</p></td>
        @else
            <td><span class="remaining">{{$appointment->capacity-$appointment->count}}</span> seats <input class="available_place" value="{{$appointment->id}}" type="radio" name="appointments_id" style="float: right;"></td>
        @endif
    </tr>
@endforeach
