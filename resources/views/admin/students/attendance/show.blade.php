@extends('admin.index')
@section('title',trans('admin.show_attendances'))
@section('root_link', route('attendance.index'))
@section('root_name', trans('admin.attendance'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.show_attendances')}}</h3>
        </div>
        <div class="box-body">


            <div class="form-group row">
                
                
                
            </div>

            <div class="form-group row">

                <div class="col-md-4 ">
                    {{ Form::label('groups', trans('admin.groups'), ['class' => 'control-label']) }}
                    <div class="form-control">{{$attendance->groups}}</div>
                </div>

                <div class="col-md-4 ">
                    {{ Form::label(trans('admin.capacity'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$attendance->capacity}}</div>
                </div>
                <div class="col-md-4 ">
                    {{ Form::label(trans('admin.days'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$attendance->days}}</div>
                </div>

            </div>

            <a href="{{aurl('appointments')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>



@endsection
