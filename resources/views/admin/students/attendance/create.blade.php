@extends('admin.index')
@section('title',trans('admin.add_absence'))
@section('root_link', route('attendance.index'))
@section('root_name', trans('admin.absence'))
@section('content')


@push('js')

@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{trans('admin.add_absence')}}</h3>
    </div>
    <div class="box-body">


        {!! Form::open(['method'=>'POST','route' => 'attendance.store']) !!}

        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('student_id', trans('admin.student'), ['class' => 'control-label']) }}
                <select name="student_id" id="student_id" class="searchselect form-control">
                    <option value="">Select student</option>
                    @foreach($students as $student)
                        <option value="{{$student->id}}">{{$student->{'acc_no'} }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('month_id', trans('admin.month_id'), ['class' => 'control-label']) }}
                <select name="month_id" id="month_id" class="form-control searchselect">
                    <option value="">Select month</option>
                    @foreach(\App\Enums\MonthName::toSelectArray() as $key => $month)
                        <option value="{{$key}}">{{$month}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        


        @foreach(\App\Enums\Lecture::toSelectArray() as $key => $lecture)

        
            <div class="col-md-3 form-group">
                <div class="form-group">
                    {{ Form::label($lecture, $lecture, ['class' => 'control-label']) }}
                    <!-- <input type="checkbox" name="lecture_id[]" id="{{$lecture}}" class="checkbox-inline" value="{{$key}}"> -->
                </div>

                <div>
                    {{ Form::label('absence_date',trans('admin.absence_date'), ['class' => 'control-label']) }}
                    <input type="text" name="lecture_id[]" id="{{$lecture}}"  class="datepicker">
                    <!-- {{ Form::text('lecture_id[]',$student->absence_date, array_merge(['class' => 'form-control datepicker','style'=>'direction:rtl', 'placeholder' => trans('admin.choose_date')])) }} -->
                </div>
            </div>
       
            
        @endforeach

        <div class="col-md-12">
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-info btn-block'])}}
        </div>

            {!! Form::close() !!}
    </div>

</div>

@endsection
