@extends('admin.index')

@section('title',trans('admin.edit_absence'))
@section('root_link', route('attendance.index'))
@section('root_name', trans('admin.attendance'))
@section('content')


@push('js')

@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{trans('admin.edit_absence')}}</h3>
    </div>
    <div class="box-body">

        {!! Form::model($attendance,['method'=>'PUT','route' => ['attendance.update',$attendance->id]]) !!}

        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('student_id', trans('admin.student'), ['class' => 'control-label']) }}
                <input class="form-control" type="text" value="{{$attendance->student->name_en}}" readonly>
                <input type="hidden" name="student_id" value="{{$attendance->student->id}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('month_id', trans('admin.month_id'), ['class' => 'control-label']) }}
                <select class="form-control" name="month_id" id="month_id">
                    <option value="">Select month</option>
                    @foreach(\App\Enums\MonthName::toSelectArray() as $key => $month)
                        <option @if($key == $attendance->month_id) selected @endif  value="{{$key}}">{{$month}}</option>
                    @endforeach
                </select>
            </div>
        </div>




        @foreach(json_decode($attendance->lecture_id) as $storedKey => $value)
            <div class="col-md-2">
                <div class="form-group">
                    
                   
                <label for="">{{\App\Enums\Lecture::getDescription($storedKey+1)}}</label>
                    <input  type="text" name="lecture_id[]" value="{{$value}}" class="datepicker form-control">
                     
                     
                </div>
            </div>
            @endforeach




        <div class="col-md-12">
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-info btn-block'])}}
        </div>
            {!! Form::close() !!}
    </div>

</div>

@endsection
