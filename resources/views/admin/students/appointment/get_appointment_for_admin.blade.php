<div class="form-group">
    {{ Form::label(trans('admin.appointment'), null, ['class' => 'control-label']) }}
    <select name="appointments_id" id="appointments_id" class="appointments_id">
        <option value="null">select appointment</option>
        @foreach($appointments as $appointment)
            @if($appointment->capacity == $appointment->count) @continue @endif
            <option value="{{$appointment->id}}">{{$appointment->groups}}</option>
        @endforeach
    </select>
</div>
@if($student->appointments_id != null)
    <div class="parent" style="@if($student->appointments_id == null)  display:none; @endif">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {{ Form::label(trans('admin.appointment'), null, ['class' => 'control-label']) }}
                    <br>
                    {{$student->appointment->groups}}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {{ Form::label(trans('admin.days'), null, ['class' => 'control-label']) }}
                    <br>
                    {{$student->appointment->days}}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {{ Form::label('relation', trans('admin.capacity'), ['class' => 'control-label']) }}
                    <br>
                    {{$student->appointment->capacity}}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {{ Form::label('relation', trans('admin.branch'), ['class' => 'control-label']) }}
                    <br>
                    {{$student->appointment->branch->name_en}}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {{ Form::label('relation', trans('admin.grade_name'), ['class' => 'control-label']) }}
                    <br>
                    {{$student->appointment->grade->name_en}}
                </div>
            </div>
        </div>
    </div>
@endif
