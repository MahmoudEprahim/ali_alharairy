@extends('admin.index')
@section('title',trans('admin.edit_appointment'))
@section('root_link', route('appointments.index'))
@section('root_name', trans('admin.appointments'))
@section('content')
    @hasanyrole('writer|admin')
        <div class="box">
            @include('admin.layouts.message')
            <div class="box-header">
                <h3 class="box-title">{{trans('admin.edit_appointment')}}</h3>
            </div>
            <div class="box-body">
                {!! Form::model($appointment,['method'=>'PUT','route' => ['appointments.update',$appointment->id],'files'=>true]) !!}

                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::label('branch_id', trans('admin.branch_id'), ['class' => 'control-label']) }}
                        {{ Form::select('branch_id',$branche ,$appointment->branche_id, array_merge(['class' => 'form-control', 'placeholder'=>trans('admin.select')])) }}
                    </div>
                    <div class="col-md-6">
                        {{ Form::label('grade_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                        {{ Form::select('grade_id',$grade ,$appointment->grade_id, array_merge(['class' => 'form-control', 'placeholder'=>trans('admin.grade_id')])) }}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        {{ Form::label(trans('admin.appointment'), null, ['class' => 'control-label']) }}
                        {{ Form::text('groups', old('groups'), array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::label(trans('admin.days'), null, ['class' => 'control-label']) }}
                        {{ Form::text('days', old('days'), array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::label(trans('admin.capacity'), null, ['class' => 'control-label']) }}
                        {{ Form::text('capacity', old('capacity'), array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>

                <br>
                <div class="clearfix"></div>
                    {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
                    <a href="{{aurl('appointments')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
                    {!! Form::close() !!}
            </div>
        </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
