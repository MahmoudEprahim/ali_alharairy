<div class="table-responsive column-exams">
    <table class="table table-bordered table-hover table-striped text-center">
        <tr>
            <th>{{trans('admin.exams_name')}}</th>

            <th>{{trans('admin.grade')}}</th>

            <th>{{trans('admin.delete')}}</th>
        </tr>

        @foreach($student->exams as $exam)

            <tr>

                <td>{{$exam->name_ar}}</td>
                <td>{{$exam->pivot->grade}} </td>



                <td>
                    <a href="{{ route('remove_subexam.destroy', [$student->id, $exam->id]) }}" class="btn btn-danger waves-effect waves-light" >{{trans('admin.delete')}}</a>

            </td>
            </tr>
        @endforeach
    </table>
</div>

