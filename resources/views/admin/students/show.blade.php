@extends('admin.index')
@section('title',trans('admin.basicinformation'))
@section('content')
@push('css')
<<<<<<< HEAD
<style>
    .stle{
        margin-right: 0px !important;
        width: 12%;

    }
</style>
@endpush
        @push('js')
            <script>
                $(function () {
                    'use strict';
                    $('.bus').select2({
                        placeholder: "{{trans('admin.Select_a_State')}}",
                        allowClear: true
                    });
                });
            </script>
        @endpush
        @push('js')
            <script>
                $(function () {
                    'use strict';

                    $(document).on('change','.subbus',function () {
                        var bus = $('.subbus option:selected').val();
{{--                        --}}{{--var student = '{{$student->id}}';--}}
                        $("#loadingmessage").css("display","block");
                        $(".column-form").css("display","none");
                        console.log(bus);
                        if (bus){
                            $.ajax({
                                url: '{{aurl('subbus')}}',
                                type:'get',
                                dataType:'html',
                                data:{student:student,bus : bus},
                                success: function (data) {
                                    $("#loadingmessage").css("display","none");
                                    $('.column-form').css("display","block").html(data);
                                }
                            });
                        }else{
                            $('.column-form').html('');
                        }
                    });

                });
            </script>
            <script>
                function readURL(input) {

                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $(input).next('img').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $(".upld").change(function () {
                    readURL(this);
                });
            </script>

        @endpush

        <div class="box">
            @include('admin.layouts.message')
            <div class="box-header">
                <h3 class="box-title">{{trans('admin.show_basicinformation_students')}}</h3>
            </div>
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{trans('admin.delete')}}</h4>
                        </div>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['students.destroy',$student->id],'id'=>'modal-delete']) !!}
                        <div class="modal-body">
                            <p>{{trans('admin.You_Want_You_Sure_Delete_This_Record')}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" value="delete">{{trans('admin.close')}}</button>
                            {!! Form::submit('delete', ['type' => 'submit', 'class' => 'btn btn-danger']) !!}
                        </div>
                        {!! Form::close() !!}
=======
    <style>
        .list-group-item {
            padding: 30px 15px !important;
        }
        .arabic{
            direction: ltr;
        }
    </style>

    @endpush


        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        @if($student->image != null) 
                            <img class="profile-user-img img-responsive img-circle" src="{{asset('storage/'.$student->image)}}" alt="{{trans('admin.User_profile_picture')}}">
                        @else
                            <img src="{{asset('/')}}adminlte/previewImage.png" class="profile-user-img img-responsive img-circle" alt="User Image">
                        @endif

                        <h3 class="profile-username text-center">{{session_lang($student->name_en,$student->name_ar)}}</h3>

                        <p class="text-muted text-center">@if($student->grades){{session_lang($student->grades->name_en,$student->grades->name_ar)}}@endif</p>
                        <p class="text-muted text-center">{{\App\Enums\PerstatusType::getDescription($student->per_status)}}</p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
{{--                                <b>{{trans('admin.assigned_by')}}</b> <a class="pull-right">@if($student->user_id == null) <div class="badge">{{$student->admins->getRoleNames()[0]}}</div> {{$student->admins->name}} @else <div class="badge">User</div>{{session_lang($student->users->name_en,$student->users->name)}} @endif</a>--}}
                            </li>
                            <li class="list-group-item">
                                <b>{{trans('admin.student_status')}}</b> <br> <a class="pull-right">{{\App\Enums\TypeType::getDescription($student->type)}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>{{trans('admin.email')}}</b> <br> <a class="pull-right">{{$student->email}}</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{trans('admin.About_Subscriber')}}</h3>
>>>>>>> 9f9555e83be1a3b244190464a052e9de854a3dd2
                    </div>

                </div>
            </div>
            <div class="box-body">

                <br>
{{--                {!! Form::model($student,['method'=>'PUT','route' => ['students.update',$student->id],'files'=>true]) !!}--}}
                {{--{!! Form::submit( 'Save', ['class' => 'btn btn-default']) !!}--}}
                <div>
                    <a href="{{aurl('students')}}" class="btn btn-primary"> {{trans('admin.back')}}</a>
                </div>
                <br>

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" style="width: 105%;">
                        <li class="active stle"><a href="#busowner" data-toggle="tab"><i class="fa fa-user"></i> {{trans('admin.basicinformation')}}</a></li>
                        <li class="stle"><a href="#activity" data-toggle="tab"><i class="fa fa-user"></i> {{trans('admin.Parentdata')}}</a></li>
                        <li   class="stle" ><a href="#category"  data-toggle="tab"><i class="fa fa-list"></i> {{trans('admin.dataphase')}}</a></li>
                        <li class="stle" ><a href="#menu6" data-toggle="tab"><i class="fa fa-photo"></i> {{trans('admin.home_address')}}</a></li>
                        <li class="stle"  ><a href="#menu5" data-toggle="tab"><i class="fa fa-photo"></i> {{trans('admin.medical_history')}}</a></li>
                        <li class="stle"  ><a href="#menu3" data-toggle="tab"><i class="fa fa-university"></i> {{trans('admin.relativesinschool')}}</a></li>
                        <li class="stle"  ><a href="#menu7" data-toggle="tab"><i class="fa fa-photo"></i> {{trans('admin.document')}}</a></li>
                        <li class="stle"  ><a href="#menu9" data-toggle="tab"><i class="fa fa-university"></i> {{trans('admin.exams_data')}}</a></li>

                    </ul>
                    <div class="tab-content">

                        @include('admin.students.show.basicinformation')
                        @include('admin.students.show.Parentdata')
                        @include('admin.students.show.dataphase')
                        @include('admin.students.show.address')

                        @include('admin.students.show.medicalhistory')
                        @include('admin.students.show.relativesinschool')


                    @include('admin.students.show.documents')
                    @include('admin.students.show.exams_data')
                        {{--                    @include('admin.students.create.documentation')--}}
                        {{--                    @include('admin.students.create.arithmeticdata')--}}

                    </div>

                    <!-- /.tab-content -->
                </div>

                {!! Form::close() !!}



            </div>
        </div>




@endsection
