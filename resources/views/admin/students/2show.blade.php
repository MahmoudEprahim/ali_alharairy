@extends('admin.index')
@section('title',trans('admin.show_profile_to') .session_lang($student->name_en,$student->name_ar))
@section('content')
@push('css')
    <style>
        .list-group-item {
            padding: 30px 15px !important;
        }
        .arabic{
            direction: ltr;
        }
    </style>

    @endpush


        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        @if($student->image != null)
                            <img class="profile-user-img img-responsive img-circle" src="{{asset('storage/' . $student->image)}}" alt="{{trans('admin.User_profile_picture')}}">
                        @else
                            <img src="{{asset('/')}}adminlte/previewImage.png" class="profile-user-img img-responsive img-circle" alt="User Image">
                        @endif

                        <h3 class="profile-username text-center">{{session_lang($student->name_en,$student->name_ar)}}</h3>

                        <p class="text-muted text-center">@if($student->grades){{session_lang($student->grades->name_en,$student->grades->name_ar)}}@endif</p>
                        <p class="text-muted text-center">{{\App\Enums\PerstatusType::getDescription($student->per_status)}}</p>

                        <ul class="list-group list-group-unbordered">
                            @if($student->grade != null)
                            <li class="list-group-item">
                                <b>{{trans('admin.grade')}}</b> <br> <a class="pull-right">{{$student->grade->name_ar}}</a>
                            </li>
                            @endif
                                @if($student->appointments != null)
                            <li class="list-group-item">
                                <b>{{trans('admin.appointments_name')}}</b> <br> <a class="pull-right">{{$student->appointments->appointment}}</a>
                            </li>
                                @endif
                            <li class="list-group-item">
                                <b>{{trans('admin.email')}}</b> <br> <a class="pull-right">{{$student->email}}</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{trans('admin.About_Subscriber')}}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">



                        <strong><i class="fa fa-map-marker margin-r-5"></i> {{trans('admin.sUrl')}}</strong>

                        <p class="text-muted">{{$student->addriss}}</p>
                        <hr>

                        <strong><i class="fa fa-circle margin-r-5"></i> {{trans('admin.age')}}</strong>

                        <p class="text-muted">{{$student->age}}</p>

                        <hr>

                        <strong><i class="fa fa-circle margin-r-5"></i> {{trans('admin.gender')}}</strong>

                        <p class="text-muted">{{\App\Enums\GenderType::getDescription($student->gender)}}</p>



                            <hr>

                            <strong><i class="fa fa-phone margin-r-5"></i> {{trans('admin.mob')}} </strong>

                            <p class="text-muted">{{$student->phone}}</p>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#activity" data-toggle="tab">{{trans('admin.activity')}}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="activity">
                            <!-- Post -->
                            <ul class="timeline timeline-inverse">
                                <!-- timeline time label -->
                                <li class="time-label">
                                    <span class="@if(session('lang') == 'ar') arabic @else '' @endif @if($student->status == 0) bg-red @else bg-green @endif">
                                      {{date('Y M d', strtotime($student->created_at))}}
                                    </span>
                                    @if($student->branches != null)
                                        <span class="@if(session('lang') == 'ar') arabic @else '' @endif @if($student->status == 0) bg-red @else bg-green @endif">
                                          {{session_lang($student->branches->name_en,$student->branches->name_ar)}}
                                        </span>
                                    @endif

                                </li>
                                <li>
                                    <i class="fa fa-user bg-green"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header">{{trans('admin.arabic_name')}}</h3>

                                        <div class="timeline-body">
                                            <div class="form-group">
                                                {{session_lang($student->name_en,$student->name_ar)}}
                                            </div>
                                        </div>
                                    </div>
                                </li>

{{--                                0--}}
                                <li>
                                    <i class="fa fa-user bg-green"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header">{{trans('admin.number')}}</h3>

                                        <div class="timeline-body">
                                            <div class="form-group">
                                                {{session_lang($student->number,$student->number)}}
                                            </div>
                                        </div>
                                    </div>
                                </li>
{{--                                1--}}


                            </ul>
                            <!-- /.post -->
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                </div>
            </div>





</div>

@endsection
