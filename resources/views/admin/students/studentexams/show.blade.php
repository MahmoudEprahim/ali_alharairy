@extends('admin.index')
@section('title',trans('admin.show_exam'). ' ' . session_lang($exam->name_ar, $exam->name_en))
@section('content')   

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.show_content'). ' ' . session_lang($exam->name_ar, $exam->name_en)}}</h3>
        </div>
        <div class="box-body">
           

            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label('branche_id', trans('admin.branche_id'), ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($exam->Branches->name_en,$exam->Branches->name_ar)}}</div>
                </div>
                <div class="col-md-6">
                    {{ Form::label('grade_id', trans('admin.ar_grade_id'), ['class' => 'control-label']) }}
                    <div class="form-control">{{$exam->grade_id)}}</div>
                </div>
            </div>

            <div class="form-group row">
                
                <div class="col-md-4 ">
                    {{ Form::label('appointment', trans('admin.appointment'), ['class' => 'control-label']) }}
                    <div class="form-control">{{$exam->name_ar}}</div>
                </div>

                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$exam->name_en}}</div>
                </div>
                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    <img class="gallery_img profile-img" alt="img" src="{{asset('storage/'.$exam->image)}}">
        
                </div>

            </div>
        
            <a href="{{aurl('exams')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>



@endsection
