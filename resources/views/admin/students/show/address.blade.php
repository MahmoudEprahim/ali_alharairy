
<div class="tab-pane fade in" id="menu6">


    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label(trans('admin.street'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{$student->street}}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label(trans('admin.phone_1'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{$student->phone_1}}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{$student->phone_2}}</div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('near',trans('admin.near'), ['class' => 'control-label']) }}
                {{ Form::textarea('near' ,$student->near, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
    </div>
</div>
