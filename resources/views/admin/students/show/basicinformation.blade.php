<div class="tab-pane fade in active" id="busowner">

    <div class="row">
        @if($student->branches != null)
        <div class="col-md-4">
            <div class="form-group">

                {{ Form::label('branches', trans('admin.branche'), ['class' => 'control-label']) }}
                <div class="form-control" >{{$student->branches->name_ar}} </div>
            </div>
        </div>
        @endif
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('created_at', trans('admin.register_date'), ['class' => 'control-label']) }}
                {{ Form::text('created_at', date('Y-m-d',strtotime($student->created_at)), array_merge(['class' => 'form-control','disabled'])) }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('per_status', trans('admin.per_status'), ['class' => 'control-label']) }}
                <div class="form-control" >{{ \App\Enums\PerstatusType::getDescription($student->per_status)}} </div>

            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.arabic_name'), null, ['class' => 'control-label']) }}
                <div class="form-control" >{{$student->name_ar}} </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.english_name'), null, ['class' => 'control-label']) }}
                <div class="form-control" >{{$student->name_en}} </div>

            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('National number', trans('admin.number'), ['class' => 'control-label']) }}
                <div class="form-control" >{{$student->number}} </div>
            </div>
            </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('phone', trans('admin.mob'), ['class' => 'control-label']) }}
                <div class="form-control" >{{$student->phone}} </div>

            </div>
            </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label(trans('admin.birth_date'), null, ['class' => 'control-label']) }}
                <div class="form-control" >{{$student->birthdate}} </div>
            </div>
        </div>
        <div class="col-md-3">

{{--        <div class="form-group">--}}
{{--            {{ Form::label('bloodtype', trans('admin.blood'), ['class' => 'control-label']) }}--}}
{{--            {{ Form::select('bloodtype', \App\Enums\BloodType::toSelectArray(),$student->bloodtype, array_merge(['class' => 'form-control owner','placeholder'=>trans('admin.select')])) }}--}}
{{--        </div>--}}
        </div>



    </div>

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('addriss', trans('admin.student_address'), ['class' => 'control-label']) }}
                <div class="form-control" >{{$student->addriss}} </div>

            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('age', trans('admin.age'), ['class' => 'control-label']) }}
                <div class="form-control" >{{$student->age}} </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('gender', trans('admin.gender'), ['class' => 'control-label']) }}
                <div class="form-control" >{{\App\Enums\GenderType::getDescription($student->gender)}} </div>
            </div>
        </div>
    </div>

    <div class="row">


        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('email', trans('admin.email'), ['class' => 'control-label']) }}
                <div class="form-control" >{{$student->email}} </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('password', trans('admin.password'), ['class' => 'control-label']) }}
                <div class="form-control" >{{null}} </div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('remember_token', trans('admin.remember_token'), ['class' => 'control-label']) }}
                <div class="form-control" >{{null}}</div>

            </div>

        </div>
    </div>
    <div class="row">

        <div class="col-md-4">
            {{ Form::label('image', trans('admin.image'), ['class' => 'control-label']) }}


            @if($student->image != null)
                <img src="{{asset('storage/'.$student->image)}}" style="width: 150px" />
            @else
                <img src="{{url('/')}}adminlte/preview_Image.png" style="width: 150px">
            @endif
        </div>
    </div>
</div>
