@push('js')
        <script>
            $(function () {
                'use strict'

                $('#add_dataz').on('click',function () {
                    var exams = $('.exams option:selected').val();
                    var student = '{{$student->id}}';
                    var grade = $('.grade').val();

                        $.ajax({
                            url: '{{aurl('sub_exams')}}',
                            type:'post',
                            dataType:'html',
                            data:{ "_token": "{{ csrf_token() }}",student: student,exams : exams, grade: grade},
                            success: function (data) {
                                $('.column-exams').html(data);

                            }
                        });
                });

            });
        </script>
        <script>
            {{--$(document).ready(function(){--}}
            {{--    // For A Delete Record Popup--}}
            {{--    $('.remove-record-exam').click(function() {--}}
            {{--        var id = $(this).attr('data-id');--}}
            {{--        var url = $(this).attr('data-url');--}}
            {{--        var token = '{{csrf_token()}}';--}}
            {{--        console.log(url,id);--}}
            {{--        $(".remove-record-model-exam").attr("action",url);--}}
            {{--        $('body').find('.remove-record-model-exam').append('<input name="_token" type="hidden" value="'+ token +'">');--}}
            {{--        $('body').find('.remove-record-model-exam').append('<input name="_method" type="hidden" value="DELETE">');--}}
            {{--        $('body').find('.remove-record-model-exam').append('<input name="id" type="hidden" value="'+ id +'">');--}}
            {{--    });--}}

            {{--    // $('.remove-data-from-delete-form').click(function() {--}}
            {{--    //     $('body').find('.remove-record-model-exam').find( "input" ).remove();--}}
            {{--    // });--}}
            {{--    // $('.modal').click(function() {--}}
            {{--    //     $('body').find('.remove-record-model-exam').find( "input" ).remove();--}}
            {{--    // });--}}
            {{--});--}}
        </script>
@endpush




<div class="tab-pane fade in" id="menu9">

    <div class="form-group">

        <br>
        <br>
        <div class="table-responsive column-exams">
            <table class="table table-bordered table-hover table-striped text-center">
                <tr>
                    <th>{{trans('admin.exams_name')}}</th>

                    <th>{{trans('admin.grade')}}</th>

                    <th>{{trans('admin.delete')}}</th>
                </tr>

                @foreach($student->exams as $exam)

                <tr>

                    <td>{{$exam->name_ar}}</td>
                    <td>{{$exam->pivot->grade}} </td>



                    <td>

                        <a href="{{ route('remove_subexam.destroy', [$student->id, $exam->id]) }}" class="btn btn-danger waves-effect waves-light" >delete</a>


                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>


