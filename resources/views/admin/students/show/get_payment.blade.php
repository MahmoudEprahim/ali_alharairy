
  

@foreach(\App\Enums\MonthName::toSelectArray() as $key => $value)

@foreach($payments as $payment)

        <tr>
            <td >
            <div class="form-group">
                {{ Form::label(trans('admin.payment_value'), \App\Enums\MonthName::getDescription($payment->MonthName), ['class' => 'control-label']) }}
            </div>

            </td>
            <td>
            <div class="form-group">
            <input type="text" data-month-id={{$payment->MonthName}} value="{{$payment->fees}}" class="form-control price">
            </div>
                
            </td>
        </tr>
    @endforeach
 
@endforeach

