@push('js')
    <script>
        $(function () {
            'use strict'

            $(document).on('change','.learn_time,.level',function () {
                var learn_time = $('.learn_time option:selected').val();
                var level = $('.level option:selected').val();
                var student = '{{$student->id}}';
                console.log(learn_time,level,student);
                if (learn_time && level) {
                    $.ajax({
                        url: '{{aurl('phase')}}',
                        type: 'get',
                        dataType: 'html',
                        data: {learn_time: learn_time, level: level, student: student},
                        success: function (data) {
                            $('.column-phase').html(data);
                        }
                    });
                }else{
                    $('.column-phase').html('');
                }
            });

        });
    </script>

@endpush
<div class="tab-pane fade in" id="category">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('learn_time',trans('admin.learn_time'), ['class' => 'control-label']) }}

                <div class="form-control">{{\App\Enums\learntimeType::getDescription($student->learn_time)}}</div>            </div>
        </div>
        <div class="col-md-6">


            <div class="form-group">
                {{ Form::label('grades_id',trans('admin.student_grade'), ['class' => 'control-label']) }}
                <div class="form-control">{{$student->grade->name_ar}}</div>
            </div>
        </div>
    </div>
    <div class="column-phase">

    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('appointment_id',trans('admin.appointment_id'), ['class' => 'control-label']) }}
                <div class="form-control">{{$student->appointments->appointment}}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('Joining_date',trans('admin.Joining_date'), ['class' => 'control-label']) }}
                <div class="form-control">{{$student->Joining_date}}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('past_school',trans('admin.past_school'), ['class' => 'control-label']) }}
                <div class="form-control">{{$student->past_school}}</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('previous_estimate',trans('admin.past_year_result'), ['class' => 'control-label']) }}

                <div class="form-control">{{$student->previous_estimate}}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('health_status',trans('admin.health_status'), ['class' => 'control-label']) }}
                <div class="form-control">{{$student->health_status}}</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('education_notes',trans('admin.education_notes'), ['class' => 'control-label']) }}
                <div class="form-control">{{$student->education_notes}}</div>
            </div>
        </div>
    </div>
</div>
