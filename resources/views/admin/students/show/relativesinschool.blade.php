@push('js')
        <script>
            $(function () {
                'use strict'

                $('#add_data').on('click',function () {
                    var students = $('.students option:selected').val();
                    var self = '{{$student->id}}';
                    console.log(students,self);
                        $.ajax({
                            url: '{{aurl('sub_student')}}',
                            type:'post',
                            dataType:'html',
                            data:{ "_token": "{{ csrf_token() }}",self: self,students : students},
                            success: function (data) {
                                $('.column-students').html(data);
                                console.log(data);
                            }
                        });
                });

            });
        </script>
        <script>
            $(document).ready(function(){
                $("#submitBtn").click(function(){
                    $("#myForm").submit(); // Submit the form
                });
            });
        </script>
{{--        <script>--}}
{{--            $(document).ready(function(){--}}
{{--                // For A Delete Record Popup--}}
{{--                $('.remove-record').click(function() {--}}
{{--                    var id = $(this).attr('data-id');--}}

{{--                    var url = $(this).attr('data-url');--}}

{{--                    var token = '{{csrf_token()}}';--}}
{{--                    $(".remove-record-model-relative").attr("action",url);--}}

{{--                    $('body').find('.remove-record-model-relative').append('<input name="_token" type="hidden" value="'+ token +'">');--}}
{{--                    $('body').find('.remove-record-model-relative').append('<input name="_method" type="hidden" value="DELETE">');--}}
{{--                    $('body').find('.remove-record-model-relative').append('<input name="id" type="hidden" value="'+ id +'">');--}}
{{--                });--}}

{{--                $('.remove-data-from-delete-form').click(function() {--}}
{{--                    $('body').find('.remove-record-model-relative').find( "input" ).remove();--}}
{{--                });--}}
{{--                $('.modal').click(function() {--}}
{{--                    $('body').find('.remove-record-model-relative').find( "input" ).remove();--}}
{{--                });--}}
{{--            });--}}
{{--        </script>--}}
@endpush



<div class="tab-pane fade in" id="menu3">

    <div class="form-group">

        <div class="table-responsive column-students">
            <table class="table table-bordered table-hover table-striped text-center">
                <tr>
                    <th>{{trans('admin.name')}}</th>
                    <th>{{trans('admin.phase')}}</th>

                    <th>{{trans('admin.delete')}}</th>
                </tr>

                @foreach($student->other_students as $parent)
                <tr>

                    <td>{{session_lang($parent->name_en,$parent->name_ar) ? session_lang($parent->name_en,$parent->name_ar) : null}}</td>
                    <td>{{$parent->level ? \App\Enums\EduLevelType::getDescription($parent->level) : null}}</td>
                    <td>
                    <td>
                        <a href="{{ route('remove_subparents.destroy', [$parent->id, $student->id]) }}" class="btn btn-danger waves-effect waves-light" >delete</a>






                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>



