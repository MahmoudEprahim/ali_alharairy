

<div class="tab-pane fade in" id="menu4">


    <div class="form-group row">
        <div class="col-md-6">
            {{ Form::label(trans('admin.first_date_debtor'), null, ['class' => 'control-label']) }}
            {{ Form::text('debtor', 0, array_merge(['class' => 'form-control','required'=>'required'])) }}
        </div>
        <div class="col-md-6">
            {{ Form::label(trans('admin.first_date_creditor'), null, ['class' => 'control-label']) }}
            {{ Form::text('creditor', 0, array_merge(['class' => 'form-control','required'=>'required'])) }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('receivables_Present_id', trans('admin.receivables_Present'), ['class' => 'control-label']) }}
                {{ Form::select('receivables_Present_id', $departments,null, array_merge(['class' => 'form-control owner','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('receivables_past_id', trans('admin.receivables_past'), ['class' => 'control-label']) }}
                {{ Form::select('receivables_past_id', $departments,null, array_merge(['class' => 'form-control owner','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('receivables_future_id', trans('admin.receivables_future'), ['class' => 'control-label']) }}
                {{ Form::select('receivables_future_id', $departments,null, array_merge(['class' => 'form-control owner','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('activity_revenues_id', trans('admin.activity_revenues'), ['class' => 'control-label']) }}
                {{ Form::select('activity_revenues_id', $departments,null, array_merge(['class' => 'form-control owner','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
    </div>
    <div class="hidden">
        {{$balance = 0}}
        {{$dataDebtor = 0}}
        {{$dataCredit = 0}}
        {{$balance1 = 0}}
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <th>الشهر</th>
                <th>الحركه مدين</th>
                <th>الحركه دائن</th>
                <th>رصيد حالي</th>
                <th>{{trans('admin.receivables_past')}}</th>
            </tr>
            @for($i = 1;$i < 13;$i++)
                <tr>
                    <td>{{\App\Enums\dataLinks\MonthType::getDescription($i)}}</td>
                    <td>{{getpjitmmsfl_student($student->receivables_Present_id,$i,\Carbon\Carbon::today()->format('Y'))['debtor']}}
                        <div class="hidden">{{$dataDebtor += getpjitmmsfl_student($student->receivables_Present_id,$i,\Carbon\Carbon::today()->format('Y'))['debtor']}}</div>
                    </td>
                    <td>{{getpjitmmsfl_student($student->receivables_Present_id,$i,\Carbon\Carbon::today()->format('Y'))['creditor']}}
                        <div class="hidden">{{$dataCredit += getpjitmmsfl_student($student->receivables_Present_id,$i,\Carbon\Carbon::today()->format('Y'))['creditor']}}</div>
                    </td>
                    <td>{{getpjitmmsfl_student($student->receivables_Present_id,$i,\Carbon\Carbon::today()->format('Y'))['current_balance']}}
                        <div class="hidden">{{$balance += getpjitmmsfl_student($student->receivables_Present_id,$i,\Carbon\Carbon::today()->format('Y'))['current_balance']}}</div>
                    </td>
                    <td>{{getpjitmmsfl_student($student->receivables_Present_id,$i,\Carbon\Carbon::today()->format('Y')-1)['current_balance']}}
                        <div class="hidden">{{$balance1 += getpjitmmsfl_student($student->receivables_Present_id,$i,\Carbon\Carbon::today()->format('Y')-1)['current_balance']}}</div>
                    </td>
                </tr>
            @endfor
            <tr>
                <td>الاجمالي</td>
                <td>{{$dataDebtor}}</td>
                <td>{{$dataCredit}}</td>
                <td>{{$balance}}</td>
                <td>{{$balance1}}</td>
            </tr>
        </table>
    </div>

</div>
