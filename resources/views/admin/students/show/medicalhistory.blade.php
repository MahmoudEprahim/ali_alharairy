
<div class="tab-pane fade in" id="menu5">

    <div class="row">
        <div class="col-md-4">
            {{ Form::label('special_needs', trans('admin.special_needs'), ['class' => 'control-label']) }}
            <br>

            @foreach(\App\Enums\SpecialNeedsType::toArray() as $index => $special_needs)
                {{ Form::label('special_needs',trans('admin.'.\App\Enums\SpecialNeedsType::getKey($special_needs)), ['class' => 'control-label']) }}
                <input type="radio" name="special_needs" id="{!! $special_needs !!}" value="{{$special_needs}}" @if ($student->special_needs == $special_needs) checked @endif>

            @endforeach

            {{--            {{ Form::select('gender', \App\Enums\GenderType::toSelectArray(),$applicant->gender, array_merge(['class' => 'form-control','placeholder'=>'select ...'])) }}--}}
        </div>
        <div class="col-md-4">
            {{ Form::label('medical_condition', trans('admin.medical_condition'), ['class' => 'control-label']) }}
            <br>

            @foreach(\App\Enums\MedicalConditionType::toArray() as $index => $medical_condition)
                {{ Form::label('medical_condition',trans('admin.'.\App\Enums\MedicalConditionType::getKey($medical_condition)), ['class' => 'control-label']) }}
                <input type="radio" name="medical_condition" id="{!! $medical_condition !!}" value="{{$medical_condition}}" @if ($student->medical_condition == $medical_condition) checked @endif>

            @endforeach

            {{--            {{ Form::select('gender', \App\Enums\GenderType::toSelectArray(),$applicant->gender, array_merge(['class' => 'form-control','placeholder'=>'select ...'])) }}--}}
        </div>
        <div class="col-md-4">
            {{ Form::label('medical_attention', trans('admin.medical_attention'), ['class' => 'control-label']) }}
            <br>

            @foreach(\App\Enums\MedicalAttentionType::toArray() as $index => $medical_attention)
                {{ Form::label('medical_attention',trans('admin.'.\App\Enums\MedicalAttentionType::getKey($medical_attention)), ['class' => 'control-label']) }}
                <input type="radio" name="medical_attention" id="{!! $medical_attention !!}" value="{{$medical_attention}}" @if ($student->medical_attention == $medical_attention) checked @endif>

            @endforeach

            {{--            {{ Form::select('gender', \App\Enums\GenderType::toSelectArray(),$applicant->gender, array_merge(['class' => 'form-control','placeholder'=>'select ...'])) }}--}}
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('medical_desc',trans('admin.medical_desc'), ['class' => 'control-label']) }}
                <div class="form-control">{{$student->medical_desc}}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('medical_notes',trans('admin.medical_notes'), ['class' => 'control-label']) }}
                <div class="form-control">{{$student->medical_notes}}</div>            </div>
        </div>
    </div>
</div>
