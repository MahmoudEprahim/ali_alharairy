@extends('admin.index')
@section('title',trans('admin.edit_student'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($student,['method'=>'PUT','route' => ['students.update',$student->id]]) !!}
            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label('Brn_No', trans('admin.Brn_No'), ['class' => 'control-label']) }}
                    {{ Form::select('Brn_No',$branche ,$student->Brn_No, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>

                <div class="col-md-4">
                    {{Form::label(trans('admin.Cstm_NmAr'),null,['class'=>'control-label'])}}
                    {{Form::text('Cstm_NmAr', $student->Cstm_NmAr, array_merge(['class'=>'form-control','placeholder'=>trans('admin.Cstm_NmAr')]))}}
                </div>

                <div class="col-md-4">
                    {{Form::label(trans('admin.Cstm_NmEn'),null,['class'=>'control-label'])}}
                    {{Form::text('Cstm_NmEn', $student->Cstm_NmEn, array_merge(['class'=>'form-control','placeholder'=>trans('admin.Cstm_NmAr')]))}}
                </div>
            </div>


            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label(trans('admin.Tr_Dt'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Tr_Dt', $student->Tr_Dt, array_merge(['class' => 'form-control datepicker','placeholder'=>trans('admin.Tr_Dt'),'autocomplete'=>'off'])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.addriss'), null, ['class' => 'control-label']) }}
                    {{ Form::text('addriss', $student->addriss, array_merge(['class' => 'form-control','placeholder'=>trans('admin.addriss')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.Fees'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Fees', $student->Fees, array_merge(['class' => 'form-control','placeholder'=>trans('admin.Fees')])) }}
                </div>
            </div>



            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label('Parnt_Name', trans('admin.Parnt_Name'), ['class' => 'control-label']) }}
                    {{ Form::text('Parnt_Name', $student->Parnt_Name, array_merge(['class' => 'form-control','placeholder'=>trans('admin.Parnt_Name')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label('Parnt_Job', trans('admin.Parnt_Job'), ['class' => 'control-label']) }}
                    {{ Form::text('Parnt_Job', $student->Parnt_Job, array_merge(['class' => 'form-control','placeholder'=>trans('admin.Parnt_Job')])) }}
                </div>

            <!-- <div class="col-md-4">
                {{ Form::label('image', trans('admin.upload_image') . ':', ['class' => 'control-label']) }}
            {{ Form::file('image') }}
                </div> -->


            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label('Sex_Ty', trans('admin.Sex_Ty'), ['class' => 'control-label']) }}
                    {{ Form::select('Sex_Ty',\App\Enums\GenderType::toSelectArray() ,$student->Sex_Ty, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>
            </div>
            <br>
            {{Form::submit(trans('admin.update'),['class'=>'btn btn-primary'])}}
            {!! Form::close() !!}
        </div>
    </div>








@endsection
