@extends('admin.index')
@section('title',trans('admin.show_exam'))
@section('root_link', route('exams.index'))
@section('root_name', trans('admin.exams'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.show_exam')}}</h3>
        </div>
        <div class="box-body">


            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label('branche_id', trans('admin.branche_id'), ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($exam->Branche->name_en,$exam->Branche->name_ar)}}</div>
                </div>
                <div class="col-md-6">
                    {{ Form::label('grade_id', trans('admin.ar_grade_id'), ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($exam->grade->name_en,$exam->grade->name_ar)}}</div>

                </div>
            </div>

            <div class="form-group row">

                <div class="col-md-6">
                    {{ Form::label('name_ar', trans('admin.name_ar'), ['class' => 'control-label']) }}
                    <div class="form-control">{{$exam->name_ar}}</div>
                </div>

                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$exam->name_en}}</div>
                </div>


            </div>
            <div class="form-group row">
                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    <img class="gallery_img profile-img" alt="img" src="{{asset('storage/'.$exam->image)}}" style="width: 20%;">

                </div>
            </div>

            <a href="{{aurl('exams')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>



@endsection
