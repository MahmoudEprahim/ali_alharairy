@push('js')
        <script>
            $(document).ready(function () {
                $('#add_dataz').on('click',function () {
                    var exams = $('.exams option:selected').val();
                    var student = '{{$student->id}}';
                    var grade = $('.grade').val();

                    $.ajax({
                        url: '{{aurl('sub_exams')}}',
                        type:'post',
                        dataType:'html',
                        data:{ "_token": "{{ csrf_token() }}",student: student,exams : exams, grade: grade},
                        success: function (data) {
                            $('.column-exams').html(data);

                        }
                    });
                });
            })
        </script>
@endpush
<div class="tab-pane fade in exams-data" id="menu9">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('exams', trans('admin.exams'), ['class' => 'control-label']) }}
            <select name="exams" id="exams" class="exams">
                <option value="null">select exams</option>
                @foreach($exams as $exam)
                    <option value="{{$exam->id}}">{{$exam->{'name_'.session('lang')} }}</option>
                @endforeach
            </select>
        </div>

    </div>
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label(trans('admin.examgrade'), null, ['class' => 'control-label']) }}
            {{ Form::text('grade', '', array_merge(['class' => 'form-control grade'])) }}
        </div>
    </div>
    <button type="button" id="add_dataz" class="btn btn-primary">{{trans('admin.add')}}</button>
    <br><br>
    <div class="table-responsive column-exams">
            <table class="table table-bordered table-hover table-striped text-center">
                <tr>
                    <th>{{trans('admin.exams_name')}}</th>
                    <th>{{trans('admin.grade')}}</th>
                    <th>{{trans('admin.delete')}}</th>
                </tr>
                @foreach($student->exams as $exam)
                <tr>
                    <td>{{$exam->name_ar}}</td>
                    <td>{{$exam->pivot->grade}} </td>
                <td>
                    <a href="{{ route('remove_subexam.destroy', [$student->id, $exam->id]) }}" class="btn btn-danger waves-effect waves-light" >delete</a>
                </tr>
                @endforeach
            </table>
        </div>
</div>


