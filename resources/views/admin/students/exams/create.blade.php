@extends('admin.index')
@section('title',trans('admin.Create_new_exam'))
@section('root_link', route('exams.index'))
@section('root_name', trans('admin.exams'))
@section('content')


@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">


        {!! Form::open(['method'=>'POST','route' => 'exams.store','files'=>true]) !!}
        <div class="form-group row">

        <div class="col-md-6 required">
            {{ Form::label('branche_id', trans('admin.branches_id'), ['class' => 'control-label']) }}
            {{ Form::select('branche_id',$branche ,null, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=> trans('admin.select')])) }}
        </div>
        <div class="col-md-6 required">
            {{ Form::label('grade_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
            {{ Form::select('grade_id',$grade ,null, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=> trans('admin.select')])) }}
        </div>

        </div>



        <!--exams data -->
            <div class="form-group row">
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_ar', null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_en', null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>


            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image') }}
                </div>
                </div>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('exams')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>

    </div>

@endsection
