@extends('admin.index')
@section('title',trans('admin.edit_exam'))
@section('root_link', route('exams.index'))
@section('root_name', trans('admin.exams'))
@section('content')

        <div class="box">
            @include('admin.layouts.message')
            <div class="box-header">
                <h3 class="box-title">{{$title}}</h3>
            </div>
            <div class="box-body">
                {!! Form::model($exam,['method'=>'PUT','route' => ['exams.update',$exam->id],'files'=>true]) !!}

                <div class="form-group row">
                    <div class="col-md-6 required">
                        {{ Form::label('branche_id', trans('admin.branche_id'), ['class' => 'control-label']) }}
                        {{ Form::select('branche_id',$branche ,$exam->branche_id, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.select')])) }}
                    </div>
                    <div class="col-md-6 required">
                        {{ Form::label('grade_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                        {{ Form::select('grade_id',$grade ,$exam->grade_id, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.select')])) }}
                    </div>
                </div>



                <div class="form-group row">
                    <div class="col-md-6 required">
                        {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                        {{ Form::text('name_ar', old('name_ar'), array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                    </div>
                    <div class="col-md-6 required">
                        {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                        {{ Form::text('name_en', old('name_en'), array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                    </div>



                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image') }}
                     </div>
                    @if($exam->image != null)
                    <div class="col-md-4">
                     <img class="gallery_img profile-img" alt="img" src="{{asset('storage/'.$exam->image)}}" style="width: 50%">
                    </div>
                        @endif
                </div>
                <br>
                <div class="clearfix"></div>
                    {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
                    <a href="{{aurl('exams')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
                    {!! Form::close() !!}
            </div>
    </div>







@endsection
