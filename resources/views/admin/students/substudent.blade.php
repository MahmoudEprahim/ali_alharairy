<script>
    function invoicesFormSubmit()
    {
        document.getElementById("invoices_form").submit();
    }


</script>
<table class="table table-bordered table-hover table-striped text-center">
    <tr>
        <th>{{trans('admin.name')}}</th>
        <th>{{trans('admin.phase')}}</th>
{{--        <th>{{trans('admin.Balance')}}</th>--}}
        <th>{{trans('admin.delete')}}</th>
    </tr>

    @foreach($self->other_students as $parent)
        <tr>
            <td> {{session_lang($parent->name_en,$parent->name_ar)}}</td>
            <td>{{$parent->level ? \App\Enums\EduLevelType::getDescription($parent->level) : null}}</td>
{{--            <td>{{trans('admin.Balance')}}</td>--}}
{{--            <td>{{session_lang($parent->classes->name_en,$parent->classes->name_ar) ? session_lang($parent->classes->name_en,$parent->classes->name_ar) : null}}</td>--}}

            <td>
                <a href="{{ route('remove_subparents.destroy', [$parent->id, $self->id]) }}" class="btn btn-danger waves-effect waves-light" >delete</a>

            </td>
        </tr>
    @endforeach
</table>
