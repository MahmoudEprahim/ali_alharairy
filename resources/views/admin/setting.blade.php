@extends('admin.index')
@section('title',trans('admin.dashboard_setting'))
@section('content')
    @push('css')

        <link href="{{url('/')}}./setting_icon/css/icon-picker.min.css" media="all" rel="stylesheet" type="text/css" />


{{--        <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>--}}
        <script src="{{url('/')}}./setting_icon/js/iconPicker.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $(".icon-picker").iconPicker();
            });
        </script>

        <style>
            .icon-popup
            {
                top: 919.264px !important;
                right: 279px !important;
                left: 0px !important;
                display: block !important;
                width: 18%;

            }

        </style>
<style>
    .move_menu
    {

    }
</style>
        @endpush


    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.dashboard_setting')}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="row" style="margin-bottom: 15px;">
            <div class="col-md-12">
                <a href="{{ route('slider.index') }}" class="btn btn-primary">{{trans('admin.Edit_Slider')}}</a>
                <a href="{{ url('admin/profile') }}" class="btn btn-primary">{{trans('admin.lecturer_information')}}</a>

                <a href="{{url('/admin/setting/features')}}" class="btn btn-primary" >{{trans('admin.features')}} </a>

                <a href="{{url('/admin/honorboard')}}" class="btn btn-primary" >{{trans('admin.honorboard_information')}} </a>

                {{--                <a href="{{url('/admin/setting/units')}}" class="btn  btn-primary">{{trans('admin.units')}} </a>--}}
                {{--                <a href="{{url('/admin/setting/wordlist')}}" class="btn  btn-primary">{{trans('admin.wordlist')}} </a>--}}
                {{--                <a href="{{url('/admin/setting/grammar')}}" class="btn  btn-primary">{{trans('admin.grammar_list')}} </a>--}}
                {{--                <a href="{{url('/admin/setting/content')}}" class="btn  btn-primary">{{trans('admin.content')}} </a>--}}

                <!--<a href="{{url('/admin/setting/homevideos')}}" class="btn btn-primary" >{{trans('admin.homevideos')}} </a>-->
                <a href="{{url('/admin/setting/team')}}" class="btn btn-primary" >{{trans('admin.team')}} </a>
                <a href="{{url('/admin/sayabout')}}" class="btn btn-primary" >{{trans('admin.sayabout')}} </a>


            </div>
        </div>
        <div class="box-body">
            @include('admin.layouts.message')
            {!! Form::open(['route'=>'setting.save','files' => true]) !!}


            <div class="row form-group">
                <!--<div class="col-sm-6">-->
                <!--    {{ Form::label(trans('admin.arabic_name'), null, ['class' => 'control-label']) }}-->
                <!--    {{ Form::text('sitename_ar', setting()->sitename_ar, array_merge(['class' => 'form-control'])) }}-->
                <!--</div>-->
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.english_name'), null, ['class' => 'control-label']) }}
                    {{ Form::text('sitename_en', setting()->sitename_en, array_merge(['class' => 'form-control'])) }}
                </div>
            
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.footer_logo'), null, ['class' => 'control-label']) }}
                    {{ Form::file('logo_ar', array_merge(['class' => 'form-control'])) }}
                    @if(!empty(setting()->logo_ar))
                        <img src="{{asset('storage/'.setting()->logo_ar)}}" style="width: 50px; margin-top: 20px" class="img-responsive" >
                    @endif
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.brand'), null, ['class' => 'control-label']) }}
                    {{ Form::file('logo_en', array_merge(['class' => 'form-control'])) }}
                    @if(!empty(setting()->logo_en))
                        <img src="{{asset('storage/'.setting()->logo_en)}}" style="width: 50px; margin-top: 20px" class="img-responsive" >
                    @endif
                </div>
            </div>


            

            <!-- <div class="form-group">
                {{ Form::label(trans('admin.main_lang'), null, ['class' => 'control-label']) }}
                {{ Form::select('main_lang',['ar'=>trans('admin.ar'),'en'=>trans('admin.en')] , setting()->main_lang ,array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
            </div> -->




            <!-- <div class="row form-group">
                <div class="col-sm-6">
                    {{ Form::label(trans('admin.title_about_center_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_about_center_ar', setting()->title_about_center_ar, array_merge(['class' => 'form-control'])) }}
                </div>

                <div class="col-sm-6">
                    {{ Form::label(trans('admin.title_about_center_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_about_center_en', setting()->title_about_center_en, array_merge(['class' => 'form-control'])) }}
                </div>
            </div> -->
            
                <!--<div class="col-sm-6">-->
                <!--    {{ Form::label(trans('admin.about_center_ar'), null, ['class' => 'control-label']) }}-->
                <!--    {{ Form::textarea('about_center_ar', setting()->about_center_ar, array_merge(['class' => 'form-control'])) }}-->
                <!--</div>-->

                <div class="col-sm-12">
                    {{ Form::label(trans('admin.about_center_en'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('about_center_en', setting()->about_center_en, array_merge(['class' => 'form-control'])) }}
                </div>
            




            <!-- <div class="row form-group">
                <div class="col-sm-6">
                    {{ Form::label(trans('admin.features_title_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('features_title_ar', setting()->features_title_ar, array_merge(['class' => 'form-control'])) }}
                </div>

                <div class="col-sm-6">
                    {{ Form::label(trans('admin.features_title_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('features_title_en', setting()->features_title_en, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.icon_1'), null, ['class' => 'control-label']) }}
                    {{ Form::text('icon_1', setting()->icon_1, array_merge(['class' => 'icon-picker'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.icon_2'), null, ['class' => 'control-label']) }}
                    {{ Form::text('icon_2', setting()->icon_2, array_merge(['class' => 'icon-picker'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.icon_3'), null, ['class' => 'control-label']) }}
                    {{ Form::text('icon_3', setting()->icon_3, array_merge(['class' => 'icon-picker'])) }}


                </div>
            </div> -->



            <div class="row form-group">
                <div class="col-sm-4">
                    {{ Form::label(trans('admin.email'), null, ['class' => 'control-label']) }}
                    {{ Form::email('email', setting()->email, array_merge(['class' => 'form-control'])) }}
                </div>
                <!--<div class="col-sm-4">-->
                <!--    {{ Form::label(trans('admin.address_1'), null, ['class' => 'control-label']) }}-->
                <!--    {{ Form::text('addriss', setting()->addriss, array_merge(['class' => 'form-control'])) }}-->
                <!--</div>-->
                <!--<div class="col-sm-4">-->
                <!--    {{ Form::label(trans('admin.address_2'), null, ['class' => 'control-label']) }}-->
                <!--    {{ Form::text('linkedin', setting()->linkedin, array_merge(['class' => 'form-control'])) }}-->
                <!--</div>-->
            </div>
            
             <!--<div class="row form-group">-->
             <!--    <div class="col-sm-6">-->
             <!--       {{ Form::label(trans('admin.address_1_location'), null, ['class' => 'control-label']) }}-->
             <!--       {{ Form::textarea('contact_us_title_ar', setting()->contact_us_title_ar, array_merge(['class' => 'form-control'])) }}-->
             <!--   </div>-->
             <!--   <div class="col-sm-6">-->
             <!--       {{ Form::label(trans('admin.address_2_location'), null, ['class' => 'control-label']) }}-->
             <!--       {{ Form::textarea('contact_us_title_en', setting()->contact_us_title_en, array_merge(['class' => 'form-control'])) }}-->
             <!--   </div>-->
             <!--</div>-->
            
            <div class="row form-group">
                <div class="col-md-3">
                    {{ Form::label(trans('admin.phone'), null, ['class' => 'control-label']) }}
                    {{ Form::text('phone', setting()->phone, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.facebook'), null, ['class' => 'control-label']) }}
                    {{ Form::text('facebook', setting()->facebook, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.inistgram'), null, ['class' => 'control-label']) }}
                    {{ Form::text('twitter', setting()->twitter, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.youtube'), null, ['class' => 'control-label']) }}
                    {{ Form::text('youtube', setting()->youtube, array_merge(['class' => 'form-control'])) }}
                </div>
                <!-- <div class="col-md-3">
                    {{ Form::label(trans('admin.phone_2'), null, ['class' => 'control-label']) }}
                    {{ Form::text('phone_2', setting()->phone_2, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.phone_3'), null, ['class' => 'control-label']) }}
                    {{ Form::text('phone_3', setting()->phone_3, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.phone_4'), null, ['class' => 'control-label']) }}
                    {{ Form::text('phone_4', setting()->phone_4, array_merge(['class' => 'form-control'])) }}
                </div> -->
            </div>


            <!-- <div class="form-group">
                {{ Form::label(trans('admin.work_from'), null, ['class' => 'control-label']) }}
                {{ Form::time('work_from', setting()->work_from, array_merge(['class' => 'form-control'])) }}
            </div>
            <div>
                {{ Form::label(trans('admin.work_to'), null, ['class' => 'control-label']) }}
                {{ Form::time('work_to', setting()->work_to, array_merge(['class' => 'form-control'])) }}
            </div> -->



             <div class="form-group">
                {{ Form::label(trans('admin.keyword'), null, ['class' => 'control-label']) }}
                {{ Form::textarea('keyword', setting()->keyword, array_merge(['class' => 'form-control'])) }}
            </div>
            <!--
            <div class="form-group">
                {{ Form::label(trans('admin.status'), null, ['class' => 'control-label']) }}
                {{ Form::select('status',['open'=>trans('admin.open_status'),'close'=>trans('admin.close_status')] , setting()->status ,array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
            </div> -->


        {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
        <a href="{{aurl('dashboard')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        {!! Form::close() !!}

        </div>
    </div>






@endsection
