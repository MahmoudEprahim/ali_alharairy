@extends('admin.index')
@section('title',trans('admin.edit_blog'))
@section('content')
    @push('js')
        <link rel="stylesheet" href="{{url('/')}}/css/bootstrap-datetimepicker.min.css">
        <script src="{{url('/')}}/js/bootstrap-datetimepicker.min.js"></script>
        <script>
            $(function () {
                $('#datetimepicker1').datetimepicker({
                });
            });
        </script>
    @endpush

 )
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($breaknew,['method'=>'PUT','route' => ['breaknews.update',$breaknew->id]]) !!}
            <div class="row">
                <div class="col-md-12">

                        <div class="col-md-6">
                            {{ Form::label('title', trans('admin.title_blog'), ['class' => 'control-label']) }}
                            {{ Form::text('title', $breaknew->title, array_merge(['class' => 'form-control title'])) }}
                        </div>
                    <div class="col-md-6">
                            {{ Form::label('publish_in', trans('admin.publish_in'), ['class' => 'control-label']) }}
                            {{ Form::text('publish_in',date('d/m/Y h:i A', strtotime($breaknew->created_at)), array_merge(['class' => 'form-control','id'=>'datetimepicker1'])) }}
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::label(trans('admin.body'), null, ['class' => 'control-label']) }}
                                {{ Form::textarea('description', $breaknew->description, array_merge(['class' => 'form-control','id' => 'body'])) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    {!! Form::submit(trans('admin.save'),['class' => 'btn btn-success btn-block']) !!}
                </div>
                    <div class="col-md-3">


                    <a href="{{aurl('blog')}}" class="btn btn-danger btn-block" >{{trans('admin.back')}}</a>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>








@endsection
