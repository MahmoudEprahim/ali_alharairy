@extends('admin.index')
@section('title',trans('admin.secondary_gates'))
@section('root_link', route('units.index'))
@section('root_name', trans('admin.units'))
@section('content')
    @hasanyrole('writer|admin')
        <div class="box">
            @include('admin.layouts.message')
            <div class="box-header">
                <h3 class="box-title">{{$title}}</h3>
            </div>
            <div class="box-body">
                {!! Form::model($unit,['method'=>'PUT','route' => ['units.update',$unit->id],'files'=>true]) !!}

                <div class="form-group row">
                    
                    <div class="col-md-4"> 
                    {{ Form::label('grade_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                    {{ Form::select('grade_id',$grade ,$unit->grade_id, array_merge(['class' => 'form-control', 'placeholder'=>trans('admin.grade_id')])) }}
                </div>
                    <div class="col-md-4 hidden">
                        {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                        {{ Form::text('name_ar', old('name_ar'), array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                        {{ Form::text('name_en', old('name_en'), array_merge(['class' => 'form-control'])) }}
                    </div>
                </div>

                <br>
                <div class="clearfix"></div>
                    {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
                    <a href="{{aurl('setting/units')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
                    {!! Form::close() !!}
            </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection

