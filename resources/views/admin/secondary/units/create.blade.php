@extends('admin.index')
@section('title',trans('admin.secondary_gates'))
@section('root_link', route('units.index'))
@section('root_name', trans('admin.units'))
@section('content')


@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">


        {!! Form::open(['method'=>'POST','route' => 'units.store','files'=>true]) !!}

        <div class="form-group row">
            
            <div class="col-md-4"> 
                {{ Form::label('grade_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                {{ Form::select('grade_id',$grade ,null, array_merge(['class' => 'form-control', 'placeholder'=> trans('admin.grade_id')])) }}
            </div>
        
            <div class="col-md-4 hidden">
                {{ Form::label(trans('admin.unit_name_ar'), null, ['class' => 'control-label']) }}
                {{ Form::text('name_ar', null, array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="col-md-4">
                {{ Form::label(trans('admin.unit_name_en'), null, ['class' => 'control-label']) }}
                {{ Form::text('name_en', null, array_merge(['class' => 'form-control'])) }}
            </div>

        </div>

            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('setting/units')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>

    </div>
</div>

@endsection