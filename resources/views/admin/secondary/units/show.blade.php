@extends('admin.index')
@section('title',trans('admin.secondary_gates'))
@section('root_link', route('units.index'))
@section('root_name', trans('admin.units'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.show_unit')}} <span style="color:#f00">{{session_lang($unit->name_en,$unit->name_ar)}}</span> </h3>
        </div>
        <div class="box-body">
            {!! Form::model($unit,['method'=>'PUT','route' => ['units.update',$unit->id],'files'=>true]) !!}

            <div class="form-group row">
                <div class="col-md-4 ">
                    {{ Form::label('grade_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                    <div class="form-control">{{session_lang($unit->grade->name_en,$unit->grade->name_ar)}}</div>
                </div>


                <div class="col-md-4 hidden">
                    {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$unit->name_ar}}</div>
                </div>
                <div class="col-md-4 ">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$unit->name_en}}</div>

                </div>




            </div>

            <br>
            <div class="clearfix"></div>

            <a href="{{aurl('setting/units')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>








@endsection
