

<div class="form-group">
    <div class="col-md-4">
        {{ Form::label('units_id', trans('admin.units_id'), ['class' => 'control-label']) }}
        {{ Form::select('units_id',$unit ,null, array_merge(['class' => 'form-control units_id', 'placeholder'=>trans('admin.select')])) }}
    </div>
</div>
<!--<div class="form-group">-->
<!--    <div class="col-md-4">-->
<!--        {{ Form::label(trans('admin.Title'), null, ['class' => 'control-label']) }}-->
<!--        {{ Form::text('title', old('title'), array_merge(['class' => 'form-control'])) }}-->
<!--    </div>-->
<!--</div>-->
<div class="col-lg-4 col-md-4 col-sm-12">
    <div class="form-group">
        {{ Form::label('type_list', trans('admin.type_list'), ['class' => 'control-label']) }}
        <select name="type_list" id="type_list" class="type_list form-control">
            <option value="null">select type</option>
            @foreach(\App\Enums\SecondaryTypeList::toSelectArray() as $key=>$type)
                <option value="{{$key}}">{{$type}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-md-6 hidden">
        {{ Form::label(trans('admin.description_ar'), null, ['class' => 'control-label']) }}
        {{ Form::textarea('description_ar', old('description_ar'), array_merge(['class' => 'form-control', 'style' => 'resize: none;'])) }}
    </div>
<!-- <a class=" btn btn-primary clone">{{trans('admin.add_div')}}</a>
                <a class="btn btn-danger remove">{{trans('admin.not_div')}}</a> -->

</div>
<div class="form-group">
    <div class="col-md-12 "> 
        {{ Form::label(trans('admin.description_en'), null, ['class' => 'control-label']) }}
        {{ Form::textarea('description_en', old('description_en'), array_merge(['id'=> 'description_en', 'class' => 'form-control','style' => 'resize: none;'])) }}
    </div>
    
</div>


<div class="form-group appendDiv col-sm-4">
    <label for="media" class="control-label">{{ trans('admin.media') }}</label>
    <div class="form-group" style="display: flex;">
        <input type="text" name="video_src[]" id="media" placeholder="video source" class="form-control">
        <input type="text" name="video_desc[]" id="media" placeholder="video Description" class="form-control">
        <a id="appendSubCat" href="#" class="btn btn-success" style="margin-right: 10px;"><i class="fa fa-plus"></i></i></a>
    </div>           
</div>




<div class="form-group">
    <div class="col-md-12">
        <br>
        {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
        <a href="{{aurl('content')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
    </div>
</div>


<!-- word plugin -->
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"></script>
    <script>
      $(document).ready(function () {

        tinymce.init({
            selector: '#description_en',
            height: 500,
            theme: 'modern',
            plugins: 'print preview powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],

        });
        var temp =tinymce.get('#description_en').getContent();
        console.log(temp);
    })
        
    </script>

<script>
     // add new media
     $(document).ready(function () {
            $('#appendSubCat').click(function (e) {
                
                e.preventDefault();
                let parentDiv = $('.appendDiv');
                parentDiv.append(`
                <div class="form-group" style="display: flex">
                       <input type="text" name="video_src[]" class="form-control" id="media" placeholder="{{trans('admin.media')}}">
                       <input type="text" name="video_desc[]" id="media" placeholder="video Description" class="form-control">
                       <a style="margin-right: 10px" id="deleteLink" onclick="$(this).parent().remove()" class="btn btn-danger" href="#"><i class="fa fa-close"></i></a>
                   </div>
                `);
            });
        })
</script>




    







