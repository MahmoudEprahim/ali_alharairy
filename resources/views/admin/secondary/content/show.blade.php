@extends('admin.index')
@section('title',trans('admin.show_secondary_content'))
@section('root_link', route('content.index'))
@section('root_name', trans('admin.content'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.show_content'). ' >> ' . session_lang($content->title,$content->title)}}</h3>
        </div>
        <div class="box-body">
           

            <div class="form-group row">
                
                <div class="col-md-4">
                    {{ Form::label('grade_id', trans('admin.ar_grade_id'), ['class' => 'control-label']) }}
                    <div class="form-control">{{\App\Enums\SecondaryGate::getDescription($content->grade_id)}}</div>
                </div>
                <div class="col-md-4 ">
                    {{ Form::label('units_id', trans('admin.units_id'), ['class' => 'control-label']) }}

                    <div class="form-control">{{session_lang($content->unit->name_en,$content->unit->name_ar)}}</div>
                </div>


                <div class="col-md-6 hidden">
                    {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->description_ar}}</div>
                </div>
                <div class="col-md-6 ">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$content->description_en}}</div>

                </div>
            </div>

            <br>
            <div class="clearfix"></div>
           
            <a href="{{aurl('setting/content')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>








@endsection
