@extends('admin.index')
@section('title',trans('admin.secondary_gates'))
@section('root_link', route('content.index'))
@section('root_name', trans('admin.secondary_gates'))
@section('content')
   @push('js')
       <script>
           $(function () {
               'use strict';

               $('.type_list').on('change',function () {

                   var branche_id = $('.branche_id option:selected').val();
                   var grade_id = $('.grade_id option:selected').val();
                   var units_id = $('.units_id option:selected').val();
                   var type_list = $('.type_list option:selected').val();

                   $("#loadingmessage").css("display","block");
                   $(".column-form").css("display","none");
                   console.log(type_list,type_list);
                   if (this){
                       $.ajax({
                           url: '{{aurl('content_grammar')}}',
                           type:'get',
                           dataType:'html',
                           data:{branche_id : branche_id,grade_id : grade_id,units_id : units_id,type_list : type_list,},
                           success: function (data) {
                               $("#loadingmessage").css("display","none");
                               $('.column-form').css("display","block").html(data);




                           }
                       });
                   }else{
                       $('.column-form').html('');
                   }
               });


           });
       </script>


<!-- word plugin -->
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"></script>
    <script>
      $(document).ready(function () {

        tinymce.init({
            selector: '#description_en',
            height: 500,
            theme: 'modern',
            plugins: 'print preview powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],

        });
        var temp =tinymce.get('#description_en').getContent();
        console.log(temp);
    })
        

        // add new media
        $(document).ready(function () { 
            $('#appendSubCat').click(function (e) {
                
                e.preventDefault();
                let parentDiv = $('.appendDiv');
                parentDiv.append(`
                <div class="form-group" style="display: flex">
                       <input type="text" name="video_src[]" class="form-control" id="media" placeholder="{{trans('admin.media')}}">
                       <input style="margin-right: 10px;"  type="text" name="video_desc[]" id="media" placeholder="video Description" class="form-control">
                      
                   </div>
                `);
            });
        })

    </script>
    @endpush
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($content,['method'=>'PUT','route' => ['content.update',$content->id],'files'=>true]) !!}

            <div class="form-group row">
                <div class="col-md-3 hidden">
                    {{ Form::label('branche_id', trans('admin.branche_id'), ['class' => 'control-label']) }}
                    {{ Form::select('branches_id',$branche ,$content->branches_id, array_merge(["readonly" => "readonly" , 'class' => 'form-control' , 'placeholder'=>trans('admin.select')])) }}
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    {{ Form::label('grade_id', trans('admin.ar_grade_id'), ['class' => 'control-label']) }}
                    {{ Form::select('grade_id',$grade , $content->grade_id, array_merge(['disabled'=>'true', 'class' => 'form-control grade_id' , 'placeholder'=>trans('admin.select')])) }}
                    <input type="hidden" name="grade_id" value="{{$content->grade_id}}">
                </div>

                
                <div class="col-md-3">
                    {{ Form::label('units_id', trans('admin.units_id'), ['class' => 'control-label']) }}
                    {{ Form::select('units_id',$unit ,$content->units_id, array_merge([ 'disabled'=> 'true', 'class' => 'form-control', 'placeholder'=>trans('admin.select')])) }}
                    <input type="hidden" name="units_id" value="{{$content->units_id}}">
                </div>
                <div class="col-md-3">
                    {{ Form::label('type_list', trans('admin.type_list'), ['class' => 'control-label']) }}
                    {{ Form::select('type_list',\App\Enums\SecondaryTypeList::toSelectArray($content->type_list) ,null, array_merge(['class' => 'form-control type_list', 'placeholder'=>trans('admin.select')])) }}

                </div>


                    <div class="col-md-12">
                        {{ Form::label(trans('admin.description_en'), null, ['class' => 'control-label']) }}
                        {{ Form::textarea('description_en', old('description_en'), array_merge([ 'id'=>'description_en' ,'class' => 'form-control'])) }}
                    </div>

                    @foreach($content->contentsMedia as $contentMedia)
                    <div class="form-group appendDiv col-sm-12">
                        <label for="media" class="control-label">{{ trans('admin.media') }}</label>
                        <div class="form-group" style="display: flex;">
                            <input type="text" value="{{ $contentMedia->video_src}}" name="video_src[]" id="media" placeholder="video source" class="form-control">
                            <input style="margin-right: 10px;" value="{{ $contentMedia->video_desc}}" type="text" name="video_desc[]" id="media" placeholder="video Description" class="form-control">
                            
                        </div>           
                    </div>
                    @endforeach


                </div>

            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <!--<a href="{{aurl('content')}}" class="btn btn-danger">{{trans('admin.back')}}</a>-->
            {!! Form::close() !!}
        </div>
    </div>








@endsection
