@extends('admin.index')
@section('title',trans('admin.Create_new_content'))
@section('root_link', route('content.index'))
@section('root_name', trans('admin.description_information'))
@section('content')
    @push('js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
        <script>

            $('.content').on('click', '.remove', function() {
                $('.remove').closest('.content').find('.element').not(':first').last().remove();
            });
            $('.content').on('click', '.clone', function() {
                $('.clone').closest('.content').find('.element').first().clone().appendTo('.results');

            });
        </script>

        <script>
            $(document).ready(function () {
                $('.grade_id').on('change',function () {
                    
                    var grade_id = $('.grade_id option:selected').val();
                    var units_id = $('.units_id option:selected').val();
                    var type_list = $('.type_list option:selected').val();
                    $("#loadingmessage").css("display","block");
                    $(".column-form").css("display","none");
                    if (this){
                        $.ajax({
                            url: '{{aurl('content_grammar')}}',
                            type:'get',
                            dataType:'html',
                            data:{
                                grade_id : grade_id,
                                units_id : units_id,
                                type_list : type_list
                            },
                            success: function (data) {
                                $("#loadingmessage").css("display","none");
                                $('.column-form').css("display","block").html(data);

                            }
                        });
                    }else{
                        $('.column-form').html('');
                    }
                });
            })

            // add new media
            $(document).ready(function () {
            $('#appendSubCat').click(function (e) {
                alert('jjj');
                return
                e.preventDefault();
                let parentDiv = $('.appendDiv');
                parentDiv.append(`
                <div class="form-group" style="display: flex">
                       <input type="file" name="media[]" class="form-control" id="media" placeholder="{{trans('admin.media')}}">
                       <a style="margin-right: 10px" id="deleteLink" onclick="$(this).parent().remove()" class="btn btn-danger" href="#"><i class="fa fa-close"></i></a>
                   </div>
                `);
            });
        })


        </script>

    @endpush


<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">

        {!! Form::open(['method'=>'POST','route' => 'content.store', 'files' => true]) !!}

        <div class="form-group">
            <div class="col-lg-6 col-md-6 col-sm-12 hidden">
                {{ Form::label('branches_id', trans('admin.branches_id'), ['class' => 'control-label']) }}
                {{ Form::select('branches_id',$branche ,null, array_merge(['class' => 'form-control branche_id' , 'placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-4 col-md-4 col-sm-12">
                {{ Form::label('grade_id', trans('admin.ar_grade_id'), ['class' => 'control-label']) }}
                {{ Form::select('grade_id',$grade  ,null, array_merge(['class' => 'form-control grade_id' , 'placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
        <div id='loadingmessage' style='display:none; margin-top: 20px' class="text-center">
            <img src="{{ url('/') }}/images/ajax-loader.gif"/>
        </div>
        <div class="column-form"></div>

        {{Form::close()}}

    </div>
</div>









@endsection
