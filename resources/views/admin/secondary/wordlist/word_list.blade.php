
 @if($type_list == 0)

     <div class="form-group row">
         <div class="col-md-6 required">
             {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
             {{ Form::text('name_ar', null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
         </div>
         <div class="col-md-6 required">
             {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
             {{ Form::text('name_en', null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
         </div>



     </div>


 @elseif($type_list == 1)
     <div class="form-group row">
         <div class="col-md-6 required">
             {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
             {{ Form::text('name_ar', null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
         </div>
         <div class="col-md-6 required">
             {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
             {{ Form::text('name_en', null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
         </div>



     </div>


 @endif



