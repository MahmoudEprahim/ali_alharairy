@extends('admin.index')
@section('title',trans('admin.secondary_gates'))
@section('content')

{{--    @push('js')--}}
{{--        <script>--}}
{{--            $(function () {--}}
{{--                'use strict';--}}

{{--                $('.type_list').on('change',function () {--}}

{{--                    var branche_id = $('.branche_id option:selected').val();--}}

{{--                    var units_id = $('.units_id option:selected').val();--}}
{{--                    var type_list = $('.type_list option:selected').val();--}}

{{--                    $("#loadingmessage").css("display","block");--}}
{{--                    $(".column-form").css("display","none");--}}
{{--                    console.log(type_list,type_list);--}}
{{--                    if (this){--}}
{{--                        $.ajax({--}}
{{--                            url: '{{aurl('wordlist_grammar')}}',--}}
{{--                            type:'get',--}}
{{--                            dataType:'html',--}}
{{--                            data:{branche_id : branche_id,units_id : units_id,type_list : type_list,},--}}
{{--                            success: function (data) {--}}
{{--                                $("#loadingmessage").css("display","none");--}}
{{--                                $('.column-form').css("display","block").html(data);--}}

{{--                                $('.datepicker').datepicker({--}}
{{--                                    format: 'yyyy-mm-dd',--}}
{{--                                    rtl: true,--}}
{{--                                    language: '{{session('lang')}}',--}}
{{--                                    inline:true,--}}
{{--                                    minDate: 0,--}}
{{--                                    autoclose:true,--}}
{{--                                    minDateTime: dateToday--}}

{{--                                });--}}


{{--                            }--}}
{{--                        });--}}
{{--                    }else{--}}
{{--                        $('.column-form').html('');--}}
{{--                    }--}}
{{--                });--}}


{{--            });--}}
{{--        </script>--}}

{{--    @endpush--}}
    @push('js')
        <script>
            function myJsFunc() {
                var i = 0;
                ++i;
                var newInput = $('.new-row').html();
                $('.new-one').append(newInput);
            }
        </script>
    @endpush
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">


            {!! Form::open(['method'=>'POST','route' => 'wordlist.store','files'=>true]) !!}
            <div class="form-group row">
                <div class="col-md-6">
                {{ Form::label('branches_id', trans('admin.branches_id'), ['class' => 'control-label']) }}
                {{ Form::select('branches_id',$branche ,null, array_merge(['class' => 'form-control', 'placeholder'=> trans('admin.select')])) }}
                </div>
                <div class="col-md-6"> 
                    {{ Form::label('grade_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                    {{ Form::select('grade_id',$grade ,null, array_merge(['class' => 'form-control', 'placeholder'=> trans('admin.grade_id')])) }}
                </div>
                <div class="col-md-6"> 
                    {{ Form::label('units_id', trans('admin.unit_id'), ['class' => 'control-label']) }}
                    {{ Form::select('units_id',$unit ,null, array_merge(['class' => 'form-control', 'placeholder'=> trans('admin.unit_id')])) }}
                </div>
                <div class="col-md-6">
                    {{ Form::label('type', trans('admin.type'), ['class' => 'control-label']) }}
                    {{ Form::select('type',\App\Enums\word_list::toSelectArray() ,null, array_merge(['class' => 'form-control type_list', 'placeholder'=>trans('admin.select')])) }}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_ar', null, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-6">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_en', null, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>

            <div id='loadingmessage' style='display:none; margin-top: 20px' class="text-center">
                <img src="{{ url('/') }}/images/ajax-loader.gif"/>
            </div>
            <div id="report">
                <div class="column-form">

                </div>
            </div>
            <br>

            <!--teacher name -->


            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{url('admin/setting/wordlist')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>








    </div>
    </div>








@endsection
