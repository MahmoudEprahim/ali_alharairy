@extends('admin.index')
@section('title',trans('admin.secondary_gates'))
@section('content')
        <div class="box">
            @include('admin.layouts.message')
            <div class="box-header">
                <h3 class="box-title">{{$title}}</h3>
            </div>
            <div class="box-body">
                {!! Form::model($wordlist,['method'=>'PUT','route' => ['wordlist.update',$wordlist->id],'files'=>true]) !!}

                <div class="form-group row">
                    <div class="col-md-4">
                        {{ Form::label('branches_id', trans('admin.branche_id'), ['class' => 'control-label']) }}
                        {{ Form::select('branches_id',$branche ,$wordlist->branches_id, array_merge(['class' => 'form-control', 'placeholder'=>trans('admin.select')])) }}
                    </div>
                    <div class="col-md-4"> 
                    {{ Form::label('grade_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                    {{ Form::select('grade_id',$grade ,$wordlist->grade_id, array_merge(['class' => 'form-control', 'placeholder'=> trans('admin.grade_id')])) }}
                    </div>
                    <div class="col-md-4"> 
                        {{ Form::label('unit_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                        {{ Form::select('units_id',$unit ,$wordlist->units_id, array_merge(['class' => 'form-control', 'placeholder'=>trans('admin.grade_id')])) }}
                    </div>
                </div>
                <div class="form-grup row">
                    <div class="col-md-4">
                        {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                        {{ Form::text('name_ar', old('name_ar'), array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                        {{ Form::text('name_en', old('name_en'), array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="col-md-4">
                    
                    {{ Form::label('type', trans('admin.type'), ['class' => 'control-label']) }}
                    {{ Form::select('type',\App\Enums\word_list::toSelectArray($wordlist->type_list) ,null, array_merge(['class' => 'form-control type_list', 'placeholder'=>trans('admin.select')])) }}

                    </div>
                </div>
                   
                <br>
                <div class="clearfix"></div>
                    {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
                    <a href="{{aurl('setting/wordlist')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
                    {!! Form::close() !!}
            </div>
    </div>
   

@endsection
