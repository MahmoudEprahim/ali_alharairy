@extends('admin.index')
@section('title',trans('admin.secondary_gates'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.show_unit'). ' ' . session_lang($wordlist->name_en,$wordlist->name_ar)}}</h3>
        </div>
        <div class="box-body">

            <div class="form-group row">
                <div class="col-md-4 ">
                    {{ Form::label('type', trans('admin.type'), ['class' => 'control-label']) }}
                    <div class="form-control">{{\App\Enums\word_list::getDescription($wordlist->type)}}</div>
                </div>
                <div class="col-md-4 ">
                    {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$wordlist->name_ar}}</div>
                </div>
                <div class="col-md-4 ">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    <div class="form-control">{{$wordlist->name_en}}</div>
                </div>
            </div>

            

            <br>
            <div class="clearfix"></div>
           
            <a href="{{aurl('setting/wordlist')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>








@endsection
