@extends('admin.index')
@section('title',trans('admin.edit_list'). ' ' . $list->list_name_ar)
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($list,['method'=>'PUT','route' => ['lists.update',$list->id],'files'=>true]) !!}
            

            <div class="form-group row">
                <div class="col-md-4">
                {{ Form::label(trans('admin.list_name_ar'), null, ['class' => 'control-label']) }}
                {{ Form::text('list_name_ar', $list->list_name_ar, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
                <div class="col-md-4">
                {{ Form::label(trans('admin.list_name_en'), null, ['class' => 'control-label']) }}
                {{ Form::text('list_name_en', $list->list_name_en, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label('grade_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                    {{ Form::select('grade_id',$grade ,$list->grade_id, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label('unit_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                    {{ Form::select('unit_id',$unit ,$list->unit_id, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>

            </div>

            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('lists')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
