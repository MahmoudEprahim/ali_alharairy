@extends('admin.index')
@section('title',trans('admin.add_new_profile'))
@section('content')
    @push('css')
        <style>
            .list-group-item {
                padding: 30px 15px !important;
            }
        </style>

    @endpush


    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="@if($profile->image != null){{asset('storage/'.$profile->image)}}@else {{url('/')}}/adminlte/previewImage.png @endif" alt="User profile picture">

                    <h3 class="profile-username text-center">{{session_lang($profile->name_en,$profile->name_ar)}}</h3>
                    <p class="profile-username text-center">{{session_lang($profile->user_title_en,$profile->user_title_ar)}}</p>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>{{trans('admin.profile_desc')}}</b><br>
                            {{--<p class="pull-right">{{$profile->profile_desc}}</p>--}}
                            @if($profile->profile_desc == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->profile_desc}}  @endif
                        </li>
                        <li class="list-group-item">
                            <b>{{trans('admin.facebook')}}</b>
                            {{--<p class="pull-left">{{$profile->facebook}}</p>--}}
                            @if($profile->facebook == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->facebook}}  @endif
                        </li>
                        <li class="list-group-item">
                            <b>{{trans('admin.linkedin')}}</b>
                            {{--<p class="pull-right">{{$profile->linkedin}}</p>--}}
                            @if($profile->linkedin == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->linkedin}}  @endif
                        </li>

                        <li class="list-group-item">
                            <b>{{trans('admin.twitter')}}</b>
                            {{--<p class="pull-right">{{$profile->twitter}}</p>--}}
                            @if($profile->twitter == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->twitter}}  @endif
                        </li>

                        <li class="list-group-item">
                            <b>{{trans('admin.google')}}</b>
                            {{--<p class="pull-right">{{$profile->google}}</p>--}}
                            @if($profile->google == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->google}}  @endif
                        </li>

                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <!-- <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{trans('admin.about_profile_owner')}}</h3>
                </div>
                
                <div class="box-body">

                    <strong><i class="fa fa-user-circle margin-r-5"></i> {{trans('admin.owner_name')}}</strong>

                    {{--<p class="text-muted">{{$profile->owner_name}}</p>--}}
                    @if($profile->owner_name == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->owner_name}}  @endif
                    <hr>
                    <strong><i class="fa fa-phone margin-r-5"></i> {{trans('admin.owner_phone')}}</strong>

                    {{--<p class="text-muted">{{$profile->owner_phone}}</p>--}}
                    @if($profile->owner_phone == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->owner_phone}}  @endif
                    <hr>

                    <strong><i class="fa fa-phone margin-r-5"></i> {{trans('admin.owner_mobile')}}</strong>

                    {{--<p class="text-muted">{{$profile->owner_mobile}}</p>--}}
                    @if($profile->owner_mobile == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->owner_mobile}}  @endif

                    <hr>
                    <strong><i class="fa fa-phone margin-r-5"></i> {{trans('admin.owner_phone_num')}}</strong>

                    {{--<p class="text-muted">{{$profile->owner_phone_num}}</p>--}}
                    @if($profile->owner_phone_num == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->owner_phone_num}}  @endif

                    <hr>


                </div>
                
            </div> -->
            
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <div class="row">
                            <div class="col-md-2">
                                <strong>
                                    {{trans('admin.full_name')}}
                                </strong>
                            </div>
                            <div class="col-md-10">
                                :     {{session_lang($profile->name_en,$profile->name_ar)}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-2">
                                <strong>
                                    {{trans('admin.user_title_ar')}}
                                </strong>
                            </div>
                            <div class="col-md-10">
                                :     {{session_lang($profile->user_title_en,$profile->user_title_ar)}}
                            </div>
                        </div>
                       
                        <br>
                        
                        
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <strong>
                                    {{trans('admin.phone_1')}}
                                </strong>
                            </div>
                            <div class="col-md-10">
                                :
                                @if($profile->profile_desc == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->profile_desc}}  @endif
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <strong>
                                    {{trans('admin.first_grade_num')}}
                                </strong>
                            </div>
                            <div class="col-md-10">
                                :
                                @if($profile->first_grade_num == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->first_grade_num}}  @endif
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <strong>
                                    {{trans('admin.second_grade_num')}}
                                </strong>
                            </div>
                            <div class="col-md-10">
                                :
                                @if($profile->second_grade_num == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->second_grade_num}}  @endif
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <strong>
                                    {{trans('admin.third_grade_num')}}
                                </strong>
                            </div>
                            <div class="col-md-10">
                                :
                                @if($profile->third_grade_num == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$profile->third_grade_num}}  @endif
                            </div>
                        </div>

                        <a href="/admin/profile/{{$profile->id}}/edit" class="btn btn-success edit"><i class="fa fa-edit"></i> {{ trans('admin.edit') }}</a>

                        <a href="/admin/profile" class="btn btn-primary "><i class="fa fa-angle-double-left"></i> {{ trans('admin.previous_page') }}</a>

                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>
   
@endsection