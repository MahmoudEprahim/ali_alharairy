@extends('admin.index')
@section('title',trans('admin.add_new_list'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
      

        {!! Form::open(['method'=>'POST','route' => 'lists.store','files'=>true]) !!}
        

        <!--lists name -->
            <div class="form-group row">
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.list_name_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('list_name_ar', old('list_name_ar'), array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.list_name_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('list_name_en', old('list_name_en'), array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6 required"> 
                    {{ Form::label('grade_id', trans('admin.grade_id'), ['class' => 'control-label']) }}
                    {{ Form::select('grade_id',$grade ,null, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.grade_id')])) }}
                </div>
                <div class="col-md-6 required"> 
                    {{ Form::label('unit_id', trans('admin.unit_id'), ['class' => 'control-label']) }}
                    {{ Form::select('unit_id',$unit ,null, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.unit_id')])) }}
                </div>
            </div>


            
        <!-- lists -->

        <div class="clearfix"></div>
        {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
        <a href="{{aurl('lists')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        {!! Form::close() !!}
        </div>
        

        

        


       
    </div>
</div>
        







@endsection
