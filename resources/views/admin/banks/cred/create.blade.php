@extends('admin.index')
@section('title',trans('admin.create_cred_limitations'))
@section('content')
    @push('js')
        <script>
            $(function () {
                'use strict'
                $('.e2').select2({
                    placeholder: "{{trans('admin.select')}}",
                    dir: '{{direction()}}'
                });
            });

        </script>
    @endpush
    <limitations-cred-component invoice="{{generateBarcodeNumber()}}"></limitations-cred-component>
    <vue-progress-bar>

    </vue-progress-bar>




@endsection
