@extends('admin.index')
@section('title',trans('admin.add_user_profile'))
@section('content')


@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">


        {!! Form::open(['method'=>'POST','route' => 'profile.store','files'=>true]) !!}
        
            <div class="form-group">
                {{ Form::label(trans('admin.profile_image'), null, ['class' => 'control-label']) }}
                {{ Form::file('profile_image', array_merge(['class' => 'form-control'])) }}

            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_ar', old('name_ar'), array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-6">
                    {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name_en', old('name_en'), array_merge(['class' => 'form-control'])) }}
                </div>
            </div>

        <!--teacher title -->
            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.user_title_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_ar', old('title_ar'), array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-md-6">
                    {{ Form::label(trans('admin.user_title_en'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title_en', old('title_en'), array_merge(['class' => 'form-control'])) }}
                </div>
            </div>




        <!-- profile description -->

            <div class="form-group row">
                <div class="col-sm-6">
                    {{ Form::label(trans('admin.profile_desc_ar'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('profile_desc_ar',old('profile_desc_ar'), array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::label(trans('admin.profile_desc_en'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('profile_desc_en', old('profile_desc_en'), array_merge(['class' => 'form-control'])) }}
                </div>
            </div>
            <div class="form-group row EDUCATION">
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.first_stage'), null, ['class' => 'control-label']) }}

                    <div class="col-sm-12">
                        <div class="col-sm-4">
                        {{ Form::text('data_first_stage', old('data_first_stage'), array_merge(['class' => 'form-control','style' => 'width:50px;'])) }}
                        </div>
                        <div class="col-sm-8">
                          {{ Form::textarea('first_stage', old('first_stage'), array_merge(['class' => 'form-control','style' => 'height:70px;width:145px;resize:none;'])) }}
                         </div>
                  </div>
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.second_stage'), null, ['class' => 'control-label']) }}

                    <div class="col-sm-12">
                    <div class="col-sm-4">
                    {{ Form::text('data_second_stage', old('data_second_stage'), array_merge(['class' => 'form-control','style' => 'width:50px;'])) }}
                    </div>
                        <div class="col-sm-8">
                    {{ Form::textarea('second_stage', old('second_stage'), array_merge(['class' => 'form-control','style' => 'height:70px;width:145px;resize:none;'])) }}


                  </div>
                  </div>
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.third_stage'), null, ['class' => 'control-label']) }}

                    <div class="col-sm-12">
                    <div class="col-sm-4">
                    {{ Form::text('data_third_stage', old('data_third_stage'), array_merge(['class' => 'form-control','style' => 'width:50px;'])) }}
                    </div>
                        <div class="col-sm-8">
                    {{ Form::textarea('third_stage', old('third_stage'), array_merge(['class' => 'form-control','style' => 'height:70px;width:145px;resize:none;'])) }}


                  </div>
                  </div>
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.fourth_stage'), null, ['class' => 'control-label']) }}

                    <div class="col-sm-12">
                    <div class="col-sm-4">
                    {{ Form::text('data_fourth_stage', old('data_fourth_stage'), array_merge(['class' => 'form-control','style' => 'width:50px;'])) }}
                    </div>
                        <div class="col-sm-8">
                    {{ Form::textarea('fourth_stage', old('fourth_stage'), array_merge(['class' => 'form-control','style' => 'height:70px;width:129px;resize:none;'])) }}


                  </div>
                  </div>
                </div>
            </div>
            <div class="form-group row EXPERIENCE">
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.ex_first_stage'), null, ['class' => 'control-label']) }}

                    <div class="col-sm-12">
                        <div class="col-sm-4">
                        {{ Form::text('data_ex_first_stage', old('data_ex_first_stage'), array_merge(['class' => 'form-control','style' => 'width:50px;'])) }}
                        </div>
                        <div class="col-sm-8">
                          {{ Form::textarea('ex_first_stage', old('ex_first_stage'), array_merge(['class' => 'form-control','style' => 'height:70px;width:145px;resize:none;'])) }}
                         </div>
                  </div>
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.ex_second_stage'), null, ['class' => 'control-label']) }}

                    <div class="col-sm-12">
                    <div class="col-sm-4">
                    {{ Form::text('data_ex_second_stage', old('data_ex_second_stage'), array_merge(['class' => 'form-control','style' => 'width:50px;'])) }}
                    </div>
                        <div class="col-sm-8">
                    {{ Form::textarea('ex_second_stage', old('ex_second_stage'), array_merge(['class' => 'form-control','style' => 'height:70px;width:145px;resize:none;'])) }}


                  </div>
                  </div>
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.ex_third_stage'), null, ['class' => 'control-label']) }}

                    <div class="col-sm-12">
                    <div class="col-sm-4">
                    {{ Form::text('data_ex_third_stage', old('data_ex_third_stage'), array_merge(['class' => 'form-control','style' => 'width:50px;'])) }}
                    </div>
                        <div class="col-sm-8">
                    {{ Form::textarea('ex_third_stage', old('ex_third_stage'), array_merge(['class' => 'form-control','style' => 'height:70px;width:145px;resize:none;'])) }}


                  </div>
                  </div>
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.ex_fourth_stage'), null, ['class' => 'control-label']) }}

                    <div class="col-sm-12">
                    <div class="col-sm-4">
                    {{ Form::text('data_ex_fourth_stage', old('data_ex_fourth_stage'), array_merge(['class' => 'form-control','style' => 'width:50px;'])) }}
                    </div>
                        <div class="col-sm-8">
                    {{ Form::textarea('ex_fourth_stage', old('ex_fourth_stage'), array_merge(['class' => 'form-control','style' => 'height:70px;width:129px;resize:none;'])) }}


                  </div>
                  </div>
                </div>
            </div>




            <div class="form-group row SKILLS">
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.skill_1'), null, ['class' => 'control-label']) }}
                    {{ Form::text('skill_1', $profile->skill_1, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.skill_2'), null, ['class' => 'control-label']) }}
                    {{ Form::text('skill_2', $profile->skill_2, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.skill_3'), null, ['class' => 'control-label']) }}
                    {{ Form::text('skill_3', $profile->skill_3, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.skill_4'), null, ['class' => 'control-label']) }}
                    {{ Form::text('skill_4', $profile->skill_4, array_merge(['class' => 'form-control'])) }}
                </div>

            </div>
          <!-- profile teaching grades -->
            <div class="row form-group social_media">
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.facebook'), null, ['class' => 'control-label']) }}
                    {{ Form::text('facebook', setting()->facebook, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.twitter'), null, ['class' => 'control-label']) }}
                    {{ Form::text('twitter', setting()->twitter, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.google'), null, ['class' => 'control-label']) }}
                    {{ Form::text('google', setting()->google, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.linkedin'), null, ['class' => 'control-label']) }}
                    {{ Form::text('linkedin', setting()->linkedin, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>

            <div class="form-group row CONTACT">
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.email'), null, ['class' => 'control-label']) }}
                    {{ Form::text('email', $profile->email, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.phone'), null, ['class' => 'control-label']) }}
                    {{ Form::text('phone', $profile->phone, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.Location'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Location', $profile->Location, array_merge(['class' => 'form-control'])) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::label(trans('admin.site'), null, ['class' => 'control-label']) }}
                    {{ Form::text('site', $profile->site, array_merge(['class' => 'form-control'])) }}
                </div>

            </div>


        {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}
        </div>








    </div>
</div>








@endsection
