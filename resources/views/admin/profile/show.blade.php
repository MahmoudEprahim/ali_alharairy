@extends('admin.index')
@section('title',trans('admin.edit_profile'). ' ' . $profile->name_ar)
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{trans('admin.profile')}}</h3>
        </div>
        <div class="box-body">


            <div class="form-group row">
                <div class="col-md-3">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    @if($profile->image ==null)
                        <img src="{{asset('adminlte/Bus.png')}}" style="width: 100px;margin-top: 10px">

                    @else
                        <img src="{{asset('storage/'.$profile->image)}}" style="width: 100px;margin-top: 10px">

                    @endif
                </div>
            </div>
            <div class="form-group row">

                <div class="col-md-6">
                    <strong>{{trans('admin.name_ar')}}</strong>
                    : <div class ="form-control">{{$profile->name_ar}}</div>
                </div>
                <div class="col-md-6">

                    <strong class ="control-label">{{trans('admin.name_en')}}</strong>
                    : <div class ="form-control">{{$profile->name_en}}</div>

                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <strong>{{trans('admin.user_title_ar')}}</strong>
                    : <div class ="form-control">{{$profile->title_ar}}</div>

                </div>
                <div class="col-md-6">
                    <strong>{{trans('admin.user_title_en')}}</strong>
                    : <div class ="form-control">{{$profile->title_en}}</div>
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-6">
                    {{ Form::label(trans('admin.profile_desc'), null, ['class' => 'control-label']) }}
                    <strong>{{trans('admin.profile_desc')}}</strong>
                    : <div >{{$profile->profile_desc_ar}}</div>
                </div>
                <div class="col-sm-6">
                    <strong>{{trans('admin.profile_desc')}}</strong>
                    : <div >{{$profile->profile_desc_en}}</div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-3">
                    <strong>{{trans('admin.facebook')}}</strong>
                    : <div class="form-control">{{$profile->facebook}}</div>

                </div>
                <div class="col-sm-3">
                    <strong>{{trans('admin.twitter')}}</strong>
                    : <div class="form-control">{{$profile->twitter}}</div>

                </div>
                <div class="col-sm-3">
                    <strong>{{trans('admin.linkedin')}}</strong>
                    : <div class="form-control">{{$profile->linkedin}}</div>

                </div>
                <div class="col-sm-3">
                    <strong>{{trans('admin.google')}}</strong>
                    : <div class="form-control">{{$profile->google}}</div>

                </div>

            </div>



            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('profile')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>







@endsection
