@extends('admin.index')
@section('title',trans('admin.add_new_list'))
@section('content')


@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush

@if($hours > 24 || $hours == null)
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>

    <div class="box-body">


        {!! Form::open(['method'=>'POST','route' => 'posts.store','files'=>true]) !!}


        <!--teacher name -->
            <div class="form-group row">
                <div class="col-md-5 required">
                    {{ Form::label(trans('admin.post_title'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title', null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-10 required">
                    {{ Form::label(trans('admin.post_content'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('body', null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
             </div>
            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image',null, array_merge(['class' => 'form-control'])) }}
                </div>

            </div>




        <!-- profile description -->
        <div class="clearfix"></div>
        {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('posts')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        {!! Form::close() !!}
        </div>

    </div>


@else



<div class="box-header">
        <p class= "text-center">you must wait 24 hours to create a new post</p>
    </div>

@endif





@endsection
