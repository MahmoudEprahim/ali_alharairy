@extends('admin.index')
@section('title',trans('admin.post_information'))
@section('content')
    @push('css')
        <style>
            .list-group-item {
                padding: 30px 15px !important;
            }
        </style>

    @endpush


    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="@if($user->image != null){{asset('storage/'.$user->image)}}@else {{url('/')}}/adminlte/previewImage.png @endif" alt="User profile picture">

                    <h3 class="profile-username text-center">{{session_lang($user->name,$user->name)}}</h3>

                    <p class="text-center">{{$post->created_at->diffForHumans()}}</p>
                </div>
                <!-- /.box-body -->
            </div>


        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <div class="row">

                            <div class="col-md-5">
                               {{trans('admin.post_title')}} :     {{$post->title}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                {{trans('admin.post_body')}}
                            </div>
                            <div class="col-md-7">
                                :{{$post->body}}
                            </div>
                    </div>
                        <br>

                        <div class="row">

                            <div class="col-md-10">
                                {{trans('admin.post_image')}}:
                            <img class=" img-responsive" src="@if($post->image != null){{asset('storage/'.$post->image)}}@else {{url('/')}}/default/message.png @endif" alt="post image">
                            </div>
                        </div>

                        <br>

                        <a href="{{aurl('posts')}}" class="btn btn-danger edit"><i class="fa fa-back"></i> {{ trans('admin.back') }}</a>


                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>

@endsection
