@extends('admin.index')
@section('title',trans('admin.edit_message'). ' ' . $post->user_id)
@section('content')


    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($post,['method'=>'PUT','route' => ['posts.update',$post->id],'files'=>true]) !!}


            <div class="form-group row">
                <div class="col-md-4 required">
                    {{ Form::label(trans('admin.title'), null, ['class' => 'control-label']) }}
                    {{ Form::text('title', old('title'), array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>

                <div class="col-md-4 required">
                    {{ Form::label(trans('admin.post_content'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('body', old('body'), array_merge(['id' => 'basic-example','class' => 'form-control', 'required' => 'required'])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image',old('image'), array_merge(['class' => 'form-control'])) }}
                </div>
                <!-- <div class="col-md-4 ">
                    {{ Form::label('user_id', trans('admin.user'), ['class' => 'control-label']) }}
                    {{ Form::select('user_id',$user ,$post->user_id, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.user')])) }}
                </div> -->
            </div>

            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('messages')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>





    @push('scripts')
        <script>

            tinymce.init({
                selector: 'textarea#basic-example',
                height: 500,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tiny.cloud/css/codepen.min.css'
                ]
            });

        </script>
    @endpush

@endsection
