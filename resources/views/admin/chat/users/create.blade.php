@extends('admin.index')
@section('title',trans('admin.add_user_profile'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
      

        {!! Form::open(['method'=>'POST','route' => 'users.store','files'=>true]) !!}
        

        <!--teacher name -->
            <div class="form-group row">
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.name'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name',null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.email'), null, ['class' => 'control-label']) }}
                    {{ Form::email('email', null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
            </div>

        <!--teacher title -->
            <div class="form-group row">
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.password'), null, ['class' => 'control-label']) }}
                    {{ Form::password('password', array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
               
            </div>

        <!-- profile image -->
            <div class="form-group row">
                
                <div class="col-sm-6">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                </div>
            </div>
            
        <!-- profile description -->

            

          
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('users')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
            </div>
        

        

        


       
    </div>
</div>
        







@endsection
