@extends('admin.index')
@section('title',trans('admin.edit_user'))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($user,['method'=>'PUT','route' => ['users.update',$user->id],'files'=>true]) !!}
            

           
            <div class="form-group row">
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.name'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name', old('name'), array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.email'), null, ['class' => 'control-label']) }}
                    {{ Form::email('email', old('email'), array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
            </div>

        <!--teacher title -->
            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.password'), null, ['class' => 'control-label']) }}
                    {{ Form::password('password', array_merge(['class' => 'form-control'])) }}
                </div>

                <div class="col-sm-6">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', array_merge(['class' => 'form-control'])) }}
                </div>
               
            </div>


            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.update'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('profile')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
