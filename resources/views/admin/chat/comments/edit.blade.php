@extends('admin.index')
@section('title',trans('admin.edit_chapter'). ' ' . $comment->body)
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($comment,['method'=>'PUT','route' => ['comments.update',$comment->id],'files'=>true]) !!}



        <!--comment body -->
        <div class="form-group row">
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.body'), null, ['class' => 'control-label']) }}
                    {{ Form::text('body', old('body'), array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
                <div class="col-md-6">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', old('image'), array_merge(['class' => 'form-control'])) }}
                </div>
            </div>

            <div class="form-group row">
            <div class="col-md-6 required">
                    {{ Form::label('user_id', trans('admin.user_id'), ['class' => 'control-label']) }}
                    {{ Form::select('user_id',$user ,$comment->user_id, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.user_id')])) }}
                </div>
                <div class="col-md-6 required">
                    {{ Form::label('message_id', trans('admin.message_id'), ['class' => 'control-label']) }}
                    {{ Form::select('message_id',$message ,$comment->message_id, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.message_id')])) }}
                </div>

            </div>

        <div class="cleafix"></div>
        <br>

        {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
        <a href="{{aurl('comments')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        {!! Form::close() !!}
        </div>

    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
