@extends('admin.index')
@section('title',trans('admin.create_new_comment'))
@section('content')
    
    
@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    
    <div class="box-body">
      

        {!! Form::open(['method'=>'POST','route' => 'comments.store','files'=>true]) !!}
        

        <!--comment body -->
            <div class="form-group row">
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.comment_body'), null, ['class' => 'control-label']) }}
                    {{ Form::text('body', null, array_merge(['class' => 'form-control', 'required' => 'required'])) }}
                </div>
                <div class="col-md-6">
                    {{ Form::label(trans('admin.image'), null, ['class' => 'control-label']) }}
                    {{ Form::file('image', null, array_merge(['class' => 'form-control'])) }}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6 required"> 
                    {{ Form::label('user_id', trans('admin.user_id'), ['class' => 'control-label']) }}
                    {{ Form::select('user_id',$user ,null, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.user_id')])) }}
                </div>
                <div class="col-md-6 required"> 
                    {{ Form::label('post_id', trans('admin.post_id'), ['class' => 'control-label']) }}
                    {{ Form::select('post_id',$post ,null, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.post_id')])) }}
                </div>
               
                
            </div>
                
        <div class="cleafix"></div>
        <br>

        {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
        <a href="{{aurl('comments')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
        {!! Form::close() !!}
        </div>
      


        

        

        


       
    </div>
</div>
        







@endsection
