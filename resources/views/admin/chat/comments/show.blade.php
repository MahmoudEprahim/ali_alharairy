@extends('admin.index')
@section('title',trans('admin.show_comments'))
@section('content')
    @push('css')
        <style>
            .list-group-item {
                padding: 30px 15px !important;
            }
        </style>

    @endpush


    <div class="row">
    {!! Form::model($comment,['method'=>'Post','route' => ['comments.reply',$comment->id],'files'=>true]) !!}

    {{--        <div class="col-md-3">--}}

{{--            <!-- Profile Image -->--}}
{{--            <div class="box box-primary">--}}
{{--                <div class="box-body box-profile">--}}
{{--                    <img class="profile-user-img img-responsive img-circle" src="@if($comment->image != null){{asset('storage/'.$comment->image)}}@else {{url('/')}}/adminlte/previewImage.png @endif" alt="User profile picture">--}}

{{--                    <h3 class="profile-username text-center">{{session_lang($comment->name_en,$comment->name_ar)}}</h3>--}}
{{--                    <p class="profile-username text-center">{{session_lang($comment->user_title_en,$comment->user_title_ar)}}</p>--}}
{{--                    <ul class="list-group list-group-unbordered">--}}
{{--                        <li class="list-group-item">--}}
{{--                            <b>{{trans('admin.profile_desc')}}</b><br>--}}
{{--                            --}}{{--<p class="pull-right">{{$comment->profile_desc}}</p>--}}
{{--                            @if($comment->profile_desc == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$comment->profile_desc}}  @endif--}}
{{--                        </li>--}}
{{--                        <li class="list-group-item">--}}
{{--                            <b>{{trans('admin.facebook')}}</b>--}}
{{--                            --}}{{--<p class="pull-left">{{$comment->facebook}}</p>--}}
{{--                            @if($comment->facebook == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$comment->facebook}}  @endif--}}
{{--                        </li>--}}
{{--                        <li class="list-group-item">--}}
{{--                            <b>{{trans('admin.linkedin')}}</b>--}}
{{--                            --}}{{--<p class="pull-right">{{$comment->linkedin}}</p>--}}
{{--                            @if($comment->linkedin == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$comment->linkedin}}  @endif--}}
{{--                        </li>--}}

{{--                        <li class="list-group-item">--}}
{{--                            <b>{{trans('admin.twitter')}}</b>--}}
{{--                            --}}{{--<p class="pull-right">{{$comment->twitter}}</p>--}}
{{--                            @if($comment->twitter == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$comment->twitter}}  @endif--}}
{{--                        </li>--}}

{{--                        <li class="list-group-item">--}}
{{--                            <b>{{trans('admin.google')}}</b>--}}
{{--                            --}}{{--<p class="pull-right">{{$comment->google}}</p>--}}
{{--                            @if($comment->google == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$comment->google}}  @endif--}}
{{--                        </li>--}}

{{--                    </ul>--}}
{{--                </div>--}}
{{--                <!-- /.box-body -->--}}
{{--            </div>--}}
{{--            <!-- /.box -->--}}

{{--            <!-- About Me Box -->--}}
{{--            <!-- <div class="box box-primary">--}}
{{--                <div class="box-header with-border">--}}
{{--                    <h3 class="box-title">{{trans('admin.about_profile_owner')}}</h3>--}}
{{--                </div>--}}

{{--                <div class="box-body">--}}

{{--                    <strong><i class="fa fa-user-circle margin-r-5"></i> {{trans('admin.owner_name')}}</strong>--}}

{{--                    --}}{{--<p class="text-muted">{{$comment->owner_name}}</p>--}}
{{--                    @if($comment->owner_name == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$comment->owner_name}}  @endif--}}
{{--                    <hr>--}}
{{--                    <strong><i class="fa fa-phone margin-r-5"></i> {{trans('admin.owner_phone')}}</strong>--}}

{{--                    --}}{{--<p class="text-muted">{{$comment->owner_phone}}</p>--}}
{{--                    @if($comment->owner_phone == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$comment->owner_phone}}  @endif--}}
{{--                    <hr>--}}

{{--                    <strong><i class="fa fa-phone margin-r-5"></i> {{trans('admin.owner_mobile')}}</strong>--}}

{{--                    --}}{{--<p class="text-muted">{{$comment->owner_mobile}}</p>--}}
{{--                    @if($comment->owner_mobile == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$comment->owner_mobile}}  @endif--}}

{{--                    <hr>--}}
{{--                    <strong><i class="fa fa-phone margin-r-5"></i> {{trans('admin.owner_phone_num')}}</strong>--}}

{{--                    --}}{{--<p class="text-muted">{{$comment->owner_phone_num}}</p>--}}
{{--                    @if($comment->owner_phone_num == null) <div class="badge">{{trans('admin.theres_no_data')}} </div> @else {{$comment->owner_phone_num}}  @endif--}}

{{--                    <hr>--}}


{{--                </div>--}}

{{--            </div> -->--}}

{{--        </div>--}}
        <!-- /.col -->
        <div class="col-md-12">
            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <div class="row">
                            <div class="col-md-2">
                                <strong>
                                    {{trans('admin.full_name')}}
                                </strong>
                            </div>
                            <div class="col-md-10">
                                :     {{session_lang($comment->title,$comment->title)}}
                            </div>

                        </div>
                        <br><br><br><br>

                        <div class="row">
                            <div class="col-md-2">
                                <strong>
                                    {{trans('admin.body')}}
                                </strong>
                            </div>
                            <div class="col-md-10">
                                :     {{session_lang($comment->body,$comment->body)}}
                            </div>
                        </div>
                        <br><br><br><br>
                        <div class="row">
                            <div class="col-md-2">
                                <strong>
                                    {{trans('admin.reply')}}
                                </strong>
                            </div>
                            <div class="col-md-10">
                                :     {{ Form::textarea('reply', old('reply'), array_merge(['class' => 'form-control'])) }}
                                <div class="hidden">
                                   {{ Form::text('id',$comment->id, array_merge(['class' => 'form-control'])) }}

                                </div>
                            </div>

                        </div>




                        {{Form::submit(trans('admin.reply'),['class'=>'btn btn-primary'])}}

                        <a href="{{url('comments')}}" class="btn btn-danger"><i class="fa fa-angle-double-left"></i> {{ trans('admin.previous_page') }}</a>

                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>

@endsection
