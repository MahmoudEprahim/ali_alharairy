@extends('admin.index')
@section('title',trans('admin.edit_relatedness'))
@section('content')
    @push('js')

        <script>
            $(function () {
                'use strict'
                $('#e2').select2({
                    placeholder: "select a subscriber",
                    dir: '{{direction()}}'
                });
            })
        </script>


    @endpush
    @push('css')
        <style>
            .select2-container--default .select2-selection--multiple .select2-selection__choice{
                background-color: #333;
            }
        </style>

    @endpush
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($related,['method'=>'PUT','route' => ['relatedness.update',$related->id]]) !!}
            <div class="parent">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label(trans('admin.arabic_name'), null, ['class' => 'control-label']) }}
                            {{ Form::text(  'name_ar', $related->name_ar , array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label(trans('admin.english_name'), null, ['class' => 'control-label']) }}
                            {{ Form::text(  'name_en', $related->name_en , array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ Form::label('relation', trans('admin.relation'), ['class' => 'control-label']) }}
                            {{ Form::select('relation', \App\Enums\RelationType::toSelectArray(), $related->relation  , array_merge(['class' => 'form-control owner','placeholder'=>trans('admin.select')])) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label(trans('admin.phone_1'), null, ['class' => 'control-label']) }}
                            {{ Form::text('phone_1', $related->phone_1  , array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label(trans('admin.phone_2'), null, ['class' => 'control-label']) }}
                            {{ Form::text('phone_2', $related->phone_2, array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }} <strong>1</strong>
                            {{ Form::text('mobile_1', $related->mobile_1, array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }} <strong>2</strong>
                            {{ Form::text('mobile_2', $related->mobile_2, array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--<div class="col-md-3">-->
                    <!--    <div class="form-group">-->
                    <!--        {{ Form::label(  'countries', trans('admin.nationality'), ['class' => 'control-label']) }}-->
                    <!--        {{ Form::select( 'countries_id', $countries , $related->countries_id,array_merge(['class' => 'form-control owner','placeholder'=>trans('admin.select')])) }}-->
                    <!--    </div>-->
                    <!--</div>-->
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label(  'type', trans('admin.prove_personal'), ['class' => 'control-label']) }}
                            {{ Form::select(  'type', \App\Enums\PersonalType::toSelectArray(), $related->type, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label(  'number', trans('admin.number'), ['class' => 'control-label']) }}
                            {{ Form::text(  'number', $related->number, array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label(  'created_from', trans('admin.source'), ['class' => 'control-label']) }}
                            {{ Form::text(  'created_from', $related->created_from, array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label(  'job', trans('admin.job'), ['class' => 'control-label']) }}
                            {{ Form::text(  'job', $related->job, array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label(  'home_address', trans('admin.home_address'), ['class' => 'control-label']) }}
                            {{ Form::text(  'home_address', $related->home_address, array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label(  'note', trans('admin.note'), ['class' => 'control-label']) }}
                            {{ Form::text(  'note', $related->note, array_merge(['class' => 'form-control'])) }}
                        </div>
                    </div>
                </div>
            {{Form::submit(trans('admin.update'),['class'=>'btn btn-primary'])}}
            {!! Form::close() !!}
        </div>
    </div>







@endsection