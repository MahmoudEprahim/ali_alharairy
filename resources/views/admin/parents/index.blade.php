@extends('admin.index')
@section('title',trans('admin.parents_data'))
@section('content')
@push('js')
    @hasrole('reader')
    <script src="{{url('/')}}/js/dataTables.buttons.min.js"></script>
    @endhasrole

    <script>
        $(document).ready(function () {

            $(document).on('click', '.parent_active_link', function () {
                let url = $(this).attr('data-url'),
                    id = $(this).attr('data-id'),
                    linkElement = $('.link_'+id),
                    iconElement = linkElement.children('i');
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    data: { id: id },
                    success: function (data) {
                        if (data.status === 1){
                            iconElement.toggleClass('fa-close fa-check');
                            linkElement.toggleClass('btn-danger btn-success');
                        }

                    }
                })
            })


        })
    </script>
@endpush
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div>
    @include('admin.layouts.message')
    <!-- /.box-header -->
        <div class="box-body table-responsive">
            {!! $dataTable->table([
             'class' => 'table table-bordered table-striped table-hover'
             ],true) !!}
        </div>
        <!-- /.box-body -->
    </div>








    @push('js')
        {!! $dataTable->scripts() !!}
    @endpush
@endsection
