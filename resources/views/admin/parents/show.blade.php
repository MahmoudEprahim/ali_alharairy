@extends('admin.index')
{{--@section('title',trans('admin.show_profile_to') .session_lang($parents->name_en,$parents->name_ar))--}}
@section('content')
    @push('css')
        <style>
            .list-group-item {
                padding: 30px 15px !important;
            }
            .arabic{
                direction: ltr;
            }
        </style>

    @endpush


    <div class="row">

        <!-- /.col -->
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">{{trans('admin.activity')}}</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <!-- Post -->
                        <ul class="timeline timeline-inverse">

                                <li>
                                    <i class="fa fa-user bg-green"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header">{{trans('admin.arabic_name')}}</h3>

                                        <div class="timeline-body">
                                            <div class="form-group">
                                                {{session_lang($parents->name_en,$parents->name_ar)}}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-map-signs bg-green"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header">{{trans('admin.relation')}}</h3>

                                        <div class="timeline-body">
                                            <div class="form-group">
{{--                                                {{ Form::label('relation', trans('admin.relation'), ['class' => 'control-label']) }}--}}
{{--                                                <br>--}}
                                                {{\App\Enums\RelationType::getDescription($parents->relation)}}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-map-signs bg-green"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header">{{trans('admin.students_whom_follow')}}</h3>

                                        <div class="timeline-body">
                                            <div class="form-group">
{{--                                                {{ Form::label('relation', trans('admin.relation'), ['class' => 'control-label']) }}--}}
{{--                                                <br>--}}
                                                  <ul>
                                                    @foreach($parents->students as $student)
                                                        <li><a href="{{route('students.show',$student->id)}}">{{session_lang($student->name_en,$student->name_ar)}}</a></li>
                                                    @endforeach
                                                  </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-phone bg-green"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header">{{trans('admin.phone_1')}}</h3>

                                        <div class="timeline-body">
                                            <div class="form-group">
{{--                                                {{ Form::label(trans('admin.phone_1'), null, ['class' => 'control-label']) }}--}}
{{--                                                <br>--}}
                                                {{$parents->phone_1}}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-phone bg-green"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header">{{trans('admin.phone_2')}}</h3>

                                        <div class="timeline-body">
                                            <div class="form-group">
{{--                                                {{ Form::label(trans('admin.phone_2'), null, ['class' => 'control-label']) }}--}}
{{--                                                <br>--}}
                                                {{$parents->phone_2}}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-phone bg-green"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header">{{trans('admin.mob')}}</h3>

                                        <div class="timeline-body">
                                            <div class="form-group">
{{--                                                {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }} <strong>1</strong>--}}
{{--                                                <br>--}}
                                                {{$parents->mobile_1}}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-phone bg-green"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header">{{trans('admin.mob')}}</h3>

                                        <div class="timeline-body">
                                            <div class="form-group">
{{--                                                {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }} <strong>2</strong>--}}
{{--                                                <br>--}}
                                                {{$parents->mobile_2}}
                                            </div>
                                        </div>
                                    </div>
                                </li>


                                    <li>
                                        <i class="fa fa-sort-numeric-asc bg-green"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header">{{trans('admin.number')}}</h3>

                                            <div class="timeline-body">
                                                <div class="form-group">
{{--                                                    {{ Form::label('par_number', trans('admin.number'), ['class' => 'control-label']) }}--}}
{{--                                                    <br>--}}
                                                    {{$parents->number}}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker bg-green"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header">{{trans('admin.source')}}</h3>

                                            <div class="timeline-body">
                                                <div class="form-group">
{{--                                                    {{ Form::label('par_created_from', trans('admin.source'), ['class' => 'control-label']) }}--}}
{{--                                                    <br>--}}
                                                    {{$parents->created_from}}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="fa fa-address-book bg-green"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header">{{trans('admin.job')}}</h3>

                                            <div class="timeline-body">
                                                <div class="form-group">
{{--                                                    {{ Form::label('par_job', trans('admin.job'), ['class' => 'control-label']) }}--}}
{{--                                                    <br>--}}
                                                    {{$parents->job }}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="fa fa-address-book bg-green"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header">{{trans('admin.home_address')}}</h3>

                                            <div class="timeline-body">
                                                <div class="form-group">
{{--                                                    {{ Form::label('par_home_address', trans('admin.home_address'), ['class' => 'control-label']) }}--}}
{{--                                                    <br>--}}
                                                    {{$parents->home_address}}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="fa fa-sticky-note bg-green"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header">{{trans('admin.note')}}</h3>

                                            <div class="timeline-body">
                                                <div class="form-group">
{{--                                                    {{ Form::label('par_note', trans('admin.note'), ['class' => 'control-label']) }}--}}
{{--                                                    <br>--}}
                                                    {{$parents->note}}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <hr>
                                <hr>
                                <hr>
                        </ul>
                        <!-- /.post -->
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </div>
        </div>
    </div>







@endsection
