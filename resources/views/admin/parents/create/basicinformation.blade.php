<div class="tab-pane fade in active" id="busowner">
    <h3>{{trans('admin.basicinformation')}}</h3>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label(trans('admin.arabic_name'), null, ['class' => 'control-label']) }}
                {{ Form::text('name_ar', $related->name_ar , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label(trans('admin.english_name'), null, ['class' => 'control-label']) }}
                {{ Form::text('name_en', $related->name_en  , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label('relation', trans('admin.relation'), ['class' => 'control-label']) }}
                {{ Form::select('relation', \App\Enums\RelationType::toSelectArray(), $related->relation, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label(trans('admin.boys_num'), null, ['class' => 'control-label']) }}
                {{ Form::number('boys_num', $related->boys_num, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label(trans('admin.girls_num'), null, ['class' => 'control-label']) }}
                {{ Form::number('girls_num', $related->girls_num, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label(trans('admin.birth_date'), null, ['class' => 'control-label']) }}
                {{ Form::text('birthdate', $related->birthdate, array_merge(['class' => 'form-control datepicker','style'=>'direction: rtl'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label(trans('admin.birth_place'), null, ['class' => 'control-label']) }}
                {{ Form::text('birthplace', $related->birthplace, array_merge(['class' => 'form-control','style'=>'direction: rtl'])) }}

            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label(trans('admin.phone_1'), null, ['class' => 'control-label']) }}
                {{ Form::text('phone_1', $related->phone_1  , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }} <strong>1</strong>
                {{ Form::text('mobile_1', $related->mobile_1  , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label(trans('admin.mob'), null, ['class' => 'control-label']) }} <strong>2</strong>
                {{ Form::text('mobile_2', $related->mobile_2  , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label('number', trans('admin.id_number'), ['class' => 'control-label']) }}
                {{ Form::text('number', $related->number , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label('created_from', trans('admin.source'), ['class' => 'control-label']) }}
                {{ Form::text('created_from', $related->created_from  , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label('education_level', trans('admin.education_level'), ['class' => 'control-label']) }}
                {{ Form::select('education_level', \App\Enums\EducationLevelType::toSelectArray(), $related->education_level, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label('Specialization', trans('admin.Specialization'), ['class' => 'control-label']) }}
                {{ Form::text('Specialization', $related->specialization, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('email', trans('admin.email'), ['class' => 'control-label']) }}
                {{ Form::text('email', $related->email , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('email', trans('admin.password'), ['class' => 'control-label']) }}
                {{ Form::password('password', ['class' => 'form-control', 'placeholder' => trans('admin.password')]) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('monthly_income', trans('admin.monthly_income'), ['class' => 'control-label']) }}
                {{ Form::text('monthly_income', $related->monthly_income, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('average_income', trans('admin.average_income'), ['class' => 'control-label']) }}
                {{ Form::text('average_income', $related->average_income , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('tax_number', trans('admin.tax_number'), ['class' => 'control-label']) }}
                {{ Form::text('tax_number', $related->tax_number , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('home_address', trans('admin.home_address'), ['class' => 'control-label']) }}
                {{ Form::text('home_address', $related->home_address, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
    </div>

    <h3 class="box-title">{{trans('admin.job_info')}}</h3>
    <br>
    <div class="form-group">
        {{ Form::label('work_status', trans('admin.work_status'), ['class' => 'control-label']) }}
        <br>
        @foreach(\App\Enums\WorkStatusType::toArray() as $index => $work_status)
            {{ Form::label('work_status',trans('admin.'.\App\Enums\WorkStatusType::getKey($work_status)), ['class' => 'control-label']) }}
            <input style="margin-left:30px" type="radio" name="work_status" id="{!! $work_status !!}" value="{{$work_status}}" @if ($related->work_status == $work_status) checked @endif>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('job', trans('admin.job'), ['class' => 'control-label']) }}
                {{ Form::text('job', $related->job , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('job_address', trans('admin.job_address'), ['class' => 'control-label']) }}
                {{ Form::text('job_address', $related->job_address, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label('job_number', trans('admin.job_number'), ['class' => 'control-label']) }}
                {{ Form::text('job_number', $related->job_number , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label(trans('admin.phone_2'), null, ['class' => 'control-label']) }}
                {{ Form::text('phone_2', $related->phone_2  , array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {{ Form::label('labor_sector', trans('admin.labor_sector'), ['class' => 'control-label']) }}
                {{ Form::select('labor_sector', \App\Enums\LaborSectorType::toSelectArray(), $related->labor_sector  , array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
            </div>
        </div>


    </div>
</div>
