
<div class="tab-pane fade in" id="address">
    <h3>{{trans('admin.home_address')}}</h3>
    <div class="row">
        <div class="col-md-6">
            {{ Form::label('city',trans('admin.city') , ['class' => 'control-label']) }}
{{--            {{ Form::select('city_id', $city,$related->city_id, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}--}}
        </div>
        <div class="col-md-6">
            {{ Form::label('state',trans('admin.state') , ['class' => 'control-label']) }}
{{--            {{ Form::select('state_id', $state,$related->state_id, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}--}}
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.street'), null, ['class' => 'control-label']) }}
                {{ Form::text('street', $related->street, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.house_num'), null, ['class' => 'control-label']) }}
                {{ Form::text('house_num', $related->house_num, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.compound_num'), null, ['class' => 'control-label']) }}
                {{ Form::text('compound_num', $related->compound_num, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label(trans('admin.compound_name'), null, ['class' => 'control-label']) }}
                {{ Form::text('compound_name', $related->compound_name, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('near',trans('admin.near'), ['class' => 'control-label']) }}
                {{ Form::textarea('near' ,$related->near, array_merge(['class' => 'form-control'])) }}
            </div>
        </div>
    </div>
</div>
