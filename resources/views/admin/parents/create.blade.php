@extends('admin.index')
@section('title',trans('admin.basicinformation'))
@section('content')


        <div class="box">
            @include('admin.layouts.message')
            <div class="box-header">
                <h3 class="box-title">{{$title}}</h3>
            </div>
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{trans('admin.delete')}}</h4>
                        </div>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['parents.destroy',$related->id],'id'=>'modal-delete']) !!}
                        <div class="modal-body">
                            <p>{{trans('admin.You_Want_You_Sure_Delete_This_Record')}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" value="delete">{{trans('admin.close')}}</button>
                            {!! Form::submit('delete', ['type' => 'submit', 'class' => 'btn btn-danger']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
            <div class="box-body">
                {!! Form::model($related,['method'=>'PUT','route' => ['parents.update',$related->id],'files'=>true]) !!}
                {{--{!! Form::submit( 'Save', ['class' => 'btn btn-default']) !!}--}}
                <div>
                    {!! Form::button( '<i class="fa fa-floppy-o"></i> ' . trans('admin.save'), ['type' => 'submit','class' => 'btn btn-primary', 'name' => 'submitbutton', 'value' => 'save'])!!}
                    <a href="#" class="btn btn-danger"  data-toggle="modal" data-target="#myModal"><i class="fa fa-trash"></i> {{trans('admin.delete')}}</a>
                </div>
                <br>

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#busowner" data-toggle="tab"><i class="fa fa-user"></i> {{trans('admin.basicinformation')}}</a></li>
                        <li><a href="#address" data-toggle="tab"><i class="fa fa-photo"></i> {{trans('admin.home_address')}}</a></li>
                    </ul>
                    <div class="tab-content">

                        @include('admin.parents.create.basicinformation')
                        @include('admin.parents.create.address')


                    </div>

                    <!-- /.tab-content -->
                </div>

                {!! Form::close() !!}

            <!-- /.nav-tabs-custom -->
                {{----}}
                {{--<div class="form-group">--}}
                {{--{{ Form::label('arabic name', null, ['class' => 'control-label']) }}--}}
                {{--{{ Form::text('name_ar', old('name_ar'), array_merge(['class' => 'form-control'])) }}--}}
                {{--</div>--}}
            </div>
        </div>









@endsection
