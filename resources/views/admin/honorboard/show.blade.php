@extends('admin.index')
@section('title',trans('admin.show_honorboard'))
@section('root_link', route('honorboard.index'))
@section('root_name', trans('admin.honorboard'))
@section('content')
    @push('css')
        <style>
            .list-group-item {
                padding: 30px 15px !important;
            }
        </style>

    @endpush

<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{trans('admin.show_honorboard')}}
        </h3></div>
    <div class="box-body">


        <div class="form-group row">
            <div class="col-md-6 ">
                {{ Form::label(trans('admin.name'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{session_lang($content->student->name_en,$content->student->name_ar)}}</div>
            </div>

            <div class="col-md-6 ">
                {{ Form::label(trans('admin.desc'), null, ['class' => 'control-label']) }}
                <div class="form-control">{{$content->description}}</div>
            </div>
        </div>


        <a href="{{aurl('honorboard')}}" class="btn btn-danger">{{trans('admin.back')}}</a>

    </div>
</div>

@endsection
