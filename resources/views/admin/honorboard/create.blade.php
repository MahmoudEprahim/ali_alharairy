@extends('admin.index')
@section('title',trans('admin.add_student_to_honorboard'))
@section('root_link', route('honorboard.index'))
@section('root_name', trans('admin.honorboard'))
@section('content')


@push('js')
    <script>
        function myJsFunc() {
            var i = 0;
            ++i;
            var newInput = $('.new-row').html();
            $('.new-one').append(newInput);
        }
    </script>
@endpush
<div class="box">
    @include('admin.layouts.message')
    <div class="box-header">
        <h3 class="box-title">{{$title}}</h3>
    </div>
    <div class="box-body">
        {!! Form::open(['method'=>'POST','route' => 'honorboard.store','files'=>true]) !!}

            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label('student_id', trans('admin.student_id'), ['class' => 'control-label']) }}
                    {{ Form::select('student_id',$student ,null, array_merge(['class' => 'searchselect form-control','required' => 'required', 'placeholder'=>trans('admin.student_id')])) }}
                </div>

                <div class="col-md-6">
                    {{ Form::label(trans('admin.description'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('description', null, array_merge(['class' => 'form-control', 'required' => 'required', 'style' =>'resize: none'])) }}
                </div>

            </div>


            <div class="clearfix"></div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('honorboard')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
    </div>
    </div>
</div>








@endsection
