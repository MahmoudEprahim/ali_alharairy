@extends('admin.index')
@section('title',trans('admin.edit_honor_board'). ' ' . $honorBoard->student_name)
@section('root_link', route('honorboard.index'))
@section('root_name', trans('admin.honorboard'))
@section('content')
    @hasanyrole('writer|admin')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($honorBoard,['method'=>'PUT','route' => ['honorboard.update',$honorBoard->id],'files'=>true]) !!}

            <div class="form-group row">
                <div class="col-md-6 required">
                    {{ Form::label(trans('admin.description'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('description', $honorBoard->description, array_merge(['class' => 'form-control', 'required' => 'required' , 'style' =>'resize: none'])) }}
                </div>
                <div class="col-md-6">
                    {{ Form::label('student_id', trans('admin.student_id'), ['class' => 'control-label']) }}
                    {{ Form::select('student_id',$student ,$honorBoard->student_id, array_merge(['class' => 'form-control','required' => 'required', 'placeholder'=>trans('admin.select')])) }}
                </div>
            </div>


            <br>
            <div class="clearfix"></div>
            {{Form::submit(trans('admin.update'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('honorboard')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasanyrole







@endsection
