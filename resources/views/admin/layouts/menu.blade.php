
<aside class="main-sidebar" id="mySidenav">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="@if(admin()->user()->image !== null){{asset('storage/'.admin()->user()->image)}}@else {{url('/')}}/adminlte/previewImage.png @endif"  style="height: 45px !important;" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{admin()->user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> {{trans('admin.Online')}}</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <!-- @if(admin()->user()->branches != null) <li class="header" style="color: #ccc !important;"><p>{{session_lang(admin()->user()->branches->name_en,admin()->user()->branches->name_ar)}}</p></li>
            @else
                <li class="header" style="color: #ccc !important;"><p>{{trans('admin.MAIN_NAVIGATION')}}</p></li>
            @endif -->

            <!-- first section control panel-->
            <li class="treeview {{ active_menu('dashboard')[0]  }} {{ active_menu('setting')[0]  }} {{ active_menu('papers_required')[0]  }}{{ active_menu('countries')[0]  }}{{ active_menu('currency')[0]  }}">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>{{trans('admin.Dashboard')}}</span>
                    <span class="pull-right-container"></span>
                </a>
                <ul class="treeview-menu" style="{{ active_menu('dashboard')[1]  }}{{ active_menu('setting')[1]  }}{{ active_menu('papers_required')[1]  }}{{ active_menu('countries')[1]  }}{{ active_menu('currency')[1]  }}">
                    <li class="active"><a href="{{url('admin/dashboard')}}"><i class="fa fa-circle-o"></i> {{trans('admin.Dashboard')}}</a></li>
                    @hasrole('admin')
                    <li><a href="{{url('admin/branches')}}"><i class="fa fa-circle-o"></i> {{trans('admin.branches')}}</a></li>
                    <li><a href="{{route('setting')}}"><i class="fa fa-circle-o"></i> {{trans('admin.Setting')}}</a></li>
                    @endhasrole
                </ul>
            </li>

{{--               <!-- بوابة الثانوية العامة second section-->--}}


                <li class="treeview {{ active_menu('english_gates')[0]  }} {{ active_menu('applicantrequest')[0]  }}  {{ active_menu('relatives')[0]  }} {{ active_menu('questions')[0]  }}">
                    <a href="#">
                        <i class="fa fa-tags"></i> <span>{{trans('admin.secondary_gates')}}</span>
                        <span class="pull-right-container"></span>
                    </a>
                    <ul class="treeview-menu" style=" {{ active_menu('english_gates')[1]  }} {{ active_menu('applicantrequest')[1]  }}  {{ active_menu('relatives')[1]  }} {{ active_menu('questions')[1]  }}">
                        <li class="active"><a href="{{url('/admin/setting/units')}}"><i class="fa fa-circle-o"></i>{{trans('admin.units_information')}} </a></li>
                        <!-- <li class="active"><a href="{{url('/admin/setting/wordlist')}}"><i class="fa fa-circle-o"></i>{{trans('admin.wordlist')}} </a></li> -->
                        <li class="active"><a href="{{url('/admin/setting/content')}}"><i class="fa fa-circle-o"></i>{{trans('admin.content_information')}} </a></li>
                        <!-- <li class="active"><a href="{{url('/admin/englishgrammer')}}"><i class="fa fa-circle-o"></i>{{trans('admin.grammer_information')}} </a></li> -->
                        <li class="active"><a href="{{url('/admin/setting/units/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_units')}} </a></li>
                        <!-- <li class="active"><a href="{{url('/admin/setting/wordlist/create')}}"><i class="fa fa-plus"></i>{{trans('admin.create_word_list')}} </a></li> -->
                        <li class="active"><a href="{{url('/admin/setting/content/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_content')}} </a></li>
                        <!-- <li class="active"><a href="{{url('/admin/englishgrammer/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_grammer_information')}} </a></li> -->
                    </ul>
                </li>


                 <!--############## section threee بوابة اللغة الانجليزية  #########-->

                 <li class="treeview {{ active_menu('english_gates')[0]  }} {{ active_menu('applicantrequest')[0]  }}  {{ active_menu('relatives')[0]  }} {{ active_menu('questions')[0]  }}">
                    <a href="#">
                        <i class="fa fa-tags"></i> <span>{{trans('admin.english_gates')}}</span>
                        <span class="pull-right-container"></span>
                    </a>
                    <ul class="treeview-menu" style=" {{ active_menu('english_gates')[1]  }} {{ active_menu('applicantrequest')[1]  }}  {{ active_menu('relatives')[1]  }} {{ active_menu('questions')[1]  }}">
{{--                        <li class="active"><a href="{{url('/admin/englishlists')}}"><i class="fa fa-circle-o"></i>{{trans('admin.lists_information')}} </a></li>--}}
{{--                        <li class="active"><a href="{{url('/admin/englishlistening')}}"><i class="fa fa-circle-o"></i>{{trans('admin.listening_information')}} </a></li>--}}
{{--                        <li class="active"><a href="{{url('/admin/englishwords')}}"><i class="fa fa-circle-o"></i>{{trans('admin.englishwords_information')}} </a></li>--}}
{{--                        <li class="active"><a href="{{url('/admin/englishgrammer')}}"><i class="fa fa-circle-o"></i>{{trans('admin.grammer_information')}} </a></li>--}}
{{--                        <!-- <li class="active"><a href="{{url('/admin/englishlists/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_lists_information')}} </a></li>--}}
{{--                        <li class="active"><a href="{{url('/admin/englishlistening/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_listening_information')}} </a></li>--}}
{{--                        <li class="active"><a href="{{url('/admin/englishwords/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_words_information')}} </a></li>--}}
{{--                        <li class="active"><a href="{{url('/admin/englishgrammer/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_grammer_information')}} </a></li> -->--}}
                        <li class="active"><a href="{{route('category.index')}}"><i class="fa fa-plus"></i>{{trans('admin.add_content')}} </a></li>
                    </ul>
                </li>






               <li class="treeview {{ active_menu('honorboard')[0]  }} ">

                   <a href="#">
                       <i class="fa fa-tags"></i> <span>{{trans('admin.honorboard')}}</span>
                       <span class="pull-right-container"></span>
                    </a>
                    <ul class="treeview-menu" style=" {{ active_menu('companies')[1]  }} {{ active_menu('applicantrequest')[1]  }}  {{ active_menu('relatives')[1]  }} {{ active_menu('questions')[1]  }}">
                    <li class="active"><a href="{{url('/admin/honorboard')}}"><i class="fa fa-circle-o"></i>{{trans('admin.honorboard_information')}} </a></li>
                    <li class="active"><a href="{{url('/admin/honorboard/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_new_honorboard')}} </a></li>
                    </ul>
                </li>

                 <!--############## section threee  المكتبة   #########-->

                 <li class="treeview {{ active_menu('library')[0]  }}">
                    <a href="#">
                        <i class="fa fa-tags"></i> <span>{{trans('admin.library')}}</span>
                        <span class="pull-right-container"></span>
                    </a>
                    <ul class="treeview-menu" style="{{ active_menu('library')[1]  }}">

                        <!--<li class=""><a href="{{url('/admin/librarylists')}}"><i class="fa fa-circle-o"></i>{{trans('admin.library_lists')}} </a></li>-->
                        <li class=""><a href="{{url('/admin/images')}}"><i class="fa fa-circle-o"></i>{{trans('admin.images')}} </a></li>
                        <li class=""><a href="{{url('/admin/videos')}}"><i class="fa fa-circle-o"></i>{{trans('admin.videos')}} </a></li>
                        <li class=""><a href="{{url('/admin/resources')}}"><i class="fa fa-circle-o"></i>{{trans('admin.references')}} </a></li>
                        <!-- <li class=""><a href="{{url('/admin/librarybooks')}}"><i class="fa fa-circle-o"></i>{{trans('admin.data_books')}} </a></li> -->
                        <!-- <li class=""><a href="{{url('/admin/library/resources')}}"><i class="fa fa-circle-o"></i>{{trans('admin.data_resources')}} </a></li> -->
                        <!--<li class=""><a href="{{url('/admin/librarylists/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_list')}} </a></li>-->
                        <li class=""><a href="{{url('/admin/images/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_images')}} </a></li>
                        <li class=""><a href="{{url('/admin/videos/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_videos')}} </a></li>
                        <li class=""><a href="{{url('/admin/resources/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_resources')}} </a></li>

                        <!-- <li class=""><a href="{{url('/admin/librarybooks/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_content')}} </a></li> -->
                        <!-- <li class=""><a href="{{url('/admin/library/resources/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_resources')}} </a></li> -->

                    </ul>
                </li>



{{--                <li class="treeview {{ active_menu('posts')[0]  }} ">--}}
{{--                    <a href="#">--}}
{{--                        <i class="fa fa-tags"></i> <span>{{trans('admin.test_now')}}</span>--}}
{{--                        <span class="pull-right-container"></span>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu" style=" {{ active_menu('companies')[1]  }} {{ active_menu('applicantrequest')[1]  }}  {{ active_menu('relatives')[1]  }} {{ active_menu('questions')[1]  }}">--}}
{{--                        <li class=""><a href="{{url('/admin/posts')}}"><i class="fa fa-circle-o"></i>{{trans('admin.posts_infomation')}} </a></li>--}}
{{--                        <li class=""><a href="{{url('/admin/posts/create')}}"><i class="fa fa-plus"></i>{{trans('admin.create_new_post')}} </a></li>--}}
{{--                        <li class=""><a href="{{url('/admin/comments')}}"><i class="fa fa-circle-o"></i>{{trans('admin.comments_infomation')}} </a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

                <li class="treeview {{ active_menu('appointments')[0]  }}">
                    <a href="#">
                        <i class="fa fa-tags"></i> <span>{{trans('admin.appointments')}}</span>
                        <span class="pull-right-container"></span>
                    </a>
                    <ul class="treeview-menu" style=" {{ active_menu('appointments')[1]  }}">
                        <li class=""><a href="{{url('/admin/appointments')}}"><i class="fa fa-circle-o"></i>{{trans('admin.appointments')}} </a></li>
                        <li class=""><a href="{{url('/admin/appointments/create')}}"><i class="fa fa-plus"></i>{{trans('admin.create_new_appointments')}} </a></li>

                    </ul>
                </li>
                <li class="treeview {{ active_menu('students')[0]  }}}} ">
                    <a href="#">
                        <i class="fa fa-tags"></i> <span>{{trans('admin.Students')}}</span>
                        <span class="pull-right-container"></span>
                    </a>
                    <ul class="treeview-menu" style=" {{ active_menu('companies')[1]  }} {{ active_menu('applicantrequest')[1]  }}  {{ active_menu('relatives')[1]  }} {{ active_menu('questions')[1]  }}">
                        <li class="active" ><a href="{{url('admin/students')}}"><i class="fa fa-circle-o"></i> {{trans('admin.information_students')}}</a></li>
                        <li><a href="{{url('/admin/students/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_new_students')}} </a></li>
                    </ul>
                </li>
                <li class="treeview {{ active_menu('students')[0]  }}}} ">
                    <a href="#">
                        <i class="fa fa-tags"></i> <span>{{trans('admin.attendance')}}</span>
                        <span class="pull-right-container"></span>
                    </a>
                    <ul class="treeview-menu" style=" {{ active_menu('companies')[1]  }} {{ active_menu('applicantrequest')[1]  }}  {{ active_menu('relatives')[1]  }} {{ active_menu('questions')[1]  }}">
                        <li><a href="{{route('attendance.index')}}"><i class="fa fa-circle-o"></i>{{trans('admin.attendance')}} </a></li>
                        <li><a href="{{route('attendance.create')}}"><i class="fa fa-plus"></i>{{trans('admin.attendance')}} </a></li>
                    </ul>
                </li>
                <li class="treeview  {{ active_menu('relatedness')[0]  }}">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>{{trans('admin.parents')}}</span>
                        <span class="pull-right-container">
                </span>
                    </a>
                    <ul class="treeview-menu" style="{{ active_menu('users')[1]  }}">
                        @hasrole('admin')
                        <li class="active"><a href="{{url('/admin/parents')}}"><i class="fa fa-circle-o"></i>{{trans('admin.parents')}}</a></li>


                        <!--<li><a href="{{url('admin/parents/create')}}"><i class="fa fa-plus"></i> {{trans('admin.add_new_parents')}}</a></li>-->
                        @endhasrole
                    </ul>
                </li>

{{--            0--}}
{{--                @hasanyrole('account|admin')--}}
{{--                <li class="treeview {{ active_menu('cc_accounting_reports')[0]  }}  }}">--}}
{{--                    <a href="#">--}}
{{--                        <i class="fa fa-credit-card"></i> <span>{{trans('admin.cc_accounting_reports')}}</span>--}}
{{--                        <span class="pull-right-container">--}}
{{--                    </span>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu" style="{{ active_menu('cc_accounting_reports')[1]  }} {{ active_menu('cc_accounting_reports')[1]  }} {{ active_menu('cc_accounting_reports')[1]  }}">--}}

{{--                            <li><a href="{{url('/admin/cc/report/checkReports')}}"><i class="fa fa-circle-o"></i>{{trans('admin.disclosure_of_balances_of_accounts_of_cost_centers')}} </a></li>--}}
{{--                            <li><a href="{{url('/admin/cc/report/motioncc')}}"><i class="fa fa-circle-o"></i>{{trans('admin.motion_detection_center_cost')}} </a></li>--}}
{{--                            <li><a href="{{url('/admin/cc/report/ccpublicbalance')}}"><i class="fa fa-circle-o"></i>{{trans('admin.public_balance_cc')}} </a></li>--}}
{{--                            <li><a href="#"><i class="fa fa-circle-o"></i>{{trans('admin.CC_analyais')}} </a></li>--}}
{{--                            <li><a href="#"><i class="fa fa-circle-o"></i>{{trans('admin.CC_daily_limition')}} </a></li>--}}


{{--                    </ul>--}}
{{--                </li>--}}
{{--                @endhasanyrole--}}

{{--                @hasanyrole('account|admin')--}}
{{--                <li class="treeview {{ active_menu('banks')[0]  }}">--}}
{{--                    <a href="#">--}}
{{--                        <i class="fa fa-tasks"></i><span>{{trans('admin.fund_and_banks')}}</span>--}}
{{--                        <span class="pull-right-container">--}}
{{--                 </span>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu" style=" {{ active_menu('banks')[1]  }}">--}}

{{--                        --}}{{-- <li><a href="{{url('/admin/limitations/cred/create')}}"><i class="fa fa-circle-o"></i>{{trans('admin.create_cred_limitations')}} </a></li> --}}
{{--                        <li><a href="{{url('/admin/banks/Receipt/receipts/catch/catch')}}"><i class="fa fa-circle-o"></i>{{trans('admin.catch_receipt')}} </a></li>--}}
{{--                        <li><a href="{{url('/admin/banks/Receipt/receipts/caching/caching')}}"><i class="fa fa-circle-o"></i>{{trans('admin.caching_receipt')}} </a></li>--}}
{{--                        <li><a href="{{url('admin/limitations/notice/noticedebt')}}"><i class="fa fa-circle-o"></i>{{trans('admin.debt_limitations')}}</a></li>--}}
{{--                        --}}{{-- <li><a href="{{url('admin/limitations/notice/noticecred')}}"><i class="fa fa-circle-o"></i>{{trans('admin.cred_limitations')}}</a></li> --}}
{{--                        <li><a href="{{url('/admin/banks/Receipt/receipts/catch/all')}}"><i class="fa fa-plus"></i>{{trans('admin.create_catch_receipt')}}</a></li>--}}
{{--                        <li><a href="{{url('/admin/banks/Receipt/receipts/caching/all')}}"><i class="fa fa-plus"></i>{{trans('admin.create_caching_receipt')}}</a></li>--}}
{{--                        <li><a href="{{url('/admin/limitations/dept/create')}}"><i class="fa fa-plus"></i>{{trans('admin.create_debt_limitations')}} </a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                @endhasanyrole--}}
                @hasanyrole('account|admin')
{{--                <li class="treeview">--}}
{{--                    <a href="#">--}}
{{--                        <i class="fa fa-tasks"></i><span>{{trans('admin.limitations_added')}}</span>--}}
{{--                        <span class="pull-right-container">--}}
{{--                        </span>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li><a href="{{url('admin/limitations')}}"><i class="fa fa-circle-o"></i>{{trans('admin.daily_limitations__edit')}}</a></li>--}}
{{--                        <li><a href="{{url('/admin/limitations/daily/create')}}"><i class="fa fa-plus"></i>{{trans('admin.create_daily_limitations')}} </a></li>--}}
{{--                        <li><a href="{{url('/admin/openingentry/create')}}"><i class="fa fa-plus"></i>{{trans('admin.create_opening_entry')}} </a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
                @endhasanyrole
                @hasanyrole('account|admin')

{{--                <li class="treeview {{ active_menu('main_data')[0]  }} {{ active_menu('setting')[0]  }} {{ active_menu('branches')[0]  }}">--}}
{{--                    <a href="#">--}}
{{--                        <i class="fa fa-dashboard"></i> <span>{{trans('admin.basic_data')}}</span>--}}
{{--                        <span class="pull-right-container"></span>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu" style="{{ active_menu('dashboard')[1]  }}{{ active_menu('setting')[1]  }}{{ active_menu('branches')[1]  }}">--}}
{{--                        @hasanyrole('account|admin')--}}
{{--                        <li class="treeview {{ active_menu('departments')[0]  }}">--}}
{{--                            <a href="#">--}}
{{--                                <i class="fa fa-tasks"></i> <span>{{trans('admin.Departments')}}</span>--}}
{{--                                <span class="pull-right-container"></span>--}}
{{--                            </a>--}}
{{--                            <ul class="treeview-menu" style=" {{ active_menu('departments')[1]  }}">--}}
{{--                                <li class="active"><a href="{{url('/admin/departments')}}"><i class="fa fa-circle-o"></i>{{trans('admin.Departments')}} </a></li>--}}

{{--                                    <li><a href="{{url('/admin/departments/create')}}"><i class="fa fa-plus"></i>{{trans('admin.Create_New_Department')}} </a></li>--}}

{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        @endhasanyrole--}}

{{--                        @hasanyrole('account|admin')--}}
{{--                        <li class="treeview {{ active_menu('cc')[0]  }}">--}}
{{--                            <a href="#">--}}
{{--                                <i class="fa fa-tasks"></i> <span>{{trans('admin.cc')}}</span>--}}
{{--                                <span class="pull-right-container"></span>--}}
{{--                            </a>--}}
{{--                            <ul class="treeview-menu" style=" {{ active_menu('cc')[1]  }}">--}}
{{--                                <li class="active"><a href="{{url('/admin/cc')}}"><i class="fa fa-circle-o"></i>{{trans('admin.cc')}} </a></li>--}}
{{--                                --}}{{----}}{{--<li class="active"><a href="{{url('/admin/cc/reports/report')}}"><i class="fa fa-circle-o"></i>{{trans('admin.Departments_reports')}} </a></li>--}}
{{--                                @can('create')--}}
{{--                                    <li><a href="{{url('/admin/cc/create')}}"><i class="fa fa-plus"></i>{{trans('admin.create_new_cc')}} </a></li>--}}
{{--                                @endcan--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        @endhasanyrole--}}


{{--                        @hasanyrole('account|admin')--}}
{{--                        <li class="treeview {{ active_menu('employees')[0]  }}">--}}
{{--                            <a href="#">--}}
{{--                                <i class="fa fa-tasks"></i> <span>{{trans('admin.employees_accounts')}}</span>--}}
{{--                                <span class="pull-right-container">--}}
{{--                                         </span>--}}
{{--                            </a>--}}
{{--                            <ul class="treeview-menu" style=" {{ active_menu('drivers')[1]  }}">--}}
{{--                                <li class="active"><a href="{{url('/admin/employees')}}"><i class="fa fa-circle-o"></i>{{trans('admin.employees_accounts')}} </a></li>--}}
{{--                                @can('create')--}}
{{--                                    <li><a href="{{url('/admin/employees/create')}}"><i class="fa fa-plus"></i>{{trans('admin.Add_New_employee')}} </a></li>--}}
{{--                                @endcan--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        @endhasanyrole--}}

{{--                    </ul>--}}
{{--                </li>--}}
                @endhasanyrole

                @hasanyrole('account|admin')
{{--                <li class="treeview">--}}
{{--                    <a href="#">--}}
{{--                        <i class="fa fa-tasks"></i><span>{{trans('admin.report_basic_data')}}</span>--}}
{{--                        <span class="pull-right-container">--}}
{{--                        </span>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li><a href="{{url('/admin/departments/reports/report')}}"><i class="fa fa-circle-o"></i>{{trans('admin.Departments_reports')}} </a></li>--}}

{{--                        <li><a target="_blank" href="{{url('/admin/departments/department/print')}}"><i class="fa fa-circle-o"></i>{{trans('admin.department_print')}} </a></li>--}}
{{--                        <li><a  href="#"><i class="fa fa-circle-o"></i>{{trans('admin.CC_print')}} </a></li>--}}
{{--                        <li><a  href="#"><i class="fa fa-circle-o"></i>{{trans('admin.employe_print')}} </a></li>--}}


{{--                    </ul>--}}
{{--                </li>--}}
                @endhasanyrole

                @hasrole('meeting')

                <li class="treeview {{ active_menu('blog')[0]  }}">
                    <a href="#">
                        <i class="fa fa-tags"></i> <span>{{trans('admin.Blogs')}}</span>
                        <span class="pull-right-container"></span>
                    </a>
                    <ul class="treeview-menu" style=" {{ active_menu('blog')[1]  }}">
                        <li class=""><a href="{{url('/admin/breaknews')}}"><i class="fa fa-circle-o"></i>{{trans('admin.breaknews')}} </a></li>
                        <li><a href="{{url('admin/breaknews/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_new_breaknews')}} </a></li>
                    </ul>
                </li>

                <li class="treeview {{ active_menu('blog')[0]  }}">
                    <a href="#">
                        <i class="fa fa-tags"></i> <span>{{trans('admin.exams')}}</span>
                        <span class="pull-right-container"></span>
                    </a>
                    <ul class="treeview-menu" style=" {{ active_menu('blog')[1]  }}">
                        <li class=""><a href="{{url('/admin/exams')}}"><i class="fa fa-circle-o"></i>{{trans('admin.exams')}} </a></li>
                        <li><a href="{{url('admin/exams/create')}}"><i class="fa fa-plus"></i>{{trans('admin.add_new_exam')}} </a></li>
                    </ul>
                </li>
                


                @endhasrole
                


                {{--            @endhasanyrole--}}
{{--                <li class="treeview {{ active_menu('main_data')[0]  }} {{ active_menu('setting')[0]  }} {{ active_menu('branches')[0]  }}">--}}
{{--                    <a href="#">--}}
{{--                        <i class="fa fa-dashboard"></i> <span>{{trans('admin.basic_data')}}</span>--}}
{{--                        <span class="pull-right-container"></span>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu" style="{{ active_menu('dashboard')[1]  }}{{ active_menu('setting')[1]  }}{{ active_menu('branches')[1]  }}">--}}


{{--                        --}}{{-- دليل الحسابات --}}
{{--                        @hasanyrole('account|admin')--}}
{{--                        <li class="treeview {{ active_menu('departments')[0]  }}">--}}
{{--                            <a href="#">--}}
{{--                                <i class="fa fa-tasks"></i> <span>{{trans('admin.Departments')}}</span>--}}
{{--                                <span class="pull-right-container"></span>--}}
{{--                            </a>--}}
{{--                            <ul class="treeview-menu" style=" {{ active_menu('departments')[1]  }}">--}}
{{--                                <li class="active"><a href="{{url('/admin/departments')}}"><i class="fa fa-circle-o"></i>{{trans('admin.Departments')}} </a></li>--}}
{{--                                @can('create')--}}
{{--                                    <li><a href="{{url('/admin/departments/create')}}"><i class="fa fa-plus"></i>{{trans('admin.Create_New_Department')}} </a></li>--}}
{{--                                @endcan--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        @endhasanyrole--}}

{{--                        @hasanyrole('account|admin')--}}
{{--                        <li class="treeview {{ active_menu('cc')[0]  }}">--}}
{{--                            <a href="#">--}}
{{--                                <i class="fa fa-tasks"></i> <span>{{trans('admin.cc')}}</span>--}}
{{--                                <span class="pull-right-container"></span>--}}
{{--                            </a>--}}
{{--                            <ul class="treeview-menu" style=" {{ active_menu('cc')[1]  }}">--}}
{{--                                <li class="active"><a href="{{url('/admin/cc')}}"><i class="fa fa-circle-o"></i>{{trans('admin.cc')}} </a></li>--}}
{{--                                --}}{{--<li class="active"><a href="{{url('/admin/cc/reports/report')}}"><i class="fa fa-circle-o"></i>{{trans('admin.Departments_reports')}} </a></li>--}}
{{--                                @can('create')--}}
{{--                                    <li><a href="{{url('/admin/cc/create')}}"><i class="fa fa-plus"></i>{{trans('admin.create_new_cc')}} </a></li>--}}
{{--                                @endcan--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        @endhasanyrole--}}
{{--                        --}}{{-- نهاية مراكز التكلفه --}}

{{--                        @hasanyrole('account|admin')--}}
{{--                        <li class="treeview {{ active_menu('employees')[0]  }}">--}}
{{--                            <a href="#">--}}
{{--                                <i class="fa fa-tasks"></i> <span>{{trans('admin.employees_accounts')}}</span>--}}
{{--                                <span class="pull-right-container">--}}
{{--                                         </span>--}}
{{--                            </a>--}}
{{--                            <ul class="treeview-menu" style=" {{ active_menu('drivers')[1]  }}">--}}
{{--                                <li class="active"><a href="{{url('/admin/employees')}}"><i class="fa fa-circle-o"></i>{{trans('admin.employees_accounts')}} </a></li>--}}
{{--                                @can('create')--}}
{{--                                    <li><a href="{{url('/admin/employees/create')}}"><i class="fa fa-plus"></i>{{trans('admin.Add_New_employee')}} </a></li>--}}
{{--                                @endcan--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        @endhasanyrole--}}

{{--                    </ul>--}}
{{--                </li>--}}

                @hasrole('admin')
                <li class="treeview {{ active_menu('admins')[0]  }} {{ active_menu('permissions')[0]  }} {{ active_menu('roles')[0]  }}">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>{{trans('admin.Admin_Account')}}</span>
                        <span class="pull-right-container">
            </span>
                    </a>
                    <ul class="treeview-menu" style="{{ active_menu('admins')[1]  }} {{ active_menu('permissions')[1]  }} {{ active_menu('roles')[1]  }}">
                        <li class="active"><a href="{{url('/admin/admins')}}"><i class="fa fa-circle-o"></i>{{trans('admin.Admin_Account')}} </a></li>
                        <li><a href="{{url('/admin/permissions')}}"><i class="fa fa-circle-o"></i>{{trans('admin.permission')}} </a></li>
                        <li><a href="{{url('/admin/roles')}}"><i class="fa fa-circle-o"></i>{{trans('admin.Roles')}} </a></li>
                    </ul>
                </li>
                @endhasrole
{{--            1--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
