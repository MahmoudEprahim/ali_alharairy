@extends('admin.index')
@section('title',trans('admin.show_department'))
@section('content')
    @hasrole('writer')
    @push('js')
        <script>
            function calculatedept() {
                var creditor = $('input[name=\'creditor\']').val(),
                    debtor = $('input[name=\'debtor\']').val(),
                    minus = debtor - creditor;
                $('#subtract').text(minus);

            }
        </script>



    @endpush
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($department,['method'=>'PUT','route' => ['departments.update',$department->id]]) !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-md-6">

                            <strong>
                                {{trans('admin.arabic_name')}}
                            </strong>
                            {{session_lang($department->dep_name_ar,$department->dep_name_ar)}}
                        </div>

                        <div class="col-md-6">
                            <strong>   {{'نوع الحساب'}}   </strong>:

                            {{session_lang($department->operations->name_en,$department->operations->name_ar)}}

                        </div>
                    </div>
                    <div class="form-group row">

                        <div class="col-md-6">
                            <strong>   {{'التصنيف بالحسابات الختاميه'}}   </strong>:
                                {{  \App\Enums\dataLinks\IncomeListType::getDescription($department->budget)}}
                        </div>

                        <div class="col-md-4">

                                              <strong>{{ trans('admin.department_type') }}</strong>   :
                                                {{ \App\Enums\dataLinks\TypeAccountType::getDescription($department->type) }}

                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            @if($department->cc_type == 0)
                                <div>
                                    <strong>   {{trans('admin.with_cc')}}   </strong>
                                    :

                                    {{ Form::checkbox('cc_type',$department->cc_type,$department->cc_type,['checked','disabled']) }}
                                </div>
                            @else
                                <div >
                                    <strong>   {{trans('admin.with_cc')}}   </strong>
                                    :

                                    {{ Form::checkbox('cc_type',$department->cc_type,$department->cc_type,['checked','disabled']) }}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-6">

                                <strong> {{ trans('admin.category') }}</strong>:
                                {{ \App\Enums\dataLinks\CategoryAccountType::getDescription($department->category)}}

                        </div>
                    </div>
                        <div class="form-group row">
                        <div class="col-md-6">
                            <strong> {{ trans('admin.first_date_debtor') }}</strong>:

                            @if($department->type =='0')

                                {{ totaldepartment($department->id,'debtor') }}
                            @else

                                {{ $department->debtor }}

                            @endif
                        </div>
                        <div class="col-md-6">
                            <strong> {{ trans('admin.first_date_creditor') }}</strong>:

                            @if($department->type =='0')
                                {{ totaldepartment($department->id,'creditor') }}
                            @else
                                {{$department->creaditor}}
                            @endif
                        </div>

                    </div>

                    <div class="hidden">
                        {{$balance = 0}}
                        {{$dataDebtor = 0}}
                        {{$dataCredit = 0}}
                        {{$allcreditor = 0}}
                        {{$alldebtor = 0}}
                        {{$balance1 = 0}}
                        {{$balance2 = 0}}
                        {{$balance3 = 0}}
                        {{$balance4 = 0}}
                        {{$balance5 = 0}}
                        {{$estimated = 0}}
                        {{$balance5  = 0}}
                        {{$balance6 = 0}}
                        {{$balance7 = 0}}
                        {{$balance8 = 0}}

                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th>الشهر</th>
                                <th>الحركه مدين</th>
                                <th>الحركه دائن</th>

                                <th>رصيد حالي</th>
                                <th>رصيد تقديري</th>
                                @for($date = \Carbon\Carbon::today()->format('Y')-1;$date > \Carbon\Carbon::today()->format('Y')-6;$date--)
                                    <th>رصيد {{$date}}</th>
                                @endfor
                            </tr>

                            @for($i = 1;$i < 13;$i++)
                                <tr>
                                    <td>{{\App\Enums\dataLinks\MonthType::getDescription($i)}}</td>
                                    <td>



                                        @if($department->type==  "0")
                                            {{$debtor = departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.\Carbon\Carbon::today()->format('Y')) ) , date("Y-m-t", strtotime('1-'.$i.'-'.\Carbon\Carbon::today()->format('Y')) ) ,'debtor','>=')}}

                                        @else

                                            {{$debtor = departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.\Carbon\Carbon::today()->format('Y')) ) , date("Y-m-t", strtotime('1-'.$i.'-'.\Carbon\Carbon::today()->format('Y')) ) ,'debtor','>=')}}

                                        @endif
                                        <div class="hidden">{{$alldebtor += $debtor}}</div>

                                    </td>
                                    <td>

                                        @if($department->type== '0')
                                            {{ $creditor= departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.\Carbon\Carbon::today()->format('Y')) ) , date("Y-m-t", strtotime('1-'.$i.'-'.\Carbon\Carbon::today()->format('Y')) ) ,'creditor','>=')}}
                                        @else

                                            {{$creditor =departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.\Carbon\Carbon::today()->format('Y')) ) , date("Y-m-t", strtotime('1-'.$i.'-'.\Carbon\Carbon::today()->format('Y')) ) ,'creditor','>=')}}

                                        @endif
                                        <div class="hidden">{{$allcreditor += $creditor}}</div>
                                    </td>

                                    <td>

                                        {{ $debtor - $creditor }}
                                        <div class="hidden">
                                            @if($department->type == '1')
                                                {{$balance =($department->debtor + $alldebtor)  - ($department->creditor + $allcreditor) }}
                                            @else
                                                {{$balance =(totaldepartment($department->id,'debtor') + $alldebtor)  - (totaldepartment($department->id,'creditor') + $allcreditor) }}
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        {{$estimated }}

                                        {{--                                        {{getpjitmmsfl($department->id,$i,\Carbon\Carbon::today()->format('Y'))['estimated_balance']}}--}}
                                        {{--                                        <div class="hidden">{{$estimated += getpjitmmsfl($department->id,$i,\Carbon\Carbon::today()->format('Y'))['estimated_balance']}}</div>--}}
                                    </td>
                                    <td>

                                        <?php
                                        $lastyear = \Carbon\Carbon::today()->format('Y')-1;
                                        ?>

                                        <div class="hidden">
                                            @if($department->type== '0')
                                                {{ $creditor8= departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear)) ,'creditor','>=')}}
                                            @else

                                                {{$creditor8 =departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear)),'creditor','>=')}}

                                            @endif

                                            @if($department->type==  "0")
                                                {{$debtor8 = departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear) ) ,'debtor','>=')}}

                                            @else

                                                {{$debtor8 = departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear) ) ,'debtor','>=')}}

                                            @endif
                                        </div>

                                        <div class="hidden">{{$balance8 +=$debtor8 -$creditor8 }}</div>
                                        {{$debtor8 -$creditor8}}
                                    </td>
                                    <td>
                                        <?php
                                        $lastyear2 = \Carbon\Carbon::today()->format('Y')-2;
                                        ?>

                                        <div class="hidden">
                                            @if($department->type== '0')
                                                {{ $creditor7= departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear2) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear2)) ,'creditor','>=')}}
                                            @else

                                                {{$creditor7 =departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear2) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear2)) ,'creditor','>=')}}

                                            @endif
                                            @if($department->type==  "0")
                                                {{$debtor7 = departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear2) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear2) ) ,'debtor','>=')}}

                                            @else

                                                {{$debtor7 = departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear2) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear2) ) ,'debtor','>=')}}

                                            @endif

                                        </div>
                                        <div class="hidden">  {{$balance7 +=  $debtor7 - $creditor7 }}</div>
                                        <div>  {{ $debtor7 - $creditor7 }}</div>
                                    </td>
                                    <td>
                                        <?php
                                        $lastyear3 = \Carbon\Carbon::today()->format('Y')-3;
                                        ?>

                                        <div class="hidden">
                                            @if($department->type== '0')
                                                {{ $creditor6= departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear3) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear3)) ,'creditor','>=')}}
                                            @else

                                                {{$creditor6 =departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear3) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear3)) ,'creditor','>=')}}

                                            @endif
                                            @if($department->type==  "0")
                                                {{$debtor6 = departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear3) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear3) ) ,'debtor','>=')}}

                                            @else

                                                {{$debtor6 = departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear3) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear3) ) ,'debtor','>=')}}

                                            @endif

                                        </div>
                                        <div class='hidden'> {{ $balance6 += $debtor6-$creditor6 }}</div>

                                        <div>  {{$debtor6-$creditor6}}</div>
                                    </td>
                                    <td>
                                        <?php
                                        $lastyear4 = \Carbon\Carbon::today()->format('Y')-4;
                                        ?>

                                        <div class="hidden">
                                            @if($department->type== '0')
                                                {{ $creditor5= departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear4) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear4)) ,'creditor','>=')}}
                                            @else

                                                {{$creditor5 =departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear4) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear4)) ,'creditor','>=')}}

                                            @endif
                                            @if($department->type==  "0")
                                                {{$debtor5 = departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear4) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear4) ) ,'debtor','>=')}}

                                            @else

                                                {{$debtor5 = departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear4) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear4) ) ,'debtor','>=')}}

                                            @endif

                                        </div>

                                        <div class='hidden'>  {{ $balance5 +=  $debtor5 - $creditor5}}</div>
                                        <div>  {{$debtor5 - $creditor5}}</div>

                                    </td>
                                    <td>
                                        <?php
                                        $lastyear5 = \Carbon\Carbon::today()->format('Y')-5;
                                        ?>

                                        <div class="hidden">
                                            @if($department->type== '0')
                                                {{ $creditor6= departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear4) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear4)) ,'creditor','>=')}}
                                            @else

                                                {{$creditor6 =departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear4) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear4)) ,'creditor','>=')}}

                                            @endif
                                            @if($department->type==  "0")
                                                {{$debtor6 = departmentsum($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear4) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear4) ) ,'debtor','>=')}}

                                            @else

                                                {{$debtor6 = departmentsum3($department->id,date("Y-m-1", strtotime('1-'.$i.'-'.$lastyear4) ) , date("Y-m-t", strtotime('1-'.$i.'-'.$lastyear4) ) ,'debtor','>=')}}

                                            @endif

                                        </div>

                                        <div class='hidden'>  {{ $balance6 += $debtor5 -$creditor5}}</div>
                                        <div>  {{$debtor5 -$creditor5}}</div>

                                    </td>

                                </tr>
                            @endfor
                            <tr>
                                <td>الاجمالي</td>
                                @if($department->type == '0')
                                    <td>{{$alldebtor +(totaldepartment($department->id,'debtor'))}}</td>
                                    <td>{{$allcreditor + (totaldepartment($department->id,'creditor'))}} </td>
                                @else
                                    <td>{{$alldebtor + $department->debtor}}</td>
                                    <td>{{$allcreditor + $department->creditor}}</td>
                                @endif


                                <td class="balance">{{$balance}}</td>
                                <td>{{$estimated}}</td>
                                <td>{{$balance8}}</td>
                                <td>{{$balance7}}</td>
                                <td>{{$balance6}}</td>
                                <td>{{$balance5}}</td>
                                <td>{{$balance6}}</td>

                            </tr>
                        </table>
                    </div>
                </div>

            </div>
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('departments')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

        @endhasrole

@endsection
