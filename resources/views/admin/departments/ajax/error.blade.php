
@if(count($receiptsType) > 0)

<table style="border:none" class="table table-bordered table-striped table-hover" data-serial="">
    <tr>
        <th colspan="3">رقم السند</th>
        <th> الاسم بالسند </th>
        <th colspan="2"> الاسم الحساب</th>
        <th  style="vertical-align: middle;">التاريخ الانشاء</th>
        <th colspan="2" style="vertical-align: middle;">العرض</th>

    </tr>
    <?php
    //            $branches = App\Branches::where('id',$receiptsType[0]->receipts->branche_id)->first();

    //            $limitationreceipts = App\limitationreceipts::where('id',$receiptsType[0]->receipts->receiptsType_id)->first();
    $r = count($receiptsType)-1;
    ?>

    @for(;$r>=0; $r--)
        @if($receiptsType[$r]->departments->dep_name_ar != $receiptsType[$r]->name_ar)
        <tr>



            <td colspan="3">{{$receiptsType[$r]->id}}</td>
            <td>{{$receiptsType[$r]->name_ar}}</td>
            <td colspan="2">{{$receiptsType[$r]->departments->dep_name_ar}}</td>
            <td>{{$receiptsType[$r]->created_at->format('Y-m-d')}}</td>
            <td><a href="{{url('admin/banks/Receipt/receipts/'.$receiptsType[$r]->id.'/edit')}}" class="btn btn-success" target="_blanck"> عرض </a></td>

        </tr>
        @endif

@endfor
</table>

@endif




@if(count($limitations_1) > 0)

<table style="border:none" class="table table-bordered table-striped table-hover" data-serial="">
    <tr>
        <th colspan="3">رقم القيد</th>
        <th>المدين </th>
        <th colspan="2"> الدائن</th>
        <th colspan="2"> وقت الانشاء</th>
        <th colspan="2" style="vertical-align: middle;">العرض</th>

    </tr>
    <?php
//    $branches = App\Branches::where('id',$limitations->limitations->branche_id)->first();

//    $limitationReceipts = App\limitationReceipts::where('id',$limitations->limitations->limitationsType_id)->first();
    $r = count($limitations_1)-1;
    ?>

   @for(;$r>=0; $r--)


@foreach($limitations_1[$r]->limitationsData as $one)

            <tr>


            <td colspan="3">{{$one->id}}</td>
                <td colspan="3">{{$one->debtor}}</td>
                <td colspan="3">{{$one->creditor}}</td>



            <td colspan="2">{{$one->created_at}}</td>

        <td colspan="2"><a href="{{url('admin/limitations/'.$limitations_1[$r]->id.'/edit')}}" class="btn btn-success" target="_blanck"> عرض </a></td>


    </tr>

    @endforeach


        @endfor
</table>
@endif



{{--tree_id -== null--}}
@if(count($limitations_2) > 0)
    <?php
    //    $branches = App\Branches::where('id',$limitations->limitations->branche_id)->first();

    //    $limitationReceipts = App\limitationReceipts::where('id',$limitations->limitations->limitationsType_id)->first();
    $r = count($limitations_2)-1;

    ?>




    <table style="border:none" class="table table-bordered table-striped table-hover" data-serial="">
        <tr>
            <th colspan="2">رقم القيد</th>
            <th>اسم الحساب	 </th>
            <th colspan="3"> الاسم بالحسابات	</th>
            <th colspan="2"> مدين</th>
            <th colspan="2" style="vertical-align: middle;">دائن</th>
            <th colspan="2" style="vertical-align: middle;">وقت الانشاء</th>
        </tr>



        @for(;$r>=0; $r--)




                    <tr>


                            <tr>

                                <td colspan="2">{{$limitations_2[$r]->limitations->limitationId}}</td>

                                <td colspan="2">{{$limitations_2[$r]->name_ar}}</td>
                @if($limitations_2[$r]->tree_id != null)
                                <td colspan="2">{{$limitations_2[$r]->departments->dep_name_ar}}</td>

                    @else
                    <td colspan="2">{{null}}</td>
                @endif
                <td colspan="2">{{$limitations_2[$r]->debtor}}</td>

                                <td colspan="2">{{$limitations_2[$r]->creditor}}</td>


                                <td>{{$limitations_2[$r]->created_at}}</td>



                                <td colspan="2"><a href="{{url('admin/limitations/'.$limitations_2[$r]->limitations->id.'/edit')}}" class="btn btn-success" target="_blanck"> عرض </a></td>


                            </tr>



                    </tr>



        @endfor
    </table>
@endif


