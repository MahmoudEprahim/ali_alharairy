@extends('admin.index')
@section('title',trans('admin.edit_branches'))
@section('root_link', route('branches.index'))
@section('root_name', trans('admin.branches'))
@section('content')
@hasrole('writer')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
            <span style="position: absolute;top: 10px;left: 20px;font-size: 30px;">رقم الفرع : {{$numberOfBranches}}</span>
        </div>
        <div class="box-body">
            {!! Form::model($branches,['method'=>'PUT','route' => ['branches.update',$branches->id]]) !!}
            
            <div class="form-group">
                {{ Form::label(trans('admin.name'), null, ['class' => 'control-label']) }}
                {{ Form::text('name_en', $branches->name_en, array_merge(['class' => 'form-control'])) }}
            </div>
            <div class="form-group">
                {{ Form::label(trans('admin.location'), null, ['class' => 'control-label']) }}
                {{ Form::text('location', $branches->location, array_merge(['class' => 'form-control'])) }}
            </div>
            
            
            
            
            <!--<div class="form-group">-->
            <!--    {{ Form::label(trans('admin.arabic_name'), null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('name_ar', $branches->name_ar, array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->
            <!--<div class="form-group">-->
            <!--    {{ Form::label(trans('admin.english_name'), null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('name_en', $branches->name_en, array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->
            <!--<div class="form-group">-->
            <!--    {{ Form::label(trans('admin.addriss'), null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('address', $branches->address, array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->
            <!--{{-- <div class="form-group">-->
            <!--    {{ Form::label(trans('admin.type'), null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::select('type', \App\Enums\BranchType::toSelectArray(),$branches->type, array_merge(['class' => 'form-control type','placeholder'=>trans('admin.select')])) }}-->
            <!--</div>-->
            <!--<div class="form-group">-->
            <!--    {{ Form::label(trans('admin.mini_charge'), null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::number('mini_charge', $branches->mini_charge, array_merge(['class' => 'form-control'])) }}-->
            <!--</div> --}}-->
            <!--<div class="form-group col-md-3">-->
            <!--    {{ Form::label('حساب الصندوق', null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('box_account', $branches->box_account, array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->
            <!--<div class="form-group col-md-3">-->
            <!--    {{ Form::label('ذمم العملاء', null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('customer_receivables', $branches->customer_receivables, array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->
            <!--<div class="form-group col-md-3">-->
            <!--    {{ Form::label('ايرادات مستحقة', null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('accrued_income', $branches->accrued_income, array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->

            <!--<div class="form-group col-md-3">-->
            <!--    {{ Form::label('ايرادات الاتعاب', null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('fee_income', $branches->fee_income, array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('branches')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
@else
    <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

    @endhasrole







@endsection
