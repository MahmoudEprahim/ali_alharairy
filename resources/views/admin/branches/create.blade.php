@extends('admin.index')
@section('title', trans('admin.add_new_branches'))
@section('root_link', route('branches.index'))
@section('root_name', trans('admin.branches'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <!--<div class="box-header">-->
        <!--    <h3 class="box-title">{{$title}}</h3>-->
        <!--    <span style="position: absolute;top: 10px;left: 20px;font-size: 30px;">رقم الفرع : {{$numberOfBranches}}</span>-->
        <!--</div>-->
        <div class="box-body">
            {!! Form::open(['method'=>'POST','route' => 'branches.store']) !!}
             <div class="form-group">
                {{ Form::label(trans('admin.name'), null, ['class' => 'control-label']) }}
                {{ Form::text('name_en', old('name_en'), array_merge(['class' => 'form-control'])) }}
            </div>
             <div class="form-group">
                {{ Form::label(trans('admin.location'), null, ['class' => 'control-label']) }}
                {{ Form::text('location', old('location'), array_merge(['class' => 'form-control'])) }}
            </div>
            <!--<div class="form-group">-->
            <!--    {{ Form::label(trans('admin.addriss'), null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('address', old('address'), array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->
            <!--{{-- <div class="form-group">-->
            <!--    {{ Form::label(trans('admin.type'), null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::select('type', \App\Enums\BranchType::toSelectArray(),null, array_merge(['class' => 'form-control type','placeholder'=>trans('admin.select')])) }}-->
            <!--</div>-->
            <!--<div class="form-group">-->
            <!--    {{ Form::label(trans('admin.mini_charge'), null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::number('mini_charge', old('name_en'), array_merge(['class' => 'form-control'])) }}-->
            <!--</div> --}}-->
            <!--<div class="form-group col-md-3">-->
            <!--    {{ Form::label('حساب الصندوق', null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('box_account', old('box_account'), array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->
            <!--<div class="form-group col-md-3">-->
            <!--    {{ Form::label('ذمم العملاء', null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('customer_receivables', old('customer_receivables'), array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->
            <!--<div class="form-group col-md-3">-->
            <!--    {{ Form::label('ايرادات مستحقة', null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('accrued_income', old('accrued_income'), array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->

            <!--<div class="form-group col-md-3">-->
            <!--    {{ Form::label('ايرادات الاتعاب', null, ['class' => 'control-label']) }}-->
            <!--    {{ Form::text('fee_income', old('fee_income'), array_merge(['class' => 'form-control'])) }}-->
            <!--</div>-->
            {{Form::submit(trans('admin.save'),['class'=>'btn btn-primary'])}}
            <a href="{{aurl('branches')}}" class="btn btn-danger">{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
    
@endsection
