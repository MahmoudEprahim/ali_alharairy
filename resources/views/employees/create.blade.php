@extends('admin.index')
@section('title',trans('admin.Create_new_students'))
@section('content')
    @push('js')

        <script>
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                rtl: true,
                language: '{{session('lang')}}',
                inline:true,
                minDate: 0,
                autoclose:true,
                minDateTime: dateToday

            });
        </script>
    @endpush

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::open(['method'=>'POST','route' => 'students.store','files' =>true,'enctype'=>'multipart/form-data']) !!}

            <div class="form-group row">
            <div class="col-md-4">
                    {{ Form::label('Brn_No', trans('admin.Brn_No'), ['class' => 'control-label']) }}
                    {{ Form::select('Brn_No',$branche ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.Brn_No')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.Cstm_NmAr'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Cstm_NmAr', old('Cstm_NmAr'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Cstm_NmAr')])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.Cstm_NmEn'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Cstm_NmEn', old('Cstm_NmEn'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Cstm_NmEn')])) }}
                </div>

            </div>

            <div class="form-group row">
            <div class="col-md-4">
                    {{ Form::label(trans('admin.Tr_Dt'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Tr_Dt', old('Tr_Dt'), array_merge(['class' => 'form-control datepicker','placeholder'=>trans('admin.Tr_Dt'),'autocomplete'=>'off'])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.Cstm_Adr'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Cstm_Adr', old('Cstm_Adr'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Cstm_Adr')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.Fees'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Fees', old('Fees'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Fees')])) }}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label('Sex_Ty', trans('admin.Sex_Ty'), ['class' => 'control-label']) }}
                    {{ Form::select('Sex_Ty',\App\Enums\GenderType::toSelectArray() ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.Mobile1'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Mobile1', old('Mobile1'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Mobile1')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.Mobile2'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Mobile2', old('Mobile2'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Mobile2')])) }}
                </div>

            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label('Parnt_Name', trans('admin.Parnt_Name'), ['class' => 'control-label']) }}
                    {{ Form::text('Parnt_Name', old('Parnt_Name'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Parnt_Name')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label('Parnt_Job', trans('admin.Parnt_Job'), ['class' => 'control-label']) }}
                    {{ Form::text('Parnt_Job', old('Parnt_Job'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Parnt_Job')])) }}
                </div>

                <div class="col-md-4">
                {{ Form::label(trans('admin.Cstm_Img') ,'null', ['class' => 'control-label']) }}
                {{ Form::file('Cstm_Img', array_merge(['class' => 'form-control Cstm_Img'])) }}
                </div>

            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    {{ Form::label(trans('admin.SchL_Name'), null, ['class'=>'control-label']) }}
                    {{ Form::text('SchL_Name',old('SchL_Name'), array_merge(['class'=>'form-control','placeholder'=>trans('admin.SchL_Name')])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.Nof_Brothr'), null,['class'=>'control-label'])}}
                    {{ Form::text('Nof_Brothr',old('Nof_Brothr'), array_merge(['class'=>'form-control','placeholder'=>trans('admin.Nof_Brothr')])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.Disc_Prct'), null,['class'=>'control-label'])}}
                    {{ Form::text('Disc_Prct',old('Disc_Prct'), array_merge(['class'=>'form-control','placeholder'=>trans('admin.Fees')])) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label(trans('admin.Disc_Val'), null,['class'=>'control-label'])}}
                    {{Form::text('Disc_Val',old('Disc_Val'),array_merge(['class'=>'form-control','placeholder'=>trans('admin.Disc_Val')])) }}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label(trans('admin.TotFees'),null, ['class'=>'control-label'])}}
                    {{ Form::text('TotFees',old('TotFees'),array_merge(['class'=>'form-control','placeholder'=>trans('admin.TotFees')])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.Strt_Dt'),null, ['class'=>'control-label'])}}
                    {{ Form::text('Strt_Dt',old('Strt_Dt'),array_merge(['class'=>'form-control datepicker','placeholder'=>trans('admin.Strt_Dt')])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.Grad_No'),null, ['class'=>'control-label'])}}
                    {{ Form::text('Grad_No',old('Grad_No'),array_merge(['class'=>'form-control','placeholder'=>trans('admin.Grad_No')])) }}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label(trans('admin.Class_No'),null, ['class'=>'control-label'])}}
                    {{ Form::text('Class_No',old('Class_No'),array_merge(['class'=>'form-control','placeholder'=>trans('admin.Class_No')])) }}
                </div>
                <div class="col-md-4">
                {{ Form::label(trans('admin.Cstm_Catg'),null,['class'=>'control-label'])}}
                {{Form::text('Cstm_Catg',old('Cstm_Catg'),array_merge(['class'=>'form-control','placeholder'=>trans('admin.Cstm_Catg')])) }}
                </div>
                <div class="col-md-4">
                {{ Form::label(trans('admin.Cstm_Activ'),null,['class'=>'control-label'])}}
                {{Form::text('Cstm_Activ',old('Cstm_Activ'),array_merge(['class'=>'form-control','placeholder'=>trans('admin.Cstm_Activ')])) }}
                </div>
            </div>



            {{Form::submit(trans('admin.create'),['class'=>'btn btn-primary'])}}
            {!! Form::close() !!}
        </div>
    </div>







@endsection
