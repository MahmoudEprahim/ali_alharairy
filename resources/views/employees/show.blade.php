@extends('admin.index')
@section('title', trans('admin.show_profile_to') .session_lang($student->name_en,$student->name_ar))
@section('content')
    @push('css')
        <style>
            .list-group-item {
                padding: 30px 15px !important;
            }
        </style>

    @endpush


    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <h3 class="profile-username text-center">{{session_lang($student->name_en,$student->name_ar)}}</h3>
</div>
</div>

                    
@endsection