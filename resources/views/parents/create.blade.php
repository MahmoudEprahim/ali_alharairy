@extends('admin.index')
@section('title',trans('admin.Create_new_parents'))
@section('content')
    @push('js')

        <script>
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                rtl: true,
                language: '{{session('lang')}}',
                inline:true,
                minDate: 0,
                autoclose:true,
                minDateTime: dateToday

            });
        </script>
    @endpush

        <div class="box">
            @include('admin.layouts.message')
            <div class="box-header">
                <h3 class="box-title">{{$title}}</h3>
            </div>
            <div class="box-body">
                {!! Form::open(['method'=>'POST','route' => 'parents.store','files' =>true,'enctype'=>'multipart/form-data']) !!}

                <div class="form-group row">
                    <div class="col-md-4">
                        {{ Form::label('branch_id', trans('admin.Brn_No'), ['class' => 'control-label']) }}
                        {{ Form::select('branch_id',$branche ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.Brn_No')])) }}
                    </div>

                    <div class="col-md-4">
                        {{ Form::label(trans('admin.name_ar'), null, ['class' => 'control-label']) }}
                        {{ Form::text('name_ar', old('name_ar'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.name_ar')])) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::label(trans('admin.name_en'), null, ['class' => 'control-label']) }}
                        {{ Form::text('name_en', old('name_en'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.name_en')])) }}
                    </div>

                </div>

                <div class="form-group row">


                    <div class="col-md-6">
                        {{ Form::label(trans('admin.Cstm_Adr'), null, ['class' => 'control-label']) }}
                        {{ Form::text('Parnt_address', old('Parnt_address'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Cstm_Adr')])) }}
                    </div>

                    <div class="col-md-6">
                        {{ Form::label(trans('admin.Job'), null, ['class' => 'control-label']) }}
                        {{ Form::text('Parnt_Job', old('Parnt_Job'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Job')])) }}
                    </div>
                </div>
                <div class="form-group row">


                    <div class="col-md-6">
                        {{ Form::label(trans('admin.Mobile1'), null, ['class' => 'control-label']) }}
                        {{ Form::text('Mobile1', old('Mobile1'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Mobile1')])) }}
                    </div>

                    <div class="col-md-6">
                        {{ Form::label(trans('admin.Mobile2'), null, ['class' => 'control-label']) }}
                        {{ Form::text('Mobile2', old('Mobile2'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Mobile2')])) }}
                    </div>
                </div>





                {{Form::submit(trans('admin.create'),['class'=>'btn btn-primary'])}}
                {!! Form::close() !!}
            </div>
        </div>








@endsection
