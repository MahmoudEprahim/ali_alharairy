@extends('admin.index')
@section('title',trans('admin.edit_parents'))
@section('content')

    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">

            {!! Form::model($parents,['method'=>'PUT','route' => ['parents.update',$parents->id]]) !!}
            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label('Brn_No', trans('admin.Brn_No'), ['class' => 'control-label']) }}
                    {{ Form::select('Brn_No',$branche ,$parents->branch_id, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>

                <div class="col-md-4">
                    {{Form::label(trans('admin.name_ar'),null,['class'=>'control-label'])}}
                    {{Form::text('name_ar', $parents->name_ar, array_merge(['class'=>'form-control','placeholder'=>trans('admin.name_ar')]))}}
                </div>

                <div class="col-md-4">
                    {{Form::label(trans('admin.name_en'),null,['class'=>'control-label'])}}
                    {{Form::text('name_en', $parents->name_en, array_merge(['class'=>'form-control','placeholder'=>trans('admin.name_en')]))}}
                </div>
            </div>


            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.Parnt_address'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Parnt_address', $parents->Parnt_address, array_merge(['class' => 'form-control ','placeholder'=>trans('admin.Parnt_address'),'autocomplete'=>'off'])) }}
                </div>

                <div class="col-md-6">
                    {{ Form::label(trans('admin.Parnt_Job'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Parnt_Job', $parents->Parnt_Job, array_merge(['class' => 'form-control','placeholder'=>trans('admin.Parnt_Job')])) }}
                </div>


            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    {{ Form::label(trans('admin.parents_Mobile1'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Mobile1', $parents->Mobile1, array_merge(['class' => 'form-control ','placeholder'=>trans('admin.Mobile1'),'autocomplete'=>'off'])) }}
                </div>

                <div class="col-md-6">
                    {{ Form::label(trans('admin.parents_Mobile2'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Mobile2', $parents->Mobile2, array_merge(['class' => 'form-control','placeholder'=>trans('admin.Mobile2')])) }}
                </div>


            </div>




            <br>
            {{Form::submit(trans('admin.update'),['class'=>'btn btn-primary'])}}
         <a class="btn btn-danger" href={{(aurl('parents'))}}>{{trans('admin.back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>








@endsection
