@extends('admin.index')
@section('title',trans('admin.Create_new_students'))
@section('content')
    @push('js')

        <script>
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                rtl: true,
                language: '{{session('lang')}}',
                inline:true,
                minDate: 0,
                autoclose:true,
                minDateTime: dateToday

            });
        </script>
    @endpush
@hasanyrole('writer|admin')
@can('create')
    <div class="box">
        @include('admin.layouts.message')
        <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
            {!! Form::open(['method'=>'POST','route' => 'students.store']) !!}

            <div class="form-group row">
            <div class="col-md-4">
                    {{ Form::label('Brn_No', trans('admin.Brn_No'), ['class' => 'control-label']) }}
                    {{ Form::select('Brn_No',$branche ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.Brn_No')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.Cstm_NmAr'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Cstm_NmAr', old('Cstm_NmAr'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Cstm_NmAr')])) }}
                </div>
                <div class="col-md-4">
                    {{ Form::label(trans('admin.Cstm_NmEn'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Cstm_NmEn', old('Cstm_NmEn'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Cstm_NmEn')])) }}
                </div>

            </div>

            <div class="form-group row">
            <div class="col-md-4">
                    {{ Form::label(trans('admin.Tr_Dt'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Tr_Dt', old('Tr_Dt'), array_merge(['class' => 'form-control datepicker','placeholder'=>trans('admin.Tr_Dt'),'autocomplete'=>'off'])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.addriss'), null, ['class' => 'control-label']) }}
                    {{ Form::text('addriss', old('addriss'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.addriss')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.Fees'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Fees', old('Fees'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Fees')])) }}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label('Sex_Ty', trans('admin.Sex_Ty'), ['class' => 'control-label']) }}
                    {{ Form::select('Sex_Ty',\App\Enums\GenderType::toSelectArray() ,null, array_merge(['class' => 'form-control','placeholder'=>trans('admin.select')])) }}
                </div>

                <!-- <div class="col-md-4">
                    {{ Form::label(trans('admin.Mobile1'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Mobile1', old('Mobile1'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Mobile2')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label(trans('admin.Mobile2'), null, ['class' => 'control-label']) }}
                    {{ Form::text('Mobile2', old('Mobile2'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Mobile2')])) }}
                </div> -->
                
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    {{ Form::label('Parnt_Name', trans('admin.Parnt_Name'), ['class' => 'control-label']) }}
                    {{ Form::text('Parnt_Name', old('Parnt_Name'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Parnt_Name')])) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label('Parnt_Job', trans('admin.Parnt_Job'), ['class' => 'control-label']) }}
                    {{ Form::text('Parnt_Job', old('Parnt_Job'), array_merge(['class' => 'form-control','placeholder'=>trans('admin.Parnt_Job')])) }}
                </div>

                <!-- <div class="col-md-4">
                {{ Form::label('image', trans('admin.upload_image') . ':', ['class' => 'control-label']) }}
                {{ Form::file('image') }}
                </div> -->

            </div>

            

            {{Form::submit(trans('admin.create'),['class'=>'btn btn-primary'])}}
            {!! Form::close() !!}
        </div>
    </div>
    @endcan
@else
    <div class="alert alert-danger">{{trans('admin.you_cannt_see_invoice_because_you_dont_have_role_to_access')}}</div>

@endhasanyrole







@endsection
