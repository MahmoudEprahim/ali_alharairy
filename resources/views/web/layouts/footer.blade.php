

<style>
    .footer{
        /* background: rgba(250,211,87, 1); */
        /* background: #162234; */
        background: #111111;
        padding: 0 12%;

    }
    .footer h3{
        color:#ffb606;
        font-size: 28px;
    }
    .footer p
    {
        color: #fff;
    }


    a:hover{
        text-decoration: none;
    }


    .copyright {
        text-transform: capitalize;
        /* background: #0d131e;
        text-align: center;
        width: 100%; */
        padding: 7px 0;
    }

    .links{
    ul {list-style-type: none;}
    li a{
        color: white;
        transition: color .2s;
        text-decoration:none;
    &:hover{
         text-decoration:none;
         color:#4180CB;
     }
    }
    }
    .about-company{
    i{font-size: 25px;}
    a{
        color:white;
        transition: color .2s;
    &:hover{color:#4180CB}
    }
    }
    .location{
    i{font-size: 18px;}
    }
    .social-media a{
        margin-right: 10px;
        color: #ffb606;
    }
    .social-media a:hover{
        color:#e7865d
    }
    .footer .links ul li a{color: #fff}
    .copyright p{border-top:1px solid #ffb606;}
    }
    ul{

    }

    .social-footer-icon{
        /* border: 1px solid #ffb606; */
        text-align: center;
        display: inline-block;
        width: 100px;
        height: 25px;
        line-height: 24px;
        border-radius: 50%;
    }
    .facebook-icon{
        color: #ffb606;
    }
</style>
<div class="mt-5 pt-5  footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-xs-12 about-company">
                <img style="margin-right: 5px;" src="{{asset(setting()->logo_ar)}}" alt="logo img">
{{--                <p class="pr-5 ">{{setting()->about_center_en}}</p>--}}
                <p class="social-media">
                <p><i class="fa fa-envelope mr-2" style="color: #ffb606;"></i>{{setting()->email}}</p>
                <p><img style="margin-right: 5px;" src="{{url('assets/img/footer/icons/whatsapp/whatsapp.png')}}" alt="whattsapp img"> {{setting()->phone}} </p>
                <p><img style="margin-right: 5px;" src="{{url('assets/img/footer/icons/facebook/facebook.png')}}" alt="facebook img"> <a class="social-footer-icon" target="_blank" href="{{setting()->facebook}}"> Mr Ali Elhariry </a> </p>
                <br>
                <!--<p><img style="margin-right: 5px;" src="{{url('assets/img/footer/icons/instagram/instagram.png')}}" alt="twitter img"> <a class="social-footer-icon"  target="_blank"  href="{{setting()->twitter}}"> aly.elhariry</a> </p>-->
                <p><img style="margin-right: 5px;" src="{{url('assets/img/footer/icons/youtube/youtube.png')}}" alt="twitter img"> <a class="social-footer-icon"  target="_blank" href="{{setting()->youtube}}"> Mr Ali Elhariry</a> </p>
 <br>

{{--                <p class="social-media" style="padding: 0 9%;">--}}
{{--                    <a class="social-footer-icon" href="{{setting()->facebook}}"> <img style="margin-right: 5px;" src="{{url('assets/img/footer/icons/facebook/facebook.png')}}" alt="facebook img"></a>--}}
{{--                    <a class="social-footer-icon" href="{{setting()->twitter}}"><img style="margin-right: 5px;" src="{{url('assets/img/footer/icons/instagram/instagram.png')}}" alt="instagram img"></a>--}}
{{--                    <a class="social-footer-icon" href="{{setting()->youtube}}"><img style="margin-right: 5px;" src="{{url('assets/img/footer/icons/youtube/youtube.png')}}" alt="youtube img"></a>--}}
{{--                </p>--}}
            </div>
            <div class="col-lg-3 col-xs-12 links">
                <h3 class="mt-lg-0 mt-sm-3">{{trans('web.importtant_links')}}</h3>
                <ul class="m-0 p-0" style="list-style-type: none;">
                    <li><a href="{{url('/')}}">{{trans('web.Home')}}</a></li>
                    <br>
                    <li><a href="{{url('profile')}}">{{trans('web.profile')}} </a></li>
                    <br>
                    <li><a href="{{url('high-school-gate')}}">{{trans('web.Secondary_school_gate')}}</a></li>
                    <br>
                     <li><a href="{{url('english-gate')}}">{{trans('web.english-gate')}}</a></li>
                    <br>



                </ul>

            </div>
            <div class="col-lg-4 col-xs-12 location">
                <h3 class="mt-lg-0 mt-sm-4">{{trans('web.Location')}}</h3>
                @foreach( branches() as $key => $branch)
                <p>@if(\App::getLocale() == 'en') {{$branch->name_en}} @else {{$branch->name_ar}} @endif <a href="{{ $branch->location }}"> <img class="icon_location" src="{{url('assets/img/footer/icons/map/map.png')}}" alt="map img"></a></p>
                @endforeach

            </div>
        </div>
        <div class="row mt-5 mt-5 ">
            <div class="col copyright">
                <p class="text-center"><small class="text-white-50">{{trans('web.Copyrights_Reserved')}}</small></p>
            </div>
        </div>
    </div>
</div>

<!--  Jquery js file  -->
<script src="{{url('/assets//js/jquery.3.4.1.js')}}"></script>
<!--  Bootstrap js file  -->
<script src="{{url('/assets/js/bootstrap.min.js')}}"></script>
<!-- timeline -->
<script src="{{url('/assets//js/timeline/timeline.min.js')}}"></script>
<!-- timeline -->

<!-- datepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{url('/assets/js/datepicker/datepicker.all.js')}}"></script>
<script src="{{url('/assets/js/datepicker/datepicker.en.js')}}"></script>
<!-- datepicker -->

<!--  isotope js library  -->
<script src="{{url('/assets/vendor/isotope/isotope.min.js')}}"></script>
<!--  Magnific popup script file  -->
<script src="{{url('/assets/vendor/Magnific-Popup/dist/jquery.magnific-popup.min.js')}}"></script>
<!--  Owl-carousel js file  -->
<script src="{{url('/assets/vendor/owl-carousel/js/owl.carousel.min.js')}}"></script>
<!--  custom js file  -->
<script src="{{url('/assets/js/main.js')}}"></script>

<script src="https://apps.elfsight.com/p/platform.js" defer></script>
@if(\App::getLocale() == 'en')
    <div class="whatsapp" style="position:fixed;bottom: 10px;right: 10px ; z-index: 150">
        <a target="_blank" href="https://api.whatsapp.com/send?phone=0020 100 963 9170">
            <img src="{{url('/')}}/web/img/unnamed.png" alt="" style="width: 70px">
        </a>
    </div>
@else
    <div class="whatsapp" style="position:fixed;bottom: 10px;left: 10px;z-index: 150">
        <a target="_blank" href="https://api.whatsapp.com/send?phone=0020 100 963 9170">
            <img src="{{url('/')}}/web/img/unnamed.png" alt="" style="width: 70px">
        </a>
    </div>
    @endif





    @stack('js')
@stack('css')

</body>

</html>
