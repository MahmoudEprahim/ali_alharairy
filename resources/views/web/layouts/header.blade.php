<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
        <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">


    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="{{asset('storage/'.setting()->logo_en)}}" title="Ali Elhariry logo"/>
    <title> @yield('title') | {{trans('web.Ali_Elhariry')}}  </title>
    <meta name="description" content="{{ setting()->keyword }}">
     <meta name="keywords" content="Aly Elhariry,to speak engish fluenty confidently teaching for secondary students,online accademy english teaching">
     <meta property="og:title" content="Aly Elhariry, online english teaching accademy">
    <meta property="fb:app_id" content="420228585428590">
    <meta property="og:title" content="Aly Elhariry, online english teaching accademy">

    @if(\App::getLocale() == 'ar')
        <link href="{{url('/')}}/assets/css/bootstrap-rtl.css" rel="stylesheet">
    @else
        <link rel="stylesheet" href="{{url('/assets/css/bootstrap.min.css')}}">
    @endif
    <!--  Bootstrap css file  -->
    <link href="{{url('/assets/css/vendors/font-awesome.min.css')}}" rel="stylesheet">
    <!--  font awesome icons  -->
    <link rel="stylesheet" href="{{url('/assets/css/all.min.css')}}">

    <!--  Magnific Popup css file  -->
    <link rel="stylesheet" href="{{url('/assets/vendor/Magnific-Popup/dist/magnific-popup.css')}}">

    <!--  Owl-carousel css file  -->
    <link rel="stylesheet" href="{{url('/assets/vendor/owl-carousel/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{url('/assets/vendor/owl-carousel/css/owl.theme.default.min.css')}}">

    <!-- datepiker -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{url('/assets/css/datepicker/datepicker.css')}}">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <!-- datepiker -->

    <!-- timeline -->
    <link rel="stylesheet" href="{{url('/assets/css/timeline/timeline.min.css')}}">
    <!-- timeline -->


    @if(\App::getLocale() == 'ar')
    <!--  custom css file  -->
        <link rel="stylesheet" href="{{url('/assets/css/ar_style.css')}}">

        <!--  Responsive css file  -->
        <link rel="stylesheet" href="{{url('/assets/css/responsive.css')}}">
    @else
    <!--  custom css file  -->
        <link rel="stylesheet" href="{{url('/assets/css/En_style.css')}}">

        <!--  Responsive css file  -->
        <link rel="stylesheet" href="{{url('/assets/css/responsive.css')}}">
@endif


    <link href="https://fonts.googleapis.com/css?family=Cairo|Jomolhari&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <script src="{{url('/assets//js/sweetalert2.js')}}"></script>
</head>

<body>
