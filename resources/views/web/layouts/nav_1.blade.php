
<section id="dvPassport" class="Header_Title">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h6 class="pt-2"> {{ $breaknew ? $breaknew->description : ''}}</h6>
            </div>

            <button class="div_cross">x</button>
        </div>

    </div>

</section>
<header class="header_area font-family-alias">
    <div class="main-menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('storage/'.setting()->logo_en)}}" alt="ali elhariry logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <!-- <div class="mr-auto"></div> -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link {{active_menu('')[0]}}" href="{{url('/')}}">{{trans('admin.Home')}} <span class="sr-only">(current)</span></a>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link {{active_menu('photos-gallery')[0]}}" href="{{url('photos-gallery')}}">{{trans('admin.Photos')}}</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{url('video-gallery')}}">{{trans('admin.Videos')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('references')}}">{{trans('admin.References')}}</a>
                    </li>
                    @if(auth()->user() == null)
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('student_register')}}">{{trans('admin.Enrol')}}</a>
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{auth()->user()->name_en}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('parentLogout')}}"><i class="fa fa-sign-in-alt"></i> Log out</a>
                            </div>
                        </li>
                    @endif
                    @if(\App::getLocale() == 'ar')
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('lang/en')}}">En</a>
                        </li>
                    @else

                        <li class="nav-item">
                            <a class="nav-link" href="{{url('lang/ar')}}">Ar</a>
                        </li>
                    @endif

                </ul>
            </div>
        </nav>
    </div>
</header>

@push('js')
    <script>
        $(document).ready(function() {
            $('.div_cross').click(function() {
                $("#dvPassport").hide();

            });
        });
    </script>
@endpush

