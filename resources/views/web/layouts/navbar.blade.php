<?php  $breaknew = \App\breaknew::first();?>
    <section id="dvPassport" class="Header_Title">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h6 class="pt-2"> @if(\App::getLocale() == 'ar') {{$breaknew->ar_describtion}} @else  {{$breaknew->description}} @endif</h6>
                </div>

                <button class="div_cross">x</button>
            </div>

        </div>

    </section>
    <header>
    <nav class="navbar navbar-expand-lg">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-content" aria-controls="navbar-content" aria-expanded="false" aria-label="toggle-navigation">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand"  href="{{url('/')}}"><img  style="max-width:67%;" src="{{asset(setting()->logo_en)}}" alt="ali elhariry logo" class="img-responsive"></a>
        </div>



        <div class="collapse navbar-collapse" id="navbar-content">

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{active_menu('')[0]}}" href="{{url('/')}}">{{trans('admin.Home')}} <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{active_menu('photos-gallery')[0]}}" href="{{url('photos-gallery')}}">{{trans('admin.Photos')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('video-gallery')}}">{{trans('admin.Videos')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('references')}}">{{trans('admin.References')}}</a>
                </li>

                @if(auth()->user() == null)
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('login')}}">{{trans('admin.Enrol')}}</a>
                    </li>


                @else

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{auth()->user()->name}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{url('logout')}}"><i class="fa fa-sign-in-alt"></i> {{trans('web.Log_out')}}</a>
                        </div>
                    </li>

                @endif
                @if(\App::getLocale() == 'ar')
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('lang/en')}}">En</a>
                    </li>
                @else

                    <li class="nav-item">
                        <a class="nav-link" href="{{url('lang/ar')}}">Ar</a>
                    </li>
                @endif
            </ul>
        </div>
    </nav>

    <div class="box">
        <div class="box-1"></div>
        <div class="box-2"></div>
    </div>
    </header>














    @push('js')
    <script>
        $(document).ready(function() {
            $('.div_cross').click(function() {
                $("#dvPassport").hide();

            });
        });
    </script>
@endpush

