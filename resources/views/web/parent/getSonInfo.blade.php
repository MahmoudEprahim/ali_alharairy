@if($student)
    <div class="justify-content-center tab-pane fade show active" id="myTabContent_son_{{$student->id}}">
        <div class=" mb-3 parents-follow-up-container">
            <div class=" no-gutters">
                <ul class="nav nav-pills row" id="pills-tab" role="tablist">
                    <li class="nav-item col-lg-3 col-md-3 col-sm-3">
                        <a class="default-main-border text-center nav-link parents-follow-nav active" id="personal-data-tab" data-toggle="pill" href="#personal-data" role="tab" aria-controls="personal-data" aria-selected="true">Personal data</a>
                    </li>
                    <li class="nav-item col-lg-3 col-md-3 col-sm-3">
                        <a class="default-main-border text-center nav-link parents-follow-nav" id="attendence-tab" data-toggle="pill" href="#attendence" role="tab" aria-controls="attendence" aria-selected="false">Attendence</a>
                    </li>
                    <li class="nav-item col-lg-3 col-md-3 col-sm-3">
                        <a class="default-main-border text-center nav-link parents-follow-nav" id="exams-results-tab" data-toggle="pill" href="#exams-results" role="tab" aria-controls="exams-results" aria-selected="false">Exams results</a>
                    </li>
                    <li class="nav-item col-lg-3 col-md-3 col-sm-3">
                        <a class="default-main-border text-center nav-link parents-follow-nav paymet-last-nav" id="payment-tab" data-toggle="pill" href="#payment" role="tab" aria-controls="payment" aria-selected="false">payment</a>
                    </li>
                </ul>
                <div class="tab-content card" id="myTabContent_son_{{$student->id}}">
                    <div class="tab-pane fade show active" id="personal-data" role="tabpanel" aria-labelledby="personal-data-tab">

                        <div class="mb-3">
                            <div class="row no-gutters personal-data-container">
                                <div class="col-md-2">
                                    <div class="rounded student-padding-img">
                                        @if ($student->image) 
                                        <img src="{{asset('storage/'.$student->image)}}" class="card-img" alt="{{ $student->name_en }}">
                                        @else
                                        <img src="{{url('assets/img/student-img.png')}}" class="card-img" alt="{{ $student->name_en }}">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$student->name_en}}</h5>
                                        <div class="rate-student">
                                            <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                            <i class="fa fa-star star-hover b2" title="Good"></i>
                                            <i class="fa fa-star star-hover b3" title="ok"></i>
                                            <i class="fa fa-star star-hover b4" title="poor"></i>
                                            <i class="fa fa-star star-hover b5" title="usless"></i>
                                        </div>
                                        <table class="table table-borderless">
                                            <thead>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th scope="row">Accademic year</th>
                                                <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span>{{$student->grade?$student->grade->name_en:''}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">School</th>
                                                <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span> {{$student->school?$student->school:''}}</td>

                                            </tr>
                                            <tr>
                                                <th scope="row">Address</th>
                                                <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span> {{$student->addriss?$student->addriss:''}} </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Mobile no.</th>
                                                <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span> {{$student->phone?$student->phone:''}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Parent no.</th>
                                                <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span> {{auth('parent')->user()->mobile_1}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Email</th>
                                                <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span> {{$student->email?$student->email:''}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Group</th>
                                                <td class="font-personal-data orange-main-color"> <span class="dots-span main-black-color">:</span> {{$student->appointment?$student->appointment->days:''}}</td>
                                            </tr>
                                            <tr>
                                                <th >Teacher's notes</th>
                                            </tr>
                                            <tr>
                                                <td colspan="7">
                                                    <div class="card-footer teacher-note-text default-main-border">
                                                        {{$student->teacher_notes?$student->teacher_notes:'No teacher notes'}}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="text-muted last-update">
                                                        <span>Last updated at {{$student->updated_at?$student->updated_at:''}}</span>
                                                    </div>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>


                    <div class="attendence-container tab-pane fade attendence-control-progress-table" id="attendence" role="tabpanel" aria-labelledby="attendence-tab">
                        @foreach(\App\Enums\MonthName::toSelectArray() as $MonthKey => $month)
                            <table class="attendence-table table table-bordered">
                                <thead>
                                <tr>
                                    <th>Lecture</th>
                                    @foreach(\App\Enums\Lecture::toSelectArray() as $lectureKey=>$lecture)
                                        <th class="text-center">{{$lectureKey}}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                <tr>

                                    <th>{{$month}}</th>
                                     @php
                                        $attendArray = \App\Attendance::where('student_id', $student->id)->where('month_id', $MonthKey)->first();
                                    @endphp

                                    @foreach(\App\Enums\Lecture::toSelectArray() as $lectureKey=>$lecture)
                                    
                                    
                                  
                                        @if($attendArray && in_array($lectureKey, json_decode($attendArray->lecture_id)))
                                            <td class="text-center" style="font-weight: 600;"><span class="text-danger">Absence</span></td>
                                        @else
                                            <td >
                                                <div class="progress-wrap progress" data-progress-percent="100">
                                                    <div class="progress-bar progress"></div>
                                                </div>
                                            </td>
                                        @endif
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                        @endforeach
                    </div>

                    @if(count($student->exams) == 0) 
                    <div class="tab-pane fade" id="exams-results" role="tabpanel" aria-labelledby="exams-results-tab">
                        <div class="mb-3 exam-results-second-container">
                            <div class="row no-gutters">
                                <div class="col-lg-7 col-md-7 col-sm-7">
                                    <p>{{ $student->name_en }} has no exams yet.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif


                    <div class="tab-pane fade" id="exams-results" role="tabpanel" aria-labelledby="exams-results-tab">
                        @if(count($student->exams) > 0)
                            @foreach($student->exams as $exam)
                                <div class="mb-3 exam-results-second-container">
                                    <div class="row no-gutters">
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                            <h1 class="section-header-titles text-uppercase orange-main-color font-cairo exam-title">
                                                {{$exam->name_en}}
                                            </h1>
                                            <p>Date : {{$exam->exam_date}}</p>
                                        </div>

                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{$exam->pivot->grade}}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{$exam->pivot->grade.'%'}}">
                                                    <span>{{$exam->pivot->grade}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                            <p class="exam-result">
                                                <span class="text-center">{{$exam->pivot->grade}}</span>
                                                <span style="" class="text-center default-full-exam-mark">100</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                         @endif
                    </div>
                    <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
                        <ul class="attendence-months-container nav nav-pills" id="pills-tab" role="tablist">
                            @foreach(\App\Enums\MonthName::toSelectArray() as $key => $value)
                                <li class="nav-item col-3 ">
                                    <a>
                                        <div class="mt-payment-dot"> <span class="@if(count($student->payments) > 0 && $student->payments->where('MonthName', $key)->first()->fees != null) active-dot @endif payment-dot"> </span></div>
                                        <div class="mt-right-payment">
                                            {{$value}}
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @else
    <div class="col-md-12">
        <div class="alert label-info text-danger">There is no data</div>
    </div>
@endif
