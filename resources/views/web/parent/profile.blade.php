@extends('web.layouts.app')

@section('title')
{{auth()->user()->name_en}} profile
@endsection

@section('content')

    @push('css')
        <style>
           /* loading div */
           .loading{
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%) rotate(75deg);
                width: 15px;
                height: 15px;
                }

                .loading::before,.loading::after{
                content: "";
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                width: 15px;
                height: 15px;
                border-radius: 15px;
                animation: loading 1.5s infinite linear;
                }

                .loading::before{
                box-shadow: 15px 15px #e77f67, -15px -15px #778beb;
                }

                .loading::after{
                box-shadow: 15px 15px #f8a5c2, -15px -15px #f5cd79;
                transform: translate(-50%,-50%) rotate(90deg);
                }

                @keyframes loading {
                50%{
                    height: 45px;
                }
                }
            /* loading div */

            .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link{
                color: #111;
                font-weight: 600 !important;
                border: 1px solid #ffb606 !important;
                margin: 5px;
            }

            .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active{
                background: #ffb606;
            }

            .nav-tabs{
                border: none;
            }


        </style>
    @endpush

    @push('js')

        <script>
            $(document).ready(function () {
                $('.sons_link').each(function () {
                    $(this).click(function () {
                        let studentId = $(this).data('son-id');
                        $('.loading').removeClass('d-none');
                        $('.son_content').addClass('d-none');
                        $.ajax({
                            url: "{{route('getSonInfo')}}",
                            type: 'get',
                            dataType: 'html',
                            data:{studentId: studentId},
                            success: function (data) {
                                $('.son_content').removeClass('d-none').html(data);
                                $('.loading').addClass('d-none')
                            }
                        })
                    })
                })
            })
        </script>
    @endpush

<section class="brand-area about-area parents-follow-up" style="padding: 6% 10%;">
    <div class="container">
        <!-- <ul class="nav nav-tabs mb-2" id="myTab_1" role="tablist"> -->
            <!-- @foreach($sons as $keySon => $son) -->
                <!-- <li class="text-center nav-item @if($keySon == 0) active @endif col-lg-4 col-md-4 col-sm-4"> -->
                    <!-- <a class="nav-link sons_link @if($keySon == 0) active @endif" data-son-id="{{$son->id}}" id="home-tab" data-toggle="tab" href="#myTabContent_son_{{$son->id}}" role="tab" aria-controls="home" aria-selected="true">{{$son->name_en}}</a> -->
                <!-- </li> -->
            <!-- @endforeach -->
        <!-- </ul> -->
    <h1 class="section-header-titles text-uppercase orange-main-color font-cairo">Parents Follow Up</h1>
        <span class="title_area_span"></span>
        <div class="loading d-none"></div>
        <div class="son_content">
            <div class="justify-content-center tab-pane fade show active" id="myTabContent_son_ {{count($sons) > 0 ? $sons->first()->id : ''}}">
                <div class=" mb-3 parents-follow-up-container">
                    <div class=" no-gutters">
                        <ul class="nav nav-pills row" id="pills-tab" role="tablist">
                            <li class="nav-item col-lg-3 col-md-3 col-sm-3">
                                <a class="default-main-border text-center nav-link parents-follow-nav active" id="personal-data-tab" data-toggle="pill" href="#personal-data" role="tab" aria-controls="personal-data" aria-selected="true">Personal data</a>
                            </li>
                            <li class="nav-item col-lg-3 col-md-3 col-sm-3">
                                <a class="default-main-border text-center nav-link parents-follow-nav" id="attendence-tab" data-toggle="pill" href="#attendence" role="tab" aria-controls="attendence" aria-selected="false">Attendence</a>
                            </li>
                            <li class="nav-item col-lg-3 col-md-3 col-sm-3">
                                <a class="default-main-border text-center nav-link parents-follow-nav" id="exams-results-tab" data-toggle="pill" href="#exams-results" role="tab" aria-controls="exams-results" aria-selected="false">Exams results</a>
                            </li>
                            <li class="nav-item col-lg-3 col-md-3 col-sm-3">
                                <a class="default-main-border text-center nav-link parents-follow-nav paymet-last-nav" id="payment-tab" data-toggle="pill" href="#payment" role="tab" aria-controls="payment" aria-selected="false">payment</a>
                            </li>
                        </ul>

                        @if(count($sons) > 0 )

                            <div class="tab-content card" id="myTabContent_son_{{count($sons) > 0 ? $sons->first()->id : ''}}">
                                <div class="tab-pane fade show active" id="personal-data" role="tabpanel" aria-labelledby="personal-data-tab">

                                    <div class="mb-3">
                                        <div class="row no-gutters personal-data-container">
                                            <div class="col-md-2">
                                                <div class="rounded student-padding-img">
                                                    @if (count($sons) > 0 && $sons->first()->image) 
                                                    <img src="{{asset('storage/'.$sons->first()->image)}}" class="card-img" alt="{{ $sons->first()->name_en }}">
                                                    @else
                                                    <img src="{{url('assets/img/student-img.png')}}" class="card-img" alt="{{ $sons->first()->name_en }}">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10">
                                                <div class="card-body">
                                                    <h5 class="card-title">{{count($sons) > 0 ? $sons->first()->name_en : ''}}</h5>
                                                    <div class="rate-student">
                                                        <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                        <i class="fa fa-star star-hover b2" title="Good"></i>
                                                        <i class="fa fa-star star-hover b3" title="ok"></i>
                                                        <i class="fa fa-star star-hover b4" title="poor"></i>
                                                        <i class="fa fa-star star-hover b5" title="usless"></i>
                                                    </div>
                                                    <table class="table table-borderless">
                                                        <thead>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <th scope="row">Accademic year</th>
                                                            <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span>{{count($sons) > 0 && $sons->first()->grade ?$sons->first()->grade->name_en: "No grade"}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">School</th>
                                                            <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span> {{count($sons) > 0 && $sons->first()->school ?$sons->first()->school:'No School'}}</td>

                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Address</th>
                                                            <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span> {{count($sons) > 0 && $sons->first()->addriss?$sons->first()->addriss: 'NO Address'}} </td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Mobile no.</th>
                                                            <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span> {{count($sons) > 0 && $sons->first()->phone?$sons->first()->phone:'No Phone'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Parent no.</th>
                                                            <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span> {{auth('parent')->user()->mobile_1}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Email</th>
                                                            <td class="font-personal-data orange-main-color"><span class="dots-span main-black-color">:</span> {{count($sons) > 0 && $sons->first()->email?$sons->first()->email: 'NO email'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Group</th>
                                                            <td class="font-personal-data orange-main-color"> <span class="dots-span main-black-color">:</span> {{count($sons) > 0 && $sons->first()->appointment?$sons->first()->appointment->days: 'No appointment'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th >Teacher's notes</th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="7">
                                                                <div class="card-footer teacher-note-text default-main-border">
                                                                    {{count($sons) > 0 && $sons->first()->teacher_notes?$sons->first()->teacher_notes: 'No teacher notes'}}
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="text-muted last-update">
                                                                    <span>Last updated at {{count($sons) > 0 &&$sons->first() ? $sons->first()->updated_at : ''}}</span>
                                                                </div>
                                                            </td>

                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="attendence-container tab-pane fade attendence-control-progress-table" id="attendence" role="tabpanel" aria-labelledby="attendence-tab">

                                   
                                
                                    @foreach(\App\Enums\MonthName::toSelectArray() as $MonthKey => $month)
                                        <table class="attendence-table table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Lecture</th>
                                                @foreach(\App\Enums\Lecture::toSelectArray() as $lectureKey=>$lecture)
                                                    <th class="text-center">{{$lectureKey}}</th>
                                                @endforeach
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>

                                                <th>{{$month}}</th>
                                                @php
                                                    $attendArray = count($sons) > 0 ? \App\Attendance::where('student_id', $sons->first()->id)->where('month_id', $MonthKey)->first() : null;
                                            
                                                    $attendMonth = \App\Attendance::where('student_id', $sons->first()->id)->where('month_id', $MonthKey)->first();
                                                   
                                                @endphp
                                                @if(count($sons) > 0)
                                                
                                                    @foreach(\App\Enums\Lecture::toSelectArray() as $lectureKey=>$lecture)
                                                    @if($attendMonth)
                                                        @if(json_decode($attendMonth->lecture_id)[$lectureKey-1] == null) 
                                                        <td >
                                                            <div class="progress-wrap progress" data-progress-percent="100">
                                                                <div class="progress-bar progress"></div>
                                                            </div>
                                                        </td>
                                                        @else
                                                            <td class="text-center" style="font-weight: 600;"><span class="text-danger">Absence</span></td>
                                                         @endif
                                                         @else
                                                         <td >
                                                            
                                                        </td>
                                                     @endif
                                                     
                                                    @endforeach
                                                @endif
                                            </tr>
                                            </tbody>
                                        </table>
                                    @endforeach
                                </div>

                               
                                <!-- if students has no exams -->
                                @if(count($sons->first()->exams) == 0) 
                                <div class="tab-pane fade" id="exams-results" role="tabpanel" aria-labelledby="exams-results-tab">
                                    <div class="mb-3 exam-results-second-container">
                                        <div class="row no-gutters">
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <p>{{ $sons->first()->name_en }} has no exams yet.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <div class="tab-pane fade" id="exams-results" role="tabpanel" aria-labelledby="exams-results-tab">
                                    @foreach($sons->first()->exams as $exam)
                                        <div class="mb-3 exam-results-second-container">
                                            <div class="row no-gutters">
                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                    <h1 class="section-header-titles text-uppercase orange-main-color font-cairo exam-title">
                                                        {{$exam->name_en}}
                                                    </h1>
                                                    <p>Date : {{$exam->exam_date}}</p>
                                                </div>

                                                <div class="col-lg-7 col-md-7 col-sm-7">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{$exam->pivot->grade}}"
                                                                aria-valuemin="0" aria-valuemax="100" style="width:{{$exam->pivot->grade.'%'}}">
                                                            <span>{{$exam->pivot->grade}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                    <p class="exam-result">
                                                        <span class="text-center">{{$exam->pivot->grade}}</span>
                                                        <span style="" class="text-center default-full-exam-mark">100</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                               
                                @if(count($sons) > 0)
                                    <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
                                        <ul class="attendence-months-container nav nav-pills" id="pills-tab" role="tablist">

                                            @foreach(\App\Enums\MonthName::toSelectArray() as $key => $value)
                                                @php
                                                    $fees = $sons->first()->payments->where('MonthName', $key)->first()->fees;
                                                @endphp
                                                <li class="nav-item col-3 ">
                                                    <a>
                                                        <div class="mt-payment-dot"> <span class="@if(count($sons->first()->payments) != 0)
                                                            @if($fees != null) active-dot @endif @endif payment-dot"> </span></div>
                                                        <div class="mt-right-payment">
                                                            {{$value}}
                                                        </div>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection
