@extends('web.layouts.app')

@section('title')
    {{trans('web.log_in')}}
@endsection

@section('content')

<div class="parents-sign-up-container d-flex  align-items-center justify-content-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10 col-md-6 py-5">
                @include('web.layouts.error')
                <form method="post" id="Form3" action="{{route('login')}}" id="subscribeForLaunch" class="input-group">
                    @csrf()

                    <div class="card">
                        <div class="card-header">
                        <h1 class="section-header-titles text-uppercase font-cairo text-center"> {{trans('web.login')}}</h1>
                        </div>
                        <ul class="list-group list-group-flush">

                            <li class="list-group-item">
                                <div class="input-group mb-3 inputs-container-div">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="fab fa-facebook" aria-hidden="true"></i></button>
                                    </div>
                                    <input name="email" type="email" class="form-control input-form-text" placeholder="E-mail">
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="input-group mb-3 inputs-container-div">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="fab fa-facebook" aria-hidden="true"></i></button>
                                    </div>
                                    <input name="password" type="password" class="form-control input-form-text" placeholder="Password">
                                </div>
                            </li>

                            <li class="list-group-item" style="margin: 9px;">
                            <div class="row">
                                <div class="col-am-6">
                                    <input id="remember_me" type="checkbox" name="remember_me" value="1"> <label for="remember_me">{{trans('web.Remembe_me')}}</label><br>
                                </div>


                                <!--@if (Route::has('password.request'))-->
                                <!--    <span style="display: inline-block;position: absolute; right: 0;" class="psw" >-->
                                <!--        <a class="btn btn-link" href="{{ route('password.request') }}" style="color: #ffb606;">-->
                                <!--            {{ __('Forgot Your Password?') }}-->
                                <!--        </a>-->
                                <!--    </span>-->
                                <!--@endif-->

                            </div>
                            </li>




                            <div class="row">
                                <div class="col">
                                    <div class="mx-auto w-50 text-white text-center submit-center-padding">
                                    <button type="submit" class="btn button text-uppercase orange-background-color submit-btn">{{trans('web.Log_in')}}</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mx-auto w-50 text-center submit-center-padding">
                                <!--<p>you don't have an account! <a href="{{url('sign-up-parents')}}" style="color: #ffb606;">Sign up</a> </p>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <a class="btn btn-warning" style="cursor: pointer" href="{{route('register')}}">{{trans('web.You_don_have_account_Register')}}</a>
                                </div>
                                <div class="col-md-6">
                                    <a class="btn btn-warning" style="cursor: pointer;float: left" href="{{url('forgetPassword')}}">{{trans('web.Did_you_forget_password')}}</a>
                                </div>
                            </div>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>

@push('js')
    {{--login--}}
    <script>


        $(function() {
            @if( $errors->has('email') || $errors->has('password'))
            // $(".submit-btn").click(function(){




                Swal.fire({
                    type: 'warning',
                    title: `{{trans('web.there_is_error_in_inputs')}}`,
                    showConfirmButton: true,
                    timer: 10000
                });
            // });
                @else
{{--                $(".submit-btn").click(function(){--}}

{{--                    Swal.fire({--}}
{{--                        type: 'warning',--}}
{{--                        title: `{{trans('web.Success_login')}}`,--}}
{{--                        showConfirmButton: true,--}}
{{--                        timer: 10000--}}
{{--                    });--}}
{{--                });--}}

                @endif


        });
        //register



    </script>
@endpush
@endsection
