@extends('web.layouts.app')

@section('content')

<div class="parents-sign-up-container h-100 d-flex  align-items-center justify-content-center">
    <div class="container">
        <div class="col-md-6 mx-auto">@include('web.layouts.error')</div>
        <div class="row justify-content-center">
            <div class="col-10 col-md-6">
                <form action="{{route('parentRegister')}}" method="post" id="subscribeForLaunch" class="input-group">
                    @csrf()
                    <div class="card">
                        <div class="card-header">
                        <h1 class="section-header-titles text-uppercase font-cairo text-center">Sign Up</h1>
                        </div>
                        <ul class="list-group list-group-flush">
                            <!-- <li class="list-group-item">
                            <i class="fab fa-facebook" aria-hidden="true"></i>
                            <input type="email" class="form-control email" required="required" placeholder="Email" />
                            </li> -->
                            <li class="list-group-item">
                                <div class="input-group mb-3 inputs-container-div">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="fab fa-facebook" aria-hidden="true"></i></button>
                                    </div>
                                    <input name="name_en" type="text" class="form-control input-form-text" placeholder="Full name">

                                </div>
                                @if ($errors->has('name_en'))
                                    <span class="text-danger">{{ $errors->first('name_en') }}</span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <div class="input-group mb-3 inputs-container-div">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="fab fa-facebook" aria-hidden="true"></i></button>
                                    </div>
                                    <input name="home_address" type="text" class="form-control input-form-text" placeholder="Address">
                                </div>
                                @if ($errors->has('home_address'))
                                    <span class="text-danger">{{ $errors->first('home_address') }}</span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <div class="input-group mb-3 inputs-container-div">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="fab fa-facebook" aria-hidden="true"></i></button>
                                    </div>
                                    <input name="job" type="text" class="form-control input-form-text" placeholder="Job">
                                </div>
                                @if ($errors->has('job'))
                                    <span class="text-danger">{{ $errors->first('job') }}</span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <div class="input-group mb-3 inputs-container-div">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="fab fa-facebook" aria-hidden="true"></i></button>
                                    </div>
                                    <input name="mobile_1" type="text" class="form-control input-form-text" placeholder="Mobile number">
                                </div>
                                @if ($errors->has('mobile_1'))
                                    <span class="text-danger">{{ $errors->first('mobile_1') }}</span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <div class="input-group mb-3 inputs-container-div">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="fab fa-facebook" aria-hidden="true"></i></button>
                                    </div>
                                    <input name="parent_email" type="email" class="form-control input-form-text" placeholder="E-mail" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E-mail'">
                                </div>
                                @if ($errors->has('parent_email'))
                                    <span class="text-danger">{{ $errors->first('parent_email') }}</span>
                                @endif
                            </li>
                            <li class="list-group-item">
                                <div class="input-group mb-3 inputs-container-div">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="fab fa-facebook" aria-hidden="true"></i></button>
                                    </div>
                                    <input name="password" type="password" class="form-control input-form-text" placeholder="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E-mail'">
                                </div>
                                @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </li>
                            <div class="row">
                                <div class="col">
                                    <div class="mx-auto w-50 text-white text-center submit-center-padding">
                                        <button type="submit" class="btn button text-uppercase orange-background-color submit-btn">Sign up</button>
                                    </div>
                                    <div class="col-md-6 mx-auto my-3">
                                        <span>You have account? <a class="orange-main-color" href="{{route('parentLogin')}}">Sign in</a></span>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>


@endsection
