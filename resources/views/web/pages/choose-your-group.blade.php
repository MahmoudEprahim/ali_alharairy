@extends('web.layouts.app')

@section('content')

<style>
    /* .datepicker {
  width: 100%;
  background: #fff;
  border-radius: 10px;
  
  overflow: hidden;
} */

</style>

<section class="brand-area about-area" style="padding: 0% 10%;">
    <div class="container">
        <h1 class="section-header-titles text-center text-uppercase font-cairo">1.Choose your group</h1>
        
        <div class="row justify-content-center">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="orange-main-color text-center">Groups</th>
                    <th  class="orange-main-color text-center">Days</th>
                    <th  class="orange-main-color text-center">Group Availability</th>
                    <th  class="orange-main-color text-center">Available Places</th>
                    
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>01:00</td>
                    <td>sat sun mon</td>
                    <td>Yes</td>
                    <td>Yes10 seats <input type="checkbox" name="vehicle1" value="Bike" style="float: right;"></td>
                    
                </tr>
                <tr>
                    <td>01:00</td>
                    <td>sat sun mon</td>
                    <td>Yes</td>
                    <td>Yes10 seats <input type="checkbox" name="vehicle1" value="Bike" style="float: right;"></td>
                    
                </tr>
                <tr>
                    <td>01:00</td>
                    <td>sat sun mon</td>
                    <td>Yes</td>
                    <td>Yes10 seats <input type="checkbox" name="vehicle1" value="Bike" style="float: right;"></td>
                    
                </tr>
                <tr>
                    <td>01:00</td>
                    <td>sat sun mon</td>
                    <td>Yes</td>
                    <td>Yes10 seats <input type="checkbox" name="vehicle1" value="Bike" style="float: right;"></td>
                    
                </tr>
            
                </tbody>
            </table>
        </div>
        <h1 class="section-header-titles text-center text-uppercase font-cairo">2.Choose your start date</h1>
        
        <div class="row justify-content-center choose-ou-group">
        
        <div class="row">
       
            <div class="col-lg-8 col-md-8 col-sm-8">
                    
                <div id="demo" class="datepicker c-datepicker-date-editor c-datepicker-single-editor example">
                    <i class="c-datepicker-range__icon kxiconfont icon-clock"></i>
                    <input id="datepicker-13" style="width:100%;" type="text" autocomplete="off" name="time" placeholder="Select date and time" class="c-datepicker-data-input only-date" value="">
                </div>
            </div>
        </div>

        </div>
    </div>
</section>



 <!-- Modal not work but we put to active datepiker input -->
 <div class="modal fade bd-example-modal-lg" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    
                    <div class="modal-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>
                                <div class="wrap ">
                                    <div id="arrow-left" class="arrow"><i class="fas fa-angle-left"></i></div>
                                    <div id="slider">
                                        
                                        <div class="brad-slide slide1">
                                            <div class="slide-content">
                                            <span> One</span>
                                            </div>
                                        </div>
                                        <div class="brad-slide slide2">
                                            <div class="slide-content">
                                            <span> Two</span>
                                            </div>
                                        </div>
                                        <div class="brad-slide slide3">
                                            <div class="slide-content">
                                            <span> Three</span>
                                            </div>
                                        </div>
                                    
                                    </div>
                                    <div id="arrow-right" class="arrow"><i class="fas fa-angle-right"></i></div>
                                </div>
                                
                            </td>
                        </tr>
                        </tbody>
                    </table>
            <h1 class="section-header-titles orange-main-color font-cairo text-center">Choose your start date</h1>
            <!--  -->
            <div class="row">
                
                <div class="col-lg-8 col-md-8 col-sm-8">
                      
                    <div class="datepicker c-datepicker-date-editor c-datepicker-single-editor example">
                        <i class="c-datepicker-range__icon kxiconfont icon-clock"></i>
                        <input type="text" autocomplete="off" name="" placeholder="Select date and time" class="c-datepicker-data-input only-date" value="">
                    </div>
                </div>
            </div>
       
        </div>
        
        </div>
    </div>
</div>
<!-- modal -->




@endsection


        