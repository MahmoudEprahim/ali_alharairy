@extends('web.layouts.app')

@section('title')

    {{trans('web.video_gallery')}}
@endsection

@section('content')
 <!--  ========================= About me Area ==========================  -->
        <section class="brand-area about-area" style="padding: 3% 10%;">
            <div class="container">
                <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">{{trans('web.videos')}}</h1>
                <span class="title_area_span"></span>
                <div class="row justify-content-center">


            <div class=" mb-3 videos-container">
                <div class=" no-gutters">

                    <ul class="nav nav-pills row" id="pills-tab" role="tablist">
                        <li class="nav-item col-lg-4 col-md-4 col-sm-4">
                            <a class="default-main-border text-center nav-link parents-follow-nav active" id="recommended-data-tab" data-toggle="pill" href="#recommended-data" role="tab" aria-controls="recommended-data" aria-selected="true">{{trans('web.Recommended')}}</a>
                        </li>
                        <li class="nav-item col-lg-4 col-md-4 col-sm-4">
                            <a class="default-main-border text-center nav-link parents-follow-nav" id="lectures-tab" data-toggle="pill" href="#lectures" role="tab" aria-controls="lectures" aria-selected="false">{{trans('web.Lectures')}}</a>
                        </li>
                        <li class="nav-item col-lg-4 col-md-4 col-sm-4">
                            <a class="default-main-border text-center nav-link parents-follow-nav" id="commics-tab" data-toggle="pill" href="#commics" role="tab" aria-controls="commics" aria-selected="false">{{trans('web.Comics')}}</a>
                        </li>

                    </ul>
                    <div class="tab-content card" id="myTabContent">
                        <div class="tab-pane fade show active" id="recommended-data" role="tabpanel" aria-labelledby="recommended-data-tab">

                                @foreach($contents as $content)
                                    @if($content->VideoType == 0)
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-lg-5 col-md-12 about-title" style="border-radius: 20px;">
                                                    <div class="paragraph py-4">
                                                    <h1 class=" section-header-titles text-uppercase font-cairo">{{trans('web.video_decription')}}</h1>
                                                        <p class="para font-cairo">
                                                        @if(\App::getLocale() == 'ar')   {{$content->desc_en}}  @else  {{$content->desc_en}} @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12">
                                                    <div class="about-image py-4">

                                                        <div class="modal fade" id="modal_{{$content->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg" role="document">

                                                            <div class="modal-content">
                                                            <!--Body-->
                                                            <div class="modal-body mb-0 p-0">
                                                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                                                <iframe class="embed-responsive-item" src="{{$content->video_src}}"
                                                                    allowfullscreen></iframe>
                                                                </div>
                                                            </div>
                                                            <!--Footer-->
                                                            <div class="modal-footer justify-content-center">
                                                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>
                                                            </div>
                                                            </div>
                                                            <!--/.Content-->
                                                        </div>
                                                        </div>
                                                        <!--Modal: Name-->
                                                        <a>
                                                            <img class="video-gallery-img img-fluid z-depth-1" src="{{asset('storage/'.$content->image)}}" alt="video">
                                                            <button class="btn watch-video" data-toggle="modal" data-target="#modal_{{$content->id}}"> <img class="watch-icon" src="{{url('assets/img/icons/watch_icon.png')}}" alt=""> <span class="watch-video-text" style="display: block;">watch video</span>  </button>
                                                            <!-- <button  data-toggle="modal" data-target="#modal1" type="button" class="watch-video btn button float-left">watch video</button> -->
                                                        </a>
                                                        <!-- Grid column -->
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                @endforeach

                        </div>
                        <div class="tab-pane fade attendence-control-progress-table" id="lectures" role="tabpanel" aria-labelledby="lectures-tab">
                            @foreach($contents as $content)
                            @if($content->VideoType == 1)
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12 about-title" style="border-radius: 20px;">
                                            <div class="paragraph py-4">
                                            <h1 class=" section-header-titles text-uppercase font-cairo">video decription</h1>
                                                <p class="para font-cairo">
                                                    {{$content->desc_en}}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="about-image py-4">

                                                <div class="modal fade" id="modal_{{$content->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">

                                                    <div class="modal-content">
                                                    <!--Body-->
                                                    <div class="modal-body mb-0 p-0">
                                                        <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                                        <iframe class="embed-responsive-item" src="{{$content->video_src}}"
                                                            allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <!--Footer-->
                                                    <div class="modal-footer justify-content-center">
                                                        <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>
                                                    </div>
                                                    </div>
                                                    <!--/.Content-->
                                                </div>
                                                </div>
                                                <!--Modal: Name-->
                                                <a>
                                                    <img class="video-gallery-img img-fluid z-depth-1" src="{{asset('storage/'.$content->image)}}" alt="video">
                                                    <button class="btn watch-video" data-toggle="modal" data-target="#modal_{{$content->id}}"> <img class="watch-icon" src="{{url('assets/img/icons/watch_icon.png')}}" alt=""> <span class="watch-video-text" style="display: block;">watch video</span>  </button>
                                                    <!-- <button  data-toggle="modal" data-target="#modal1" type="button" class="watch-video btn button float-left">watch video</button> -->
                                                </a>
                                                <!-- Grid column -->
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endif
                            @endforeach

                        </div>
                        <div class="tab-pane fade" id="commics" role="tabpanel" aria-labelledby="commics-tab">
                            @foreach($contents as $content)
                            @if($content->VideoType == 2)
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12 about-title" style="border-radius: 20px;">
                                            <div class="paragraph py-4">
                                            <h1 class=" section-header-titles text-uppercase font-cairo">video decription</h1>
                                                <p class="para font-cairo">
                                                    {{$content->desc_en}}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="about-image py-4">

                                                <div class="modal fade" id="modal_{{$content->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">

                                                    <div class="modal-content">
                                                    <!--Body-->
                                                    <div class="modal-body mb-0 p-0">
                                                        <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                                        <iframe class="embed-responsive-item" src="{{$content->video_src}}"
                                                            allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <!--Footer-->
                                                    <div class="modal-footer justify-content-center">
                                                        <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>
                                                    </div>
                                                    </div>
                                                    <!--/.Content-->
                                                </div>
                                                </div>
                                                <!--Modal: Name-->
                                                <a>
                                                    <img class="video-gallery-img img-fluid z-depth-1" src="{{asset('storage/'.$content->image)}}" alt="video">
                                                    <button class="btn watch-video" data-toggle="modal" data-target="#modal_{{$content->id}}"> <img class="watch-icon" src="{{url('assets/img/icons/watch_icon.png')}}" alt=""> <span class="watch-video-text" style="display: block;">watch video</span>  </button>
                                                    <!-- <button  data-toggle="modal" data-target="#modal1" type="button" class="watch-video btn button float-left">watch video</button> -->
                                                </a>
                                                <!-- Grid column -->
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endif
                            @endforeach
                        </div>

                    </div>

                </div>
            </div>



                </div>
            </div>





















        </section>
        <!--  ========================= End About me Area ==========================  -->


@endsection
