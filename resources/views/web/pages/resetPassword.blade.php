@extends('web.layouts.app')
@section('title','Single Blog')
@section('content')
    <div class="container">
        <h6 class="h2 text-center">{{trans('web.Reset Password')}}</h6>
        <div class="reset" style="height: 50vh">
            {!! Form::open(['method'=>'post','route'=>['resetpassword',$csrfToken->token]]) !!}
            <div class="form-group">
                {{ Form::email('email',$csrfToken->email,['class'=>'form-control','placeholder'=>trans('web.email')]) }}
            </div>
            <div class="form-group">
                {{ Form::password('password',['class'=>'form-control','placeholder'=>trans('web.password')]) }}
            </div>
            <div class="form-group">
                {{ Form::password('password_confirmation',['class'=>'form-control','placeholder'=>trans('web.password confirmation')]) }}
            </div>
            <div class="form-group">
                {{ Form::submit('Reset Password',['class'=>'bg-oud border-0 py-2 text-white mb-4 btn-block btn-flat']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection
