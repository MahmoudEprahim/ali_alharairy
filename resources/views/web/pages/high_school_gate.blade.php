@extends('web.layouts.app')

@section('title')
   Secondary high school gate
@endsection

@section('content')


    @push('css')
        <style>
            /* ajax loader*/
            .loading{
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%,-50%) rotate(75deg);
              width: 15px;
              height: 15px;
            }
            
            .loading::before,.loading::after{
              content: "";
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%,-50%);
              width: 15px;
              height: 15px;
              border-radius: 15px;
              animation: loading 1.5s infinite linear;
            }
            
            .loading::before{
              box-shadow: 15px 15px #e77f67, -15px -15px #778beb;
            }
            
            .loading::after{
              box-shadow: 15px 15px #f8a5c2, -15px -15px #f5cd79;
              transform: translate(-50%,-50%) rotate(90deg);
            }
            
            @keyframes loading {
              50%{
                height: 45px;
              }
            }

                
            /* ajax loader*/
            .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link{
                color: #111;
                font-weight: 600 !important;
                border: 1px solid #ffb606 !important;
                margin: 5px;
            }

            .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active{
                background: #ffb606;
            }

            .nav-tabs{
                border: none;
            }

            .nav-link{
                padding: .5rem .7rem;
            }
            .black-main-color{
                color: #111;
            }
            
            @media (max-width: 500px) {
                .card_wrapper_container{
                    padding-top: 0 !important;
                }
            }
            
        </style>
    @endpush

    @push('js')
        <script>
            $(document).ready(function () {
                // $('.secondary_link').each(function () {
                //     $(this).click(function () {
                //         $($(this).attr('href')).addClass('show active in').parent().siblings().children().removeClass('show active in')
                //     });
                // });

                $('.grades_link').each(function () {
                    $(this).click(function () {
                        let grade_id = $(this).data('grade-id');
                        $('.ajax_loader').removeClass('d-none');
                        $('.info_card').addClass('d-none');
                        $.ajax({
                            url: "{{route('getGradeInfo')}}",
                            type: 'get',
                            dataType: 'html',
                            data:{grade_id: grade_id},
                            success: function (data) {
                                $('.info_card').removeClass('d-none');
                                $('#myTabContent').removeClass('d-none').html(data);
                                $('.ajax_loader').addClass('d-none')
                            }
                        })
                    })
                })
            })
        </script>
    @endpush

    <section class="brand-area about-area parents-follow-up" style="padding: 6% 10%;">

        <div class="container">
            <h1 class="section-header-titles text-uppercase orange-main-color font-cairo">High School Gate</h1>
            <span class="title_area_span"></span>
             <div class="justify-content-center">
                 <div class="mb-3 parents-follow-up-container">
                     <div class="no-gutters">

                         <ul class="nav nav-tabs mb-2" id="myTab_1" role="tablist">
                             @foreach($grades as $keyGrade=>$grade)
                                 <li class="nav-item @if($keyGrade == 0)  @endif col-lg-4 col-md-4 col-sm-4">
                                     <a class="nav-link grades_link @if($keyGrade == 0)  @endif" data-grade-id="{{$grade->id}}" id="home-tab" data-toggle="tab" href="#secondary_tap_{{$grade->id}}" role="tab" aria-controls="home" aria-selected="true">{{$grade->name_en}}</a>
                                 </li>
                             @endforeach
                         </ul>
                         <div class="container-fluid">
                             <div class="row justify-content-center ajax_loader d-none loading"></div>
                                 
                             <div class="card info_card">
                                 <div class="card-body py-5 card_wrapper_container">
                                     <div class="tab-content col-md-12 col-sm-12" id="myTabContent">
                                        <p class="text-center section-header-titles text-uppercase font-family-alias black-main-color">Please select your grade to open the content.</p>
                                    
                                    
    @foreach($grades->first()->units as $keyUnit=>$unit)
        @foreach(\App\Enums\SecondaryTypeList::toSelectArray() as $keyList=>$list)
            @foreach(\App\Content::with('grade', 'unit')->where('grade_id', $grades->first()->id)->where('units_id', $unit->id)->where('type_list', $keyList)->get() as $content)
                @foreach($content->contentsMedia as $contentMedia)
                    <div class="col-lg-6 col-md-12">
                <div class="about-image py-4">
                    <div class="modal fade" id="modal_{{$contentMedia->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                        
                            <div class="modal-content">
                            <!--Body-->
                            <div class="modal-body mb-0 p-0">
                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                <iframe class="embed-responsive-item" src="{{$contentMedia->video_src}}"
                                    allowfullscreen></iframe>
                                </div>
                            </div>
                            <!--Footer-->
                            <div class="modal-footer justify-content-center">
                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>
                            </div>
                            </div>
                            <!--/.Content-->
                        </div>
                    </div>
                    <!--Modal: Name-->
                </div>
            </div>
                @endforeach
            @endforeach
        @endforeach
    @endforeach
                                    
                                    </div>
                                 </div>
                             </div>
                         </div>

                     </div>

                 </div>
             </div>

        </div>
    </section>
@endsection


