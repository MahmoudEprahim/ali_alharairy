@extends('web.layouts.app')

@section('title')
    references
@endsection

@section('content')

@push('css')
<style>

.download-show-btns{
  padding: .5rem 0rem;
  position: absolute;
  bottom: 10px;
  color: #fff;
}

.download-btn{
  background: #ffb606;
  color: #fff;
}
.show-btn{
  background: #000;
  color: #fff;
  width: 175px;
}
.download-btn:hover, .show-btn:hover{
  color:#fff; 
}

/* references page*/
@media (max-width: 768px) {
    .download-show-btns{
        position: initial;
        padding: 0px;
    }
    .show-btn{
        width: 100px;
    }
    .button{
        padding: 7px;
    }

}
</style>
@endpush
<section class="brand-area about-area" style="padding: 6% 10%;">
    <div class="container">
        <div class="justify-content-center">
           @foreach($contents as $content)
            <div class="card mb-3 references-container">
                <div class="row no-gutters">
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Description</h5>
                            <p class="card-text">
                                {{$content->desc_en}}
                            </p>
                            <div class="download-show-btns">
                                <a href="{{asset('uploads/library/resources/'.$content->media)}}" download type="button" class="download-btn btn button  text-uppercase "><span class="hover-padding"><i class="fa fa-download"></i></span> download </a>
                                <a href="{{$content->desc_ar}}" show type="button" class="show-btn btn button  text-uppercase " target="_blank"><span class="hover-padding"><i class="fa fa-eye"></i></span> show </a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card-body">
                            @if($content->image)
                                <img src="{{asset('storage/'.$content->image)}}" style="height: 225px;" class="card-img" alt="resource image">
                            @else
                                <img src="{{url('/assets/img/refrences/dictionary-image.jpg')}}" style="height: 225px;" class="card-img" alt="resource image">
                            @endif
                        </div>
                    </div>
                    
                </div>
            </div>
            @endforeach
        </div>
    </div>

</section>
@endsection