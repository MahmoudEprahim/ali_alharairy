@extends('web.layouts.app')

@section('title')
    {{trans('web.Student_register')}}
@endsection

@section('content')
    @push('css')
        <style>



            body,
            #slider,
            .wrap,
            .slide-content {
                margin: 0;
                padding: 0;
                font-family: Arial, Helvetica, sans-serif;
                width: 100%;
                height: 100%;
                /* overflow-x: hidden; */
            }

            .student-wrapper_container{
                margin-left: auto;
                margin-right: auto;
                -webkit-box-shadow: 2px 2px 3px 0 rgba(117,124,129,.12);
                box-shadow: 2px 2px 3px 0 rgba(117,124,129,.12);
                /* max-width: 620px; */
                border: 1px solid #dbe2e8;
                text-align: center;
            }

            /* edits in 17/1  */
            .btn-margin {
                margin: 30px 0;
            }
            .enroll-course-btn {
                height: 40px;
            }



            .upload-photo {
                width: 80px;
                height: 80px;
            }

            .wrap {
                position: relative;
            }

            .brad-slide {
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;
            }
            .slide-content {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                text-align: center;
            }

            .slide-content span {
                font-size: 1.5rem;
                color: #000;
            }

            .arrow {
                cursor: pointer;
                position: absolute;
                top: 112%;
                margin-top: -35px;
                width: 0;
                height: 0;
                /* border-style: solid; */
            }

            #arrow-left {
                /* border-width: 5px 10px 5px 0; */
                /* border-color: transparent rgb(80, 20, 20) transparent transparent; */
                left: 0;
                margin-left: 30px;
            }

            #arrow-right {
                /* border-width: 5px 0 5px 10px; */
                /* border-color: transparent transparent transparent rgb(136, 23, 23); */
                right: 0;
                margin-right: 30px;
            }

            .modal-th-width{
                width: 168px;
            }
            .modal-last-th-width{
                width: 320px;
            }

            #preview{
                width: 200px;
                /* border: 1px solid black; */
                height: 150px;
            }
            #preview img{
                width: 200px;
                height: 100%;
                box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.2);
            }



            /* sign-in-btn */
            .sign-in-btn{
                box-shadow: 12px 15px 20px rgba(0,0,0,.1);
            }
            .sign-in-btn:hover{
                box-shadow: none;
                background: #e6a301;
            }

            .btn-file {
                    position: absolute;
                    overflow: hidden;
                    top: -14%;
                    left: 42%;
                    cursor: pointer;
                    font-weight: 500;
                    color: #0a0a0a
            }

            .padding-5{
                padding-top: 5rem;
            }
            .padding-7{
                padding-bottom: 3rem!important;
                padding-top: 7rem!important;
            }
            .list-group-item{
                padding: 0px 1.25rem
            }
            .select-options{
                border-bottom: none;
            }
            .parents-sign-up-container .inputs-container-div{
                padding: 5px;
            }
            .input-text{
                border-bottom: none;
            }
            .c-datepicker-date-editor{
                border-bottom: none;
            }
            .only-date{
                font-family: Roboto, sans-serif;
                font-size: 1.3rem;
                margin-left: 5px;
            }
            .center-margin{
                margin-top: 30px;
                margin-bottom: 30px;
            }


            /* mobile */
            @media (max-width: 500px) {
                .padding-b-5{
                    padding-bottom: 1.5rem!important;
                }
                .btn-file{
                    top: -9%;
                    left: 29%;
                }
                .upload-photo {
                    width: 60px;
                    height: 60px;
                }
                .padding-5{
                    padding-left: 7%;
                }
                .wrapper__responsive_table_container{
                    padding: 0 12px;
                }
                .wrapper__container_responsive{
                    padding-right: 0 !important;
                    padding-left: 0 !important;
                }
                .footer{
                    padding: 0;
                }
            }

        </style>

    @endpush
    @push('js')
        <script>
            $(document).ready(function(){
                $("#form2").show();
                // $(".button1").click(function(){
                //     $("#form2").hide();
                //     $("#form1").show();
                //     $(this).addClass("btn-active").removeClass("closed-btn");
                //     $(".button2").addClass("closed-btn").removeClass("btn-active");

                // });
                // $(".button2").click(function(){
                //     $("#form2").show();
                //     $("#form1").hide();
                //     $(this).addClass("btn-active").removeClass("closed-btn");
                //     $(".button1").addClass("closed-btn").removeClass("btn-active");
                // });

                // show appointments depending branch and grade selected
                $('.grades_id, .branches_id').change(function(){
                    let grade_id = $('.grades_id').val(),
                        branch_id = $('input[type="radio"].branches_id:checked').val();

                    if(grade_id !== 'null' && branch_id !== 'null'){
                        $.ajax({
                            url: "{{route('showAppointments')}}",
                            type: "get",
                            dataType: 'html',
                            data:{'_token': "{{csrf_token()}}",grade_id:grade_id, branch_id:branch_id},
                            success: function (data) {
                                $('.appointment_table tbody').html(data)
                            }
                        })
                    }
                });
                // $('.form2_grades_id, .form2_branches_id').change(function(){
                //     let grade_id = $('.grades_id').val(),
                //     alert(grade_id);
                //         branch_id = $('input[type="radio"].form2_branches_id:checked').val();

                //     if(grade_id !== 'null' && branch_id !== 'null'){
                //         $.ajax({
                //             url: "{{route('showAppointments')}}",
                //             type: "get",
                //             dataType: 'html',
                //             data:{'_token': "{{csrf_token()}}",grade_id:grade_id, branch_id:branch_id},
                //             success: function (data) {
                //                 $('.form2_appointment_table tbody').html(data)
                //             }
                //         })
                //     }
                // });


                // preview image before upload
                function readURL(input){
                    if(input.files && input.files[0]){
                        var reader = new FileReader();
                        reader.onload = function(e){

                            $('#preview').removeClass('d-none');
                            $('#preview img').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $(document).on('change','input[type="file"]',function(){
                    readURL(this);
                });

            })

        </script>
    @endpush

    <!--      -->

    <div class="parents-sign-up-container d-flex  align-items-center justify-content-center">

        <div class="container wrapper__container_responsive">
            <div class="col-md-6 mx-auto">@include('web.layouts.error')</div>
            <div class="row justify-content-center">
                <div class="col-sm-5 d-none offset-2" id="preview"><img src="" alt></div>
                <div class="col-10 col-md-7 padding-5">
                    <!--say about login form -->
                    <div class="row ">
                        <div class="rolls padding-b-5 col-sm-5 offset-1">
                            <!--<button class="button1 btn btn-active enroll-course-btn" > {{trans('web.Private_Groups')}} <span class="watch-video-text" style="display: block;"></span>  </button>-->
                            <button class="button1 btn closed-btn enroll-course-btn" > {{trans('web.Private_Groups')}} <span class="watch-video-text" style="display: block;"></span>  </button>
                        </div>
                        <div class="col-sm-5 offset-1">
                            <!--<button class="button2 btn closed-btn enroll-course-btn "> {{trans('web.Enroll_in_summer_cuorse')}}   </button>-->
                             <button class="button2 btn btn-active enroll-course-btn "> {{trans('web.Enroll_in_summer_cuorse')}}   </button>
{{--                            <span class="closed-now-text">closed now</span>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="parents-sign-up-container d-flex  align-items-center justify-content-center">
        <div class="container">

            <div class="row justify-content-center">

                <!-- Modal -->
                <div class="modal fade bd-example-modal-lg" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <!-- <h5 class="modal-title text-center section-header-titles font-cairo" id="exampleModalLabel">Choose your group</h5> -->
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered">
                                    <select class="form-control form-control-lg orange-main-color">
                                        <option>Choose your group</option>
                                        <option>group 1</option>
                                        <option>group 2</option>
                                    </select>

                                    <thead>
                                    <tr>
                                        <th class="text-center modal-th-width orange-main-color" >groups</th>
                                        <th class="text-center modal-th-width orange-main-color" >group Availability</th>
                                        <th class="text-center modal-th-width orange-main-color">Available places</th>
                                        <th class="text-center modal-last-th-width orange-main-color" ></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>

                                        <td>
                                            <!-- <input type="text" class="form-control" id="inputZip"> -->
                                        </td>
                                        <td>
                                            <!-- <input type="text" class="form-control" id="inputZip"> -->
                                        </td>
                                        <td>
                                            <!-- <input type="text" class="form-control" id="inputZip"> -->
                                        </td>
                                        <td><!-- slider -->
                                            <div class="wrap ">
                                                <div id="arrow-left" class="arrow"><i class="fas fa-angle-left"></i></div>
                                                <div id="slider">

                                                    <div class="brad-slide slide1">
                                                        <div class="slide-content">
                                                            <span> One</span>
                                                        </div>
                                                    </div>
                                                    <div class="brad-slide slide2">
                                                        <div class="slide-content">
                                                            <span> Two</span>
                                                        </div>
                                                    </div>
                                                    <div class="brad-slide slide3">
                                                        <div class="slide-content">
                                                            <span> Three</span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div id="arrow-right" class="arrow"><i class="fas fa-angle-right"></i></div>



                                            </div>
                                            <!--slider  -->
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <h1 class="section-header-titles orange-main-color font-cairo text-center">Choose your start date</h1>
                                <!--  -->
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="{{url('assets/img/why-us/support_students/support_students.png')}}" alt="About us" class="prevent-selection prevent-drag img-fluid">
                                            </div>
                                            <div class="col-lg-12">
                                                <img src="{{url('assets/img/why-us/support_students/support_students.png')}}" alt="About us" class="prevent-selection prevent-drag img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8">

                                        <div class="datepicker c-datepicker-date-editor c-datepicker-single-editor example">
                                            <i class="c-datepicker-range__icon kxiconfont icon-clock"></i>
                                            <input type="text" autocomplete="off" name="" placeholder="Select date and time" class="c-datepicker-data-input only-date" value="">
                                        </div>
                                    </div>
                                </div>

                                <!--  -->

                            </div>
                            <div class="modal-footer row">
                                <div class="form-group col-md-12">
                                    <button style="font-size: 1.5rem;" type="submit" class="btn col-md-12 btn-active text-uppercase white-color">continue</button>
                                </div>
                                <div class="form-group col-md-12">
                                    <button style="font-size: 1.5rem;" type="button" class="btn btn-light col-md-12 text-uppercase" data-dismiss="modal">cancel</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->


<!--                <div id="form1" class="col-12 col-md-7 col-sm-12 padding-7" style='display:none'>-->
<!--                    <form method="POST" action="{{route('studentRegister')}}" class="row"  enctype="multipart/form-data">-->
<!--                        @csrf()-->
<!--                        <div class="card">-->

                            <!-- upload photo -->
<!--                            <span class="btn btn-default btn-file">-->
<!--                            <img  class="upload-photo upload-photo-icon" src="{{url('assets/img/icons/sign-in/upload-photo.png')}}" alt="">-->
<!--                            {{trans('web.Upload_photo')}} <input name="image" type="file" title="upload profile photo">-->
<!--                        </span>-->

<!--                            <div class="card-header">-->
<!--                                <h1 class="section-header-titles text-uppercase font-cairo text-center"> {{trans('web.Private_Groups')}}</h1>-->
<!--                            </div>-->

<!--                            <ul class="list-group list-group-flush">-->

<!--                                <li class="list-group-item">-->
<!--                                    <div class="input-group center-margin row">-->
<!--                                        <p class="col-md-2 col-sm-2 col-xs-2 p-header-titles" style="display:inline">{{trans('web.Center')}} :</p>-->
<!--                                        <div class="col-md-8 col-sm-8 col-xs-8">-->
<!--                                            @foreach($branches as $one)-->
<!--                                                <label for="{{$one->id}}" class="btn-radio">-->
<!--                                                    <input  {{ old('branches_id') == $one->id ? "checked" : "" }} class="branches_id" type="radio"  id="{{$one->id}}" value="{{$one->id}}" name="branches_id">-->
<!--                                                    <svg width="15px" height="15px" viewBox="0 0 20 20">-->
<!--                                                        <circle cx="10" cy="10" r="9"></circle>-->
<!--                                                        <path d="M10,7 C8.34314575,7 7,8.34314575 7,10 C7,11.6568542 8.34314575,13 10,13 C11.6568542,13 13,11.6568542 13,10 C13,8.34314575 11.6568542,7 10,7 Z" class="inner"></path>-->
<!--                                                        <path d="M10,1 L10,1 L10,1 C14.9705627,1 19,5.02943725 19,10 L19,10 L19,10 C19,14.9705627 14.9705627,19 10,19 L10,19 L10,19 C5.02943725,19 1,14.9705627 1,10 L1,10 L1,10 C1,5.02943725 5.02943725,1 10,1 L10,1 Z" class="outer"></path>-->
<!--                                                    </svg>-->
<!--                                                    <span>@if(\App::getLocale() == 'ar'){{$one->name_ar}} @else {{$one->name_en}} @endif</span>-->
<!--                                                </label>-->
<!--                                            @endforeach-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </li>-->

<!--                                <li class="list-group-item">-->
<!--                                    <div class="input-group mb-3 inputs-container-div">-->
<!--                                        <input  value="{{old('name_en')}}" name="name_en" type="text" class="form-control input-form-text" placeholder="{{trans('web.Full_name')}}">-->
<!--                                    </div>-->
<!--                                    @if ($errors->has('name_en'))-->
<!--                                        <span class="text-danger" style=    font-size: x-large;font-size: x-large>{{ $errors->first('name_en') }}</span>-->
<!--                                    @endif-->
<!--                                </li>-->

<!--                                <li class="list-group-item">-->
<!--                                    <div class="input-group mb-3 inputs-container-div ">-->

<!--                                        <select name="grades_id" id="inputState" class="form-control select-options grades_id input-form-text" >-->
<!--                                            <option value="" class="input-form-text">{{trans('web.select_grade')}}</option>-->
<!--                                            @foreach($grade as $one)-->
<!--                                                <option {{ old('grades_id') == $one->id ? "selected" : "" }} class="grades_id input-form-text" value="{{$one->id}}">@if(\App::getLocale() == 'ar'){{$one->name_ar}} @else {{$one->name_en}} @endif</option>-->
<!--                                            @endforeach-->
<!--                                        </select>-->
<!--                                        @if ($errors->has('grades_id'))-->
<!--                                            <span class="text-danger" style=}font-size: x-large>{{ $errors->first('grades_id') }}</span>-->
<!--                                        @endif-->
<!--                                    </div>-->
<!--                                </li>-->

<!--{{--                                <li class="list-group-item">--}}-->
<!--{{--                                    <div class="input-group mb-3 inputs-container-div">--}}-->
<!--{{--                                        <input value="{{old('school')}}" type="text" name="school" placeholder="{{trans('web.School')}}" class="form-control input-form-text" >--}}-->
<!--{{--                                        @if ($errors->has('school'))--}}-->
<!--{{--                                            <span class="text-danger" style=font-size: x-large>{{ $errors->first('school') }}</span>--}}-->
<!--{{--                                        @endif--}}-->
<!--{{--                                    </div>--}}-->
<!--{{--                                </li>--}}-->

<!--                                <li class="list-group-item">-->
<!--                                    <div class="input-group mb-3 inputs-container-div">-->

<!--                                        <input  value="{{old('addriss')}}" type="text" name="addriss" class="form-control input-form-text" placeholder="{{trans('web.Address')}}" >-->
<!--                                    </div>-->
<!--                                    @if ($errors->has('addriss'))-->
<!--                                        <span class="text-danger" style=font-size: x-large>{{ $errors->first('addriss') }}</span>-->
<!--                                    @endif-->
<!--                                </li>-->

<!--                                <li class="list-group-item">-->
<!--                                    <div class="input-group mb-3 inputs-container-div">-->
<!--                                        <input value="{{old('phone')}}" name="phone" type="text" class="form-control input-form-text" placeholder="{{trans('web.Mobile_no')}}." >-->
<!--                                    </div>-->
<!--                                    @if ($errors->has('phone'))-->
<!--                                        <span class="text-danger" style=font-size: x-large>{{ $errors->first('phone') }}</span>-->
<!--                                    @endif-->
<!--                                </li>-->
<!--                                <li class="list-group-item">-->
<!--                                    <div class="input-group mb-3 inputs-container-div">-->
<!--                                        <input value="{{old('mobile_1')}}" type="text" name="mobile_1" placeholder="{{trans('web.Parent\'s_mobile no')}}." class="form-control input-form-text" >-->
<!--                                    </div>-->
<!--                                    @if ($errors->has('mobile_1'))-->
<!--                                        <span class="text-danger" style=font-size: x-large>{{ $errors->first('mobile_1') }}</span>-->
<!--                                    @endif-->
<!--                                </li>-->
<!--{{--                                <li class="list-group-item">--}}-->
<!--{{--                                    <div class="input-group mb-3 inputs-container-div">--}}-->
<!--{{--                                        <div class="col-md-12">--}}-->
<!--{{--                                            <div id="demo" class="datepicker c-datepicker-date-editor c-datepicker-single-editor example">--}}-->
<!--{{--                                                <i class="c-datepicker-range__icon kxiconfont icon-clock"></i>--}}-->
<!--{{--                                                <input value="{{old('start_register')}}" id="datepicker-13" style="width:100%;" type="text" autocomplete="off" name="start_register" placeholder="{{trans('web.beginning_attendance')}}" class="c-datepicker-data-input only-date" >--}}-->
<!--{{--                                            </div>--}}-->
<!--{{--                                        </div>--}}-->
<!--{{--                                    </div>--}}-->
<!--{{--                                </li>--}}-->

<!--                                <li class="list-group-item">-->
<!--                                    <div class="input-group mb-3 inputs-container-div">-->
<!--                                        <input value="{{old('email')}}" name="email" type="email" class="form-control input-form-text" placeholder="{{trans('web.Email')}}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E-mail'" >-->
<!--                                    </div>-->
<!--                                    @if ($errors->has('email'))-->
<!--                                        <span class="text-danger" style=font-size: x-large>{{ $errors->first('email') }}</span>-->
<!--                                    @endif-->
<!--                                </li>-->
<!--                                <li class="list-group-item">-->
<!--                                    <div class="input-group mb-3 inputs-container-div">-->

<!--                                        <input value="{{old('password')}}" type="password" name="password" class="form-control input-form-text" placeholder="{{trans('web.password')}}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'password'" >-->
<!--                                    </div>-->
<!--                                    @if ($errors->has('password'))-->
<!--                                        <span class="text-danger" style=font-size: x-large>{{ $errors->first('password') }}</span>-->
<!--                                    @endif-->
<!--                                </li>-->
<!--                                <li class="list-group-item">-->
<!--                                    <div class="input-group mb-3 inputs-container-div">-->

<!--                                        <input value="{{old('password_confirmation')}}" type="password" name="password_confirmation" class="col-sm-11 form-control input-form-text" placeholder="{{trans('web.confirm_password')}}" >                                </div>-->
<!--                                    @if ($errors->has('password_confirmation'))-->
<!--                                        <span class="text-danger" style=font-size: x-large>{{ $errors->first('password_confirmation') }}</span>-->
<!--                                    @endif-->
<!--                                </li>-->
<!--                                <li class="list-group-item" style="display: none">-->
<!--                                    <div class="input-group mb-3 inputs-container-div">-->

<!--                                        <input value="1" type="number" name="type"  class="col-sm-11 form-control input-form-text" >-->
<!--                                    </div>-->

<!--                                </li>-->
<!--                                <li class="list-group-item wrapper__responsive_table_container">-->
<!--                                    <table class="student-register-table table table-bordered appointment_table" id="choose-group-table">-->
<!--                                        <thead>-->
<!--                                        <tr>-->
<!--                                            <th class="orange-main-color text-center">{{trans('web.Groups')}}</th>-->
<!--                                            <th class="orange-main-color text-center">{{trans('web.Days')}}</th>-->
<!--                                            <th class="orange-main-color text-center">{{trans('web.Group_Availability')}}</th>-->
<!--                                            <th class="orange-main-color text-center">{{trans('web.Available_Places')}}</th>-->
<!--                                        </tr>-->
<!--                                        </thead>-->
<!--                                        <tbody>-->
<!--                                        </tbody>-->
<!--                                    </table>-->
<!--                                </li>-->

<!--                                <div class="row">-->
<!--                                    <div class="col">-->
<!--                                        <div class="mx-auto w-50 text-white text-center submit-center-padding">-->
<!--                                            <button type="submit" class="sign-in-btn btn button text-uppercase orange-background-color">{{trans('web.Enroll')}}</button>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </form>-->
<!--                </div>-->
                <div id="form2" class="col-12 col-md-7 col-sm-12 padding-7">
                    <form method="POST" action="{{route('studentRegister')}}" class="row"  enctype="multipart/form-data">
                        @csrf()
                        <div class="card">

                            <!-- upload photo -->
                            <span class="btn btn-default btn-file">
                            <img  class="upload-photo upload-photo-icon" src="{{url('assets/img/icons/sign-in/upload-photo.png')}}" alt="">
                            {{trans('web.Upload_photo')}} <input name="image" type="file" title="upload profile photo">
                        </span>

                            <div class="card-header">
                                <h1 class="section-header-titles text-uppercase font-cairo text-center"> {{trans('web.Enroll_in_summer_cuorse')}}</h1>
                            </div>
                            <ul class="list-group list-group-flush">

                                <li class="list-group-item">
                                    <div class="input-group center-margin row">
                                        <p class="col-md-2 col-sm-2 col-xs-2 p-header-titles" style="display:inline">{{trans('web.Center')}} :</p>
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            @foreach($branches as $one)
                                                <label for="{{$one->id}}" class="btn-radio">
                                                    <input  {{ old('branches_id') == $one->id ? "checked" : "" }} class="branches_id" type="radio"  id="{{$one->id}}" value="{{$one->id}}" name="branches_id">
                                                    <svg width="15px" height="15px" viewBox="0 0 20 20">
                                                        <circle cx="10" cy="10" r="9"></circle>
                                                        <path d="M10,7 C8.34314575,7 7,8.34314575 7,10 C7,11.6568542 8.34314575,13 10,13 C11.6568542,13 13,11.6568542 13,10 C13,8.34314575 11.6568542,7 10,7 Z" class="inner"></path>
                                                        <path d="M10,1 L10,1 L10,1 C14.9705627,1 19,5.02943725 19,10 L19,10 L19,10 C19,14.9705627 14.9705627,19 10,19 L10,19 L10,19 C5.02943725,19 1,14.9705627 1,10 L1,10 L1,10 C1,5.02943725 5.02943725,1 10,1 L10,1 Z" class="outer"></path>
                                                    </svg>
                                                    <span>@if(\App::getLocale() == 'ar'){{$one->name_ar}} @else {{$one->name_en}} @endif</span>
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="input-group mb-3 inputs-container-div">
                                        <input  value="{{old('name_en')}}" name="name_en" type="text" class="form-control input-form-text" placeholder="{{trans('web.Full_name')}}">
                                    </div>
                                    @if ($errors->has('name_en'))
                                        <span class="text-danger" style="font-size: x-large">{{ $errors->first('name_en') }}</span>
                                    @endif
                                </li>

                                <li class="list-group-item">
                                    <div class="input-group mb-3 inputs-container-div ">

                                        <select name="grades_id" id="inputState" class="form-control select-options grades_id input-form-text" >
                                            <option value="" class="input-form-text">{{trans('web.select_grade')}}</option>
                                            @foreach($grade as $one)
                                                <option {{ old('grades_id') == $one->id ? "selected" : "" }} class="grades_id input-form-text" value="{{$one->id}}">@if(\App::getLocale() == 'ar'){{$one->name_ar}} @else {{$one->name_en}} @endif</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('grades_id'))
                                            <span class="text-danger" style="font-size: x-large">{{ $errors->first('grades_id') }}</span>
                                        @endif

                                    </div>
                                </li>

{{--                                <li class="list-group-item">--}}
{{--                                    <div class="input-group mb-3 inputs-container-div">--}}
{{--                                        <input value="{{old('school')}}" type="text" name="school" placeholder="{{trans('web.School')}}" class="form-control input-form-text" >--}}
{{--                                        @if ($errors->has('school'))--}}
{{--                                            <span class="text-danger" style=font-size: x-large;>{{ $errors->first('school') }}</span>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </li>--}}

                                <li class="list-group-item">
                                    <div class="input-group mb-3 inputs-container-div">

                                        <input  value="{{old('addriss')}}" type="text" name="addriss" class="form-control input-form-text" placeholder="{{trans('web.Address')}}" >
                                    </div>
                                    @if ($errors->has('addriss'))
                                        <span class="text-danger" style="font-size: x-large">{{ $errors->first('addriss') }}</span>
                                    @endif
                                </li>

                                <li class="list-group-item">
                                    <div class="input-group mb-3 inputs-container-div">
                                        <input value="{{old('phone')}}" name="phone" type="text" class="form-control input-form-text" placeholder="{{trans('web.Mobile_no')}}." >
                                    </div>
                                    @if ($errors->has('phone'))
                                        <span class="text-danger" style="font-size: x-large">{{ $errors->first('phone') }}</span>
                                    @endif
                                </li>
                                <li class="list-group-item">
                                    <div class="input-group mb-3 inputs-container-div">
                                        <input value="{{old('mobile_1')}}" type="text" name="mobile_1" placeholder="{{trans('web.Parent\'s_mobile no')}}." class="form-control input-form-text" >
                                    </div>
                                    @if ($errors->has('mobile_1'))
                                        <span class="text-danger" style="font-size: x-large">{{ $errors->first('mobile_1') }}</span>
                                    @endif
                                </li>
{{--                                <li class="list-group-item">--}}
{{--                                    <div class="input-group mb-3 inputs-container-div">--}}
{{--                                        <div class="col-md-12">--}}
{{--                                            <div id="demo" class="datepicker c-datepicker-date-editor c-datepicker-single-editor example">--}}
{{--                                                <i class="c-datepicker-range__icon kxiconfont icon-clock"></i>--}}
{{--                                                <input value="{{old('start_register')}}" id="datepicker-13" style="width:100%;" type="text" autocomplete="off" name="start_register" placeholder="{{trans('web.beginning_attendance')}}" class="c-datepicker-data-input only-date" >--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </li>--}}

                                <li class="list-group-item">
                                    <div class="input-group mb-3 inputs-container-div">
                                        <input value="{{old('email')}}" name="email" type="email" class="form-control input-form-text" placeholder="{{trans('web.Email')}}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E-mail'" >
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="text-danger" style="font-size: x-large">{{ $errors->first('email') }}</span>
                                    @endif
                                </li>
                                <li class="list-group-item">
                                    <div class="input-group mb-3 inputs-container-div">

                                        <input value="{{old('password')}}" type="password" name="password" class="form-control input-form-text" placeholder="{{trans('web.password')}}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'password'" >
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="text-danger" style="font-size: x-large">{{ $errors->first('password') }}</span>
                                    @endif
                                </li>
                                <li class="list-group-item">
                                    <div class="input-group mb-3 inputs-container-div">

                                        <input value="{{old('password_confirmation')}}" type="password" name="password_confirmation" class="col-sm-11 form-control input-form-text" placeholder="{{trans('web.confirm_password')}}" >                                </div>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="text-danger" style="font-size: x-large">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </li>
                                <li class="list-group-item" style="display: none">
                                    <div class="input-group mb-3 inputs-container-div">

                                        <input value="2" type="number" name="type"  class="col-sm-11 form-control input-form-text" >
                                    </div>

                                </li>

                                <li class="list-group-item wrapper__responsive_table_container">
                                    <table class="student-register-table table table-bordered appointment_table" id="choose-group-table">
                                        <thead>
                                        <tr>
                                            <th class="orange-main-color text-center">{{trans('web.Groups')}}</th>
                                            <th class="orange-main-color text-center">{{trans('web.Days')}}</th>
                                            <th class="orange-main-color text-center">{{trans('web.Group_Availability')}}</th>
                                            <th class="orange-main-color text-center">{{trans('web.Available_Places')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                           @if ($errors->has('appointments_id'))
                                        <span class="text-danger" style="font-size: x-large">{{ $errors->first('appointments_id') }}</span>
                                    @endif
                                </li>

                                <div class="row">
                                    <div class="col">
                                        <div class="mx-auto w-50 text-white text-center submit-center-padding">
                                            <button type="submit" id="button_form1" class="sign-in-btn btn button text-uppercase orange-background-color">{{trans('web.Enroll')}}</button>
                                        </div>
                                    </div>
                                </div>

                            </ul>
                        </div>
                    </form>
                </div>
                <div class="col-md-12">
                    <a class="btn btn-warning" style="cursor: pointer" href="{{route('login')}}">{{trans('web.Do_you_already_have_account_Sign_in')}}</a>
                </div>
            </div>
        </div>
    </div>
    @push('js')
        {{--login--}}
        <script>


            {{--$(function() {--}}
            {{--    $(".submit-btn").click(function(){--}}

            {{--        @if( $errors->has('email') || $errors->has('password'))--}}


            {{--        Swal.fire({--}}
            {{--            type: 'warning',--}}
            {{--            title: `{{trans('web.there_is_error_in_inputs')}}`,--}}
            {{--            showConfirmButton: true,--}}
            {{--            timer: 10000--}}
            {{--        });--}}
            {{--        @else--}}

            {{--        Swal.fire({--}}
            {{--            type: 'warning',--}}
            {{--            title: `{{trans('web.Success_login')}}`,--}}
            {{--            showConfirmButton: true,--}}
            {{--            timer: 10000--}}
            {{--        });--}}
            {{--        @endif--}}

            {{--    });--}}
            {{--})--}}
            // $(function() {
                // $("#button_form1").click(function(){

                    @if( $errors->has('branches_id') || $errors->has('name_en') || $errors->has('grades_id') || $errors->has('appointments_id') || $errors->has('email') || $errors->has('password') )
                    Swal.fire({
                        type: 'warning',
                        title: `{{trans('web.there_is_error_in_inputs')}}`,
                        showConfirmButton: true,
                        timer: 10000
                    });
{{--                    @else--}}

{{--                    Swal.fire({--}}
{{--                        type: 'warning',--}}
{{--                        title: `{{trans('web.Success_login')}}`,--}}
{{--                        showConfirmButton: true,--}}
{{--                        timer: 10000--}}
{{--                    });--}}
                    @endif

                // });
            // })
            //register



        </script>
    @endpush
@endsection
