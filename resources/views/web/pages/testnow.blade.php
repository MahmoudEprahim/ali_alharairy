@extends('web.layouts.app')

@section('content')

<section class="brand-area about-area parents-follow-up" style="padding: 3% 5%;">
    <div class="container">
    <h1 class="section-header-titles text-uppercase orange-main-color font-cairo">Online test</h1>
            <span class="title_area_span"></span>
        <div class="justify-content-center">

            <div class=" mb-3 parents-follow-up-container">
                <div class=" no-gutters">

                <div class="tab-content card padding-15" id="myTabContent">
                        <h4 class="text-center margin-bottom-30 text-uppercase orange-main-color font-cairo">Secondary first grade exam</h4>
                        <form class="margin-bottom-15" action="">
                            <div class="margin-bottom-15">
                                <h5 class="margin-bottom-15">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Error rerum iure obcaecati vel
                                    possimus officia maiores perferendis ut! Quos, perspiciatis.
                                </h5>
                                <label>Answer :</label>
                                <div class="row">
                                      <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 1"> <span>consectetur adipisicing </span>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 2"><span>consectetur adipisicing</span>  
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 3"> <span>consectetur adipisicing</span>  
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 4"><span>Lorem ipsum dolor sit amet</span> 
                                    </div>
                                </div> 
                            </div>  
                            <div class="margin-bottom-15">
                                <h5 class="margin-bottom-15">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Error rerum iure obcaecati vel
                                    possimus officia maiores perferendis ut! Quos, perspiciatis.
                                </h5>
                                <label>Answer :</label>
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 1"> <span>consectetur adipisicing </span>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 2"><span>consectetur adipisicing</span>  
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 3"> <span>consectetur adipisicing</span>  
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 4"><span>Lorem ipsum dolor sit amet</span> 
                                    </div>
                                </div> 
                            </div>  
                            <div class="margin-bottom-15">
                                <h5 class="margin-bottom-15">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Error rerum iure obcaecati vel
                                    possimus officia maiores perferendis ut! Quos, perspiciatis.
                                </h5>
                                <label>Answer :</label>
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 1"> <span>consectetur adipisicing </span>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 2"><span>consectetur adipisicing</span>  
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 3"> <span>consectetur adipisicing</span>  
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                    <input type="radio" name="radio" value="Radio 4"><span>Lorem ipsum dolor sit amet</span> 
                                    </div>
                                </div> 
                            </div>  
                        </form>
                        
                        <div class="col-sm-12 justify-content-center" style="margin-top: 15px;padding: 0;">
                                <div class="btn-submit">
                                    <!-- <button type="submit" class="btn float-right button.primary-button">Submit</button> -->
                                    <button type="submit" class="btn float-right button satrt-now-btn font-cairo btn-hover">Next</button>
                                </div>
                            </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>



@endsection