@extends('web.layouts.app')

@section('content')

<section class="brand-area about-area parents-follow-up" style="padding: 6% 10%;">
    <div class="container">
    <h1 class="section-header-titles text-uppercase orange-main-color font-cairo">Online test</h1>
            <span class="title_area_span"></span>
        <div class="justify-content-center">

            <div class=" mb-3 parents-follow-up-container">
                <div class=" no-gutters">
                    
                    <ul class="nav nav-pills row" id="pills-tab" role="tablist">
                        <li class="nav-item col-lg-4 col-md-4 col-sm-4">
                            <a class="default-main-border text-center nav-link onlinetest-nav active" id="secondary-first-grade-tab" data-toggle="pill" href="#secondary-first-grade" role="tab" aria-controls="secondary-first-grade" aria-selected="true">Secondary first grade</a>
                        </li>
                        <li class="nav-item col-lg-4 col-md-4 col-sm-4">
                            <a class="default-main-border text-center nav-link onlinetest-nav" id="secondary-second-grade-tab" data-toggle="pill" href="#secondary-second-grade" role="tab" aria-controls="secondary-second-grade" aria-selected="false">Secondary second grade</a>
                        </li>
                        <li class="nav-item col-lg-4 col-md-4 col-sm-4">
                            <a class="default-main-border text-center nav-link onlinetest-nav float-right" id="secondary-third-grade-tab" data-toggle="pill" href="#secondary-third-grade" role="tab" aria-controls="secondary-third-grade" aria-selected="false">Secondary third grade</a>
                        </li>
                    </ul>
                    <div class="tab-content card padding-10" id="myTabContent">
                        <div class="tab-pane fade show active" id="secondary-first-grade" role="tabpanel" aria-labelledby="secondary-first-grade-tab">
                            <div class="mb-3 exam-results-second-container">
                                <div class="col-lg-2 col-md-2 col-sm-2 justify-content-center" style="margin-top: 15px;padding: 0;">
                                    <div class="btn-submit">
                                        <a href="{{url('testnow')}}" class="btn button satrt-now-btn sign-in-btn font-cairo">Start now</a>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="tab-pane fade" id="secondary-second-grade" role="tabpanel" aria-labelledby="secondary-second-grade-tab">
                            <div class="mb-3 exam-results-second-container">
                                <div class="col-lg-2 col-md-2 col-sm-2 justify-content-center" style="margin-top: 15px;padding: 0;">
                                    <div class="btn-submit">
                                        <a href="{{url('testnow')}}" class="btn button satrt-now-btn sign-in-btn font-cairo">Start now</a>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <div class="tab-pane fade" id="secondary-third-grade" role="tabpanel" aria-labelledby="secondary-third-grade-tab">
                            <div class="mb-3 exam-results-second-container">
                                <div class="col-lg-2 col-md-2 col-sm-2 justify-content-center" style="margin-top: 15px;padding: 0;">
                                    <div class="btn-submit">
                                        <a href="{{url('testnow')}}" class="btn button satrt-now-btn sign-in-btn font-cairo">Start now</a>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection