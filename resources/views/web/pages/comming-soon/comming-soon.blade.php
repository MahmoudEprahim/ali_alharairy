<!DOCTYPE HTML>
<html lang="en">
<head>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" type="image/png" href="{{url('/assets/img/logo/black/B-logo.png')}}"/>
    <title>Ali Elhariry</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CPoppins:400,500" rel="stylesheet">
	<link href=" {{url('/assets/comming-soon/common-css/ionicons.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{url('/assets/comming-soon/common-css/jquery.classycountdown.css')}}" />
	<link href="{{url('/assets/comming-soon/03-comming-soon/css/styles.css')}}" rel="stylesheet">
	<link href="{{url('/assets/comming-soon/03-comming-soon/css/responsive.css')}}" rel="stylesheet">
	
</head>
<body>
	
	<div class="main-area center-text comming-soon" >
		<div class="display-table">
			<div class="display-table-cell">
				<img src="{{url('/assets/img/logo/white/W-logo.png')}}" alt="">
				<h1 class="title font-white"><b>Comming Soon</b></h1>
				<p class="desc font-white">Our website is currently undergoing scheduled maintenance.
					We'll be back shortly. Thank you for your patience.</p>
				
				<!-- <a class="notify-btn" href="#"><b>NOTIFY US</b></a> -->
				
				<ul class="social-btn font-white">
					<li><a href="#">facebook</a></li>
					<!-- <li><a href="#">twitter</a></li>
					<li><a href="#">google</a></li> -->
					<li><a href="#">instagram</a></li>
				</ul><!-- social-btn -->
				
			</div><!-- display-table -->
		</div><!-- display-table-cell -->
	</div><!-- main-area -->
	
</body>
</html>