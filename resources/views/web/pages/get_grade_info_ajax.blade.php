

@if(count($grades->units) != 0)

<div class="tab-pane fade show active" id="secondary_tap_{{$grades->id}}" role="tabpanel" aria-labelledby="home-tab">
    
 
   
    
    <div class="mb-3 exam-results-second-container">
        <div class="row no-gutters">

            <div class="col-lg-2 col-md-2 col-sm-2">
                <ul class="nav nav-tabs flex-column nav-pills left-nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    {{--start get side unit--}}
                    @foreach($grades->units as $key=>$unit)
                        <li class="nav-item @if($key == 0) active @endif">
                            <a class="nav-link @if($key == 0) active @endif" id="first-year-unit-one-tab" data-toggle="pill" href="#first_unit_tap_{{$key}}" role="tab" aria-controls="first-year-unit-one" aria-selected="true">{{$unit->name_en}}</a>
                        </li>
                    @endforeach
                    {{--end get side unit--}}
                </ul>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 offset-1">
                <div class="tab-content col-md-12 col-sm-12 col-xs-12" id="v-pills-tabContent">
                    @foreach($grades->units as $keyUnit=>$unit)
                        <div class="tab-pane fade @if($keyUnit == 0) show active in @else show @endif" id="first_unit_tap_{{$keyUnit}}" role="tabpanel" aria-labelledby="first-year-unit-one-tab">
                            <!-- ul headers in a unit one-->
                            <!--<h2>{{$unit->name_en}}z</h2>-->
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                @foreach(\App\Enums\SecondaryTypeList::toSelectArray() as $keyList=>$list)
                                    <li class="nav-item @if($keyList == 1) active @endif">
                                        <a class="nav-link secondary_link @if($keyList == 1) active @endif" id="unit-one-pills-vocabulary-tab" data-toggle="pill" href="#tap_{{$unit->id.'_'}}{{$keyList-1}}" role="tab" aria-controls="unit-one-pills-vocabulary" aria-selected="true">{{$list}}</a>
                                    </li>
                                @endforeach
                            </ul>
                            @foreach(\App\Enums\SecondaryTypeList::toSelectArray() as $keyList=>$list)
                                <div class="tab-content @if($keyList == 1) show active in @endif col-md-12 col-sm-12 col-xs-12" id="pills-tabContent">
                                    <div class="tab-pane fade @if($keyList == 1) show active in @endif" id="tap_{{$unit->id.'_'}}{{$keyList-1}}" role="tabpanel" aria-labelledby="unit-one-pills-vocabulary-tab">
                                        @foreach(\App\Content::with('grade', 'unit')->where('grade_id', $grades->id)->where('units_id', $unit->id)->where('type_list', $keyList)->get() as $content)
                                        <p>{!! $content->description_en !!}</p>
                                        
                                        @foreach($content->contentsMedia as $contentMedia)
                                        <a data-toggle="modal" data-target="#modal_{{ $contentMedia->id }}" style="color:#ffb606" href="{{ $contentMedia->video_src }}">{{ $contentMedia->video_src }}</a>
                                        <!--<a style="color:#ffb606" href="{{ $contentMedia->video_src }}" target="_blanck">{{ $contentMedia->video_src }}</a>-->
                                        <p>{{ $contentMedia->video_desc }}</p>
                                        
                                        <!-- video modal -->
                                        <div class="col-lg-6 col-md-12">
                                            <div class="about-image py-4">
                                                <div class="modal fade" id="modal_{{$contentMedia->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body mb-0 p-0">
                                                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                                                <iframe class="embed-responsive-item" src="{{$contentMedia->video_src}}"
                                                                    allowfullscreen></iframe>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer justify-content-center">
                                                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- video modal -->
                                        
                                        
                                        @endforeach
                                        
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>
@else
    <div class="alert alert-info text-center text-danger mt-3">There is no data</div>
@endif

<script>
    $(document).ready(function () {
        $('.secondary_link').each(function () {
            $(this).click(function () {
                $($(this).attr('href')).addClass('show active in').parent().siblings().children().removeClass('show active in')
            });

        })
    })
</script>

