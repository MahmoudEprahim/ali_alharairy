@extends('web.layouts.app')

@section('content')

<div class="parents-sign-up-container h-100 d-flex  align-items-center justify-content-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10 col-md-6 py-5">
                <p>Hello, you are successfully registered.</p>
            </div>
        </div>
    </div>
  </div>

@endsection