@extends('web.layouts.app')

@section('title')
   English gate
@endsection

@section('content')

@push('css')

<style>
.treeul {
  list-style-type: none;
}

.treeul {
  margin: 0;
  padding: 0;
}

.caret {
  cursor: pointer;
  -webkit-user-select: none; /* Safari 3.1+ */
  -moz-user-select: none; /* Firefox 2+ */
  -ms-user-select: none; /* IE 10+ */
  user-select: none;
    display: block;
    margin-bottom: 10px;


}

.caret::before {
  content: "\00BB"; 
  color: black;
  display: inline-block;
  margin-right: 6px;
}

.caret-down::before {
  -ms-transform: rotate(90deg); /* IE 9 */
  -webkit-transform: rotate(90deg); /* Safari */'
  transform: rotate(90deg);
}

.nested {
  display: none;
}

.active {
  display: block;
}

    .mainCat{
        background: #ffb606;
        padding: 10px;
        border-radius: 10px;
        display: block;
        width: 40%;
        margin-bottom: 10px;
    }

    .contentLink{
        margin-bottom: 10px;
        display: block;
    }
    
    /* added 7/3/2020*/
    @media (max-width: 768px) {
        .p-0{
            padding:0;
        }
        .mainCat{
            width: fit-content;
        }
    }
    
    

</style>


@endpush

    @push('js')


        <script>
            $(document).ready(function () {
                $('.sectionLink').click(function () {
                    $('.subCard').addClass('d-none');
                })
                $('.contentLink').click(function (e) {
                    e.preventDefault();
                    let id = $(this).attr('value');
                    $.ajax({
                        url: "{{route('getTreeContent')}}",
                        type: 'get',
                        dataType: 'html',
                        data: { id:id },
                        success: function (data) {
                            $('.subchildcontent').html(data);
                            $('.subCard').removeClass('d-none');
                        }
                    })
                })
            })
        </script>


<script>
    var toggler = document.getElementsByClassName("caret");
    var i;

    for (i = 0; i < toggler.length; i++) {
    toggler[i].addEventListener("click", function() {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
    });
    }
</script>



@endpush


<section class="brand-area about-area parents-follow-up" style="padding: 3% 10%;">
    <div class="container">
    <h1 class="section-header-titles text-uppercase orange-main-color font-cairo">English Gate</h1>
            <span class="title_area_span"></span>
        <div class="justify-content-center">

            <div class=" mb-3">
                <div class=" no-gutters">
                    <div class="tab-content card english-gate-container" id="myTabContent">
                    <ul class="nav nav-pills row" id="pills-tab" role="tablist">

                        @foreach(\App\Enums\EnglishSections::toSelectArray() as $key => $section)
                        <li class="nav-item col-lg-4 col-md-4 col-sm-4 offset-1">
                            <a data-section-id="{{$key}}" class="sectionLink default-main-border text-center nav-link high-school-nav @if($key == 1 ) active @endif" id="first-year-high-school-gate-tab" data-toggle="pill" href="#study_{{$key}}" role="tab" aria-controls="study" aria-selected="true">{{$section}}</a>
                        </li>
                        @endforeach
                    </ul>
                        {{-- first tab study--}}
                        <div class="tab-pane fade show active" id="study_1" role="tabpanel" aria-labelledby="study-tab">
                            <div class="mb-3 exam-results-second-container">
                                <div class="row no-gutters">
                                    <div class="col-sm-6 offset-1">
                                        <div class="treeContent nav flex-column nav-pills left-nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            @if($category_1)
                                                <ul class="treeul" id="myUL">
                                                    @foreach($category_1 as $cat)
                                                        <li>
                                                            <span class="caret mainCat">{{$cat->cat_name_en}}</span>
                                                            @foreach($cat->subCategories as $sub)
                                                                <ul class="nested active p-0">
                                                                    <li>
                                                                        <span class="caret">{{$sub->sub_cat_name_en}}</span>
                                                                        <ul class="nested active">
                                                                            @foreach($sub->subCategoryChild as $subchild)
                                                                                <a class="contentLink" value="{{$subchild->id}}" href="#" style="color: #ffb606;">{{$subchild->child_name_en}}</a>
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            @endforeach
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="d-none subCard card col-md-4">
                                        <div class="card-body">
                                            <div class="subchildcontent"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        {{-- second tab exercise--}}
                        <div class="tab-pane fade" id="study_2" role="tabpanel" aria-labelledby="study-tab">
                            <div class="mb-3 exam-results-second-container">
                                <div class="row no-gutters">
                                    <div class="col-sm-6 offset-1">
                                        <div class="treeContent nav flex-column nav-pills left-nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            @if($category_2)
                                                <ul class="treeul" id="myUL">
                                                    @foreach($category_2 as $cat)
                                                        <li>
                                                            <span class="caret mainCat">{{$cat->cat_name_en}}</span>
                                                            @foreach($cat->subCategories as $sub)
                                                                <ul class="nested">
                                                                    <li><span class="caret">{{$sub->sub_cat_name_en}}</span>
                                                                        <ul class="nested">
                                                                            @foreach($sub->subCategoryChild as $subchild)
                                                                                <li>
                                                                                    <a class="contentLink" value="{{$subchild->id}}" href="#" style="color: #ffb606;">{{$subchild->child_name_en}}</a>

                                                                                </li>

                                                                            @endforeach
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            @endforeach
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="d-none subCard card col-md-4">
                                        <div class="card-body">
                                            <div class="subchildcontent"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
