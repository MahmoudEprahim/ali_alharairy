<style>
    .treeul {
        list-style-type: none;
    }

    .treeul {
        margin: 0;
        padding: 0;
    }

    .caret {
        cursor: pointer;
        -webkit-user-select: none; /* Safari 3.1+ */
        -moz-user-select: none; /* Firefox 2+ */
        -ms-user-select: none; /* IE 10+ */
        user-select: none;
    }

    .caret::before {
        content: "\25B6";
        color: black;
        display: inline-block;
        margin-right: 6px;
    }

    .caret-down::before {
        -ms-transform: rotate(90deg); /* IE 9 */
        -webkit-transform: rotate(90deg); /* Safari */'
    transform: rotate(90deg);
    }

    

    .active {
        display: block;
    }
</style>
@foreach($category as $cat)
    <li><span class="caret">{{$cat->cat_name_en}}</span>
        @foreach($cat->subCategories as $sub)
            <ul class="nested active">
                <li><span class="caret">{{$sub->sub_cat_name_en}}</span>
                    <ul class="nested active">
                        @foreach($sub->subCategoryChild as $subchild)
                            <li>{{$subchild->child_name_en}}</li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        @endforeach
    </li>
@endforeach

