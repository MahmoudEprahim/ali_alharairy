@extends('web.layouts.app')

@section('content')

<section class="brand-area about-area" style="padding: 3% 10%;">
            <div class="container">
                <h1 class="section-header-titles text-uppercase orange-main-color font-cairo">my cv</h1>
                <span class="title_area_span"></span>
                <div class="row justify-content-center">
                   
                    <div class="container-fluid" >
                    <div class="profile-container" style="padding:0;box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.2);">

                   
                        <div class="cv-color" style="background:#e4e4e4;">
                            <div class="row">
                                <div class="col-lg-3 col-md-12">
                                    <div class="about-image" style="padding-top: 27%;padding-left: 19%;">
                                        <img style="z-index: 5;position:absolute;border-radius: 50%;border: 3px solid #fff;" src="{{url('assets/img/mr-alii.png')}}" alt="About us" class="img-fluid">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-12 about-title">
                                    
                                    <div class="paragraph py-4" style="padding: 30%;">
                                        <h3 class="para font-cairo" style="color:#141414;font-size: 2.75rem;font-weight: 800;">Ali Elhariry</h3>
                                        <p class="para font-cairo" style="color:#141414;font-size: 1.3rem;">Lecturer</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cv-color " style="background:#e4e4e4;padding: 3%;margin-top: 3px;"></div>
                        <!-- <hr class="my-4"> -->
                        <div class="profile-second-section" style="background:#f3f3f3">
                            <div class="row ">
                                <div class="col-4 col-lg-4 col-md-12" style="padding-top: 81px;margin-left: 61px;">
                                    <h1 style="background: #111111;margin: 30px auto;width: 150px;" class="white-color text-center section-header-titles text-uppercase font-cairo">contact</h1>
                                
                                    <div class="row" style="padding: 0 0 10px 10px;">
                                        <div class="col-sm-2">
                                            <img style="margin: 0 -14px;" class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/mail/mail.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text">example896@gmail.com</h4></div>
                                    </div>             
                                    <div class="row" style="padding: 10px;">
                                    <div class="col-sm-2" style="padding:0">
                                        <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/phone/phone.png')}}" alt="email-icon">
                                    </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text">012354689754</h4></div>
                                    </div>             
                                    <div class="row" style="padding: 10px;">
                                    <div class="col-sm-2" style="padding:0">
                                        <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/location/location.png')}}" alt="email-icon">
                                    </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text text-uppercase">Mansoura</h4></div>
                                    </div>             
                                    <div class="row" style="padding: 10px;">
                                    <div class="col-sm-2" style="padding:0;">
                                        <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/web/web.png')}}" alt="email-icon">
                                    </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text">www.mrenglish.com</h4></div>
                                    </div>
                                </div>

                                <div class="col-7 col-lg-7 col-md-12">
                                    <div class="cv-head" style="padding-left: 6%;margin-top: 32px;">
                                        <h1 class="section-header-titles text-uppercase font-cairo" >about me</h1>
                                       
                                        <p> <i class="fa fa-quote-right fa-lg"></i> second left row second left row second left row second left row  row second left row second left row</p>
                                    </div>
                                    
                                    <div class="row" style="border-left: 3px solid #000;"> <!-- first right row -->
                                        <div class="col-3 col-lg-3 col-md-12" >
                                            <div class="row" style="margin: 69px auto 18px;">
                                                <div class="col-sm-6">
                                                <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/education/education.png')}}" alt="email-icon">
                                            
                                                </div>
                                                <div class="col-sm-6">
                                                <h1 style="background: #111111;width: 150px;"
                                                class="white-color text-center section-header-titles text-uppercase font-cairo">education</h1>
                                                </div>
                                            
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 0px auto;">
                                            <div class="col-sm-2">
                                                <p>20022019</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                            <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                            <p>second left row second left row second left row second left row</p>
                                            </div>
                                        
                                        </div>
                                        <div class="row" style="margin: 10px auto;">
                                            <div class="col-sm-2">
                                                <p>20022019</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                            <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                            <p>second left row second left row second left row second left row</p>
                                            </div>
                                        
                                        </div>
                                        <div class="row" style="margin: 0px auto;">
                                            <div class="col-sm-2">
                                                <p>20022019</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                            <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                            <p>second left row second left row second left row second left row</p>
                                            </div>
                                        
                                        </div>
                                    </div> <!-- first riht row -->
                                </div>
                                
                            </div>
                            <div class="row" style="padding-bottom: 8%;"> <!-- second left row -->
                                <div class="col-4 col-lg-4 col-md-12" style="margin-left: 61px;">
                                    <h1 style="background: #111111;margin: 15px auto;width: 150px;" class="white-color text-center section-header-titles text-uppercase font-cairo">skills</h1>
                                    <div class="row" style="padding: 10px 0 0 36px;">
                                    
                                        <div class="col-sm-10"><h4 class="skill-single-before heading-title-text">Web design</h4></div>
                                    </div>
                                    <div class="row" style="padding: 10px 0 0 36px;">
                                        
                                        <div class="col-sm-10"><h4 class="skill-single-before heading-title-text">Web design</h4></div>
                                    </div>
                                    <div class="row" style="padding: 10px 0 0 36px;">
                                        
                                        <div class="col-sm-10"><h4 class="skill-single-before heading-title-text">Web design</h4></div>
                                    </div>
                                    <div class="row" style="padding: 10px 0 0 36px;">
                                    
                                        <div class="col-sm-10"><h4 class="skill-single-before heading-title-text">Web design</h4></div>
                                    </div> 
                                </div>

                                <!-- second right row -->
                                <div class="col-lg-7 col-md-12" style="border-left: 3px solid #000;">
                                    
                                    <div class="row"> <!-- first right row -->
                                    

                                        <div class="col-3 col-lg-3 col-md-12" >
                                            <div class="row" style="margin: 69px auto 18px;">
                                                <div class="col-sm-6">
                                                <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/EXPERIENCE/EXPERIENCE.png')}}" alt="EXPERIENCE-icon">
                                            
                                                </div>
                                                <div class="col-sm-6">
                                                <h1 style="background: #111111;width: 150px;"
                                                class="white-color text-center section-header-titles text-uppercase font-cairo">experience</h1>
                                                </div>
                                            
                                            </div>
                                        </div>

                                        <div class="row" style="margin: 0px auto;">
                                            <div class="col-sm-2">
                                                <p>20022019</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                            <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                            <p>second left row second left row second left row second left row</p>
                                            </div>
                                        
                                        </div>
                                        <div class="row" style="margin: 10px auto;">
                                            <div class="col-sm-2">
                                                <p>20022019</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                            <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                            <p>second left row second left row second left row second left row</p>
                                            </div>
                                        
                                        </div>
                                        <div class="row" style="margin: 0px auto;">
                                            <div class="col-sm-2">
                                                <p>20022019</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                            <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                            <p>second left row second left row second left row second left row</p>
                                            </div>
                                        
                                        </div>
                                    </div> <!-- first riht row -->
                                </div>
                                <!-- second right row -->
                                
                            </div> <!-- second left row -->
                             <!-- third left row -->
                            <!-- <div class="row">
                                <div class="col-4 col-lg-4 col-md-12" style="margin-left: 61px;">
                                    <h1 style="background: #111111;margin: 30px auto;width: 150px;" class="white-color text-center section-header-titles text-uppercase font-cairo">hobbies</h1>
                                    <div class="row" style="padding: 0px;">
                                        <div class="col-sm-6 row">
                                        <div class="col-sm-1">
                                            <img class="float-left cv-right-icon-img prevent-selection prevent-drag" src="{{url('assets/img/icons/high_school/high_school.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-5"><h4 class="heading-title-text">Travel</h4></div>
                                        </div>
                                        <div class="col-sm-6 row">
                                        <div class="col-sm-1">
                                            <img class="float-left cv-right-icon-img prevent-selection prevent-drag" src="{{url('assets/img/icons/high_school/high_school.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-5"><h4 class="heading-title-text">Music</h4></div>
                                        </div>
                                        <div class="col-sm-6 row">
                                        <div class="col-sm-1">
                                            <img class="float-left cv-right-icon-img prevent-selection prevent-drag" src="{{url('assets/img/icons/high_school/high_school.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-5"><h4 class="heading-title-text">Reading</h4></div>
                                        </div>
                                        <div class="col-sm-6 row">
                                        <div class="col-sm-1">
                                            <img class="float-left cv-right-icon-img prevent-selection prevent-drag" src="{{url('assets/img/icons/high_school/high_school.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-5"><h4 class="heading-title-text">Writing</h4></div>
                                        </div>
                                    </div>
                                     
                                </div>
                            </div>  -->
                            <!-- third left row -->
                            
                        </div>
                       
                    </div>
                    </div>
                </div>
            </div>
        </section>

@endsection