@extends('web.layouts.app')

@section('title')
   profile
@endsection

@section('content')

@push('css')
<style>
    .contact-header-text{
        background: #111111;
        margin: 30px 0;
        width: 150px;
        padding: 3px;
    }
    
    .about-image{
        padding-top: 27%;padding-left: 19%;
    }
    .main-profile-img{
        z-index: 5;position:absolute;border-radius: 50%;border: 3px solid #fff;
    }
    .paragraph{
        padding: 30%;
    }
    .lecturer-name{
        color:#141414;font-size: 2.75rem;font-weight: 800;
    }
    .lecturer-title{
        color:#141414;font-size: 1.3rem;
    }
    .first-right-row{
        border-left: 3px solid #000;
    }
    .circle-before{
        border-left: 2px solid #000;
    }
    .second-right-row{
        border-left: 3px solid #000;
    }
    .second-ex-stage-div{
        margin: 10px; margin-left:2px;
    }
    
    .second-ex-stage{
        margin-left: 0px; 
    }
    .second-left-row{
        margin-left: 61px;
    }  
    
    .p-0{
        padding:0;
    }
    
    /* 7/3/2020*/
    .mr__first_left_section{
        padding-top: 81px;
        margin-left: 61px;
    }
    
    @media (max-width: 768px) {
        .about-image{
            padding-top: 0px; 
            padding-left: 0%; 
            padding: 7px 7px;
        }
        .main-profile-img{
            position: initial;
        }
        .paragraph{
            padding: 0;
        }
        .section-header-titles{
            margin-bottom: 7px;
        }
        .lecturer-name, .lecturer-title{
            text-align: center;
        }
        .first-right-row{
            border-left: none;
        }
        .second-right-row{
            border-left: none;
        }
        .circle-before {
            margin-left: 30px;
        }
        .second-ex-stage-div{
            margin: 10px;
            margin-left:0;
        }
        
        .second-ex-stage{
            margin-left: 0; 
        }
        .second-left-row{
        margin-left: 0;
        }
        .mr__first_left_section{
        padding-top:0;
        margin-left: 0;
        }
        .lecturer-name{
            font-size: 2.75rem;
        }
        .m-0-14{
            margin: 0 -14px;
        }
    }
    
</style>

@endpush

<section class="brand-area about-area" style="padding: 3% 10%;">
            <div class="container">
                <h1 class="section-header-titles text-uppercase orange-main-color font-family-alias">my cv</h1>
                <span class="title_area_span"></span>
                <div class="row justify-content-center">
                   
                    <div class="container-fluid" >
                    <div class="profile-container" style="padding:0;box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.2);">

                   
                        <div class="cv-color" style="background:#e4e4e4;">
                            <div class="row">
                                <div class="col-lg-3 col-md-12">
                                    <div class="about-image">
                                        <img src="{{asset('storage/'.$profile->image)}}" alt="Ali Elhariry" class="img-fluid main-profile-img" title="Ali Elhariry">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-12 about-title">
                                    
                                    <div class="paragraph py-4">
                                        <h3 class="lecturer-name para font-family-alias" title="Ali Elhariry">{{$profile->name_en}}</h3>
                                        <p class="lecturer-title para font-family-alias">{{$profile->title_en}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cv-color " style="background:#e4e4e4;padding: 3%;margin-top: 3px;"></div>
                        <!-- <hr class="my-4"> -->
                        <div class="profile-second-section" style="background:#f3f3f3">
                            <div class="row ">
                                <div class="col-4 col-lg-4 col-md-12 mr__first_left_section">
                                    <h1 class="contact-header-text white-color text-center section-header-titles text-uppercase font-family-alias">contact</h1>
                                
                                    <div class="row" style="padding: 10px;">
                                        <div class="col-sm-2 p-0">
                                            <img class="prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/mail/mail.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text">{{$profile->email}}</h4></div>
                                    </div>             
                                    <div class="row" style="padding: 10px;">
                                    <div class="col-sm-2" style="padding:0">
                                        <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/phone/phone.png')}}" alt="email-icon">
                                    </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text">{{$profile->phone}}</h4></div>
                                    </div>             
                                    <!--<div class="row" style="padding: 10px;">-->
                                    <!--<div class="col-sm-2" style="padding:0">-->
                                    <!--    <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/location/location.png')}}" alt="email-icon">-->
                                    <!--</div>-->
                                    <!--    <div class="col-sm-10">-->
                                    <!--        <h4 class="heading-title-text text-uppercase">-->
                                    <!--        <a class="orange-main-color" href="{{$profile->location}}">{{$profile->location}}</a> -->
                                    <!--        </h4>-->
                                    <!--    </div>-->
                                    <!--</div>             -->
                                    <div class="row" style="padding: 10px;">
                                    <div class="col-sm-2" style="padding:0;">
                                        <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/web/web.png')}}" alt="email-icon">
                                    </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text">{{$profile->site}}</h4></div>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-7 col-md-12 col-sm-12">
                                    <div class="cv-head" style="padding-left: 6%;margin-top: 32px;">
                                        <h1 class="section-header-titles text-uppercase font-family-alias" >about me</h1>
                                       
                                        <p> <i class="fa fa-quote-right fa-lg"></i> {{$profile->profile_desc_en}}</p>
                                    </div>
                                    
                                    <div class="first-right-row row"> <!-- first right row -->
                                        <div class="col-3 col-lg-3 col-md-12" >
                                            <div class="row" style="margin: 69px auto 18px;">
                                                <div class="col-sm-6">
                                                <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/education/education.png')}}" alt="email-icon">
                                            
                                                </div>
                                                <div class="col-sm-6">
                                                <h1 style="background: #111111;width: 150px;"
                                                class="white-color text-center section-header-titles text-uppercase font-family-alias">education</h1>
                                                </div>
                                            
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 0px auto;">
                                            <div class="col-sm-2">
                                                <p>{{$profile->date_first_stage}}</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style=""> 
                                            <h1 class=" section-header-titles text-uppercase font-family-alias">{{$profile->first_stage_title}}</h1>
                                            <p>{{$profile->first_stage}}</p>
                                            </div>
                                        
                                        </div>
                                        <div class="row" style="margin: 10px auto;">
                                            <div class="col-sm-2">
                                                <p>{{$profile->date_second_stage}}</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style=""> 
                                            <h1 class=" section-header-titles text-uppercase font-family-alias">{{$profile->second_stage_title}}</h1>
                                            <p>{{$profile->second_stage}}</p>
                                            </div>
                                        
                                        </div>
                                        <div class="row" style="margin: 0px auto;">
                                            <div class="col-sm-2">
                                                <p>{{$profile->date_third_stage}}</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style=""> 
                                            <h1 class=" section-header-titles text-uppercase font-family-alias">{{$profile->second_stage_title}}</h1>
                                            <p>{{$profile->fourth_stage}}</p>
                                            </div>
                                        
                                        </div>
                                    </div> <!-- first riht row -->
                                </div>
                                
                            </div>
                            <div class="row" style="padding-bottom: 8%;"> <!-- second left row -->
                                <div class="second-left-row col-4 col-lg-4 col-md-12">
                                    <h1 class="contact-header-text white-color text-center section-header-titles text-uppercase font-family-alias">skills</h1>
                                    <div class="row" style="padding: 10px 0 0 36px;">
                                    
                                        <div class="col-sm-10"><h4 class="skill-single-before heading-title-text">{{$profile->skill_1}}</h4></div>
                                    </div>
                                    <div class="row" style="padding: 10px 0 0 36px;">
                                        
                                        <div class="col-sm-10"><h4 class="skill-single-before heading-title-text">{{$profile->skill_2}}</h4></div>
                                    </div>
                                    <div class="row" style="padding: 10px 0 0 36px;">
                                        
                                        <div class="col-sm-10"><h4 class="skill-single-before heading-title-text">{{$profile->skill_3}}</h4></div>
                                    </div>
                                    <div class="row" style="padding: 10px 0 0 36px;">
                                    
                                        <div class="col-sm-10"><h4 class="skill-single-before heading-title-text">{{$profile->skill_4}}</h4></div>
                                    </div> 
                                </div>

                                <!-- second right row -->
                                <div class="second-right-row col-lg-7 col-md-12">
                                    
                                    <div class="row"> <!-- first right row -->
                                    

                                        <div class="col-3 col-lg-3 col-md-12" >
                                            <div class="row" style="margin: 69px auto 18px;">
                                                <div class="col-sm-6">
                                                <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/EXPERIENCE/EXPERIENCE.png')}}" alt="EXPERIENCE-icon">
                                            
                                                </div>
                                                <div class="col-sm-6">
                                                <h1 style="background: #111111;width: 150px;"
                                                class="white-color text-center section-header-titles text-uppercase font-family-alias">experience</h1>
                                                </div>
                                            
                                            </div>
                                        </div>

                                        <div class="row" style="margin: 0px auto;">
                                            <div class="col-sm-2">
                                                <p>{{$profile->date_ex_first_stage}}</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" > 
                                            <h1 class=" section-header-titles text-uppercase font-family-alias">{{$profile->ex_first_stage_title}}</h1>
                                            <p>{{$profile->ex_first_stage}}</p>
                                            </div>
                                        
                                        </div>
                                        <div class="second-ex-stage-div row">
                                            <div class="col-sm-2">
                                                <p class="second-ex-stage">{{$profile->date_ex_second_stage}}</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before"> 
                                            <h1 class=" section-header-titles text-uppercase font-family-alias">{{$profile->ex_second_stage_title}}</h1>
                                            <p>{{$profile->ex_second_stage}}</p>
                                            </div>
                                        
                                        </div>
                                        <div class="row" style="margin: 0px auto;">
                                            <div class="col-sm-2">
                                                <p>{{$profile->date_ex_third_stage}}</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before"> 
                                            <h1 class=" section-header-titles text-uppercase font-family-alias">{{$profile->ex_third_stage_title}}</h1>
                                            <p>{{$profile->ex_third_stage}}</p>
                                            </div>
                                        
                                        </div>
                                    </div> <!-- first riht row -->
                                </div>
                                <!-- second right row -->
                                
                            </div> <!-- second left row -->
                             <!-- third left row -->
                            <!-- <div class="row">
                                <div class="col-4 col-lg-4 col-md-12" style="margin-left: 61px;">
                                    <h1 style="background: #111111;margin: 30px auto;width: 150px;" class="white-color text-center section-header-titles text-uppercase font-family-alias">hobbies</h1>
                                    <div class="row" style="padding: 0px;">
                                        <div class="col-sm-6 row">
                                        <div class="col-sm-1">
                                            <img class="float-left cv-right-icon-img prevent-selection prevent-drag" src="{{url('assets/img/icons/high_school/high_school.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-5"><h4 class="heading-title-text">Travel</h4></div>
                                        </div>
                                        <div class="col-sm-6 row">
                                        <div class="col-sm-1">
                                            <img class="float-left cv-right-icon-img prevent-selection prevent-drag" src="{{url('assets/img/icons/high_school/high_school.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-5"><h4 class="heading-title-text">Music</h4></div>
                                        </div>
                                        <div class="col-sm-6 row">
                                        <div class="col-sm-1">
                                            <img class="float-left cv-right-icon-img prevent-selection prevent-drag" src="{{url('assets/img/icons/high_school/high_school.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-5"><h4 class="heading-title-text">Reading</h4></div>
                                        </div>
                                        <div class="col-sm-6 row">
                                        <div class="col-sm-1">
                                            <img class="float-left cv-right-icon-img prevent-selection prevent-drag" src="{{url('assets/img/icons/high_school/high_school.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-5"><h4 class="heading-title-text">Writing</h4></div>
                                        </div>
                                    </div>
                                     
                                </div>
                            </div>  -->
                            <!-- third left row -->
                            
                        </div>
                       
                    </div>
                    </div>
                </div>
            </div>
        </section>

@endsection