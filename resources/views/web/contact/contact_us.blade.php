
@extends('web.layouts.app')


@section('content')
<section id="contact" style="direction:rtl">

<link href="https://fonts.googleapis.com/css?family=Cairo&display=swap" rel="stylesheet">

@section('content')
@push('css')

        @endpush
        <section id="contact" style="direction:rtl;margin-top: 30px;font-family: 'Cairo', sans-serif;">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="title_area">
              <h2 class="title_two">تسجيل</h2>
              <span></span>
              <p>التسجيل فى الكورس</p>
            </div>
          </div>
       </div>
       <div class="row">
         <div class="col-lg-8 col-md-8 col-sm-8" style="margin:0 14%">
           <div class="contact_form wow fadeInLeft">

              <form class="submitphoto_form"  method="POST" action="{{ URl('student_register') }}">
                  @csrf
                <label style="margin-left:10px" for="branch">السنتر:</label>
{{--                  <input type="radio" id="branch" name="branche" value="Bike"> أجا--}}
{{--                  <input type="radio" name="branche" value="Car"> --}}
                  @foreach($branches as $one)
                      <input name="branches_id" type="radio" id="{{$one->id}}" value="{{$one->id}}">
                      {{session($one->name_en,$one->name_ar)}}
                  @endforeach
                  <br><br>
                <input type="text" name="name_ar" class="classroom wp-form-control wpcf7-text" placeholder="الاسم">

                <select class="wp-form-control wpcf7-text branch" name="grades_id" id="">
                        <option value="" disabled selected hidden="hidden">اختر الصف الدراسي</option>


                    @foreach($grade as $one)
                        <option value="{{$one->id}}">{{session($one->name_en,$one->name_ar)}}</option>
                        @endforeach
                </select>




                <input type="text" name="school" class="wp-form-control wpcf7-text" placeholder="اسم المدرسة">
                <input type="date" name="start_register" class="wp-form-control wpcf7-text" placeholder="اسم تاريخ البدأ">



                <select class="wp-form-control wpcf7-text " name="appointments_id" id="">
                  <option value="" hidden="hidden">اختر المجموعة</option>
                    @foreach($appointment as $one)
                  <option value="{{($one->id)}}">{{$one->appointment}}</option>

                        @endforeach
                </select>

                <!-- <input type="text" class="wp-form-control wpcf7-text" placeholder="المجموعة التى تريد التسجيل بها"> -->

                <input name="phone" type="text" class="wp-form-control wpcf7-text" placeholder="رقم التليفون">
                <input  name="parents_phone" type="text" class="wp-form-control wpcf7-text" placeholder="تليفون الوالد">
                <input  name="email" type="email" class="wp-form-control wpcf7-text" placeholder="البريد الالكترونى">
                <input  type="submit" value="تسجيل" class="wpcf7-submit">
              </form>
           </div>
         </div>

       </div>
      </div>
    </section>

    @endsection
