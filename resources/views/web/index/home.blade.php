@extends('web.layouts.app')

@section('title')
   {{trans('web.Home')}}
@endsection

@section('content')

@push('css')
<style>

.breaking-news-headline{
    margin-top: -27px;
}
.breaking-news-title{
    padding-top: 5px;
}

.section-1 .row .col-md-6 .panel{
    box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.2);
    background: none;
    top: -6.5vmin;
    left: -16vmin;
    padding: 69px;
    border-radius: 10px;
}
.profile-about-img{
    margin: 9px auto;
}
.pofile-btn {
    margin: -42px 0;
    font-family: Tahoma, Geneva, sans-serif;
    border: 3px solid transparent;
    border-radius: 4px;
}

.about-me-title{
    margin-bottom: 80px;
}
.main-side-top-title{
    margin-bottom: 40px;
    margin-top: 40px;
}
.our-tear_container_wrapper{
    padding-right: 0;
}
.team-image_wrapper{
    height: 135px;
    margin: 0 auto;
}
.team-image_wrapper:nth-child(4) {
    width: 80%;
}

.controls-top{
    float:right !important;
    margin-right:19px !important;
}



@media screen
and (min-width: 1368px)
and (max-width: 2732px){
    .section-1 .row .col-md-6 .panel{
          padding: 112px;
          left: -11vmin;
      }
}

@media screen
and (min-width: 1368px)
and (max-width: 2049px){
    .section-1 .row .col-md-6 .panel{
          padding: 90px;
      }
}

@media only screen
and (min-width: 1368px)
and (max-width: 1821px){
    .section-1 .row .col-md-6 .panel{
          padding: 205px;
      }
      .section-1 .row .col-md-6 .panel h1{
          margin: 0px 0 2% 80px;
      }

      .section-1 .row .col-md-6 .panel p{
          font-size: 1em;
      }
}

@media only screen
and (min-width: 1368px)
and (max-width: 1707px){
    .section-1 .row .col-md-6 .panel{
          padding: 190px;
      }
      .section-1 .row .col-md-6 .panel h1{
          margin: 0px 0 2% 80px;
      }

      .section-1 .row .col-md-6 .panel p{
          font-size: 1em;
      }
}

@media only screen
and (min-width: 1368px)
and (max-width: 1517px){
      .section-1 .row .col-md-6 .panel{
          padding: 54px 114px 200px;
      }
      .section-1 .row .col-md-6 .panel h1{
          margin: 0px 0 2% 80px;
      }

}

@media only screen
and (min-width: 1210px)
  and (max-width: 1242px){
      .section-1 .row .col-md-6 .panel{
          padding: 95px;
      }
  }

/*media from 1250 to 1210*/
@media only screen
  and (min-width: 1210px)
  and (max-width: 1250px){
    .section-1{
        padding: 5vmin 8% 20vmin;
    }
  }
/* labtop   */
@media only screen
and (min-width: 1188px)
 and (max-width: 1210px) {
    .section-1{
        padding: 5vmin 4% 20vmin;
    }
    .pofile-btn {
        padding: .3rem .8rem;
        /* bottom: -41px; */
        bottom: 5px;
    }
}


@media (max-width: 1020px) {
    .section-1 .row .col-md-6 .panel{
          padding: 90px;
      }
      .pofile-btn{
          bottom: -14px;
          margin-top:0;
      }
}

@media (max-width: 992px) {
    /* profile -about me in home page-*/
    .pofile-btn {
        margin: 0px 40px;
    }
    .section-1 {
        padding: 0vmin 0% 15vmin;
    }
}
/*  */
@media only screen
and (min-width: 995px)
 and (max-width: 1187px) {
    .section-1{
        padding: 5vmin 1% 20vmin;
    }

    .pofile-btn {
        margin: 2px 40px;
        padding: .3rem .8rem;
    }
}

@media only screen
and (min-width: 905px)
 and (max-width: 995px) {

    .section-1 .row .profile-respomsive-content .panel{
        padding: 90px;
    }

    .pofile-btn {
        padding: .3rem .6rem;
        bottom: -24px;
    }
 }
@media only screen
and (min-width: 850px)
 and (max-width: 905px) {

    .section-1 .row .profile-respomsive-content .panel{
        padding: 75px;
    }

    .pofile-btn {
        padding: .3rem .7rem;
        bottom: -18px;
    }
 }
@media only screen
and (min-width: 805px)
 and (max-width: 850px ) {

    .section-1 .row .profile-respomsive-content .panel{
        padding: 60px;
    }

    .pofile-btn {
        padding: .3rem .7rem;
        bottom: -12px;
    }
 }
@media only screen
and (min-width: 705px)
 and (max-width: 805px) {

    .section-1 .row .profile-respomsive-content .panel{
        padding: 23px;
    }

    .pofile-btn {
        padding: .2rem .5rem;
        bottom: -1px;
    }
 }

@media (max-width: 768px) {
    .main-slider-div{
        height:271px;
    }
    .hover-padding{
        display: none;
    }
    .slider-head-text{
        margin: 0;
    }
    .head-text-slider .english-text {
        font-size: 40px;
        line-height: 52px;
        margin: 0;
    }

    .search-form{
        left:3px;
        top: -115px
    }

    .img{
        width: 107px;
        height: 105px;
        font-size: .4rem;
    }
    .img .col-sm-1{
        margin-left: -9px;
    }
    .slider-gate-img {
        margin-top: 25px;
        margin-right: -10px;
        width: 28px;
    }
    .heading-title-gate {
        margin: 7px 0;
        font-size: 1rem;
    }

    .watch-video{
        bottom: 61px;
        left: 18px;
        height: 87px;
        width: 85px;

    }
    /* section-1 */
    .section-1 {
        padding: 1vmin 0% 5vmin;
    }
    .profile-about-img{
        border: none;
        border-radius: none;
        box-shadow: 0;
        margin: 0;
    }
    .section-1 .about-img-responsive{
        position: absolute;
        top: 1.5vmin;
        left: -63px;
        border: 3px solid #ffb606;
        border-radius: 4px;
        box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.2);
        margin: 50px auto;
    }
    .profile-name-responsive{
        display: none;
    }


      .about-me-title{
        margin-bottom: 0;
      }
      .section-1 .row .profile-respomsive-content .panel{
        /*position: absolute;*/
        /*top: -0.5vmin;*/
        /*left: -7vmin;*/
        font-family: Tahoma, Geneva, sans-serif;
        padding: 21px;
        font-size: .7rem;
    }

    .section-1 .row .profile-respomsive-content .panel p{
        margin-left: 26px;
    }
    .pofile-btn{
        padding: 2px;
        bottom: 7px;
        right: 0px;
    }




    /* start honor board area*/
    /* start honor board area carousel*/
      .carousel-inner.vv-3 .carousel-item.active,
      .carousel-inner.vv-3 .carousel-item-next,
      .carousel-inner.vv-3 .carousel-item-prev {
        display: flex;

      }

      .carousel-inner.vv-3 .carousel-item-right.active,
      .carousel-inner.vv-3 .carousel-item-next {
        transform: translateX(100%)
      }


      .carousel-inner.vv-3 .carousel-item-left.active,
      .carousel-inner.vv-3 .carousel-item-prev {
        transform: translateX(-100%)
      }

      .carousel-inner.vv-3 .carousel-item-right,
      .carousel-inner.vv-3 .carousel-item-left {
        transform: translateX(0);
        transform: translateY(0);
      }

      /* our team*/
      .our-team-heading-name{
        font-size: .7rem;
      }

      /*parents follow up page*/
      .parents-follow-up .nav-pills{
        margin-bottom: 5px;
      }
      .parents-follow-up .nav-pills .nav-link{
          margin-bottom: 5px;
      }
      .paymet-last-nav{
        position: relative;
        right: 0;
    }
      /*parents follow up page*/

      .c-datepicker-date-picker.has-sidebar{
        width: 395px;
    }

    /*references*/



}

@media (max-width: 768px) {
    .width-90{
        width:90%;
    }
}

@media (max-width: 500px){
    .mobile-d-none{
        display:none;
    }
    .img {
        width: 75px;
        height: 78px;
    }
    .heading-title-gate{
        text-align: center!important;
    }
    .head-text-slider .english-text {
        font-size: 18px;
    }
    .search-form{
        top: -150px;
    }
    .section-1 .about-img-responsive{
        left: -20px;
    }

    .why-us-man-img {
        margin-top: 0;
        width: 200px;
        margin-left: 45px;
    }
    .team-image_wrapper{
        height: 100px;
    }
    .controls-top{
        float: initial !important;
        /*margin-right: 50px !important;*/
    }
    .honor-board-container{
        padding-left: 0;
    }
    .cta-100{
        padding-left: 0;
        padding-right: 0;
    }
}


</style>
@endpush

@push('js')


<script>
// tooltip in right sidebar
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});

// fade out to input say your opinion
$("#say-about-input").focus(function(){
  $("#say-about-form").fadeIn(400, function(){
    $("#containerfadeoutinput").fadeOut();
  });
});

</script>
@endpush

@if (Session::has('success'))
<div class="alert alert-success">
<a href="#" class="close" data-dismiss="alert">&times;</a>
{{ Session::get('success') }}
</div>
@endif
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        @foreach(slider() as $key=>$slider)
@if($key == 0)
            <div class="carousel-item main-slider-div slider-img carousel-item {{$key == 0 ? 'active' : ' '}}">
                <img src="{{asset('storage/'. $slider->image)}}" class="d-block w-100 img-slider prevent-selection prevent-drag" alt="">>
                <div class="carousel-caption d-none d-md-block" style='transform: translate(0%, -150%);'>
                    <h2 class="color-white slider-head-text" style="color: #fff">{{$slider->quote}}</h2>
                    <h1 class="orange-main-color english-text" style="font-family: 'Jomolhari', serif;">{{$slider->quote_owner}}</h1>
                </div>
            </div>
            @endif
        @endforeach

    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
    <!--  ======================= start slider Area ============================== -->




{{--    <div class="container search-container font-family-alias">--}}
{{--     --}}

{{--        <div class="row justify-content-center search-form">--}}
{{--            <div class="col-12 col-md-10 col-lg-10">--}}

{{--                <div class="row">--}}
{{--                    <div class="col-3 col-md-3 col-lg-3 col-sm-3 slider-gates-shape offset-1">--}}
{{--                        <a class="show-more orange-main-color" href="{{url('high-school-gate')}}">--}}
{{--                            <div class="img">--}}
{{--                                <div class="col-sm-1 mobile-d-none">--}}
{{--                                    <img class="float-left slider-gate-img prevent-selection prevent-drag" src="{{url('/assets/img/icons/high_school/high_school.png')}}"  alt="email-icon"></div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-sm-12"><h4 class="heading-title-gate">High School Gate</h4></div>--}}
{{--                                    <div class="col-sm-12 mobile-d-none"> <a class="show-more orange-main-color" href="{{url('high-school-gate')}}">Show more  <span class="hover-padding"><i class="fa fa-angle-right"></i></span></a></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <div class="col-3 col-md-3 col-lg-3 col-sm-3 slider-gates-shape">--}}
{{--                        <a class="show-more orange-main-color" href="{{url('english-gate')}}">--}}
{{--                            <div class="img">--}}
{{--                                <div class="col-sm-1 mobile-d-none">--}}
{{--                                    <img class="float-left slider-gate-img prevent-selection prevent-drag" src="{{url('/assets/img/icons/english_gate/english_gate.png')}}"  alt="email-icon"></div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-sm-12"><h4 class="heading-title-gate">English Language Gate</h4></div>--}}
{{--                                    <div class="col-sm-12 mobile-d-none"> <a class="show-more orange-main-color" href="{{url('english-gate')}}">Show more  <span class="hover-padding"><i class="fa fa-angle-right"></i></span> </a></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}

{{--                        <a href="{{route('parentLogin')}}">--}}
{{--                            <div class="col-3 col-md-3 col-lg-3 col-sm-3 slider-gates-shape">--}}
{{--                            <a class="show-more orange-main-color" href="{{route('parentLogin')}}">--}}
{{--                                <div class="img">--}}
{{--                                    <div class="col-sm-1 mobile-d-none">--}}
{{--                                        <img class="float-left slider-gate-img prevent-selection prevent-drag" src="{{url('/assets/img/icons/parents/parents.png')}}"  alt="email-icon"></div>--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-lg-12 "><h4 class="heading-title-gate"><a class="heading-title-gate-anchor" href="{{route('parentLogin')}}">Parents Follow-up</a> </h4></div>--}}
{{--                                        <div class=" col-lg-12 mobile-d-none"> <a class="show-more orange-main-color" href="{{route('parentLogin')}}">Show more  <span class="hover-padding"><i class="fa fa-angle-right"></i></span></a></div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            </div>--}}
{{--                        </a>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!--  ======================= End slider Area ============================== -->


    <!--  ======================= Start Main Area ================================ -->
    <div class="parents-sign-up-container d-flex  align-items-center justify-content-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-12 py-5">
    <main class="site-main font-family-alias">
        <div class="padding-columns">
        <!--  ======================= Start Banner Area =======================  -->
        <!-- <section class="site-banner">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-4 col-lg-6 col-md-12 site-title">
                        <h3 class="title-text">Hey</h3>
                        <h1 class="title-text text-uppercase">I am Aly Elhariry</h1>
                        <h4 class="title-text text-uppercase">Senior English Teacher</h4>
                        <div class="site-buttons">
                            <div class="d-flex flex-row flex-wrap">
                                <button type="button" class="btn button primary-button mr-4 text-uppercase">show
                                    more</button>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 banner-image">
                        <img src="./img/banner/banner-image.png" alt="banner-img" class="img-fluid">
                    </div>
                </div>
            </div>
        </section> -->
        <!--  ======================= End Banner Area =======================  -->
        <!--  ======================= start section one [pofile] Area =======================  -->
        <section class="section-1 font-family-alias">
            <div class="container">
                <div class="about-me-title">
                    <h1 class="section-header-titles text-uppercase orange-main-color font-cairo">{{trans('web.ABOUT')}}</h1>
                    <span class="title_area_span"></span>
                </div>
                <div class="row  ">
                    <div class="col-3 col-lg-3 col-md-3 col-sm-3">
                        <div class="pray about-image profile-about-img">
                            <img src="{{asset('storage/'.$profile->image)}}" alt="Ali Elhariry" title="Ali Elhariry" class="img-fluid about-img-responsive">
                        </div>
                    </div>
                    <div class="col-12 col-lg-9 col-md-6 col-sm-12 profile-respomsive-content">
                        <div class="panel text-left">
                            <h1 class="profile-name-responsive">{{$profile->name_en}}</h1>
                            <p>{{$profile->profile_desc_en}}</p>
                        </div>
                        <a href="{{url('profile')}}" type="button" class="btn text-uppercase float-right pofile-btn">Read more <span class="hover-padding"><i class="fa fa-angle-right"></i></span> </a>
                    </div>
                </div>
            </div>
        </section>




        <!-- <section class="section-1 font-family-alias">
            <div class="container">
                <div class="about-me-title">
                    <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">about me</h1>
                    <span class="title_area_span"></span>
                </div>
                <div class="row  ">
                    <!-- <div class="col-3 col-lg-3 col-md-3 col-sm-3">
                        <div class="pray about-image profile-about-img">
                            <img src="{{asset('storage/'.$profile->image)}}" alt="Pray" class="img-fluid">
                        </div>
                    </div> -->
                    <!-- <div class="col-9 col-lg-9 col-md-9 col-sm-9 offset-3">
                    <img src="{{asset('storage/'.$profile->image)}}" alt="Pray" class="img-fluid">
                        <div class="panel text-left">
                            <h1>{{$profile->name_en}}</h1>
                            <p>{{$profile->profile_desc_en}}</p>
                        </div>
                        <a href="{{url('profile')}}" type="button" class="btn button  text-uppercase float-right pofile-btn">Read more <span class="hover-padding"><i class="fa fa-angle-right"></i></span> </a>
                    </div>
                </div>
            </div>
        </section> -->



        <!--  ======================= end section one Area =======================  -->




            <!--  ========================= About me Area ==========================  -->
            <!-- <section class="brand-area about-area about-me-background" style="padding: 3% 10%;">
                <div class="container">
                    <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">about me</h1>
                    <span class="title_area_span"></span>
                    <div class="row justify-content-center">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-3 col-md-12">
                                    <div class="about-image profile-about-img">
                                        <img src="{{url('assets/img/mr-alii.png')}}" alt="About us" class="img-fluid">
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-12 about-title about-me-right-section">

                                    <div class="paragraph py-4" style="width: 600px;margin-top: -41px;">
                                        <p class="para font-cairo" style="padding-left: 0;padding: 1rem 7rem;">
                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Error rerum iure obcaecati vel
                                            possimus officia maiores perferendis ut! Quos, perspiciatis.
                                            It is a long established fact that a reader will be distracted by the readable content
                                            of a page when looking at its layout. The point of using Lorem Ipsum is that it has a
                                            more-or-less normal distribution of letters, as opposed to using 'Content here, content
                                            here
                                        </p>

                                    </div>
                                    <button style="margin: -73px 241px;padding: .5rem 1rem;" type="button" class="btn button primary-button text-uppercase float-right pofile-btn">Show more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->
            <!--  ========================= End About me Area ==========================  -->




            <!--  ======================= start why us Area =======================  -->
            <section class="brand-area font-family-alias">
                <div class="container">
                    <div class="col-md-6 mx-auto">@include('web.layouts.error')</div>
                <div class="main-side-top-title">
                    <h1 class="section-header-titles text-uppercase orange-main-color font-cairo">@if(\App::getLocale() == 'en'){{$feature->features_title_en}} @else {{$feature->features_title_ar}} @endif</h1>
                    <span class="title_area_span"></span>
                </div>

                    <div class="row justify-content-center text-center">
                            <div class="col-xl-4 col-lg-12 col-md-12">

                                <img src="{{asset('storage/'.$feature->main_image)}}" alt="About us" class="prevent-selection prevent-drag img-fluid why-us-man-img">
                            </div>
                            <!-- why us right section -->
                            <div class="col-xl-8 col-lg-12 col-md-12">
                            <div class="first-row row">
                                <div class="row">
                                    <div class="about-us-right col-lg-6 col-md-6 col-sm-6">

                                            <!-- <img class="prevent-selection prevent-drag single-brand" src="{{url('assets/img/why-us/best_techniques/best_techniques.png')}}" alt="best techniques img"> -->
                                            <div class="single-brand">
                                                <img src="{{asset('storage/'.$feature->image_1)}}" alt="Brand-1 ">
                                            </div>

                                        <p class="features-text text-center">@if(\App::getLocale() == 'en'){{$feature->title_1_en}} @else {{$feature->title_1_ar}} @endif</p>
                                    </div>
                                    <div class="about-us-right col-lg-6 col-md-6 col-sm-6">
                                        <div class="single-brand">
                                            <img src="{{asset('storage/'.$feature->image_2)}}" alt="Brand-2 ">
                                        </div>
                                        <!-- <img class="prevent-selection prevent-drag single-brand" src="{{url('assets/img/why-us/best_teacher/best_teacher.png')}}" alt="Best Teacher img"> -->
                                        <p class="features-text text-center">@if(\App::getLocale() == 'en'){{$feature->title_2_en}} @else {{$feature->title_2_ar}} @endif</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="about-us-right col-lg-6 col-md-6 col-sm-6">
                                        <div class="single-brand">
                                            <img src="{{asset('storage/'.$feature->image_3)}}" alt="Brand-2 ">
                                        </div>
                                    <!-- <img class="prevent-selection prevent-drag single-brand" src="{{url('assets/img/why-us/summer_courses/summer_courses.png')}}" alt="Best Courses img"> -->
                                        <p class="features-text text-center">@if(\App::getLocale() == 'en'){{$feature->title_3_en}} @else {{$feature->title_3_ar}} @endif</p>
                                    </div>
                                    <div class="about-us-right col-lg-6 col-md-6 col-sm-6">
                                        <div class="single-brand">
                                            <img src="{{asset('storage/'.$feature->image_4)}}" alt="Brand-2 ">
                                        </div>
                                    <!-- <img class="prevent-selection prevent-drag single-brand" src="{{url('assets/img/why-us/support_students/support_students.png')}}" alt="Support students img"> -->
                                        <p class="features-text text-center">@if(\App::getLocale() == 'en'){{$feature->title_4_en}} @else {{$feature->title_4_ar}} @endif</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <!-- why us right section -->
                    </div>
                </div>
            </section>
            <!--  ======================= End why us Area =======================  -->
            <!--  ======================= start honor board Area =======================  -->
            <div class="container my-4 cta-100 honor-board-container font-family-alias">
                    <!-- <hr class="my-4"> -->
                    <div class="head-section" style="padding: 0 1%;">
                        <div class="main-side-top-title">
                            <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">{{trans('web.Honor_Frame')}}</h1>
                            <span class="title_area_span"></span>
                        </div>
                    </div>
                        <!-- Grid column -->
                    <div class=" text-center my-3">

                        <!-- <h2>Bootstrap 4 Multiple Item Carousel</h2> -->
                        <div class="row mx-auto my-auto">
                        <div id="recipeCarousel2" class="honor-board-carusel honor-carousel carousel slide w-100 " data-ride="carousel">
                            <!--Controls-->
                        <div class="controls-top">
                            <a class="btn-floating" href="#recipeCarousel2" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                            <a class="btn-floating" href="#recipeCarousel2" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                        </div>
                        <!--/.Controls-->
                            <div class="carousel-inner w-100 vv-3" role="listbox">
                                @foreach($honorboards->chunk(3) as $key => $honorboard)
                                <div class="carousel-item {{$key == 0 ? 'active' : ''}} ">
                                    @foreach($honorboard as $honor)
                                    <div class="col-12 col-lg-4 col-md-6 col-sm-12 padding-none">
                                        <div class="card mb-2">
                                            @if($honor->student)
                                                @if($honor->student->image)


                                                    <img  class="card-img-top prevent-selection prevent-drag border-honor-img" src="{{asset('storage/'.$honor->student->image)}}"
                                                    alt="Card image cap">
                                                    @else
                                                    <img  class="card-img-top prevent-selection prevent-drag border-honor-img" src="{{ url('images/honour.png') }}"
                                                    alt="Card image cap">
                                                @endif
                                            <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                <div class="item-box-number">

                                                    <h5 class="honor-student-number">1</h5>

                                                </div>
                                                <div class="item-box-blog-heading">

                                                    <h5 style="color: #000;">{{$honor->student->name_en}}</h5>

                                                </div>
                                                <!--Data-->
                                                <div class="item-box-blog-data" style="padding: px 15px;">
                                                <p class="honor-student-year"><i class="fa fa-user-o"></i>{{$honor->student->grade ? $honor->student->grade->name_en : ''}}</p>
                                                </div>
                                                <!--Text-->
                                                <div class="item-box-blog-text">
                                                <p class="honor-desc">Kind of Excellence</p>
                                                </div>
                                                <!--<div class="mt"> <i class="fa fa-heart"></i></div>-->
                                                <!--<div class="mt-right">-->
                                                <!--    <i class="fa fa-star star-hover b1" title="Excellent"></i>-->
                                                <!--    <i class="fa fa-star star-hover b2" title="Good"></i>-->
                                                <!--    <i class="fa fa-star star-hover b3" title="ok"></i>-->
                                                <!--    <i class="fa fa-star star-hover b4" title="poor"></i>-->
                                                <!--    <i class="fa fa-star star-hover b5" title="usless"></i>-->
                                                <!--</div>-->
                                                <!--Read More Button-->
                                            </div>
                                             @endif
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                @endforeach

                            </div>

                        </div>
                        </div>

                    </div>
                        <!-- Grid column -->
                <!-- Grid row -->
            </div>
            <!--  ======================= End honor board us Area =======================  -->

            <!--  ======================= start our team Area =======================  -->


            <!--<div class="container my-4 cta-100 font-family-alias">-->
                    <!-- <hr class="my-4"> -->
            <!--    <div class="main-side-top-title">-->
            <!--        <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">Our Team</h1>-->
            <!--        <span class="title_area_span"></span>-->
            <!--    </div>-->
            <!--    <div class="container-fluid our-tear_container_wrapper">-->
                    <!-- Grid row -->
            <!--        <div class="row">-->
                        <!-- Grid column -->
                                <!-- <h2>Bootstrap 4 Multiple Item Carousel</h2> -->
            <!--                    <div class="row mx-auto my-auto">-->
            <!--                        <div class=" w-100 ">-->
            <!--                            <div class="our-team-carousel-inner carousel-inner w-100 vv-3 row" role="listbox">-->
            <!--                            @foreach($teams as $team)-->
            <!--                                <div class="col-6 col-lg-3 col-md-6 col-sm-6">-->
            <!--                                    <div class="card mb-2 our-team-card">-->
            <!--                                        <img  class="team-image_wrapper border-honor-img card-img-top prevent-selection prevent-drag" src="{{asset('storage/'.$team->image)}}"-->
            <!--                                        alt="Card image cap">-->

            <!--                                        <div class="item-box-blog-body text-center">-->
                                                            <!--Heading-->
            <!--                                            <div class="text-center item-box-blog-heading out-tema-name">-->
            <!--                                            <div id="prevent-default" href="#" tabindex="0">-->
            <!--                                                <h5 class="our-team-heading-name">{{$team->name_en}}</h5>-->
            <!--                                            </div>-->
            <!--                                            </div>-->
                                                        <!--Data-->
            <!--                                            <div class="item-box-blog-data" style="padding: px 15px;">-->
            <!--                                            <p class="out-team-title"><i class="fa fa-user-o"></i>{{$team->teams_title_en}}</p>-->
            <!--                                            </div>-->
                                                        <!--Text-->

                                                        <!--Read More Button-->
            <!--                                        </div>-->
            <!--                                    </div>-->
            <!--                                </div>-->
            <!--                                @endforeach-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                    </div>-->
                        <!-- Grid column -->

            <!--        </div>-->
                <!-- Grid row -->
            <!--    </div>-->
            <!--</div>-->

            <!--  ======================= End our team Area =======================  -->

            <!--  ======================= start what say about Area =======================  -->

            <div class="container my-4 cta-100 font-family-alias">
                <!-- <hr class="my-4"> -->
                <div class="main-side-top-title">
                    <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">{{trans('web.What_people_say_about_us')}}</h1>
                    <span class="title_area_span"></span>
                </div>
                <div id="our-team" class="row">

                        @foreach($sayabouts as $sayabout)
                        <div class="col-12 col-md-12 col-lg-12 col-sm-12 margin-bottom">
                            <div class="say-about-div row">
                                <div class=" col-2 col-lg-1 col-md-1 col-sm-1 say-about-div-img">
                                    <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag border-honor-img" src="{{url('assets/img/student-img.png')}}" alt="Card image cap">
                                </div>
                                <div class="col-10 col-lg-10 col-md-10 col-sm-10">
                                    <div class="col-10 col-lg-12 col-md-12 col-sm-9"><h4 class="heading-title-say">{{$sayabout->name}} <span class="say-about-span">{{$sayabout->graduation_year}} - {{$sayabout->current_work}}</span></h4></div>
                                    <div class="col-10 col-lg-12 col-md-12 col-sm-9"> <a class="our-team-comment-text" href="#"> {{$sayabout->comment}}</a></div>
                                    <div class="col-10 col-lg-12 col-md-12 col-sm-9"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">  <!-- reply --> </a> <span class="say-about-span">{{$sayabout->updated_at}}</span></div>
                                </div>
                            </div>
                        </div>
                        @endforeach


                        <!--form -->
                        <div id="containerfadeoutinput" class=" col-12 col-md-12 col-lg-12 col-sm-12 margin-bottom" style="position: absolute; bottom: 0;width: 73%;">
                            <div class="say-form say-about-div row">
                                <div class="col-12 col-md-12 col-lg-12 col-sm-12" style="padding: 8px;">
                                    <form method="POST" style="display: flex;">
                                        <input type="hidden" name="" value="">
                                        <input id="say-about-input" type="text" name="comment" placeholder="{{trans('web.Leave_comment')}}" class="form-control" style="border-radius: 0;border: none;border-bottom: 1px solid #d1d1d1;">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--form -->



                </div>
            </div>
            <!--  ======================= end what say about Area =======================  -->

            <!--say about login form -->
            <div class="container my-4 cta-100 font-family-alias">
                <div id="say-about-form" class=" col-12 col-md-12 col-lg-12 col-sm-12 margin-bottom display-none" >
                    <div class="say-form say-about-div row">
                        <div class="col-12 col-md-12 col-lg-12 col-sm-12 margin-bottom" style="padding: 8px;">
                            <form method="POST" action="{{route('sayabout.store')}}" style="display: flex;margin-left: 20px;" class="row"  enctype="multipart/form-data">
                            {{csrf_field()}}
                                <input type="hidden" name="" value="">

                                <input type="text" name="name" placeholder="{{trans('web.Name')}}" class="col-sm-11 form-control width-90" style="border-radius: 0;border: none;border-bottom: 1px solid #495057;">
                                <select name="graduation_year" id="inputState" class="col-sm-11 form-control width-90" style="border: none;border-bottom: 1px solid #555;border-radius: 0">
                                    {{ $i = 0}}
                                     @for($i = 1998; $i <= 2050; $i++)
                                     <option disabled selected>{{trans('web.graduation_year')}}</option>
                                    <option value="{{$i}}" >{{$i}}</option>
                                    @endfor
                                </select>
                                <input type="text" name="current_work" placeholder="{{trans('web.Current_work')}}" class="col-sm-11 form-control width-90" style="border-radius: 0;border: none;border-bottom: 1px solid #495057;">
                                <input type="text" name="email" placeholder="{{trans('web.Email')}}" class="col-sm-11 form-control width-90" style="border-radius: 0;border: none;border-bottom: 1px solid #495057;">
                                <input type="text" name="comment" placeholder="{{trans('web.Say_opinion')}}" class="col-sm-11 form-control width-90" style="border-radius: 0;border: none;border-bottom: 1px solid #495057;">
                                <div class="col-sm-6 justify-content-center" style="margin-top: 15px;">
                                    <div class="btn-submit">
                                        <button type="submit" class="say-about-button btn float-right button text-uppercase">{{trans('web.Submit')}}</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--say about login form -->


        </div> <!--end of padding-div elements-->
    </main>

    </div>
        </div>
    </div>
  </div>
    <!--  ======================= End Main Area ================================ -->




 <!--  ======================= start chat Area ================================ -->

 <!--   <div id="chatbox" class="jumbotron m-0 p-0 bg-transparent display-none">-->
	<!--	<div class="row m-0 p-0 position-relative">-->
	<!--	  <div class="chat-messages-container col-3 p-0 m-0" >-->
	<!--		<div class=" card border-0 rounded" style="z-index:555; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.10), 0 6px 10px 0 rgba(0, 0, 0, 0.01);">-->
 <!--               <div class="card-header p-1 bg-light border border-top-0 border-left-0 border-right-0" style="color: rgba(96, 125, 139,1.0);">-->

 <!--                   <img class="chat-main-img  float-left" src="{{url('assets/img/mr-alii.png')}}" />-->

 <!--                   <h6 class="chat-main-title float-left"> Mr. Ali Elhariry</br></h6>-->

 <!--                   <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button"><i class="fa fa-remove"></i></button>-->

 <!--                   <div class="dropdown show">-->
 <!--                       <div class="dropdown-menu dropdown-menu-right border p-0" aria-labelledby="dropdownMenuLink">-->

 <!--                           <a class="dropdown-item p-2 text-secondary" href="#"> <i class="fa fa-user m-1" aria-hidden="true"></i> Profile </a>-->
 <!--                           <hr class="my-1">-->
 <!--                           <a class="dropdown-item p-2 text-secondary" href="#"> <i class="fa fa-trash m-1" aria-hidden="true"></i> Delete </a>-->

 <!--                       </div>-->
 <!--                   </div>-->

 <!--               </div>-->
 <!--               <div id="chat-body" class="card bg-sohbet border-0 m-0 p-0" style="overflow:auto; height: 315px;">-->
 <!--                   <div id="sohbet" class="card border-0 m-0 p-0 position-relative bg-transparent">-->

 <!--                       <div class="balon1 p-2 m-0 position-relative" data-is="You - 3:20 pm">-->

 <!--                           <a class="float-right"> Hey there! What's up? </a>-->

 <!--                       </div>-->

 <!--                       <div class="balon2 p-2 m-0 position-relative" data-is="Mr. Ali Elhariry - 3:22 pm">-->

 <!--                           <a class="float-left sohbet2"> Checking out iOS7 you know.. </a>-->

 <!--                       </div>-->

 <!--                       <div class="balon1 p-2 m-0 position-relative" data-is="You - 3:23 pm">-->

 <!--                           <a class="float-right"> Check out this bubble! </a>-->

 <!--                       </div>-->

 <!--                       <div class="balon2 p-2 m-0 position-relative" data-is="Mr. Ali Elhariry - 3:26 pm">-->

 <!--                           <a class="float-left sohbet2"> It's pretty cool! </a>-->

 <!--                       </div>-->

 <!--                       <div class="balon1 p-2 m-0 position-relative" data-is="You - 3:28 pm">-->

 <!--                           <a class="float-right"> Yeah it's pure CSS & HTML </a>-->

 <!--                       </div>-->

 <!--                       <div class="balon2 p-2 m-0 position-relative" data-is="Mr. Ali Elhariry - 3:33 pm">-->

 <!--                           <a class="float-left sohbet2"> Wow that's impressive. But what's even more impressive is that this bubble is really high. </a>-->

 <!--                       </div>-->

 <!--                   </div>-->
 <!--               </div>-->
 <!--           </div>-->
 <!--           <div class="w-100 card-footer p-0 bg-light border border-bottom-0 border-left-0 border-right-0">-->
 <!--               <form class="m-0 p-0" action="" method="POST" autocomplete="off">-->
 <!--                   <div class="row m-0 p-0">-->
 <!--                   <div class="col-9 m-0 p-1">-->
 <!--                       <input id="text" class="mw-100 border rounded form-control" type="text" name="text" title="Type a message..." placeholder="Type a message..." required>-->
 <!--                   </div>-->
 <!--                   <div class="col-3 m-0 p-1">-->
 <!--                       <button class="btn btn-outline-secondary rounded border w-100" title="Send!" style="padding-right: 16px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>-->
 <!--                   </div>-->
 <!--                   </div>-->
 <!--               </form>-->
 <!--           </div>-->
	<!--	  </div>-->
	<!--	</div>-->
	<!--</div>-->
 <!--  ======================= End chat Area ================================ -->

<!--  ======================= start test now Area ================================ -->
<!--<div id="translatorcontainer" class=" mb-3 videos-container position-relative display-none">-->
<!--    <div class=" no-gutters translate-container">-->

<!--    <button data-widget="remove" id="closetranslate" class="chat-header-button pull-right" type="button"><i class="fa fa-remove"></i></button>-->
<!--    <h6 class="chat-main-title float-left margin-bottom"> Translate </br></h6>-->

<!--    <textarea id="translatortext" class="mw-100 border rounded form-control" rows="4" cols="50" style="resize: none;" placeholder="type to translate ..."></textarea>-->

    <!-- <input id="text" class="mw-100 border rounded form-control" type="text" name="text" title="Type to translate" placeholder="Type to translate..." required> -->
<!--    <p class="chat-main-title text-center margin-bottom">-->
<!--    عبارات الترجمة  عبارات الترجمة عبارات الترجمة عبارات الترجمة عبارات الترجمة عبارات الترجمة عبارات الترجمة عبارات الترجمة-->
<!--    </p>-->
<!--    </div>-->
<!--</div>-->
<!--  ======================= start test now Area ================================ -->

<!--  ======================= start test now Area ================================ -->
<!--<div id="onlinetest" class=" mb-3 videos-container position-relative display-none">-->
<!--    <div class=" no-gutters testnow-buttons-container">-->

<!--    <button data-widget="remove" id="closeonlinetest" class="chat-header-button pull-right" type="button"><i class="fa fa-remove"></i></button>-->
<!--    <h6 class="chat-main-title text-center margin-bottom"> Online Test</br></h6>-->
<!--        <ul class="nav nav-pills row" id="pills-tab" role="tablist">-->
<!--            <li class="nav-item col-lg-6 col-md-6 col-sm-6">-->
<!--                <a class="default-main-border text-center nav-link testnow-nav" href="{{url('onlinetest')}}">English student?</a>-->
<!--            </li>-->
<!--            <li class="nav-item col-lg-6 col-md-6 col-sm-6">-->
<!--                <a class="default-main-border text-center nav-link testnow-nav" href="{{url('onlinetest')}}">Secondary student?</a>-->
<!--            </li>-->

<!--        </ul>-->
<!--    </div>-->
<!--</div>-->
<!--  ======================= start test now Area ================================ -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

@endsection
