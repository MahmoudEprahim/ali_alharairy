<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png" />
	<title>Paper Bootstrap Wizard by Creative Tim | Free Bootstrap Wizard</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
	<!-- Canonical SEO -->
    <link rel="canonical" href="https://www.creative-tim.com/product/paper-bootstrap-wizard"/>
    <meta name="keywords" content="wizard, bootstrap wizard, creative tim, long forms, 3 step wizard, sign up wizard, beautiful wizard, long forms wizard, wizard with validation, paper design, paper wizard bootstrap, bootstrap paper wizard">
    <meta name="description" content="Paper Bootstrap Wizard is a fully responsive wizard that is inspired by our famous Paper Kit  and comes with 3 useful examples and 5 colors.">
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Paper Bootstrap Wizard by Creative Tim">
    <meta itemprop="description" content="Paper Bootstrap Wizard is a fully responsive wizard that is inspired by our famous Paper Kit  and comes with 3 useful examples and 5 colors.">
    <meta itemprop="image" content="https://s3.amazonaws.com/creativetim_bucket/products/49/opt_pbw_thumbnail.jpg">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@creativetim">
    <meta name="twitter:title" content="Paper Bootstrap Wizard by Creative Tim">
    <meta name="twitter:description" content="Paper Bootstrap Wizard is a fully responsive wizard that is inspired by our famous Paper Kit  and comes with 3 useful examples and 5 colors.">
    <meta name="twitter:creator" content="@creativetim">
    <meta name="twitter:image" content="https://s3.amazonaws.com/creativetim_bucket/products/49/opt_pbw_thumbnail.jpg">

    <!-- Open Graph data -->
    <meta property="og:title" content="Paper Bootstrap Wizard by Creative Tim | Free Boostrap Wizard" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://demos.creative-tim.com/paper-bootstrap-wizard/wizard-list-place.html" />
    <meta property="og:image" content="https://s3.amazonaws.com/creativetim_bucket/products/49/opt_pbw_thumbnail.jpg" />
    <meta property="og:description" content="Paper Bootstrap Wizard is a fully responsive wizard that is inspired by our famous Paper Kit  and comes with 3 useful examples and 5 colors." />
    <meta property="og:site_name" content="Creative Tim" />

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{url('/assets/css/wizered/bootstrap.min.css')}}">
	<link href="{{url('/assets/css/wizered/paper-bootstrap-wizard.css')}}" rel="stylesheet" />

	<!-- CSS Just for demo purpose, don't include it in your project -->
	

	<!-- Fonts and Icons -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
	<link href="{{url('/assets/css/wizered/themify-icons.css')}}" rel="stylesheet">
	
	<style>
		.wizard-card .tab-content{
			padding: 15px 20px 10px;
		}
	</style>
</head>

	<body>
		<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div class="image-container set-full-height" style="background-image: url('/assets/wizered/img/paper-2.jpeg')">
	    
	    <!--   Big container   -->
	    <div class="container">
	        <div class="row">
		        <div class="col-sm-8 col-sm-offset-2">

		            <!--      Wizard container        -->
		            <div class="wizard-container">
		                <div class="card wizard-card" data-color="red" id="wizard">
		                <form action="" method="">
		                <!--You can switch " data-color="green" "  with one of the next bright colors: "blue", "azure", "orange", "red"-->
		                    	<div class="wizard-header">
		                        	<h3 class="wizard-title">List your place</h3>
		                        	<p class="category">This information will let us know more about your place.</p>
		                    	</div>
								
		                        <div class="tab-content" >
		                            <div class="tab-pane row" id="location" style="height: 400px !important;">
		                            	  
										
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s2.png')}}" alt="Services-2">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">location</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s2.png')}}" alt="Services-2">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">location</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s3.png')}}" alt="Services-3">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">web design</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s4.png')}}" alt="Services-4">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">seo optimize</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
		                            </div>
		                            <div class="tab-pane row" id="type" style="height: 400px !important;">
		                            	  
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s1.png')}}" alt="Services-1">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">Wp developer</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s2.png')}}" alt="Services-2">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">ux/ui desing</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s3.png')}}" alt="Services-3">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">web design</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s4.png')}}" alt="Services-4">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">seo optimize</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
		                            </div>
		                            <div class="tab-pane row" id="facilities" style="height: 400px !important;">
		                            	  
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s1.png')}}" alt="Services-1">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">facilities</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s2.png')}}" alt="Services-2">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">ux/ui desing</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s3.png')}}" alt="Services-3">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">web design</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s4.png')}}" alt="Services-4">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">seo optimize</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
		                            </div>
		                            <div class="tab-pane row" id="description" style="height: 400px !important;">
		                            	  
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s1.png')}}" alt="Services-1">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">description</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s2.png')}}" alt="Services-2">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">ux/ui desing</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s3.png')}}" alt="Services-3">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">web design</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-6 col-sm-12">
											<div class="services">
												<div class="sevices-img text-center py-4">
													<img src="{{url('/assets/img/services/s4.png')}}" alt="Services-4">
												</div>
												<div class="card-body text-center">
													<h5 class="card-title text-uppercase font-roboto">seo optimize</h5>
													<p class="card-text text-secondary">
														Some quick example text to build on the card
														title and make up
														the bulk of the card's content.
													</p>
												</div>
											</div>
										</div>
		                            </div>
		                           
									
									<!-- controls -->
									<div class="wizard-navigation">
										<div class="progress-with-circle">
											<div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 15%;"></div>
										</div>
										<ul>
											<li>
												<a href="#location" data-toggle="tab">
													<div class="icon-circle">
														<i class="ti-map"></i>
													</div>
													Location
												</a>
											</li>
											<li>
												<a href="#type" data-toggle="tab">
													<div class="icon-circle">
														<i class="ti-direction-alt"></i>
													</div>
													Type
												</a>
											</li>
											<li>
												<a href="#facilities" data-toggle="tab">
													<div class="icon-circle">
														<i class="ti-panel"></i>
													</div>
													Facilities
												</a>
											</li>
											<li>
												
												<a href="#description" data-toggle="tab">
													<div class="icon-circle">
														<i class="ti-comments"></i>
													</div>
													Comments
												</a>
											</li>
										</ul>
									</div>
									<!-- controls -->


		                        </div>
								
								

		                    </form>
		                </div>
		            </div> <!-- wizard container -->
		        </div>
	        </div> <!-- row -->
	    </div> <!--  big container -->


		
	</div>



</body>

	<!--   Core JS Files   -->
	<script src="{{url('/assets/js/wizered/jquery-2.2.4.min.js')}}" type="text/javascript"></script>
	<script src="{{url('/assets/js/wizered/bootstrap.min.js')}}" type="text/javascript"></script>
	<script src="{{url('/assets/js/wizered/jquery.bootstrap.wizard.js')}}" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="{{url('/assets/js/wizered/demo.js')}}" type="text/javascript"></script>
	<script src="{{url('/assets/js/wizered/paper-bootstrap-wizard.js')}}" type="text/javascript"></script>

	<!--  More information about jquery.validate here: https://jqueryvalidation.org/	 -->
	<script src="{{url('/assets/js/wizered/jquery.validate.min.js')}}" type="text/javascript"></script>

</html>
