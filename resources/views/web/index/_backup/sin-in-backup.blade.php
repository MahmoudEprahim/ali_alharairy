@extends('web.layouts.app')

@section('content')

<style>
        body,
#slider,
.wrap,
.slide-content {
  margin: 0;
  padding: 0;
  font-family: Arial, Helvetica, sans-serif;
  width: 100%;
  height: 100%;
  /* overflow-x: hidden; */
}

.wrap {
  position: relative;
}

.brad-slide {
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
}



.slide-content {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
}

.slide-content span {
  font-size: 1.5rem;
  color: #000;
}

.arrow {
  cursor: pointer;
  position: absolute;
  top: 112%;
  margin-top: -35px;
  width: 0;
  height: 0;
  /* border-style: solid; */
}

#arrow-left {
  /* border-width: 5px 10px 5px 0; */
  /* border-color: transparent rgb(80, 20, 20) transparent transparent; */
  left: 0;
  margin-left: 30px;
}

#arrow-right {
  /* border-width: 5px 0 5px 10px; */
  /* border-color: transparent transparent transparent rgb(136, 23, 23); */
  right: 0;
  margin-right: 30px;
}

.modal-th-width{
    width: 168px;
}
.modal-last-th-width{
    width: 320px;
}


.datepicker {
  width: 100%;
  background: #fff;
  border-radius: 10px;
  /* box-shadow: 0 0 50px 0 rgba(0, 0, 0, 0.2); */
  /* margin: 50px auto; */
  overflow: hidden;
}


</style>

<section class="brand-area about-area" style="padding: 3% 10%;">
    <div class="container">
        <h1 class="section-header-titles text-uppercase orange-main-color font-cairo">Sign in</h1>
        <span class="title_area_span"></span>
        <div class="row justify-content-center">

            <!-- Modal -->
            <div class="modal fade bd-example-modal-lg" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <!-- <h5 class="modal-title text-center section-header-titles font-cairo" id="exampleModalLabel">Choose your group</h5> -->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <table class="table table-bordered">

            <select class="form-control form-control-lg orange-main-color">
                <option>Choose your group</option>
                <option>group 1</option>
                <option>group 2</option>
            </select>

    <thead>
      <tr>
        <th class="text-center modal-th-width orange-main-color" >groups</th>
        <th  class="text-center modal-th-width orange-main-color" >group Availability</th>
        <th  class="text-center modal-th-width orange-main-color">Available places</th>
        <th  class="text-center modal-last-th-width orange-main-color" ></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
            <!-- <input type="text" class="form-control" id="inputZip"> -->
        </td>
        <td>
            <!-- <input type="text" class="form-control" id="inputZip"> -->
        </td>
        <td>
            <!-- <input type="text" class="form-control" id="inputZip"> -->
        </td>
        <td><!-- slider -->
            <div class="wrap ">
                <div id="arrow-left" class="arrow"><i class="fas fa-angle-left"></i></div>
                <div id="slider">
                    
                    <div class="brad-slide slide1">
                        <div class="slide-content">
                        <span> One</span>
                        </div>
                    </div>
                    <div class="brad-slide slide2">
                        <div class="slide-content">
                        <span> Two</span>
                        </div>
                    </div>
                    <div class="brad-slide slide3">
                        <div class="slide-content">
                        <span> Three</span>
                        </div>
                    </div>
                
                </div>
                <div id="arrow-right" class="arrow"><i class="fas fa-angle-right"></i></div>


                
            </div>
            <!--slider  -->
        </td>
      </tr>
    </tbody>
</table>
            <h1 class="section-header-titles orange-main-color font-cairo text-center">Choose your start date</h1>
            <!--  -->
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="row">
                        <div class="col-lg-12">
                        <img src="{{url('assets/img/why-us/support_students/support_students.png')}}" alt="About us" class="prevent-selection prevent-drag img-fluid">
                        </div>
                        <div class="col-lg-12">
                        <img src="{{url('assets/img/why-us/support_students/support_students.png')}}" alt="About us" class="prevent-selection prevent-drag img-fluid">
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                      
                    <div class="datepicker c-datepicker-date-editor c-datepicker-single-editor example">
                        <i class="c-datepicker-range__icon kxiconfont icon-clock"></i>
                        <input type="text" autocomplete="off" name="" placeholder="Select date and time" class="c-datepicker-data-input only-date" value="">
                    </div>
                </div>
            </div>
            
            <!--  -->



<!-- slider -->

<!--slider  -->


                    </div>
                    <div class="modal-footer row">
                        <div class="form-group col-md-12">
                            <button style="font-size: 1.5rem;" type="submit" class="btn col-md-12 btn-active text-uppercase white-color">continue</button>
                        </div>
                        <div class="form-group col-md-12">
                            <button style="font-size: 1.5rem;" type="button" class="btn btn-light col-md-12 text-uppercase" data-dismiss="modal">cancel</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal -->

            <!--say about login form -->
            <div class="row col-10 col-md-10 col-lg-10 col-sm-10 btn-margin justify-content-center">
                <div class="col-sm-5">
                    <button class="btn btn-active enroll-course-btn" data-toggle="modal" data-target="#modal2">Private Groups <span class="watch-video-text" style="display: block;"></span>  </button>
                </div>
                <div class="col-sm-5">
                    <button class="btn closed-btn enroll-course-btn " data-toggle="modal" data-target="#modal2"> Enroll in summer cuorse  <span class="closed-now-text">closed now</span>  </button>
                </div>
            </div>
            <div class="row">
            <div id="say-about-form" class=" col-12 col-md-12 col-lg-12 col-sm-12 margin-bottom" style="margin-top:15%;">
                <div class="say-about-div">
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12 margin-bottom" style="padding: 8px;">
                        <form method="POST" style="display: flex;margin-left: 20px;" class="row">
                           <!-- upload photo -->
                            <span class="btn btn-default btn-file">
                              <img  class="upload-photo upload-photo-icon" src="{{url('assets/img/icons/sign-in/upload-photo.png')}}" alt="">
                                Upload photo <input type="file" title="upload profile photo">
                            </span>
                            <!-- upload photo -->
                            <!-- choose center -->
                            <p class="p-header-titles" style="display:inline">Center :</p>
                            <label for="rdo-1" class="btn-radio">
                                <input type="radio" checked id="rdo-1" name="radio-grp">
                                <svg width="15px" height="15px" viewBox="0 0 20 20">
                                    <circle cx="10" cy="10" r="9"></circle>
                                    <path d="M10,7 C8.34314575,7 7,8.34314575 7,10 C7,11.6568542 8.34314575,13 10,13 C11.6568542,13 13,11.6568542 13,10 C13,8.34314575 11.6568542,7 10,7 Z" class="inner"></path>
                                    <path d="M10,1 L10,1 L10,1 C14.9705627,1 19,5.02943725 19,10 L19,10 L19,10 C19,14.9705627 14.9705627,19 10,19 L10,19 L10,19 C5.02943725,19 1,14.9705627 1,10 L1,10 L1,10 C1,5.02943725 5.02943725,1 10,1 L10,1 Z" class="outer"></path>
                                </svg>
                                <span>Option One</span>
                            </label>
                            <label for="rdo-2" class="btn-radio">
                                <input type="radio"  id="rdo-2" name="radio-grp">
                                <svg width="15px" height="15px" viewBox="0 0 20 20">
                                    <circle cx="10" cy="10" r="9"></circle>
                                    <path d="M10,7 C8.34314575,7 7,8.34314575 7,10 C7,11.6568542 8.34314575,13 10,13 C11.6568542,13 13,11.6568542 13,10 C13,8.34314575 11.6568542,7 10,7 Z" class="inner"></path>
                                    <path d="M10,1 L10,1 L10,1 C14.9705627,1 19,5.02943725 19,10 L19,10 L19,10 C19,14.9705627 14.9705627,19 10,19 L10,19 L10,19 C5.02943725,19 1,14.9705627 1,10 L1,10 L1,10 C1,5.02943725 5.02943725,1 10,1 L10,1 Z" class="outer"></path>
                                </svg>
                                <span>Option One</span>
                            </label>
                            <!-- choose center -->
                            <!-- input fields -->
                            <input type="text" name="name" placeholder="Name" class="col-sm-11 form-control input-text">
                            <select id="inputState" class="col-sm-11 form-control select-options">
                                <option selected>Academic Year</option>
                                <option>First</option>
                                <option>Second</option>
                                <option>Third</option>
                            </select>
                            <input type="text" name="school-name" placeholder="School name" class="col-sm-11 form-control input-text">
                            <input type="text" name="address" placeholder="Address" class="col-sm-11 form-control input-text">
                            <input type="text" name="mobile-number" placeholder="Mobile number" class="col-sm-11 form-control input-text">
                            <input type="text" name="parent-phone-number" placeholder="Parent mobile number" class="col-sm-11 form-control input-text">
                            <input type="text" name="email" placeholder="E-mail" class="col-sm-11 form-control input-text">
                            <input type="text" name="choose-group" placeholder="Choose group" class="col-sm-11 form-control input-text" data-toggle="modal" data-target="#modal2">
                            <input type="text" name="start-date" placeholder="Start date" class="col-sm-11 form-control input-text">
                            <!-- input fields -->
                            <div class="col-sm-11 justify-content-center" style="margin-top: 15px;padding: 0;">
                                <div class="btn-submit">
                                    <!-- <button type="submit" class="btn float-right button.primary-button">Submit</button> -->
                                    <button type="submit" class="btn float-right button btn-active sign-in-btn font-cairo">Sign in</button>
                                </div>
                            </div>
                        </form>
                    </div>        
                </div>
            </div>
            </div>
            
            <!--say about login form -->
        </div>
    </div>
</section>
@endsection