@extends('web.layouts.app')

@section('content')



<style>

  .carousel-indicators{
    background: #d1d1d1;
    height: 2px;
    /* bottom: -25px; */
  }
  .carousel-indicators li{
    background:#ffb606;
    border-radius: 50%;
    border-bottom: none;
    border-top: none;
    width: 120px;
    height: 120px;
    margin-top: -4px;
    margin: -4px auto;
  }
  #our-team{
    height: 800px;
  }
  .photos-slider-indicator-text{
    cursor: pointer;
  }

  /** wzered */
  .wizard-container {
      padding-top: 100px;
      z-index: 3;
  }

  .wizard-card {
      min-height: 410px;
      /* box-shadow: 0 20px 16px -15px rgba(0, 0, 0, 0.57); */
  }

  .wizard-card .wizard-header {
      padding: 15px 15px 15px 15px;
      position: relative;
      border-radius: 3px 3px 0 0;
      z-index: 3;
    }

    .wizard-card .tab-content {
      padding: 15px 20px 10px;
  }
  /* .carousel-control-next, .carousel-control-prev{
    color: #f00;
  }
  .carousel-control-next, .carousel-control-prev:hover,
  .carousel-control-next, .carousel-control-prev:active
  .carousel-control-next, .carousel-control-prev:focus{
    color: #f00;
  } */
  .ramady-background{
    background-color: #d1d1d1;
    
  }
  .photos-carousel-container{
    padding:2%;
    border: 1px solid #d1d1d1;
  }

  .carousel-control-left-padding{
    left: -11px;
  }
  .carousel-control-right-padding{
    right: -12px;
  }
</style>    

<div class="container my-4">



<!-- <hr class="my-4"> -->

<!--Carousel Wrapper-->
<div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

  <!--Controls-->
  <!-- <div class="controls-top">
    <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
    <a class="btn-floating" href="#multi-item-example" data-slide="next"><i class="fa fa-chevron-right"></i></a>
  </div> -->
  <!--/.Controls-->

  <!-- left rigth Controls-->
  <a class="carousel-control-prev carousel-control-left-padding" href="#multi-item-example" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon ramady-background" aria-hidden="true"> <i class="fas fa-chevron-left"></i></span></a>
   
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next carousel-control-right-padding" href="#multi-item-example" role="button" data-slide="next">
    <span class="carousel-control-next-icon ramady-background" aria-hidden="true"><i class="fas fa-chevron-right"></i></span></a>
    
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->

  <!--Indicators-->
  <!-- <ol class="carousel-indicators">
    <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
    <li data-target="#multi-item-example" data-slide-to="1"></li>
    <li data-target="#multi-item-example" data-slide-to="2"></li>
    <li data-target="#multi-item-example" data-slide-to="3"></li>
  </ol> -->
  <ol class="carousel-indicators">
  <div class="col-sm-3">
  
    <li data-target="#multi-item-example" data-slide-to="0" class="active"> products </li>
  </div>
  <div class="col-sm-3"><li data-target="#multi-item-example" data-slide-to="1"></li></div>
  <div class="col-sm-3"><li data-target="#multi-item-example" data-slide-to="2"></li></div>
  <div class="col-sm-3"><li data-target="#multi-item-example" data-slide-to="3"></li></div>
  </ol>

  <!--/.Indicators-->

  <!--Slides-->
  <div class="carousel-inner photos-carousel-container" role="listbox">

    <!--First slide-->
    <div class="carousel-item active">

      <div class="row">
        <div class="col-md-4">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).jpg"
              alt="Card image cap">
            
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).jpg"
              alt="Card image cap">
            
          </div>
        </div>
      </div>

    </div>
    <!--/.First slide-->

    <!--Second slide-->
    <div class="carousel-item">

      <div class="row">
        <div class="col-md-4">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(60).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(47).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(48).jpg"
              alt="Card image cap">
            
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(60).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(47).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(48).jpg"
              alt="Card image cap">
            
          </div>
        </div>
      </div>

    </div>
    <!--/.Second slide-->

    <!--Third slide-->
    <div class="carousel-item">

      <div class="row">
        <div class="col-md-4">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(53).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(45).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(51).jpg"
              alt="Card image cap">
            
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(53).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(45).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(51).jpg"
              alt="Card image cap">
            
          </div>
        </div>
      </div>

    </div>
    <!--/.Third slide-->
    <!--fourth slide-->
    <div class="carousel-item">

      <div class="row">
        <div class="col-md-4">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(53).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(45).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(51).jpg"
              alt="Card image cap">
            
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(53).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(45).jpg"
              alt="Card image cap">
            
          </div>
        </div>

        <div class="col-md-4 clearfix d-none d-md-block">
          <div class="card mb-2">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(51).jpg"
              alt="Card image cap">
            
          </div>
        </div>
      </div>

    </div>
    <!--/.fourth slide-->

  </div>
  <!--/.Slides-->

</div>
<!--/.Carousel Wrapper-->


</div>


@endsection