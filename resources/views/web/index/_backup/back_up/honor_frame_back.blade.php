 
 <!-- start honor frame -->
 
 <div class="container my-4 cta-100">
                <hr class="my-4">
                <h1 class="title-text text-uppercase orange-main-color font-cairo">Honor Frame</h1>
                <!--Carousel Wrapper-->
                <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
            
                <!--Controls-->
                <div class="controls-top" style="float:right;">
                    <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                    <a class="btn-floating" href="#multi-item-example" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                </div>
                <!--/.Controls-->
            
                <!--Indicators-->
                <!-- <ol class="carousel-indicators">
                    <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
                    <li data-target="#multi-item-example" data-slide-to="1"></li>
                    <li data-target="#multi-item-example" data-slide-to="2"></li>
                    <li data-target="#multi-item-example" data-slide-to="3"></li>
                </ol> -->
                <!--/.Indicators-->
            
                <!--Slides-->
                <div class="carousel-inner" role="listbox">
            
                    <!--First slide-->
                    @foreach($honorboards as $key=>$student)
                        <div class="carousel-item {{$key == 0 ? 'active' : ' '}}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card mb-2">
                                    <img class="card-img-top" src="{{asset('storage/'. $student->student->image)}}" alt="Card image cap">
                                    <div class="item-box-blog-body text-center">
                                            <!--Heading-->
                                            <div class="item-box-number">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">1</h5>
                                            </a>
                                            </div>
                                            <div class="item-box-blog-heading">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">{{$student->student->name_en}}</h5>
                                            </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                            <p><i class="fa fa-user-o"></i>{{$student->student->grade->name_en}}</p>
                                            </div>
                                            <!--Text-->
                                            <div class="item-box-blog-text">
                                            <p class="honor-desc">{{$student->description}}</p>
                                            </div>
                                            <div @click="liked =! liked" :class="className"  class="mt"> <i class="fa fa-heart"></i></div>
                                            <div class="mt-right">
                                                <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                <i class="fa fa-star star-hover b2" title="Good"></i>
                                                <i class="fa fa-star star-hover b3" title="ok"></i>
                                                <i class="fa fa-star star-hover b4" title="poor"></i>
                                                <i class="fa fa-star star-hover b5" title="usless"></i>
                                            </div>
                                            <!--Read More Button-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    
                    <!--/.First slide-->
                    <!--Second slide-->
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card mb-2">
                                        
                                    <img  class="card-img-top" src="{{url('assets/img/student-img.png')}}"
                                    alt="Card image cap">
                                    
                                    <div class="item-box-blog-body text-center">
                                            <!--Heading-->
                                            <div class="item-box-number">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">1</h5>
                                            </a>
                                            </div>
                                            <div class="item-box-blog-heading">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">Student Name</h5>
                                            </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                            <p><i class="fa fa-user-o"></i> First Year</p>
                                            </div>
                                            <!--Text-->
                                            <div class="item-box-blog-text">
                                            <p class="honor-desc">Kind of Excellence</p>
                                            </div>
                                            <div class="mt"> <i class="fa fa-heart"></i></div>
                                            <div class="mt-right">
                                                <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                <i class="fa fa-star star-hover b2" title="Good"></i>
                                                <i class="fa fa-star star-hover b3" title="ok"></i>
                                                <i class="fa fa-star star-hover b4" title="poor"></i>
                                                <i class="fa fa-star star-hover b5" title="usless"></i>
                                            </div>
                                            <!--Read More Button-->
                                        </div>
                                </div>
                            </div>
                
                            <div class="col-md-4 clearfix d-none d-md-block">
                                <div class="card mb-2">
                                
                                        <img  class="card-img-top" src="{{url('assets/img/student-img.png')}}"
                                            alt="Card image cap">
                                            
                                            <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                <div class="item-box-number">
                                                    <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">1</h5>
                                                    </a>
                                                </div>
                                                <div class="item-box-blog-heading">
                                                    <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">Student Name</h5>
                                                    </a>
                                                </div>
                                                <!--Data-->
                                                <div class="item-box-blog-data" style="padding: px 15px;">
                                                    <p><i class="fa fa-user-o"></i> First Year</p>
                                                </div>
                                                <!--Text-->
                                                <div class="item-box-blog-text">
                                                    <p class="honor-desc">Kind of Excellence</p>
                                                </div>
                                                <div class="mt"> <i class="fa fa-heart"></i></div>
                                                <div class="mt-right">
                                                    <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                    <i class="fa fa-star star-hover b2" title="Good"></i>
                                                    <i class="fa fa-star star-hover b3" title="ok"></i>
                                                    <i class="fa fa-star star-hover b4" title="poor"></i>
                                                    <i class="fa fa-star star-hover b5" title="usless"></i>
                                                </div>
                                                <!--Read More Button-->
                                                </div>
                                </div>
                            </div>
                
                            <div class="col-md-4 clearfix d-none d-md-block">
                                <div class="card mb-2">
                                
                                        <img class="card-img-top" src="{{url('assets/img/student-img.png')}}"
                                            alt="Card image cap">
                                            
                                            <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                <div class="item-box-number">
                                                    <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">1</h5>
                                                    </a>
                                                </div>
                                                <div class="item-box-blog-heading">
                                                    <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">Student Name</h5>
                                                    </a>
                                                </div>
                                                <!--Data-->
                                                <div class="item-box-blog-data" style="padding: px 15px;">
                                                    <p><i class="fa fa-user-o"></i> First Year</p>
                                                </div>
                                                <!--Text-->
                                                <div class="item-box-blog-text">
                                                    <p class="honor-desc">Kind of Excellence</p>
                                                </div>
                                                <div class="mt"> <i class="fa fa-heart"></i></div>
                                                <div class="mt-right">
                                                    <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                    <i class="fa fa-star star-hover b2" title="Good"></i>
                                                    <i class="fa fa-star star-hover b3" title="ok"></i>
                                                    <i class="fa fa-star star-hover b4" title="poor"></i>
                                                    <i class="fa fa-star star-hover b5" title="usless"></i>
                                                </div>
                                                <!--Read More Button-->
                                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.Second slide-->
                    <!--Third slide-->
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-4">
                            <div class="card mb-2">
                                    
                                <img class="card-img-top" src="{{url('assets/img/student-img.png')}}"
                                alt="Card image cap">
                                
                                <div class="item-box-blog-body text-center">
                                        <!--Heading-->
                                        <div class="item-box-number">
                                        <a href="#" tabindex="0">
                                            <h5 style="color: #000;">1</h5>
                                        </a>
                                        </div>
                                        <div class="item-box-blog-heading">
                                        <a href="#" tabindex="0">
                                            <h5 style="color: #000;">Student Name</h5>
                                        </a>
                                        </div>
                                        <!--Data-->
                                        <div class="item-box-blog-data" style="padding: px 15px;">
                                        <p><i class="fa fa-user-o"></i> First Year</p>
                                        </div>
                                        <!--Text-->
                                        <div class="item-box-blog-text">
                                        <p class="honor-desc">Kind of Excellence</p>
                                        </div>
                                        <div class="mt"> <i class="fa fa-heart"></i></div>
                                        <div class="mt-right">
                                            <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                            <i class="fa fa-star star-hover b2" title="Good"></i>
                                            <i class="fa fa-star star-hover b3" title="ok"></i>
                                            <i class="fa fa-star star-hover b4" title="poor"></i>
                                            <i class="fa fa-star star-hover b5" title="usless"></i>
                                        </div>
                                        <!--Read More Button-->
                                    </div>
                            </div>
                            </div>
                
                            <div class="col-md-4 clearfix d-none d-md-block">
                                <div class="card mb-2">
                                
                                        <img class="card-img-top" src="{{url('assets/img/student-img.png')}}"
                                            alt="Card image cap">
                                            
                                        <div class="item-box-blog-body text-center">
                                            <!--Heading-->
                                            <div class="item-box-number">
                                                <a href="#" tabindex="0">
                                                <h5 style="color: #000;">1</h5>
                                                </a>
                                            </div>
                                            <div class="item-box-blog-heading">
                                                <a href="#" tabindex="0">
                                                <h5 style="color: #000;">Student Name</h5>
                                                </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                                <p><i class="fa fa-user-o"></i> First Year</p>
                                            </div>
                                            <!--Text-->
                                        <div class="item-box-blog-text">
                                            <p class="honor-desc">Kind of Excellence</p>
                                        </div>
                                        <div class="mt"> <i class="fa fa-heart"></i></div>
                                        <div class="mt-right">
                                            <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                            <i class="fa fa-star star-hover b2" title="Good"></i>
                                            <i class="fa fa-star star-hover b3" title="ok"></i>
                                            <i class="fa fa-star star-hover b4" title="poor"></i>
                                            <i class="fa fa-star star-hover b5" title="usless"></i>
                                        </div>
                                        <!--Read More Button-->
                                        </div>
                                </div>
                            </div>
                
                            <div class="col-md-4 clearfix d-none d-md-block">
                                <div class="card mb-2">
                                
                                        <img class="card-img-top" src="{{url('assets/img/student-img.png')}}"
                                            alt="Card image cap">
                                            
                                            <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                <div class="item-box-number">
                                                    <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">1</h5>
                                                    </a>
                                                </div>
                                                <div class="item-box-blog-heading">
                                                    <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">Student Name</h5>
                                                    </a>
                                                </div>
                                                <!--Data-->
                                                <div class="item-box-blog-data" style="padding: px 15px;">
                                                    <p><i class="fa fa-user-o"></i> First Year</p>
                                                </div>
                                                <!--Text-->
                                                <div class="item-box-blog-text">
                                                    <p class="honor-desc">Kind of Excellence</p>
                                                </div>
                                                <div class="mt"> <i class="fa fa-heart"></i></div>
                                                <div class="mt-right">
                                                    <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                    <i class="fa fa-star star-hover b2" title="Good"></i>
                                                    <i class="fa fa-star star-hover b3" title="ok"></i>
                                                    <i class="fa fa-star star-hover b4" title="poor"></i>
                                                    <i class="fa fa-star star-hover b5" title="usless"></i>
                                                </div>
                                                <!--Read More Button-->
                                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.Third slide-->
                    

                </div>
                <!--/.Slides-->
            
                </div>
                <!--/.Carousel Wrapper-->
            
            
        </div>




         <div class="container my-4 cta-100">
                <hr class="my-4">
                <h1 class="title-text text-uppercase orange-main-color font-cairo">Honor Frame</h1>
                <!--Carousel Wrapper-->
                <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
            
                <!--Controls-->
                    <div class="controls-top" style="float:left;">
                        <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                        <a class="btn-floating" href="#multi-item-example" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                    </div>
                    <!--/.Controls-->
                
                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">
                        
                    </div>
                <!--/.Slides-->
            
                </div>
                <!--/.Carousel Wrapper-->
            
            
        </div>