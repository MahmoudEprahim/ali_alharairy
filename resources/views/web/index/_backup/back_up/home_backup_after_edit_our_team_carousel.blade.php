

@extends('web.layouts.app')

@section('content')
    

    <!--  ======================= start slider Area ============================== -->
    <section>
        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        
            <div class="carousel-inner">
            @foreach(slider() as $key=>$slider)
                <div class="main-slider-div slider-img carousel-item {{$key == 0 ? 'active' : ' '}}">
                    <img src="{{asset('storage/'. $slider->image)}}" class="d-block w-100 img-slider prevent-selection prevent-drag" alt="...">
                </div>
            @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <div class="customButton">
        <div class="right-top-buttons">
            <img class="right-top-img" src="{{url('/assets/img/icons/online_test/online_test.png')}}"  alt="email-icon">
            <img class="right-top-img" src="{{url('/assets/img/icons/translate/translate.png')}}"  alt="email-icon">
            <img class="right-top-img" src="{{url('/assets/img/icons/ask_question/ask_question.png')}}"  alt="email-icon">
        </div>
    </div>

    <div class="container search-container">
        <br/>

        <div class="row justify-content-center head-text-slider">
            <div class="col-12 col-md-10 col-lg-10">
            <h2 class="color-white slider-head-text" style="color: #fff">It's time to learn</h2>
            <h1 class="orange-main-color english-text" style="font-family: 'Jomolhari', serif;">ENGLISH</h1>
            </div>
        </div>

        <div class="row justify-content-center search-form">
            <div class="col-12 col-md-10 col-lg-10">
                
                <div class="row">
                    <div class="col-3 col-md-3 col-lg-3 col-sm-3 slider-gates-shape">
                        <div class="img">
                            <div class="col-sm-1">
                                <img class="float-left slider-gate-img prevent-selection prevent-drag" src="{{url('/assets/img/icons/high_school/high_school.png')}}"  alt="email-icon"></div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-gate">High School Gate</h4></div>
                                <div class="col-sm-12"> <a class="show-more orange-main-color" href="#">Show more  <span class="hover-padding">&gt;</span></a></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-3 col-md-3 col-lg-3 col-sm-3 slider-gates-shape">
                        <div class="img">
                            <div class="col-sm-1">
                                <img class="float-left slider-gate-img prevent-selection prevent-drag" src="{{url('/assets/img/icons/english_gate/english_gate.png')}}"  alt="email-icon"></div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-gate">English Language Gate</h4></div>
                                <div class="col-sm-12"> <a class="show-more orange-main-color" href="#">Show more <span class="hover-padding">&gt;</span> </a></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-3 col-md-3 col-lg-3 col-sm-3 slider-gates-shape">
                        <div class="img">
                            <div class="col-sm-1">
                                <img class="float-left slider-gate-img prevent-selection prevent-drag" src="{{url('/assets/img/icons/parents/parents.png')}}"  alt="email-icon"></div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-gate">Parents Follow up</h4></div>
                                <div class="col-sm-12"> <a class="show-more orange-main-color" href="#">Show more  <span class="hover-padding">&gt;</span></a></div>
                            </div>             
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>


    <!--  ======================= End slider Area ============================== -->

    <!--  ======================= Start Main Area ================================ -->
    <main class="site-main">
        <div class="padding-columns">
        <!--  ======================= Start Banner Area =======================  -->
        <!-- <section class="site-banner">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-4 col-lg-6 col-md-12 site-title">
                        <h3 class="title-text">Hey</h3>
                        <h1 class="title-text text-uppercase">I am Aly Elhariry</h1>
                        <h4 class="title-text text-uppercase">Senior English Teacher</h4>
                        <div class="site-buttons">
                            <div class="d-flex flex-row flex-wrap">
                                <button type="button" class="btn button primary-button mr-4 text-uppercase">show
                                    more</button>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 banner-image">
                        <img src="./img/banner/banner-image.png" alt="banner-img" class="img-fluid">
                    </div>
                </div>
            </div>
        </section> -->
        <!--  ======================= End Banner Area =======================  -->
        

        <!--  ========================= About Area ==========================  -->
        <section class="brand-area about-area" style="padding: 3% 10%;">
            <div class="container">
                <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">about me</h1>
                <span class="title_area_span"></span>
                <div class="row justify-content-center">
                   
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                <div class="about-image profile-about-img">
                                    <img src="{{url('assets/img/student-img.png')}}" alt="About us" class="img-fluid">
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-12 about-title">
                                
                                <div class="paragraph py-4">
                                    <p class="para font-cairo">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Error rerum iure obcaecati vel
                                        possimus officia maiores perferendis ut! Quos, perspiciatis.
                                        It is a long established fact that a reader will be distracted by the readable content
                                        of a page when looking at its layout. The point of using Lorem Ipsum is that it has a
                                        more-or-less normal distribution of letters, as opposed to using 'Content here, content
                                        here
                                    </p>
                                
                                </div>
                                <button type="button" class="btn button primary-button text-uppercase float-right">Show more</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--  ========================= End About Area ==========================  -->

       

    
        <!--  ======================= start why us Area =======================  -->
        <section class="brand-area">
            <div class="container">
                <div class="row justify-content-center">
                        <div class="col-xl-4 col-lg-12 col-md-12">
                                <h1 class="section-header-titles text-uppercase orange-main-color font-cairo">{{$feature->features_title_en}}</h1>
                                <span class="title_area_span"></span>
                            <img src="{{asset('storage/'.$feature->main_image)}}" alt="About us" class="prevent-selection prevent-drag img-fluid">
                        </div>
                    <div class="col-xl-8 col-lg-12 col-md-12">
                        <div class="first-row row">
                            <div class="row">
                                <div class="about-us-right col-lg-6 col-md-6 col-sm-6">
                                    
                                        <img class="prevent-selection prevent-drag single-brand" src="{{url('assets/img/why-us/best_techniques/best_techniques.png')}}" alt="best techniques img">
                                        <!-- <img src="{{asset('storage/'.$feature->image_1)}}" alt="Brand-1 "> -->
                                   
                                    <p class="features-text text-center">{{$feature->title_1_en}}</p>
                                </div>
                                <div class="about-us-right col-lg-6 col-md-6 col-sm-6">
                                    <!-- <div class="single-brand">
                                        <img src="{{asset('storage/'.$feature->image_2)}}" alt="Brand-2 ">
                                    </div> -->
                                    <img class="prevent-selection prevent-drag single-brand" src="{{url('assets/img/why-us/best_teacher/best_teacher.png')}}" alt="Best Teacher img">
                                    <p class="features-text text-center">{{$feature->title_2_en}}</p>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="about-us-right col-lg-6 col-md-6 col-sm-6">
                                <img class="prevent-selection prevent-drag single-brand" src="{{url('assets/img/why-us/summer_courses/summer_courses.png')}}" alt="Best Courses img">
                                    <p class="features-text text-center">{{$feature->title_3_en}}</p>
                                </div>
                                <div class="about-us-right col-lg-6 col-md-6 col-sm-6">
                                <img class="prevent-selection prevent-drag single-brand" src="{{url('assets/img/why-us/support_students/support_students.png')}}" alt="Support students img">
                                    <p class="features-text text-center">{{$feature->title_4_en}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--  ======================= End why us Area =======================  -->

         <!--  ======================= stat honor board us Area =======================  -->
         <div class="container my-4 cta-100">
                <!-- <hr class="my-4"> -->
                <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">Honor Frame</h1>
                <span class="title_area_span"></span>
                
            <div class="container-fluid ">
                <!-- Grid row -->
                
                <div class="row">

                    <!-- Grid column -->
                    <div class="col-md-12 mb-4">

                    <div class="container text-center my-3">
                        <!--Controls-->
                        <div class="controls-top" style="float:right;">
                            <a class="btn-floating" href="#recipeCarousel2" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                            <a class="btn-floating" href="#recipeCarousel2" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                        </div>
                        <!--/.Controls-->
                        <!-- <h2>Bootstrap 4 Multiple Item Carousel</h2> -->
                        <div class="row mx-auto my-auto">
                        <div id="recipeCarousel2" class="carousel slide w-100 " data-ride="carousel">
                            <div class="carousel-inner w-100 vv-3" role="listbox">
                            <div class="carousel-item active">
                                <div class="col-4">
                                    <div class="card mb-2">
                                                            
                                        <img  class="card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                        
                                        <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                <div class="item-box-number">
                                                <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">1</h5>
                                                </a>
                                                </div>
                                                <div class="item-box-blog-heading">
                                                <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">Student Name1</h5>
                                                </a>
                                                </div>
                                                <!--Data-->
                                                <div class="item-box-blog-data" style="padding: px 15px;">
                                                <p><i class="fa fa-user-o"></i> First Year</p>
                                                </div>
                                                <!--Text-->
                                                <div class="item-box-blog-text">
                                                <p class="honor-desc">Kind of Excellence</p>
                                                </div>
                                                <div class="mt"> <i class="fa fa-heart"></i></div>
                                                <div class="mt-right">
                                                    <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                    <i class="fa fa-star star-hover b2" title="Good"></i>
                                                    <i class="fa fa-star star-hover b3" title="ok"></i>
                                                    <i class="fa fa-star star-hover b4" title="poor"></i>
                                                    <i class="fa fa-star star-hover b5" title="usless"></i>
                                                </div>
                                                <!--Read More Button-->
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-4">
                                    <div class="card mb-2">
                                                            
                                        <img  class="card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                        
                                        <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                <div class="item-box-number">
                                                <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">1</h5>
                                                </a>
                                                </div>
                                                <div class="item-box-blog-heading">
                                                <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">Student Name2</h5>
                                                </a>
                                                </div>
                                                <!--Data-->
                                                <div class="item-box-blog-data" style="padding: px 15px;">
                                                <p><i class="fa fa-user-o"></i> First Year</p>
                                                </div>
                                                <!--Text-->
                                                <div class="item-box-blog-text">
                                                <p class="honor-desc">Kind of Excellence</p>
                                                </div>
                                                <div class="mt"> <i class="fa fa-heart"></i></div>
                                                <div class="mt-right">
                                                    <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                    <i class="fa fa-star star-hover b2" title="Good"></i>
                                                    <i class="fa fa-star star-hover b3" title="ok"></i>
                                                    <i class="fa fa-star star-hover b4" title="poor"></i>
                                                    <i class="fa fa-star star-hover b5" title="usless"></i>
                                                </div>
                                                <!--Read More Button-->
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-4">
                                    <div class="card mb-2">
                                                            
                                        <img  class="card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                        
                                        <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                <div class="item-box-number">
                                                <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">1</h5>
                                                </a>
                                                </div>
                                                <div class="item-box-blog-heading">
                                                <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">Student Name3</h5>
                                                </a>
                                                </div>
                                                <!--Data-->
                                                <div class="item-box-blog-data" style="padding: px 15px;">
                                                <p><i class="fa fa-user-o"></i> First Year</p>
                                                </div>
                                                <!--Text-->
                                                <div class="item-box-blog-text">
                                                <p class="honor-desc">Kind of Excellence</p>
                                                </div>
                                                <div class="mt"> <i class="fa fa-heart"></i></div>
                                                <div class="mt-right">
                                                    <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                    <i class="fa fa-star star-hover b2" title="Good"></i>
                                                    <i class="fa fa-star star-hover b3" title="ok"></i>
                                                    <i class="fa fa-star star-hover b4" title="poor"></i>
                                                    <i class="fa fa-star star-hover b5" title="usless"></i>
                                                </div>
                                                <!--Read More Button-->
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-4">
                                    <div class="card mb-2">
                                                            
                                        <img  class="card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                        
                                        <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                <div class="item-box-number">
                                                <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">1</h5>
                                                </a>
                                                </div>
                                                <div class="item-box-blog-heading">
                                                <a href="#" tabindex="0">
                                                    <h5 style="color: #000;">Student Name4</h5>
                                                </a>
                                                </div>
                                                <!--Data-->
                                                <div class="item-box-blog-data" style="padding: px 15px;">
                                                <p><i class="fa fa-user-o"></i> First Year</p>
                                                </div>
                                                <!--Text-->
                                                <div class="item-box-blog-text">
                                                <p class="honor-desc">Kind of Excellence</p>
                                                </div>
                                                <div class="mt"> <i class="fa fa-heart"></i></div>
                                                <div class="mt-right">
                                                    <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                    <i class="fa fa-star star-hover b2" title="Good"></i>
                                                    <i class="fa fa-star star-hover b3" title="ok"></i>
                                                    <i class="fa fa-star star-hover b4" title="poor"></i>
                                                    <i class="fa fa-star star-hover b5" title="usless"></i>
                                                </div>
                                                <!--Read More Button-->
                                            </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            </div>
                            
                        </div>
                        </div>
                        
                    </div>

                    </div>
                    <!-- Grid column -->

                </div>
            <!-- Grid row -->
            </div>
        </div>
        <!--  ======================= End honor board us Area =======================  -->

        <!--  ======================= start our team Area =======================  -->
        <div class="container my-4 cta-100">
                <!-- <hr class="my-4"> -->
                <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">Our Team</h1>
                <span class="title_area_span"></span>
            <div class="container-fluid ">
                <!-- Grid row -->
                
                
                <div class="row">

                    <!-- Grid column -->
                    <div class="col-md-12 mb-4">

                    <div class="container text-center my-3">
                        <!--Controls-->
                        <div class="controls-top" style="float:right;">
                            <a class="btn-floating" href="#ourteamcarousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                            <a class="btn-floating" href="#ourteamcarousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                        </div>
                        <!--/.Controls-->
                        <!-- <h2>Bootstrap 4 Multiple Item Carousel</h2> -->
                        <div class="row mx-auto my-auto">

                

                        <div id="ourteamcarousel" class="carousel slide w-100 " data-ride="carousel">
                            <div class="our-team-carousel-inner carousel-inner w-100 vv-3" role="listbox">
                            <div class="carousel-item active">
                                <div class="col-6">
                                    <div class="card mb-2 our-team-card">
                                                            
                                        <img  class="card-img-top prevent-selection prevent-drag" src="{{url('assets/img/Team/tamer/tamer.png')}}"
                                        alt="Card image cap">
                                        
                                        <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                
                                            <div class="text-center item-box-blog-heading out-tema-name">
                                            <a href="#" tabindex="0">
                                                <h5 class="our-team-heading-name">Tamer ekhairy</h5>
                                            </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                            <p class="out-team-title"><i class="fa fa-user-o"></i>business coordinator</p>
                                            </div>
                                            <!--Text-->
                                            
                                            <!--Read More Button-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item ">
                                <div class="col-6">
                                    <div class="card mb-2 our-team-card">
                                                            
                                        <img  class="card-img-top prevent-selection prevent-drag" src="{{url('assets/img/Team/amany/amany.png')}}"
                                        alt="Card image cap">
                                        
                                        <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                
                                            <div class="text-center item-box-blog-heading out-tema-name">
                                            <a href="#" tabindex="0">
                                                <h5 class="our-team-heading-name">Amany Badr El-deen</h5>
                                            </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                            <p class="out-team-title"><i class="fa fa-user-o"></i>Assistant Teacher</p>
                                            </div>
                                            <!--Text-->
                                            
                                            <!--Read More Button-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item ">
                                <div class="col-6">
                                    <div class="card mb-2 our-team-card">
                                                            
                                        <img  class="card-img-top prevent-selection prevent-drag" src="{{url('assets/img/Team/ashraf/ashraf.png')}}"
                                        alt="Card image cap">
                                        
                                        <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                
                                            <div class="text-center item-box-blog-heading out-tema-name">
                                            <a href="#" tabindex="0">
                                                <h5 class="our-team-heading-name">Ashraf El-Ansary</h5>
                                            </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                            <p class="out-team-title"><i class="fa fa-user-o"></i>business coordinator</p>
                                            </div>
                                            <!--Text-->
                                            
                                            <!--Read More Button-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-6">
                                    <div class="card mb-2 our-team-card">
                                                            
                                        <img  class="card-img-top prevent-selection prevent-drag" src="{{url('assets/img/Team/ibrahim/ibrahim.png')}}"
                                        alt="Card image cap">
                                        
                                        <div class="item-box-blog-body text-center">
                                                <!--Heading-->
                                                
                                            <div class="text-center item-box-blog-heading out-tema-name">
                                            <a href="#" tabindex="0">
                                                <h5 class="our-team-heading-name">Ibrahim El-Ghandour</h5>
                                            </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                            <p class="out-team-title"><i class="fa fa-user-o"></i>Website developer</p>
                                            </div>
                                            <!--Text-->
                                            
                                            <!--Read More Button-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                           
                            
                            
                            
                            
                            </div>
                            
                        </div>
                        </div>
                        
                    </div>

                    </div>
                    <!-- Grid column -->

                </div>
            <!-- Grid row -->
            </div>
        </div>
        
        <!--  ======================= End our team Area =======================  -->
        
        <!--  ======================= start what say about Area =======================  -->
        
        
        <div class="container my-4 cta-100">
            <!-- <hr class="my-4"> -->
            <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">What people say about us</h1>
            <span class="title_area_span"></span>
            <div id="our-team" class="row">
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-about-div">
                            <div class="col-sm-1" style="padding: 8px;">
                            <img  class="say-about-img float-left card-img-top prevent-selection prevent-drag" src="{{url('assets/img/student-img.png')}}"
                                        alt="Card image cap">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><h4 class="heading-title-say">High School Gate <span class="say-about-span">Academic Year - his job</span></h4></div>
                                <div class="col-sm-12"> <a class="our-team-comment-text" href="#">Coment simply dummy text of the pinting</a></div>
                                <div class="col-sm-12"> <a style="color: #ffb606;" class="our-team-comment-text" href="#">reply </a> <span class="say-about-span">3d</span></div>
                            </div>             
                        </div>
                    </div>
                    <!--form -->
                    <div class=" col-12 col-md-12 col-lg-12 col-sm-12">
                        <div class="say-form say-about-div">
                            <div class="col-6 col-md-6 col-lg-6 col-sm-6" style="padding: 8px;">
                                <form method="POST" style="display: flex;">
                                    <input type="hidden" name="" value="">
                                    <input id="say-about-input" type="text" name="comment" placeholder="Say your opinion" class="form-control" style="border-radius: 0;border: none;border-bottom: 1px solid #d1d1d1;">
                                </form>
                            </div>        
                        </div>
                    </div>
                     <!--form -->

                    <!--say about login form -->
                    
                    <div id="say-about-form" class=" col-12 col-md-12 col-lg-12 col-sm-12 display-none">
                        <div class="say-form say-about-div">
                            <div class="col-12 col-md-12 col-lg-12 col-sm-12" style="padding: 8px;">
                                <form method="POST" style="display: flex;margin-left: 20px;" class="row">
                                    <input type="hidden" name="" value="">
                                    <input type="text" name="comment" placeholder="Name" class="col-sm-10 form-control" style="border-radius: 0;border: none;border-bottom: 1px solid #495057;">
                                    
                                    <select id="inputState" class="col-sm-10 form-control" style="border: none;border-bottom: 1px solid #555;border-radius: 0">
                                        <option selected>Academic Year</option>
                                        <option>First</option>
                                        <option>Second</option>
                                        <option>Third</option>
                                    </select>
                                    <select id="inputState" class="col-sm-10 form-control" style="border: none;border-bottom: 1px solid #555;border-radius: 0">
                                        <option selected>Current work</option>
                                        <option>First</option>
                                        <option>Second</option>
                                        <option>Third</option>
                                    </select>
                                    <input type="text" name="comment" placeholder="Email" class="col-sm-10 form-control" style="border-radius: 0;border: none;border-bottom: 1px solid #495057;">
                                    <input type="text" name="comment" placeholder="Say your opinion" class="col-sm-10 form-control" style="border-radius: 0;border: none;border-bottom: 1px solid #495057;">
                                    <div class="col-sm-6 justify-content-center" style="margin-top: 15px;">
                                        <div class="btn-submit">
                                            <!-- <button type="submit" class="btn float-right button.primary-button">Submit</button> -->
                                            <button type="submit" class="btn float-right button primary-button text-uppercase">Submit</button>
                                        </div>
                                    </div>
                                   
                                </form>
                            </div>        
                        </div>
                    </div>
                    <!--say about login form -->

                </div>
        </div>



        <!--  ======================= end what say about Area =======================  -->


        


        </div> <!--end of paddind-div elements-->
    </main>
    <!--  ======================= End Main Area ================================ -->

@endsection
