<div class="carousel-item active">
                            <div class="col-4">
                                <div class="card mb-2">
                                                        
                                    <img  class="card-img-top" src="{{url('assets/img/student-img.png')}}"
                                    alt="Card image cap">
                                    
                                    <div class="item-box-blog-body text-center">
                                            <!--Heading-->
                                            <div class="item-box-number">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">1</h5>
                                            </a>
                                            </div>
                                            <div class="item-box-blog-heading">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">Student Name1</h5>
                                            </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                            <p><i class="fa fa-user-o"></i> First Year</p>
                                            </div>
                                            <!--Text-->
                                            <div class="item-box-blog-text">
                                            <p class="honor-desc">Kind of Excellence</p>
                                            </div>
                                            <div class="mt"> <i class="fa fa-heart"></i></div>
                                            <div class="mt-right">
                                                <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                <i class="fa fa-star star-hover b2" title="Good"></i>
                                                <i class="fa fa-star star-hover b3" title="ok"></i>
                                                <i class="fa fa-star star-hover b4" title="poor"></i>
                                                <i class="fa fa-star star-hover b5" title="usless"></i>
                                            </div>
                                            <!--Read More Button-->
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-4">
                                <div class="card mb-2">
                                                        
                                    <img  class="card-img-top" src="{{url('assets/img/student-img.png')}}"
                                    alt="Card image cap">
                                    
                                    <div class="item-box-blog-body text-center">
                                            <!--Heading-->
                                            <div class="item-box-number">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">1</h5>
                                            </a>
                                            </div>
                                            <div class="item-box-blog-heading">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">Student Name2</h5>
                                            </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                            <p><i class="fa fa-user-o"></i> First Year</p>
                                            </div>
                                            <!--Text-->
                                            <div class="item-box-blog-text">
                                            <p class="honor-desc">Kind of Excellence</p>
                                            </div>
                                            <div class="mt"> <i class="fa fa-heart"></i></div>
                                            <div class="mt-right">
                                                <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                <i class="fa fa-star star-hover b2" title="Good"></i>
                                                <i class="fa fa-star star-hover b3" title="ok"></i>
                                                <i class="fa fa-star star-hover b4" title="poor"></i>
                                                <i class="fa fa-star star-hover b5" title="usless"></i>
                                            </div>
                                            <!--Read More Button-->
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-4">
                                <div class="card mb-2">
                                                        
                                    <img  class="card-img-top" src="{{url('assets/img/student-img.png')}}"
                                    alt="Card image cap">
                                    
                                    <div class="item-box-blog-body text-center">
                                            <!--Heading-->
                                            <div class="item-box-number">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">1</h5>
                                            </a>
                                            </div>
                                            <div class="item-box-blog-heading">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">Student Name3</h5>
                                            </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                            <p><i class="fa fa-user-o"></i> First Year</p>
                                            </div>
                                            <!--Text-->
                                            <div class="item-box-blog-text">
                                            <p class="honor-desc">Kind of Excellence</p>
                                            </div>
                                            <div class="mt"> <i class="fa fa-heart"></i></div>
                                            <div class="mt-right">
                                                <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                <i class="fa fa-star star-hover b2" title="Good"></i>
                                                <i class="fa fa-star star-hover b3" title="ok"></i>
                                                <i class="fa fa-star star-hover b4" title="poor"></i>
                                                <i class="fa fa-star star-hover b5" title="usless"></i>
                                            </div>
                                            <!--Read More Button-->
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-4">
                                <div class="card mb-2">
                                                        
                                    <img  class="card-img-top" src="{{url('assets/img/student-img.png')}}"
                                    alt="Card image cap">
                                    
                                    <div class="item-box-blog-body text-center">
                                            <!--Heading-->
                                            <div class="item-box-number">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">1</h5>
                                            </a>
                                            </div>
                                            <div class="item-box-blog-heading">
                                            <a href="#" tabindex="0">
                                                <h5 style="color: #000;">Student Name4</h5>
                                            </a>
                                            </div>
                                            <!--Data-->
                                            <div class="item-box-blog-data" style="padding: px 15px;">
                                            <p><i class="fa fa-user-o"></i> First Year</p>
                                            </div>
                                            <!--Text-->
                                            <div class="item-box-blog-text">
                                            <p class="honor-desc">Kind of Excellence</p>
                                            </div>
                                            <div class="mt"> <i class="fa fa-heart"></i></div>
                                            <div class="mt-right">
                                                <i class="fa fa-star star-hover b1" title="Excellent"></i>
                                                <i class="fa fa-star star-hover b2" title="Good"></i>
                                                <i class="fa fa-star star-hover b3" title="ok"></i>
                                                <i class="fa fa-star star-hover b4" title="poor"></i>
                                                <i class="fa fa-star star-hover b5" title="usless"></i>
                                            </div>
                                            <!--Read More Button-->
                                        </div>
                                </div>
                            </div>
                        </div>