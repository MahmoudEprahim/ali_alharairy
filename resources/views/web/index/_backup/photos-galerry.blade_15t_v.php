

@extends('web.layouts.app')

@section('content')

<style>
/* #carousel-with-lb{
  display: flex;
    margin-right: 10%;
    margin-left: 10%;
    justify-content: center;
    border: 1px solid #d1d1d1;
} */
.carousel-indicators{
  background: #d1d1d1;
  height: 2px;
}
.carousel-indicators li{
  background:#ffb606;
  border-radius: 50%;
  border-bottom: none;
  border-top: none;
  width: 10px;
  height: 10px;
  margin-top: -4px;
  margin:0 auto;
}
#our-team{
  height: 800px;
}
.photos-slider-indicator-text{
  cursor: pointer;
}
</style>    






<!--  ======================= start what say about Area =======================  -->
        
<div class="container my-4 cta-100">
  <!-- <hr class="my-4"> -->
  <h1 class=" section-header-titles text-uppercase orange-main-color font-cairo">Photos</h1>
  <span class="title_area_span"></span>
  <div id="our-team" class="row">

  <!-- caousel -->
    <!--Carousel Wrapper-->
      <div id="carousel-with-lb" class="container carousel slide carousel-multi-item" data-ride="carousel">

          <!--Controls-->
          <div class="controls-top">
            <a class="btn-floating btn-secondary" href="#carousel-with-lb" data-slide="prev"><i
                class="fas fa-chevron-left"></i></a>
            <a class="btn-floating btn-secondary" href="#carousel-with-lb" data-slide="next">
            <i class="fas fa-chevron-right"></i></a>
          </div>
          <!--/.Controls-->


          <!-- left rigth Controls-->
            <a class="carousel-control-prev" href="#carousel-with-lb" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <i class="fas fa-chevron-left"></i></a>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-with-lb" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <i class="fas fa-chevron-right"></i></a>
              <span class="sr-only">Next</span>
            </a>
            <!--/.Controls-->


              <!--Indicators-->
              <ol class="carousel-indicators row">
              <div class="col-lg-3 col-md-3 col-sm-3">
              <li data-target="#carousel-with-lb" data-slide-to="0" class="active secondary-color">
              <p>vocabulary</p>

              </li>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3">
              <li data-target="#carousel-with-lb" data-slide-to="1" class="secondary-color"></li>
              <p data-target="#carousel-with-lb" data-slide-to="3" class="text-center secondary-color photos-slider-indicator-text">grammer</p>

              </div>
              <div class="col-lg-3 col-md-3 col-sm-3">
                <li data-target="#carousel-with-lb" data-slide-to="2" class="secondary-color"></li>
                <p data-target="#carousel-with-lb" data-slide-to="3" class="text-center secondary-color photos-slider-indicator-text">quotes</p>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3">
              <li data-target="#carousel-with-lb" data-slide-to="3" class="secondary-color"></li>
              <p data-target="#carousel-with-lb" data-slide-to="3" class="text-center secondary-color photos-slider-indicator-text">situations</p>
              </div>
                
                
                
                
              </ol>
              <!--/.Indicators-->

              <!--Slides and lightbox-->
              
              <div class="carousel-inner mdb-lightbox" role="listbox">
                <div id="mdb-lightbox-ui"></div>
                <!--First slide-->
                
                  <div class=" carousel-item active text-center">

                    <figure class="col-md-4 d-md-inline-block">
                      <a href="{{url('assets/img/student-img.png')}}"
                        data-size="1600x1067">
                        <img src="{{url('assets/img/student-img.png')}}"
                          class="img-fluid">
                      </a>
                    </figure>
                    <figure class="col-md-4 d-md-inline-block">
                      <a href="{{url('assets/img/student-img.png')}}"
                        data-size="1600x1067">
                        <img src="{{url('assets/img/student-img.png')}}"
                          class="img-fluid">
                      </a>
                    </figure>
                    <figure class="col-md-4 d-md-inline-block">
                      <a href="{{url('assets/img/student-img.png')}}"
                        data-size="1600x1067">
                        <img src="{{url('assets/img/student-img.png')}}"
                          class="img-fluid">
                      </a>
                    </figure>

                    <figure class="col-md-4 d-md-inline-block">
                      <a href="{{url('assets/img/student-img.png')}}"
                        data-size="1600x1067">
                        <img src="{{url('assets/img/student-img.png')}}"
                          class="img-fluid">
                      </a>
                    </figure>

                  </div>
                
                <!--/.First slide-->

                

                <!--Second slide-->
    <div class="carousel-item text-center">

<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>

<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>

<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>

<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>
<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>
<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>

</div>
<!--/.Second slide-->

<!--Third slide-->
<div class="carousel-item text-center">

<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>

<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>

<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>

<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>
<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>
<figure class="col-md-4 d-md-inline-block">
  <a href="{{url('assets/img/student-img.png')}}"
    data-size="1600x1067">
    <img src="{{url('assets/img/student-img.png')}}"
      class="img-fluid">
  </a>
</figure>

</div>
<!--/.Third slide-->
                

              </div>
          <!--/.Slides-->
      </div>
  <!--/.Carousel Wrapper-->
  <!-- caousel -->

  </div>
</div>
<!--  ======================= end what say about Area =======================  -->


@endsection
