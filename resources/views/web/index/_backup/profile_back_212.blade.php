@extends('web.layouts.app')

@section('content')

<style>
    .cv-profile-container{
        margin:30px auto;
    }
    #header{
    display: grid;
    /* grid-template-columns: 2fr 5fr; */
    background: black;
    color: white;
    /* padding: 3%; */
    }
    #ui{
    display: grid;
    grid-template-columns: 0.2fr 0.5fr;
    }
    .skills{
    display: grid;
    grid-template-columns: 1fr 1fr;
    }
    #ent{
    margin-top: 0px;
    }
    #space{
    margin-left: 30px;

    }
    #progress-bar{
    background: #CDCDCF;
    height: 10px;
    margin-top: 5px;
    width: 100px;
    }
    #progress{
    background: black;
    height: 10px;
    width: 80px;
    }
    .topM{
    margin-top: 20px;
    margin-bottom: 10px;

    }
    #contacts{
    background: #CDCDCF;
    padding: 10px;
    }
    #skills{
    background: #CDCDCF;
    padding: 10px;
    }
    #exp1{
    margin-right: 20px;
    display: grid;
    grid-template-columns: 1fr 2fr 4fr;
    }
    p{
    text-align: justify;
    }
    #head{
    font-weight: bold;
    }
    svg{
    height: 1em;
    margin-left: 40px;
    }
    #left-mrgm{
    margin-left: 40px;
    font-weight: bold;
    }
    .box{
    height: 20px;
    width: 20px;
    background: black;
    }
    #image{
    height: 150px;
    width: 150px;
    margin-top: 40px;
    margin-bottom: 20px;
    margin-left: 20px;
    }
    #name{
    letter-spacing: 0.1em;
    font-weight: bold;
    margin-top: 20px;
    }
    #web{
    letter-spacing: 0.1em;
    /* margin-top: 30px; */
    }
    #underline{
        width: 100%;
        height: 2px;
        margin-top: 40px;
        background: #fff;
    }
    #exp{
    background: #CDCDCF;
    padding: 10px;
    }
    #wrapper{
    padding: 0px;
    margin-top: 50px;
    }
</style>



 <div class="col-md-offset-2 col-md-8 cv-profile-container" style="background: #E4E5E9" id="wrapper">
  <div id="header">
   <div>
    <img class="img-fluid" style="z-index: 5;position:absolute;border-radius: 50%;border: 3px solid #fff;" src="{{url('assets/img/mr-alii.png')}}" id="image">
   </div>
   <div>
    <h2 id="name" class="text-center">Ali Elhariry</h2>
    <!-- <p id="underline"></p> -->
    <h5 id="web" class="text-center">Lecturer</h5>
   </div>
   <p id="underline"></p> 
  </div><!--  end header -->
  <div class="row">
   <div class="col-md-12">
    <!-- <h4 id="exp">EXPERIENCE</h4> -->
    <div id="experience">
     
     
     <div id="exp1">
      <div>
       
      </div>
      <div>
       <!-- <span id="head">Senior Graphic Designer</span>
       <p>2012-2015</p> -->
      </div>
      <div>
       <h4 id="ent">About me</h4>
       <p> <i class="fa fa-quote-right fa-lg"></i> Experience is the knowledge or mastery of an event or subject gained through involvement in or exposure to it. Terms in philosophy such as "empirical knowledge" or "a posteriori knowledge" are used to refer to knowledge based on experience.</p>
      </div>
     </div> <!-- end section 1 -->
    </div>
   </div>
  </div> <!-- end row 1 -->
  <div class="row">
   <div class="col-md-12">
    <h4 id="exp">EDUCATION</h4>
    <div id="experience">
     <div id="exp1">
      <div>
      
      </div>
      <div>
      <div class="col-4 col-lg-4 col-md-12" style="padding-top: 81px;margin-left: 61px;">
                                    <h1 style="background: #111111;margin: 30px auto;width: 150px;" class="white-color text-center section-header-titles text-uppercase font-cairo">contact</h1>
                                
                                    <div class="row" style="padding: 0 0 10px 10px;">
                                        <div class="col-sm-2">
                                            <img style="margin: 0 -14px;" class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/mail/mail.png')}}" alt="email-icon">
                                        </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text">example896@gmail.com</h4></div>
                                    </div>             
                                    <div class="row" style="padding: 10px;">
                                    <div class="col-sm-2" style="padding:0">
                                        <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/phone/phone.png')}}" alt="email-icon">
                                    </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text">012354689754</h4></div>
                                    </div>             
                                    <div class="row" style="padding: 10px;">
                                    <div class="col-sm-2" style="padding:0">
                                        <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/location/location.png')}}" alt="email-icon">
                                    </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text text-uppercase">Mansoura</h4></div>
                                    </div>             
                                    <div class="row" style="padding: 10px;">
                                    <div class="col-sm-2" style="padding:0;">
                                        <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/web/web.png')}}" alt="email-icon">
                                    </div>
                                        <div class="col-sm-10"><h4 class="heading-title-text">www.mrenglish.com</h4></div>
                                    </div>
                                </div>
      </div>
      <div>

      <div class="row" style="border-left: 3px solid #000;"> <!-- first right row -->
                                        <div class="col-3 col-lg-3 col-md-12" >
                                            <div class="row" style="margin: 69px auto 18px;">
                                                <div class="col-sm-6">
                                                <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/education/education.png')}}" alt="email-icon">
                                            
                                                </div>
                                                <div class="col-sm-6">
                                                <h1 style="background: #111111;width: 150px;"
                                                class="white-color text-center section-header-titles text-uppercase font-cairo">education</h1>
                                                </div>
                                            
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 0px auto;">
                                            <div class="col-sm-2">
                                                <p>20022019</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                            <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                            <p>second left row second left row second left row second left row</p>
                                            </div>
                                        
                                        </div>
                                        <div class="row" style="margin: 10px auto;">
                                            <div class="col-sm-2">
                                                <p>20022019</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                            <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                            <p>second left row second left row second left row second left row</p>
                                            </div>
                                        
                                        </div>
                                        <div class="row" style="margin: 0px auto;">
                                            <div class="col-sm-2">
                                                <p>20022019</p>                                
                                            </div>
                                            <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                            <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                            <p>second left row second left row second left row second left row</p>
                                            </div>
                                        
                                        </div>
                                    </div> <!-- first riht row -->
      
      </div>
     </div> <!-- end section 1 -->
     <div id="exp1">
      <div>
      
      </div>
      <div>
       
      </div>
      <div>
      

      </div>
     </div> <!-- end section 1 -->
    </div>
   </div>
  </div> <!-- end row 1 -->


  <div class="row">
   <div class="col-md-5">
    <h4 id="contacts">skills</h4>
    <div class="topM">
     
      <span id="space"><a href="#">web design</a></span>
    </div>
    <div class="topM">
    
     <span id="space"><a href="https://www.youtube.com/channel/UC8UsfNYmbKiRJvI9ZhhApEw?view_as=subscriber">Youtube Link</a></span>
    </div>
    <div class="topM">
     
       <span id="space"><a href="#">ammara@gmail.com</a></span>
    </div>
   </div>
   <div class="col-md-7">
    <h4 id="skills">SKILLS</h4>
    <div class="skills">
     <div>
     <div class="row"> <!-- first right row -->
                                    

                                    <div class="col-3 col-lg-3 col-md-12" >
                                        <div class="row" style="margin: 69px auto 18px;">
                                            <div class="col-sm-6">
                                            <img class="float-left prevent-selection prevent-drag" src="{{url('assets/img/icons/cv-icons/EXPERIENCE/EXPERIENCE.png')}}" alt="EXPERIENCE-icon">
                                        
                                            </div>
                                            <div class="col-sm-6">
                                            <h1 style="background: #111111;width: 150px;"
                                            class="white-color text-center section-header-titles text-uppercase font-cairo">experience</h1>
                                            </div>
                                        
                                        </div>
                                    </div>

                                    <div class="row" style="margin: 0px auto;">
                                        <div class="col-sm-2">
                                            <p>20022019</p>                                
                                        </div>
                                        <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                        <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                        <p>second left row second left row second left row second left row</p>
                                        </div>
                                    
                                    </div>
                                    <div class="row" style="margin: 10px auto;">
                                        <div class="col-sm-2">
                                            <p>20022019</p>                                
                                        </div>
                                        <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                        <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                        <p>second left row second left row second left row second left row</p>
                                        </div>
                                    
                                    </div>
                                    <div class="row" style="margin: 0px auto;">
                                        <div class="col-sm-2">
                                            <p>20022019</p>                                
                                        </div>
                                        <div class="col-sm-9 circle-before" style="border-left: 2px solid #000;"> 
                                        <h1 class=" section-header-titles text-uppercase font-cairo">lorem ipsum</h1>
                                        <p>second left row second left row second left row second left row</p>
                                        </div>
                                    
                                    </div>
                                </div> <!-- first riht row -->
     </div>
     
    </div>
   </div>




   
  </div>



  
 </div>

@endsection