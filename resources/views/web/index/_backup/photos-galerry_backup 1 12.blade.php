

@extends('web.layouts.app')

@section('content')

<style>
/* #carousel-with-lb{
  display: flex;
    margin-right: 10%;
    margin-left: 10%;
    justify-content: center;
    border: 1px solid #d1d1d1;
} */
.carousel-indicators{
  background: #d1d1d1;
  height: 2px;
}
.carousel-indicators li{
  background:#ffb606;
  border-radius: 50%;
  border-bottom: none;
  border-top: none;
  width: 10px;
  height: 10px;
  margin-top: -4px;
  margin:0 auto;
}
#our-team{
  height: 800px;
}
.photos-slider-indicator-text{
  cursor: pointer;
}

/** wzered */
.wizard-container {
    padding-top: 100px;
    z-index: 3;
}

.wizard-card {
    min-height: 410px;
    /* box-shadow: 0 20px 16px -15px rgba(0, 0, 0, 0.57); */
}

.wizard-card .wizard-header {
    padding: 15px 15px 15px 15px;
    position: relative;
    border-radius: 3px 3px 0 0;
    z-index: 3;
  }

  .wizard-card .tab-content {
    padding: 15px 20px 10px;
}
.carousel-control-next, .carousel-control-prev{
  color: #f00;
}
.carousel-control-next, .carousel-control-prev:hover,
.carousel-control-next, .carousel-control-prev:active
.carousel-control-next, .carousel-control-prev:focus{
  color: #f00;
}
</style>    
         
  <!--   Big container   -->
<div class="container">
	        <div class="row">
            <div class="col-sm-2"></div>
		        <div class="col-sm-8 col-sm-offset-2">

		            <!--      Wizard container        -->
		            <div class="wizard-container">
		                <div class="card wizard-card" data-color="red" id="wizard">



            <!--Carousel Wrapper-->
<div id="carousel-with-lb" class="container carousel slide carousel-multi-item" data-ride="carousel">

          <!--Controls-->
          <!-- <div class="controls-top">
            <a class="btn-floating btn-secondary" href="#carousel-with-lb" data-slide="prev"><i
                class="fas fa-chevron-left"></i></a>
            <a class="btn-floating btn-secondary" href="#carousel-with-lb" data-slide="next">
            <i class="fas fa-chevron-right"></i></a>
          </div> -->
          <!--/.Controls-->


<!-- left rigth Controls-->
  <a class="carousel-control-prev" href="#carousel-with-lb" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"> <i class="fas fa-chevron-left"></i></a></span>
   
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-with-lb" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"><i class="fas fa-chevron-right"></i></a></span>
    
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->


    <!--Indicators-->
    <ol class="carousel-indicators row">
      <div class="col-lg-3 col-md-3 col-sm-3">
      <li data-target="#carousel-with-lb" data-slide-to="0" class="active secondary-color"></li>
      <p data-target="#carousel-with-lb" data-slide-to="3" class="text-center secondary-color photos-slider-indicator-text">vocabulary</p>

      
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3">
      <li data-target="#carousel-with-lb" data-slide-to="1" class="secondary-color"></li>
      <p data-target="#carousel-with-lb" data-slide-to="3" class="text-center secondary-color photos-slider-indicator-text">grammer</p>

      </div>
      <div class="col-lg-3 col-md-3 col-sm-3">
        <li data-target="#carousel-with-lb" data-slide-to="2" class="secondary-color"></li>
        <p data-target="#carousel-with-lb" data-slide-to="3" class="text-center secondary-color photos-slider-indicator-text">quotes</p>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3">
      <li data-target="#carousel-with-lb" data-slide-to="3" class="secondary-color"></li>
      <p data-target="#carousel-with-lb" data-slide-to="3" class="text-center secondary-color photos-slider-indicator-text">situations</p>
      </div>
    </ol>
    <!--/.Indicators-->

    <!--Slides and lightbox-->
    
      <div class="carousel-inner mdb-lightbox" role="listbox">
        <div id="mdb-lightbox-ui"></div>
        <!--First slide-->
      <div class=" carousel-item active text-center">
          <figure class="col-md-4 d-md-inline-block">
            <a href="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(2).jpg"
            data-size="1600x1067">
            <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(2).jpg"
              class="img-fluid">
            </a>
          </figure>
          <figure class="col-md-4 d-md-inline-block">
            <a href="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(4).jpg"
              data-size="1600x1067">
              <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(4).jpg"
                class="img-fluid">
            </a>
          </figure>
          <figure class="col-md-4 d-md-inline-block">
            <a href="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(4).jpg"
              data-size="1600x1067">
              <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(4).jpg"
                class="img-fluid">
            </a>
          </figure>

    </div>
        <!--/.First slide-->
      </div>
<!--/.Slides-->
</div>
<!--/.Carousel Wrapper-->


                    
        </div>
      </div> <!-- wizard container -->
    </div>
  </div> <!-- row -->
</div> <!--  big container -->

@endsection
