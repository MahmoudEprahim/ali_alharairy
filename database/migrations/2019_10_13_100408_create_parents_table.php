<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ar')->nullable();
            $table->string('name_en')->nullable();
            $table->string('password', 200)->nullable();
            $table->string('phone_1')->nullable();
            $table->string('phone_2')->nullable();
            $table->string('mobile_1')->nullable();
            $table->string('mobile_2')->nullable();
            $table->enum('relation',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14]);
            $table->enum('type',[0,1,2])->nullable();

            $table->string('number')->nullable();
            $table->date('date_issuance')->nullable();
            $table->date('expired_date')->nullable();
            $table->string('created_from')->nullable();
            $table->string('job')->nullable();
            $table->string('home_address')->nullable();
            $table->string('note')->nullable();
            // start edit by ibrahim el monier
            $table->enum('education_level',[0,1,2,3,4,5])->nullable();
            $table->string('Specialization')->nullable();
            $table->string('parent_email')->nullable();
            $table->string('monthly_income')->nullable();
            $table->string('average_income')->nullable();
            $table->string('tax_number')->nullable();
//            $table->string('bank_account_number')->nullable();
//            $table->string('bank_name')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('birthplace')->nullable();
            $table->enum('work_status',[0,1])->nullable();
            $table->enum('labor_sector',[0,1])->nullable();
            $table->string('job_number')->nullable();
            $table->string('job_phone')->nullable();
            $table->string('job_address')->nullable();
            $table->string('boys_num')->nullable();
            $table->string('girls_num')->nullable();
            $table->unsignedInteger('state_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->string('street')->nullable();
            $table->string('house_num')->nullable();
            $table->string('compound_num')->nullable();
            $table->string('compound_name')->nullable();
            $table->text('near')->nullable();
            $table->smallInteger('active')->default(0);
            $table->rememberToken();
            $table->timestamps();

        });

//        Schema::table('students', function (Blueprint $table) {
//            $table->foreign('parent_id')->references('id')->on('parents');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parents');
    }
}
