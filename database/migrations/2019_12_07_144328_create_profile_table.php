<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            
            $table->increments('id');
            $table->text('name_ar')->nullable();
            $table->text('name_en')->nullable();
            $table->text('title_ar')->nullable();
            $table->text('title_en')->nullable();
            $table->string('image')->nullable();
            $table->longText('profile_desc_ar')->nullable();
            $table->longText('profile_desc_en')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('google')->nullable();
            $table->string('linkedin')->nullable();
            $table->text('date_first_stage')->nullable();
            $table->text('first_stage_title')->nullable();
            $table->text('first_stage')->nullable();
            $table->text('date_second_stage')->nullable();
            $table->text('second_stage_title')->nullable();
            $table->text('second_stage')->nullable();
            $table->text('date_third_stage')->nullable();
            $table->text('third_stage')->nullable();
            $table->text('third_stage_title')->nullable();
            $table->text('date_fourth_stage')->nullable();
            $table->text('fourth_stage_title')->nullable();
            $table->text('fourth_stage')->nullable();
            $table->text('date_ex_first_stage')->nullable();
            $table->text('ex_first_stage_title')->nullable();
            $table->text('ex_first_stage')->nullable();
            $table->text('date_ex_second_stage')->nullable();
            $table->text('ex_second_stage_title')->nullable();
            $table->text('ex_second_stage')->nullable();
            $table->text('date_ex_third_stage')->nullable();
            $table->text('ex_third_stage_title')->nullable();
            $table->text('ex_third_stage')->nullable();
            $table->text('date_ex_fourth_stage')->nullable();
            $table->text('ex_fourth_stage_title')->nullable();
            $table->text('ex_fourth_stage')->nullable();
            $table->text('skill_1')->nullable();
            $table->text('skill_2')->nullable();
            $table->text('skill_3')->nullable();
            $table->text('skill_4')->nullable();
            $table->text('email')->nullable();
            $table->text('phone')->nullable();
            $table->text('location')->nullable();
            $table->text('site')->nullable();
            $table->text('hobbies_1')->nullable();
            $table->text('hobbies_2')->nullable();
            $table->text('hobbies_3')->nullable();
            $table->text('hobbies_4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
