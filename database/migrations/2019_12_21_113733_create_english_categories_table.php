<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnglishCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('englishcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cat_name_en')->nullable();
            $table->integer('EnglishGate')->nullable();
//            $table->string('name_ar')->nullable();
//            $table->text('content')->nullable();
//            $table->integer('parent_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('englishlists');
    }
}
