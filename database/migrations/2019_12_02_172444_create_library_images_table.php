<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libraryimages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->text('name_ar')->nullable();
            $table->text('name_en')->nullable();
            $table->text('desc_ar')->nullable();
            $table->text('desc_en')->nullable();
            $table->integer('list_id')->unsigned()->nullable()->index();
            $table->foreign('list_id')->references('id')->on('librarylists')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('branch_id')->unsigned()->nullable()->index();
            $table->foreign('branch_id')->references('id')->on('branches')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
