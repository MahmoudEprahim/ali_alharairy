<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlcustmrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name_ar')->nullable();
            $table->string('name_en')->nullable();
            $table->string('email')->nullable();
            $table->string('image')->default('/public/images/honour.png');
            $table->string('addriss')->nullable();
            $table->string('status')->default(0);
            $table->text('teacher_notes')->nullable();
            $table->enum('per_status',[0,1,2,3])->nullable();
            $table->enum('learn_time',[0,1,2,3])->nullable();
            $table->integer('age')->nullable();
            $table->integer('admin_id')->nullable();
            $table->enum('gender',[0,1])->nullable();
            $table->date('birthdate')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('health_status')->nullable();
            $table->string('previous_estimate')->nullable();
            $table->string('past_school')->nullable();
            $table->enum('religion',[0,1,2,3])->nullable();
            $table->enum('employees_sons',[0,1])->nullable();
            $table->text('education_notes')->nullable();
            $table->enum('special_needs',[0,1])->nullable()->default(1);
            $table->enum('medical_condition',[0,1])->nullable()->default(0);
            $table->enum('medical_attention',[0,1])->nullable()->default(1);
            $table->text('medical_notes')->nullable();
            $table->text('medical_desc')->nullable();
            $table->unsignedInteger('state_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->string('street')->nullable();
            $table->string('house_num')->nullable();
            $table->string('compound_num')->nullable();
            $table->string('compound_name')->nullable();
            $table->string('phone_1')->nullable();
            $table->string('phone_2')->nullable();
            $table->enum('id_type',[0,1,2])->nullable();
            $table->string('number')->nullable();
            $table->text('near')->nullable();
            $table->text('image_1')->nullable();
            $table->text('image_2')->nullable();
            $table->text('image_3')->nullable();
            $table->text('image_accommodation')->nullable();
            $table->text('image_passport')->nullable();
            $table->text('image_birth_certificate')->nullable();
            $table->text('family_id_number')->nullable();
            $table->text('certificate_previous_year')->nullable();
            $table->text('Health_card')->nullable();
            $table->text('password')->nullable();
            $table->text('remember_token')->nullable();
            $table->text('school')->nullable();
            $table->text('start_register')->nullable();
            $table->text('parents_phone')->nullable();
            $table->string('phone')->nullable();

            $table->unsignedInteger('branches_id')->nullable();
            $table->unsignedInteger('grades_id')->nullable();
            $table->unsignedInteger('appointments_id')->nullable();

//            $table->foreign('grades_id')->references('id')->on('grades');
//            $table->foreign('branches_id')->references('id')->on('branches');
//            $table->foreign('appointments_id')->references('id')->on('appointments');

            $table->timestamps();
            $table->softDeletes();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('glcustmr');
    }
}
