<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('book_name')->nullable();
            $table->integer('grade_id')->unsigned()->nullable()->index();
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade')->onUpdate('cascade');
            $table->string('book_description')->nullable();
            $table->enum('type', [0,1])->nullable();
            $table->enum('class',[0,1])->nullable();
            $table->string('publish_date')->nullable();
            $table->string('book_author')->nullable();
            $table->string('book');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
