<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondaryContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secondarycontents', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->longText('description')->nullable();
            $table->text('word')->nullable();
            $table->text('meaning')->nullable();
            $table->string('image')->nullable();
            $table->integer('grade_id')->unsigned()->nullable()->index();
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('list_id')->unsigned()->nullable()->index();
            $table->foreign('list_id')->references('id')->on('secondarylists')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('chapter_id')->unsigned()->nullable()->index();
            $table->foreign('chapter_id')->references('id')->on('units')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secondarycontents');
    }
}
