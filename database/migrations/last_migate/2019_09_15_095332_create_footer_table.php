<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFooterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footer', function (Blueprint $table) {
            $table->increments('id');
             // footer
            // contact us
            $table->text('contact_us_title_ar')->nullable();
            $table->text('contact_us_title_en')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('google')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('youtube')->nullable();
            // members team
            $table->text('title_of_team_members_ar')->nullable();
            $table->text('title_of_team_members_en')->nullable();
            $table->string('member_name_ar')->nullable();
            $table->string('member_name_en')->nullable();
            // right
            $table->text('title_of_about_desc_ar');
            $table->text('title_of_about_desc_en');
            $table->longtext('about_desc_ar');
            $table->longtext('about_desc_en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('footer');
    }
}
