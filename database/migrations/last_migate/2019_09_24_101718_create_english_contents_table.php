<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnglishContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('englishcontents', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->longText('content')->nullable();
            $table->text('word')->nullable();
            $table->text('maeaning')->nullable();
            $table->string('image')->nullable();
            $table->integer('list_id')->unsigned()->nullable()->index();
            $table->foreign('list_id')->references('id')->on('englishlists')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('englishcontents');
    }
}
