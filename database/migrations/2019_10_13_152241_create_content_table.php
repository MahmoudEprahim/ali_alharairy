<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->text('description_ar')->nullable();
            $table->text('description_en')->nullable();
            $table->integer('branches_id')->unsigned();
            $table->integer('grade_id')->unsigned();
            $table->integer('units_id')->unsigned();
            $table->integer('type_list')->unsigned()->default(1);
            $table->foreign('branches_id')->references('id')->on('branches');
            $table->foreign('units_id')->references('id')->on('units');
            $table->foreign('grade_id')->references('id')->on('grades');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
