<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->increments('id');
            $table->text('features_title_ar')->nullable();
            $table->text('features_title_en')->nullable();
            $table->string('main_image')->nullable();
            $table->string('image_1')->nullable();
            $table->string('image_2')->nullable();
            $table->string('image_3')->nullable();
            $table->string('image_4')->nullable();
            $table->text('title_1_ar')->nullable();
            $table->text('title_1_en')->nullable();
            $table->text('title_2_ar')->nullable();
            $table->text('title_2_en')->nullable();
            $table->text('title_3_ar')->nullable();
            $table->text('title_3_en')->nullable();
            $table->text('title_4_ar')->nullable();
            $table->text('title_4_en')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
    }
}
