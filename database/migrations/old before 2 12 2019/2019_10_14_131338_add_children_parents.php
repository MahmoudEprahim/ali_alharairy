<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChildrenParents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('parents');
//            $table->foreign('operation_id')->references('id')->on('operations');
//            $table->foreign('limitations_id')->references('id')->on('limitations');
//            $table->foreign('cc_id')->references('id')->on('glccs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
