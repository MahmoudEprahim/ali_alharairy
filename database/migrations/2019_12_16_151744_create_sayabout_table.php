<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSayaboutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sayabout', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->text('graduation_year')->nullable();
            $table->text('current_work')->nullable();
            $table->string('email')->nullable();
            $table->text('comment')->nullable();
            $table->integer('isactive')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sayabout');
    }
}
