<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sitename_ar');
            $table->string('sitename_en')->nullable();
            $table->text('logo_ar')->nullable();
            $table->text('logo_en')->nullable();
            $table->string('email')->nullable();
            $table->string('main_lang')->default('ar');
            $table->Text('title_about_center_ar')->nullable();
            $table->Text('title_about_center_en')->nullable();
            $table->longText('about_center_ar')->nullable();
            $table->longText('about_center_en')->nullable();


            $table->text('features_title_ar')->nullable();
            $table->text('features_title_en')->nullable();
            $table->string('icon_1')->nullable();
            $table->string('icon_2')->nullable();
            $table->string('icon_3')->nullable();


            $table->string('addriss')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone_2')->nullable();
            $table->string('phone_3')->nullable();
            $table->string('phone_4')->nullable();

            $table->text('contact_us_title_ar')->nullable();
            $table->text('contact_us_title_en')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('google')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('youtube')->nullable();
            $table->string('work_from')->nullable();
            $table->string('work_to')->nullable();

            $table->text('keyword')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
