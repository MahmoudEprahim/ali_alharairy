<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoryChildTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_category_child', function (Blueprint $table) {
            $table->increments('id');
            $table->string('child_name_en')->nullable();
            $table->text('bodyContent')->nullable();
            $table->unsignedInteger('cat_id')->nullable();
            $table->unsignedInteger('sub_cat_id')->nullable();
            $table->foreign('cat_id')->references('id')->on('englishcategories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sub_cat_id')->references('id')->on('sub_categories')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_category_child');
    }
}
