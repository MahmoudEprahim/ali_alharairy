<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name_ar')->nullble();
            $table->text('name_en')->nullble();

            $table->unsignedInteger('grade_id')->nullable();
            $table->unsignedInteger('branches_id')->nullable();
            $table->integer('type')->nullable();

            $table->foreign('branches_id')->references('id')->on('branches');
            $table->foreign('grade_id')->references('id')->on('grades');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
