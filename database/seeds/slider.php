<?php

use Illuminate\Database\Seeder;

class slider extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sliders')->insert([
            [
                'quote' => "DON'T WATCH THE CLOCK, DO WHAT IT DOES.",
                'quote_owner' => 'Sam Levenson',
                'image' => 'slider/OmUkuk1ktbXZV9UxneHcl0tL4XfMNCTd9YdtGOaX.jpeg',
            ],
            [
                'quote' => "The way to get started is to quit talking and begin doing. ",
                'quote_owner' => 'Walt Disney',
                'image' => 'slider/Hl9e8PovfZF4ADTOhO7ILyaLSzGg6TI6bQ3kqusS.jpeg',
            ],
        ]);
    }
}
