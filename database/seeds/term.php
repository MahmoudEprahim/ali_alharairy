<?php

use Illuminate\Database\Seeder;

class term extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('terms')->insert([
            [
                'name_ar' => 'الفصل الدراسى الاول',
                'name_en' => 'First term',
            ],
            [
                'name_ar' => 'الفصل الدراسى الثانى',
                'name_en' => 'Second term',
            ]
        ]);
    }
}
