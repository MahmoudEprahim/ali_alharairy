<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(Profile::class);
        $this->call(branche::class);
        $this->call(setting::class);
        $this->call(Features::class);
        $this->call(admin::class);
        $this->call(permissions::class);
        $this->call(roles::class);
        $this->call(model_has_roles::class);
        $this->call(slider::class);
        $this->call(grade::class);
        $this->call(units::class);
        $this->call(appointments::class);
        $this->call(students::class);
        $this->call(Parents::class);

    }
}
