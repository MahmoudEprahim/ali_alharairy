<?php

use Illuminate\Database\Seeder;

class grade extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grades')->insert([
            [
                'name_ar' => 'الصف الاول الثانوي',
                'name_en' => 'Secondary first grade',
            ],[
                'name_ar' => 'الصف الثاني الثانوي',
                'name_en' => 'Secondary second grade ',
            ],[
                'name_ar' => 'الصف الثالث الثانوي',
                'name_en' => 'Secondary third grade',
            ]
        ]);
    }
}
