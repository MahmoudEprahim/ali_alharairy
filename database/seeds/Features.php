<?php

use Illuminate\Database\Seeder;

class Features extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('features')->insert([
            'features_title_ar' => 'لماذا نحن',
            'features_title_en' => 'Why Us',
            'main_image' => 'setting/features/x9DM9yXn3kAfcHyJhtVQr0xkgkXyNoL2wDpmRicQ.jpeg',
            'image_1' => 'setting/features/ArecPP5MPMEPa5RT1WntKe67tNge32guquzAw0LM.png',
            'image_2' => 'setting/features/ArecPP5MPMEPa5RT1WntKe67tNge32guquzAw0LM.png',
            'image_3' => 'setting/features/WfsXjiRD2sCAqRxhUlPdsyxJA3EqYF8tnkjJl3Si.png',
            'image_4' => 'setting/features/NGvH00qJUAB7V7jhg8UIsuQjmX4ulHkbU3RiyO55.png',
            'title_1_ar' => 'أفضل التقنيات',
            'title_1_en' => 'best techniques',
            'title_2_ar' => 'أفضل مدرس',
            'title_2_en' => 'best teacher',
            'title_3_ar' => 'أفضل الكورسات',
            'title_3_en' => 'best courses',
            'title_4_ar' => 'ندعم الطلاب',
            'title_4_en' => 'support students',
        ]);
    }
}
