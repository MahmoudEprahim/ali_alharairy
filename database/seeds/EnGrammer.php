<?php

use Illuminate\Database\Seeder;

class EnGrammer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('englishlgrammer')->insert(
           [
               [
                   'name_en' =>'Past simple ',
                   'name_ar' =>'الماضى البسيط',
                   'grammer_content' => 'i heared this before',
                   'EnglishGate' => 0,
                   'list_id' => 1,
                   'branches_id' =>2,
               ]
           ]);
}
}
