<?php

use Illuminate\Database\Seeder;

class setting extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'sitename_en' => 'friends center',
            'sitename_ar' => 'سنتر الاصدقاء',
            'email' => 'friendsCenter@gmail.com',
            'title_about_center_ar' => 'نبذة عنا',
            'title_about_center_en' => 'about us',
            'about_center_ar' => 'مرحبًا بكم في مركز تعلم اللغة الإنجليزية خطوة بخطوة الذي يستهدف متعلمي اللغة الإنجليزية كلغة ثانية وخاصة طلاب المرحلة الثانوية
            استخدام أحدث التقنيات جنبًا إلى جنب مع الأفكار الجديدة لدعمهم طوال فترة تعليمهم و
            في وقت لاحق في مهنة وردية.',
            'about_center_en' => 'Welcome to a step-by-step English learning centre aimed at ESL learners particularly secondary students 
            using state-of-the-art technology along with fresh ideas to support them throughout their education and 
            later on a rosy career.',
            'main_lang' => 'ar',


            'features_title_ar' => 'لماذا نحن؟',
            'features_title_en' => 'features',

            'addriss' => 'أجا / برج النور',
            'phone' => '123456789',
            'phone_3' => '123456789',
            'phone_4' => '123456789',
            'contact_us_title_ar' => 'تواصل معنا',
            'contact_us_title_en' => 'Contact Us',
            'facebook' => 'https://www.facebook.com/aly.elhariry',
            'twitter' => 'https://twitter.com/_alyElhariry',
            'google' => 'https://googel.com/_alyElhariry',
            'linkedin' => 'https://linkedin.com/_alyElhariry',
            'youtube' => 'https://youtube.com/_alyElhariry',

        ]);
    }
}
