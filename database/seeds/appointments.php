<?php

use Illuminate\Database\Seeder;

class appointments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('appointments')->insert([
            [
                'groups' => '1:00 pm',
                'days' => 'Sunday',
                'capacity' => '50',
                'grade_id' => '1',
                'branch_id' => '1',
            ],
            [
                'groups' => '1:00 pm',
                'days' => 'Sunday',
                'capacity' => '50',
                'grade_id' => '2',
                'branch_id' => '1',
            ],
            [
                'groups' => '1:00 pm',
                'days' => 'Sunday',
                'capacity' => '50',
                'grade_id' => '3',
                'branch_id' => '1',
            ],
            [
                'groups' => '1:00 pm',
                'days' => 'Sunday',
                'capacity' => '50',
                'grade_id' => '1',
                'branch_id' => '2',
            ],
            [
                'groups' => '1:00 pm',
                'days' => 'Sunday',
                'capacity' => '50',
                'grade_id' => '2',
                'branch_id' => '2',
            ],
            [
                'groups' => '1:00 pm',
                'days' => 'Sunday',
                'capacity' => '50',
                'grade_id' => '3',
                'branch_id' => '2',
            ],

        ]);
    }
}
