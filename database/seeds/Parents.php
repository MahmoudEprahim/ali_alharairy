<?php

use Illuminate\Database\Seeder;

class Parents extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parents')->insert([
            [
                'name_en' => 'Sallam',
                'name_ar' => 'سلام',
                'parent_email' => 'sa@sa.com',
                'relation' => 1,
                'password' => bcrypt('123456'),
            ],
            [
                'name_en' => 'Adel',
                'name_ar' => 'عادل',
                'parent_email' => 'ad@ad.com',
                'relation' => 1,
                'password' => bcrypt('123456'),
            ],
            [
                'name_en' => 'Hsham',
                'name_ar' => 'هشام',
                'parent_email' => 'he@he.com',
                'relation' => 1,
                'password' => bcrypt('123456'),
            ],

        ]);
    }
}
