<?php

use Illuminate\Database\Seeder;

class Profile extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile')->insert([
           'name_ar' => 'على الحريرى',
           'name_en' => 'Aly Elhariry',
           'title_ar' => 'محاضر لغة إنجليزية',
           'title_en' => 'Lecturer',
            'image' => 'profile/mrali.png',
            'profile_desc_ar' => 'اسمي علي رضا محمد الفول الملقب باسم علي الحريري.
            تجربتي مع اللغة الإنجليزية يمكن أن تكون مختلفة.
            كنت أدرس وتدرس اللغة الإنجليزية في وقت واحد خلال المرحلة الجامعية (1998 - 2002).
            منذ ذلك الحين ، أدرس اللغة الإنجليزية. كمدرس متمرس ،
             أعرف حقًا كيف يمكن لطلابي فهم اللغة الإنجليزية.
             إذا كانت هناك نصيحة أشعر أنها يمكن أن تحدث فرقًا ، فهذه هي:
              الأمور صعبة دائمًا في البداية ، لذا ابدأ الآن',
            'profile_desc_en' => 'My name is Ali Reda Muhammad Elful nicknamed as Ali Elhariry.
            My experience with the English language can be different.
            I was studying and teaching English simultaneously during the university stage (1998 - 2002).
            Since then, I have been teaching English. As an experienced teacher,
             I really know how my students can understand English.
             If there is a piece of advice I feel can make a difference is this:
              Things are always tough at the beginning, so start right now.',
            'facebook' => '',
            'twitter'=> '',
            'google'=> '',
            'linkedin'=> '',

        ]);

    }
}
