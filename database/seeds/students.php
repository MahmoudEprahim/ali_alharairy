<?php

use Illuminate\Database\Seeder;

class students extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
            [
                'name_ar' =>'محمد سلام',
                'name_en' =>'Mohammed Sallam',
                'grades_id' =>'1',
                'branches_id' =>'1',
                'parent_id' =>'1',
                'appointments_id' => '1'
            ],
            [
                'name_ar' =>'مي عادل',
                'name_en' =>'Mai adel',
                'grades_id' =>'1',
                'branches_id' =>'1',
                'parent_id' =>'2',
                'appointments_id' => '1'
            ],
            [
                'name_ar' =>'نورهان',
                'name_en' =>'Norhan',
                'grades_id' =>'1',
                'branches_id' =>'1',
                'parent_id' =>'3',
                'appointments_id' => '1'
            ],







        ]);
    }
}
