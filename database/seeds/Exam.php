<?php

use Illuminate\Database\Seeder;

class Exam extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('exams')->insert([[
            'name_ar' => 'general_look',
            'name_en' => 'general_look',
            'image' => '25',
            'branche_id' => '25',
            'grade_id' => '25',
        ],[
            'name_ar' => 'general_look',
            'name_en' => 'general_look',
            'image' => '25',
            'branche_id' => '25',
            'grade_id' => '25',
        ]]);


    }
}
