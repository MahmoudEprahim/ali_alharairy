$(document).ready(function()
{
	var check02 = $("span").filter("#langu02").text("2") // من هنا تقوم بـ تغير معدل اتقانك لتلك اللغات
	
	var backcolor = $("span").filter("#backcolor").css("color") // من هنا يتم الكشف عن لون خلفية النجوم لكي يتم طباعته
	
	var checkcolor02 = $("li").filter("#head-color02").css("background-color") // من هنا يتم الكشف عن لون النجوم الظاهر لكي يتم طباعته
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// شروط و قواعد تلوين النجوم
	
	if( check02.text() === "0" )
	{		
		$("div").filter("#lang02").find("i").css({
			
			"color":backcolor
		});
		
		return
	}	
	
	if( check02.text() === "1" )
	{
		$("div").filter("#lang02").find("i:nth-child(1)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(1)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}

	if( check02.text() === "2" )
	{
		$("div").filter("#lang02").find("i:nth-child(1)").css({
			
			"color":checkcolor02
		});		
		
		$("div").filter("#lang02").find("i:nth-child(2)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(2)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check02.text() === "3" )
	{
		$("div").filter("#lang02").find("i:nth-child(1)").css({
			
			"color":checkcolor02
		});		
		
		$("div").filter("#lang02").find("i:nth-child(2)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(3)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(3)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check02.text() === "4" )
	{
		$("div").filter("#lang02").find("i:nth-child(1)").css({
			
			"color":checkcolor02
		});		
		
		$("div").filter("#lang02").find("i:nth-child(2)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(3)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(4)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(4)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check02.text() === "5" )
	{
		$("div").filter("#lang02").find("i:nth-child(1)").css({
			
			"color":checkcolor02
		});		
		
		$("div").filter("#lang02").find("i:nth-child(2)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(3)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(4)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(5)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(5)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check02.text() === "6" )
	{
		$("div").filter("#lang02").find("i:nth-child(1)").css({
			
			"color":checkcolor02
		});		
		
		$("div").filter("#lang02").find("i:nth-child(2)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(3)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(4)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(5)").css({
			
			"color":checkcolor02
		});
		
		$("div").filter("#lang02").find("i:nth-child(6)").css({
			
			"color":checkcolor02
		});
		
		return
	}
	
	// نهاية شروط و قواعد تلوين النجوم
});