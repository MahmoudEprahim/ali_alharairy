$(document).ready(function()
{		
	var check01 = $("span").filter("#langu01").text("4") // من هنا تقوم بـ تغير معدل اتقانك لتلك اللغات
	
	var backcolor = $("span").filter("#backcolor").css("color") // من هنا يتم الكشف عن لون خلفية النجوم لكي يتم طباعته

	var checkcolor01 = $("li").filter("#head-color01").css("background-color") // من هنا يتم الكشف عن لون النجوم الظاهر لكي يتم طباعته
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// شروط و قواعد تلوين النجوم

	if( check01.text() === "0" )
	{		
		$("div").filter("#lang01").find("i").css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check01.text() === "1" )
	{
		$("div").filter("#lang01").find("i:nth-child(1)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(1)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}

	if( check01.text() === "2" )
	{
		$("div").filter("#lang01").find("i:nth-child(1)").css({
			
			"color":checkcolor01
		});		
		
		$("div").filter("#lang01").find("i:nth-child(2)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(2)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check01.text() === "3" )
	{
		$("div").filter("#lang01").find("i:nth-child(1)").css({
			
			"color":checkcolor01
		});		
		
		$("div").filter("#lang01").find("i:nth-child(2)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(3)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(3)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check01.text() === "4" )
	{
		$("div").filter("#lang01").find("i:nth-child(1)").css({
			
			"color":checkcolor01
		});		
		
		$("div").filter("#lang01").find("i:nth-child(2)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(3)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(4)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(4)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check01.text() === "5" )
	{
		$("div").filter("#lang01").find("i:nth-child(1)").css({
			
			"color":checkcolor01
		});		
		
		$("div").filter("#lang01").find("i:nth-child(2)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(3)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(4)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(5)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(5)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check01.text() === "6" )
	{
		$("div").filter("#lang01").find("i:nth-child(1)").css({
			
			"color":checkcolor01
		});		
		
		$("div").filter("#lang01").find("i:nth-child(2)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(3)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(4)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(5)").css({
			
			"color":checkcolor01
		});
		
		$("div").filter("#lang01").find("i:nth-child(6)").css({
			
			"color":checkcolor01
		});
		
		return
	}
	
	// نهاية شروط و قواعد تلوين النجوم
});