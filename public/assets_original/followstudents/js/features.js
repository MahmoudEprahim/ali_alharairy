$(document).ready(function()
{
	// تخصيص مؤشر النزول
	
	$("html").niceScroll({
		
		cursorcolor:"#c1e2eb",
		cursorborder:"5px solid #c1e2eb",
		cursorborderradius:"0",
		scrollspeed:65
	})
	
	$("ul").filter("#ulscrolls").niceScroll({
		
		cursorcolor:"#7cc0c9",
		cursorborder:"2px solid #7cc0c9",
		cursorborderradius:"0",
		scrollspeed:65
	})
	
	$("p").filter("#head-info").niceScroll({
		
		cursorcolor:"#7cc0c9",
		cursorborder:"2px solid #7cc0c9",
		cursorborderradius:"0",
		scrollspeed:65
	})
	
	// نهاية تخصيص مؤشر النزول	
});