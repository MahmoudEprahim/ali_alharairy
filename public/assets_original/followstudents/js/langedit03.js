$(document).ready(function()
{
	var check03 = $("span").filter("#langu03").text("0") // من هنا تقوم بـ تغير معدل اتقانك لتلك اللغات
	
	var backcolor = $("span").filter("#backcolor").css("color") // من هنا يتم الكشف عن لون خلفية النجوم لكي يتم طباعته
	
	var checkcolor03 = $("li").filter("#head-color03").css("background-color") // من هنا يتم الكشف عن لون النجوم الظاهر لكي يتم طباعته
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// شروط و قواعد تلوين النجوم	
	
	if( check03.text() === "0" )
	{		
		$("div").filter("#lang03").find("i").css({
			
			"color":backcolor
		});
		
		return
	}	
	
	if( check03.text() === "1" )
	{
		$("div").filter("#lang03").find("i:nth-child(1)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(1)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}

	if( check03.text() === "2" )
	{
		$("div").filter("#lang03").find("i:nth-child(1)").css({
			
			"color":checkcolor03
		});		
		
		$("div").filter("#lang03").find("i:nth-child(2)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(2)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check03.text() === "3" )
	{
		$("div").filter("#lang03").find("i:nth-child(1)").css({
			
			"color":checkcolor03
		});		
		
		$("div").filter("#lang03").find("i:nth-child(2)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(3)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(3)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check03.text() === "4" )
	{
		$("div").filter("#lang03").find("i:nth-child(1)").css({
			
			"color":checkcolor03
		});		
		
		$("div").filter("#lang03").find("i:nth-child(2)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(3)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(4)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(4)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check03.text() === "5" )
	{
		$("div").filter("#lang03").find("i:nth-child(1)").css({
			
			"color":checkcolor03
		});		
		
		$("div").filter("#lang03").find("i:nth-child(2)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(3)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(4)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(5)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(5)").nextAll().css({
			
			"color":backcolor
		});
		
		return
	}
	
	if( check03.text() === "6" )
	{
		$("div").filter("#lang03").find("i:nth-child(1)").css({
			
			"color":checkcolor03
		});		
		
		$("div").filter("#lang03").find("i:nth-child(2)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(3)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(4)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(5)").css({
			
			"color":checkcolor03
		});
		
		$("div").filter("#lang03").find("i:nth-child(6)").css({
			
			"color":"#c5adc7"
		});
		
		return
	}	
	
	// نهاية شروط و قواعد تلوين النجوم
});