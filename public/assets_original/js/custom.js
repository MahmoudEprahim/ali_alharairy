/**
	*
	* Version: 1.0
	* Template Scripts
	*
	*

	Custom JS

	1. DROPDOWN MENU
	2. SUPERSLIDES SLIDER
	3. NEWS SLIDER
	4. SKILL CIRCLE
	5. WOW SMOOTH ANIMATIN
	6. COURSE SLIDER
	7. TUTORS SLIDER
	8. BOOTSTRAP TOOLTIP
	9. PRELOADER
	10. EVENTS SLIDER
	11. GALLERY SLIDER
	12. SCROLL TOP BUTTON
	13. SCROLL UP BUTTON

**/

jQuery(function($){


	$('.language').click(function (e) {
		e.preventDefault();
		// var myStyleFile = $('.style_rtl');
		// var baseLink = myStyleFile.data('url');

		var bootStyleFile = $('.changelangen');
		var bootLink = bootStyleFile.data('url');

		if($(this).hasClass('en')){
			$(this).children('span').html('En');
			// myStyleFile.attr('href', baseLink+'/lang_en.css');
			bootStyleFile.attr('href', bootLink+'/bootstrap_ar.min.css');
		} else {
			$(this).children('span').html('Ar');
			// myStyleFile.attr('href', baseLink+'/lang_ar.css');
			bootStyleFile.attr('href', bootLink+'/bootstrap_en.min.css')
		}

		$(this).toggleClass('en')
	})



	$('.chat-link').click(function (e) {
		e.preventDefault();
		console.log($(this).attr('data value is '+'data-value'));
		// var myStyleFile = $('.changelangen');
		// var baseLink = $("link[href*='main.css']").attr("href",$(this).attr("data-value"));

		// if($(this).hasClass('en')){
		// 	myStyleFile.attr('href', baseLink+'/bootstrapen.min.css');
		// 	console.log('en');
		// } else {
		// 	myStyleFile.attr('href', baseLink+'/bootstrap-rtl.min.css');
		// 	console.log('ar');
		// }

	})




	// $('.language').click(function(){

	// 	$("link[href*='main.css']").attr("href",$(this).attr("data-value"));

	// 	// console.log($("link[href*='main.css']"))
	// 	// $('link[=*"/assets/css/main.css"]').remove();
	// 	// console.log($(this).attr("data-value"))
	// });


	$('.like').click(function () {
		$(this).toggleClass('addcolor')
		if($(this).hasClass('addcolor')){
			$(this).css('color','rgb(224, 36, 94)');
		} else {
			$(this).css('color','rgb(101, 119, 134)');
		}
		})


		$('.word-plus').click(function (e) {
			e.preventDefault();
			// var i = 5;
			$('.add-new-words').append(`
			<div class="added row">
			<div class="col-md-6"><label for="الكلمة" class="control-label">الكلمة</label> <input  name="word[]" type="text" class="form-control"></div>
			<div class="col-md-6"><label for="المعنى" class="control-label">المعنى</label> <input name="meaning[]" type="text" class="form-control"></div>
			<a class="col-md-2 remove-siblings btn btn-danger"><i class="fa fa-trash"></i></a>
			</div>
			`)

		})


		$('.grammer-plus').click(function (e) {
			e.preventDefault();
			// var i = 5;
			$('.add-new-grammer').append(`
			<div class="added row">
			




			<div class="col-md-6">
                <label for="{{trans('admin.grammer_name')}}" class="control-label">اسم القاعدة</label> 
                <input  name="name_en[]" type="text" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="{{trans('admin.grammer_name')}}" class="control-label">اسم القاعدة بالعربى</label> 
                <input  name="name_ar[]" type="text" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="{{trans('admin.grammer_content')}}" class="control-label">محتوى القاعدة</label> 
                <textarea name="grammer_content[]" style=" resize: none;" cols="50" rows="10" class="form-control"></textarea>
            </div>
            <div class="col-md-6">
                <label for="{{trans('admin.grammer_content_ar')}}" class="control-label">محتوى القاعدة بالعربى</label> 
                <textarea name="grammer_content_ar[]" style=" resize: none;" cols="50" rows="10" class="form-control"></textarea>
			</div>
			</div>
			`)

		})

		$(document).on('click', '.remove-siblings', function () {

			console.log('jeeee');
			$(this).parents('.added').remove()
		})

		// $('.branch').change(function () {
		// 	$(this).next().removeClass('hidden').attr('disabled', 'disabled')

		// })

		$('.secondary-gate-titles a').each(function() {
			$(this).click(function (e) {
				e.preventDefault();

				var id = $(this).data('id');

				$('#'+id).removeClass('hidden').siblings().addClass('hidden')

			})
		})

		$('.listening .listening-plus').click(function(e){
			e.preventDefault();
			$('.listening-plus').next('ul').slideToggle().removeClass('hidden')
		});

		// $('.secondary-grade-list .secondary-grade-link').on('click',function(e){
		// 	e.preventDefault();
		// 	$(this).next('table').toggleClass();
		// });



		$('.secondary-grade-list .secondary-grade-link').each(function() {
			$(this).click(function (e) {
				e.preventDefault();

				var id = $(this).data('id');

				$('#'+id).removeClass('hidden').siblings().addClass('hidden')

			})
		})


		$('.secondary-grade-list-grammer .secondary-grade-link-grammer').each(function() {
			$(this).click(function (e) {
				e.preventDefault();

				var id = $(this).data('id');

				$('#'+id).removeClass('hidden').siblings().addClass('hidden')

			})
		})











	/* ----------------------------------------------------------- */
  /*  1. DROPDOWN MENU
  /* ----------------------------------------------------------- */

   // for hover dropdown menu
  $('ul.nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
    });

	/* ----------------------------------------------------------- */
	/*  2. SUPERSLIDES SLIDER
	/* ----------------------------------------------------------- */
	$('#slides').superslides({
      animation: 'fade',
      animation_easing: 'linear',
      pagination: 'true'
    });

	/* ----------------------------------------------------------- */
	/*  3. NEWS SLIDER
	/* ----------------------------------------------------------- */
	$('.single_notice_pane').slick({
      slide: 'ul'

    });
    $('[href="#notice"]').on('shown.bs.tab', function (e) {
    $('.single_notice_pane').resize();
	});
	 $('[href="#news"]').on('shown.bs.tab', function (e) {
    $('.single_notice_pane').resize();
	});



	/* ----------------------------------------------------------- */
	/*  4. SKILL CIRCLE
	/* ----------------------------------------------------------- */

	$('#myStathalf').circliful();
	$('#myStat').circliful();
	$('#myStathalf2').circliful();
	$('#myStat2').circliful();
	$('#myStat3').circliful();
	$('#myStat4').circliful();
	$('#myStathalf3').circliful();

	/* ----------------------------------------------------------- */
	/*  5. WOW SMOOTH ANIMATIN
	/* ----------------------------------------------------------- */

	wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100
      }
    );
    wow.init();


	/* ----------------------------------------------------------- */
	/*  6. COURSE SLIDER
	/* ----------------------------------------------------------- */

    $('.course_nav').slick({
	  dots: false,
	  infinite: false,
		speed: 300,
		autoplay: true,
		infinite: true,
	  slidesToShow: 3,
	  arrows:true,
	  slidesToScroll: 1,
	  slide: 'li',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

	/* ----------------------------------------------------------- */
	/*  7. TUTORS SLIDER
	/* ----------------------------------------------------------- */

	$('.tutors_nav').slick({
		dots: true,	  
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		arrows:false,                         
		slidesToScroll: 1,
		slide: 'li',
		responsive: [
		  {
			breakpoint: 1024,
			settings: {
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  infinite: true,
			  arrows:false
			}
		  },
		  {
			breakpoint: 600,
			settings: {
			  slidesToShow: 1,
			  slidesToScroll: 1
			}
		  },
		  {
			breakpoint: 480,
			settings: {
			  slidesToShow: 1,
			  slidesToScroll: 1
			}
		  }
		  // You can unslick at a given breakpoint now by adding:
		  // settings: "unslick"
		  // instead of a settings object
		]
	  });
		  
	/* ----------------------------------------------------------- */
	/*  8. BOOTSTRAP TOOLTIP
	/* ----------------------------------------------------------- */
		$('.soc_tooltip').tooltip('hide')



	/* ----------------------------------------------------------- */
	/*  9. PRELOADER
	/* ----------------------------------------------------------- */
	  window.addEventListener('DOMContentLoaded', function() {
        new QueryLoader2(document.querySelector("body"), {
            barColor: "#efefef",
            backgroundColor: "#111",
            percentage: true,
            barHeight: 1,
            minimumTime: 200,
            fadeOutTime: 1000
        });
    });


    /* ----------------------------------------------------------- */
	/*  10. EVENTS SLIDER
	/* ----------------------------------------------------------- */

	$('.events_slider').slick({
	  dots: true,
	  infinite: true,
	  speed: 500,
	  fade: true,
	  cssEase: 'linear'
	});

    /* ----------------------------------------------------------- */
	/*  11. GALLERY SLIDER
	/* ----------------------------------------------------------- */
	 $('#gallerySLide a').tosrus({
          buttons: 'inline',
          pagination  : {
            add     : true,
            type    : 'thumbnails'
          },
          caption   : {
            add     : true
          }
        });

	/* ----------------------------------------------------------- */
	/*  12. SCROLL UP BUTTON
	/* ----------------------------------------------------------- */

	//Check to see if the window is top if not then display button

	  $(window).scroll(function(){
	    if ($(this).scrollTop() > 300) {
	      $('.scrollToTop').fadeIn();
	    } else {
	      $('.scrollToTop').fadeOut();
	    }
	  });

	  //Click event to scroll to top

	  $('.scrollToTop').click(function(){
	    $('html, body').animate({scrollTop : 0},800);
	    return false;
	  });

		$( ".ticker-move" ).hover(function() {
			$( ".ticker-move" ).stop();
		});

});



// students slider
