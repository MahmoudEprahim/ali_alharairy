<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('lang/{lang}',function ($lang){
    session()->has('lang')?session()->forget('lang'):'';
    $lang == 'ar' ? session()->put('lang','ar') : session()->put('lang','en');
    return back();
});

Auth::routes();



Route::namespace('web')->group(function(){
    Route::get('/', 'HomeController@index');

//    Route::get('/profile', 'ProfileController@index');

//	Route::get('/Secondary', 'SecondaryController@index');
//	Route::get('/honoboard/{id}/show', 'HonorBoardController@show')->name('honoboard.profile');
//
//    // chat
//    Route::get('/home', 'HomeController@chat')->name('home');
//
//    Route::get('/posts', 'chat\PostsController@index')->name('posts');
//    Route::get('/post/show/{id}', 'chat\PostsController@show')->name('post.show');
//
//    Route::get('/post/create', 'chat\PostsController@create')->name('post.create');
//    Route::post('/post/store', 'chat\PostsController@store')->name('post.store');
//
//    Route::post('/comment/store', 'NestedCommentController@store')->name('comment.add');
//
//
//    Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');
//
//
//    Route::get('forum', 'ForumController@index');



});






Route::get('hamlogin', function () {
    return view('web/index/login');
});
Route::get('contactus', function () {
    return view('web/contact-us');
});
// Route::get('followstudents', function () {
//     return view('web/followstudeents');
// });
Route::get('hamlogin', function () {
    return view('web/index/login');
});
Route::get('studentsfollow', function () {
    return view('web/studentsfollow/studentsfollow');
});

Route::get('second_grade', function () {
    return view('web/WPF/Secondary-second-grade');
});


Route::get('third_grade', function () {
    return view('web/WPF/Secondary-third-grade');
});


Route::get('grammer', function () {
    return view('web/WPF/english-gate-grammer');
});

Route::get('followstudents', function () {
    return view('web/WPF/followstudents');
});

Route::get('gallery', function () {
    return view('web/WPF/gallery');
});

Route::get('videos', function () {
    return view('web/WPF/videos');
});

Route::get('resources', function () {
    return view('web/WPF/resources');
});

Route::get('books', function () {
    return view('web/WPF/books');
});

Route::get('exams', function () {
    return view('web/WPF/exams');
});

Route::get('timetable-age', function () {
    return view('web/WPF/timetable-age');
});

Route::get('timetable-nour', function () {
    return view('web/WPF/timetable-nour');
});



Route::get('news', function () {
    return view('web/WPF/news');
});
Route::get('student-register', function () {
    return view('web/WPF/student-register');
});
Route::get('contact', function () {
    return view('web/WPF/contact');
});



Route::get('facebook', function () {
    return view('facebook');
});
Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');
Route::put('auth/facebook/save/{id}', 'Auth\FacebookController@save');
Route::get('get-state','Auth\FacebookController@getStateList');
Route::get('get-city','Auth\FacebookController@getCityList');


Route::get('job_details/{id}', 'web\PageController@jobDetails');
Route::get('alljobs', 'web\PageController@alljobs');
Route::get('apply_to_job/{id}', 'web\PageController@applyToJob');

Route::get('contact', 'web\PageController@contact');
Route::post('contact', 'web\PageController@postContact')->name('contact');

Route::get('company', 'web\companyController@company');
Route::post('company', 'web\companyController@postcompany')->name('company.store');
Route::put('company/{id}', 'web\companyController@update')->name('company.update');


Route::get('employer', 'web\employerController@employer');
Route::post('employer', 'web\employerController@postemployer')->name('employer.store');
Route::put('employer/{id}', 'web\employerController@update')->name('employer.update');

Route::get('blog', 'web\PageController@blog');
Route::get('blog/{id}', ['as' => 'web.blog.single', 'uses' => 'web\PageController@getSingle']);

Route::get('Papers_required_for_travel', 'web\PaperRequiredController@Papers_required_for_travel');

Route::get('profile', 'web\profileController@index')->name('profile.index');
Route::get('profilecompany', 'web\profileController@indexcompany');
Route::get('show', 'web\profileController@show');
Route::get('edit_cv/{id}', 'web\profileController@editcv');
Route::get('profilecompany/cv/{id}', 'web\profileController@companyeditcv');
Route::post('profile', 'web\profileController@postEditCv')->name('profile');

Route::resource('applicant_request','web\ApplicantRequestController');
Route::get('candidates','web\ApplicantRequestController@candidates');
Route::get('applicants','web\ApplicantRequestController@applicants');
Route::resource('job_feature','web\JobFeatureController');

//Route::get('chosen','Api\ChosenController@index');
//Route::get('chosen_applicants','web\ApplicantRequestController@chosen_applicants');
Route::post('CandidateStatus','web\ApplicantRequestController@CandidateStatus')->name('CandidateStatus');

Route::get('get-job-spec-list','web\ApplicantRequestController@getJobSpecList')->name('webGetJobSpecList');

Route::get('chosen','web\ChosenController@index');
Route::get('chosen/companies','web\ChosenController@companies');
Route::get('chosen/jobs','web\ChosenController@jobs');
Route::get('chosen_applicants','web\ChosenController@chosen_applicants');
Route::put('chosen_applicants/{id}','web\ChosenController@update');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
