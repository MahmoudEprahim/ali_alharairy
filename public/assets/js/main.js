$(document).ready(function () {




  let $btns = $('.project-area .button-group button');


  $btns.click(function (e) {

      $('.project-area .button-group button').removeClass('active');
      e.target.classList.add('active');

      let selector = $(e.target).attr('data-filter');
      $('.project-area .grid').isotope({
          filter: selector
      });

      return false;
  })

  $('.project-area .button-group #btn1').trigger('click');

  // $('.project-area .grid .test-popup-link').magnificPopup({
  //     type: 'image',
  //     gallery: { enabled: true }
  // });


  // Owl-carousel

  $('.site-main .about-area .owl-carousel').owlCarousel({
      loop: true,
      autoplay: true,
      dots: true,
      responsive: {
          0: {
              items: 1
          },
          560: {
              items: 2
          }
      }
  })

  // sticky navigation menu
  let nav_offset_top = $('.header_area').height() + 50;
  function navbarFixed() {
      if ($('.header_area').length) {
          $(window).scroll(function () {
              let scroll = $(window).scrollTop();
              if (scroll >= nav_offset_top) {
                  $('.header_area .main-menu').addClass('navbar_fixed');
              } else {
                  $('.header_area .main-menu').removeClass('navbar_fixed');
              }
          })
      }
  }

  navbarFixed();



  // honor board
  // $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
  //     var next = $(this).next();
  //     if (!next.length) {
  //       next = $(this).siblings(':first');
  //     }
  //     next.children(':first-child').clone().appendTo($(this));
    
  //     for (var i=0;i<4;i++) {
  //       next=next.next();
  //       if (!next.length) {
  //         next=$(this).siblings(':first');
  //       }
  //       next.children(':first-child').clone().appendTo($(this));
  //     }
  //   });

  $('.honor-board-carusel').carousel({
      interval: 5000,
      pause: "hover",
      keyboard: true,
      touch: true,
      number: 3,
      });

  

  });

  /** multi item carousel */
$(document).ready(function () {
  var itemsMainDiv = ('.MultiCarousel');
  var itemsDiv = ('.MultiCarousel-inner');
  var itemWidth = "";

  $('.leftLst, .rightLst').click(function () {
      var condition = $(this).hasClass("leftLst");
      if (condition)
          click(0, this);
      else
          click(1, this)
  });

  ResCarouselSize();




  $(window).resize(function () {
      ResCarouselSize();
  });

  //this function define the size of the items
  function ResCarouselSize() {
      var incno = 0;
      var dataItems = ("data-items");
      var itemClass = ('.item');
      var id = 0;
      var btnParentSb = '';
      var itemsSplit = '';
      var sampwidth = $(itemsMainDiv).width();
      var bodyWidth = $('body').width();
      $(itemsDiv).each(function () {
          id = id + 1;
          var itemNumbers = $(this).find(itemClass).length;
          btnParentSb = $(this).parent().attr(dataItems);
          itemsSplit = btnParentSb.split(',');
          $(this).parent().attr("id", "MultiCarousel" + id);


          if (bodyWidth >= 1200) {
              incno = itemsSplit[3];
              itemWidth = sampwidth / incno;
          }
          else if (bodyWidth >= 992) {
              incno = itemsSplit[2];
              itemWidth = sampwidth / incno;
          }
          else if (bodyWidth >= 768) {
              incno = itemsSplit[1];
              itemWidth = sampwidth / incno;
          }
          else {
              incno = itemsSplit[0];
              itemWidth = sampwidth / incno;
          }
          $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
          $(this).find(itemClass).each(function () {
              $(this).outerWidth(itemWidth);
          });

          $(".leftLst").addClass("over");
          $(".rightLst").removeClass("over");

      });
  }


  // multi items carousel
  // $('.carousel').carousel({
  //     interval: 200000,
  //     keyboard:true,
  //     });
  $('.honor-carousel .carousel-item').each(function () {
      interval: 2000;
      var next = $(this).next();
      if (!next.length) {
        next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
    
      for (var i = 0; i < 2; i++) {
        next = next.next();
        if (!next.length) {
          next = $(this).siblings(':first');
        }
    
        next.children(':first-child').clone().appendTo($(this));
      }
    });

});


$( "#our-team" ).hover(function() {
  $( this ).fadeIn( 1000, function(){$('#our-team').css({"overflow":"auto", "margin":"0"});},);
  
});


$("#say-about-input").focus(function(){
  // console.log('hello');
  $("#say-about-form").removeClass('display-none', function(){
      console.log('hello');
      $("#say-about-input").css("display","none");
  });
});

/** choose-group-table */
$("#choose_group").focus(function(){
  // console.log('hello');
  $("#choose-group-table").removeClass('display-none', function(){
      console.log('hello');
      $("#say-about-input").css("display","none");
  });
});

// $( "#our-team" ).hover(function() {
//   $( this ).fadeOut( 100 );
//   $( this ).fadeIn( 500 );
// });

$('#modal1').on('hidden.bs.modal', function (e) {
  // do something...
  $('#modal1 iframe').attr("src", $("#modal1 iframe").attr("src"));
});

$('#modal6').on('hidden.bs.modal', function (e) {
  // do something...
  $('#modal6 iframe').attr("src", $("#modal6 iframe").attr("src"));
});

$('#modal4').on('hidden.bs.modal', function (e) {
  // do something...
  $('#modal4 iframe').attr("src", $("#modal4 iframe").attr("src"));
});


$('#prevent-default').click(function(event){
  event.preventDefault();
});

// $( "a" ).click(function( event ) {
//     event.preventDefault();
  
//   });




// sign in slider

let sliderImages = document.querySelectorAll(".brad-slide"),
arrowLeft = document.querySelector("#arrow-left"),
arrowRight = document.querySelector("#arrow-right"),
current = 0;

// Clear all images
function reset() {
for (let i = 0; i < sliderImages.length; i++) {
sliderImages[i].style.display = "none";
}
}

// Init slider
function startSlide() {
reset();
sliderImages[0].style.display = "block";
}

// Show prev
function slideLeft() {
reset();
sliderImages[current - 1].style.display = "block";
current--;
}

// Show next
function slideRight() {
reset();
sliderImages[current + 1].style.display = "block";
current++;
}

// Left arrow click
arrowLeft.addEventListener("click", function() {
if (current === 0) {
current = sliderImages.length;
}
slideLeft();
});

// Right arrow click
arrowRight.addEventListener("click", function() {
if (current === sliderImages.length - 1) {
current = -1;
}
slideRight();
});

startSlide();
///////////////////// sign in slider times /////////////

/// datepiker work js
// 

$('.example').datePicker({
"timePicker": true,
  format: 'YY-MM-DD HH:mm:ss'
});
// time picker
$('.example').datePicker({
  format: 'HH:mm:ss'
});

// month picker
$('.example').datePicker({
  format: 'YYYY-MM'
});

// year picker
$('.example').datePicker({
  format: 'YYYY'
});
// time picker
$('.example').datePicker({
  min:'2018-01-01 04:00:00',
  max:'2029-10-29 20:59:59',
});

// month picker
$('.example').datePicker({
  min: '2018-01',
  max: '2029-04',
});

// year picker
$('.example').datePicker({
  min: '2018',
  max: '2029'
});
// $('.example').datePicker({
//   hasShortcut: true,
//   shortcutOptions: [{
//     name: 'Yesterday',
//     day: '-1,-1',
//     time: '00:00:00,23:59:59'
//   },{
//     name: 'Last Week',
//     day: '-7,0',
//     time:'00:00:00,'
//   }, {
//     name: 'Last Month',
//     day: '-30,0',
//     time: '00:00:00,'
//   }, {
//     name: 'Last Three Months',
//     day: '-90, 0',
//     time: '00:00:00,'
//   }],
// });
// $('.example').datePicker({
//   isRange: true,
//   between:'month', 
// });
// $('.example').datePicker({
//   hasShortcut: true,
//   shortcutOptions: [{
//     name: 'Yesterday',
//     day: '-1,-1',
//     time: '00:00:00,23:59:59'
//   },{
//     name: 'Last Week',
//     day: '-7,0',
//     time:'00:00:00,'
//   }, {
//     name: 'Last Month',
//     day: '-30,0',
//     time: '00:00:00,'
//   }, {
//     name: 'Last Three Months',
//     day: '-90, 0',
//     time: '00:00:00,'
//   }],
// });
// $.fn.datePicker.dates['en'] = {
//   days: ["Sun", 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
//   months: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
//   now: "now",
//   clear: 'clear',
//   headerYearLink:'',
//   units: ['-', ''],
//   button: ["confirm", "cancel"],
//   confirm: 'Okay',
//   cancel: 'Cancel',
//   chooseDay: 'Choose Day',
//   chooseTime: 'Choose Time',
//   begin: 'Start Time',
//   end: 'End Time',
//   prevYear: 'prevYear',
//   prevMonth: 'prevMonth',
//   nextYear: 'nextYear',
//   nextMonth: 'nextMonth',
//   zero: '0:00'
// };
$('.example').datePicker({
  hide: function () { },
  show: function () {
    
   }
});

// 
////// ebd date piker

//// timeline
$('.timeline-1').Timeline();



// datepiker with image
$(document).ready(function() {

$(".datepicker").datepicker({
  prevText: '<i class="fa fa-fw fa-angle-left"></i>',
  nextText: '<i class="fa fa-fw fa-angle-right"></i>'
});
});


// progress bar div
const component = document.querySelector(".component");
component.addEventListener("click", () => {
const rand = Math.floor(Math.random() * 100);
component.style.setProperty("--progress", rand);
});




// active line after nav link
// $(document).on('click', 'navbar-nav nav-item', function(){
//   $(this).addClass(active-nav-after-color).siblings().removeClass(active-nav-after-color)
// });



