<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('lang/{lang}',function ($lang){
    session()->has('lang')?session()->forget('lang'):'';
    $lang == 'ar' ? session()->put('lang','ar') : session()->put('lang','en');
    return back();
});




Route::namespace('web')->group(function(){
    //  Route::get('/', function(){
    //      return view('web.index');
    //  });
    Route::get('/', 'HomeController@index')->name('home');
//    Route::get('showAppointments', 'WebController@showAppointments')->name('showAppointments');
    Route::get('showAppointments', 'HomeController@showAppointments')->name('showAppointments');
    Route::post('studentRegister','WebController@register')->name('studentRegister');
    Route::group(['middleware'=>'auth'],function (){
    //pages
    Route::get('/profile', 'HomeController@profile');
    Route::get('/video-gallery', 'HomeController@videogallery');
    Route::get('/onlinetest', 'HomeController@onlinetest');
    Route::get('/testnow', 'HomeController@testnow');
    // Route::get('/signin', 'HomeController@signin');
    Route::get('student_register','WebController@index')->name('student_register');


    Route::get('/sign-up-parents', 'HomeController@sign_up_parents');

    Route::post('parentRegister','WebController@parentRegister')->name('parentRegister');


    Route::get('getTree', 'HomeController@getTree')->name('getTree');
    Route::get('getTreeContent', 'HomeController@getTreeContent')->name('getTreeContent');
     Route::get('sayabout','WebController@sayabout');
     Route::POST('sayabout','WebController@sayabout_store')->name('sayabout.store');
    Route::get('/success_register', 'WebController@success_register');


    Route::get('/choose-your-group', 'HomeController@choose_your_group');
    Route::get('/photos-gallery', 'HomeController@photosgallery');
    Route::get('/references', 'HomeController@references');
    Route::get('/high-school-gate', 'HomeController@high_school_gate');
    Route::get('getGradeInfo', 'HomeController@getGradeInfo')->name('getGradeInfo');
    Route::get('/english-gate', 'HomeController@english_gate');

    Route::get('/sign-in-parents', 'HomeController@sign_in_parents');


    // Route::get('/', function(){
    //     return(view('web.pages.comming-soon.comming-soon'));
    // });

//    Route::get('/profile', 'ProfileController@index');

//	Route::get('/Secondary', 'SecondaryController@index');
//	Route::get('/honoboard/{id}/show', 'HonorBoardController@show')->name('honoboard.profile');
//
//    // chat
//    Route::get('/home', 'HomeController@chat')->name('home');
//
//    Route::get('/posts', 'chat\PostsController@index')->name('posts');
   Route::get('/posts', function(){
       return view('web.chat.index');
   });
//    Route::get('/post/show/{id}', 'chat\PostsController@show')->name('post.show');
//
//    Route::get('/post/create', 'chat\PostsController@create')->name('post.create');
//    Route::post('/post/store', 'chat\PostsController@store')->name('post.store');
//
//    Route::post('/comment/store', 'NestedCommentController@store')->name('comment.add');
//
//
//    Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');
//
//
//    Route::get('forum', 'ForumController@index');

Route::get('uploadfile', 'UploadController@create');
Route::post('upload', 'UploadController@upload');

// secondary
Route::get('/secgrade1/{id}', 'GatesController@show')->name('grade.show');

// Route::get('secgrade1', 'GatesController@index');


});
});




// ==========================

//mahmoud

//Route::resource('/studentLogin','WebstudentController' );
//Route::get('followstudents', 'WebstudentController@getlogin');

Route::get('forgetPassword','Auth\LoginController@forgetPassword');
Route::post('forgetPassword','Auth\LoginController@forgetPasswordPost')->name('reset_password');



Route::get('reset/password/{token}','Auth\LoginController@reset_password');
Route::post('reset/password/{token}','Auth\LoginController@reset_password_post')->name('resetpassword');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
//    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
//    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
//    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:clear');
//    return '<h1>Clear Config cleared</h1>';
});

Route::get('/logout', function (){
    auth()->logout();
   
    return redirect('/');
});



//Route::get('student_register', function () {
//    return view('web/contact-us');
//});
//Route::resource('student_register','web\WebController');
//Route::POST('student_register','web\WebController@register');

Route::get('secgrade', function () {
    return view('web/Secondary-first-grade');
});

Route::get('gallery', function () {
    return view('web/gallery');
});

Route::get('videos', function () {
    return view('web/videos');
});

Route::get('resources', function () {
    return view('web/resources');
});

Route::get('books', function () {
    return view('web/books');
});

Route::get('engate', function () {
    return view('web/englishgate');
});

Route::get('chat', function () {
    return view('web/chat');
});

// =======================













// Route::get('followstudents', function () {
//     return view('web/WPF/followstudents');
// });





Route::get('exams', function () {
    return view('web/WPF/exams');
});

Route::get('timetable-age', function () {
    return view('web/WPF/timetable-age');
});

Route::get('timetable-nour', function () {
    return view('web/WPF/timetable-nour');
});



Route::get('news', function () {
    return view('web/WPF/news');
});

Route::get('contact', function () {
    return view('web/WPF/contact');
});




Auth::routes();


