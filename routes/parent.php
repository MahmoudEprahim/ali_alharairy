<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|---------------------a-----------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('lang/{laweng}',function ($lang){
    session()->has('lang')?session()->forget('lang'):'';
    $lang == 'ar' ? session()->put('lang','ar') : session()->put('lang','en');
    return back();
});
Route::group(['namespace'=>'Auth', 'prefix'=>'parent'], function(){
    Route::get('login', 'LoginController@showLoginForm')->name('parentLogin');
    Route::post('login', 'LoginController@login')->name('doLogin');
    Route::any('parentLogout', 'LoginController@logout')->name('parentLogout');



});


//Auth::routes();

Route::group([
    'middleware' => 'auth:parent',
    'namespace' => 'Parent',
    'prefix'=>'parent'
    ], function(){

    Route::resource('parent', 'ParentController');
    Route::get('getSonInfo', 'ParentController@getSonInfo')->name('getSonInfo');

});
