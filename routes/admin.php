<?php

Route::group(['prefix'=>'admin'],function (){
    Config::set('auth.defines','admin');

//    login
    Route::get('/','Admin\AdminAuth@login')->name('admin.login');
    Route::post('adminlogin','Admin\AdminAuth@dologin');
    Route::get('forgetPassword','Admin\AdminAuth@forgetPassword');
    Route::post('forgetPassword','Admin\AdminAuth@forgetPasswordPost');
    Route::get('reset/password/{token}','Admin\AdminAuth@reset_password');
    Route::post('reset/password/{token}','Admin\AdminAuth@reset_password_post');




    Route::get('lang/{lang}',function ($lang){
        session()->has('lang')?session()->forget('lang'):'';
        $lang == 'ar' ? session()->put('lang','ar') : session()->put('lang','en');
        return back();
    });
//    admin panal
    Route::group(['middleware'=>'auth:admin'],function (){

        Route::get('/dashboard','DashboardController@home')->name('admin.home');
        Route::post('/sendmail','DashboardController@sendmail')->name('admin.sendmail');
        Route::any('logout','Admin\AdminAuth@a_logout');

//        admins
        Route::resource('admins','Admin\AdminController');
        Route::delete('admins/{id}','Admin\AdminController@destroy');


//        permission & role
        Route::resource('permissions','Admin\roles\PermissionController');
        Route::resource('roles','Admin\roles\RoleController');
        Route::resource('admins/permission_role','Admin\roles\permission_roles');
        //branches
        Route::resource('branches','Admin\Branches\BranchesController');
        Route::get('branchActive', 'Admin\Branches\BranchesController@branchActive')->name('branchActive'); 
//        setting
        Route::get('setting','SettingController@index')->name('setting');
        Route::post('setting','SettingController@setting_save')->name('setting.save');

        // features
        Route::get('setting/features','Admin\setting\FeaturesController@index')->name('features');
        Route::post('setting/features','Admin\setting\FeaturesController@features_save')->name('features.save');

        Route::resource('setting/homevideos','Admin\setting\HomeVideosController');
        Route::resource('setting/team','Admin\setting\teamController');


        Route::resource('setting/units', 'Admin\secondary\UnitsController');
        Route::resource('setting/wordlist', 'Admin\secondary\WordlistController');
        Route::get('wordlist_grammar', 'Admin\secondary\WordlistController@wordGrammar');


        Route::resource('slider', 'Admin\setting\SliderController');
        Route::resource('setting/content', 'Admin\secondary\ContentController');
        Route::resource('appointments', 'Admin\student\AppointmentController');
        Route::get('show_appointments', 'Admin\student\AppointmentController@show_appointments')->name('show_appointments');
        Route::get('show_exams', 'Admin\student\ExamsController@show_exams')->name('show_exams');
        Route::resource('exams', 'Admin\student\ExamsController');
        Route::resource('payments', 'Admin\student\PaymentsController');
        Route::get('getStudentPrice', 'Admin\student\PaymentsController@getStudentPrice')->name('getStudentPrice');

        Route::get('content_grammar', 'Admin\secondary\ContentController@content_grammar');

        // enlish gate EnglishListsController  EnglishListsContoller
        // english tree categories
        Route::resource('category', 'Admin\english\CategoryController');
        Route::get('deleteSubCat', 'Admin\english\CategoryController@deleteSubCat')->name('deleteSubCat');
        Route::get('editSubCat', 'Admin\english\CategoryController@editSubCat')->name('editSubCat');
        Route::get('getSubCategories', 'Admin\english\CategoryController@getSubCategories')->name('getSubCategories');
        Route::get('getSubCategoryChild', 'Admin\english\CategoryController@getSubCategoryChild')->name('getSubCategoryChild');
        Route::get('createTree','Admin\english\CategoryController@createTree')->name('createTree');
        Route::post('addContent','Admin\english\CategoryController@addContent')->name('addchildcatContent');
//        Route::get('category-tree-view/{id}','Admin\english\CategoryController@edit')->name('edit.category-tree-view');
//        Route::post('update-category','Admin\english\CategoryController@updateCategory')->name('category.update');

        Route::resource('englishlists', 'Admin\english\EnglishListsContoller');
        Route::resource('englishlistening', 'Admin\english\EnglishListeningController');
        Route::resource('englishwords', 'Admin\english\EnglishWordsController');
        Route::resource('englishgrammer', 'Admin\english\EnglishGrammersController');
        Route::get('english_content', 'Admin\english\EnglishGrammersController@english_content');

        Route::resource('englishdescription', 'Admin\english\EnglishDescriptionCntoller');

        // honor board
        Route::resource('honorboard','Admin\honorboard\HonorBoardController');

        // parents routes
        Route::resource('parents', 'Admin\parents\ParentsController');
        Route::get('parentActive', 'Admin\parents\ParentsController@parentActive')->name('parentActive');

        // library
        Route::resource('librarylists', 'Admin\library\LibraryListsContoller');
        // Route::resource('librarybooks','Admin\library\LibraryBooksController');
        // Route::get('library_content','Admin\library\LibraryBooksController@library_content');
        // add images or videos to library
        // Route::resource('librarymedia','Admin\library\LibraryImagesController');
        // Route::resource('libraryimages','Admin\pages\ImagesController');
        // Route::resource('libraryvideos','Admin\pages\VideosController');

         // pages
         Route::resource('images','Admin\pages\ImagesController');
         Route::resource('videos','Admin\pages\VideosController');
         Route::resource('resources','Admin\pages\ResourcesController');

         // homepage
         Route::resource('sayabout','Admin\pages\SayaboutController');
        //  Route::get('sayabout','Admin\pages\SayaboutController@index');
        //  Route::get('sayabout/{id}','Admin\pages\SayaboutController@destroy');
        Route::POST('sayabout','Admin\pages\SayaboutController@store')->name('sayabout.store');
         Route::get('sayabout/{id}/activate','Admin\pages\SayaboutController@isActive');


//        search
        Route::get('search', 'Admin\SearchController@index');



        Route::resource('posts','Admin\chat\PostsController');
        Route::resource('comments','Admin\chat\CommentsController');
          Route::Post('reply/{id}','Admin\chat\CommentsController@reply')->name('comments.reply');

        Route::resource('profile','Admin\profile\ProfileController');








        // customer
        Route::resource('students','Admin\student\studentController');
        Route::get('studentActive', 'Admin\student\studentController@studentActive')->name('studentActive');
        Route::resource('attendance','Admin\student\AttendancesController');

        Route::get('subbus','Admin\student\subbusController@index');
        Route::post('sub_student','Admin\student\subbusController@substudent');
        Route::post('sub_appointments','Admin\student\subbusController@subappointments');
        // Route::get('grades_data','Admin\student\studentController@grades_data')->name('grades_data');
        Route::post('sub_exams','Admin\student\subbusController@subexams');
        Route::get('remove_subparents/{parent_id}/{id}','Admin\student\subbusController@remove_subparents')->name('remove_subparents.destroy');
        Route::get('remove_subexam/{id}/{exam_id}','Admin\student\subbusController@remove_subexam')->name('remove_subexam.destroy');

        Route::resource('parents','Admin\parents\ParentsController');


//        employees
        Route::resource('employees','Admin\employees\EmployeeController');

//        blog
        Route::resource('blog','Admin\blog\BlogController');
        Route::resource('breaknews','Admin\blog\BreaknewController');

        //  departments
        Route::resource('departments','Admin\Department\DepartmentsController');
        Route::get('departments/department/print','Admin\Department\DepartmentsController@print');
        Route::get('departments/reports/report','Admin\Department\DepartmentsController@reports')->name('departments.reports');
        Route::get('departments/reports/details','Admin\Department\DepartmentsController@details')->name('departments.details');
        Route::post('departments/reports/pdf','Admin\Department\DepartmentsController@pdf');
//
        Route::get('departments/department/Review','Admin\Department\DepartmentsController@Review');
        Route::get('departments/department/reviewdepartment','Admin\Department\DepartmentsController@reviewdepartment')->name('reviewdepartment');
        Route::get('departments/{id}/show','Admin\Department\DepartmentsController@show_Department')->name('show_Department');


//
        //        cc
        Route::resource('cc','Admin\CC\CcController');
        Route::get('cc/report/motioncc','Admin\Cc\ReportController@motioncc');
        Route::get('cc/report/motioncc/show','Admin\Cc\ReportController@show');
        Route::get('cc/report/motioncc/details','Admin\Cc\ReportController@details');
        Route::get('cc/report/ccpublicbalance','Admin\Cc\ReportController@CCpublicbalance');
        Route::get('cc/report/ccpublicbalance/level','Admin\Cc\ReportController@CCpublicbalancelevel');
        Route::get('cc/report/ccpublicbalance/print','Admin\Cc\ReportController@CCpublicbalanceprint');
        Route::POST('cc/report/ccpublicbalance/pdf','Admin\Cc\ReportController@CCpublicbalancepdf');
        Route::post('cc/reports/pdf','Admin\Cc\ReportController@pdf');
        Route::get('cc/report/checkReports','Admin\Cc\ReportController@checkReports');
        Route::get('cc/report/checkReports/show','Admin\Cc\ReportController@checkShow');
        Route::get('cc/report/checkReports/details','Admin\Cc\ReportController@checkDetails');
        Route::post('cc/report/checkReports/pdf','Admin\Cc\ReportController@print');



//        funds and banks
        Route::get('banks/Receipt/create','Admin\banks\ReceiptController@create')->name('receipt.create');
        Route::get('banks/Receipt/show','Admin\banks\ReceiptController@show')->name('receipt.show');
        Route::get('banks/Receipt/detailsSelect','Admin\banks\ReceiptController@detailsSelect')->name('receipt.detailsSelect');
        Route::get('banks/Receipt/cc','Admin\banks\ReceiptController@cc')->name('receipt.cc');
        Route::get('banks/Receipt/invoice','Admin\banks\ReceiptController@index')->name('receipts.invoice');
        Route::get('receiptsData/create','Admin\banks\ReceiptController@receiptsData')->name('receiptsData.create');
//        Route::delete('receiptsData/{id}','Admin\banks\ReceiptController@receiptsDataDelete')->name('receiptsData.destroy');
        Route::get('banks/Receipt/invoice/invoice','Admin\banks\ReceiptController@invoice');
        Route::get('banks/Receipt/receipts/{id}/edit','Admin\banks\ReceiptController@edit');
        Route::put('banks/Receipt/receipts/{id}','Admin\banks\ReceiptController@update')->name('receipts.update');

//        edit by Ibrahim El Monier
        Route::post('banks/Receipt/receipts/pdf/{id}','Admin\banks\ReceiptController@pdf');
//        end edit by Ibrahim El Monier
        Route::post('banks/Receipt/receipts/print/{id}','Admin\banks\ReceiptController@print');
        Route::get('banks/Receipt/receipts/print/{id}','Admin\banks\ReceiptController@print')->name('receipts.print');
        Route::get('banks/Receipt/receipts','Admin\banks\ReceiptController@receipts')->name('receipts');
        Route::post('receiptsData/editdatatable','Admin\banks\ReceiptController@editdatatable');
        Route::post('receiptsData/select','Admin\banks\ReceiptController@select');
        Route::get('banks/Receipt/receipts/{id}','Admin\banks\ReceiptController@receiptsShow')->name('receiptsShow');
        Route::delete('banks/Receipt/invoice/{id}','Admin\banks\ReceiptController@destroy')->name('receipts.destroy');
        Route::post('banks/Receipt','Admin\banks\ReceiptController@store')->name('receipt.store');
        Route::post('receiptsData/delete','Admin\banks\ReceiptController@delete');
        Route::post('receiptsData/singledelete','Admin\banks\ReceiptController@singledelete');
        Route::get('banks/Receipt/receipts/catch/catch','Admin\banks\ReceiptController@catchindex')->name('receipts.catch');
        Route::get('banks/Receipt/receipts/caching/caching','Admin\banks\ReceiptController@cachingindex')->name('receipts.caching');
        Route::get('banks/Receipt/receipts/catch/all','Admin\banks\ReceiptController@catch')->name('receipts.catch');
        Route::get('banks/Receipt/receipts/caching/all','Admin\banks\ReceiptController@caching')->name('receipts.caching');
//
//        limitations
        Route::resource('limitations','Admin\limitations\LimitationsController');
        Route::get('limitations/daily/create','Admin\limitations\LimitationsController@daily');
        Route::get('limitations/dept/create','Admin\limitations\LimitationsController@debt');
        Route::get('limitations/cred/create','Admin\limitations\LimitationsController@cred');
        Route::get('limitations/notice/noticedebt','Admin\limitations\LimitationsController@noticedebt');
//        Route::get('limitations/notice/noticecred','Admin\limitations\LimitationsController@noticecred');
        Route::get('limitations/show/{id}','Admin\limitations\limitationsData@show')->name('limitations.show');
        Route::post('limitationsData/create','Admin\limitations\limitationsData@create');
        Route::post('limitationsData/editdatatable','Admin\limitations\limitationsData@editdatatable');
        Route::post('limitationsData','Admin\limitations\limitationsData@store')->name('limitations.store');
        Route::get('limitationsData/invoice','Admin\limitations\limitationsData@index')->name('limitations.invoice');
        Route::get('limitationsData/invoice/invoice','Admin\limitations\limitationsData@invoice');
        Route::post('limitationsData/invoice/print/{id}','Admin\limitations\limitationsData@print');
        Route::get('limitationsData/invoice/print/{id}','Admin\limitations\limitationsData@print')->name('limitation.print');
        Route::post('limitationsData/invoice/pdf/{id}','Admin\limitations\limitationsData@pdf');
        Route::post('limitationsData/select','Admin\limitations\limitationsData@select');
        Route::post('limitationsData/delete','Admin\limitations\limitationsData@destroy');
        Route::post('limitationsData/softdelete','Admin\limitations\limitationsData@softdelete');

//        openingentry
        Route::resource('openingentry','Admin\limitations\OpeningEntryController');
        Route::post('openingentrydata','Admin\limitations\OpeningEntryData@store')->name('openingentrydata.store');
        Route::post('openingentrydata/create','Admin\limitations\OpeningEntryData@create');
        Route::post('openingentrydata/select','Admin\limitations\OpeningEntryData@select');
        Route::get('openingentrydata/invoice','Admin\limitations\OpeningEntryData@index')->name('openingentrydata.invoice');
        Route::get('openingentrydata/show/{id}','Admin\limitations\OpeningEntryData@show')->name('openingentrydata.show');
        Route::post('openingentrydata/invoice/print/{id}','Admin\limitations\OpeningEntryData@print');
        Route::get('openingentrydata/invoice/print/{id}','Admin\limitations\OpeningEntryData@print')->name('openingentry.print');
        Route::post('openingentrydata/invoice/pdf/{id}','Admin\limitations\OpeningEntryData@pdf');
        Route::get('openingentrydata/invoice/invoice','Admin\limitations\OpeningEntryData@invoice');


//        accountingReports
//        dailyReport
        Route::get('dailyReport','Admin\accountingReports\dailyReportController@index');
        Route::get('dailyReport/show','Admin\accountingReports\dailyReportController@show');
        Route::get('dailyReport/details','Admin\accountingReports\dailyReportController@details');
        Route::post('dailyReport/pdf','Admin\accountingReports\dailyReportController@pdf');



//        accountStatement
        Route::get('accountStatement','Admin\accountingReports\accountStatementController@index');
        Route::get('accountStatement/show','Admin\accountingReports\accountStatementController@show');
        Route::get('accountStatement/details','Admin\accountingReports\accountStatementController@details');
        Route::Post('accountStatement/pdf','Admin\accountingReports\accountStatementController@pdf');
//        trialBalanceController
        Route::get('trialbalance','Admin\accountingReports\trialBalanceController@index');
        Route::get('trialbalance/show','Admin\accountingReports\trialBalanceController@show');
        Route::get('trialbalance/details','Admin\accountingReports\trialBalanceController@details');
        Route::get('trialbalance/details2','Admin\accountingReports\trialBalanceController@details2');
        Route::post('trialbalance/pdf','Admin\accountingReports\trialBalanceController@pdf');
        Route::post('trialbalance/pdf2','Admin\accountingReports\trialBalanceController@pdf2');


//        publicbalance
        Route::get('publicbalance','Admin\accountingReports\publicBalanceController@index');
        Route::get('publicbalance/show','Admin\accountingReports\publicBalanceController@show');
        Route::post('publicbalance/pdf','Admin\accountingReports\publicBalanceController@pdf');
        Route::get('publicbalance/level','Admin\accountingReports\publicBalanceController@level');



    });

});
